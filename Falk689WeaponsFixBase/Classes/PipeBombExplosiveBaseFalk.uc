class PipeBombExplosiveBaseFalk extends KFMod.PipeBombExplosive;

var int   fBackupSellValue;      // backup sell value for when we reach 0 ammo
var bool  fWeaponReady;          // used to prevent multi nade throw

var bool  fDoBringUpAnim;
var float fCurReadyTime;

var bool  fEmptyAnim;

replication
{
    reliable if(Role != ROLE_Authority)
        fEmptyAnim, fWeaponReady, fDoBringUpAnim;

    reliable if(Level.NetMode == NM_ListenServer)
        SetClientGrenadeState;
}

// set fWeaponReady to false before bringing this up
simulated function BringUp(optional Weapon PrevWeapon)
{
    local int Mode;

    if (ClientGrenadeState == GN_None)
    {
        fWeaponReady    = false;
        fCurReadyTime   = BringUpTime + 0.2;
    }

    fDoBringUpAnim  = false;

    if (!HasAmmo() && (ClientGrenadeState == GN_BringUp || KFPawn(Instigator).bIsQuickHealing > 0))
    {
        //warn("HAS AMMO"@HasAmmo()@"ClientGrenadeState"@ClientGrenadeState == GN_BringUp@"healing"@KFPawn(Instigator).bIsQuickHealing > 0);

        DioMerda(PrevWeapon);

        if (Level.NetMode == NM_ListenServer)
        {
            SetClientGrenadeState(GN_None);
            ClientGrenadeState = GN_None;
        }

        return;
    }

    HandleSleeveSwapping();

    if (KFHumanPawn(Instigator) != none)
        KFHumanPawn(Instigator).SetAiming(false);

    bAimingRifle = false;
    bIsReloading = false;
    IdleAnim = default.IdleAnim;

    // (not) From Weapon.uc
    if (ClientState == WS_Hidden || ClientGrenadeState == GN_BringUp || KFPawn(Instigator).bIsQuickHealing > 0)
    {
        if (!fEmptyAnim)
        {
            PlayOwnedSound(SelectSound, SLOT_Interact,,,,, false);
            ClientPlayForceFeedback(SelectForce);  //
        }

        if (Instigator.IsLocallyControlled())
        {
            if (Mesh!=None && HasAnim(SelectAnim))
            {
                if (ClientGrenadeState == GN_BringUp || KFPawn(Instigator).bIsQuickHealing > 0)
                    PlayAnim(SelectAnim, SelectAnimRate * (BringUpTime/QuickBringUpTime), 0.0);

                else
                    PlayAnim(SelectAnim, SelectAnimRate, 0.0);
            }
        }

        ClientState = WS_BringUp;

        if (ClientGrenadeState == GN_BringUp || KFPawn(Instigator).bIsQuickHealing > 0)
        {
            ClientGrenadeState = GN_None;
            SetTimer(QuickBringUpTime, false);
        }

        else
            SetTimer(BringUpTime, false);
    }

    for (Mode = 0; Mode < NUM_FIRE_MODES; Mode++)
    {
        FireMode[Mode].bIsFiring = false;
        FireMode[Mode].HoldTime = 0.0;
        FireMode[Mode].bServerDelayStartFire = false;
        FireMode[Mode].bServerDelayStopFire = false;
        FireMode[Mode].bInstantStop = false;
    }

    if ((PrevWeapon != None) && PrevWeapon.HasAmmo() && !PrevWeapon.bNoVoluntarySwitch)
        OldWeapon = PrevWeapon;

    else
        OldWeapon = None;
}

// part of the vanilla bringup without the anims, here to fix the quick bringup anim doing weird shit
simulated function DioMerda(optional Weapon PrevWeapon)
{
    local int Mode;

    HandleSleeveSwapping();

    if (KFHumanPawn(Instigator) != none)
        KFHumanPawn(Instigator).SetAiming(false);

    bAimingRifle = false;
    bIsReloading = false;

    ClientState        = WS_BringUp;
    ClientGrenadeState = GN_None;
    SetTimer(QuickBringUpTime, false);

    for (Mode = 0; Mode < NUM_FIRE_MODES; Mode++)
    {
        FireMode[Mode].bIsFiring = false;
        FireMode[Mode].HoldTime = 0.0;
        FireMode[Mode].bServerDelayStartFire = false;
        FireMode[Mode].bServerDelayStopFire = false;
        FireMode[Mode].bInstantStop = false;
    }

    if ((PrevWeapon != None) && PrevWeapon.HasAmmo() && !PrevWeapon.bNoVoluntarySwitch)
        OldWeapon = PrevWeapon;

    else
        OldWeapon = None;
}


// attempt to fix this variable never resetting in listen server
simulated function SetClientGrenadeState(EClientGrenadeState NewState)
{
    //warn("Set new state"@NewState);
    ClientGrenadeState = NewState;
}


// called regardless of this being the current weapon
simulated function Tick(float dt)
{
    if (!fWeaponReady && Instigator.IsLocallyControlled())
    {
        if (fCurReadyTime >= 0)
            fCurReadyTime -= dt;

        else
        {
            fWeaponReady  = true;
            //warn("READY"@Level.TimeSeconds);
        }
    }

    Super.Tick(dt);
}


// return true if we can switch this weapon
simulated function bool fCanBeSwitched()
{
    if (ClientState != WS_ReadyToFire || FireMode[0].bIsFiring || (HasAmmo() && (!FireMode[0].AllowFire() || FireMode[0].NextFireTime + 0.2 > Level.TimeSeconds)))
        return false;

    return true;
}

// return true if we can throw a nade
simulated function bool fCanThrowGrenade()
{
    local bool result;

    if (!fWeaponReady || ClientGrenadeState != GN_None)
    {
        //warn("BLOCK NADE"@Level.TimeSeconds);
        return false;
    }

    result = fCanBeSwitched();

    //warn("CAN NADE:"@result@"TIME:"@Level.TimeSeconds);

    return result;
}

defaultproperties
{
}
