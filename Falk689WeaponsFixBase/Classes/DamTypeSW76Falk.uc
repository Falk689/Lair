class DamTypeSW76Falk extends KFProjectileWeaponDamageType;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FArtilleristDamage', Amount);
}

defaultproperties
{
   HeadShotDamageMult=1.100000
   DeathString="%k killed %o (SW76)"
   KDamageImpulse=3500.000000
   KDeathVel=110.000000
   KDeathUpKick=15.000000
   bIsPowerWeapon=False
}
