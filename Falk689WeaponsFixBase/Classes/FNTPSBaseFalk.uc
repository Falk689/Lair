class FNTPSBaseFalk extends FN_TPS.FN_TPS
   abstract;

var float fLastBulletRate;              // last bullet reload rate, used to see the entire reload animation
var bool  fShouldBreechReload;          // should we actually play the whole animation?
var bool  fLoadingLastBullet;           // we're loading the last bullet, trigger stuff and prevent weapon drop and shit

replication
{
   reliable if((Level.NetMode == NM_ListenServer && Role != ROLE_Authority) || Role == ROLE_Authority)
      fShouldBreechReload, fLoadingLastBullet;
}

defaultproperties
{
}
