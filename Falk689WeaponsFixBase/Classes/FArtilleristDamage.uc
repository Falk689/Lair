class FArtilleristDamage extends SRCustomProgressInt;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FArtilleristDamage', Amount);
}

defaultproperties 
{
   ProgressName = "Artillerist damage"
}
