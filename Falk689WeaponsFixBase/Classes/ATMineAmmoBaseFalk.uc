class ATMineAmmoBaseFalk extends ATMine.ATMineAmmo
    abstract;

var float           fWeaponWeight;

// here for the buy menu
static function bool FShouldBuyAmmo(int currentAmmo, int buyAmmo, KFHumanPawn KFHP)
{
    if (KFHP != none && currentAmmo <= 0 && currentAmmo + buyAmmo > 0)
    {
        if (KFHP.CanCarry(default.fWeaponWeight))
            return true;

        return false;
    }

    return true;
}

// used to prevent adding ammo to this class and to add the weight back to the pawn in case we do
function bool FShouldAddAmmo(int AmmoToAdd)
{
    local KFHumanPawn KFHP;

    if (AmmoAmount <= 0 && AmmoAmount + AmmoToAdd > 0)
    {
        KFHP = KFHumanPawn(Owner);

        if (KFHP != none && KFHP.CanCarry(default.fWeaponWeight))
            KFHP.CurrentWeight += default.fWeaponWeight;

        else if (KFHP != none)
            return false;
    }

    return true;
}

defaultproperties
{
    fWeaponWeight=1.000000
}
