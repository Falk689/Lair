class DamTypeFragImpactFalk extends KFWeaponDamageType;


defaultproperties
{
   bIsExplosive=False
   bCheckForHeadShots=False
   WeaponClass=Class'KFMod.Frag'
   DeathString="%o filled %k's body with shrapnel."
   FemaleSuicide="%o blew up."
   MaleSuicide="%o blew up."
   bLocationalHit=False
   bThrowRagdoll=True
   bExtraMomentumZ=True
   DamageThreshold=1
   DeathOverlayMaterial=Combiner'Effects_Tex.GoreDecals.PlayerDeathOverlay'
   DeathOverlayTime=999.000000
   KDamageImpulse=2500.000000
   KDeathVel=300.000000
   KDeathUpKick=200.000000
   HumanObliterationThreshhold=150
}
