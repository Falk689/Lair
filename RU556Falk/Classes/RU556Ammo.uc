class RU556Ammo extends KFAmmunition;

defaultproperties
{
   AmmoPickupAmount=30
   MaxAmmo=300
   InitialAmount=120
   ItemName="5.56x45mm NATO rounds"
   IconMaterial=Texture'KillingFloorHUD.Generic.HUD'
   IconCoords=(X1=336,Y1=82,X2=382,Y2=125)
}
