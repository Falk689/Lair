class Falk689GameTypeSP extends Falk689GameType;

var bool               fSPZedTimeUsed;   // single player variable
var bool               fAlreadySkipped;  // used to fix trader skipping in solo

var float              FSingleSlayTimer;
var float              FSingleHeavySlayTimer;
var int                FSingleSlayNumber;

replication
{
    reliable if(Role == ROLE_Authority)
        fSPZedTimeUsed, fAlreadySkipped;
}

// We're alone, just skip the trader.
function bool VoteSkipTrader(PlayerController PC, optional FalkIdState fState)
{
    if (fAlreadySkipped)
        return False;

    fAlreadySkipped   = True;
    fShouldSkipTrader = True;

    return True;
}

// revert this to vanilla
function RestartPlayer(Controller aPlayer)
{
    Super(KFGameType).RestartPlayer(aPlayer);
}

// force game length to custom
event PreBeginPlay()
{
    FinalWave    = 10;
    KFGameLength = GL_Custom;
    Super.PreBeginPlay();
}

// Return true if we can use our zed time skill, simplified to sp mode
function bool CanUseZedTimeSkill(PlayerController P, float ZTECD, float ZTSCD)
{
    if (InZedTime())
    {

        if (!fSPZedTimeUsed                               &&
                Level.TimeSeconds >= fZedTimeEnd + ZTECD       &&
                Level.TimeSeconds >= LastZedTimeEvent + ZTSCD)
            return True;
    }

    return false;
}

// simplified to set single player used state
function ZedTimeSkillUsed(PlayerController P)
{
    fSPZedTimeUsed = True;
}

// simplified reset zed time skill
function ZedTimeEndReset()
{
    fSPZedTimeUsed = False;
}

// edited ammo pickups, added a proper slay timer, implemented a part or the new spawn system
State MatchInProgress
{
    // now my kids tend to die if you (a single parent) forget them in your car under the boiling sun, just like irl
    function Tick(float Delta)
    {
        local Controller C;
        local array<FalkMonster> FC;

        Global.Tick(Delta);

        if (Level.NetMode == NM_ListenServer && fBossWaveStarted && FWaveState == F_Wave_Init && Level.TimeSeconds >= NextMonsterTime)
        {
            //warn("TRY SPAWN");
            NextMonsterTime = Level.TimeSeconds + 0.05;
            AddBoss();
        }

        else if (Level.NetMode == NM_ListenServer && bWaveInProgress && Level.TimeSeconds >= NextMonsterTime && (FWaveState == F_Wave_In_Progress || (!fBossWaveStarted && FWaveState != F_Wave_Ending && FWaveState != F_Wave_Boss_Ended)))
        {
            NextMonsterTime = Level.TimeSeconds + CalcNextSquadSpawnTime(); // get next squad spawn time

            if (!bDisableZedSpawning)
            {
                //warn("AddSquad"@Level.TimeSeconds@"NextTime"@NextMonsterTime);
                AddSquad();
            }
        }

        if (Level.NetMode == NM_ListenServer && !fBossWaveStarted && FCurrentWave < FinalWave && NumMonsters <= FSingleSlayNumber)
        {
            // normal zeds slay timer
            if (!FSlayedZeds)
            {
                // wait
                if (FCurSlayTimer < FSingleSlayTimer)
                    FCurSlayTimer += Delta;

                // slay them
                else if (!FSlayedZeds)
                {
                    FSlayedZeds = True;

                    for (C=Level.ControllerList; C!=None; C=C.NextController)
                    {
                        if (C != none && C.Pawn                 != none &&
                                FalkMonster(C.Pawn)                 != none &&
                                FalkZombieBoss(C.Pawn)              == none &&
                                FalkZombieFleshpound(C.Pawn)        == none &&
                                FalkZombieScrake(C.Pawn)            == none &&
                                FZombieMetalClot_STANDARD(C.Pawn)   == none)
                        {
                            FC[FC.Length] = FalkMonster(C.Pawn);
                        }
                    }

                    FSlayZedsList(FC);
                }
            }

            // after slaying the others, wait a bit more before trying to slay heavy ones
            else if (FCurSlayTimer < FSingleHeavySlayTimer)
                FCurSlayTimer += Delta;

            // periodical checks to slay heavy zeds
            else if (FHeavyZedsSlayTimer > 0)
                FHeavyZedsSlayTimer -= Delta;

            // see if we can slay heavy zeds
            else
            {
                FHeavyZedsSlayTimer = Default.FHeavyZedsSlayTimer;

                for (C=Level.ControllerList; C!=None; C=C.NextController)
                {
                    if (C != none && C.Pawn != none && FalkMonster(C.Pawn) != none &&
                            ((FalkZombieFleshpound(C.Pawn)      != none &&  FalkZombieFleshpound(C.Pawn).fSlayMe()) ||
                             (FalkZombieScrake(C.Pawn)          != none &&  FalkZombieScrake(C.Pawn).fSlayMe())     ||
                             (FZombieMetalClot_STANDARD(C.Pawn) != none &&  FZombieMetalClot_STANDARD(C.Pawn).fSlayMe())))
                    {
                        FC[FC.Length] = FalkMonster(C.Pawn);
                    }
                }

                FSlayZedsList(FC);
            }
        }
    }

    function BeginState()
    {
        Super.BeginState();

        // two seconds initial countdown
        WaveCountDown   = 2;
        NextMonsterTime = Level.TimeSeconds + WaveCountDown + CalcNextSquadSpawnTime();
    }
}

// allow skip trader
function FalkEnablePerkSelect()
{
    fAlreadySkipped = False;
}

// nope
function FlareSpawned(PlayerController PC)
{
}

// hyper nope
function FlareDestroyed(PlayerController PC)
{
}

// it's single player, just check if we're alive
function int GetAlivePlayers()
{
    local Controller C;

    C = Level.GetLocalPlayerController();

    if (C != none && C.Pawn != none && !C.Pawn.bPendingDelete && (FHumanPawn(C.Pawn) != none && FHumanPawn(C.Pawn).fHealth > 0))
        return 1;

    return 0;
}

// just get the local player
function Controller FGetNewZedSpawnPlayer()
{
    local Controller C;

    // just get the local player
    C = Level.GetLocalPlayerController();

    if (C != none)
        return C;

    // backup system to find the controller since KF is doing its best not to spawn zeds
    for (C=Level.ControllerList; C!=None; C=C.NextController)
    {
        if (C.PlayerReplicationInfo != None && C.bIsPlayer && !C.PlayerReplicationInfo.bOnlySpectator && C.Pawn != none && !C.Pawn.bPendingDelete && (FHumanPawn(C.Pawn) != none && FHumanPawn(C.Pawn).fHealth > 0))
            return C;
    }

    warn("local FGetNewZedSpawnPlayer loop exited without result... fuck.");

    return none;
}

// return how many zombies we should spawn at once
function int GetMaxZombiesOnce(int fPlayers)
{
    return 28;
}


defaultproperties
{
    GameName="Lair Single Player"
    Description="Total game balance rework, with more guns, more zeds, custom maps, and with a side of bugfixes. This game mode is meant to be a demo experience to let you test the waters and get you acquainted with the rework itself. All perk levels are set to 9 and there is no progression. You're welcome in our online servers to experience the originally intended mode."
    bEnemyHealthBars=False
    FallbackMonsterClass="Falk689ZedsFix.FZombieStalker_STANDARD"
    InitialWave=0
    FinalWave=10
    FSingleSlayTimer=65.0
    FSingleHeavySlayTimer=130.0
    FSingleSlayNumber=3
    bAllowNonTeamChat=True
    FriendlyFireScale=0.000000
    NetWait=5
    bForceRespawn=True
    bAdjustSkill=False
    bAllowTaunts=True
    bAllowTrans=False
    SpawnProtectionTime=0.000000
    LateEntryLives=1
    bAllowPrivateChat=True
    bWeaponStay=True
    bAllowWeaponThrowing=True
    ResetTimeDelay=10
    GoalScore=60
    MaxLives=1
    TimeLimit=0
    ScreenShotName="LairTextures_T.Thumbnails.LairTGBSinglePlayer"
    StartingCash=500
    //FStartingCash=500
    GIPropsExtras(0)="2.000000;Normal;4.000000;Hard;5.000000;Suicidal;7.000000;Hell On Earth"
}
