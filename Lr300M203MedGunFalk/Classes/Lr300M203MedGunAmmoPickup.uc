class Lr300M203MedGunAmmoPickup extends KFAmmoPickup;

defaultproperties
{
    AmmoAmount=20
    InventoryType=Class'Lr300M203MedGunAmmo'
    PickupMessage="You found 5.56x45mm NATO rounds"
    StaticMesh=StaticMesh'KillingFloorStatics.L85Ammo'
}