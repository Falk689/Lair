class Med203Fire extends M203FireFalk;

// this was here uncommented, probably unneded, still...
function InitEffects()
{
    Super.InitEffects();

    if (FlashEmitter != None)
		Weapon.AttachToBone(FlashEmitter, 'tip2');
}


// don't log errors on spawn
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   return Super(M79Fire).SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local Lr300MedicNadeFalk FG;

   if (P != None)
   {
      FG = Lr300MedicNadeFalk(P);

      if (FG != none && Instigator != none && Instigator.Controller != None)
         FG.fCInstigator = Instigator.Controller;
   }

   Super(M79Fire).PostSpawnProjectile(P);
}

defaultproperties
{
   EffectiveRange=2500.000000
   maxVerticalRecoilAngle=200
   maxHorizontalRecoilAngle=50
   FireAimedAnim="Fire_Secondary"
   FireSoundRef="KF_M79Snd.M79_Fire"
   StereoFireSoundRef="KF_M79Snd.M79_FireST"
   NoAmmoSoundRef="KF_M79Snd.M79_DryFire"
   ProjPerFire=1
   ProjSpawnOffset=(X=50.000000,Y=10.000000)
   bWaitForRelease=True
   TransientSoundVolume=1.800000
   FireAnim="Fire_Secondary"
   FireForce="AssaultRifleFire"
   FireRate=3.69
   AmmoClass=Class'Med203Ammo'
   ShakeRotMag=(X=3.000000,Y=4.000000,Z=2.000000)
   ShakeRotRate=(X=10000.000000,Y=10000.000000,Z=10000.000000)
   ShakeOffsetMag=(X=3.000000,Y=3.000000,Z=3.000000)
   ProjectileClass=Class'Lr300MedicNadeFalk'
   BotRefireRate=1.800000
   FlashEmitterClass=Class'ROEffects.MuzzleFlash1stNadeL'
   aimerror=20.000000
   Spread=0.015000
   FireLastRate=0.6
   FireLastAnim="FireLast_Secondary"
}
