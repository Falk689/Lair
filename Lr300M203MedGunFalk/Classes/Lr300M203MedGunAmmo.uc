class Lr300M203MedGunAmmo extends KFAmmunition;

#EXEC OBJ LOAD FILE=KillingFloorHUD.utx

defaultproperties
{
    AmmoPickupAmount=20
    MaxAmmo=300
    InitialAmount=140
    PickupClass=Class'Lr300M203MedGunAmmoPickup'
    IconMaterial=Texture'KillingFloorHUD.Generic.HUD'
    IconCoords=(X1=336,Y1=82,X2=382,Y2=125)
    ItemName="5.56x45mm NATO rounds"
}