class Lr300M203MedGunPickup extends Lr300M203MedGunPickupBase;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   cost=4000
   Weight=7.000000
   AmmoCost=10
   BuyClipSize=20
   PowerValue=22
   SpeedValue=50
   RangeValue=100
   ItemName="Medical LR300+M203 Assault Rifle"
   ItemShortName="LR300-M"
   SecondaryAmmoShortName="M203 Mednades"
   PickupMessage="You got a Medical LR300+M203 Assault Rifle"
   AmmoItemName="5.56x45mm NATO rounds"
   AmmoMesh=StaticMesh'KillingFloorStatics.L85Ammo'
   EquipmentCategoryID=3
   CorrespondingPerkIndex=0
   InventoryType=Class'Lr300M203MedGun'
   PickupForce="AssaultRiflePickup"
   StaticMesh=StaticMesh'LR300M203_A.lr300_stc.w_lr300m203'
   CollisionRadius=25.000000
   CollisionHeight=5.000000
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
