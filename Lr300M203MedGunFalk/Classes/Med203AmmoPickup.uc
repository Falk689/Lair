class Med203AmmoPickup extends KFAmmoPickup;

defaultproperties
{
     AmmoAmount=2
     InventoryType=Class'Med203Ammo'
     PickupMessage="M203 Mednades"
     StaticMesh=StaticMesh'KillingFloorStatics.FragPickup'
     CollisionRadius=25.000000
}
