class Lr300M203MedGun extends Lr300M203MedGunBase
   config(user);

#exec OBJ LOAD FILE=LR300M203_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE="LairAnimations_A.ukx"

var name ReloadShortAnim;                   // short reload anim name
var float ReloadShortRate;                  // short reload anim rate

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

var float             FBringUpTime;         // last time bringup was called
var float             FBringUpSafety;       // safety to prevent reload on server before the weapon is ready

var color ChargeColor;

var float Range;
var float LastRangingTime;

var() Material ZoomMat;
var() Sound ZoomSound;
var bool bArrowRemoved;

var()		int			lenseMaterialID;

var()		float		scopePortalFOVHigh;
var()		float		scopePortalFOV;
var()       vector      XoffsetScoped;
var()       vector      XoffsetHighDetail;

var()		int			scopePitch;
var()		int			scopeYaw;
var()		int			scopePitchHigh;
var()		int			scopeYawHigh;

// 3d Scope vars
var   ScriptedTexture   ScopeScriptedTexture;
var	  Shader		    ScopeScriptedShader;
var   Material          ScriptedTextureFallback;

var     Combiner            ScriptedScopeCombiner;

var     texture             TexturedScopeTexture;

var	    bool				bInitializedScope;

exec function pfov(int thisFOV)
{
   if( !class'ROEngine.ROLevelInfo'.static.RODebugMode() )
      return;

   scopePortalFOV = thisFOV;
}

exec function pPitch(int num)
{
   if( !class'ROEngine.ROLevelInfo'.static.RODebugMode() )
      return;

   scopePitch = num;
   scopePitchHigh = num;
}

exec function pYaw(int num)
{
   if( !class'ROEngine.ROLevelInfo'.static.RODebugMode() )
      return;

   scopeYaw = num;
   scopeYawHigh = num;
}

simulated exec function TexSize(int i, int j)
{
   if( !class'ROEngine.ROLevelInfo'.static.RODebugMode() )
      return;

   ScopeScriptedTexture.SetSize(i, j);
}

// stuff for the scope
simulated function bool ShouldDrawPortal()
{
   if( bAimingRifle )
      return true;
   else
      return false;
}

simulated function PostBeginPlay()
{
   super.PostBeginPlay();

   // Get new scope detail value from KFWeapon
   KFScopeDetail = class'KFMod.KFWeapon'.default.KFScopeDetail;

   UpdateScopeMode();
}

// add short reload
exec function ReloadMeNow()
{
   local float ReloadMulti;

   if(!AllowReload())
      return;

   if (bHasAimingMode && bAimingRifle)
   {
      FireMode[1].bIsFiring = False;

      ZoomOut(false);
      if( Role < ROLE_Authority)
         ServerZoomOut(false);
   }

   if (KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none)
      ReloadMulti = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.Static.GetReloadSpeedModifier(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo), self);

   else
      ReloadMulti = 1.0;

   bIsReloading = true;
   ReloadTimer  = Level.TimeSeconds;

   if (MagAmmoRemaining <= 0)
      ReloadRate = Default.ReloadRate / ReloadMulti;

   else if (MagAmmoRemaining >= 1)
      ReloadRate = Default.ReloadShortRate / ReloadMulti;

   if (bHoldToReload)
      NumLoadedThisReload = 0;

   ClientReload();
   Instigator.SetAnimAction(WeaponReloadAnim);

   // Reload message commented out for now - Ramm
   if (Level.Game.NumPlayers > 1 && KFGameType(Level.Game).bWaveInProgress && KFPlayerController(Instigator.Controller) != none &&
      Level.TimeSeconds - KFPlayerController(Instigator.Controller).LastReloadMessageTime > KFPlayerController(Instigator.Controller).ReloadMessageDelay)
   {
      KFPlayerController(Instigator.Controller).Speech('AUTO', 2, "");
      KFPlayerController(Instigator.Controller).LastReloadMessageTime = Level.TimeSeconds;
   }
}

// add the short reload animation
simulated function ClientReload()
{
   local float ReloadMulti;

   if (bHasAimingMode && bAimingRifle)
   {
      FireMode[1].bIsFiring = False;

      ZoomOut(false);
      if( Role < ROLE_Authority)
         ServerZoomOut(false);
   }

   if (KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none)
      ReloadMulti = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.Static.GetReloadSpeedModifier(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo), self);

   else
      ReloadMulti = 1.0;

   bIsReloading = true;
   if (MagAmmoRemaining <= 0)
      PlayAnim(ReloadAnim, ReloadAnimRate*ReloadMulti, 0.1);

   else if (MagAmmoRemaining >= 1)
      PlayAnim(ReloadShortAnim, ReloadAnimRate*ReloadMulti, 0.1);
}

// Handles initializing and swithing between different scope modes
simulated function UpdateScopeMode()
{
   if (Level.NetMode != NM_DedicatedServer && Instigator != none && Instigator.IsLocallyControlled() &&
      Instigator.IsHumanControlled() )
   {
      if( KFScopeDetail == KF_ModelScope )
      {
            scopePortalFOV = default.scopePortalFOV;
            ZoomedDisplayFOV = default.ZoomedDisplayFOV;
            //bPlayerFOVZooms = false;
            if (bUsingSights)
            {
               PlayerViewOffset = XoffsetScoped;
            }

            if( ScopeScriptedTexture == none )
            {
               ScopeScriptedTexture = ScriptedTexture(Level.ObjectPool.AllocateObject(class'ScriptedTexture'));
            }

            ScopeScriptedTexture.FallBackMaterial = ScriptedTextureFallback;
            ScopeScriptedTexture.SetSize(1024,1024);
            ScopeScriptedTexture.Client = Self;

            if( ScriptedScopeCombiner == none )
            {
               // Construct the Combiner
               ScriptedScopeCombiner = Combiner(Level.ObjectPool.AllocateObject(class'Combiner'));
               ScriptedScopeCombiner.Material1 = Texture'LR300M203_A.lr300_txr.AimPoint';
               ScriptedScopeCombiner.FallbackMaterial = Shader'ScopeShaders.Zoomblur.LensShader';
               ScriptedScopeCombiner.CombineOperation = CO_Multiply;
               ScriptedScopeCombiner.AlphaOperation = AO_Use_Mask;
               ScriptedScopeCombiner.Material2 = ScopeScriptedTexture;
            }

            if( ScopeScriptedShader == none )
            {
               // Construct the scope shader
               ScopeScriptedShader = Shader(Level.ObjectPool.AllocateObject(class'Shader'));
               ScopeScriptedShader.Diffuse = ScriptedScopeCombiner;
               ScopeScriptedShader.SelfIllumination = ScriptedScopeCombiner;
               ScopeScriptedShader.FallbackMaterial = Shader'ScopeShaders.Zoomblur.LensShader';
            }

            bInitializedScope = true;
      }
      else if( KFScopeDetail == KF_ModelScopeHigh )
      {
            scopePortalFOV = scopePortalFOVHigh;
            ZoomedDisplayFOV = default.ZoomedDisplayFOVHigh;
            //bPlayerFOVZooms = false;
            if (bUsingSights)
            {
               PlayerViewOffset = XoffsetHighDetail;
            }

            if( ScopeScriptedTexture == none )
            {
               ScopeScriptedTexture = ScriptedTexture(Level.ObjectPool.AllocateObject(class'ScriptedTexture'));
            }
            ScopeScriptedTexture.FallBackMaterial = ScriptedTextureFallback;
            ScopeScriptedTexture.SetSize(1024,1024);
            ScopeScriptedTexture.Client = Self;

            if( ScriptedScopeCombiner == none )
            {
               // Construct the Combiner
               ScriptedScopeCombiner = Combiner(Level.ObjectPool.AllocateObject(class'Combiner'));
               ScriptedScopeCombiner.Material1 = Texture'LR300M203_A.lr300_txr.AimPoint';
               ScriptedScopeCombiner.FallbackMaterial = Shader'ScopeShaders.Zoomblur.LensShader';
               ScriptedScopeCombiner.CombineOperation = CO_Multiply;
               ScriptedScopeCombiner.AlphaOperation = AO_Use_Mask;
               ScriptedScopeCombiner.Material2 = ScopeScriptedTexture;
            }

            if( ScopeScriptedShader == none )
            {
               // Construct the scope shader
               ScopeScriptedShader = Shader(Level.ObjectPool.AllocateObject(class'Shader'));
               ScopeScriptedShader.Diffuse = ScriptedScopeCombiner;
               ScopeScriptedShader.SelfIllumination = ScriptedScopeCombiner;
               ScopeScriptedShader.FallbackMaterial = Shader'ScopeShaders.Zoomblur.LensShader';
            }

            bInitializedScope = true;
      }
      else if (KFScopeDetail == KF_TextureScope)
      {
            ZoomedDisplayFOV = default.ZoomedDisplayFOV;
            PlayerViewOffset.X = default.PlayerViewOffset.X;

            bInitializedScope = true;
      }
   }
}

simulated event RenderTexture(ScriptedTexture Tex)
{
   local rotator RollMod;

   RollMod = Instigator.GetViewRotation();


   if(Owner != none && Instigator != none && Tex != none && Tex.Client != none)
      Tex.DrawPortal(0,0,Tex.USize,Tex.VSize,Owner,(Instigator.Location + Instigator.EyePosition()), RollMod,  scopePortalFOV );
}

simulated function SetZoomBlendColor(Canvas c)
{
   local Byte    val;
   local Color   clr;
   local Color   fog;

   clr.R = 255;
   clr.G = 255;
   clr.B = 255;
   clr.A = 255;

   if( Instigator.Region.Zone.bDistanceFog )
   {
      fog = Instigator.Region.Zone.DistanceFogColor;
      val = 0;
      val = Max( val, fog.R);
      val = Max( val, fog.G);
      val = Max( val, fog.B);
      if( val > 128 )
      {
            val -= 128;
            clr.R -= val;
            clr.G -= val;
            clr.B -= val;
      }
   }
   c.DrawColor = clr;
}


/**
* Handles all the functionality for zooming in including
* setting the parameters for the weapon, pawn, and playercontroller
*
* @param bAnimateTransition whether or not to animate this zoom transition
*/
simulated function ZoomIn(bool bAnimateTransition)
{
   super(BaseKFWeapon).ZoomIn(bAnimateTransition);

   bAimingRifle = True;

   if( KFHumanPawn(Instigator)!=None )
      KFHumanPawn(Instigator).SetAiming(True);

   if( Level.NetMode != NM_DedicatedServer && KFPlayerController(Instigator.Controller) != none )
   {
      if( AimInSound != none )
      {
            PlayOwnedSound(AimInSound, SLOT_Interact,,,,, false);
      }
   }
}

/**
* Handles all the functionality for zooming out including
* setting the parameters for the weapon, pawn, and playercontroller
*
* @param bAnimateTransition whether or not to animate this zoom transition
*/
simulated function ZoomOut(bool bAnimateTransition)
{
   super.ZoomOut(bAnimateTransition);

   bAimingRifle = False;

   if( KFHumanPawn(Instigator)!=None )
      KFHumanPawn(Instigator).SetAiming(False);

   if( Level.NetMode != NM_DedicatedServer && KFPlayerController(Instigator.Controller) != none )
   {
      if( AimOutSound != none )
      {
            PlayOwnedSound(AimOutSound, SLOT_Interact,,,,, false);
      }
      KFPlayerController(Instigator.Controller).TransitionFOV(KFPlayerController(Instigator.Controller).DefaultFOV,0.0);
   }
}

simulated function WeaponTick(float dt)
{
   super.WeaponTick(dt);

   if( bAimingRifle && ForceZoomOutTime > 0 && Level.TimeSeconds - ForceZoomOutTime > 0 )
   {
      ForceZoomOutTime = 0;

      ZoomOut(false);

      if( Role < ROLE_Authority)
            ServerZoomOut(false);
   }
}


/**
* Called by the native code when the interpolation of the first person weapon to the zoomed position finishes
*/
simulated event OnZoomInFinished()
{
   local name anim;
   local float frame, rate;

   GetAnimParams(0, anim, frame, rate);

   if (ClientState == WS_ReadyToFire)
   {
      // Play the iron idle anim when we're finished zooming in
      if (anim == IdleAnim)
      {
         PlayIdle();
      }
   }

   if( Level.NetMode != NM_DedicatedServer && KFPlayerController(Instigator.Controller) != none &&
      KFScopeDetail == KF_TextureScope )
   {
      KFPlayerController(Instigator.Controller).TransitionFOV(PlayerIronSightFOV,0.0);
   }
}

simulated function bool CanZoomNow()
{
   Return (!FireMode[0].bIsFiring && Instigator!=None && Instigator.Physics!=PHYS_Falling);
}

simulated event RenderOverlays(Canvas Canvas)
{
   local int m;
   local PlayerController PC;

   if (Instigator == None)
      return;

   // Lets avoid having to do multiple casts every tick - Ramm
   PC = PlayerController(Instigator.Controller);

   if(PC == None)
      return;

   if(!bInitializedScope && PC != none )
   {
         UpdateScopeMode();
   }

   // draw muzzleflashes/smoke for all fire modes so idle state won't
   // cause emitters to just disappear
   Canvas.DrawActor(None, false, true); // amb: Clear the z-buffer here

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      if (FireMode[m] != None)
      {
            FireMode[m].DrawMuzzleFlash(Canvas);
      }
   }


   SetLocation( Instigator.Location + Instigator.CalcDrawOffset(self) );
   SetRotation( Instigator.GetViewRotation() + ZoomRotInterp);

   PreDrawFPWeapon();	// Laurent -- Hook to override things before render (like rotation if using a staticmesh)

   if(bAimingRifle && PC != none && (KFScopeDetail == KF_ModelScope || KFScopeDetail == KF_ModelScopeHigh))
   {
      if (ShouldDrawPortal())
      {
            if ( ScopeScriptedTexture != none )
            {
               Skins[LenseMaterialID] = ScopeScriptedShader;
               ScopeScriptedTexture.Client = Self;   // Need this because this can get corrupted - Ramm
               ScopeScriptedTexture.Revision = (ScopeScriptedTexture.Revision +1);
            }
      }

      bDrawingFirstPerson = true;
      Canvas.DrawBoundActor(self, false, false,DisplayFOV,PC.Rotation,rot(0,0,0),Instigator.CalcZoomedDrawOffset(self));
      bDrawingFirstPerson = false;
   }
   // Added "bInIronViewCheck here. Hopefully it prevents us getting the scope overlay when not zoomed.
   // Its a bit of a band-aid solution, but it will work til we get to the root of the problem - Ramm 08/12/04
   else if( KFScopeDetail == KF_TextureScope && PC.DesiredFOV == PlayerIronSightFOV && bAimingRifle)
   {
      Skins[LenseMaterialID] = ScriptedTextureFallback;

      SetZoomBlendColor(Canvas);

      //Black-out either side of the main zoom circle.
      Canvas.Style = ERenderStyle.STY_Normal;
      Canvas.SetPos(0, 0);
      Canvas.DrawTile(ZoomMat, (Canvas.SizeX - Canvas.SizeY) / 2, Canvas.SizeY, 0.0, 0.0, 8, 8);
      Canvas.SetPos(Canvas.SizeX, 0);
      Canvas.DrawTile(ZoomMat, -(Canvas.SizeX - Canvas.SizeY) / 2, Canvas.SizeY, 0.0, 0.0, 8, 8);

      //The view through the scope itself.
      Canvas.Style = 255;
      Canvas.SetPos((Canvas.SizeX - Canvas.SizeY) / 2,0);
      Canvas.DrawTile(ZoomMat, Canvas.SizeY, Canvas.SizeY, 0.0, 0.0, 512, 512);

      //Draw some useful text.
      Canvas.Font = Canvas.MedFont;
      Canvas.SetDrawColor(200,150,0);

      Canvas.SetPos(Canvas.SizeX * 0.16, Canvas.SizeY * 0.43);
      Canvas.DrawText(" "); //Canvas.DrawText("Zoom: 2.50");

      Canvas.SetPos(Canvas.SizeX * 0.16, Canvas.SizeY * 0.47);
   }
   else
   {
      Skins[LenseMaterialID] = ScriptedTextureFallback;
      bDrawingFirstPerson = true;
      Canvas.DrawActor(self, false, false, DisplayFOV);
      bDrawingFirstPerson = false;
   }
}

simulated function AdjustIngameScope()
{
   local PlayerController PC;

   PC = PlayerController(Instigator.Controller);

   if( !bHasScope )
      return;

   switch (KFScopeDetail)
   {
      case KF_ModelScope:
            if( bAimingRifle )
               DisplayFOV = default.ZoomedDisplayFOV;
            if ( PC.DesiredFOV == PlayerIronSightFOV && bAimingRifle )
            {
               if( Level.NetMode != NM_DedicatedServer && KFPlayerController(Instigator.Controller) != none )
               {
                  KFPlayerController(Instigator.Controller).TransitionFOV(KFPlayerController(Instigator.Controller).DefaultFOV,0.0);
}
            }
            break;

      case KF_TextureScope:
            if( bAimingRifle )
               DisplayFOV = default.ZoomedDisplayFOV;
            if ( bAimingRifle && PC.DesiredFOV != PlayerIronSightFOV )
            {
               if( Level.NetMode != NM_DedicatedServer && KFPlayerController(Instigator.Controller) != none )
               {
                  KFPlayerController(Instigator.Controller).TransitionFOV(PlayerIronSightFOV,0.0);
               }
            }
            break;

      case KF_ModelScopeHigh:
            if( bAimingRifle )
            {
               if( ZoomedDisplayFOVHigh > 0 )
                  DisplayFOV = default.ZoomedDisplayFOVHigh;
               else
                  DisplayFOV = default.ZoomedDisplayFOV;
            }
            if ( bAimingRifle && PC.DesiredFOV == PlayerIronSightFOV )
            {
               if( Level.NetMode != NM_DedicatedServer && KFPlayerController(Instigator.Controller) != none )
               {
                  KFPlayerController(Instigator.Controller).TransitionFOV(KFPlayerController(Instigator.Controller).DefaultFOV,0.0);
               }
            }
            break;
   }

   UpdateScopeMode();
}

simulated event Destroyed()
{
   if (ScopeScriptedTexture != None)
   {
      ScopeScriptedTexture.Client = None;
      Level.ObjectPool.FreeObject(ScopeScriptedTexture);
      ScopeScriptedTexture=None;
   }

   if (ScriptedScopeCombiner != None)
   {
      ScriptedScopeCombiner.Material2 = none;
      Level.ObjectPool.FreeObject(ScriptedScopeCombiner);
      ScriptedScopeCombiner = none;
   }

   if (ScopeScriptedShader != None)
   {
      ScopeScriptedShader.Diffuse = none;
      ScopeScriptedShader.SelfIllumination = none;
      Level.ObjectPool.FreeObject(ScopeScriptedShader);
      ScopeScriptedShader = none;
   }

   Super.Destroyed();
}

simulated function PreTravelCleanUp()
{
   if (ScopeScriptedTexture != None)
   {
      ScopeScriptedTexture.Client = None;
      Level.ObjectPool.FreeObject(ScopeScriptedTexture);
      ScopeScriptedTexture=None;
   }

   if (ScriptedScopeCombiner != None)
   {
      ScriptedScopeCombiner.Material2 = none;
      Level.ObjectPool.FreeObject(ScriptedScopeCombiner);
      ScriptedScopeCombiner = none;
   }

   if (ScopeScriptedShader != None)
   {
      ScopeScriptedShader.Diffuse = none;
      ScopeScriptedShader.SelfIllumination = none;
      Level.ObjectPool.FreeObject(ScopeScriptedShader);
      ScopeScriptedShader = none;
   }
}

//simulated function AltFire(float F){}
// Don't switch fire mode
exec function SwitchModes(){}

// Take into account our modified last fire rate for the last putdown
simulated function bool PutDown()
{
   local int Mode;

   InterruptReload();

   if (bIsReloading)
      return false;

   if( bAimingRifle )
      ZoomOut(False);

   // From Weapon.uc
   if (ClientState == WS_BringUp || ClientState == WS_ReadyToFire)
   {
      if ((Instigator.PendingWeapon != None) && !Instigator.PendingWeapon.bForceSwitch && AmmoAmount(1) > 0 && !ReadyToFire(0))
         DownDelay = FMax(DownDelay, FireMode[1].NextFireTime - Level.TimeSeconds - FireMode[1].FireRate*(1.f - MinReloadPct));

      if (Instigator.IsLocallyControlled())
      {
         for (Mode = 0; Mode < NUM_FIRE_MODES; Mode++)
         {
            // if _RO_
            if (FireMode[Mode] == none)
               continue;
            // End _RO_

            if (FireMode[Mode].bIsFiring)
               ClientStopFire(Mode);
         }

         if (DownDelay <= 0  || KFPawn(Instigator).bIsQuickHealing > 0)
         {
            if (ClientState == WS_BringUp || KFPawn(Instigator).bIsQuickHealing > 0)
               TweenAnim(SelectAnim, PutDownTime);

            else if (HasAnim(PutDownAnim))
            {
               if (ClientGrenadeState == GN_TempDown || KFPawn(Instigator).bIsQuickHealing > 0)
                  PlayAnim(PutDownAnim, PutDownAnimRate * (PutDownTime/QuickPutDownTime), 0.0);

               else
                  PlayAnim(PutDownAnim, PutDownAnimRate, 0.0);
            }
         }
      }

      ClientState = WS_PutDown;

      if (Level.GRI.bFastWeaponSwitching)
         DownDelay = 0;

      if (DownDelay > 0)
         SetTimer(DownDelay, false);

      else
      {
         if( ClientGrenadeState == GN_TempDown )
            SetTimer(QuickPutDownTime, false);

         else
            SetTimer(PutDownTime, false);
      }
   }

   for (Mode = 0; Mode < NUM_FIRE_MODES; Mode++)
   {
      // if _RO_
      if (FireMode[Mode] == none)
         continue;
      // End _RO_

      FireMode[Mode].bServerDelayStartFire = false;
      FireMode[Mode].bServerDelayStopFire  = false;
   }

   Instigator.AmbientSound = None;
   OldWeapon = None;

   return true;
}

// take into account our last fire rate for the reload
simulated function bool AllowReload()
{
   local bool fResult;
   local FHumanPawn FHP;

    FHP = FHumanPawn(Instigator);

    if (FHP != none && FHP.fReloadState != F_Reload_Allowed)
        return false;

   if (Level.TimeSeconds <= FBringUpTime + BringUpTime + FBringUpSafety)
      return false;

   fResult = Super.AllowReload();

   if (!bIsReloading && MagAmmoRemaining < MagCapacity && AmmoAmount(0) > 0 && !fResult && AmmoAmount(1) <= 0 && !ReadyToFire(0))
      return true;

   return fResult;
}

// take into account our last fire rate and block firing while reloading
simulated function bool ReadyToFire(int Mode)
{
   // Don't allow firing while reloading the shell
   if (AmmoAmount(1) > 0 && (((FireMode[1].NextFireTime - FireMode[1].FireRate * 0.06) > Level.TimeSeconds + FireMode[1].PreFireTime) || bIsReloading))
      return False;

   // attempt to allow fire on the last shot
   if (!bIsReloading && MagAmmoRemaining > 0 && Mode == 0 && AmmoAmount(1) <= 0 && M203FireFalk(FireMode[1]) != none &&
      ((FireMode[1].NextFireTime - FireMode[1].FireRate + M203FireFalk(FireMode[1]).FireLastRate) < Level.TimeSeconds + FireMode[1].PreFireTime))
   {
      FireMode[1].bIsFiring = False;
      return True;
   }

   return super.ReadyToFire(Mode);
}

// don't block throw or swap on no ammo and block while waiting for reload
simulated function bool CanThrow()
{
   // attempt to allow throw before its otherwise weird time on the last alt shot
   if ((AmmoAmount(0) == 0 && AmmoAmount(1) == 0) || ReadyToFire(0))
   {
      FireMode[1].bIsFiring = False;
      return True;
   }

   return Super.CanThrow();
}

// removed weird vanilla shit
simulated function FillToInitialAmmo()
{
   if (bNoAmmoInstances)
   {
      if (AmmoClass[0] != None)
         AmmoCharge[0] = AmmoClass[0].Default.InitialAmount;

      if ((AmmoClass[1] != None) && (AmmoClass[0] != AmmoClass[1]))
         AmmoCharge[1] = AmmoClass[1].Default.InitialAmount;
      return;
   }

   if (Ammo[0] != None)
      Ammo[0].AmmoAmount = Ammo[0].Default.InitialAmount;

   if (Ammo[1] != None)
      Ammo[1].AmmoAmount = Ammo[1].Default.InitialAmount;
}

// removed more vanilla shit
function GiveAmmo(int m, WeaponPickup WP, bool bJustSpawned)
{
   local bool bJustSpawnedAmmo;
   local int addAmount, InitialAmount;
   local KFPawn KFP;
   local KFPlayerReplicationInfo KFPRI;

   KFP = KFPawn(Instigator);

   if(KFP != none)
      KFPRI = KFPlayerReplicationInfo(KFP.PlayerReplicationInfo);

   UpdateMagCapacity(Instigator.PlayerReplicationInfo);

   if (FireMode[m] != None && FireMode[m].AmmoClass != None)
   {
      Ammo[m] = Ammunition(Instigator.FindInventoryType(FireMode[m].AmmoClass));
      bJustSpawnedAmmo = false;

      if (bNoAmmoInstances)
      {
         if ((FireMode[m].AmmoClass == None) || ((m != 0) && (FireMode[m].AmmoClass == FireMode[0].AmmoClass)))
            return;

         InitialAmount = FireMode[m].AmmoClass.Default.InitialAmount;

         if(WP!=none && WP.bThrown==true)
            InitialAmount = WP.AmmoAmount[m];
         else
            MagAmmoRemaining = MagCapacity;

         if (Ammo[m] != None)
         {
            addamount = InitialAmount + Ammo[m].AmmoAmount;
            Ammo[m].Destroy();
         }
         else
            addAmount = InitialAmount;

         AddAmmo(addAmount,m);
      }

      else
      {
         if ((Ammo[m] == None) && (FireMode[m].AmmoClass != None))
         {
            Ammo[m] = Spawn(FireMode[m].AmmoClass, Instigator);
            Instigator.AddInventory(Ammo[m]);
            bJustSpawnedAmmo = true;
         }

         else if ((m == 0) || (FireMode[m].AmmoClass != FireMode[0].AmmoClass))
            bJustSpawnedAmmo = (bJustSpawned || ((WP != None) && !WP.bWeaponStay));

         if (WP != none && WP.bThrown == true)
            addAmount = WP.AmmoAmount[m];

         else if (bJustSpawnedAmmo)
         {
            if (default.MagCapacity == 0)
               addAmount = 0;  // prevent division by zero.
            else
               addAmount = Ammo[m].InitialAmount;
         }

         if (WP != none && m > 0 && (FireMode[m].AmmoClass == FireMode[0].AmmoClass))
            return;

         if(KFPRI != none && KFPRI.ClientVeteranSkill != none)
            Ammo[m].MaxAmmo = float(Ammo[m].MaxAmmo) * KFPRI.ClientVeteranSkill.Static.AddExtraAmmoFor(KFPRI, Ammo[m].Class);

         Ammo[m].AddAmmo(addAmount);
         Ammo[m].GotoState('');
      }
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
   local int Mode;

   Instigator = Pawn(Owner);

   bPendingSwitch = bPossiblySwitch;

   if( Instigator == None )
   {
      GotoState('PendingClientWeaponSet');
      return;
   }

   for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
   {
      if( FireModeClass[Mode] != None )
      {
            // laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
               GotoState('PendingClientWeaponSet');
               return;
            }
      }

      FireMode[Mode].Instigator = Instigator;
      FireMode[Mode].Level = Level;
   }

   ClientState = WS_Hidden;
   GotoState('Hidden');

   if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
      return;

   if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
   {
      if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
      else
            BringUp();
      return;
   }

   if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
      return;

   if ( Instigator.Weapon == None)
   {
      Instigator.PendingWeapon = self;
      Instigator.ChangedWeapon();
   }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}


// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

   if (Instigator != None)
      Direction = vector(Instigator.GetViewRotation());

   else if (Pawn(Owner) != none)
      Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      {
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }

   // we failed to drop this weapon on death, award us some cash instead
   else if (Instigator.Health <= 0 && Instigator.PlayerReplicationInfo != none)
   {
      Instigator.PlayerReplicationInfo.Score += SellValue;

      if (FHumanPawn(Instigator) != none)
         FHumanPawn(Instigator).FalkSetDosh();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// setting bringup time
simulated function BringUp(optional Weapon PrevWeapon)
{
   if (Level.NetMode == NM_DedicatedServer)
      FBringUpTime = Level.TimeSeconds;

   Super(KFWeapon).BringUp(PrevWeapon);
}

defaultproperties
{
   ZoomMat=FinalBlend'LR300M203_A.lr300_txr.AimPoint_FinalBlend'
   scopePortalFOVHigh=25.000000
   scopePortalFOV=35.000000
   ScriptedTextureFallback=Texture'LR300M203_A.lr300_txr.Alpha'
   bHasScope=True
   ZoomedDisplayFOVHigh=45.000000
   ForceZoomOutOnAltFireTime=0.400000
   MagCapacity=20
   ReloadRate=3.6
   ReloadShortAnim="TacReload"
   ReloadShortRate=2.68
   bHasSecondaryAmmo=True
   bReduceMagAmmoOnSecondaryFire=False
   ReloadAnim="Reload"
   ReloadAnimRate=1.000000
   WeaponReloadAnim="Reload_M4203"
   HudImage=Texture'LairTextures_T.CustomReskins.LR300Unselect'
   SelectedHudImage=Texture'LairTextures_T.CustomReskins.LR300Select'
   Weight=7.000000
   bHasAimingMode=True
   IdleAimAnim="Idle"
   StandardDisplayFOV=60.000000
   bModeZeroCanDryFire=True
   SleeveNum=7
   TraderInfoTexture=Texture'LairTextures_T.CustomReskins.LR300Trader'
   bIsTier2Weapon=True
   SelectSoundRef="KF_SCARSnd.SCAR_Select"
   PlayerIronSightFOV=50.000000
   ZoomedDisplayFOV=55.000000
   FireModeClass(0)=Class'Lr300M203MedGunFire'
   FireModeClass(1)=Class'Med203Fire'
   PutDownAnim="PutDown"
   SelectForce="SwitchToAssaultRifle"
   AIRating=0.550000
   CurrentRating=0.550000
   bShowChargingBar=True
   Description="The LR300 is an american assault rifle and conversion kit designed and manufactured by Z-M Weapons. This particular model has been equipped with a tube for medical M203 grenades, ideal for healing multiple targets."
   EffectOffset=(X=100.000000,Y=25.000000,Z=-10.000000)
   DisplayFOV=55.000000
   Priority=145
   CustomCrosshair=11
   CustomCrossHairTextureName="Crosshairs.HUD.Crosshair_Cross5"
   InventoryGroup=4
   GroupOffset=8
   PickupClass=Class'Lr300M203MedGunPickup'
   PlayerViewOffset=(X=40.000000,Y=31.000000,Z=-2.000000)
   BobDamping=6.000000
   AttachmentClass=Class'Lr300M203MedGunAttachment'
   IconCoords=(X1=245,Y1=39,X2=329,Y2=79)
   ItemName="Medical LR300+M203 Assault Rifle"
   Mesh=SkeletalMesh'LairAnimations_A.Lr300Mesh'
   MeshRef="LairAnimations_A.Lr300Mesh"
   Skins(0)=Texture'LR300M203_A.lr300_txr.Alpha'
   Skins(1)=Combiner'LR300M203_A.lr300_txr.F4_cmb'
   Skins(2)=Combiner'LR300M203_A.lr300_txr.F2_cmb'
   Skins(3)=Combiner'LR300M203_A.lr300_txr.F5_cmb'
   Skins(4)=Combiner'LR300M203_A.lr300_txr.F3_cmb'
   Skins(5)=Combiner'LR300M203_A.lr300_txr.F1_cmb'
   Skins(6)=Combiner'LR300M203_A.lr300_txr.Gl_nade_cmb'
   Skins(7)=Texture'KF_Weapons_Trip_T.hands.hands_1stP_riot_D'
   SkinRefs(0)="LR300M203_A.lr300_txr.Alpha"
   SkinRefs(1)="LR300M203_A.lr300_txr.F4_cmb"
   SkinRefs(2)="LR300M203_A.lr300_txr.F2_cmb"
   SkinRefs(3)="LR300M203_A.lr300_txr.F5_cmb"
   SkinRefs(4)="LR300M203_A.lr300_txr.F3_cmb"
   SkinRefs(5)="LR300M203_A.lr300_txr.F1_cmb"
   SkinRefs(6)="LR300M203_A.lr300_txr.Gl_nade_cmb"
   SkinRefs(7)="KF_Weapons_Trip_T.hands.hands_1stP_riot_D"
   TransientSoundVolume=1.250000
   fMaxTry=3
   PutDownTime=0.33
   BringUpTime=0.45
   FBringUpSafety=0.1
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
