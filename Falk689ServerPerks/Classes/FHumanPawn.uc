class FHumanPawn extends ServerPerks.SRHumanPawn;

#exec OBJ LOAD FILE=LairSounds_S.uax

enum FReloadStates                           // enum used to prevent weapons reload at the wrong time
{
    F_Reload_Allowed,
    F_Reload_Disallowed,
    F_Reload_Timer
};

var float             fSpeedScaleTimer;      // time between each speed reduction after zedtime
var float             fScaleStep;            // how much we decrease each step
var float             fTargetMeleeIncrease;  // target speed increase, do not edit this
var float             fCTimer;               // current timer, do not edit this
var float             fAcidDamage;           // multiplier for the acid dot damage
var float             fBurnDamage;           // multiplier for the fire dot damage
var float             fHuskDamage;           // multiplier for the husk dot damage
var float             fAcidTime;             // since we're not the only one using timer() we save last acid tick
var float             fBurnTime;             // last time we were on fire
var float             fSingleHealMulti;      // single heal amount multiplier
var float             fWeaponAddTime;        // how much time to wait before giving us our weapons
var float             fCurWeaponAddTime;     // current time before giving us weapons
var float             fAddDoshTimer;         // delay before adding FalkDosh on spawn
var float             fCurAddDoshTimer;      // current add dosh timer
var float             fFlashlightDrain;      // flashlight drain timer in seconds
var float             fCurFlashlightDrain;   // current flashlight drain timer
var class<DamageType> fLastBurnClass;        // last burn class, used to whatever
var int               fKevlarCost;           // how much does a kevlar cost
var int               fMinHealBoost;         // minimum heal amount to boost in solo
var int               fHealthMax;            // yep we've got our HealthMax
var int               fBileCount;            // not sure if some other codes does shit with BileCount so overriding
var int               LastAcidDamage;        // store last acid damage for the tick
var int               fHealth;               // replicated health to use in falk code
var int               fShield;               // replicated shield to use in falk code
var int               fShieldMax;            // maximum shield for this pawn
var int               fPlayerFlares;         // how many flares we've thrown
var int               fTotalFlares;          // how many flares are active in the map
var float             fLastFlareMsg;         // when the last flare message was displayed to the player
var float             fFlareMsgCD;           // flare message cooldown in seconds, used to prevent spam
var byte              patBID;                // last patriarch hit ID
var byte              bloatBID;              // last bloat hit ID
var byte              fInvGroupSwitch;       // what inventory group to attempt to automatically switch to
var bool              fClientInZedTime;      // are we in zed time? Used to tweak commando related zed time skills
var bool              fSStalkerInTeam;       // do we have a breathing stalker view sharer in our current team?
var bool              fShouldTakeDamage;     // should we take damage?
var bool              fShouldAddDosh;        // don't add it twice
var bool              fShouldScale;          // should we scale? do not edit this
var bool              fZTScaling;            // used to force gradual scaling during zed time
var bool              fSwitchToEmpty;        // switch to an empty weapon since we have none with ammo
var bool              fChargingHuskGun;      // are we charging an husk gun shot?
var byte              fSpitfireReload;       // spitfire reload attempts
var byte              fStingerReload;        // stinger reload attempts
var byte              fMercyReload;          // mercy reload attempts
var byte              fWeapSwitch;           // client weapon switch
var byte              fQHWeapSwitch;         // client weapon switch during delayed quick heal
var float             fWeapSwapTick;         // time to check to swap the syringe if another weapon is selected while healing
var float             fCurWeapSwapTick;      // current syringe tick
var bool              fSyringeMain;          // have we used a left click on the syringe?
var float             fSyringeSwapLeft;      // swap time on heal other
var float             fSyringeSwapRight;     // swap time on heal self
var bool              fForceWeapSwap;        // should we just swap the weapon even if not droppable?
var bool              fPendingWeapSwitch;    // there's a pending weapon switch
var bool              fCanSwitchSyringe;     // we are switching back from syringe
var bool              fShouldForceCI;        // should force create inventory
var bool              fForceAmmoCheck;       // force ammo check in allow hold weapon
var float             fCreateInvTick;        // create inventory delay on listen server
var float             fCurCreateInvTick;     // current create inventory delay
var byte              fAddInvPass;           // add default inventory in two different calls to prevent weird shit
var bool              fShouldClearInv;       // used to clear inventory only once
var bool              fIsQuickHealing;       // replicated var to prevent some weird bugs
var float             fIsQHResetTime;        // fIsQuickHealing quick healing reset time
var bool              fPermitWeapSwitch;     // replicated var to prevent random weapon switch at spawn
var bool              fDelayQuickHeal;       // quick heal after reload
var bool              fSwitchingCrazyFive;   // are we switching back to a crazy five weapon?
var float             fCrazyFiveNadeTick;    // how much time to wait before re-equipping a Crazy Five weapon after tossing a nade
var float             fDQuickHealTick;       // delay quick heal tick
var float             fDQuickHealCurTick;    // current delay quick heal tick
var float             fWeapSwitchTime;       // when we switched weapon last time
var float             fWeapSwitchCD;         // time to wait before switching weapon again
var float             fLastNadeTime;         // when the last nade was thrown
var float             fNadeCD;               // nade cooldown
var float             fHealthToGive;         // replicated health to give
var float             fHardRPCostMulti;      // multiplier for random weapons pickups sell value at hard difficulty
var float             fSuicidalRPCostMulti;  // multiplier for random weapons pickups sell value at suicidal difficulty
var float             fHOERPCostMulti;       // multiplier for random weapons pickups sell value at hell on earth difficulty
var float             fBBRPCostMulti;        // multiplier for random weapons pickups sell value at bloodbath difficulty
var int               fStoredSyringeAmmo;    // stored syringe, medic and spark gun ammo, used to fix a weird bug when dropping them
var float             fStoredSyringeTime;    // when we stored syringe ammo, so we don't activate the fix if too much time passed
var float             fHealTick;             // custom heal tick instead of whatever happens in vanilla
var float             fCurHealTick;          // current custom heal tick
var float             fIRCheckTick;          // time between checks if the weapon is reloading to close the inventory
var float             fCurIRCheckTick;       // current reload check tick
var byte              fPerkIndex;            // current perk index so we can drop the right weapon on death
var Weapon            fOldWeapon;            // old weapon, used to attempt to fix stuff with the quick heal not selecting our old weapon back
var Weapon            fReorderOldWeapon;     // old weapon, used on reorder function
var int               fAmmoZAmount;          // ammo amount 0 of the old weapon, used to store ammo during inventory reorder
var int               fAmmoOAmount;          // ammo amount 1 of the old weapon, used to store ammo during inventory reorder
var byte              fTry;                  // How many times we tried to spawn a pickup
var byte              fMaxTry;               // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;           // Z Correction we apply every retry
var vector            fCXRetryLoc;           // X Correction we apply every retry
var vector            fCYRetryLoc;           // Y Correction we apply every retry
var vector            fACZRetryLoc;          // computed Z correction
var vector            fACXRetryLoc;          // computed X correction
var vector            fACYRetryLoc;          // computed Y correction
var int               fSingleAmmoOnPickup;   // how many ammo our single weapon had when we picked up another one from the ground, used for randomly spawned dual weapons
var int               fSingleMagOnPickup;    // how many ammo in mag our single weapon had when we picked up another one from the ground, used for randomly spawned dual weapons
var bool              fJumpDisabled;         // can't jump right now
var class<DamageType> fLastHitDamType;       // used to pass damage types to FGetBodyArmorDamageModifier
var float             fSirenSoundCooldown;   // cooldown used for the siren damage sound
var sound             fSirenDamageSounds[4]; // sounds used for the siren bloodbath skill
var float             fScreamTime;           // last scream time used for the bloodbath skill
var class<Weapon>     BufferedWeaponClass;   // buffered weapon class to switch to
var float             fPickupSoundCooldown;  // pickup sound cooldown in seconds
var float             fNextPickupSound;      // next time a pickup sound is allowed to play
var float             fNadeReloadDelay;      // delay in seconds to allow a weapon reload after tossing a nade
var FReloadStates     fReloadState;          // used to prevent reloading and triggering the delay timer

// crawler fix
struct fCrawlers
{
    var float           hitTime;
    var array<byte>     hitID;
    var FalkMonsterBase M;
};

var transient array<fCrawlers> JCrawlin;      // here is where I keep my crawlers, not only them... actually.
var float                      CrawlerLeapCD; // crawler attack cooldown, used as standard since they were the first one to inspire this fix
var float                      FleshPoundCD;  // fleshpound attack fix time
var float                      BloatVomitCD;  // bloat vomit attack cooldown this time used in conjunction with hitID for the the fix
var float                      PatStandardCD; // patriarch random attack cooldown
var float                      PatRadialCD;   // patriarch radial cooldown

replication
{
    reliable if(Role == ROLE_Authority)
        fShouldScale, fAddInvPass, fTargetMeleeIncrease, fZTScaling, LastAcidDamage, fAcidDamage, fAcidTime, fBurnTime, fLastBurnClass, fBileCount,
        fHealthMax, fShieldMax, fHealth, fShield, fShouldAddDosh, fCurAddDoshTimer, fCurFlashlightDrain, fSpitfireReload, fWeapSwitch, fCurWeapSwapTick,
        fSyringeMain, fCanSwitchSyringe, fPendingWeapSwitch, fPermitWeapSwitch, fDelayQuickHeal, FSwitchToBestWeapon, fHealthToGive, fStingerReload,
        fMercyReload, FalkCloseInventory, fPerkIndex, fQHWeapSwitch, fChargingHuskGun, fPlayerFlares, fTotalFlares, fSStalkerInTeam, FClientClearBuffers,
        FClientSetFIsQuickHealing, FClientZoomOut, fReloadState;

    reliable if(Role < ROLE_Authority)
        fWeapSwitchTime, FalkServerBuyKevlar, FNadeReset, fServerSetReloadState;

    reliable if(Level.NetMode == NM_Client || (Level.NetMode == NM_ListenServer && Role == ROLE_Authority)) // I'm not totally sure that this is working
        fDQuickHealCurTick, fInvGroupSwitch, fForceWeapSwap, fSwitchToEmpty, FServerClearBuffers, fIsQuickHealing;

    reliable if (bNetDirty && Role == Role_Authority && bNetOwner)
        fJumpDisabled;
}

// setup current total flares counter
simulated function PostNetBeginPlay()
{
    local Falk689GameTypeBase Flk;

    Super.PostNetBeginPlay();

    if (Role == ROLE_Authority)
    {
        Flk = Falk689GameTypeBase(Level.Game);

        if (Flk != none)
        {
            fTotalFlares = Flk.fCurrentFlares;

            //warn("fTotalFlares:"@fTotalFlares);
        }
    }
}

// initial pawn setup, mostly husk gun related stuff... lel.
simulated function Setup(xUtil.PlayerRecord rec, optional bool bLoadNow)
{
    local ClientPerkRepLink ST;

    Super.Setup(rec, bLoadNow);

    //log("Falking Pawn");

    // husk gun stuff
    Class'HuskGunAmmo'.Default.InitialAmount                         = 60;
    Class'HuskGunAmmo'.Default.AmmoPickupAmount                      = 30;
    Class'HuskGun'.Default.Priority                                  = 65;
    Class'HuskGun'.Default.Weight                                    = 8;
    Class'HuskGun'.Default.ItemName                                  = "Husk Fireball Launcher";
    Class'HuskGun'.Default.Description                               = "This fireball launcher is a cannon ripped from an unwilling husk. When charged up, it can cause high-degree burns to even the largest specimens, dealing up to three times its initial damage and having two times its original explosion radius. Launches fast-travelling fireballs. Ammo are quite expensive.";
    Class'HuskGun'.Default.FireModeClass[0]                          = Class'HuskGunFireFalk';
    Class'HuskGun'.Default.PickupClass                               = Class'HuskGunPickupFalk';

    Class'BullpupSwitchMessage'.Default.SwitchMessage[0]             = "Set to semi-automatic";
    Class'BullpupSwitchMessage'.Default.SwitchMessage[1]             = "Set to fully automatic";

    // vest stuff
    class'BuyableVest'.default.ItemName                              = "Kevlar";
    class'BuyableVest'.default.CorrespondingPerkIndex                = 8;
    class'BuyableVest'.default.ItemCost                              = fKevlarCost;

    fShouldAddDosh                                                   = True;
    fShouldForceCI                                                   = True;

    // voice packs edits
    if (Level.NetMode != NM_DedicatedServer)
        class'FVoicePacksEdit'.static.DoEdit();

    // find the serverperks replication link and store it
    if (PlayerController(Controller) != none)
        ST = Class'ClientPerkRepLink'.Static.FindStats(PlayerController(Controller));

    if (ST == None)
        warn("Can't connect to ClientPerkRepLink.");
}

// was I already hit by this zed
function bool AlreadyHitByThisZed(FalkMonsterBase M, class<DamageType> damageType, optional int HitIdx)
{
    local int i, x, b;
    local bool cFound, bFound;

    // don't take damage from uninitialized bloat projectiles
    if (class<DamTypeBloatVomitFalk>(damageType) != none && HitIdx == 0)
        return True;

    for (i=0; i<JCrawlin.length; i++)
    {
        if (JCrawlin[i].hitTime < Level.TimeSeconds) // removing useless zeds
        {
            JCrawlin.Remove(i, 1);
            //warn("Removing"@M@"- idx:"@i@"- Time:"@Level.TimeSeconds);

            i--;

            if (JCrawlin.length <= 0)
                break;

            if (i < 0)
                i = 0;
        }

        else if (!bFound && class<DamTypeBloatVomitFalk>(damageType) != none && JCrawlin[i].M == M)
        {
            bFound = True;
            b = i;

            for (x=0; x<jCrawlin[i].hitID.length; x++)
            {
                if (jCrawlin[i].hitID[x] == HitIdx)
                {
                    //warn("HitIdx:"@HitIdx@"found in pos:"@x@"Time:"@Level.TimeSeconds);
                    cFound = True;
                }
            }
        }

        else if (!bFound && !cFound && JCrawlin[i].M == M)
            cFound  = True;
    }

    // we found our zed, return
    if (cFound)
    {
        //warn("Double HitIdx:"@HitIdx@"- Time:"@Level.TimeSeconds);
        return True;
    }

    // just stay on possible indexes
    if (i < 0)
        i = 0;

    // 404 zed not found, add it
    if (!bFound)
    {
        JCrawlin.insert(i, 1);
        JCrawlin[i].M = M;
        b             = i;
    }

    // boss timings
    if (M.default.FIsBoss)
    {
        if (class<DamTypeBossRadialFalk>(damageType) != none)
        {
            //warn("Boss 1");
            JCrawlin[i].hitTime = Level.TimeSeconds + PatRadialCD;
        }

        else
        {
            //warn("Boss 2");
            JCrawlin[i].hitTime = Level.TimeSeconds + PatStandardCD;
        }
    }

    else if (M.FalkGetZedClass() == FALK_Fleshpound)
    {
        //warn("Fleshpound:"@Level.TimeSeconds);
        JCrawlin[i].hitTime = Level.TimeSeconds + FleshPoundCD;
    }

    else if (class<DamTypeBloatVomitFalk>(damageType) != none)
    {
        JCrawlin[b].hitTime = Level.TimeSeconds + BloatVomitCD;

        x = jCrawlin[b].hitID.length;

        JCrawlin[b].hitID.insert(x, 1);
        JCrawlin[b].hitID[x] = HitIdx;

        //warn("Adding bloat vomit idx:"@b@" HitIdx:"@HitIdx@"to list, pos:"@x@"current time:"@Level.TimeSeconds@"RemoveTime:"@JCrawlin[b].hitTime);
    }

    // crawler/standard timings
    else
        JCrawlin[i].hitTime = Level.TimeSeconds + CrawlerLeapCD;

    //warn("No double hit from this monster:"@M@"- HitIdx:"@HitIdx@"- Time:"@Level.TimeSeconds);
    return False;
}

// check if we're immune to all damage
function bool fDamageImmune()
{
    local KFGameType KFG;

    // blocked damage in specific situations
    if (!fShouldTakeDamage)
    {
        //warn("BLOCK"@Level.TimeSeconds);
        return true;
    }

    //log(instigatedBy);
    //log(damageType);

    KFG = KFGameType(Level.Game);

    // no damage during trader time
    if (KFG != None && !KFG.bWaveInProgress)
    {
        return true;
    }

    return false;
}

// Override to prevent healthToGive reduction on damage and to fix multiple hits from flying crawlers
function TakeDamage(int Damage, Pawn instigatedBy, Vector hitlocation, Vector momentum, class<DamageType> damageType, optional int HitIdx)
{
    local KFPlayerReplicationInfo KFPRI;
    local FalkMonsterBase fM;
    local int fRDamage;
    local class<FVeterancyTypes>  fVet;
    local bool fVomitDotImmune;
    local Class<KFWeaponDamageType> KFWDT;
    local int RandSirenIdx;

    //warn("TakeDamage:"@damageType@"-"@Level.TimeSeconds@"-"@Damage);

    KFWDT             = Class<KFWeaponDamageType>(damageType);

    // don't take vomit or explosive damage unless it's from a zed or, welp, yourself.
    if (((KFWDT != none && (KFWDT.default.bIsExplosive || KFWDT.default.bDealBurningDamage)) || class<DamTypeVomit>(damageType) != none) && instigatedBy != Self && KFMonster(instigatedBy) == None)
        return;

    // no more damage from the bile launcher
    if (class<DamTypeBlowerThrower>(damageType) != none)
        return;

    if (fDamageImmune())
        return;

    fM = FalkMonsterBase(instigatedBy);

    // multi hit fix for fleshpound, crawlers and pat
    if (fM != none)
    {
        if (class<DamTypeBossLAWProj>(damageType) != none)
        {
            if (patBID == HitIdx)
                return;

            patBID = HitIdx;
        }

        else if ((fM.FZMultiFix || class<DamTypeHuskFire>(damageType) != none) && class<DamTypeBossChainGunFalk>(damageType) == none && AlreadyHitByThisZed(fM, damageType, HitIdx))
        {
            //warn("DOUBLE:"@Level.TimeSeconds);
            return;
        }

        //else
        //   warn("Multifix was enabled:"@fM.FZMultiFix@"- No Chaingun:"@class<DamTypeBossChainGunFalk>(damageType) == none@"- Time:"@Level.TimeSeconds);
    }

    else if (instigatedBy != self && damageType != class'Fell' && damageType != class'Burned')
    {
        //warn("Blocked unknown damage:"@Level.TimeSeconds);
        return;
    }


    if (fM != none && (fM.fFrozen || fM.fIsStunned || fM.bZapped || fM.fFlinching))
    {
        //warn("fFrozen:"@FalkMonsterBase(InstigatedBy).fFrozen@"- fIsStunned"@FalkMonsterBase(InstigatedBy).fIsStunned);
        return;
    }

    // block damage from dead sirens
    if (class<SirenScreamDamage>(DamageType) != none)
    {
        if (InstigatedBy == none || InstigatedBy.Health <= 0)
        {
            //warn("Block 1");
            return;
        }

        if (KFMonster(InstigatedBy) != none && (KFMonster(InstigatedBy).HeadHealth <= 0 || KFMonster(InstigatedBy).bZapped))
        {
            //warn("Block 2");
            return;
        }
    }

    if (KFMonster(InstigatedBy) != none)
        KFMonster(InstigatedBy).bDamagedAPlayer = true;

    // Don't allow momentum from a player shooting a player
    if(InstigatedBy != none && KFHumanPawn(InstigatedBy) != none)
        Momentum = vect(0,0,0);

    fLastHitDamType = damageType; // hack to have kevlars to modify damage taken based on damtype

    Super(xPawn).TakeDamage(Damage, instigatedBy, hitLocation, momentum, damageType);

    KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

    fRDamage = Damage;

    // Just return if this wouldn't even damage us. Prevents us from catching on fire for high level perks that dont take fire damage
    if(KFPRI != none &&  KFPRI.ClientVeteranSkill != none)
    {
        fRDamage = KFPRI.ClientVeteranSkill.Static.ReduceDamage(KFPRI, self, instigatedBy, Damage, DamageType);

        if (fRDamage <= 0)
            return;
    }

    if ((class<DamTypeBurned>(damageType) != none || class<DamTypeSpitfireFalk>(damageType) != none) &&
            (BurnDown <= 0 || Damage + (Damage * fBurnDamage) > LastBurnDamage))
    {
        if(TeamGame(Level.Game) != None && TeamGame(Level.Game).FriendlyFireScale == 0 && instigatedBy != None && instigatedBy != Self && instigatedBy.GetTeamNum() == GetTeamNum())
            return;

        // Do burn damage if the damage was significant enough
        if (fRDamage > 5)
        {
            if (class<DamTypeHuskFire>(damageType) != none)
                LastBurnDamage = Damage * fHuskDamage;

            else
                LastBurnDamage = Damage * fBurnDamage;

            bBurnified      = true;
            BurnDown        = 4;
            BurnInstigator  = instigatedBy;
            fLastBurnClass  = damageType;
            fBurnTime       = Level.TimeSeconds;
            SetTimer(1.0, false);
        }
    }

    // we took acid damage
    if (class<DamTypeVomit>(damageType) != none)
    {
        // bloat vomit immunity
        if (class<DamTypeBloatVomitFalk>(damageType) != none && KFPRI != none && KFPRI.ClientVeteranSkill != none)
        {
            fVet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

            if (fVet != none && fVet.Static.BloatDotImmunity(KFPRI))
                fVomitDotImmune = True;
        }

        //warn("Vomit: "@class<DamTypeBloatVomitFalk>(damageType) != none);

        // vomit damage behavior
        if (!fVomitDotImmune && (fBileCount <= 0 || Damage * fAcidDamage > LastAcidDamage || class<DamTypeBloatVomitFalk>(damageType) != none))
        {
            fBileCount            = 4;
            BileInstigator        = instigatedBy;
            LastBileDamagedByType = class<DamTypeVomit>(DamageType);

            if (class<DamTypeBloatVomitFalk>(damageType) != none)
                LastAcidDamage        = Damage;// * FGetBloatVomitMulti();

            else
                LastAcidDamage        = Damage * fAcidDamage;

            fAcidTime                = Level.TimeSeconds;

            //warn("Acid Damage Start:"@LastAcidDamage);

            SetTimer(1.0, false); // Sets timer function to be executed each second

            if (Level.Game != none && Controller != none && KFPlayerController(Controller) != none && !KFPlayerController(Controller).bVomittedOn)
            {
                KFPlayerController(Controller).bVomittedOn    = true;
                KFPlayerController(Controller).VomittedOnTime = Level.TimeSeconds;

                if (Controller.TimerRate == 0.0)
                    Controller.SetTimer(10.0, false);
            }
        }
    }

    else if (class<SirenScreamDamage>(DamageType) != none)
    {
        if (Level.Game != none && Controller != none && KFPlayerController(Controller) != none && !KFPlayerController(Controller).bScreamedAt)
        {
            KFPlayerController(Controller).bScreamedAt = true;
            KFPlayerController(Controller).ScreamTime = Level.TimeSeconds;

            if (Controller.TimerRate == 0.0)
                Controller.SetTimer(10.0, false);
        }

        // siren bloodbath skill
        if (Level.Game != none && Level.Game.GameDifficulty >= 8.0 && Controller != none && PlayerController(Controller) != none && fScreamTime + fSirenSoundCooldown < Level.TimeSeconds)
        {
            fScreamTime  = Level.TimeSeconds;
            RandSirenIdx = rand(4);
            PlayerController(Controller).ClientPlaySound(fSirenDamageSounds[RandSirenIdx], true, 1.9, SLOT_Misc);
        }
    }

    LastHitDamType = damageType;
    LastDamagedBy  = instigatedBy;
    fHealth        = Health;

    //Bloody Overlays
    if ((Health-Damage) <= 0.5 * fHealthMax)
        SetOverlayMaterial(InjuredOverlay,0, true);

    if (Role == ROLE_Authority && Level.Game.NumPlayers > 1 && Health < 0.25 * fHealthMax &&
            Level.TimeSeconds - LastDyingMessageTime > DyingMessageDelay && PlayerController(Controller) != none)
    {
        // Tell everyone we're dying
        PlayerController(Controller).Speech('AUTO', 6, "");
        LastDyingMessageTime = Level.TimeSeconds;
    }
}


// override to balance sell prices on the lair
function ServerBuyWeapon(Class<Weapon> WClass, float Weight)
{
    local float Price;
    local int OtherPrice;
    local Inventory I,OI;
    local class<KFWeapon> SecType;
    local KFPlayerReplicationInfo KFPRI;

    if (!CanBuyNow() || Class<KFWeapon>(WClass)==None || Class<KFWeaponPickup>(WClass.Default.PickupClass)==None || HasWeaponClass(WClass))
        Return;

    // Validate if allowed to buy that weapon.
    if(PerkLink == None)
        PerkLink = FindStats();

    if( PerkLink!=None && !PerkLink.CanBuyPickup(Class<KFWeaponPickup>(WClass.Default.PickupClass)) )
        return;

    Price = class<KFWeaponPickup>(WClass.Default.PickupClass).Default.Cost;
    KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

    if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
        Price *= KFPRI.ClientVeteranSkill.static.GetCostScaling(KFPRI, WClass.Default.PickupClass);

    Weight = Class<KFWeapon>(WClass).Default.Weight;

    if (class'DualWeaponsManager'.Static.IsDualWeapon(WClass,SecType))
    {
        if (WClass != class'NineMMPlusDual' && HasWeaponClass(SecType,OI))
        {
            Weight -= SecType.Default.Weight;
            Price *= 0.5f;
            OtherPrice = KFWeapon(OI).SellValue;

            if (OtherPrice == -1)
            {
                OtherPrice = class<KFWeaponPickup>(SecType.Default.PickupClass).Default.Cost * 0.5;

                if (KFPRI.ClientVeteranSkill != none)
                    OtherPrice *= KFPRI.ClientVeteranSkill.static.GetCostScaling(KFPRI, SecType.Default.PickupClass);
            }
        }

        else
            OtherPrice = Price * 0.25f;

    }

    else if( class'DualWeaponsManager'.Static.HasDualies(WClass,Inventory) )
        return;

    Price = int(Price); // Truncuate price.

    if( Weight>0 && !CanCarry(Weight) )
    {
        ClientMessage("Error: "$WClass.Name$" is too heavy ("$CurrentWeight$"+"$Weight$">"$MaxCarryWeight$")");
        return;
    }

    if ( PlayerReplicationInfo.Score<Price )
    {
        ClientMessage("Error: "$WClass.Name$" is too expensive ("$int(Price)$">"$int(PlayerReplicationInfo.Score)$")");
        Return;
    }

    I = Spawn(WClass);

    if ( I != none )
    {
        if ( KFGameType(Level.Game) != none )
            KFGameType(Level.Game).WeaponSpawned(I);

        KFWeapon(I).UpdateMagCapacity(PlayerReplicationInfo);
        KFWeapon(I).FillToInitialAmmo();
        KFWeapon(I).SellValue = Price * 0.5;

        if (OtherPrice > 0)
            KFWeapon(I).SellValue = OtherPrice;

        I.GiveTo(self);

        PlayerReplicationInfo.Score -= Price;

        I.PickupFunction(self);

        FalkSetDosh(); // updating stored dosh

        //ClientForceChangeWeapon(I);
    }

    else ClientMessage("Error: "$WClass.Name$" failed to spawn.");

    FalkSetDosh(); // updating stored dosh

    SetTraderUpdate();
}

// override to update stored dosh on sell
function ServerSellWeapon(Class<Weapon> WClass)
{
    local Inventory I;
    local KFWeapon NewWep;
    local float Price;
    local class<KFWeapon> SecType;

    FalkCloseInventory();

    if ( !CanBuyNow() || Class<KFWeapon>(WClass) == none || Class<KFWeaponPickup>(WClass.Default.PickupClass)==none
            || Class<KFWeapon>(WClass).Default.bKFNeverThrow )
    {
        SetTraderUpdate();
        Return;
    }

    for ( I = Inventory; I != none; I = I.Inventory )
    {
        if (I.Class == WClass)
        {
            if (KFWeapon(I) != none && KFWeapon(I).SellValue != -1)
                Price = KFWeapon(I).SellValue;

            else
            {
                Price = (class<KFWeaponPickup>(WClass.default.PickupClass).default.Cost * 0.5);

                if (KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill != none)
                    Price *= KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill.static.GetCostScaling(KFPlayerReplicationInfo(PlayerReplicationInfo), WClass.Default.PickupClass);
            }

            if (class'DualWeaponsManager'.Static.IsDualWeapon(WClass,SecType))
            {
                NewWep           = Spawn(SecType);

                if (KFWeapon(I).SellValue == -1)
                    Price            = class<KFWeaponPickup>(WClass.default.PickupClass).default.cost * 0.25;

                NewWep.GiveTo(self);
            }

            if (I==Weapon || I==PendingWeapon)
                ClientCurrentWeaponSold();

            PlayerReplicationInfo.Score += int(Price);

            FalkSetDosh(); // updating stored dosh

            I.Destroy();

            SetTraderUpdate();

            if ( KFGameType(Level.Game)!=none )
                KFGameType(Level.Game).WeaponDestroyed(WClass);
            return;
        }
    }
}

// store dosh amount on toss
exec function TossCash(int Amount)
{
    local Vector X,Y,Z;
    local CashPickup CashPickup;
    local Vector TossVel;
    local Actor A;

    // To fix cash tossing exploit.
    if (CashTossTimer < Level.TimeSeconds)
    {
        if(Amount <= 50)
            Amount = 50;

        Controller.PlayerReplicationInfo.Score = int(Controller.PlayerReplicationInfo.Score); // To fix issue with throwing 0 pounds.

        if (Controller.PlayerReplicationInfo.Score <= 0)
            return;

        // close the inv pls
        FalkCloseInventory();

        Amount = Min(Amount, int(Controller.PlayerReplicationInfo.Score));

        GetAxes(Rotation,X,Y,Z);

        TossVel = Vector(GetViewRotation()) * 450.0f + Vect(0, 0, 300);

        CashPickup = Spawn(class'CashPickupFalk',,, Location + 0.8 * CollisionRadius * X - 0.5 * CollisionRadius * Y);

        if(CashPickup != none)
        {
            CashPickup.CashAmount   = Amount;
            CashPickup.bDroppedCash = true;
            CashPickup.RespawnTime  = 0;   // Dropped cash doesnt respawn. For obvious reasons.
            CashPickup.Velocity     = TossVel;
            CashPickup.DroppedBy    = Controller;
            CashPickup.InitDroppedPickupFor(None);
            Controller.PlayerReplicationInfo.Score -= Amount;

            if ( Level.Game.NumPlayers > 1 && Level.TimeSeconds - LastDropCashMessageTime > DropCashMessageDelay )
            {
                PlayerController(Controller).Speech('AUTO', 4, "");
            }

            // Hack to get Slot machines to accept dosh that's thrown inside their collision cylinder.
            ForEach CashPickup.TouchingActors(class 'Actor', A)
            {
                if(A.IsA('KF_Slot_Machine'))
                    A.Touch(Cashpickup);
            }
        }

        CashTossTimer = Level.TimeSeconds + 0.1f;
    }

    FalkSetDosh();
}

// added zerk speed boost in zed time
simulated function Tick(float DeltaTime)
{
    local float                   fB;
    local int                     fDosh;
    local bool                    fStopScaling;
    local string                  SteamID;
    local KF_StoryPRI             PRI;
    local Falk689GameTypeBase     Flk;
    local PlayerController        PC;
    local KFPlayerReplicationInfo KFPRI;
    local class<FVeterancyTypes>  fVet;
    local bool                    fIsZerk;

    // timer to allow weapon reload after a nade
    if (Role == ROLE_Authority && fReloadState == F_Reload_Timer)
    {
        if (fNadeReloadDelay > 0)
            fNadeReloadDelay -= DeltaTime;

        else
        {
            fNadeReloadDelay  = Default.fNadeReloadDelay;
            fReloadState      = F_Reload_Allowed;
        }
    }

    // crazy five switch back after tossing nade
    if (fSwitchingCrazyFive)
    {
        if (fCrazyFiveNadeTick > 0)
            fCrazyFiveNadeTick -= DeltaTime;

        else
        {
            fCrazyFiveNadeTick  = Default.fCrazyFiveNadeTick;
            fSwitchingCrazyFive = False;

            if (Weapon != none && (M79GrenadeLauncherBaseFalk(Weapon) != none || SPGrenadeLauncherBaseFalk(Weapon) != none || M99SniperRifleBaseFalk(Weapon) != none || LAWBaseFalk(Weapon) != none || BoomStickBaseFalk(Weapon) != none || AshotBaseFalk(Weapon) != none))
            {
                //warn("CRAZY FIVE SWITCH DONE:"@Level.TimeSeconds);
                Super.ThrowGrenadeFinished();
            }
        }
    }

    // attempt to reset fIsQuickHealing after a while
    if (Level.NetMode != NM_DedicatedServer && fIsQuickHealing && Level.TimeSeconds > fIsQHResetTime)
    {
        //warn("RESET"@Level.TimeSeconds);
        fIsQuickHealing = False;
    }

    // Delayed weapon switch
    if (fPendingWeapSwitch)
    {
        if (Role == ROLE_Authority || (Syringe(Weapon) != none && fSyringeMain))
        {
            // don't equip stuff before healing if there's a pending quick heal
            if (fDelayQuickHeal)
            {
                //warn("Delay Quick Takes Priority");
                fCanSwitchSyringe  = False;
                fPendingWeapSwitch = False;

                if (fWeapSwitch != 69)
                {
                    fQHWeapSwitch      = fWeapSwitch;
                    //warn("Set3:"@fQHWeapSwitch@"- Time:"@Level.TimeSeconds);
                }

                fWeapSwitch        = 69;
                //warn("fWeapSwitch Set:"@fWeapSwitch@"- Time:"@Level.TimeSeconds);
                fCurWeapSwapTick   = 0;
            }

            // Switch from syringe
            else if (fCanSwitchSyringe)
            {
                if (Syringe(Weapon) == none)
                {
                    fPendingWeapSwitch = False;
                    fCanSwitchSyringe  = False;
                    fWeapSwitch        = 69;
                    //warn("fWeapSwitch Set:"@fWeapSwitch@"- Time:"@Level.TimeSeconds);
                    fCurWeapSwapTick   = 0;
                    bIsQuickHealing    = 0;
                }

                else if (fWeapSwitch != 69)
                {
                    if (fSyringeMain)
                        fWeapSwapTick = fSyringeSwapLeft;

                    else
                        fWeapSwapTick = fSyringeSwapRight;

                    if (fCurWeapSwapTick < fWeapSwapTick)
                        fCurWeapSwapTick += DeltaTime;

                    else
                    {
                        //warn("FPawn SwitchToLastWeapon - Time:"@Level.TimeSeconds);
                        fCurWeapSwapTick = 0;
                        SwitchToLastWeapon();
                    }
                }
            }
        }

        // normal switch
        if (fWeapSwitch != 69)
        {
            if (!fDelayQuickHeal)
            {
                if (fCurWeapSwapTick < Default.fWeapSwapTick)
                    fCurWeapSwapTick += DeltaTime;

                else
                {
                    fSyringeMain     = False;
                    fCurWeapSwapTick = 0;

                    if (Weapon == none || Frag(Weapon) != none || Weapon.CanThrow() || (PipeBombExplosiveBaseFalk(Weapon) != none && PipeBombExplosiveBaseFalk(Weapon).fCanBeSwitched()) || (ATMineExplosiveBaseFalk(Weapon) != none && ATMineExplosiveBaseFalk(Weapon).fCanBeSwitched()))
                    {
                        //warn("FPawn Two SwitchToLastWeapon - Time:"@Level.TimeSeconds);
                        SwitchToLastWeapon();
                        fPendingWeapSwitch = False;
                    }
                }
            }
        }

        // not sure why this should happen but I'll keep it just to be safe
        else
        {
            //warn("Pending but fWeapSwitch == 69");
            fSyringeMain       = False;
            fCurWeapSwapTick   = 0;
            fPendingWeapSwitch = False;
            fQHWeapSwitch      = Default.fQHWeapSwitch;
        }
    }

    if (Level.NetMode == NM_Client || (Level.NetMode == NM_ListenServer && Role == ROLE_Authority))
    {
        // buffered controller GetWeapon
        if (BufferedWeaponClass != none && FCanSwitchWeapon() == 0)
        {
            PC = PlayerController(Controller);

            if (PC != none)
            {
                PC.GetWeapon(BufferedWeaponClass);
                BufferedWeaponClass = none;
            }
        }

        // delay quick heal after reload
        if (fDelayQuickHeal)
        {
            if (fDQuickHealCurTick < fDQuickHealTick)
                fDQuickHealCurTick += DeltaTime;

            else
            {
                fDQuickHealCurTick = 0;

                //warn("f2:"@fHealth + fHealthToGive@"max:"@fHealthMax);

                if (fHealth + fHealthToGive >= fHealthMax)
                    fDelayQuickHeal = False;

                else if (AllowQuickHealing())
                {
                    fDelayQuickHeal = False;
                    //warn("Delayed QuickHeal - Time:"@Level.TimeSeconds);
                    QuickHeal();
                }
            }
        }

        // falk switch to best weapon
        if (fInvGroupSwitch < 69 && fQHWeapSwitch == Default.fQHWeapSwitch)
        {
            //warn("InvGroup:"@fInvGroupSwitch@" - Time:"@Level.TimeSeconds);

            if (fInvGroupSwitch == 5)
                fInvGroupSwitch = 69;

            else
            {
                if (fInvGroupSwitch == 6)
                    fInvGroupSwitch = 4;

                else if (fInvGroupSwitch == 7)
                    fInvGroupSwitch = 1;

                else if (fInvGroupSwitch == 4 || fInvGroupSwitch == 3)
                    fInvGroupSwitch--;

                else
                {
                    KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

                    // if we're a zerk, we'll need an alternative loop
                    if (KFPRI != none)
                    {
                        fVet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

                        if (fVet != none && fVet.Static.IsMeleeBased())
                            fIsZerk = True;
                    }

                    if (fInvGroupSwitch == 2)
                    {
                        if (fIsZerk)
                            fInvGroupSwitch = 5;

                        else
                            fInvGroupSwitch = 1;
                    }

                    else if (fInvGroupSwitch == 1)
                    {
                        if (fIsZerk)
                            fInvGroupSwitch = 4;

                        else
                            fInvGroupSwitch = 5;
                    }
                }

                fForceWeapSwap  = True;
                fWeapSwitchTime = Level.TimeSeconds;
                SwitchWeapon(fInvGroupSwitch);
            }
        }

        else if (fInvGroupSwitch < 69)
        {
            //warn("TickBlock - Time:"@Level.TimeSeconds);
            fInvGroupSwitch = 69;
        }
    }

    // custom heal method, using falk vars and stuff
    if (fHealthToGive > 0 && fHealth > 0)
    {
        if (fCurHealTick < fHealTick)
            fCurHealTick += DeltaTime;

        else if (fHealth < fHealthMax)
        {
            fCurHealTick = 0;

            fHealth       += 1;
            fHealthToGive -= 1;

            if (fHealth > fHealthMax)
                fHealth = fHealthMax;

            Health         = fHealth;
        }

        else
        {
            fCurHealTick  = 0;
            fHealthToGive = 0;
        }
    }

    else if (fHealthToGive < 0)
        fHealthToGive = 0;

    Super(KFPawn).Tick(DeltaTime);

    if (IsLocallyControlled())
    {
        // hide the inventory on reload
        if (fCurIRCheckTick < fIRCheckTick)
            fCurIRCheckTick += DeltaTime;

        else
        {
            fCurIRCheckTick = 0;

            if (KFWeapon(Weapon) != none && KFWeapon(Weapon).bIsReloading && PlayerController(Controller) != none && HUDKillingFloor(PlayerController(Controller).myHud) != none)
                HUDKillingFloor(PlayerController(Controller).myHud).HideInventory();
        }

        if (!bUsingHitBlur && BlurFadeOutTime > 0)
        {
            BlurFadeOutTime	-= DeltaTime;
            DeltaTime = BlurFadeOutTime/StartingBlurFadeOutTime * CurrentBlurIntensity;

            if( BlurFadeOutTime <= 0 )
            {
                BlurFadeOutTime = 0;
                StopHitCamEffects();
            }
            else if( bUseBlurEffect && KFPC!=none && !KFPC.PostFX_IsReady() )
            {
                if( CameraEffectFound != none )
                    UnderWaterBlur(CameraEffectFound).BlurAlpha = Lerp( DeltaTime, 255, UnderWaterBlur(CameraEffectFound).default.BlurAlpha );
                else KFPC.SetBlur(DeltaTime);
            }
        }
    }

    /* Replicated Location stuff - for tracking this pawn's position to display icons when its not relevant */
    PRI = KF_StoryPRI(PlayerReplicationInfo);

    if (PRI != none && PRI.GetFloatingIconMat() != none)
    {
        if (Role == Role_Authority)  // server authoritative
        {
            if (PRI.GetOwnerPawn() != self)
            {
                PRI.SetOwnerPawn(self);
                PRI.NetUpdateTime = Level.TimeSeconds - 1;
            }

            KF_StoryPRI(PlayerReplicationInfo).SetReplicatedPawnLoc(GetHoverIconPosition());
        }

        else if(bDeleteMe || bPendingDelete) // simulated proxy.
        {
            PRI.SetOwnerPawn(none);
            PRI.NetUpdateTime = Level.TimeSeconds - 1;
        }
    }

    if (fShouldScale)
    {
        fCTimer += DeltaTime;

        if (fCTimer >= fSpeedScaleTimer)
        {
            fCTimer = 0.0;
            fStopScaling = False;

            if (fTargetMeleeIncrease < BaseMeleeIncrease)
            {
                fB = BaseMeleeIncrease - fScaleStep;

                if (fB <= fTargetMeleeIncrease)
                {
                    fStopScaling = True;
                    fB = fTargetMeleeIncrease;
                }
            }

            else if (fTargetMeleeIncrease > BaseMeleeIncrease)
            {
                fB = BaseMeleeIncrease + fScaleStep;

                if (fB >= fTargetMeleeIncrease)
                {
                    fStopScaling = True;
                    fB = fTargetMeleeIncrease;
                }
            }

            else
                fShouldScale = False;


            if (fShouldScale)
            {
                SetScaledMeleeSpeedMod(fB);

                if (Role == Role_Authority)
                {
                    fZTScaling = True;
                    ModifyVelocity(DeltaTime, Velocity);

                    if (fStopScaling)
                        fShouldScale = False;
                }
            }
        }
    }

    if (Role == ROLE_Authority)
    {
        if (FPCServ(Controller) != None && FPCServ(Controller).fBuyMenuOpen && !CanBuyNow())
            FPCServ(Controller).fCloseMenu();

        // Spawn default inventory
        if (fShouldForceCI)
        {
            fCurCreateInvTick += DeltaTime;

            if (fCurCreateInvTick > fCreateInvTick)
            {
                fCurCreateInvTick = 0;
                //fShouldAddInv     = True;

                //warn(fAddInvPass);

                if (fAddInvPass < 3)
                {
                    AddDefaultInventory();
                    fAddInvPass++;
                }

                if (fAddInvPass == 3)
                {
                    fAddInvPass++;
                    fAddInvPass    = 69;
                    fCreateInvTick = 0.3;
                }

                else if (fAddInvPass == 69)
                {
                    //warn("SwitchToBest1 - Time"@Level.TimeSeconds);
                    fPermitWeapSwitch = True;
                    FSwitchToBestWeapon();
                    fShouldForceCI = False;
                }
            }
        }

        // flashlight drain math
        if (fCurFlashlightDrain < fFlashlightDrain)
            fCurFlashlightDrain += DeltaTime;

        else
        {
            fCurFlashlightDrain = 0;

            if (KFWeapon(Weapon) != none && KFWeapon(Weapon).FlashLight != none)
            {
                // Increment / Decrement battery life
                if (KFWeapon(Weapon).FlashLight.bHasLight && TorchBatteryLife > 0)
                {
                    Flk = Falk689GameTypeBase(Level.Game);

                    if (Flk != none)
                        TorchBatteryLife -= Flk.GetTorchBatteryDrain(KFWeapon(Weapon));

                    else
                        TorchBatteryLife -= 10;
                }

                else if (!KFWeapon(Weapon).FlashLight.bHasLight && TorchBatteryLife < default.TorchBatteryLife)
                {
                    TorchBatteryLife += 20;

                    if (TorchBatteryLife > default.TorchBatteryLife)
                        TorchBatteryLife = default.TorchBatteryLife;
                }
            }

            else if (TorchBatteryLife < default.TorchBatteryLife)
            {
                TorchBatteryLife += 20;

                if (TorchBatteryLife > default.TorchBatteryLife)
                    TorchBatteryLife = default.TorchBatteryLife;
            }
        }
    }

    // add dosh on delay
    if (Level.NetMode == NM_DedicatedServer && fShouldAddDosh)
    {
        if (fCurAddDoshTimer < fAddDoshTimer)
            fCurAddDoshTimer += DeltaTime;

        else
        {
            fShouldAddDosh   = False;
            fCurAddDoshTimer = 0;
            PC               = PlayerController(Controller);
            Flk              = Falk689GameTypeBase(Level.Game);

            if (PC != none && Flk != none)
            {
                SteamID = PC.GetPlayerIDHash();

                fDosh = Flk.FalkGetDosh(SteamID);

                PC.PlayerReplicationInfo.Score = fDosh;
                PC.PlayerReplicationInfo.Kills = Flk.GetKills(PC);
                Flk.FalkSetDosh(PC, fDosh);
            }
        }
    }
}

// get the next best inventory group
simulated function int FGetBestInvGroup(int fStartGroup, bool fIsZerk)
{
    if (fStartGroup == 6)
        return 4;

    if (fStartGroup == 7)
        return 1;

    if (fStartGroup == 4 || fStartGroup == 3)
        return fStartGroup - 1;

    if (fIsZerk && fStartGroup == 2)
        return 5;

    if (fStartGroup == 2)
        return 1;

    if (fIsZerk && fStartGroup == 1)
        return 4;

    if (fStartGroup == 1)
        return 5;

    return 69;
}

// ask the perk for our new melee increase without changing weapon
function CheckMeleeSpeedMod()
{
    local KFPlayerReplicationInfo KFPRI;

    // Experience Level relate stuff .
    if (Weapon != none && KFWeapon(Weapon).bSpeedMeUp)
    {
        // Adjust Melee weapon speed bonuses depending on perk.
        BaseMeleeIncrease = default.BaseMeleeIncrease;
        KFPRI             = KFPlayerReplicationInfo(PlayerReplicationInfo);

        if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
            BaseMeleeIncrease += KFPRI.ClientVeteranSkill.Static.GetMeleeMovementSpeedModifier(KFPRI);

        InventorySpeedModifier = FMax(default.GroundSpeed * BaseMeleeIncrease, 20);
    }

    else if (Weapon == none || !KfWeapon(Weapon).bSpeedMeUp)
        InventorySpeedModifier = 0;
}


// set melee increase speed without asking the perk (to scale from current to perk)
simulated function SetScaledMeleeSpeedMod(float fBaseInc)
{
    BaseMeleeIncrease = fBaseInc;

    // edit speed
    if (Weapon != none && KFWeapon(Weapon).bSpeedMeUp)
        InventorySpeedModifier = FMax(default.GroundSpeed * BaseMeleeIncrease, 20);

    else
        InventorySpeedModifier = 0;
}


// start melee speed scaling
function StartScaleMeleeSpeedMod()
{
    local KFPlayerReplicationInfo KFPRI;

    fTargetMeleeIncrease = 0.0;

    if (Weapon != none && KFWeapon(Weapon).bSpeedMeUp)
    {
        fShouldScale = True;
        // Adjust Melee weapon speed bonuses depending on perk.
        fTargetMeleeIncrease = default.BaseMeleeIncrease;

        KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

        if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
            fTargetMeleeIncrease += KFPRI.ClientVeteranSkill.Static.GetMeleeMovementSpeedModifier(KFPRI);
    }

    else
    {
        fShouldScale = False;
        fZTScaling   = True;
        CheckMeleeSpeedMod();
        ModifyVelocity(0.01, Velocity);
    }
}


// fixed the zerk-medic switch speed exploit and added max health modifier, disabled more than one swap at a time, added a fix on artillerist percentage weapons, fixed mag sizes on perk switch
function VeterancyChanged()
{
    local FStatsBase ST;
    local ClientPerkRepLink CL;
    local Falk689GameTypeBase Flk;
    local Inventory I;
    local class<FVeterancyTypes> FVet;
    local KFPlayerReplicationInfo KFPRI;
    local float MaxAmmo, CurrentAmmo;

    // edited KFHumanPawn code
    MaxCarryWeight = Default.MaxCarryWeight;

    KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

    if ( KFPRI != none && KFPRI.ClientVeteranSkill != none )
        MaxCarryWeight += KFPRI.ClientVeteranSkill.Static.AddCarryMaxWeight(KFPRI);

    if (CurrentWeight > MaxCarryWeight) // Now carrying too much, drop something.
    {
        for (I = Inventory; I != none; I = I.Inventory)
        {
            if (KFWeapon(I) != none && !KFWeapon(I).bKFNeverThrow)
            {
                I.Velocity = Velocity;
                I.DropFrom(Location + VRand() * 10);

                if (CurrentWeight <= MaxCarryWeight)
                    break; // Drop weapons until player is capable of carrying them all.
            }
        }
    }

    // Make sure nothing is over the Max Ammo amount when changing Veterancy
    for (I = Inventory; I != none; I = I.Inventory)
    {
        if (Ammunition(I) != none)
        {
            MaxAmmo = Ammunition(I).default.MaxAmmo;

            if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
                MaxAmmo = MaxAmmo * KFPRI.ClientVeteranSkill.static.AddExtraAmmoFor(KFPRI, Ammunition(I).Class);

            if (Ammunition(I).AmmoAmount > MaxAmmo)
                Ammunition(I).AmmoAmount = MaxAmmo;

            Ammunition(I).MaxAmmo = MaxAmmo; // hopefully cap maxammo too here
        }
    }
    // KFHumanPawn code end

    // disallow any further veterancy change on dedicated server
    if (Level.NetMode == NM_DedicatedServer)
    {
        Flk = Falk689GameTypeBase(Level.Game);

        if (Flk != None)
            Flk.FalkDisablePerkSelect(PlayerController(Controller));
    }

    // disallow any further veterancy change on listen server
    else if (Level.NetMode == NM_ListenServer && Controller != None && Controller.PlayerReplicationInfo != None)
    {
        CL = ClientPerkRepLink(Controller.PlayerReplicationInfo.CustomReplicationInfo);

        if (CL != None)
        {
            ST = FStatsBase(CL.StatObject);

            if (ST != None)
                FServerPerksMutBase(ST.MutatorOwner).bNoPerkChanges = True;
        }
    }

    if (ROLE == ROLE_Authority)
    {
        KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

        if (KFPRI != none)
        {
            FVet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

            if (FVet != none)
            {
                // typecast the gametype if we haven't already
                if (Flk == none)
                    Flk = Falk689GameTypeBase(Level.Game);


                // simplified implementation in listen server
                if (Level.NetMode == NM_ListenServer)
                {
                    if (Flk != none)
                        fSStalkerInTeam = FVet.Static.ShareStalkerVisibility(KFPRI);
                }

                // we have all we need, tell the gametype if we have the stalker sharing skill
                else if (Flk != none)
                    Flk.StalkerViewShareUpdate(PlayerController(Controller), FVet.Static.ShareStalkerVisibility(KFPRI));
            }
        }
    }

    // tweak max ammo for artillerist percentage weapons
    for (I=Inventory; I!=none; I=I.Inventory)
    {
        if (Mercy(I) != none || StingerBaseFalk(I) != none)
        {
            Weapon(I).GetAmmoCount(MaxAmmo, CurrentAmmo);

            //warn("Current:"@KFWeapon(I).MagAmmoRemaining@"- AmmoAmount:"@CurrentAmmo@"- MaxAmmo:"@MaxAmmo);

            if (KFWeapon(I).MagAmmoRemaining > CurrentAmmo)
                KFWeapon(I).MagAmmoRemaining = CurrentAmmo;
        }
    }

    fShouldScale = False;
    fZTScaling = True;
    CheckMeleeSpeedMod();
    ModifyVelocity(0.01, Velocity);
}

// alternative melee speed modifier and moar stuff
simulated function ChangedWeapon()
{
    local KFPlayerReplicationInfo KFPRI;

    //warn(PendingWeapon);

    // don't equip stuff while spawning the default inventory
    if (fAddInvPass < 69)
    {
        PendingWeapon = none;
        return;
    }

    if (PendingWeapon != none && (AllowHoldWeapon(KFWeapon(PendingWeapon)) || Frag(Weapon) != none))
    {
        // syringe pls
        if (Syringe(PendingWeapon) == none)
        {
            bIsQuickHealing = 0;
            fIsQuickHealing = False;
        }

        Super(KFPawn).ChangedWeapon();

        // Experience Level related stuff
        if (Weapon != none && KFWeapon(Weapon).bSpeedMeUp)
        {
            // Adjust Melee weapon speed bonuses depending on perk
            BaseMeleeIncrease = default.BaseMeleeIncrease;

            KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

            if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
                BaseMeleeIncrease += KFPRI.ClientVeteranSkill.Static.GetMeleeMovementSpeedModifier(KFPRI);

            InventorySpeedModifier = FMax(default.GroundSpeed * BaseMeleeIncrease, 20);
        }

        else if (Weapon == none || !KFWeapon(Weapon).bSpeedMeUp )
            InventorySpeedModifier = 0;
    }

    else
        PendingWeapon = none;
}

// alternative server-side melee speed modifier
function ServerChangedWeapon(Weapon OldWeapon, Weapon NewWeapon)
{
    local KFPlayerReplicationInfo KFPRI;
    local Falk689GameTypeBase Flk;
    local PlayerController PC;

    //log(NewWeapon);

    super(KFPawn).ServerChangedWeapon(OldWeapon,NewWeapon);

    // Experience Level relate stuff
    if(Weapon != none && KFWeapon(Weapon).bSpeedMeUp)
    {
        // Adjust Melee weapon speed bonuses depending on perk
        BaseMeleeIncrease = default.BaseMeleeIncrease;

        KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

        if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
            BaseMeleeIncrease += KFPRI.ClientVeteranSkill.Static.GetMeleeMovementSpeedModifier(KFPRI);

        InventorySpeedModifier = FMax(default.GroundSpeed * BaseMeleeIncrease, 20);
    }

    else if (Weapon == none || !KfWeapon(Weapon).bSpeedMeUp)
        InventorySpeedModifier = 0;

    // artillerist camp timer optimization project
    if (Weapon != None && M60BaseFalk(Weapon) == none)
    {
        Flk = Falk689GameTypeBase(Level.Game);
        PC  = PlayerController(Controller);

        if (Flk != none && PC != none)
        {
            Flk.SetCamping(PC, False);

            //log("Weapon Switch, not camping");
        }
    }
}


// only scale velocity directly when not in zedtime, health modifier
simulated event ModifyVelocity(float DeltaTime, vector OldVelocity)
{
    local float WeightMod;
    local KFPlayerReplicationInfo KFPRI;
    local class<FVeterancyTypes> fVet;
    local int tHP, tSH;
    local Inventory I;

    KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

    if (!fShouldScale || fZTScaling)
    {
        fZTScaling = False;

        if (Role == ROLE_Authority && bMovementDisabled && Level.TimeSeconds > StopDisabledTime)
            EnableMovement();

        if (bMovementDisabled && Physics == PHYS_Walking)
        {
            Velocity.X = 0;
            Velocity.Y = 0;
            Velocity.Z = 0;
        }

        if (KFGameReplicationInfo(Level.GRI).BaseDifficulty >= 5 && bMovementDisabled && Velocity.Z > 0)
            Velocity.Z = 0;

        if (Controller != none)
        {
            WeightMod = CurrentWeight * default.WeightSpeedModifier;

            GroundSpeed = default.GroundSpeed - (default.GroundSpeed * WeightMod);
            GroundSpeed += InventorySpeedModifier;

            if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
                GroundSpeed *= KFPRI.ClientVeteranSkill.static.GetMovementSpeedModifier(KFPRI, KFGameReplicationInfo(Level.GRI));
        }
    }

    // carry weight and max health perk modifier
    if (!fZTScaling && KFPRI != none)
    {
        fVet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

        if (fVet != none)
        {
            tHP        = fHealthMax;
            tSH        = fShieldMax;
            fHealthMax = fVet.static.fGetMaxHealth(KFPRI);
            fShieldMax = fVet.static.fGetMaxShield(KFPRI);
            HealthMax  = fHealthMax;
            fPerkIndex = fVet.Default.PerkIndex;

            MaxCarryWeight = Default.MaxCarryWeight + KFPRI.ClientVeteranSkill.Static.AddCarryMaxWeight(KFPRI);

            if (tHP != fHealthMax)
            {
                //log("Changed HealthMax: "@fHealthMax);
                Health = max(int(float(Health) / float(tHP) * float(fHealthMax)), 1);
            }

            if (tSH != fShieldMax)
            {
                //log("Changed ShieldMax: "@fShieldMax);
                fShield = max(int(fShield / float(tSH) * fShieldMax), 1);

            }

            fHealth        = Health;
            ShieldStrength = fShield;

            if (CurrentWeight > MaxCarryWeight) // Now carrying too much, drop something.
            {
                for (I=Inventory; I!=none; I=I.Inventory)
                {
                    if (KFWeapon(I) != none && !KFWeapon(I).bKFNeverThrow)
                    {
                        I.Velocity = Velocity;
                        I.DropFrom(Location + VRand() * 10);

                        if (CurrentWeight <= MaxCarryWeight)
                            break; // Drop weapons until player is capable of carrying them all.
                    }
                }
            }
        }
    }
}

// added battery drain modifiers and alternative implementation of burn and acid damage
function Timer()
{
    local Falk689GameTypeBase Flk;

    Flk = Falk689GameTypeBase(Level.Game);

    if (BurnDown > 0)
    {
        if (fBurnTime + 1 <= Level.TimeSeconds)
        {
            TakeFireDamage(LastBurnDamage, LastDamagedBy);
            SetTimer(1.0, false); // Sets timer function to be executed each second
        }
    }

    else
    {
        BurnDown   = 0;
        LastBurnDamage = 0;
        bBurnified = false;
        RemoveFlamingEffects();
        StopBurnFX();
    }

    if (fAcidTime + 1 <= Level.TimeSeconds && fBileCount > 0)
    {
        /*log("fAcidTime: "@fAcidTime);
          log("NextTime: "@fAcidTime+1);
          log("CurrentTime: "@Level.TimeSeconds);*/

        FalkTakeBileDamage();

        SetTimer(1.0, false); // Sets timer function to be executed each second
    }

    if (Controller != none)
    {
        if(KFPC != none )
        {
            bOnDrugs = false;
            // Update for the scoreboards.
            if (Health <= 0)
            {
                PlaySound(MiscSound,SLOT_Talk);
                return;
            }

            if (Health < fHealthMax * 0.25)
                PlaySound(BreathingSound, SLOT_Talk, ((50-Health)/5)*TransientSoundVolume,,TransientSoundRadius,, false);

            // Accuracy vs. Movement tweakage!  - Alex
            if (KFWeapon(Weapon) != none)
                KFWeapon(Weapon).AccuracyUpdate(vsize(Velocity));
        }
    }

    if (Weapon != none)
    {
        if (WeaponAttachment(Weapon.ThirdPersonActor) == none && VSize(Velocity) <= 0)
            IdleWeaponAnim = IdleRestAnim;
    }

    else
        IdleWeaponAnim = IdleRestAnim;
}

// alternative fire damage
function TakeFireDamage(int Damage, pawn Instigator)
{
    local Vector DummyHitLoc, DummyMomentum;

    if (fDamageImmune())
    {
        BurnDown = 0;
        return;
    }

    Super(xPawn).TakeDamage(Damage, BurnInstigator, DummyHitLoc, DummyMomentum, fLastBurnClass);
    fBurnTime = Level.TimeSeconds;

    if (BurnDown > 0)
        BurnDown--;
}

// huge nope on this
function TakeBileDamage()
{
}

// alternative acid damge
function FalkTakeBileDamage()
{
    local vector BileVect;
    //local int RandBileDamage;
    local int actualDamage;
    local vector HitMomentum;

    if (fDamageImmune())
    {
        fBileCount = 0;
        return;
    }

    if (fAcidTime + 1 > Level.TimeSeconds)
        return;

    //log("TakeBileDamage:"@LastAcidDamage);

    fLastHitDamType = LastBileDamagedByType;

    Super(xPawn).TakeDamage(LastAcidDamage, BileInstigator, Location, vect(0,0,0), LastBileDamagedByType);
    fAcidTime = Level.TimeSeconds;

    if (fBileCount > 0)
        fBileCount--;

    HitMomentum = vect(0,0,0);
    actualDamage = Level.Game.ReduceDamage(LastAcidDamage, self, BileInstigator, Location, HitMomentum, LastBileDamagedByType);

    if (actualDamage <= 0)
        return;

    if (Controller == none || PlayerController(Controller) == None)
        return;

    BileVect.X = frand();
    BileVect.Y = frand();
    BileVect.Z = frand();

    if (class<DamTypeBileDeckGun>(LastBileDamagedByType) != none)
        DoHitCamEffects(BileVect, 0.25, 0.75, 0.5);

    else
        DoHitCamEffects( BileVect, 0.35, 2.0,1.0 );
}

// our shield absorbs all the damage
function int ShieldAbsorb(int dam)
{
    local float damage;
    local KFPlayerReplicationInfo KFPRI;

    damage = dam;

    if (fShield <= 0)
        return damage;

    KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

    if (KFPRI != none && KFPRI.ClientVeteranSkill != none && class<FVeterancyTypes>(KFPRI.ClientVeteranSkill) != none)
        damage *= class<FVeterancyTypes>(KFPRI.ClientVeteranSkill).static.FGetBodyArmorDamageModifier(KFPRI, fLastHitDamType);

    else if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
        damage *= KFPRI.ClientVeteranSkill.static.GetBodyArmorDamageModifier(KFPRI);

    if (damage < fShield)
    {
        fShield       -= damage;
        ShieldStrength = fShield;
        return 0;
    }

    damage         = Max(dam - fShield, 0);
    fShield        = 0;
    ShieldStrength = 0;

    return damage;
}


// override to store dosh on buy ammo and to fix issues with ammo prices
function bool ServerBuyAmmo(Class<Ammunition> AClass, bool bOnlyClip)
{
    local Inventory I;
    local float Price;
    local float fAmmoCost;
    local Ammunition AM;
    local KFWeapon KW;
    local int c;
    local float UsedMagCapacity;
    //local Boomstick DBShotty;
    //local M99SniperRifleBaseFalk M99Falk;
    local CrossbuzzsawBaseFalk CBFalk;
    local SpitfireBaseFalk SFFalk;
    local StingerBaseFalk STFalk;
    local Mercy MRFalk;

    if (!CanBuyNow() || AClass == None)
    {
        SetTraderUpdate();
        return false;
    }

    for (I = Inventory; I != none; I = I.Inventory)
    {
        if (I.Class == AClass)
            AM = Ammunition(I);

        else if (KW == None && KFWeapon(I) != None && (Weapon(I).AmmoClass[0] == AClass || Weapon(I).AmmoClass[1] == AClass))
            KW = KFWeapon(I);
    }

    if (KW == none || AM == none)
    {
        SetTraderUpdate();
        return false;
    }

    //DBShotty = Boomstick(KW);
    //M99Falk  = M99SniperRifleBaseFalk(KW);
    CBFalk   = CrossbuzzsawBaseFalk(KW);
    SFFalk   = SpitfireBaseFalk(KW);
    STFalk   = StingerBaseFalk(KW);
    MRFalk   = Mercy(KW);

    AM.MaxAmmo = AM.default.MaxAmmo;

    if (KFPlayerReplicationInfo(PlayerReplicationInfo) != none && KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill != none)
        AM.MaxAmmo = int(float(AM.MaxAmmo) * KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill.static.AddExtraAmmoFor(KFPlayerReplicationInfo(PlayerReplicationInfo), AClass));

    if (AM.AmmoAmount >= AM.MaxAmmo)
    {
        SetTraderUpdate();
        return false;
    }

    fAmmoCost = class<KFWeaponPickup>(KW.PickupClass).default.AmmoCost * KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill.static.GetAmmoCostScaling(KFPlayerReplicationInfo(PlayerReplicationInfo), KW.PickupClass); // Clip price.

    if (KW.bHasSecondaryAmmo && AClass == KW.FireModeClass[1].default.AmmoClass)
    {
        UsedMagCapacity = 1;
        c               = 1;
        fAmmoCost       = 10 * KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill.static.GetAmmoCostScaling(KFPlayerReplicationInfo(PlayerReplicationInfo), KW.PickupClass); // Clip price, hardcoded to 10
    }

    else
        UsedMagCapacity = class<KFWeaponPickup>(KW.PickupClass).default.BuyClipSize;

    if (bOnlyClip)
    {
        if (KFPlayerReplicationInfo(PlayerReplicationInfo) != none && KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill != none)
        {
            if (class<HuskGunPickup>(KW.PickupClass) != none)
                c = UsedMagCapacity * KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill.static.AddExtraAmmoFor(KFPlayerReplicationInfo(PlayerReplicationInfo), AM.Class);

            else if (!KW.bHasSecondaryAmmo || AClass == KW.FireModeClass[0].default.AmmoClass)
                c = UsedMagCapacity * KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill.static.GetMagCapacityMod(KFPlayerReplicationInfo(PlayerReplicationInfo), KW);
        }

        else
            c = UsedMagCapacity;

        if (AM.AmmoAmount + c > AM.MaxAmmo) // are we trying to buy too much ammo?
            c = AM.MaxAmmo - AM.AmmoAmount;
    }

    else
        c = AM.MaxAmmo - AM.AmmoAmount;

    Price = int(float(c) / UsedMagCapacity * fAmmoCost);

    if (PlayerReplicationInfo.Score < Price) // Not enough CASH (so buy the amount you CAN buy).
    {
        //warn("Price:"@Price@"Score:"@PlayerReplicationInfo.Score);

        c *= PlayerReplicationInfo.Score / Price;

        if (c == 0)
        {
            SetTraderUpdate();
            return false; // Couldn't even afford 1 bullet.
        }

        //warn("Amount:"@c@"AmmoCost:"@fAmmoCost@"MagCapacity"@UsedMagCapacity);

        Price = fAmmoCost / UsedMagCapacity * float(c);

        //warn("Price After:"@Price);

        if (PipeBombAmmoBaseFalk(AM) != none && !PipeBombAmmoBaseFalk(AM).FShouldAddAmmo(c))
        {
            SetTraderUpdate();
            return false;
        }

        if (ATMineAmmoBaseFalk(AM) != none && !ATMineAmmoBaseFalk(AM).FShouldAddAmmo(c))
        {
            SetTraderUpdate();
            return false;
        }

        AM.AddAmmo(c);

        /*if (DBShotty != none)
          DBShotty.AmmoPickedUp();*/

        /*if (M99Falk != none)
          M99Falk.AmmoPickedUp();*/

        if (CBFalk != none)
            CBFalk.AmmoPickedUp();

        if (SFFalk != none)
            fSpitfireReload = Default.fSpitfireReload;

        if (STFalk != none)
            fStingerReload = Default.fStingerReload;

        if (MRFalk != none)
            fMercyReload = Default.fMercyReload;

        PlayerReplicationInfo.Score = Max(PlayerReplicationInfo.Score - Price, 0);

        //warn("Score After:"@PlayerReplicationInfo.Score);

        FalkSetDosh(); // updating stored dosh

        SetTraderUpdate();
        return false;
    }

    if (PipeBombAmmoBaseFalk(AM) != none && !PipeBombAmmoBaseFalk(AM).FShouldAddAmmo(c))
    {
        SetTraderUpdate();
        return false;
    }

    if (ATMineAmmoBaseFalk(AM) != none && !ATMineAmmoBaseFalk(AM).FShouldAddAmmo(c))
    {
        SetTraderUpdate();
        return false;
    }

    PlayerReplicationInfo.Score = int(PlayerReplicationInfo.Score-Price);
    AM.AddAmmo(c);

    /*if (DBShotty != none)
      DBShotty.AmmoPickedUp();*/

    /*if (M99Falk != none)
      M99Falk.AmmoPickedUp();*/

    if (CBFalk != none)
        CBFalk.AmmoPickedUp();

    if (SFFalk != none)
        fSpitfireReload = Default.fSpitfireReload;

    if (STFalk != none)
        fStingerReload = Default.fStingerReload;

    if (MRFalk != none)
        fMercyReload = Default.fMercyReload;

    SetTraderUpdate();
    return true;
}

// buy partial kevlar
function FalkServerBuyKevlar(class<FVestPickup> FArmorClass)
{
    local float Cost;
    local KFPlayerReplicationInfo KFPRI;

    KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

    if (KFPRI == none)
        return;

    Cost = FArmorClass.Default.Cost;

    if (KFPRI.ClientVeteranSkill != none)
        Cost *= KFPRI.ClientVeteranSkill.static.GetCostScaling(KFPRI, class'Vest');

    if (!CanBuyNow() || fShield == fShieldMax)
    {
        SetTraderUpdate();
        return;
    }

    if (PlayerReplicationInfo.Score >= Cost)
    {
        PlayerReplicationInfo.Score -= Cost;
        fShield        = Min(fShield + FArmorClass.Default.ShieldCapacity, fShieldMax);
        ShieldStrength = fShield;
    }

    SetTraderUpdate();
    FalkSetDosh();
}

// custom kevlar buy function, we can buy parts of armor
function ServerBuyKevlar()
{
    local float Cost;
    local int UnitsAffordable;
    local KFPlayerReplicationInfo KFPRI;

    KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

    if (KFPRI == none)
        return;

    Cost = fKevlarCost * ((float(fShieldMax) - float(fShield)) / float(fShieldMax));

    if (KFPRI.ClientVeteranSkill != none)
        Cost *= KFPRI.ClientVeteranSkill.static.GetCostScaling(KFPRI, class'Vest');

    if (!CanBuyNow() || fShield == fShieldMax)
    {
        SetTraderUpdate();
        return;
    }

    if (PlayerReplicationInfo.Score >= Cost)
    {
        PlayerReplicationInfo.Score -= Cost;
        fShield = fShieldMax;
    }

    else
    {
        Cost = fKevlarCost;

        if (KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill != none)
            Cost *= KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill.static.GetCostScaling(KFPlayerReplicationInfo(PlayerReplicationInfo), class'Vest');

        Cost /= fShieldMax;

        UnitsAffordable = int(PlayerReplicationInfo.Score / Cost);

        PlayerReplicationInfo.Score -= int(Cost * UnitsAffordable);

        fShield += UnitsAffordable;
    }

    ShieldStrength = fShield;
    SetTraderUpdate();
    FalkSetDosh();
}


// update stored dosh in the gametype list
function FalkSetDosh()
{
    local PlayerController PC;
    local Falk689GameTypeBase Flk;

    PC  = PlayerController(Controller);
    Flk = Falk689GameTypeBase(Level.Game);

    if (Flk != none && PC != none)
        Flk.FalkSetDosh(PC, PlayerReplicationInfo.Score);
}

// set achievement unlock state
simulated function SetUnlocked(byte idx, bool uStatus)
{
    local Falk689GameTypeBase Flk;
    local KFPlayerReplicationInfo KFPRI;

    Flk   = Falk689GameTypeBase(Level.Game);
    KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

    Flk.SetUnlocked(PlayerController(KFPRI.Owner), idx, uStatus);
}

// fixed initial weapons prices and inventory stuff
function CreateInventory(string InventoryClassName)
{
    local Inventory Inv;
    local class<Inventory> InventoryClass;
    local float Price;
    local KFWeapon KFW;

    if (Level == None || Level.Game == None)
        return;

    // close the focking inventory
    FalkCloseInventory();

    InventoryClass = Level.Game.BaseMutator.GetInventoryClass(InventoryClassName);

    if (InventoryClass != None && FindInventoryType(InventoryClass) == None)
    {
        Inv = Spawn(InventoryClass);

        if (Inv != None)
        {
            Inv.GiveTo(self);

            if (Inv != None)
                Inv.PickupFunction(self);

            KFW = KFWeapon(Inv);

            if ((KFW != none)                                                &&
                    (InventoryClassName == "Falk689WeaponsFix.KnifeFalk"       ||
                     InventoryClassName == "Falk689WeaponsFix.SyringeFalk"     ||
                     InventoryClassName == "Falk689WeaponsFix.ArmorWelderFalk" ||
                     InventoryClassName == "Falk689WeaponsFix.SingleFalk"))
            {
                Price = class<KFWeaponPickup>(KFW.PickupClass).Default.Cost * 0.5;
                KFW.SellValue = Price;
            }

            // equip some weapon if none or we're not still adding default inventory
            if (Weapon == none && KFW != none && fAddInvPass == 69)
            {
                //warn("CreateInventory Equip");
                PendingWeapon   = KFW;
                ChangedWeapon();
            }
        }
    }
}

// added selectable max shield
function bool AddShieldStrength(int ShieldAmount)
{
    if (fShield >= fShieldMax)
    {
        fShield        = fShieldMax;
        ShieldStrength = fShield;
        return false;
    }

    fShield += ShieldAmount;

    if (fShield > fShieldMax)
        fShield = fShieldMax;

    ShieldStrength = fShield;
    return true;
}

// allow heal only if we're falkish and not healing yet
function bool AllowQuickHealing()
{
    //warn("Allow: "@fHealthMax@" HP: "@fHealth);

    // this should fix some weird bug with the syringe
    if (fHealth + fHealthToGive >= fHealthMax || (Weapon != none && Syringe(Weapon) != none && fIsQuickHealing))
    {
        return False;
    }

    // don't queue quick heal if we're charging an husk gun
    if (fChargingHuskGun && Huskgun(Weapon) != none)
    {
        if (!Weapon.CanThrow())
        {
            return False;
        }

        else
            fChargingHuskGun = false;
    }

    if (Level.TimeSeconds <= fWeapSwitchTime + fWeapSwitchCD)
    {
        fWeapSwitch         = 69;
        fCurWeapSwapTick    = 0;
        fPendingWeapSwitch  = False;
        FClientClearBuffers();
        return true;
    }

    if ((Weapon != none && Frag(Weapon) == none)                                                                                           &&
            ((PipeBombExplosiveBaseFalk(Weapon) != none && !PipeBombExplosiveBaseFalk(Weapon).fCanBeSwitched())                              ||
             (ATMineExplosiveBaseFalk(Weapon) != none   && !ATMineExplosiveBaseFalk(Weapon).fCanBeSwitched())                                ||
             (PipeBombExplosive(Weapon) == none && ATMineExplosive(Weapon) == none && !Weapon.CanThrow())                                    ||
             (KFWeapon(Weapon) != none && KFWeapon(Weapon).bIsReloading)))
    {
        // don't switch to other weapons before healing
        if (fWeapSwitch != 69)
        {
            fQHWeapSwitch = fWeapSwitch;
        }

        fWeapSwitch         = 69;
        fCurWeapSwapTick    = 0;
        fPendingWeapSwitch  = False;

        fDelayQuickHeal     = True;
        BufferedWeaponClass = none;
        FClientClearBuffers();
        return False;
    }

    return True;
}

// set the variable from the server
simulated function FClientSetFIsQuickHealing(bool fNewState)
{
    //warn("SETTING FROM SERVER"@fNewState@"- time"@Level.TimeSeconds);
    fIsQuickHealing = fNewState;
}

// heal only if we're not feeling that well
simulated exec function QuickHeal()
{
    local Syringe S;
    local Inventory I;
    local byte C;

    if (!AllowQuickHealing())
    {
        //warn("!Allow:"@Level.TimeSeconds);
        return;
    }

    if (Health >= HealthMax)
    {
        //warn("!Health:"@Level.TimeSeconds);
        return;
    }

    //warn("Quick Heal:"@Level.TimeSeconds);

    for (I=Inventory; (I!=None && C++<250); I=I.Inventory)
    {
        S = Syringe(I);

        if (S != None)
            break;
    }

    if (S == none)
    {
        // try to switch to this weapon regardless of the pending quick heal
        if (fQHWeapSwitch != Default.fQHWeapSwitch)
        {
            //warn("Get:"@fQHWeapSwitch@" - Time:"@Level.TimeSeconds);
            fCurWeapSwapTick      = 0;
            fSyringeMain          = False;
            fDelayQuickHeal       = False;
            fPendingWeapSwitch    = True;
            bIsQuickHealing       = 0;
            fIsQuickHealing       = False;
            fWeapSwitch           = fQHWeapSwitch;
            //warn("fWeapSwitch Set:"@fWeapSwitch@"- Time:"@Level.TimeSeconds);
        }

        //warn("!Weapon:"@Level.TimeSeconds);
        return;
    }

    if (S.ChargeBar() < 0.95)
    {
        //warn("SHOW AMMO BOX:"@Level.TimeSeconds);

        if (PlayerController(Controller) != none && HUDKillingFloor(PlayerController(Controller).myHud) != none)
        {
            HUDKillingFloor(PlayerController(Controller).myHud).ShowQuickSyringe();
        }

        // try to switch to this weapon regardless of the pending quick heal
        if (fQHWeapSwitch != Default.fQHWeapSwitch)
        {
            //warn("Get2:"@fQHWeapSwitch@" - Time:"@Level.TimeSeconds);
            fCurWeapSwapTick      = 0;
            fSyringeMain          = False;
            fDelayQuickHeal       = False;
            fPendingWeapSwitch    = True;
            fWeapSwitch           = fQHWeapSwitch;
            bIsQuickHealing       = 0;
            fIsQuickHealing       = False;
            //warn("fWeapSwitch Set2:"@fWeapSwitch@"- Time:"@Level.TimeSeconds);
        }

        return; // No can heal. - Wut moit, u sirios
    }

    if (Weapon != none)
        fOldWeapon = Weapon;

    // how about we close the fockin inventory regardless so we don't even have to test all the bugs
    FalkCloseInventory();

    //warn("Start Actual Heal - Time:"@Level.TimeSeconds);
    fCrazyFiveNadeTick  = Default.fCrazyFiveNadeTick;
    fSwitchingCrazyFive = False;
    bIsQuickHealing     = 1;
    fCurWeapSwapTick    = 0;
    fIsQuickHealing     = True;
    fSyringeMain        = False;

    if (Weapon == None)
    {
        fWeapSwitchTime = Level.TimeSeconds;
        PendingWeapon   = S;
        ChangedWeapon();
    }

    else if (Weapon != S)
    {
        fWeapSwitchTime   = Level.TimeSeconds;
        PendingWeapon     = S;
        Weapon.PutDown();
    }

    else // Syringe already selected, just start healing.
    {
        bIsQuickHealing = 0;
        S.HackClientStartFire();
    }
}

// falkified function, don't reduce burn, do only useful stuff
function bool GiveHealth(int HealAmount, int HealMax)
{
    local KFPlayerReplicationInfo KFPRI;
    local Falk689GameTypeBase Flk;
    local class<FVeterancyTypes> fVet;

    Flk = Falk689GameTypeBase(Level.Game);

    // boost syringe when we're alone
    if (HealMax != 689 && Flk != none && Flk.FWavePlayers == 1)
    {
        KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

        if (KFPRI != none)
        {
            fVet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

            if (HealAmount >= fMinHealBoost * fVet.static.GetHealPotency(KFPRI))
                HealAmount *= fSingleHealMulti;
        }
    }

    if (fHealth < fHealthMax)
    {
        fHealthToGive  += HealAmount;
        lastHealTime    = level.timeSeconds;
        return True;
    }

    return False;
}

// update fHealth on heal
simulated function AddHealth()
{
    /*local int tempHeal;

      if ((level.TimeSeconds - lastHealTime) >= 0.1)
      {
      if (fHealth < fHealthMax)
      {
      tempHeal = int(10 * (level.TimeSeconds - lastHealTime));

      if(tempHeal > 0)
      lastHealTime = level.TimeSeconds;

      Health          = Min(Health + tempHeal, HealthMax);
      fHealthToGive  -= tempHeal;
      }

      else
      {
      lastHealTime  = level.timeSeconds;
      fHealthToGive = 0 ;
      }
      }

      fHealth = Health;*/
}

// toggle flashlight using falkweapons
simulated exec function ToggleFlashlight()
{
    local KFWeapon KFWeap;

    if (Controller == none || bIsQuickHealing > 0)
        return;

    if (KFWeapon(Weapon) != none && KFWeapon(Weapon).bTorchEnabled)
        Weapon.ClientStartFire(1);

    else
    {
        KFWeap = KFWeapon(FindInventoryType(class'Slayer_AUG'));

        if (KFWeap == none)
        {
            KFWeap = KFWeapon(FindInventoryType(class'Spas'));

            if (KFWeap == none)
            {
                KFWeap = KFWeapon(FindInventoryType(class'BenelliShotgun'));

                if (KFWeap == none)
                {
                    KFWeap = KFWeapon(FindInventoryType(class'NailGun'));

                    if (KFWeap == none)
                    {
                        KFWeap = KFWeapon(FindInventoryType(class'Shotgun'));

                        if (KFWeap == none)
                        {
                            KFWeap = KFWeapon(FindInventoryType(class'NineMMPlusDual'));

                            if (KFWeap == none)
                                KFWeap = KFWeapon(FindInventoryType(class'NineMMPlusSingle'));
                        }
                    }
                }
            }
        }

        if (KFWeap != none)
        {
            KFWeap.bPendingFlashlight = true;

            PendingWeapon = KFWeap;

            if (Weapon != none)
                Weapon.PutDown();

            else
                ChangedWeapon();
        }
    }
}

simulated function int FCanSwitchWeapon()
{
    // don't switch weapon while charging the husk gun
    if (fChargingHuskGun && Weapon != none && Huskgun(Weapon) != none)
    {
        if (!Weapon.CanThrow())
        {
            //warn("Husk Gun Switch Block:"@Level.TimeSeconds);
            return 2;
        }

        else
            fChargingHuskGun = false;
    }

    // just switch if this var is true
    if (!fForceWeapSwap)
    {
        // force switch before the weapon is equipped
        if (Level.TimeSeconds <= fWeapSwitchTime + fWeapSwitchCD)
        {
            fWeapSwitchTime = Level.TimeSeconds;

            //warn("quick allow:"@Level.TimeSeconds);
            return 0;
        }

        if (Weapon != none && Frag(Weapon) == none)
        {
            // block switch while tossing a nade
            if (fReloadState != F_Reload_Allowed || (KFWeapon(Weapon) != none && KFWeapon(Weapon).ClientGrenadeState != GN_None))
                return 1;

            // pipebomb only block
            if (PipeBombExplosiveBaseFalk(Weapon) != none)
            {
                if (!PipeBombExplosiveBaseFalk(Weapon).fCanBeSwitched())
                {
                    //warn("Pipebomb Block - Time:"@Level.TimeSeconds@"fWeaponReady"@PipeBombExplosiveBaseFalk(Weapon).fWeaponReady@"HasAmmo"@Weapon.HasAmmo()@"ReadyToFire"@Weapon.ReadyToFire(0));
                    return 1;
                }
            }

            // atmine only block
            else if (ATMineExplosiveBaseFalk(Weapon) != none)
            {
                if (!ATMineExplosiveBaseFalk(Weapon).fCanBeSwitched())
                {
                    //warn("ATMine Block - Time:"@Level.TimeSeconds@"fWeaponReady"@ATMineExplosiveBaseFalk(Weapon).fWeaponReady@"HasAmmo"@Weapon.HasAmmo()@"ReadyToFire"@Weapon.ReadyToFire(0));
                    return 1;
                }
            }

            else if (KFWeapon(Weapon) != none && !Weapon.CanThrow()  &&
                    ((WinchesterBaseFalk(Weapon)          != none    && WinchesterBaseFalk(Weapon).fLoadingLastBullet)                ||
                     (ShotgunBaseFalk(Weapon)             != none    && ShotgunBaseFalk(Weapon).fLoadingLastBullet)                   ||
                     (MediShotBaseFalk(Weapon)            != none    && MediShotBaseFalk(Weapon).fLoadingLastBullet)                  ||
                     (TrenchgunBaseFalk(Weapon)           != none    && TrenchgunBaseFalk(Weapon).fLoadingLastBullet)                 ||
                     (Moss12BaseFalk(Weapon)              != none    && Moss12BaseFalk(Weapon).fLoadingLastBullet)                    ||
                     (Spas12BaseFalk(Weapon)              != none    && Spas12BaseFalk(Weapon).fLoadingLastBullet)                    ||
                     (BenelliShotgunBaseFalk(Weapon)      != none    && BenelliShotgunBaseFalk(Weapon).fLoadingLastBullet)            ||
                     (FNTPSBaseFalk(Weapon)               != none    && FNTPSBaseFalk(Weapon).fLoadingLastBullet)                     ||
                     (W1300BaseFalk(Weapon)               != none    && W1300BaseFalk(Weapon).fLoadingLastBullet)                     ||
                     (Express870BaseFalk(Weapon)          != none    && Express870BaseFalk(Weapon).fLoadingLastBullet)                ||
                     (KS23SABaseFalk(Weapon)              != none    && KS23SABaseFalk(Weapon).fLoadingLastBullet)                    ||
                     (LocalWinchesterBaseFalk(Weapon)     != none    && LocalWinchesterBaseFalk(Weapon).fLoadingLastBullet)           ||
                     (LocalMediShotBaseFalk(Weapon)       != none    && LocalMediShotBaseFalk(Weapon).fLoadingLastBullet)             ||
                     (LocalTrenchgunBaseFalk(Weapon)      != none    && LocalTrenchgunBaseFalk(Weapon).fLoadingLastBullet)            ||
                     (LocalShotgunBaseFalk(Weapon)        != none    && LocalShotgunBaseFalk(Weapon).fLoadingLastBullet)              ||
                     (LocalMoss12BaseFalk(Weapon)         != none    && LocalMoss12BaseFalk(Weapon).fLoadingLastBullet)               ||
                     (LocalSpas12BaseFalk(Weapon)         != none    && LocalSpas12BaseFalk(Weapon).fLoadingLastBullet)               ||
                     (LocalBenelliShotgunBaseFalk(Weapon) != none    && LocalBenelliShotgunBaseFalk(Weapon).fLoadingLastBullet)       ||
                     (LocalFNTPSBaseFalk(Weapon)          != none    && LocalFNTPSBaseFalk(Weapon).fLoadingLastBullet)                ||
                     (LocalW1300BaseFalk(Weapon)          != none    && LocalW1300BaseFalk(Weapon).fLoadingLastBullet)                ||
                     (LocalExpress870BaseFalk(Weapon)     != none    && LocalExpress870BaseFalk(Weapon).fLoadingLastBullet)           ||
                     (LocalKS23SABaseFalk(Weapon)         != none    && LocalKS23SABaseFalk(Weapon).fLoadingLastBullet)               ||
                     !(KFWeapon(Weapon).bHoldToReload && KFWeapon(Weapon).bIsReloading)))
                     {
                         //warn("Block:"@Weapon.CanThrow()@"- Time:"@Level.TimeSeconds@"- Switch time"@fWeapSwitchTime@"- Weapon:"@Weapon);
                         return 1;
                     }
        }
    }

    fWeapSwitchTime = Level.TimeSeconds;

    return 0;
}

// clear the buffered weapon class on the client
simulated function FClientClearBuffers()
{
    //warn("client clear"@Level.TimeSeconds);
    BufferedWeaponClass = none;
    fPendingWeapSwitch  = false;
    fInvGroupSwitch     = Default.fInvGroupSwitch;
    fQHWeapSwitch       = Default.fQHWeapSwitch;
}

// clear buffered stuff before buffering getweapon
function FServerClearBuffers()
{
    fPendingWeapSwitch = false;
    fDelayQuickHeal    = false;
    fInvGroupSwitch    = Default.fInvGroupSwitch;
    fQHWeapSwitch      = Default.fQHWeapSwitch;
}

// set a new buffered weapon class and override the previous buffers
simulated function SetBufferedWeaponClass(class<Weapon> NewWeaponClass)
{
    //warn("Set buffered weapon class"@Level.Timeseconds);
    BufferedWeaponClass = NewWeaponClass;
    fPendingWeapSwitch  = false;
    fDelayQuickHeal     = false;
    fInvGroupSwitch     = Default.fInvGroupSwitch;
    fQHWeapSwitch       = Default.fQHWeapSwitch;
    fCanSwitchSyringe   = false;
    FServerClearBuffers();
}

// clean up after getweapon
simulated function FGetWeaponCalled()
{
    //warn("GetWeaponCalled"@Level.Timeseconds@"- pending:"@fPendingWeapSwitch@"- fweap:"@fWeapSwitch);
    BufferedWeaponClass = none;
    fWeapSwitchTime     = Level.TimeSeconds;
    fCanSwitchSyringe   = false;
}

// don't glitch this
simulated function SwitchWeapon(byte F)
{
    local Weapon DesiredWeap;
    local bool AllowSwitch;
    local Inventory I;
    local int fWeapInGroup;

    //warn("SwitchWeapon F:"@F@"Force:"@fForceWeapSwap@"From"@Weapon@"- Time:"@Level.TimeSeconds);

    // testing stuff to make this more responsive
    if (Weapon == none && !fForceWeapSwap && fPendingWeapSwitch)
    {
        fWeapSwitch         = F;
        BufferedWeaponClass = none;
        return;
    }

    // don't switch weapon while charging the husk gun
    if (fChargingHuskGun && Weapon != none && Huskgun(Weapon) != none)
    {
        if (!Weapon.CanThrow())
        {
            //warn("Husk Gun Switch Block:"@Level.TimeSeconds);
            return;
        }

        else
            fChargingHuskGun = false;
    }

    // just switch if this var is true
    if (!fForceWeapSwap)
    {
        // force switch before the weapon is equipped
        if (Level.TimeSeconds <= fWeapSwitchTime + fWeapSwitchCD)
        {
            if (fIsQuickHealing || (Weapon != none && Syringe(Weapon) != none && Weapon.ClientState != WS_PutDown))
            {
                fCanSwitchSyringe   = True;
                fWeapSwitch         = F;
                BufferedWeaponClass = none;
                //warn("fWeapSwitch Set:"@fWeapSwitch@"- Time:"@Level.TimeSeconds@"- State:"@Weapon.ClientState);
                return;
            }

            fForceWeapSwap = true;
        }

        else if (Weapon != none && Frag(Weapon) == none)
        {
            // block switch while tossing a nade
            if (fReloadState != F_Reload_Allowed || (KFWeapon(Weapon) != none && KFWeapon(Weapon).ClientGrenadeState != GN_None))
                return;

            // pipebomb only block
            if (PipeBombExplosiveBaseFalk(Weapon) != none)
            {
                if (!PipeBombExplosiveBaseFalk(Weapon).fCanBeSwitched())
                {
                    //warn("Pipebomb Block - Time:"@Level.TimeSeconds@"fWeaponReady"@PipeBombExplosiveBaseFalk(Weapon).fWeaponReady@"HasAmmo"@Weapon.HasAmmo()@"ReadyToFire"@Weapon.ReadyToFire(0));

                    if (!fDelayQuickHeal)
                    {
                        fPendingWeapSwitch  = True;
                        fWeapSwitch         = F;
                        BufferedWeaponClass = none;
                        //warn("Pipebomb fWeapSwitch Set:"@fWeapSwitch@"- Time:"@Level.TimeSeconds);
                    }

                    else
                    {
                        fQHWeapSwitch       = F;
                        BufferedWeaponClass = none;
                        //warn("Pipebomb set:"@fQHWeapSwitch@" - Time:"@Level.TimeSeconds);
                    }

                    return;
                }
            }

            // atmine only block
            else if (ATMineExplosiveBaseFalk(Weapon) != none)
            {
                if (!ATMineExplosiveBaseFalk(Weapon).fCanBeSwitched())
                {
                    //warn("ATMine Block - Time:"@Level.TimeSeconds@"fWeaponReady"@ATMineExplosiveBaseFalk(Weapon).fWeaponReady@"HasAmmo"@Weapon.HasAmmo()@"ReadyToFire"@Weapon.ReadyToFire(0));

                    if (!fDelayQuickHeal)
                    {
                        fPendingWeapSwitch  = True;
                        fWeapSwitch         = F;
                        BufferedWeaponClass = none;
                        //warn("ATMine fWeapSwitch Set:"@fWeapSwitch@"- Time:"@Level.TimeSeconds);
                    }

                    else
                    {
                        fQHWeapSwitch       = F;
                        BufferedWeaponClass = none;
                        //warn("ATMine set:"@fQHWeapSwitch@" - Time:"@Level.TimeSeconds);
                    }

                    return;
                }
            }

            else if (KFWeapon(Weapon) != none && !Weapon.CanThrow()  &&
                    ((WinchesterBaseFalk(Weapon)          != none    && WinchesterBaseFalk(Weapon).fLoadingLastBullet)                ||
                     (ShotgunBaseFalk(Weapon)             != none    && ShotgunBaseFalk(Weapon).fLoadingLastBullet)                   ||
                     (MediShotBaseFalk(Weapon)            != none    && MediShotBaseFalk(Weapon).fLoadingLastBullet)                  ||
                     (TrenchgunBaseFalk(Weapon)           != none    && TrenchgunBaseFalk(Weapon).fLoadingLastBullet)                 ||
                     (Moss12BaseFalk(Weapon)              != none    && Moss12BaseFalk(Weapon).fLoadingLastBullet)                    ||
                     (Spas12BaseFalk(Weapon)              != none    && Spas12BaseFalk(Weapon).fLoadingLastBullet)                    ||
                     (BenelliShotgunBaseFalk(Weapon)      != none    && BenelliShotgunBaseFalk(Weapon).fLoadingLastBullet)            ||
                     (FNTPSBaseFalk(Weapon)               != none    && FNTPSBaseFalk(Weapon).fLoadingLastBullet)                     ||
                     (W1300BaseFalk(Weapon)               != none    && W1300BaseFalk(Weapon).fLoadingLastBullet)                     ||
                     (Express870BaseFalk(Weapon)          != none    && Express870BaseFalk(Weapon).fLoadingLastBullet)                ||
                     (KS23SABaseFalk(Weapon)              != none    && KS23SABaseFalk(Weapon).fLoadingLastBullet)                    ||
                     (LocalWinchesterBaseFalk(Weapon)     != none    && LocalWinchesterBaseFalk(Weapon).fLoadingLastBullet)           ||
                     (LocalMediShotBaseFalk(Weapon)       != none    && LocalMediShotBaseFalk(Weapon).fLoadingLastBullet)             ||
                     (LocalTrenchgunBaseFalk(Weapon)      != none    && LocalTrenchgunBaseFalk(Weapon).fLoadingLastBullet)            ||
                     (LocalShotgunBaseFalk(Weapon)        != none    && LocalShotgunBaseFalk(Weapon).fLoadingLastBullet)              ||
                     (LocalMoss12BaseFalk(Weapon)         != none    && LocalMoss12BaseFalk(Weapon).fLoadingLastBullet)               ||
                     (LocalSpas12BaseFalk(Weapon)         != none    && LocalSpas12BaseFalk(Weapon).fLoadingLastBullet)               ||
                     (LocalBenelliShotgunBaseFalk(Weapon) != none    && LocalBenelliShotgunBaseFalk(Weapon).fLoadingLastBullet)       ||
                     (LocalFNTPSBaseFalk(Weapon)          != none    && LocalFNTPSBaseFalk(Weapon).fLoadingLastBullet)                ||
                     (LocalW1300BaseFalk(Weapon)          != none    && LocalW1300BaseFalk(Weapon).fLoadingLastBullet)                ||
                     (LocalExpress870BaseFalk(Weapon)     != none    && LocalExpress870BaseFalk(Weapon).fLoadingLastBullet)           ||
                     (LocalKS23SABaseFalk(Weapon)         != none    && LocalKS23SABaseFalk(Weapon).fLoadingLastBullet)               ||
                     !(KFWeapon(Weapon).bHoldToReload && KFWeapon(Weapon).bIsReloading)))
                     {
                         //warn("Block:"@Weapon.CanThrow()@" - Time:"@Level.TimeSeconds);

                         // select new weapon while healing
                         if (Syringe(Weapon) != none)
                         {
                             fCanSwitchSyringe   = True;
                             fPendingWeapSwitch  = True;
                             fWeapSwitch         = F;
                             BufferedWeaponClass = none;
                             //warn("fWeapSwitch Set:"@fWeapSwitch@"- Time:"@Level.TimeSeconds);

                             // not sure this is needed here but it probably won't hurt
                             if (fSyringeMain)
                                 fWeapSwapTick = fSyringeSwapLeft;

                             else
                                 fWeapSwapTick = fSyringeSwapRight;
                         }

                         // select new weapon while reloading, but not when there's a pending quick heal, melee weapons were behaving weirdly but now look noice
                         else if (!fDelayQuickHeal /*&& KFMeleeGun(Weapon) == none*/)
                         {
                             fPendingWeapSwitch  = True;
                             fWeapSwitch         = F;
                             BufferedWeaponClass = none;
                             //warn("fWeapSwitch Set:"@fWeapSwitch@"- Time:"@Level.TimeSeconds);
                         }

                         else
                         {
                             fQHWeapSwitch       = F;
                             BufferedWeaponClass = none;
                             //warn("Set:"@fQHWeapSwitch@" - Time:"@Level.TimeSeconds);
                         }

                         return;
                     }
        }
    }

    if (fQHWeapSwitch == Default.fQHWeapSwitch)
    {
        //warn("RESET FORCE SWAP - Time:"@Level.TimeSeconds);
        fForceWeapSwap     = False;
    }

    else
        BufferedWeaponClass = none;

    fCanSwitchSyringe   = False;
    //warn("THIS"@Level.TimeSeconds);
    fIsQuickHealing     = False;
    fCrazyFiveNadeTick  = Default.fCrazyFiveNadeTick;
    fSwitchingCrazyFive = False;

    fWeapSwitchTime = Level.TimeSeconds;

//     if (ROLE != ROLE_Authority && Level.TimeSeconds > fWeapSwitchTime + fWeapSwitchCD + 0.1)
//     {
//         fWeapSwitchTime = Level.TimeSeconds;
//         //warn("SETTING fWeapSwitchTime: "@fWeapSwitchTime);
//     }

    // Vanilla serverperks code, added something to switch to the first weapon with ammo of that group
    if (Weapon != None && Weapon.Inventory != None)
        DesiredWeap = Weapon.Inventory.WeaponChange(F, false);

    if (DesiredWeap == none && Inventory != None)
        DesiredWeap = Inventory.WeaponChange(F, true);

    if (DesiredWeap != none)
    {
        //warn("DesiredWeap:"@DesiredWeap@"- Time:"@Level.TimeSeconds);

        // check if we have more than one weapon in that inventory group
        if (Weapon != none && F != Weapon.InventoryGroup && F < 5)
        {
            // check how many weapons we have in that inventory group and how many we can switch to
            for (I=Inventory; I!=None; I=I.Inventory)
            {
                fForceAmmoCheck = True;

                if (I.InventoryGroup == F && Weapon(I) != none && AllowHoldWeapon(Weapon(I)))
                {
                    fWeapInGroup++;
                    //warn("FOUND:"@I);
                }
            }

            // if we do have at least one, force an ammo to skip empty ones
            if (fWeapInGroup > 0)
                fForceAmmoCheck = True;

            else
                fForceAmmoCheck = False;
        }

        AllowSwitch = AllowHoldWeapon(DesiredWeap);

        // try to switch to an usable gun if the first one failed
        if (fWeapInGroup > 0 && !AllowSwitch)
        {
            //warn("SWITCH TO FULL:"@F);
            fInvGroupSwitch = F;
        }
    }

    fWeapSwitch        = 69;
    //warn("fWeapSwitch Set2:"@fWeapSwitch@"- Time:"@Level.TimeSeconds);

    if (AllowSwitch)
    {
        //warn("Allow Switch Time:"@Level.TimeSeconds);
        fInvGroupSwitch = 69;
        fSwitchToEmpty  = False;
        fQHWeapSwitch   = Default.fQHWeapSwitch;
        Super(KFPawn).SwitchWeapon(F);
    }

    else if (DesiredWeap != none && fPermitWeapSwitch && fInvGroupSwitch != 69 && fQHWeapSwitch == Default.fQHWeapSwitch)
    {
        //warn("Not allowed 1 Time:"@Level.TimeSeconds);

        if (fInvGroupSwitch < 5)
        {
            //warn("A - fInvGroupSwitch:"@fInvGroupSwitch@" - Time:"@Level.TimeSeconds);

            for (I=Inventory; I!=None; I=I.Inventory)
            {
                if (Weapon(I) != none && Weapon(I).InventoryGroup == fInvGroupSwitch && AllowHoldWeapon(Weapon(I)))
                {
                    fInvGroupSwitch = 69;
                    ClientForceChangeWeapon(Weapon(I));
                    return;
                }
            }
        }

        else
        {
            fSwitchToEmpty  = True;
            //warn("B - fInvGroupSwitch:"@fInvGroupSwitch@" - Time:"@Level.TimeSeconds);
            FSwitchToBestWeapon();
        }
    }
}

// more stuff to prevent random weapon switch at spawn and stuff about weapons without ammo and best weapon selection
simulated function bool AllowHoldWeapon(Weapon InWeapon)
{
    //warn("fSwitchToEmpty:"@fSwitchToEmpty@"- fInvGroupSwitch:"@fInvGroupSwitch@"- fWeapSwitch:"@fWeapSwitch@"- fForceAmmoCheck:"@fForceAmmoCheck@"- No Ammo:"@InWeapon.bNoAmmoInstances@"- Amount0:"@InWeapon.AmmoAmount(0)@"- Amount1:"@InWeapon.AmmoAmount(1)@"- Medic:"@MP7MMedicGun(InWeapon) != none);

    if (!fSwitchToEmpty               && (fInvGroupSwitch        != 69 || fWeapSwitch != 69 || fForceAmmoCheck) && KFMeleeGun(InWeapon)    == none && !InWeapon.bNoAmmoInstances &&
            ((InWeapon.AmmoAmount(0) <= 0  && InWeapon.AmmoAmount(1) <= 0)  || (MP7MMedicGun(InWeapon) != none && InWeapon.AmmoAmount(0) <= 0)))
    {
        fForceAmmoCheck = False;

        return False;
    }

    fForceAmmoCheck = False;

    return fPermitWeapSwitch;
}

// equip wanted weapon after syringe usage or from delayed switch
exec function SwitchToLastWeapon()
{
    local Inventory I;

    //warn("SwitchToLast Time:"@Level.TimeSeconds);

    fCurWeapSwapTick   = 0;
    fIsQuickHealing    = False;
    FClientSetFIsQuickHealing(False);
    fForceWeapSwap     = True;

    if (Weapon != None && Frag(Weapon) == None && PipeBombExplosiveBaseFalk(Weapon) == None && ATMineExplosiveBaseFalk(Weapon) == None)
        Weapon.bCanThrow = True;

    if (fWeapSwitch != 69 /*&& fCanSwitchSyringe*/)
    {
        for (I=Inventory; I!=None; I=I.Inventory)
        {
            if (Weapon(I) != none && Weapon(I).InventoryGroup == fWeapSwitch)
            {
                fPermitWeapSwitch = True;
                //warn("To: "@fWeapSwitch@" - Time:"@Level.TimeSeconds);
                SwitchWeapon(fWeapSwitch);
                fCanSwitchSyringe = False;
                fWeapSwitch       = 69;
                fQHWeapSwitch     = Default.fQHWeapSwitch;
                //warn("fWeapSwitch Set LOL:"@fWeapSwitch@"- Time:"@Level.TimeSeconds);
                return;
            }
        }

        // if we don't have a syringe, don't do the heal switch on fail
        if (Syringe(Weapon) == None)
        {
            fForceWeapSwap     = False;
            return;
        }

        //warn("WEAPON NOT FOUND");
    }

    fCanSwitchSyringe = False;
    fSwitchToEmpty    = False;
    fForceAmmoCheck   = True;

    // buffered quick heal switch buffer
    if (fQHWeapSwitch != Default.fQHWeapSwitch)
    {
        if (fOldWeapon != Weapon)
        {
            if (fWeapSwitch == 69 && AllowHoldWeapon(fOldWeapon))
            {
                //warn("Switch to selected:"@fQHWeapSwitch@"Time:"@Level.TimeSeconds);
                SwitchWeapon(fQHWeapSwitch);
                fQHWeapSwitch = Default.fQHWeapSwitch;
            }

            else
            {
                //warn("SWITCH TO BEST 1:"@Level.TimeSeconds);
                FSwitchToBestWeapon();
            }
        }
    }

    // new switch to last weapon
    else if (fOldWeapon != none)
    {
        if (fOldWeapon != Weapon)
        {
            if (fWeapSwitch == 69 && AllowHoldWeapon(fOldWeapon))
            {
                //warn("Switch to previous:"@fOldWeapon@"Time:"@Level.TimeSeconds);
                PendingWeapon = fOldWeapon;
                Weapon.PutDown();
            }

            else
            {
                Weapon.PutDown();
                FSwitchToBestWeapon();
            }
        }

        else
        {
            if (fWeapSwitch == 69)
            {
                //warn("RESET Switch to same weapon."@Level.TimeSeconds);
                fInvGroupSwitch    = 69;
                fPendingWeapSwitch = False;
                fCanSwitchSyringe  = False;
                fForceWeapSwap     = False;
            }

            else
            {
               // warn("SWITCH TO BEST 3:"@Level.TimeSeconds);
                Weapon.PutDown();
                FSwitchToBestWeapon();
            }
        }

        fOldWeapon = None;
    }

    // edited vanilla switch to last weapon
    else if (Weapon != None && Weapon.OldWeapon != None && Weapon.OldWeapon != Weapon)
    {
        if (fWeapSwitch == 69 && AllowHoldWeapon(Weapon.OldWeapon))
        {
            //warn("Switch to previous 2:"@Weapon.OldWeapon@"Time:"@Level.TimeSeconds);
            PendingWeapon = Weapon.OldWeapon;
            Weapon.PutDown();
        }

        else
        {
            //warn("SWITCH TO BEST 4:"@Level.TimeSeconds);
            Weapon.PutDown();
            FSwitchToBestWeapon();
        }
    }

    fForceWeapSwap = False;
}


// now with 200% more falk functions
function AddDefaultInventory()
{
    // commenting this stuff since I haven't worked on it and I probably won't unless really needed
    //if (KFStoryGameInfo(Level.Game) != none)
    //   FalkStoryAddDefaultInventory();

    //else
    FalkVanillaAddDefaultInventory();
}


// edited KFMod Function
function FalkVanillaAddDefaultInventory()
{
    local int i;

    if ( KFSPGameType(Level.Game) != none )
    {
        Level.Game.AddGameSpecificInventory(self);

        if ( Inventory != none )
            Inventory.OwnerEvent('LoadOut');

        return;
    }

    if (IsLocallyControlled())
    {
        Switch(fAddInvPass)
        {
            case 0:
                if (fShouldClearInv && KFPlayerReplicationInfo(PlayerReplicationInfo) != none && KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill != none)
                {
                    fShouldClearInv = False;
                    KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill.static.AddDefaultInventory(KFPlayerReplicationInfo(PlayerReplicationInfo), self);
                }
                break;

            case 1:
                if ( RequiredEquipment[0] != "" )
                {
                    CreateInventory(RequiredEquipment[i]);
                }

                break;

            case 2:
                for (i = 1; i < 16; i++)
                {
                    if ( RequiredEquipment[i] != "" )
                        CreateInventory(RequiredEquipment[i]);

                    //fAddInvPass = 69;
                }


                break;
        }

        Level.Game.AddGameSpecificInventory(self);
    }

    else
    {
        Level.Game.AddGameSpecificInventory(self);

        Switch(fAddInvPass)
        {
            case 0:
                if (fShouldClearInv && KFPlayerReplicationInfo(PlayerReplicationInfo) != none && KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill != none)
                {
                    fShouldClearInv = False;
                    KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill.static.AddDefaultInventory(KFPlayerReplicationInfo(PlayerReplicationInfo), self);
                }

                break;

            case 1:
                if (RequiredEquipment[0] != "")
                {
                    CreateInventory(RequiredEquipment[i]);
                }

                break;

            case 2:
                for (i = 1; i < 16; i++)
                {
                    if ( RequiredEquipment[i] != "" )
                        CreateInventory(RequiredEquipment[i]);

                    //fAddInvPass = 69;
                }

                break;
        }
    }

    // HACK FIXME - LOL NOPE
    if ( Inventory != None )
        Inventory.OwnerEvent('LoadOut');
}

// edited story mode function, there's an high chance for this to contain loads of bugs
// this function was never called before in the Lair and was added for possible future edits and usage - Falk689
function FalkStoryAddDefaultInventory()
{
    local KFLevelRules_Story	StoryRules;
    local int i,InvCount;
    local Inventory Inv;
    local KFPlayerController_Story SPC;
    local bool bUseCheckPointGear;
    local float MaxAmmo,CurrentAmmo;

    /* Clear pawn equipment defaults */
    for (i = 0 ; i < ArrayCount(RequiredEquipment) ; i ++)
    {
        RequiredEquipment[i] = "";
    }

    /* Base gear should fill from the level rules */
    Level.Game.AddGameSpecificInventory(self);

    /* next we Add saved gear - stuff we were carrying when we died */

    bUseCheckPointGear = RetrieveSavedLoadOut(Controller);

    Super(UnrealPawn).AddDefaultInventory();

    /* next add perk specific gear - on request */
    if (KFStoryGameInfo(Level.Game) != none && fShouldClearInv)
    {
        fShouldClearInv = False;
        StoryRules = KFStoryGameInfo(Level.Game).StoryRules;

        if (StoryRules != none && StoryRules.bAllowPerkStartingWeaps)
        {
            if (KFPlayerReplicationInfo(PlayerReplicationInfo) != none && KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill != none)
            {
                KFPlayerReplicationInfo(PlayerReplicationInfo).ClientVeteranSkill.static.AddDefaultInventory(KFPlayerReplicationInfo(PlayerReplicationInfo), self);
            }
        }
    }

    if (bUseCheckPointGear)
    {
        /* restore saved ammo values from our last checkpoint .. */
        SPC = KFPlayerController_Story(Controller);

        if (SPC != none)
        {
            for (Inv=Inventory; Inv!=None ;Inv=Inv.Inventory)
            {
                if (Weapon(Inv) != none)
                {
                    Weapon(Inv).GetAmmoCount(MaxAmmo,CurrentAmmo);
                    Weapon(Inv).ConsumeAmmo(0,MaxAmmo,true);

                    Weapon(Inv).AddAmmo(SPC.SavedAmmo[InvCount],0);
                    KFWeapon(Inv).MagAmmoRemaining = SPC.SavedMagAmmo[InvCount];

                    InvCount++;
                }
            }
        }
    }
}

// this should force a weapon change
simulated function ClientForceChangeWeapon(Inventory NewWeapon)
{
    //fForceWeapSwap = True;
    PendingWeapon  = Weapon(NewWeapon);

    if (Weapon != none)
        Weapon.PutDown();

    else
        ChangedWeapon();
}

// start switch to best weapon the falk way
simulated function FSwitchToBestWeapon()
{
    local KFPlayerReplicationInfo KFPRI;
    local class<FVeterancyTypes> fVet;

    if (fQHWeapSwitch != Default.fQHWeapSwitch)
        return;

    //warn("SwitchToBest - Time:"@Level.TimeSeconds);
    fPermitWeapSwitch = True;
    KFPRI             = KFPlayerReplicationInfo(PlayerReplicationInfo);
    fInvGroupSwitch   = 6; // this inventory group shouldn't exist

    // if we're a zerk, start switching to melee stuff
    if (KFPRI != none)
    {
        fVet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

        if (fVet != none && fVet.Static.IsMeleeBased())
            fInvGroupSwitch = 7; // this neither
    }
}

// implement our switch on current weapon sold
simulated function ClientCurrentWeaponSold()
{
    FSwitchToBestWeapon();
}

function PlayHit(float Damage, Pawn InstigatedBy, vector HitLocation, class<DamageType> damageType, vector Momentum, optional int HI )
{
    local Vector HitNormal;
    local Vector HitRay;
    local Name HitBone;
    local float HitBoneDist;
    local PlayerController PC;
    local bool bShowEffects, bRecentHit;
    local ProjectileBloodSplat BloodHit;
    local rotator SplatRot;

    bRecentHit = Level.TimeSeconds - LastPainTime < 0.2;

    if ( Damage <= 0 )
        return;

    if (FalkMonsterBase(InstigatedBy) != none && (FalkMonsterBase(InstigatedBy).fFrozen || FalkMonsterBase(InstigatedBy).fIsStunned || FalkMonsterBase(InstigatedBy).bZapped))
        return;

    // Call the modified version of the original Pawn playhit
    OldPlayHit(Damage,InstigatedBy,HitLocation,DamageType,Momentum);

    PC = PlayerController(Controller);
    bShowEffects = ( (Level.NetMode != NM_Standalone) || (Level.TimeSeconds - LastRenderTime < 2.5)
            || ((InstigatedBy != None) && (PlayerController(InstigatedBy.Controller) != None))
            || (PC != None) );
    if ( !bShowEffects )
        return;

    if ( BurnDown > 0 && !bBurnified )
        bBurnified = true;

    HitRay = vect(0,0,0);
    if( InstigatedBy != None )
        HitRay = Normal(HitLocation-(InstigatedBy.Location+(vect(0,0,1)*InstigatedBy.EyeHeight)));

    if( DamageType.default.bLocationalHit )
    {
        CalcHitLoc( HitLocation, HitRay, HitBone, HitBoneDist );
    }
    else
    {
        HitLocation = Location;
        HitBone = FireRootBone;
        HitBoneDist = 0.0f;
    }

    if( DamageType.default.bAlwaysSevers && DamageType.default.bSpecial )
        HitBone = 'head';

    if( InstigatedBy != None )
        HitNormal = Normal( Normal(InstigatedBy.Location-HitLocation) + VRand() * 0.2 + vect(0,0,2.8) );
    else
        HitNormal = Normal( Vect(0,0,1) + VRand() * 0.2 + vect(0,0,2.8) );

    //log("HitLocation "$Hitlocation) ;

    if ( DamageType.Default.bCausesBlood && (!bRecentHit || (bRecentHit && (FRand() > 0.8))))
    {
        if ( !class'GameInfo'.static.NoBlood() )
        {
            if ( Momentum != vect(0,0,0) )
                SplatRot = rotator(Normal(Momentum));
            else
            {
                if ( InstigatedBy != None )
                    SplatRot = rotator(Normal(Location - InstigatedBy.Location));
                else
                    SplatRot = rotator(Normal(Location - HitLocation));
            }

            BloodHit = Spawn(ProjectileBloodSplatClass,InstigatedBy,, HitLocation, SplatRot);
        }
    }

    // hack for siren
    if ( (DamageType.name == 'SirenScreamDamage') && (Health < 0) )
    {
        if( (InstigatedBy != None) && (VSize(InstigatedBy.Location - Location) < 200) )
        {
            DoDamageFX( 'obliterate', 5000 * Damage, DamageType, Rotator(HitNormal) );
        }
        else
        {
            DoDamageFX( HeadBone, 5000 * Damage, DamageType, Rotator(HitNormal) );
            PlaySound(DecapitationSound, SLOT_Misc,1.30,true,525);
        }
    }
    else
        DoDamageFX( HitBone, Damage, DamageType, Rotator(HitNormal) );

    if (DamageType.default.DamageOverlayMaterial != None && Damage > 0 ) // additional check in case shield absorbed
        SetOverlayMaterial( DamageType.default.DamageOverlayMaterial, DamageType.default.DamageOverlayTime, false );
}

// Be hurt in english instead of just making weird noises
function PlayTakeHit(vector HitLocation, int Damage, class<DamageType> DamageType)
{
    local vector direction;
    local rotator InvRotation;
    local float jarscale;
    local float fSoundChance;

    if (Role == ROLE_Authority && (Level.Game.NumPlayers == 1 || Health > 0.25 * fHealthMax))
    {
        if (PlayerReplicationInfo != none && Level.TimeSeconds - LastPainSound >= MinTimeBetweenPainSounds)
        {
            LastPainSound = Level.TimeSeconds;
            fSoundChance  = FRand();

            //warn("VOICE:"@PlayerReplicationInfo.VoiceType);

            // Male voice one
            if (PlayerReplicationInfo.VoiceType == Class'KFVoicePack')
            {
                if (fSoundChance <= 0.33f)
                    PlaySound(Sound'LairSounds_S.CharacterHurt.Ohcrap', SLOT_Talk, 2.0, true, 200.0);

                else if (fSoundChance <= 0.66f)
                    PlaySound(Sound'LairSounds_S.CharacterHurt.Dammit', SLOT_Talk, 2.0, true, 200.0);

                else
                    PlaySound(Sound'LairSounds_S.CharacterHurt.Christ', SLOT_Talk, 2.0, true, 200.0);
            }

            // Male voice two
            else if (PlayerReplicationInfo.VoiceType == Class'KFVoicePackTwo')
            {
                if (fSoundChance <= 0.33f)
                    PlaySound(Sound'LairSounds_S.CharacterHurt.Crap', SLOT_Talk, 2.0, true, 200.0);

                else if (fSoundChance <= 0.66f)
                    PlaySound(Sound'LairSounds_S.CharacterHurt.Dammit2', SLOT_Talk, 2.0, true, 200.0);

                else
                    PlaySound(Sound'LairSounds_S.CharacterHurt.Christ2', SLOT_Talk, 2.0, true, 200.0);
            }

            // Female
            else if (PlayerReplicationInfo.VoiceType == Class'KFVoicePackFemale')
            {
                if (fSoundChance <= 0.33f)
                    PlaySound(Sound'LairSounds_S.CharacterHurt.Ohcrap2', SLOT_Talk, 2.0, true, 200.0);

                else if (fSoundChance <= 0.66f)
                    PlaySound(Sound'LairSounds_S.CharacterHurt.Dammit3', SLOT_Talk, 2.0, true, 200.0);

                else
                    PlaySound(Sound'LairSounds_S.CharacterHurt.Ahh', SLOT_Talk, 2.0, true, 200.0);
            }

            // Robot
            else if (PlayerReplicationInfo.VoiceType == Class'KFVoicePackRobot')
            {
                if (fSoundChance <= 0.25f)
                    PlaySound(Sound'LairSounds_S.CharacterHurt.DARHurt1', SLOT_Talk, 2.0, true, 200.0);

                else if (fSoundChance <= 0.50f)
                    PlaySound(Sound'LairSounds_S.CharacterHurt.DARHurt2', SLOT_Talk, 2.0, true, 200.0);

                else if (fSoundChance <= 0.75f)
                    PlaySound(Sound'LairSounds_S.CharacterHurt.DARHurt3', SLOT_Talk, 2.0, true, 200.0);

                else
                    PlaySound(Sound'LairSounds_S.CharacterHurt.DARHurt4', SLOT_Talk, 2.0, true, 200.0);
            }
        }
    }

    // for standalone and client
    // Cooney
    if (Level.NetMode != NM_DedicatedServer)
    {
        // Get the approximate direction
        // that the hit went into the body
        direction = Location - HitLocation;
        // No up-down jarring effects since
        // I dont have the barrel valocity
        direction.Z = 0.0f;
        direction = normal(direction);

        // We need to rotate the jarring direction
        // in screen space so basically the
        // exact opposite of the player's pawn's
        // rotation.
        InvRotation.Yaw = -Rotation.Yaw;
        InvRotation.Roll = -Rotation.Roll;
        InvRotation.Pitch = -Rotation.Pitch;
        direction = direction >> InvRotation;

        jarscale = 0.1f + (Damage/10.0f);
        if ( jarscale > 1.0f )
        {
            jarscale = 1.0f;
        }

        DoHitCamEffects(direction,jarscale,2.0,1.0);
    }
}

// hopefully zoom out on weapon drop
simulated function FClientZoomOut()
{
    if (KFWeapon(Weapon) != none)
        KFWeapon(Weapon).ZoomOut(false);
}

// close the inventory on weapon toss
function TossWeapon(Vector TossVel)
{
    local Vector X,Y,Z;

    FalkCloseInventory();

    if (KFWeapon(Weapon) != none && KFWeapon(Weapon).bAimingRifle)
    {
        KFWeapon(Weapon).ZoomOut(false);
        FClientZoomOut();
    }

    if (HuskGun(Weapon) != none)
    {
        Weapon.Velocity = TossVel;
        GetAxes(Rotation,X,Y,Z);

        DropHuskGun(Location + 0.8 * CollisionRadius * X - 0.5 * CollisionRadius * Y);
    }

    else
        Super.TossWeapon(TossVel);

    FSwitchToBestWeapon();
}

// guess what, yep, close it on pickup
function HandlePickup(Pickup pick)
{
    FalkCloseInventory();
    Super.HandlePickup(pick);
}

// here too, no inventory
function ThrowGrenade()
{
    local inventory inv;
    local Frag aFrag;

    FalkCloseInventory();

    for (inv = inventory; inv != none; inv = inv.Inventory)
    {
        aFrag = Frag(inv);

        if (aFrag != none && aFrag.HasAmmo() && (!bThrowingNade || fSwitchingCrazyFive))
        {
            if (KFWeapon(Weapon) == none || Weapon.GetFireMode(0).NextFireTime - Level.TimeSeconds > 0.1 ||
                    (KFWeapon(Weapon).bIsReloading && !KFWeapon(Weapon).InterruptReload()))
            {
                return;
            }

            if (Weapon != none)
            {
                if (M79GrenadeLauncherBaseFalk(Weapon) != none)
                {
                    if (KFWeapon(Weapon).ClientGrenadeState != GN_TempDown && (!M79GrenadeLauncherBaseFalk(Weapon).fWeaponReady || !Weapon.CanThrow()))
                        return;

                    M79GrenadeLauncherBaseFalk(Weapon).fWeaponReady = False;
                }

                else if (SPGrenadeLauncherBaseFalk(Weapon) != none)
                {
                    if (KFWeapon(Weapon).ClientGrenadeState != GN_TempDown && (!SPGrenadeLauncherBaseFalk(Weapon).fWeaponReady || !Weapon.CanThrow()))
                        return;

                    SPGrenadeLauncherBaseFalk(Weapon).fWeaponReady  = False;
                }

                else if (M99SniperRifleBaseFalk(Weapon) != none)
                {
                    if (KFWeapon(Weapon).ClientGrenadeState != GN_TempDown && (!M99SniperRifleBaseFalk(Weapon).fWeaponReady || !Weapon.CanThrow()))
                        return;

                    M99SniperRifleBaseFalk(Weapon).fWeaponReady     = False;
                }

                else if (LAWBaseFalk(Weapon) != none)
                {
                    if (KFWeapon(Weapon).ClientGrenadeState != GN_TempDown && (!LAWBaseFalk(Weapon).fWeaponReady || !Weapon.CanThrow()))
                        return;

                    LAWBaseFalk(Weapon).fWeaponReady                = False;
                }

                else if (BoomStickBaseFalk(Weapon) != none)
                {
                    if (KFWeapon(Weapon).ClientGrenadeState != GN_TempDown && (!BoomStickBaseFalk(Weapon).fWeaponReady || !Weapon.CanThrow()))
                        return;

                    BoomStickBaseFalk(Weapon).fWeaponReady          = False;
                }

                else if (AshotBaseFalk(Weapon) != none)
                {
                    if (KFWeapon(Weapon).ClientGrenadeState != GN_TempDown && (!AshotBaseFalk(Weapon).fWeaponReady || !Weapon.CanThrow()))
                        return;

                    BoomStickBaseFalk(Weapon).fWeaponReady          = False;
                }

                else if (!Weapon.CanThrow() && PipeBombExplosive(Weapon) == none && ATMineExplosive(Weapon) == none )
                {
                    return;
                }

                else if (PipeBombExplosiveBaseFalk(Weapon) != none)
                {
                    if (!PipeBombExplosiveBaseFalk(Weapon).fCanThrowGrenade())
                        return;

                    PipeBombExplosiveBaseFalk(Weapon).fDoBringUpAnim = false;
                }

                else if (ATMineExplosiveBaseFalk(Weapon) != none)
                {
                    if (!ATMineExplosiveBaseFalk(Weapon).fCanThrowGrenade())
                        return;

                    ATMineExplosiveBaseFalk(Weapon).fDoBringUpAnim = false;
                }
            }

            //warn("THROW:"@Level.TimeSeconds);

            if (!fSwitchingCrazyFive)
            {
                KFWeapon(Weapon).ClientGrenadeState = GN_TempDown;
                Weapon.PutDown();
            }

            else
            {
                aFrag.StartThrow();
            }

            fCrazyFiveNadeTick  = Default.fCrazyFiveNadeTick;
            fSwitchingCrazyFive = False;


            if (Level.GetLocalPlayerController() == Controller)
            {
                fReloadState = F_Reload_Disallowed;
                fServerSetReloadState(F_Reload_Disallowed);
            }

            break;
        }
    }
}

// put a little delay to the 'Crazy Fives' so you can spam nades while having them equipped
simulated function ThrowGrenadeFinished()
{
    if (Level.GetLocalPlayerController() == Controller)
    {
        fReloadState = F_Reload_Timer;
        fServerSetReloadState(F_Reload_Timer);
    }

    if (Weapon != none && (M79GrenadeLauncherBaseFalk(Weapon) != none || SPGrenadeLauncherBaseFalk(Weapon) != none || M99SniperRifleBaseFalk(Weapon) != none || LAWBaseFalk(Weapon) != none || BoomStickBaseFalk(Weapon) != none || AshotBaseFalk(Weapon) != none))
    {
        if (!fSwitchingCrazyFive)
        {
            fSwitchingCrazyFive = True;
            fCrazyFiveNadeTick  = Default.fCrazyFiveNadeTick;
            //warn("CRAZY FIVE START:"@Level.TimeSeconds);
        }
    }

    else
    {
        SecondaryItem = none;
        bThrowingNade = false;
        FNadeReset();

        if (Weapon != none)
        {
            KFWeapon(Weapon).ClientGrenadeState = GN_BringUp;
            Weapon.BringUp();
        }

        bThrowingNade = false;
    }
}

// sets reload state on the server
function fServerSetReloadState(FReloadStates newState)
{
//     Switch (newState)
//     {
//         case F_Reload_Allowed:
//             warn("REMOTELY ALLOWING RELOAD ON THE SERVER");
//             break;
//
//         case F_Reload_Disallowed:
//             warn("REMOTELY DISALLOWING RELOAD ON THE SERVER");
//             break;
//
//         case F_Reload_Timer:
//             warn("REMOTELY STARTING TIMER ON THE SERVER");
//             break;
//
//         default:
//             warn("WTF IS HAPPENING ON THE SERVER");
//             break;
//     }

    fReloadState = newState;
}

// reset bThrowingNade on the server
function FNadeReset()
{
    bThrowingNade = false;
    SecondaryItem = none;
}

// this is called by the server and closes your fockin inventory
simulated function FalkCloseInventory()
{
    if (PlayerController(Controller) != none && HUDKillingFloor(PlayerController(Controller).myHud) != none)
        HUDKillingFloor(PlayerController(Controller).myHud).HideInventory();
}

// drop the most valuable weapon in your inventory on dead
function Died(Controller Killer, class<DamageType> damageType, vector HitLocation)
{
    local Vector                  TossVel;
    local Trigger                 T;
    local NavigationPoint         N;
    local PlayerDeathMark         D;
    local Projectile              PP;
    local FakePlayerPawn          FP;
    local int                     fTempCost;
    local int                     i;

    // falk stuff
    local Inventory               W;       // used for the loop through inventory
    local int                     Price;   // used to select most expensive weapon to drop
    local class<KFWeaponPickup>   fWPK;    // weapon pickup class, used to check its cost

    StopHitCamEffects();

    if ( bDeleteMe || Level.bLevelChange || Level.Game == None )
        return; // already destroyed, or level is being cleaned up

    if ( DamageType.default.bCausedByWorld && (Killer == None || Killer == Controller) && LastHitBy != None )
        Killer = LastHitBy;

    // mutator hook to prevent deaths
    // WARNING - don't prevent bot suicides - they suicide when really needed
    if (Level.Game.PreventDeath(self, Killer, damageType, HitLocation))
    {
        Health = max(Health, 1); //mutator should set this higher
        return;
    }

    // Turn off the auxilary collision when the player dies
    if (  AuxCollisionCylinder != none )
    {
        AuxCollisionCylinder.SetCollision(false,false,false);
    }

    // Hack fix for team-killing.
    if(KFPlayerReplicationInfo(PlayerReplicationInfo) != None)
    {
        FP = KFPlayerReplicationInfo(PlayerReplicationInfo).GetBlamePawn();

        if (FP != None)
        {
            ForEach DynamicActors(Class'Projectile',PP)
            {
                if( PP.Instigator==Self )
                    PP.Instigator = FP;
            }
        }
    }

    D = Spawn(Class'PlayerDeathMark');
    if( D!=None )
        D.Velocity = Velocity;

    Health = Min(0, Health);

    // first attempt to toss your more valuable weapon
    if (DrivenVehicle == None || DrivenVehicle.bAllowWeaponToss)
    {
        if (Controller != None)
            Controller.LastPawnWeapon = Weapon.Class;

        TossVel = Vector(GetViewRotation()) * 450.0f + Vect(0, 0, 300);
        //TossVel = TossVel * 500.0f + Vect(0, 0, 250);//((Velocity Dot TossVel) + 300) + Vect(0,0,200);

        for (W = Inventory; W != none; W = W.Inventory)
        {
            if (KFWeapon(W) != none && Frag(W) == none && PipeBombExplosive(W) == none && ATMineExplosive(W) == none)
            {
                fWPK = class<KFWeaponPickup>(KFWeapon(W).default.PickupClass);

                fTempCost = fWPK.default.Cost;

                // husk gun default cost scaling
                if (class<HuskGunPickup>(fWPK) != none)
                    fTempCost *= class'FVeterancyTypes'.Default.huskDiv;

                if (fWPK != none && fTempCost >= Price)
                {
                    // when in doubt and the cost is the same, prioritize a perked weapon
                    if (fTempCost == Price && (Knife(W) == none || fWPK.default.CorrespondingPerkIndex != fPerkIndex))
                    {
                        //warn("Skip:"@W@"Perk Index:"@fPerkIndex);
                        continue;
                    }

                    Price   = fTempCost;
                    Weapon  = Weapon(W);
                }
            }
        }

        if (Weapon != None)
        {
            //warn("Toss:"@Weapon);
            Weapon.HolderDied();
            TossWeapon(TossVel);
        }
    }

    if (DrivenVehicle != None)
    {
        Velocity = DrivenVehicle.Velocity;
        DrivenVehicle.DriverDied();
    }

    if (Controller != None)
    {
        //warn("KILLED CALL FROM PAWN");
        Level.Game.Killed(Killer, Controller, self, damageType);
        Controller.WasKilledBy(Killer);
    }

    else
        Level.Game.Killed(Killer, Controller(Owner), self, damageType);

    DrivenVehicle = None;

    if (Killer != None)
        TriggerEvent(Event, self, Killer.Pawn);

    else TriggerEvent(Event, self, None);

    // make sure to untrigger any triggers requiring player touch
    if (IsPlayerPawn() || WasPlayerPawn())
    {
        PhysicsVolume.PlayerPawnDiedInVolume(self);
        ForEach TouchingActors(class'Trigger',T)
            T.PlayerToucherDied(self);

        // event for HoldObjectives
        ForEach TouchingActors(class'NavigationPoint', N)
            if ( N.bReceivePlayerToucherDiedNotify )
                N.PlayerToucherDied( Self );
    }
    // remove powerup effects, etc.
    RemovePowerups();

    Velocity.Z *= 1.3;

    // Make attached explosives blow up when this pawn dies
    for (i=0; i<Attached.length; i++)
    {
        if(SealSquealProjectile(Attached[i])!=None)
            SealSquealProjectile(Attached[i]).HandleBasePawnDestroyed();
    }

    if (IsHumanControlled())
        PlayerController(Controller).ForceDeathUpdate();

    NetUpdateFrequency = Default.NetUpdateFrequency;
    PlayDying(DamageType, HitLocation);

    if (!bPhysicsAnimUpdate && !IsLocallyControlled())
        ClientDying(DamageType, HitLocation);
}

// attempt to fix third person grenade toss anim with custom weapons
simulated function HandleNadeThrowAnim()
{
    if (Weapon != none)
    {
        if( AK47AssaultRifle(Weapon) != none )
            SetAnimAction('Frag_AK47');

        else if (Bullpup(Weapon) != none || SeekerSixRocketLauncher(Weapon) != none)
            SetAnimAction('Frag_Bullpup');

        else if (Crossbow(Weapon) != none)
            SetAnimAction('Frag_Crossbow');

        else if (Deagle(Weapon) != none || Magnum44Pistol(Weapon) != none || MK23Pistol(Weapon) != none)
            SetAnimAction('Frag_Single9mm');

        else if (KFWeapon(Weapon) != none && KFWeapon(Weapon).bDualWeapon)
            SetAnimAction('Frag_Dual9mm');

        else if (FlameThrower(Weapon) != none)
            SetAnimAction('Frag_Flamethrower');

        else if (Axe(Weapon) != none || DwarfAxe(Weapon) != none)
            SetAnimAction('Frag_Axe');

        else if (Chainsaw(Weapon) != none)
            SetAnimAction('Frag_Chainsaw');

        else if (Katana(Weapon) != none || ClaymoreSword(Weapon) != none)
            SetAnimAction('Frag_Katana');

        else if (Knife(Weapon) != none)
            SetAnimAction('Frag_Knife');

        else if (Machete(Weapon) != none)
            SetAnimAction('Frag_Knife');

        else if (Syringe(Weapon) != none)
            SetAnimAction('Frag_Syringe');

        else if (Welder(Weapon) != none)
            SetAnimAction('Frag_Syringe');

        else if (BoomStick(Weapon) != none)
            SetAnimAction('Frag_HuntingShotgun');

        else if (LAW(Weapon) != none)
            SetAnimAction('Frag_LAW');

        else if (Shotgun(Weapon) != none || BenelliShotgun(Weapon) != none || Trenchgun(Weapon) != none)
            SetAnimAction('Frag_Shotgun');

        else if (Winchester(Weapon) != none)
            SetAnimAction('Frag_Winchester');

        else if (Single(Weapon) != none)
            SetAnimAction('Frag_Single9mm');

        else if (M14EBRBattleRifle(Weapon) != none || SPSniperRifle(Weapon) != none)
            SetAnimAction('Frag_M14');

        else if (SCARMK17AssaultRifle(Weapon) != none)
            SetAnimAction('Frag_SCAR');

        else if (AA12AutoShotgun(Weapon) != none || FNFAL_ACOG_AssaultRifle(Weapon) != none || SPAutoShotgun(Weapon) != none)
            SetAnimAction('Frag_AA12');

        else if (MP5MMedicGun(Weapon) != none || KSGShotgun(Weapon) != none)
            SetAnimAction('Frag_MP5');

        else if (MP7MMedicGun(Weapon) != none)
            SetAnimAction('Frag_MP7');

        else if (M7A3MMedicGun(Weapon) != none || KrissMMedicGun(Weapon) != none)
            SetAnimAction('Frag_Kriss');

        else if (PipeBombExplosive(Weapon) != none)
            SetAnimAction('Frag_PipeBomb');

        else if (M79GrenadeLauncher(Weapon) != none || ZEDMKIIWeapon(Weapon) != none)
            SetAnimAction('Frag_M79');

        else if (M32GrenadeLauncher(Weapon) != none)
            SetAnimAction('Frag_M32_MGL');

        else if (SealSquealHarpoonBomber(Weapon) != none)
            SetAnimAction('Frag_IJC_SealSqueal');

        else if (M4203AssaultRifle(Weapon) != none || M99SniperRifle(Weapon) != none)
            SetAnimAction('Frag_M4203');

        else if (M4AssaultRifle(Weapon) != none || MKb42AssaultRifle(Weapon) != none)
            SetAnimAction('Frag_M4');

        else if (HuskGun(Weapon) != none)
            SetAnimAction('Frag_HuskGun');

        else if (ZEDGun(Weapon) != none)
            SetAnimAction('Frag_Zed');

        else if (NailGun(Weapon) != none)
            SetAnimAction('Frag_Bullpup');

        else if (SPThompsonSMG(Weapon) != none || ThompsonDrumSMG(Weapon) != none)
            SetAnimAction('Frag_IJC_spThompson_Drum');

        else if (ThompsonSMG(Weapon) != none)
            SetAnimAction('Frag_Thompson');

        else if (Scythe(Weapon) != none)
            SetAnimAction('Frag_Scythe');

        else if (Crossbuzzsaw(Weapon) != none)
            SetAnimAction('Frag_Cheetah');

        else if (FlareRevolver(Weapon) != none)
            SetAnimAction('Frag_Single9mm');

        else
            SetAnimAction('Frag_Knife');
    }

    else
        SetAnimAction('Frag_Knife');
}

// used to edit how we order the weapons
function bool AddInventory(inventory NewItem)
{
    if (!FalkAddInventory(NewItem))
        return false;

    if (KFWeapon(NewItem) != none)
        CurrentWeight += KFWeapon(NewItem).Weight;

    return true;
}

// don't remove weight on empty pipebomb destroy
function DeleteInventory(inventory Item)
{
    if (Item == none)
        return;

    if (PipeBombExplosiveBaseFalk(Item) != none && (PipeBombExplosiveBaseFalk(Item).Weight <= 0 || PipeBombExplosiveBaseFalk(Item).SellValue <= 0 || PipeBombExplosiveBaseFalk(Item).fBackupSellValue > 0) &&
            (ATMineExplosiveBaseFalk(Item).Weight <= 0 || ATMineExplosiveBaseFalk(Item).SellValue <= 0 || ATMineExplosiveBaseFalk(Item).fBackupSellValue > 0))
    {
        Super(xPawn).DeleteInventory(Item);
        return;
    }

    Super.DeleteInventory(Item);
}

// edited vanilla function that asks the controller how to order weapons
function bool FalkAddInventory(inventory NewItem)
{
    local inventory Inv;
    local actor Last, Prev;
    local bool bAddedInOrder;

    Last = self;

    // The item should not have been destroyed if we get here.
    if (NewItem == None)
        warn("tried to add none inventory to "$self);

    NewItem.SetOwner(Self);
    NewItem.NetUpdateTime = Level.TimeSeconds - 1;

    // order weapons based on real priority
    if (Weapon(NewItem) != None && Controller != None)
    {
        Prev = self;

        for (Inv=Inventory; Inv!=None; Inv=Inv.Inventory)
        {
            if ((Inv.InventoryGroup == NewItem.InventoryGroup) && (Weapon(Inv) != None))
            {
                if (FPCServ(Controller) != none)
                {
                    if((FPCServ(Controller).FalkRateWeapon(Weapon(Inv)) < FPCServ(Controller).FalkRateWeapon(Weapon(NewItem))))
                    {
                        bAddedInOrder = true;
                        break;
                    }
                }

                else if((Controller.RateWeapon(Weapon(Inv)) < Controller.RateWeapon(Weapon(NewItem))))
                {
                    bAddedInOrder = true;
                    break;
                }
            }

            else if ((Weapon(Prev) != None) && (Weapon(Prev).InventoryGroup == NewItem.InventoryGroup))
            {
                bAddedInOrder = true;
                break;
            }

            if (!bAddedInOrder)
                Prev = Inv;
        }

        if (bAddedInOrder)
        {
            NewItem.Inventory = Prev.Inventory;
            Prev.Inventory = NewItem;
            Prev.NetUpdateTime = Level.TimeSeconds - 1;
        }
    }

    if (!bAddedInOrder)
    {
        for (Inv=Inventory; Inv!=None; Inv=Inv.Inventory)
        {
            if (Inv == NewItem)
                return false;

            Last = Inv;
        }

        // Add to back of inventory chain (so minimizes net replication effect).
        NewItem.Inventory = None;
        Last.Inventory = NewItem;
        Last.NetUpdateTime = Level.TimeSeconds - 1;
    }

    if (Controller != None)
        Controller.NotifyAddInventory(NewItem);

    return true;
}

// bloat vomit damage multiplier
/*function float FGetBloatVomitMulti()
  {
  if (Level.Game.GameDifficulty >= 7.0)       // HOE
  return 2.0;

  else if (Level.Game.GameDifficulty >= 5.0)  // suicidal
  return 2.0;

  else if (Level.Game.GameDifficulty >= 4.0)  // hard
  return 1.5;

  return 1.0;
  }*/

// since this was unused, this is now used by the husk gun fire to set the charging state on the pawn
function bool PerformDodge(eDoubleClickDir DoubleClickMove, vector Dir, vector Cross)
{

    if (HuskGun(Weapon) != none)
    {
        if (Dir.x == 689)
        {
            //warn("DODGE SET:"@Level.TimeSeconds);
            fChargingHuskGun = True;
        }

        else if (Dir.x == -689)
        {
            //warn("DODGE UNSET:"@Level.TimeSeconds);
            fChargingHuskGun = False;
        }
    }

    Return False;
}

// Hopefully drop the husk gun without overriding it
function DropHuskGun(vector StartLocation)
{
    //local int                  m;
    local Pickup               Pickup;
    local byte                 fAttempt;
    local vector               fTempPos;
    local vector               Direction;

    if (!Weapon.bCanThrow)
        return;

    Direction = vector(GetViewRotation());

    Weapon.StopFire(0);

    Pickup    = Spawn(Weapon.PickupClass,,, StartLocation);

    // Try to spawn remaining clusters
    while (Pickup == None && fTry < fMaxTry)
    {
        fTry++;
        fAttempt = 0;

        while (Pickup == None && fAttempt < 27)
        {
            fAttempt++; // we don't even test 0 since we're here for a reason

            if (fAttempt >= 27)
            {
                //warn("FAIL"@fTry);
                fACZRetryLoc += fCZRetryLoc;
                fACXRetryLoc += fCXRetryLoc;
                fACYRetryLoc += fCYRetryLoc;
            }

            else if (fAttempt >= 18)
            {
                fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
                //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
            }

            else if (fAttempt >= 9)
            {
                fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
                //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
            }

            else
            {
                fTempPos = FClusterQuad(StartLocation, fAttempt);
                //warn("First:"@fAttempt@"Location:"@fTempPos);
            }

            Pickup = Spawn(Weapon.PickupClass,,, fTempPos);
        }
    }

    fTry         = 0;
    fACZRetryLoc = fCZRetryLoc;
    fACXRetryLoc = fCXRetryLoc;
    fACYRetryLoc = fCYRetryLoc;


    if (Pickup != None)
    {
        Weapon.ClientWeaponThrown();
        Weapon.DetachFromPawn(self);

        Pickup.InitDroppedPickupFor(Weapon);
        Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

        if (Health > 0)
            WeaponPickup(Pickup).bThrown = true;

        Weapon.Destroy();
    }

    // we failed to drop this weapon on death, award us some cash instead
    else if (KFWeapon(Weapon) != none && Health <= 0 && PlayerReplicationInfo != none)
    {
        PlayerReplicationInfo.Score += KFWeapon(Weapon).SellValue;
        FalkSetDosh();
    }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
    Switch (fAttempt)
    {
        case 0:
            return fSLocation;

        case 1:
            return fSLocation + fACXRetryLoc;

        case 2:
            return fSLocation - fACXRetryLoc;

        case 3:
            return fSLocation + fACYRetryLoc;

        case 4:
            return fSLocation - fACYRetryLoc;

        case 5:
            return fSLocation + fACXRetryLoc + fACYRetryLoc;

        case 6:
            return fSLocation - fACXRetryLoc + fACYRetryLoc;

        case 7:
            return fSLocation + fACXRetryLoc - fACYRetryLoc;

        case 8:
            return fSLocation - fACXRetryLoc - fACYRetryLoc;
    }
}

// reduced fall damage
function TakeFallingDamage()
{
    local float Shake, EffectiveSpeed;
    local float UsedMaxFallSpeed;

    UsedMaxFallSpeed = MaxFallSpeed;

    // Higher max fall speed in low grav so that weapons that push you around don't cause you to die
    if (Instigator != none && Instigator.PhysicsVolume.Gravity.Z > class'PhysicsVolume'.default.Gravity.Z)
        UsedMaxFallSpeed *= 2.0;

    if (Velocity.Z < -0.5 * UsedMaxFallSpeed)
    {
        if (Role == ROLE_Authority)
        {
            MakeNoise(1.0);

            if (Velocity.Z < -1 * UsedMaxFallSpeed)
            {
                EffectiveSpeed = Velocity.Z;

                if (TouchingWaterVolume())
                    EffectiveSpeed = FMin(0, EffectiveSpeed + 100);

                if (EffectiveSpeed < -1 * UsedMaxFallSpeed)
                    TakeDamage(-60 * (EffectiveSpeed + UsedMaxFallSpeed) / UsedMaxFallSpeed, None, Location, vect(0,0,0), class'Fell');
            }
        }

        if (Controller != None)
        {
            Shake = FMin(1, -1 * Velocity.Z/MaxFallSpeed);
            Controller.DamageShake(Shake);
        }
    }

    else if (Velocity.Z < -1.4 * JumpZ)
        MakeNoise(0.5);
}

// groundwork to extend commando zed time skill to the whole team
function bool ShowStalkers()
{
    if (fSStalkerInTeam && fClientInZedTime)
        return True;

    return Super.ShowStalkers();
}

// groundwork to extend commando zed time skill to the whole team by returning the jolly number
function float GetStalkerViewDistanceMulti()
{
    if (fSStalkerInTeam && fClientInZedTime)
        return -689;

    return Super.GetStalkerViewDistanceMulti();
}

// override called by the pat so he stays invisible
function bool VanillaShowStalkers()
{
    return Super.ShowStalkers();
}

// override called by the pat to become visible from normal range only
function float VanillaGetStalkerViewDistanceMulti()
{
    return Super.GetStalkerViewDistanceMulti();
}

// return the value of the perked weapon
function float FalkGetPerkedWeaponValue()
{
    if (Level.Game.GameDifficulty >= 8.0)
        return 50;

    else if (Level.Game.GameDifficulty >= 7.0)
        return 100;

    else if (Level.Game.GameDifficulty >= 5.0)
        return 150;

    else if (Level.Game.GameDifficulty >= 4.0)
        return 200;

    return 250;
}

// prevent jump while grabbed at bloodbath
function bool DoJump(bool bUpdating)
{
    if (fJumpDisabled)
        return false;

    EnableMovement();

    return Super.DoJump(bUpdating);
}

// don't let this pawn move for a certain amount of time and optionally set fJumpDisabled based on the difficulty
function DisableMovement(float DisableDuration)
{
    StopDisabledTime = Level.TimeSeconds + DisableDuration;
    bMovementDisabled = true;

    if (Role == ROLE_Authority && Level.Game.GameDifficulty >= 8.0)
        fJumpDisabled = true;
}

// enable pawn movement
function EnableMovement()
{
    bMovementDisabled = false;
    fJumpDisabled     = false;
}

// return true if we should emit a pickup sound
function bool FShouldPlayPickupSound()
{
    if (Level.TimeSeconds > fNextPickupSound)
    {
        fNextPickupSound = Level.TimeSeconds + fPickupSoundCooldown;
        return true;
    }

    return false;
}

defaultproperties
{
    WeightSpeedModifier=0.005
    BaseMeleeIncrease=0.1
    fTargetMeleeIncrease=0.1
    fSpeedScaleTimer=0.01
    fScaleStep=0.08
    fHealthMax=100
    fHealth=100
    fShieldMax=100
    fSingleHealMulti=1.5
    fMinHealBoost=20
    fAddDoshTimer=0.5
    CrawlerLeapCD=0.4
    BloatVomitCD=0.75
    PatStandardCD=0.4
    PatRadialCD=0.2
    FleshPoundCD=0.09
    fAcidDamage=0.60
    fBurnDamage=0.60
    fHuskDamage=0.20
    fStoredSyringeAmmo=-1
    fWeaponAddTime=5.0
    fFlashlightDrain=1.2
    fSpitfireReload=3
    fStingerReload=3
    fMercyReload=3
    fWeapSwapTick=0.2
    fSyringeSwapLeft=0.9
    fSyringeSwapRight=1.5
    fDQuickHealTick=0.2
    fWeapSwitch=69
    fQHWeapSwitch=69
    fInvGroupSwitch=69
    fCreateInvTick=0.1
    fShouldClearInv=True
    fWeapSwitchCD=0.28
    fNadeCD=0.2
    fHealTick=0.05
    fIRCheckTick=0.2
    fCrazyFiveNadeTick=0.35
    RequiredEquipment[0]="Falk689WeaponsFix.SingleFalk";
    RequiredEquipment[1]="Falk689WeaponsFix.KnifeFalk";
    RequiredEquipment[2]="Falk689WeaponsFix.FragFalk";
    RequiredEquipment[3]="Falk689WeaponsFix.ArmorWelderFalk";
    RequiredEquipment[4]="Falk689WeaponsFix.SyringeFalk";
    MinTimeBetweenPainSounds=5.0
    fShouldTakeDamage=True
    fMaxTry=3
    fFlareMsgCD=3.0
    fCZRetryLoc=(Z=25.000000)
    fCXRetryLoc=(X=25.000000)
    fCYRetryLoc=(Y=25.000000)
    fACZRetryLoc=(Z=25.000000)
    fACXRetryLoc=(X=25.000000)
    fACYRetryLoc=(Y=25.000000)
    fHardRPCostMulti=0.40            // was 0.35 in patch 13
    fSuicidalRPCostMulti=0.30        // was 0.24 in patch 13
    fHOERPCostMulti=0.20             // was 0.125 in patch 13
    fBBRPCostMulti=0.10              // was 0.125 in patch 13
    fKevlarCost=250
    fSirenSoundCooldown=3.05
    fPickupSoundCooldown=0.35
    fSirenDamageSounds[0]=sound'LairSounds_S.SirenBloodbathSkill.SirenBloodbathSkill1'
    fSirenDamageSounds[1]=sound'LairSounds_S.SirenBloodbathSkill.SirenBloodbathSkill2'
    fSirenDamageSounds[2]=sound'LairSounds_S.SirenBloodbathSkill.SirenBloodbathSkill3'
    fSirenDamageSounds[3]=sound'LairSounds_S.SirenBloodbathSkill.SirenBloodbathSkill4'
    fNadeReloadDelay=0.1
}
