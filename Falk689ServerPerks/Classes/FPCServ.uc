class FPCServ extends ServerPerks.KFPCServ;

// achievements
var bool medicAchievement;
var bool supportAchievement;
var bool sharpAchievement;
var bool commandoAchievement;
var bool zerkAchievement;
var bool firebugAchievement;
var bool demoAchievement;
var bool artiAchievement;

// other stuff
var bool  fBuyMenuOpen;                // is the buy menu open?
var bool  snekBlock;                   // snek stuff
var bool  fReadyToPlay;                // are we ready to play?
var bool  fInitRep;                    // block replication initializations before we're ready
var byte  fVoiceMessages;              // how many voice messages we spammed during the last block
var float fStoredGroundSpeed;          // ground speed used to control speed changes
var int   fMinNetSpeed;                // minimum net speed, don't lower us under this

var() string FTraderString;

replication
{
    reliable if(Role == ROLE_Authority)
        medicAchievement, supportAchievement, sharpAchievement, commandoAchievement, zerkAchievement, firebugAchievement, demoAchievement, artiAchievement, snekBlock,
        FalkClientEnterZedTime, FalkClientExitZedTime, FalkCloseInventory, fReadyToPlay, fVoiceMessages, fStoredGroundSpeed, fSetNetSpeed;

    reliable if(Role != ROLE_Authority)
        FalkReadyToPlay;
}

// nope
function KFSwitchToBestWeapon();
function KFClientSwitchToBestWeapon();
exec function SwitchToBestWeapon(){}

// nerfed lorex and major
exec function GetWeapon(class<Weapon> NewWeaponClass)
{
    local FHumanPawn FHP;
    local int result;

    FHP = FHumanPawn(Pawn);

    if (FHP != none)
        result = FHP.FCanSwitchWeapon();

    // buffer the switch
    if (result == 1)
    {
        FHP.SetBufferedWeaponClass(NewWeaponClass);
        return;
    }

    // prevent the switch
    if (result != 0)
        return;

    // clean up after yourself like a well-behaved controller
    if (FHP != none)
        FHP.FGetWeaponCalled();

    //warn("GETWEAPON"@FHP);

    Super.GetWeapon(NewWeaponClass);
}

// replicated method to set the net speed
simulated function fSetNetSpeed(int value)
{
    ConsoleCommand("netspeed"@value);
    ConsoleCommand("set player ConfiguredInternetSpeed"@value);
    ConsoleCommand("set player ConfiguredInternetSpeed"@value+10000);
}

// edited trader menu, and close the fockin inventory on show
function ShowBuyMenu(string wlTag, float maxweight)
{
    fBuyMenuOpen = True;

    StopForceFeedback();

    FalkCloseInventory();

    ClientOpenMenu(string(class'FGUIBuyMenu'),,wlTag,string(maxweight));
}

// just spawn our replication
simulated event PostBeginPlay()
{
    Super.PostBeginPlay();

    if (PlayerReplicationInfo != none)
    {
        if (!PlayerReplicationInfo.bOnlySpectator && PlayerReplicationInfo.Team != None)
            PlayerReplicationInfo.Team.RemoveFromTeam(self);

        PlayerReplicationInfo.Destroy();
    }

    if (!bDeleteMe && bIsPlayer && (Level.NetMode != NM_Client))
    {
        fInitRep = True;
        PlayerReplicationInfo = Spawn(Default.PlayerReplicationInfoClass, Self,,vect(0,0,0),rot(0,0,0));
        InitPlayerReplicationInfo();
        fInitRep = False;
    }
}

// don't init the replication unless 
function InitPlayerReplicationInfo()
{
    if (fInitRep)
        Super.InitPlayerReplicationInfo();
}

// set proper dosh amount on connect
simulated event PostNetBeginPlay()
{
    local Falk689GameTypeBase Flk;
    local string SteamID;
    local int    fDosh;

    Super.PostNetBeginPlay();

    if (Level.NetMode == NM_DedicatedServer)
    {
        Flk = Falk689GameTypeBase(Level.Game);

        if (Flk != none)
        {
            SteamID = GetPlayerIDHash();
            fDosh   = Flk.FalkGetDosh(SteamID);

            if (fDosh >= 0)
                PlayerReplicationInfo.Score = fDosh;
        }
    }

    if (Player != none && Player.CurrentNetSpeed < fMinNetSpeed)
    {
        fSetNetSpeed(64000);
        SetNetSpeed(fMinNetSpeed);
    }
}

// reset spawnedthiswave to false on become active
function BecomeActivePlayer()
{
    local Invasion fInv;
    local Falk689GameTypeBase Flk;

    if (Role < ROLE_Authority)
        return;

    if (!Level.Game.AllowBecomeActivePlayer(self))
        return;

    if (Player != none && Player.CurrentNetSpeed < fMinNetSpeed)
    {
        fSetNetSpeed(64000);
        SetNetSpeed(fMinNetSpeed);
    }

    fInv = Invasion(Level.Game);
    Flk  = Falk689GameTypeBase(Level.Game);

    bBehindView = False;
    FixFOV();
    SetViewDistance();
    ServerViewSelf();
    PlayerReplicationInfo.bOnlySpectator = False;
    Level.Game.NumSpectators--;
    Level.Game.NumPlayers++;
    PlayerReplicationInfo.Reset();
    Adrenaline = 0;

    if (fInv != none && !fInv.bWaveInProgress)
    {
        bReadyToStart = true;
        bSpawnedThisWave = False;
    }

    BroadcastLocalizedMessage(Level.Game.GameMessageClass, 1, PlayerReplicationInfo);

    if (Level.Game.bTeamGame)
        Level.Game.ChangeTeam(self, Level.Game.PickTeam(int(GetURLOption("Team")), None), false);

    if (!Level.Game.bDelayedStart)
    {
        // start match, or let player enter, immediately
        Level.Game.bRestartLevel = false;  // let player spawn once in levels that must be restarted after every death
        if (Level.Game.bWaitingToStartMatch)
            Level.Game.StartMatch();
        else
            Level.Game.RestartPlayer(PlayerController(Owner));
        Level.Game.bRestartLevel = Level.Game.Default.bRestartLevel;
    }

    else
    {
        if (fInv != none && !fInv.bWaveInProgress)
        {
            PlayerReplicationInfo.bOutOfLives = false;
            PlayerReplicationInfo.NumLives    = 0;
            Pawn                              = none;
        }

        GotoState('PlayerWaiting');

        if (fInv != none && !fInv.bWaveInProgress)
        {
            SetViewTarget(self);
            ClientSetBehindView(false);
            bBehindView = False;
        }
    }

    ClientBecameActivePlayer();

    // force spawn in trader time on join
    if (Flk != none)
    {
        if (!Flk.fGetSpawnedThisWave(self))
        {
            ServerReStartPlayer();
            ClientSetViewTarget(Pawn);
        }

        Flk.fSetSpawnedThisWave(self);
    }
}

state Dead
{
    // sometimes we can't access the gametype here in Timer and, well, just don't do shit instead of printing an error - Falk689
    function Timer()
    {
        if (Role == ROLE_Authority && Level != none && Level.Game != none && Level.Game.GameReplicationInfo.bMatchHasBegun && ((KFStoryGameInfo(Level.Game) != none && KFStoryGameInfo(Level.Game).IsTraderTime()) || (KFGameType(Level.Game) != none && !KFGameReplicationInfo(GameReplicationInfo).bWaveInProgress)))
        {
            SetViewTarget(self);
            ClientSetBehindView(false);
            bBehindView = False;
            ClientSetViewTarget(Pawn);
            PlayerReplicationInfo.bOutOfLives = false;
            Pawn = none;
            ServerReStartPlayer();
        }

        super(KFPC).Timer();
    }
}

// don't hide hands in screenshot mode
exec function ToggleScreenShotMode()
{
    if (myHUD.bCrosshairShow)
    {
        myHUD.bCrosshairShow = false;
        myHUD.bHideHUD = true;
        SetWeaponHand("Right");
    }

    // return to normal
    else
    {
        myHUD.bCrosshairShow = true;
        SetWeaponHand("Right");
        myHUD.bHideHUD = false;
    }
}

// testing stuff, skip trader
exec function FalkSkipTrader()
{
    local Falk689GameTypeBase Flk;

    Flk = Falk689GameTypeBase(Level.Game);

    if (Flk != None)
        Flk.FalkSkipTrader();
}

// block and allow damage again
function SetTakeDamage(bool NextState)
{
    local FHumanPawn F;

    F = FHumanPawn(Pawn);

    if (F != None)
    {
        //warn("SETTING TO:"@NextState@"Time:"@Level.TimeSeconds);
        F.fShouldTakeDamage = NextState;
    }
}

// snek block, don't talk to yourself while healing
function SendVoiceMessage(PlayerReplicationInfo Sender, PlayerReplicationInfo Recipient, name messagetype, byte messageID, name broadcasttype, optional Pawn soundSender, optional vector senderLocation)
{
    local Controller P;
    local KFPlayerController KFPC;

    if (!AllowVoiceMessage(MessageType))
    {
        snekBlock = False;
        return;
    }

    if (Sender == none)
        Sender = PlayerReplicationInfo;

    for (P=Level.ControllerList; P!=none; P=P.NextController)
    {
        KFPC = KFPlayerController(P);

        if (KFPC != None)
        {
            // Don't allow dead people to talk and snek block while healing
            if (Pawn != none && (!snekBlock || P.Pawn != Pawn))
                KFPC.ClientLocationalVoiceMessage(Sender, Recipient, messagetype, messageID, soundSender, senderLocation);
        }
    }

    snekBlock = False;
}


// prevents switching automatically to the husk gun on pickup and hopefully solves a weird bug with nades
simulated function float RateWeapon(Weapon w)
{
    local class<FVeterancyTypes>  fVet;
    local KFPlayerReplicationInfo KFPRI;

    if (HuskGun(w) != none)
        return -100;

    else if (Frag(w) != none)
        return -200;

    KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

    if (w.InventoryGroup != 1 && w.InventoryGroup != 5 && KFPRI != none)
    {
        fVet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

        if (fVet != none && w.PickupClass                                      != none &&
                class<KFWeaponPickup>(w.PickupClass)                                != none &&
                class<KFWeaponPickup>(w.PickupClass).Default.CorrespondingPerkIndex == fVet.Default.PerkIndex)
        {
            return w.Default.Priority + 1000;
        }
    }

    return w.Default.Priority;
}

// same as above but without the fixes, used on buy
simulated function float FalkRateWeapon(Weapon w)
{
    local class<FVeterancyTypes>  fVet;
    local KFPlayerReplicationInfo KFPRI;

    KFPRI = KFPlayerReplicationInfo(PlayerReplicationInfo);

    if (w.InventoryGroup != 1 && w.InventoryGroup != 5 && KFPRI != none)
    {
        fVet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

        if (fVet != none && w.PickupClass                                      != none &&
                class<KFWeaponPickup>(w.PickupClass)                                != none &&
                class<KFWeaponPickup>(w.PickupClass).Default.CorrespondingPerkIndex == fVet.Default.PerkIndex)
        {
            return w.Default.Priority + 1000;
        }
    }

    return w.Default.Priority;
}

// don't tweak mouse sensitivity for the spark gun
function float GetMouseModifier()
{
    if (Pawn != none && Pawn.Weapon != none && SparkGunBaseFalk(Pawn.Weapon) != none)
        return -1.0;

    return Super.GetMouseModifier();
}

// don't play zed time sounds if it's a short fake one
simulated function FalkClientEnterZedTime(bool fNoSound)
{
    local FHumanPawn FHP;

    CheckZEDMessage();

    if (!fNoSound)
    {
        FHP = FHumanPawn(Pawn);

        if (FHP != none)
        {
            //warn("ENTER:"@Level.TimeSeconds);
            FHP.fClientInZedTime = True;
        }

        // if we have a weapon, play the zed time sound from it so it is higher priority and doesn't get cut off
        if (Pawn != none && Pawn.Weapon != none)
            Pawn.Weapon.PlaySound(Sound'KF_PlayerGlobalSnd.Zedtime_Enter', SLOT_Talk, 2.0,false,500.0,1.1/Level.TimeDilation,false);

        else
            PlaySound(Sound'KF_PlayerGlobalSnd.Zedtime_Enter', SLOT_Talk, 2.0,false,500.0,1.1/Level.TimeDilation,false);
    }
}

// don't play zed time sounds if it was a short fake one
simulated function FalkClientExitZedTime(bool fNoSound)
{
    local FHumanPawn FHP;

    if (!fNoSound)
    {
        FHP = FHumanPawn(Pawn);

        if (FHP != none)
        {
            //warn("EXIT:"@Level.TimeSeconds);
            FHP.fClientInZedTime = False;
        }

        if (Pawn != none && Pawn.Weapon != none)
            Pawn.Weapon.PlaySound(Sound'KF_PlayerGlobalSnd.Zedtime_Exit', SLOT_Talk, 2.0,false,500.0,1.1/Level.TimeDilation,false);

        else
            PlaySound(Sound'KF_PlayerGlobalSnd.Zedtime_Exit', SLOT_Talk, 2.0,false,500.0,1.1/Level.TimeDilation,false);
    }
}

// close that fockin inventory pls
simulated function FalkCloseInventory()
{
    if (HUDKillingFloor(myHud) != none)
        HUDKillingFloor(myHud).HideInventory();
}

// client replicated function that should set this to true regardless
function FalkReadyToPlay()
{
    fReadyToPlay = True;
}

// close the trader menu (or literally any menu) while updating the var on the server
function fCloseMenu()
{
    fBuyMenuOpen = False;
    ClientCloseMenu(True);
}

// set my ready var to true here and try to set the netspeed to 64000
event ClientCloseMenu(optional bool bCloseAll, optional bool bCancel)
{
    fReadyToPlay = True;
    FalkReadyToPlay();
    Super.ClientCloseMenu(bCloseAll, bCancel);
}

// be a bit more permissive on voice spam
function bool AllowVoiceMessage(name MessageType)
{

    if (Level.TimeSeconds - OldMessageTime > 5)
        fVoiceMessages = 0;

    else
    {
        if (fVoiceMessages > 1)
            return false;

        fVoiceMessages++;
    }

    OldMessageTime = Level.TimeSeconds;
    return true;
}

// yeah but no, just allow them to talk freely
function bool AllowTextMessage(string Msg)
{
    return true;
}

// testing stuff with the client play sound, to play them from the weapon for increased priority
simulated function ClientPlaySound(sound ASound, optional bool bVolumeControl, optional float inAtten, optional ESoundSlot slot)
{
    local float atten;

    atten = 1.0;

    if (bVolumeControl)
        atten = FClamp(inAtten,0,2);

    if (Pawn != none && Pawn.Weapon != none)
        Pawn.Weapon.PlaySound(ASound, slot, atten,,,,false);

    else if (ViewTarget != None)
        ViewTarget.PlaySound(ASound, slot, atten,,,,false);

}

// Just say like a normal person
exec function TeamSay(string Msg)
{
    say(Msg);
}

// Why does this exist
function ServerTeamSay(string Msg)
{
    ServerSay(Msg);
}

// no team say
function bool AllowTextToSpeech(PlayerReplicationInfo PRI, name Type)
{
    if (bNoTextToSpeechVoiceMessages || (PRI == None))
        return false;

    if (Type == 'Say' || Type == 'TeamSay')
    {
        //if _RO_
        if (PRI.bSilentAdmin)
            return true;
        //end _RO_
        if (PRI.bAdmin || ((GameReplicationInfo != None) && !GameReplicationInfo.bTeamGame) || (PRI == PlayerReplicationInfo))
            return true;
        if (IsInState('GameEnded') || IsInState('RoundEnded'))
            return true;

        return !bOnlySpeakTeamText;
    }
    return false;
}

event TeamMessage(PlayerReplicationInfo PRI, coerce string S, name Type)
{
    local string c;

    // Wait for player to be up to date with replication when joining a server, before stacking up messages
    if (Level.NetMode == NM_DedicatedServer || GameReplicationInfo == None)
        return;

    if(AllowTextToSpeech(PRI, Type))
        TextToSpeech(S, TextToSpeechVoiceVolume);

    if (Type == 'TeamSayQuiet')
        Type = 'Say';

    if (myHUD != None)
        myHUD.Message(PRI, c$S, Type);

    if ((Player != None) && (Player.Console != None))
    {
        if (PRI!=None)
        {
            if (PRI.Team!=None && GameReplicationInfo.bTeamGame)
            {
                if (PRI.Team.TeamIndex==0)
                    c = chr(27)$chr(200)$chr(1)$chr(1);

                else if (PRI.Team.TeamIndex==1)
                    c = chr(27)$chr(125)$chr(200)$chr(253);
            }

            S = PRI.PlayerName$": "$S;
        }

        Player.Console.Chat( c$s, 6.0, PRI );
    }
}

// don't cap us under a specified value
function ClientCapBandwidth(int Cap)
{
    local int UsedCap;

    UsedCap = Cap;

    if (UsedCap < fMinNetSpeed)
        UsedCap = fMinNetSpeed;

    ClientCap = UsedCap;

    fSetNetSpeed(64000);

    if ((Player != None) && (Player.CurrentNetSpeed > UsedCap))
    {
        SetNetSpeed(fMinNetSpeed);
    }
}

// cap to our min net speed
function LongClientAdjustPosition(float TimeStamp, name newState, EPhysics newPhysics, float NewLocX, float NewLocY, float NewLocZ, float NewVelX, float NewVelY, float NewVelZ, Actor NewBase, float NewFloorX, float NewFloorY, float NewFloorZ)
{
    local vector NewLocation, NewVelocity, NewFloor;
    local Actor MoveActor;
    local SavedMove CurrentMove;
    local float NewPing;

    // update ping
    if ( (PlayerReplicationInfo != None) && !bDemoOwner )
    {
        NewPing = FMin(1.5, Level.TimeSeconds - TimeStamp);

        if ( ExactPing < 0.004 )
            ExactPing = FMin(0.3,NewPing);
        else
        {
            if ( NewPing > 2 * ExactPing )
                NewPing = FMin(NewPing, 3*ExactPing);
            ExactPing = FMin(0.99, 0.99 * ExactPing + 0.008 * NewPing); // placebo effect
        }
        PlayerReplicationInfo.Ping = Min(250.0 * ExactPing, 255);
        PlayerReplicationInfo.bReceivedPing = true;

        /*if (Level.TimeSeconds - LastPingUpdate > 4)
          {
          if (bDynamicNetSpeed && (OldPing > DynamicPingThreshold * 0.001) && (ExactPing > DynamicPingThreshold * 0.001))
          {
          if ( Level.MoveRepSize < 64 )
          Level.MoveRepSize += 8;
          else if (Player.CurrentNetSpeed >= fMinNetSpeed)
          {
          SetNetSpeed(Player.CurrentNetSpeed - 1000);
          }

          OldPing = 0;
          }
          else
          OldPing = ExactPing;
          LastPingUpdate = Level.TimeSeconds;
          ServerUpdatePing(1000 * ExactPing);
          }*/
    }

    if ( Pawn != None )
    {
        if ( Pawn.bTearOff )
        {
            Pawn = None;
            if ( !IsInState('GameEnded') && !IsInState('RoundEnded') && !IsInState('Dead') )
            {
                GotoState('Dead');
            }
            return;
        }
        MoveActor = Pawn;
        if ( (ViewTarget != Pawn)
                && ((ViewTarget == self) || ((Pawn(ViewTarget) != None) && (Pawn(ViewTarget).Health <= 0))) )
        {
            bBehindView = false;
            SetViewTarget(Pawn);
        }
    }
    else
    {
        MoveActor = self;
        if( GetStateName() != newstate )
        {
            if ( NewState == 'GameEnded' || NewState == 'RoundEnded' )
                GotoState(NewState);
            else if ( IsInState('Dead') )
            {
                if ( (NewState != 'PlayerWalking') && (NewState != 'PlayerSwimming') )
                {
                    GotoState(NewState);
                }
                return;
            }
            else if ( NewState == 'Dead' )
                GotoState(NewState);
        }
    }
    if ( CurrentTimeStamp >= TimeStamp )
        return;
    CurrentTimeStamp = TimeStamp;

    NewLocation.X = NewLocX;
    NewLocation.Y = NewLocY;
    NewLocation.Z = NewLocZ;
    NewVelocity.X = NewVelX;
    NewVelocity.Y = NewVelY;
    NewVelocity.Z = NewVelZ;

    // skip update if no error
    CurrentMove = SavedMoves;
    while ( CurrentMove != None )
    {
        if ( CurrentMove.TimeStamp <= CurrentTimeStamp )
        {
            SavedMoves = CurrentMove.NextMove;
            CurrentMove.NextMove = FreeMoves;
            FreeMoves = CurrentMove;
            if ( CurrentMove.TimeStamp == CurrentTimeStamp )
            {
                FreeMoves.Clear();
                if ( ((Mover(NewBase) != None) || (Vehicle(NewBase) != None))
                        && (NewBase == CurrentMove.EndBase) )
                {
                    if ( (GetStateName() == NewState)
                            && IsInState('PlayerWalking')
                            && ((MoveActor.Physics == PHYS_Walking) || (MoveActor.Physics == PHYS_Falling)) )
                    {
                        if ( VSize(CurrentMove.SavedRelativeLocation - NewLocation) < 3 )
                        {
                            CurrentMove = None;
                            return;
                        }
                        else if ( (Vehicle(NewBase) != None)
                                && (VSize(Velocity) < 3) && (VSize(NewVelocity) < 3)
                                && (VSize(CurrentMove.SavedRelativeLocation - NewLocation) < 30) )
                        {
                            CurrentMove = None;
                            return;
                        }
                    }
                }
                else if ( (VSize(CurrentMove.SavedLocation - NewLocation) < 3)
                        && (VSize(CurrentMove.SavedVelocity - NewVelocity) < 3)
                        && (GetStateName() == NewState)
                        && IsInState('PlayerWalking')
                        && ((MoveActor.Physics == PHYS_Walking) || (MoveActor.Physics == PHYS_Falling)) )
                {
                    CurrentMove = None;
                    return;
                }
                CurrentMove = None;
            }
            else
            {
                FreeMoves.Clear();
                CurrentMove = SavedMoves;
            }
        }
        else
            CurrentMove = None;
    }
    if ( MoveActor.bHardAttach )
    {
        if ( MoveActor.Base == None )
        {
            if ( NewBase != None )
                MoveActor.SetBase(NewBase);
            if ( MoveActor.Base == None )
                MoveActor.bHardAttach = false;
            else
                return;
        }
        else
            return;
    }

    NewFloor.X = NewFloorX;
    NewFloor.Y = NewFloorY;
    NewFloor.Z = NewFloorZ;
    if ( (Mover(NewBase) != None) || (Vehicle(NewBase) != None) )
        NewLocation += NewBase.Location;

    if ( !bDemoOwner )
    {
        //if ( Pawn != None )
        //	log("Client "$Role$" adjust "$self$" stamp "$TimeStamp$" time "$Level.TimeSeconds$" location "$MoveActor.Location);
        MoveActor.bCanTeleport = false;
        if ( !MoveActor.SetLocation(NewLocation) && (Pawn(MoveActor) != None)
                && (MoveActor.CollisionHeight > Pawn(MoveActor).CrouchHeight)
                && !Pawn(MoveActor).bIsCrouched
                && (newPhysics == PHYS_Walking)
                && (MoveActor.Physics != PHYS_Karma) && (MoveActor.Physics != PHYS_KarmaRagDoll) )
        {
            MoveActor.SetPhysics(newPhysics);
            Pawn(MoveActor).ForceCrouch();
            MoveActor.SetLocation(NewLocation);
        }
        MoveActor.bCanTeleport = true;
    }
    // Hack. Don't let network change physics mode of karma stuff on the client.
    if( (MoveActor.Physics != newPhysics) && (MoveActor.Physics != PHYS_Karma) && (MoveActor.Physics != PHYS_KarmaRagDoll)
            && (newPhysics != PHYS_Karma) && (newPhysics != PHYS_KarmaRagDoll) )
    {
        MoveActor.SetPhysics(newPhysics);
    }
    if ( MoveActor != self )
        MoveActor.SetBase(NewBase, NewFloor);

    MoveActor.Velocity = NewVelocity;

    if( GetStateName() != newstate )
        GotoState(newstate);

    bUpdatePosition = true;
}

// removed some trader messages and tweaked some others
function ClientLocationalVoiceMessage(PlayerReplicationInfo Sender, PlayerReplicationInfo Recipient, name MessageType, byte MessageID, optional Pawn SenderPawn, optional vector SenderLocation)
{
    local VoicePack Voice;
    local class<VoicePack> fVoiceType;

    if (Sender != none && Sender.VoiceType != none)
        fVoiceType = Sender.VoiceType;

    else
        fVoiceType = class'FDefaultVoicePack';

    if (Sender == none || Player.Console == none || Level.NetMode == NM_DedicatedServer)
        return;

    Voice = Spawn(fVoiceType, self);

    if (KFVoicePack(Voice) != none)
    {
        if (MessageType == 'TRADER')
        {
            // only play the 30 seconds remaning message if we haven't been to the Shop
            if (MessageID == 5 || MessageID >= 10 || (MessageID == 4 && bHasHeardTraderWelcomeMessage))
                return;

            // Store the fact that we've heard the Trader's Welcome message on the client
            if (MessageID == 7)
                bHasHeardTraderWelcomeMessage = true;

            // If we're hearing the Shop's Closed Message, reset the Trader's Welcome message flag
            else if (MessageID == 6)
                bHasHeardTraderWelcomeMessage = false;

            KFVoicePack(Voice).ClientInitializeLocational(Sender, Recipient, MessageType, MessageID, SenderPawn, SenderLocation);

            // removed trader chat spam, only print our message
            if ((MessageID == 2 || MessageID == 3) && KFVoicePack(Voice).GetClientParsedMessage() != "")
                ClientMessage(FTraderString);
        }

        else
        {
            KFVoicePack(Voice).ClientInitializeLocational(Sender, Recipient, MessageType, MessageID, SenderPawn, SenderLocation);

            if (KFVoicePack(Voice).GetClientParsedMessage() != "")
                TeamMessage(Sender, KFVoicePack(Voice).GetClientParsedMessage(), 'Voice');
        }
    }

    else if (Voice != None)
        Voice.ClientInitialize(Sender, Recipient, MessageType, MessageID);
}

// removed a log error
simulated function ShowLoginMenu()
{
    if (Pawn != none && (Pawn.Health > 0 || (Pawn.PlayerReplicationInfo != none && Pawn.PlayerReplicationInfo.bReadyToPlay)))
        return;

    if (GameReplicationInfo != none)
        ClientReplaceMenu(LobbyMenuClassString);
}

defaultproperties
{
    PawnClass=Class'FHumanPawn'
    MidGameMenuClass="FInvasionLoginMenu"
    LobbyMenuClassString="Falk689ServerPerks.FLobbyMenu"
    OwnCamera=""
    fMinNetSpeed=64000
    FTraderString="Wave's over, you can vote to skip this trader via the appropriate tab"
    PlayerReplicationInfoClass=Class'FPlayerReplicationInfo'
}
