class FVest extends KFWeapon;

#exec OBJ LOAD FILE=LairTextures_T.utx

defaultproperties
{
   Weight=0.0
   TraderInfoTexture=Texture'LairTextures_T.CustomReskins.WornKevlarTrader'
   Description="Minimal amount of kevlar armor. The wearer will not take direct damage until the whole vest is torn apart. Sonic damage will fully bypass it."
   ItemName="Worn Kevlar"
}
