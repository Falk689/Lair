class HUDFalk extends SRHUDKillingFloor;

var() SpriteWidget FalkPercentIcon;
var() SpriteWidget FalkSyringeIcon;
var() SpriteWidget FalkQuickSyringeIcon;
var() SpriteWidget FalkWelderIcon;
var() SpriteWidget FalkATMineIcon;
var() SpriteWidget FalkFlaresIcon;

var float FDrawPlayerInfoDist;

// fixed ammo count and stuff for custom weapons
simulated function UpdateHud()
{
   local float MaxGren, CurGren;
   local KFHumanPawn KFHPawn;
   local Syringe S;

   if( PawnOwner == none )
   {
      super.UpdateHud();
      return;
   }

   KFHPawn = KFHumanPawn(PawnOwner);

   CalculateAmmo();

   if ( KFHPawn != none )
   {
      FlashlightDigits.Value = 100 * (float(KFHPawn.TorchBatteryLife) / float(KFHPawn.default.TorchBatteryLife));
   }

   if ( KFWeapon(PawnOwner.Weapon) != none )
   {
      BulletsInClipDigits.Value = KFWeapon(PawnOwner.Weapon).MagAmmoRemaining;

      if ( BulletsInClipDigits.Value < 0 )
         BulletsInClipDigits.Value = 0;
   }

   if ( Frag(PawnOwner.Weapon) != none )
   {
      ClipsDigits.Value = 0;
      SecondaryClipsDigits.Value = 0;
      BulletsInClipDigits.Value = 0;
   }

   else
   {
      ClipsDigits.Value = CurClipsPrimary;
      SecondaryClipsDigits.Value = CurClipsSecondary;
   }

   if (LAW(PawnOwner.Weapon)                != none ||
       Crossbow(PawnOwner.Weapon)           != none ||
       M99SniperRifle(PawnOwner.Weapon)     != none ||
       M79GrenadeLauncher(PawnOwner.Weapon) != none ||
       PipeBombExplosive(PawnOwner.Weapon)  != none ||
       Crossbuzzsaw(PawnOwner.Weapon)       != none ||
       HuskGun(PawnOwner.Weapon)            != none ||
       ATMineExplosive(PawnOwner.Weapon)    != none ||
       FlareHandheld(PawnOwner.Weapon)      != none ||
       AshotBaseFalk(PawnOwner.Weapon)      != none)
   {
      ClipsDigits.Value += KFWeapon(PawnOwner.Weapon).MagAmmoRemaining;
   }

   else  if (SpitfireBaseFalk(PawnOwner.Weapon) != none || StingerBaseFalk(PawnOwner.Weapon) != none || Mercy(PawnOwner.Weapon) != none)
   {
      if (KFWeapon(PawnOwner.Weapon).AmmoAmount(0) > 0)
         ClipsDigits.Value = Max(1, int(float(KFWeapon(PawnOwner.Weapon).AmmoAmount(0)) / float(KFWeapon(PawnOwner.Weapon).MaxAmmo(0)) * 100));

      else
         ClipsDigits.Value = 0;
   }


   if (PlayerGrenade == none)
   {
      FindPlayerGrenade();
   }

   if ( PlayerGrenade != none )
   {
      PlayerGrenade.GetAmmoCount(MaxGren, CurGren);
      GrenadeDigits.Value = CurGren;
   }
   else
   {
      GrenadeDigits.Value = 0;
   }

   if (Vehicle(PawnOwner) != None)
   {
      if (Vehicle(PawnOwner).Driver != None)
         HealthDigits.Value = Vehicle(PawnOwner).Driver.Health;

      ArmorDigits.Value = PawnOwner.Health;
   }

   else if (KFHPawn != none)
   {
      HealthDigits.Value = PawnOwner.Health; 
      //if (KFHPawn != None)
      ArmorDigits.Value = KFHPawn.ShieldStrength;
   }

   else
   {
      HealthDigits.Value = 0;
      ArmorDigits.Value  = 0;
   }


   // "Poison" the health meter
   if ( VomitHudTimer > Level.TimeSeconds )
   {
      HealthDigits.Tints[0].R = 196;
      HealthDigits.Tints[0].G = 206;
      HealthDigits.Tints[0].B = 0;

      HealthDigits.Tints[1].R = 196;
      HealthDigits.Tints[1].G = 206;
      HealthDigits.Tints[1].B = 0;
   }
   else if ( PawnOwner.Health < 50 )
   {
      if ( Level.TimeSeconds < SwitchDigitColorTime )
      {
         HealthDigits.Tints[0].R = 255;
         HealthDigits.Tints[0].G = 200;
         HealthDigits.Tints[0].B = 0;

         HealthDigits.Tints[1].R = 255;
         HealthDigits.Tints[1].G = 200;
         HealthDigits.Tints[1].B = 0;
      }
      else
      {
         HealthDigits.Tints[0].R = 255;
         HealthDigits.Tints[0].G = 0;
         HealthDigits.Tints[0].B = 0;

         HealthDigits.Tints[1].R = 255;
         HealthDigits.Tints[1].G = 0;
         HealthDigits.Tints[1].B = 0;

         if ( Level.TimeSeconds > SwitchDigitColorTime + 0.2 )
         {
            SwitchDigitColorTime = Level.TimeSeconds + 0.2;
         }
      }
   }
   else
   {
      HealthDigits.Tints[0].R = 255;
      HealthDigits.Tints[0].G = 50;
      HealthDigits.Tints[0].B = 50;

      HealthDigits.Tints[1].R = 255;
      HealthDigits.Tints[1].G = 50;
      HealthDigits.Tints[1].B = 50;
   }

   CashDigits.Value = PawnOwnerPRI.Score;

   WelderDigits.Value = 100 * (CurAmmoPrimary / MaxAmmoPrimary);
   SyringeDigits.Value = WelderDigits.Value / 10;

   if ( SyringeDigits.Value < 50 )
   {
      SyringeDigits.Tints[0].R = 128;
      SyringeDigits.Tints[0].G = 128;
      SyringeDigits.Tints[0].B = 128;

      SyringeDigits.Tints[1] = SyringeDigits.Tints[0];
   }
   else if ( SyringeDigits.Value < 100 )
   {
      SyringeDigits.Tints[0].R = 192;
      SyringeDigits.Tints[0].G = 96;
      SyringeDigits.Tints[0].B = 96;

      SyringeDigits.Tints[1] = SyringeDigits.Tints[0];
   }
   else
   {
      SyringeDigits.Tints[0].R = 255;
      SyringeDigits.Tints[0].G = 64;
      SyringeDigits.Tints[0].B = 64;

      SyringeDigits.Tints[1] = SyringeDigits.Tints[0];
   }

   if (bDisplayQuickSyringe)
   {
      S = Syringe(PawnOwner.FindInventoryType(class'Syringe'));
      if ( S != none )
      {
         QuickSyringeDigits.Value = S.ChargeBar() * 100;

         if ( QuickSyringeDigits.Value < 50 )
         {
            QuickSyringeDigits.Tints[0].R = 128;
            QuickSyringeDigits.Tints[0].G = 128;
            QuickSyringeDigits.Tints[0].B = 128;

            QuickSyringeDigits.Tints[1] = QuickSyringeDigits.Tints[0];
         }
         else if ( QuickSyringeDigits.Value < 100 )
         {
            QuickSyringeDigits.Tints[0].R = 192;
            QuickSyringeDigits.Tints[0].G = 96;
            QuickSyringeDigits.Tints[0].B = 96;

            QuickSyringeDigits.Tints[1] = QuickSyringeDigits.Tints[0];
         }
         else
         {
            QuickSyringeDigits.Tints[0].R = 255;
            QuickSyringeDigits.Tints[0].G = 64;
            QuickSyringeDigits.Tints[0].B = 64;

            QuickSyringeDigits.Tints[1] = QuickSyringeDigits.Tints[0];
         }
      }
   }

   // Hints
   if ( PawnOwner.Health <= 50 )
   {
      KFPlayerController(PlayerOwner).CheckForHint(51);
   }

   Super(HudBase).UpdateHud();
}

// show proper ammo with our boomstick
simulated function CalculateAmmo()
{
	MaxAmmoPrimary = 1;
	CurAmmoPrimary = 1;

	if ( PawnOwner == None || KFWeapon(PawnOwner.Weapon) == none )
		return;

	PawnOwner.Weapon.GetAmmoCount(MaxAmmoPrimary,CurAmmoPrimary);

	if ( PawnOwner.Weapon.FireModeClass[1].default.AmmoClass != none )
	{
	   CurClipsSecondary = PawnOwner.Weapon.AmmoAmount(1);
	}

	if (KFWeapon(PawnOwner.Weapon).bHoldToReload || BoomStickBaseFalk(PawnOwner.Weapon) != None)
	{
		CurClipsPrimary = Max(CurAmmoPrimary-KFWeapon(PawnOwner.Weapon).MagAmmoRemaining,0); // Single rounds reload, just show the true ammo count.
		return;
	}

	CurClipsPrimary = (CurAmmoPrimary - KFWeapon(PawnOwner.Weapon).MagAmmoRemaining) / KFWeapon(PawnOwner.Weapon).MagCapacity;

	// count the partial clip if there is one
	if ( (CurAmmoPrimary - KFWeapon(PawnOwner.Weapon).MagAmmoRemaining) % KFWeapon(PawnOwner.Weapon).MagCapacity > 0 )
		CurClipsPrimary += 1;

	if ( CurClipsPrimary < 0 )
		CurClipsPrimary = 0;
}


// more Ashot and Spitfire fix
simulated function DrawHudPassA (Canvas C)
{
   local KFHumanPawn KFHPawn;
   local Material TempMaterial, TempStarMaterial;
   local int i, TempLevel;
   local float TempX, TempY, TempSize;
   local byte Counter;
   local class<SRVeterancyTypes> SV;
   local byte FGreyThresh;

   DrawStoryHUDInfo(C);

   KFHPawn = KFHumanPawn(PawnOwner);

   DrawDoorHealthBars(C);

   if ( !bLightHud )
   {
      DrawSpriteWidget(C, HealthBG);
   }

   DrawSpriteWidget(C, HealthIcon);
   DrawNumericWidget(C, HealthDigits, DigitsSmall);

   if ( !bLightHud )
   {
      DrawSpriteWidget(C, ArmorBG);
   }

   DrawSpriteWidget(C, ArmorIcon);
   DrawNumericWidget(C, ArmorDigits, DigitsSmall);

   if (KFHPawn != none)
   {
      C.SetPos(C.ClipX * WeightBG.PosX, C.ClipY * WeightBG.PosY);

      if ( !bLightHud )
      {
         C.DrawTile(WeightBG.WidgetTexture, WeightBG.WidgetTexture.MaterialUSize() * WeightBG.TextureScale * 1.5 * HudCanvasScale * ResScaleX * HudScale, WeightBG.WidgetTexture.MaterialVSize() * WeightBG.TextureScale * HudCanvasScale * ResScaleY * HudScale, 0, 0, WeightBG.WidgetTexture.MaterialUSize(), WeightBG.WidgetTexture.MaterialVSize());
      }

      DrawSpriteWidget(C, WeightIcon);

      C.Font = LoadSmallFontStatic(5);
      C.FontScaleX = C.ClipX / 1024.0;
      C.FontScaleY = C.FontScaleX;
      C.SetPos(C.ClipX * WeightDigits.PosX, C.ClipY * WeightDigits.PosY);
      C.DrawColor = WeightDigits.Tints[0];
      C.DrawText(int(KFHPawn.CurrentWeight)$"/"$int(KFHPawn.MaxCarryWeight));
      C.FontScaleX = 1;
      C.FontScaleY = 1;
   }

   if ( !bLightHud )
   {
      DrawSpriteWidget(C, GrenadeBG);
   }

   DrawSpriteWidget(C, GrenadeIcon);
   DrawNumericWidget(C, GrenadeDigits, DigitsSmall);

   if ( PawnOwner != none && PawnOwner.Weapon != none )
   {
      if ( Syringe(PawnOwner.Weapon) != none )
      {
         if ( !bLightHud )
         {
            DrawSpriteWidget(C, SyringeBG);
         }

         DrawSpriteWidget(C, FalkSyringeIcon);
         DrawNumericWidget(C, SyringeDigits, DigitsSmall);
      }
      else
      {
         if ( bDisplayQuickSyringe )
         {
            TempSize = Level.TimeSeconds - QuickSyringeStartTime;
            if ( TempSize < QuickSyringeDisplayTime )
            {
               if ( TempSize < QuickSyringeFadeInTime )
               {
                  QuickSyringeBG.Tints[0].A = int((TempSize / QuickSyringeFadeInTime) * 255.0);
                  QuickSyringeBG.Tints[1].A = QuickSyringeBG.Tints[0].A;
                  FalkQuickSyringeIcon.Tints[0].A = QuickSyringeBG.Tints[0].A;
                  FalkQuickSyringeIcon.Tints[1].A = QuickSyringeBG.Tints[0].A;
                  QuickSyringeDigits.Tints[0].A = QuickSyringeBG.Tints[0].A;
                  QuickSyringeDigits.Tints[1].A = QuickSyringeBG.Tints[0].A;
               }
               else if ( TempSize > QuickSyringeDisplayTime - QuickSyringeFadeOutTime )
               {
                  QuickSyringeBG.Tints[0].A = int((1.0 - ((TempSize - (QuickSyringeDisplayTime - QuickSyringeFadeOutTime)) / QuickSyringeFadeOutTime)) * 255.0);
                  QuickSyringeBG.Tints[1].A = QuickSyringeBG.Tints[0].A;
                  FalkQuickSyringeIcon.Tints[0].A = QuickSyringeBG.Tints[0].A;
                  FalkQuickSyringeIcon.Tints[1].A = QuickSyringeBG.Tints[0].A;
                  QuickSyringeDigits.Tints[0].A = QuickSyringeBG.Tints[0].A;
                  QuickSyringeDigits.Tints[1].A = QuickSyringeBG.Tints[0].A;
               }
               else
               {
                  QuickSyringeBG.Tints[0].A = 255;
                  QuickSyringeBG.Tints[1].A = 255;
                  FalkQuickSyringeIcon.Tints[0].A = 255;
                  FalkQuickSyringeIcon.Tints[1].A = 255;
                  QuickSyringeDigits.Tints[0].A = 255;
                  QuickSyringeDigits.Tints[1].A = 255;
               }

               if ( !bLightHud )
               {
                  DrawSpriteWidget(C, QuickSyringeBG);
               }

               DrawSpriteWidget(C, FalkQuickSyringeIcon);
               DrawNumericWidget(C, QuickSyringeDigits, DigitsSmall);
            }
            else
            {
               bDisplayQuickSyringe = false;
            }
         }

         if (SparkGunBaseFalk(PawnOwner.Weapon) == none && (MP7MMedicGunBaseFalk(PawnOwner.Weapon) != none || FMediShotBaseFalk(PawnOwner.Weapon) != none))
         {
            if (MP7MMedicGun(PawnOwner.Weapon) != none)
            {
               MedicGunDigits.Value = MP7MMedicGun(PawnOwner.Weapon).ChargeBar() * 100;
               FGreyThresh = MP7MMedicGunBaseFalk(PawnOwner.Weapon).FHUDGreyThreshold;
            }


            else if (FMediShotBaseFalk(PawnOwner.Weapon) != none)
            {
               MedicGunDigits.Value = MediShotBaseFalk(PawnOwner.Weapon).ChargeBar() * 100;
               FGreyThresh = MediShotBaseFalk(PawnOwner.Weapon).FHUDGreyThreshold;
            }

            if (MedicGunDigits.Value < FGreyThresh)
            {
               MedicGunDigits.Tints[0].R = 128;
               MedicGunDigits.Tints[0].G = 128;
               MedicGunDigits.Tints[0].B = 128;

               MedicGunDigits.Tints[1] = SyringeDigits.Tints[0];
            }
            else if (MedicGunDigits.Value < 100)
            {
               MedicGunDigits.Tints[0].R = 192;
               MedicGunDigits.Tints[0].G = 96;
               MedicGunDigits.Tints[0].B = 96;

               MedicGunDigits.Tints[1] = SyringeDigits.Tints[0];
            }
            else
            {
               MedicGunDigits.Tints[0].R = 255;
               MedicGunDigits.Tints[0].G = 64;
               MedicGunDigits.Tints[0].B = 64;

               MedicGunDigits.Tints[1] = MedicGunDigits.Tints[0];
            }

            if ( !bLightHud )
            {
               DrawSpriteWidget(C, MedicGunBG);
            }

            DrawSpriteWidget(C, MedicGunIcon);
            DrawNumericWidget(C, MedicGunDigits, DigitsSmall);
         }

         if (ArmorWelderBaseFalk(PawnOwner.Weapon) != none || SparkGunBaseFalk(PawnOwner.Weapon) != none)
         {
            if (!bLightHud)
            {
               DrawSpriteWidget(C, WelderBG);
            }
            
            if ((SparkGunBaseFalk(PawnOwner.Weapon) != none && WelderDigits.Value < 35) || WelderDigits.Value == 0)
            {
               WelderDigits.Tints[0].R = 128;
               WelderDigits.Tints[0].G = 128;
               WelderDigits.Tints[0].B = 128;

               WelderDigits.Tints[1] = WelderDigits.Tints[0];
            }
            else if (WelderDigits.Value < 100)
            {
               WelderDigits.Tints[0].R = 192;
               WelderDigits.Tints[0].G = 96;
               WelderDigits.Tints[0].B = 96;

               WelderDigits.Tints[1] = WelderDigits.Tints[0];
            }
            else
            {
               WelderDigits.Tints[0].R = 255;
               WelderDigits.Tints[0].G = 64;
               WelderDigits.Tints[0].B = 64;

               WelderDigits.Tints[1] = WelderDigits.Tints[0];
            }

            DrawSpriteWidget(C, FalkWelderIcon);
            DrawNumericWidget(C, WelderDigits, DigitsSmall);
         }
         else if (Frag(PawnOwner.Weapon) == none && PawnOwner.Weapon.GetAmmoClass(0) != none)
         {
            if ( !bLightHud )
            {
               DrawSpriteWidget(C, ClipsBG);
            }

            if ( HuskGun(PawnOwner.Weapon) != none )
            {
               ClipsDigits.PosX = 0.873;
               DrawNumericWidget(C, ClipsDigits, DigitsSmall);
               ClipsDigits.PosX = default.ClipsDigits.PosX;
            }
            else if (Frag(PawnOwner.Weapon) == none)
            {
               DrawNumericWidget(C, ClipsDigits, DigitsSmall);
            }

            if (LAW(PawnOwner.Weapon) != none)
            {
               DrawSpriteWidget(C, LawRocketIcon);
            }
            else if (ATMineExplosive(PawnOwner.Weapon) != none)
            {
               DrawSpriteWidget(C, FalkATMineIcon);
            }
            else if (FlareHandheld(PawnOwner.Weapon) != none)
            {
               DrawSpriteWidget(C, FalkFlaresIcon);
            }
            else if (Crossbow(PawnOwner.Weapon) != none)
            {
               DrawSpriteWidget(C, ArrowheadIcon);
            }
            else if (Crossbuzzsaw(PawnOwner.Weapon) != none)
            {
               DrawSpriteWidget(C, SawAmmoIcon);
            }
            else if (PipeBombExplosive(PawnOwner.Weapon) != none)
            {
               DrawSpriteWidget(C, PipeBombIcon);
            }
            else if (M79GrenadeLauncher(PawnOwner.Weapon) != none)
            {
               DrawSpriteWidget(C, M79Icon);
            }
            else if ( HuskGun(PawnOwner.Weapon) != none )
            {
               DrawSpriteWidget(C, HuskAmmoIcon);
            }
            else if (AshotBaseFalk(PawnOwner.Weapon) != none || M99SniperRifle(PawnOwner.Weapon) != none)
            {
               DrawSpriteWidget(C, SingleBulletIcon);
            }
            else if (SpitfireBaseFalk(PawnOwner.Weapon) != none || StingerBaseFalk(PawnOwner.Weapon) != none || Mercy(PawnOwner.Weapon) != none)
            {
               DrawSpriteWidget(C, FalkPercentIcon);
            }
            else
            {
               if ( !bLightHud )
               {
                  DrawSpriteWidget(C, BulletsInClipBG);
               }

               DrawNumericWidget(C, BulletsInClipDigits, DigitsSmall);

               if (Flamethrower(PawnOwner.Weapon) != none)
               {
                  DrawSpriteWidget(C, FlameIcon);
                  DrawSpriteWidget(C, FlameTankIcon);
               }

               else if (M32GrenadeLauncher(PawnOwner.Weapon) != none)
               {
                  DrawSpriteWidget(C, M79Icon);
                  DrawSpriteWidget(C, BulletsInClipIcon);
               }

               else if (Shotgun(PawnOwner.Weapon)            != none  || BoomStickBaseFalk(PawnOwner.Weapon)     != none || Winchester(PawnOwner.Weapon)            != none ||
                        BenelliShotgun(PawnOwner.Weapon)     != none  || FN_TPS(PawnOwner.Weapon)                != none || W1300_Compact_Edition(PawnOwner.Weapon) != none ||
                        Express870(PawnOwner.Weapon)         != none  || KS23SAShotgun(PawnOwner.Weapon)         != none || Trenchgun(PawnOwner.Weapon)             != none ||
                        FMediShotBaseFalk(PawnOwner.Weapon)  != none)
               {
                  DrawSpriteWidget(C, SingleBulletIcon);
                  DrawSpriteWidget(C, BulletsInClipIcon);
               }

               else
               {
                  DrawSpriteWidget(C, ClipsIcon);
                  DrawSpriteWidget(C, BulletsInClipIcon);
               }
            }

            if (KFWeapon(PawnOwner.Weapon) != none && KFWeapon(PawnOwner.Weapon).bTorchEnabled)
            {
               if ( !bLightHud )
               {
                  DrawSpriteWidget(C, FlashlightBG);
               }

               DrawNumericWidget(C, FlashlightDigits, DigitsSmall);

               if ( KFWeapon(PawnOwner.Weapon).FlashLight != none && KFWeapon(PawnOwner.Weapon).FlashLight.bHasLight )
               {
                  DrawSpriteWidget(C, FlashlightIcon);
               }
               else
               {
                  DrawSpriteWidget(C, FlashlightOffIcon);
               }
            }
         }

         // Secondary ammo
         if ( KFWeapon(PawnOwner.Weapon) != none && Frag(PawnOwner.Weapon) == none && KFWeapon(PawnOwner.Weapon).bHasSecondaryAmmo )
         {
            if ( !bLightHud )
            {
               DrawSpriteWidget(C, SecondaryClipsBG);
            }

            DrawNumericWidget(C, SecondaryClipsDigits, DigitsSmall);
            DrawSpriteWidget(C, SecondaryClipsIcon);
         }
      }
   }

   if( KFPlayerReplicationInfo(PawnOwnerPRI)!=None )
      SV = Class<SRVeterancyTypes>(KFPlayerReplicationInfo(PawnOwnerPRI).ClientVeteranSkill);

   if ( SV!=none )
      SV.Static.SpecialHUDInfo(KFPlayerReplicationInfo(PawnOwnerPRI), C);

   if ( KFSGameReplicationInfo(PlayerOwner.GameReplicationInfo) == none || KFSGameReplicationInfo(PlayerOwner.GameReplicationInfo).bHUDShowCash )
   {
      DrawSpriteWidget(C, CashIcon);
      DrawNumericWidget(C, CashDigits, DigitsBig);
   }

   if ( SV!=None )
   {
      TempSize = 36 * VeterancyMatScaleFactor * 1.4;
      TempX = C.ClipX * 0.007;
      TempY = C.ClipY * 0.93 - TempSize;
      C.DrawColor = WhiteColor;

      TempLevel = KFPlayerReplicationInfo(PawnOwnerPRI).ClientVeteranSkillLevel;
      if( ClientRep!=None && (TempLevel+1)<ClientRep.MaximumLevel )
      {
         // Draw progress bar.
         bDisplayingProgress = true;
         if( NextLevelTimer<Level.TimeSeconds )
         {
            NextLevelTimer = Level.TimeSeconds+3.f;
            LevelProgressBar = SV.Static.GetTotalProgress(ClientRep,TempLevel+1);
         }
         Class'SRScoreBoard'.Static.DrawProgressBar(C,TempX,TempY-TempSize*0.12f,TempSize*2.f,TempSize*0.1f,VisualProgressBar);
      }

      C.DrawColor.A = 192;
      TempLevel = SV.Static.PreDrawPerk(C,TempLevel,TempMaterial,TempStarMaterial);

      C.SetPos(TempX, TempY);
      C.DrawTile(TempMaterial, TempSize, TempSize, 0, 0, TempMaterial.MaterialUSize(), TempMaterial.MaterialVSize());

      TempX += (TempSize - VetStarSize);
      TempY += (TempSize - (2.0 * VetStarSize));

      for ( i = 0; i < TempLevel; i++ )
      {
         C.SetPos(TempX, TempY-(Counter*VetStarSize*0.8f));
         C.DrawTile(TempStarMaterial, VetStarSize, VetStarSize, 0, 0, TempStarMaterial.MaterialUSize(), TempStarMaterial.MaterialVSize());

         if( ++Counter==5 )
         {
            Counter = 0;
            TempX+=VetStarSize;
         }
      }
   }

   if ( Level.TimeSeconds - LastVoiceGainTime < 0.333 )
   {
      if ( !bUsingVOIP && PlayerOwner != None && PlayerOwner.ActiveRoom != None &&
            PlayerOwner.ActiveRoom.GetTitle() == "Team" )
      {
         bUsingVOIP = true;
         PlayerOwner.NotifySpeakingInTeamChannel();
      }

      DisplayVoiceGain(C);
   }
   else
   {
      bUsingVOIP = false;
   }

   if ( bDisplayInventory || bInventoryFadingOut )
   {
      DrawInventory(C);
   }
}

simulated function SetHUDAlpha()
{
   HealthBG.Tints[0].A = KFHUDAlpha;
   HealthBG.Tints[1].A = KFHUDAlpha;
   HealthIcon.Tints[0].A = KFHUDAlpha;
   HealthIcon.Tints[1].A = KFHUDAlpha;
   HealthDigits.Tints[0].A = KFHUDAlpha;
   HealthDigits.Tints[1].A = KFHUDAlpha;

   ArmorBG.Tints[0].A = KFHUDAlpha;
   ArmorBG.Tints[1].A = KFHUDAlpha;
   ArmorIcon.Tints[0].A = KFHUDAlpha;
   ArmorIcon.Tints[1].A = KFHUDAlpha;
   ArmorDigits.Tints[0].A = KFHUDAlpha;
   ArmorDigits.Tints[1].A = KFHUDAlpha;

   WeightBG.Tints[0].A = KFHUDAlpha;
   WeightBG.Tints[1].A = KFHUDAlpha;
   WeightIcon.Tints[0].A = KFHUDAlpha;
   WeightIcon.Tints[1].A = KFHUDAlpha;
   WeightDigits.Tints[0].A = KFHUDAlpha;
   WeightDigits.Tints[1].A = KFHUDAlpha;

   GrenadeBG.Tints[0].A = KFHUDAlpha;
   GrenadeBG.Tints[1].A = KFHUDAlpha;
   GrenadeIcon.Tints[0].A = KFHUDAlpha;
   GrenadeIcon.Tints[1].A = KFHUDAlpha;
   GrenadeDigits.Tints[0].A = KFHUDAlpha;
   GrenadeDigits.Tints[1].A = KFHUDAlpha;

   ClipsBG.Tints[0].A = KFHUDAlpha;
   ClipsBG.Tints[1].A = KFHUDAlpha;
   ClipsIcon.Tints[0].A = KFHUDAlpha;
   ClipsIcon.Tints[1].A = KFHUDAlpha;
   ClipsDigits.Tints[0].A = KFHUDAlpha;
   ClipsDigits.Tints[1].A = KFHUDAlpha;

   SecondaryClipsBG.Tints[0].A = KFHUDAlpha;
   SecondaryClipsBG.Tints[1].A = KFHUDAlpha;
   SecondaryClipsIcon.Tints[0].A = KFHUDAlpha;
   SecondaryClipsIcon.Tints[1].A = KFHUDAlpha;
   SecondaryClipsDigits.Tints[0].A = KFHUDAlpha;
   SecondaryClipsDigits.Tints[1].A = KFHUDAlpha;

   BulletsInClipBG.Tints[0].A = KFHUDAlpha;
   BulletsInClipBG.Tints[1].A = KFHUDAlpha;
   BulletsInClipIcon.Tints[0].A = KFHUDAlpha;
   BulletsInClipIcon.Tints[1].A = KFHUDAlpha;
   BulletsInClipDigits.Tints[0].A = KFHUDAlpha;
   BulletsInClipDigits.Tints[1].A = KFHUDAlpha;

   M79Icon.Tints[0].A = KFHUDAlpha;
   M79Icon.Tints[1].A = KFHUDAlpha;
   HuskAmmoIcon.Tints[0].A = KFHUDAlpha;
   HuskAmmoIcon.Tints[1].A = KFHUDAlpha;
   PipeBombIcon.Tints[0].A = KFHUDAlpha;
   PipeBombIcon.Tints[1].A = KFHUDAlpha;
   LawRocketIcon.Tints[0].A = KFHUDAlpha;
   LawRocketIcon.Tints[1].A = KFHUDAlpha;
   ArrowheadIcon.Tints[0].A = KFHUDAlpha;
   ArrowheadIcon.Tints[1].A = KFHUDAlpha;
   SawAmmoIcon.Tints[0].A = KFHUDAlpha;
   SawAmmoIcon.Tints[1].A = KFHUDAlpha;
   SingleBulletIcon.Tints[0].A = KFHUDAlpha;
   SingleBulletIcon.Tints[1].A = KFHUDAlpha;
   FlameIcon.Tints[0].A = KFHUDAlpha;
   FlameIcon.Tints[1].A = KFHUDAlpha;
   FlameTankIcon.Tints[0].A = KFHUDAlpha;
   FlameTankIcon.Tints[1].A = KFHUDAlpha;
   ZEDAmmoIcon.Tints[0].A = KFHUDAlpha;
   ZEDAmmoIcon.Tints[1].A = KFHUDAlpha;

   FlashlightBG.Tints[0].A = KFHUDAlpha;
   FlashlightBG.Tints[1].A = KFHUDAlpha;
   FlashlightIcon.Tints[0].A = KFHUDAlpha;
   FlashlightIcon.Tints[1].A = KFHUDAlpha;
   FlashlightOffIcon.Tints[0].A = KFHUDAlpha;
   FlashlightOffIcon.Tints[1].A = KFHUDAlpha;
   FlashlightDigits.Tints[0].A = KFHUDAlpha;
   FlashlightDigits.Tints[1].A = KFHUDAlpha;

   WelderBG.Tints[0].A = KFHUDAlpha;
   WelderBG.Tints[1].A = KFHUDAlpha;
   FalkWelderIcon.Tints[0].A = KFHUDAlpha;
   FalkWelderIcon.Tints[1].A = KFHUDAlpha;
   WelderDigits.Tints[0].A = KFHUDAlpha;
   WelderDigits.Tints[1].A = KFHUDAlpha;

   SyringeBG.Tints[0].A = KFHUDAlpha;
   SyringeBG.Tints[1].A = KFHUDAlpha;
   FalkSyringeIcon.Tints[0].A = KFHUDAlpha;
   FalkSyringeIcon.Tints[1].A = KFHUDAlpha;
   SyringeDigits.Tints[0].A = KFHUDAlpha;
   SyringeDigits.Tints[1].A = KFHUDAlpha;

   MedicGunBG.Tints[0].A = KFHUDAlpha;
   MedicGunBG.Tints[1].A = KFHUDAlpha;
   MedicGunIcon.Tints[0].A = KFHUDAlpha;
   MedicGunIcon.Tints[1].A = KFHUDAlpha;
   MedicGunDigits.Tints[0].A = KFHUDAlpha;
   MedicGunDigits.Tints[1].A = KFHUDAlpha;

   QuickSyringeBG.Tints[0].A = KFHUDAlpha;
   QuickSyringeBG.Tints[1].A = KFHUDAlpha;
   FalkQuickSyringeIcon.Tints[0].A = KFHUDAlpha;
   FalkQuickSyringeIcon.Tints[1].A = KFHUDAlpha;
   QuickSyringeDigits.Tints[0].A = KFHUDAlpha;
   QuickSyringeDigits.Tints[1].A = KFHUDAlpha;

   CashIcon.Tints[0].A = KFHUDAlpha;
   CashIcon.Tints[1].A = KFHUDAlpha;
   CashDigits.Tints[0].A = KFHUDAlpha;
   CashDigits.Tints[1].A = KFHUDAlpha;
}

// stop glitching everything while changing weapon with mouse wheel
function SelectWeapon()
{
   local Inventory I;
   local bool bFoundItem, bAllowSelect;
   local FHumanPawn FHP;

   HideInventory();

   if (PawnOwner == none)
      return;

   FHP = FHumanPawn(PawnOwner);

   if (FHP == none)
      return;

   for (I = PawnOwner.Inventory; I != none; I = I.Inventory)
   {
      if (I == SelectedInventory)
      {
         bFoundItem = true;
         break;
      }
   }

   if (!bFoundItem)
      return;

   bAllowSelect = FHP.AllowHoldWeapon(KFWeapon(SelectedInventory));

   if (!bAllowSelect)
      return;

   if (Level.TimeSeconds <= FHP.fWeapSwitchTime + FHP.fWeapSwitchCD)
      return;

   if (Weapon(SelectedInventory) == PawnOwner.Weapon)
      return;

   if (PawnOwner.Weapon != none)
   {
      // check why we can't throw the weapon
      if (!PawnOwner.Weapon.CanThrow())
      { 
         // syringe fix
         if (Syringe(PawnOwner.Weapon) != none)
            return;

         // block switch while doing stuff
         if (M79GrenadeLauncher(PawnOwner.Weapon)   != none     ||
             SPGrenadeLauncher(PawnOwner.Weapon)    != none     ||
             BoomStickBaseFalk(PawnOwner.Weapon)    != none     ||
             AshotBaseFalk(PawnOwner.Weapon)        != none     ||
             Lr300M203MedGunBase(PawnOwner.Weapon)  != none     ||
             LAW(PawnOwner.Weapon)                  != none     ||
             M4203AssaultRifle(PawnOwner.Weapon)    != none)
         {
            return;
         }
      }

      // actual weapon switch
      if (PawnOwner.Weapon != PawnOwner.PendingWeapon)
      {
         PawnOwner.PendingWeapon = Weapon(SelectedInventory);
         PawnOwner.Weapon.PutDown();
      }
   }

   // weapon select
   else
   {
      PawnOwner.PendingWeapon = Weapon(SelectedInventory);
      PawnOwner.ChangedWeapon();
   }
}

// fix for healthmax or shieldmax != 100
function DrawPlayerInfo(Canvas C, Pawn P, float ScreenLocX, float ScreenLocY)
{
	local float XL, YL, TempX, TempY, TempSize;
	local float OffsetX;
	local byte BeaconAlpha,Counter;
	local float OldZ;
	local Material TempMaterial, TempStarMaterial;
	local byte i, TempLevel;
   local FHumanPawn FP;

	if (KFPlayerReplicationInfo(P.PlayerReplicationInfo) == none || KFPRI == none || KFPRI.bViewingMatineeCinematic)
		return;

   BeaconAlpha = 255;

	OldZ = C.Z;
	C.Z = 1.0;
	C.Style = ERenderStyle.STY_Alpha;
	C.SetDrawColor(255, 255, 255, BeaconAlpha);
	C.Font = GetConsoleFont(C);
	Class'SRScoreBoard'.Static.TextSizeCountry(C,P.PlayerReplicationInfo,XL,YL);
	Class'SRScoreBoard'.Static.DrawCountryName(C,P.PlayerReplicationInfo,ScreenLocX-(XL * 0.5),ScreenLocY-(YL * 0.75));

	OffsetX = (36.f * VeterancyMatScaleFactor * 0.6) - (HealthIconSize + 2.0);

	if (Class<SRVeterancyTypes>(KFPlayerReplicationInfo(P.PlayerReplicationInfo).ClientVeteranSkill)!=none &&
		 KFPlayerReplicationInfo(P.PlayerReplicationInfo).ClientVeteranSkill.default.OnHUDIcon!=none)
	{
		TempLevel = Class<SRVeterancyTypes>(KFPlayerReplicationInfo(P.PlayerReplicationInfo).ClientVeteranSkill).Static.PreDrawPerk(C,
					KFPlayerReplicationInfo(P.PlayerReplicationInfo).ClientVeteranSkillLevel,
					TempMaterial,TempStarMaterial);

		TempSize = 36.f * VeterancyMatScaleFactor;
		TempX = ScreenLocX + ((BarLength + HealthIconSize) * 0.5) - (TempSize * 0.25) - OffsetX;
		TempY = ScreenLocY - YL - (TempSize * 0.75);

		C.SetPos(TempX, TempY);
		C.DrawTile(TempMaterial, TempSize, TempSize, 0, 0, TempMaterial.MaterialUSize(), TempMaterial.MaterialVSize());

		TempX += (TempSize - (VetStarSize * 0.75));
		TempY += (TempSize - (VetStarSize * 1.5));

		for (i = 0; i < TempLevel; i++)
		{
			C.SetPos(TempX, TempY-(Counter*VetStarSize*0.7f));
			C.DrawTile(TempStarMaterial, VetStarSize * 0.7, VetStarSize * 0.7, 0, 0, TempStarMaterial.MaterialUSize(), TempStarMaterial.MaterialVSize());

			if (++Counter==5)
			{
				Counter = 0;
				TempX+=VetStarSize;
			}
		}
	}

   FP = FHumanPawn(P);

   if (FP != none)
   {
      // Health
      if (FP.fHealth > 0)
         DrawKFBar(C, ScreenLocX - OffsetX, (ScreenLocY - YL) - 0.4 * BarHeight, FClamp(float(FP.fHealth) / float(FP.fHealthMax), 0, 1), BeaconAlpha);

      // Armor
      if (P.ShieldStrength > 0)
         DrawKFBar(C, ScreenLocX - OffsetX, (ScreenLocY - YL) - 1.5 * BarHeight, FClamp(float(FP.fShield) / float(FP.fShieldMax), 0, 1), BeaconAlpha, true);
   }

   else
   {
      // Health
      if (P.Health > 0)
         DrawKFBar(C, ScreenLocX - OffsetX, (ScreenLocY - YL) - 0.4 * BarHeight, FClamp(P.Health / P.HealthMax, 0, 1), BeaconAlpha);

      // Armor
      if (P.ShieldStrength > 0)
         DrawKFBar(C, ScreenLocX - OffsetX, (ScreenLocY - YL) - 1.5 * BarHeight, FClamp(P.ShieldStrength / 100.f, 0, 1), BeaconAlpha, true);
   }

   C.Z = OldZ;
}

// don't show the inventory in awkward moments
function bool ShowInventory()
{
   if (PawnOwner != none && PawnOwner.Weapon != none && PipeBombExplosive(PawnOwner.Weapon) == none && ATMineExplosive(PawnOwner.Weapon) == none && (Frag(PawnOwner.Weapon) != none || !PawnOwner.Weapon.CanThrow()))
      return False;

   return Super.ShowInventory();
}

// removed fade in and fade out from inventory
function DrawInventory(Canvas C)
{
	local Inventory CurInv;
	local int i, Categorized[5], Num[5], X, Y, TempX, TempY, TempWidth, TempHeight, TempBorder, StartY;

	if( PawnOwner == none )
	{
		return;
	}

	if (bInventoryFadingOut)
	{
		C.SetDrawColor(255, 255, 255, byte((1.0 - ((Level.TimeSeconds - InventoryFadeStartTime) / InventoryFadeTime)) * 255.0));
		bInventoryFadingOut = false;
		return;
	}

	else
		C.SetDrawColor(255, 255, 255, 255);

	TempX = InventoryX * C.ClipX;
	TempY = InventoryY * C.ClipY;
	TempWidth = InventoryBoxWidth * C.ClipX;
	TempHeight = InventoryBoxHeight * C.ClipX;
	TempBorder = BorderSize * C.ClipX;
	C.Font = GetFontSizeIndex(C, -3);
	SelectedInventoryCategory = -1;

	// First count the weapons
	for ( CurInv = PawnOwner.Inventory; CurInv != none; CurInv = CurInv.Inventory )
	{
		// Don't allow non-categorized or Grenades
		if ( CurInv.InventoryGroup>0 && CurInv.InventoryGroup<=ArrayCount(Categorized) && KFWeapon(CurInv)!=None )
		{
			if( CurInv==SelectedInventory ) // Make sure index is in sync (could have desynced)
			{
				SelectedInventoryCategory = CurInv.InventoryGroup-1;
				SelectedInventoryIndex = Categorized[SelectedInventoryCategory];
			}
			++Categorized[CurInv.InventoryGroup-1];
		}
	}
	
	// Check if current selected weapon goes off screen.
	if( SelectedInventoryCategory!=-1 && (TempY+Categorized[SelectedInventoryCategory]*TempHeight)>=C.ClipY )
	{
		// Adjust offset based on current selected weapon.
		Y = SelectedInventoryIndex*TempHeight;
		X = (C.ClipY-TempY)/2;
		if( Y>X )
			StartY = X-Y;
	}
	
	// Now draw weapons.
	for ( CurInv = PawnOwner.Inventory; CurInv != none; CurInv = CurInv.Inventory )
	{
		// Don't allow non-categorized or Grenades
		if ( CurInv.InventoryGroup>0 && CurInv.InventoryGroup<=ArrayCount(Categorized) && KFWeapon(CurInv)!=None )
		{
			i = CurInv.InventoryGroup - 1;
			X = TempX+(TempWidth*i);
			Y = TempY+(Num[i]*TempHeight);
			if( i==SelectedInventoryCategory )
				Y+=StartY;

			// Draw this item's Background
			C.SetPos(X, Y);
			if ( CurInv==SelectedInventory )
				C.DrawTileStretched(SelectedInventoryBackgroundTexture, TempWidth, TempHeight);
			else C.DrawTileStretched(InventoryBackgroundTexture, TempWidth, TempHeight);

			// Draw the Weapon's Icon over the Background
			C.SetPos(X + TempBorder, Y + TempBorder);
			DrawSelectionIcon(C,CurInv==SelectedInventory,KFWeapon(CurInv),TempWidth - (2.0 * TempBorder),TempHeight - (2.0 * TempBorder));
			++Num[i];
		}
	}

	// Draw empty categories boxes.
	for ( i=0; i<ArrayCount(Categorized); i++ )
	{
		if ( Categorized[i]==0 )
		{
			C.SetPos(TempX+(TempWidth*i), TempY);
			C.DrawTileStretched(InventoryBackgroundTexture, TempWidth, TempHeight * 0.25);
		}
	}
}

// changed player info distance, so bars are rendered from 30m instead of 20m
simulated function DrawHud(Canvas C)
{
	local KFGameReplicationInfo CurrentGame;
	local rotator CamRot;
	local vector CamPos, ViewDir, ScreenPos;
	local KFPawn KFBuddy;

	RenderDelta = Level.TimeSeconds - LastHUDRenderTime;
    LastHUDRenderTime = Level.TimeSeconds;

	CurrentGame = KFGameReplicationInfo(Level.GRI);

	if ( FontsPrecached < 2 )
		PrecacheFonts(C);

	UpdateHud();

	PassStyle = STY_Modulated;
	DrawModOverlay(C);

	if ( bUseBloom )
		PlayerOwner.PostFX_SetActive(0, true);

	if ( bHideHud )
	{
		// Draw fade effects even if the hud is hidden so poeple can't just turn off thier hud
		C.Style = ERenderStyle.STY_Alpha;
		DrawFadeEffect(C);
		return;
	}

	if ( !KFPlayerReplicationInfo(PlayerOwner.PlayerReplicationInfo).bViewingMatineeCinematic )
	{
		if ( bShowTargeting )
			DrawTargeting(C);

		// Grab our View Direction
		C.GetCameraLocation(CamPos,CamRot);
		ViewDir = vector(CamRot);

		// Draw the Name, Health, Armor, and Veterancy above other players (using this way to fix portal's beacon errors).
		foreach VisibleCollidingActors(Class'KFPawn',KFBuddy,FDrawPlayerInfoDist,CamPos)
		{
			KFBuddy.bNoTeamBeacon = true;
			if ( KFBuddy!=PawnOwner && KFBuddy.PlayerReplicationInfo!=None && KFBuddy.Health>0 && ((KFBuddy.Location - CamPos) Dot ViewDir)>0.8 )
			{
				ScreenPos = C.WorldToScreen(KFBuddy.Location+vect(0,0,1)*KFBuddy.CollisionHeight);
				if( ScreenPos.X>=0 && ScreenPos.Y>=0 && ScreenPos.X<=C.ClipX && ScreenPos.Y<=C.ClipY )
					DrawPlayerInfo(C, KFBuddy, ScreenPos.X, ScreenPos.Y);
			}
		}

		PassStyle = STY_Alpha;
		DrawDamageIndicators(C);
		DrawHudPassA(C);
		DrawHudPassC(C);

		if ( KFPlayerController(PlayerOwner)!= None && KFPlayerController(PlayerOwner).ActiveNote!=None )
		{
			if( PlayerOwner.Pawn == none )
				KFPlayerController(PlayerOwner).ActiveNote = None;
			else KFPlayerController(PlayerOwner).ActiveNote.RenderNote(C);
		}

		PassStyle = STY_None;
		DisplayLocalMessages(C);
		DrawWeaponName(C);
		DrawVehicleName(C);

		PassStyle = STY_Alpha;

		if ( CurrentGame!=None && CurrentGame.EndGameType > 0 )
		{
			DrawEndGameHUD(C, (CurrentGame.EndGameType==2));
			return;
		}

		RenderFlash(C);
		C.Style = PassStyle;
		DrawKFHUDTextElements(C);
	}
	if ( KFPlayerReplicationInfo(PlayerOwner.PlayerReplicationInfo).bViewingMatineeCinematic )
	{
		PassStyle = STY_Alpha;
		DrawCinematicHUD(C);
	}
	if ( bShowNotification )
		DrawPopupNotification(C);
}

// same shit as above with info distance but for spectators
simulated function DrawSpectatingHud(Canvas C)
{
	local rotator CamRot;
	local vector CamPos, ViewDir, ScreenPos;
	local KFPawn KFBuddy;

	DrawModOverlay(C);

	if( bHideHud )
		return;

	PlayerOwner.PostFX_SetActive(0, false);

	// Grab our View Direction
	C.GetCameraLocation(CamPos, CamRot);
	ViewDir = vector(CamRot);

	// Draw the Name, Health, Armor, and Veterancy above other players (using this way to fix portal's beacon errors).
	foreach VisibleCollidingActors(Class'KFPawn',KFBuddy,FDrawPlayerInfoDist,CamPos)
	{
		KFBuddy.bNoTeamBeacon = true;
		if ( KFBuddy.PlayerReplicationInfo!=None && KFBuddy.Health>0 && ((KFBuddy.Location - CamPos) Dot ViewDir)>0.8 )
		{
			ScreenPos = C.WorldToScreen(KFBuddy.Location+vect(0,0,1)*KFBuddy.CollisionHeight);
			if( ScreenPos.X>=0 && ScreenPos.Y>=0 && ScreenPos.X<=C.ClipX && ScreenPos.Y<=C.ClipY )
				DrawPlayerInfo(C, KFBuddy, ScreenPos.X, ScreenPos.Y);
		}
	}

	DrawFadeEffect(C);

	if ( KFPlayerController(PlayerOwner) != None && KFPlayerController(PlayerOwner).ActiveNote != None )
		KFPlayerController(PlayerOwner).ActiveNote = None;

	if( KFGameReplicationInfo(Level.GRI) != none && KFGameReplicationInfo(Level.GRI).EndGameType > 0 )
	{
		if( KFGameReplicationInfo(Level.GRI).EndGameType == 2 )
		{
			DrawEndGameHUD(C, True);
			DrawStoryHUDInfo(C);
			Return;
		}
		else DrawEndGameHUD(C, False);
	}

	DrawKFHUDTextElements(C);
	DisplayLocalMessages(C);

	if ( bShowScoreBoard && ScoreBoard != None )
		ScoreBoard.DrawScoreboard(C);

	// portrait
	if ( bShowPortrait && Portrait != None )
		DrawPortraitX(C);

	// Draw hints
	if ( bDrawHint )
		DrawHint(C);
	
	DrawStoryHUDInfo(C);
}

defaultproperties
{
   FalkPercentIcon=(WidgetTexture=Texture'LairTextures_T.equipment.percentagehud',RenderStyle=STY_Alpha,TextureCoords=(X2=64,Y2=64),TextureScale=0.220000,PosX=0.853000,PosY=0.943000,ScaleMode=SM_Right,Scale=1.000000,Tints[0]=(B=255,G=255,R=255,A=255),Tints[1]=(B=255,G=255,R=255,A=255))
   FalkSyringeIcon=(WidgetTexture=Texture'LairTextures_T.equipment.syringehud',RenderStyle=STY_Alpha,TextureCoords=(X2=64,Y2=64),TextureScale=0.200000,PosX=0.850000,PosY=0.945000,Scale=1.000000,Tints[0]=(B=255,G=255,R=255,A=255),Tints[1]=(B=255,G=255,R=255,A=255))
   FalkQuickSyringeIcon=(WidgetTexture=Texture'LairTextures_T.equipment.syringehud',RenderStyle=STY_Alpha,TextureCoords=(X2=64,Y2=64),TextureScale=0.200000,PosX=0.475000,PosY=0.945000,Scale=1.000000,Tints[0]=(B=255,G=255,R=255,A=255),Tints[1]=(B=255,G=255,R=255,A=255))
   FalkWelderIcon=(WidgetTexture=Texture'LairTextures_T.equipment.welderhud',RenderStyle=STY_Alpha,TextureCoords=(X2=64,Y2=64),TextureScale=0.200000,PosX=0.850000,PosY=0.945000,Scale=1.000000,Tints[0]=(B=255,G=255,R=255,A=255),Tints[1]=(B=255,G=255,R=255,A=255))
   FalkATMineIcon=(WidgetTexture=Texture'LairTextures_T.equipment.minehud',RenderStyle=STY_Alpha,TextureCoords=(X2=64,Y2=64),TextureScale=0.200000,PosX=0.850000,PosY=0.945000,Scale=1.000000,Tints[0]=(B=255,G=255,R=255,A=255),Tints[1]=(B=255,G=255,R=255,A=255))
   FalkFlaresIcon=(WidgetTexture=Texture'LairTextures_T.equipment.flarehud',RenderStyle=STY_Alpha,TextureCoords=(X2=64,Y2=64),TextureScale=0.200000,PosX=0.850000,PosY=0.945000,Scale=1.000000,Tints[0]=(B=255,G=255,R=255,A=255),Tints[1]=(B=255,G=255,R=255,A=255))
   HealthBarFullVisDist=1400.000000
   HealthBarCutoffDist=4000.000000
   FDrawPlayerInfoDist=1500.0
}
