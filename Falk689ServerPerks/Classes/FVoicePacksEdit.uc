class FVoicePacksEdit extends Actor;

#exec OBJ LOAD FILE=LairSounds_S.uax

var int    NumSupports;
var int    NumAcknowledgments;
var int    NumAlerts;
var int    NumDirections;
var int    NumInsults;

var string SupportString[48];
var string AcknowledgmentString[48];
var string AlertString[48];
var string DirectionString[48];
var string InsultString[48];

var SoundGroup AddSupportSoundMaleOne[16];
var SoundGroup AddAcknowledgmentSoundMaleOne[16];
var SoundGroup AddAlertSoundMaleOne[14];
var SoundGroup AddDirectionSoundMaleOne[15];
var SoundGroup AddInsultSoundMaleOne[18];

var SoundGroup AddSupportSoundMaleTwo[16];
var SoundGroup AddAcknowledgmentSoundMaleTwo[16];
var SoundGroup AddAlertSoundMaleTwo[14];
var SoundGroup AddDirectionSoundMaleTwo[15];
var SoundGroup AddInsultSoundMaleTwo[18];

var SoundGroup AddSupportSoundFemale[16];
var SoundGroup AddAcknowledgmentSoundFemale[16];
var SoundGroup AddAlertSoundFemale[14];
var SoundGroup AddDirectionSoundFemale[15];
var SoundGroup AddInsultSoundFemale[18];

var SoundGroup AddSupportSoundRobot[16];
var SoundGroup AddAcknowledgmentSoundRobot[16];
var SoundGroup AddAlertSoundRobot[14];
var SoundGroup AddDirectionSoundRobot[15];
var SoundGroup AddInsultSoundRobot[18];

var SoundGroup AddSupportSoundMumble[16];
var SoundGroup AddAcknowledgmentSoundMumble[16];
var SoundGroup AddAlertSoundMumble[14];
var SoundGroup AddDirectionSoundMumble[15];
var SoundGroup AddInsultSoundMumble[18];

var class<KFVoicePack> VoicePacks[6];

// edit voice packs to tweak or add strings and sounds
simulated static function DoEdit()
{
    local int i, o, p, iSup, iAck, iAle, iDir, iIns;

    iSup = class'KFVoicePack'.Default.NumSupports;
    iAck = class'KFVoicePack'.Default.NumAcknowledgments;
    iAle = class'KFVoicePack'.Default.NumAlerts;
    iDir = class'KFVoicePack'.Default.NumDirections;
    iIns = class'KFVoicePack'.Default.NumInsults;

    for (p=0; p<6; p++)
    {
        Default.VoicePacks[p].Default.NumSupports        = Default.NumSupports;
        Default.VoicePacks[p].Default.NumAcknowledgments = Default.NumAcknowledgments;
        Default.VoicePacks[p].Default.NumAlerts          = Default.NumAlerts;
        Default.VoicePacks[p].Default.NumDirections      = Default.NumDirections;
        Default.VoicePacks[p].Default.NumInsults         = Default.NumInsults;
    }

    for (i=0; i<Default.NumSupports; i++)
    {
        for (p=0; p<6; p++)
        {
            Default.VoicePacks[p].Default.SupportString[i] = Default.SupportString[i];
            Default.VoicePacks[p].Default.SupportAbbrev[i] = Default.SupportString[i];
        }
    }

    for (i=0; i<Default.NumAcknowledgments; i++)
    {
        for (p=0; p<6; p++)
        {
            Default.VoicePacks[p].Default.AcknowledgmentString[i] = Default.AcknowledgmentString[i];
            Default.VoicePacks[p].Default.AcknowledgmentAbbrev[i] = Default.AcknowledgmentString[i];
        }
    }

    for (i=0; i<Default.NumAlerts; i++)
    {
        for (p=0; p<6; p++)
        {
            Default.VoicePacks[p].Default.AlertString[i] = Default.AlertString[i];
            Default.VoicePacks[p].Default.AlertAbbrev[i] = Default.AlertString[i];
        }
    }

    for (i=0; i<Default.NumDirections; i++)
    {
        for (p=0; p<6; p++)
        {
            Default.VoicePacks[p].Default.DirectionString[i] = Default.DirectionString[i];
            Default.VoicePacks[p].Default.DirectionAbbrev[i] = Default.DirectionString[i];
        }
    }

    for (i=0; i<Default.NumInsults; i++)
    {
        for (p=0; p<6; p++)
        {
            Default.VoicePacks[p].Default.InsultString[i] = Default.InsultString[i];
            Default.VoicePacks[p].Default.InsultAbbrev[i] = Default.InsultString[i];
        }
    }

    // add support sounds
    o = iSup;

    for (i=0; i<16; i++)
    {
        if (Default.AddSupportSoundMaleOne[i] == none)
            break;

        class'KFVoicePack'.Default.SupportSound[o]       = Default.AddSupportSoundMaleOne[i];
        class'KFVoicePackTwo'.Default.SupportSound[o]    = Default.AddSupportSoundMaleTwo[i];
        class'KFVoicePackFemale'.Default.SupportSound[o] = Default.AddSupportSoundFemale[i];
        class'KFVoicePackRobot'.Default.SupportSound[o]  = Default.AddSupportSoundRobot[i];
        class'KFVoicePackMumble'.Default.SupportSound[o] = Default.AddSupportSoundMumble[i];

        o++;
    }

    // add acknowledgment sounds
    o = iAck;

    for (i=0; i<16; i++)
    {
        if (Default.AddAcknowledgmentSoundMaleOne[i] == none)
            break;

        class'KFVoicePack'.Default.AcknowledgmentSound[o]       = Default.AddAcknowledgmentSoundMaleOne[i];
        class'KFVoicePackTwo'.Default.AcknowledgmentSound[o]    = Default.AddAcknowledgmentSoundMaleTwo[i];
        class'KFVoicePackFemale'.Default.AcknowledgmentSound[o] = Default.AddAcknowledgmentSoundFemale[i];
        class'KFVoicePackRobot'.Default.AcknowledgmentSound[o]  = Default.AddAcknowledgmentSoundRobot[i];
        class'KFVoicePackMumble'.Default.AcknowledgmentSound[o] = Default.AddAcknowledgmentSoundMumble[i];

        o++;
    }

    // add alert sounds
    o = iAle;

    for (i=0; i<14; i++)
    {
        if (Default.AddAlertSoundMaleOne[i] == none)
            break;

        class'KFVoicePack'.Default.AlertSound[o]       = Default.AddAlertSoundMaleOne[i];
        class'KFVoicePackTwo'.Default.AlertSound[o]    = Default.AddAlertSoundMaleTwo[i];
        class'KFVoicePackFemale'.Default.AlertSound[o] = Default.AddAlertSoundFemale[i];
        class'KFVoicePackRobot'.Default.AlertSound[o]  = Default.AddAlertSoundRobot[i];
        class'KFVoicePackMumble'.Default.AlertSound[o] = Default.AddAlertSoundMumble[i];

        o++;
    }

    // add direction sounds
    o = iDir;

    for (i=0; i<15; i++)
    {
        if (Default.AddDirectionSoundMaleOne[i] == none)
            break;

        class'KFVoicePack'.Default.DirectionSound[o]       = Default.AddDirectionSoundMaleOne[i];
        class'KFVoicePackTwo'.Default.DirectionSound[o]    = Default.AddDirectionSoundMaleTwo[i];
        class'KFVoicePackFemale'.Default.DirectionSound[o] = Default.AddDirectionSoundFemale[i];
        class'KFVoicePackRobot'.Default.DirectionSound[o]  = Default.AddDirectionSoundRobot[i];
        class'KFVoicePackMumble'.Default.DirectionSound[o] = Default.AddDirectionSoundMumble[i];

        o++;
    }

    // add insult sounds
    o = iIns;

    for (i=0; i<18; i++)
    {
        if (Default.AddInsultSoundMaleOne[i] == none)
            break;

        class'KFVoicePack'.Default.InsultSound[o]       = Default.AddInsultSoundMaleOne[i];
        class'KFVoicePackTwo'.Default.InsultSound[o]    = Default.AddInsultSoundMaleTwo[i];
        class'KFVoicePackFemale'.Default.InsultSound[o] = Default.AddInsultSoundFemale[i];
        class'KFVoicePackRobot'.Default.InsultSound[o]  = Default.AddInsultSoundRobot[i];
        class'KFVoicePackMumble'.Default.InsultSound[o] = Default.AddInsultSoundMumble[i];

        o++;
    }
}

defaultproperties
{
    NumSupports=5
    NumAcknowledgments=4
    NumAlerts=6
    NumDirections=5
    NumInsults=2

    SupportString(0)="Medic!"
    SupportString(1)="Help!"
    SupportString(2)="I need some dosh"
    SupportString(3)="I need a weapon"
    SupportString(4)="I need ammo"

    AcknowledgmentString(0)="Yes"
    AcknowledgmentString(1)="No"
    AcknowledgmentString(2)="Thanks"
    AcknowledgmentString(3)="Sorry"

    AlertString(0)="Look out"
    AlertString(1)="Run!"
    AlertString(2)="Wait for me"
    AlertString(3)="Welding requested"
    AlertString(4)="Let's hold this spot"
    AlertString(5)="Follow me"

    DirectionString(0)="Get to the trader"
    DirectionString(1)="Go upstairs"
    DirectionString(2)="Go downstairs"
    DirectionString(3)="Get inside"
    DirectionString(4)="Go outside"

    InsultString(0)="Insult specimens"
    InsultString(1)="Insult players"

    AddSupportSoundMaleOne(0)=Sound'LairSounds_S.CharacterSpeech.MaleOneAmmo'
    AddSupportSoundMaleTwo(0)=Sound'LairSounds_S.CharacterSpeech.MaleTwoAmmo'
    AddSupportSoundFemale(0)=Sound'LairSounds_S.CharacterSpeech.FemaleAmmo'
    AddSupportSoundRobot(0)=Sound'LairSounds_S.CharacterSpeech.RobotAmmo'
    AddSupportSoundMumble(0)=Sound'LairSounds_S.CharacterSpeech.MumbleAmmo'

    VoicePacks(0)=class'KFVoicePack'
    VoicePacks(1)=class'KFVoicePackFemale'
    VoicePacks(2)=class'KFVoicePackMumble'
    VoicePacks(3)=class'KFVoicePackRobot'
    VoicePacks(4)=class'KFVoicePackTwo'
    VoicePacks(5)=class'FDefaultVoicePack'
}
