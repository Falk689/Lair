class FVeterancyTypes extends ServerPerks.SRVeterancyTypes
   abstract;

var float huskDiv;        // Needed to fix the husk cannon value
var float ZTECD;          // Zed time end cooldown
var float ZTSCD;          // Zed time start cooldown

// Add perked weapon properly setting the value
static function FalkAddPerkedWeapon(class<KFWeapon> W, KFPlayerReplicationInfo KFPRI, Pawn P)
{
   local float C;
   local class<KFWeaponPickup> WP;
   local FHumanPawn FP;

   WP = class<KFWeaponPickup>(W.Default.PickupClass);

   if (WP == None)
      KFHumanPawn(P).CreateInventory(string(W));

   else
   {
      C = 200;

      if (P != none)
      {
         FP = FHumanPawn(P);

         if (FP != none)
         {
            C = FP.FalkGetPerkedWeaponValue();
            //warn("New Value:"@C);
         }
      }

      KFHumanPawn(P).CreateInventoryVeterancy(string(W), C);
   }
}


// by now this should do nothing
static function AddDefaultInventory(KFPlayerReplicationInfo KFPRI, Pawn P)
{
}

// Fixed function to add extra ammo
static function float AddNadeExtraAmmo(KFPlayerReplicationInfo KFPRI, KFAmmunition Other)
{
   return 1.0;
}

// Return the medic dart heal radius modifier
static function float GetHealRadiusMod(KFPlayerReplicationInfo KFPRI)
{
   return 1.0;
}

// Return the spark gun radius modifier
static function float GetSparkRadiusMod(KFPlayerReplicationInfo KFPRI)
{
   return 1.0;
}

// Called on zed time start
static function StartZedTime(KFPlayerReplicationInfo KFPRI)
{
}

// Called on zed time start
static function EndZedTime(KFPlayerReplicationInfo KFPRI)
{
}

// are we in zed time?
static function bool InZedTime(KFPlayerReplicationInfo KFPRI)
{ 
   local Falk689GameTypeBase Flk;

   Flk = Falk689GameTypeBase(KFPRI.Level.Game);

   if (Flk == None)
      return False;

   return Flk.InZedTime();
}

// return true if we can use our zed time skill
static function bool CanUseZedTimeSkill(KFPlayerReplicationInfo KFPRI)
{
   local PlayerController PC;
   local Falk689GameTypeBase Flk;

   PC  = PlayerController(KFPRI.Owner);
   Flk = Falk689GameTypeBase(KFPRI.Level.Game);

   if (Flk != none && PC != none)
      return Flk.CanUseZedTimeSkill(PC, default.ZTECD, default.ZTSCD);

   return false;
}

// tell the gametype we used our zed time skill
static function bool ZedTimeSkillUsed(KFPlayerReplicationInfo KFPRI)
{
   local PlayerController PC;
   local Falk689GameTypeBase Flk;

   PC  = PlayerController(KFPRI.Owner);
   Flk = Falk689GameTypeBase(KFPRI.Level.Game);

   if (Flk != none && PC != none)
   {
      Flk.ZedTimeSkillUsed(PC);
      return True;
   }

   return False;
}

// should we fire infinite medic darts?
static function bool InfiniteMedicDarts(KFPlayerReplicationInfo KFPRI)
{
   return false;
}

// alt fire speed mode
static function float GetAltFireSpeedMod(KFPlayerReplicationInfo KFPRI)
{
   return 1.0;
}

// shotgun spread multiplier
static function float GetShotgunSpreadMultiplier(KFPlayerReplicationInfo KFPRI, Weapon fW)
{
   return 1.0;
}

// skill based additional shotgun pellet
static function int GetShotgunProjPerFire(KFPlayerReplicationInfo KFPRI, int ProjPerFire, Weapon fW)
{
   return ProjPerFire;
}

// skill based additional explosive pellet
static function int GetExplosiveProjPerFire(KFPlayerReplicationInfo KFPRI, int ProjPerFire)
{
   return ProjPerFire;
}

// skill based one shot on stalkers during zed time
static function bool ShouldOneShotStalkers(KFPlayerReplicationInfo KFPRI, class<DamageType> damageType)
{
   return false;
}

// skill based artillerist bonus on zed time
static function bool ArtilleristZedTimeBonus(KFPlayerReplicationInfo KFPRI)
{
   return false;
}

// skill based sharpshooter bonus on zed time
static function bool SharpZedTimeBonus(KFPlayerReplicationInfo KFPRI, class<DamageType> damageType)
{
   return false;
}

// skill based demoman stuff on zed time
static function bool DetonateBulletsControlBonus(KFPlayerReplicationInfo KFPRI, optional bool fIgnoreZedTime)
{
   return false;
}

// should the LAW always spawn clusters?
static function bool LAWClustersBonus(KFPlayerReplicationInfo KFPRI)
{
   return false;
}

// can this perk use the armor welder?
static function bool CanWeldArmor()
{
   return false;
}

// is this perk based on melee weapons
static function bool IsMeleeBased()
{
   return false;
}

// here resides the code to give you nades on LAW kills, if you should get any
static function LAWKillNadeAward(KFPlayerReplicationInfo KFPRI, int HitID)
{
}

// armor welder boost based on skill level
static function float GetArmorWeldBoost(KFPlayerReplicationInfo KFPRI)
{
   return 0.0;
}

// armor welder ammo usage reduction
static function int GetArmorWelderAmmoReduction(KFPlayerReplicationInfo KFPRI)
{
   return 0;
}

// additional damage on medium zeds with shotguns
static function int AddShotgunDamage(class<DamageType> DmgType, int InDamage)
{
   return InDamage;
}

// should our explosives be immune to sirens? 
static function bool KaboomSirenImmunity(KFPlayerReplicationInfo KFPRI)
{
   return False;
}

// are we immune to bloat dot?
static function bool BloatDotImmunity(KFPlayerReplicationInfo KFPRI)
{
   return False;
}

// return max health for this perk
static function int fGetMaxHealth(KFPlayerReplicationInfo KFPRI)
{
   return 100;
}

// return max shield for this perk
static function int fGetMaxShield(KFPlayerReplicationInfo KFPRI)
{
   return 100;
}

// add damage to medic nade dot
static function float MedicDotDamage(KFPlayerReplicationInfo KFPRI)
{
   return 0.1;
}

// spark gun reload speed modifier
static function float GetSparkGunChargeRate(KFPlayerReplicationInfo KFPRI)
{
   return 1.0;
}

// should we have unlimited stalker viewing skill and share it during zed time?
static function bool ShareStalkerVisibility(KFPlayerReplicationInfo KFPRI)
{
   return False;
}

// base for the bloodbath bloat skill
static function float FGetBodyArmorDamageModifier(KFPlayerReplicationInfo KFPRI, class<DamageType> DmgType)
{
   return static.GetBodyArmorDamageModifier(KFPRI);
}

// achievement lock-unlock weapons on the client
static function bool AllowWeaponInTrader(class<KFWeaponPickup> Pickup, KFPlayerReplicationInfo KFPRI, byte Level)
{
   local FPCServ Flk;

   if (Pickup != none && KFPRI != none)
   {
      Flk = FPCServ(KFPRI.Owner);

      if (Flk != none)
      {
         if (class<Lr300M203MedGunPickupBase>(Pickup) != none)
            return Flk.medicAchievement;

         if (class<KS23SAPickupBaseFalk>(Pickup) != none)
            return Flk.supportAchievement;

         if (class<L96AWPLLIPickupBaseFalk>(Pickup) != none)
            return Flk.sharpAchievement;

         if (class<AAR525PickupBaseFalk>(Pickup) != none)
            return Flk.commandoAchievement;

         if (class<Caius_SwordPickupBaseFalk>(Pickup) != none)
            return Flk.zerkAchievement;

         if (class<AAR525SPickupBaseFalk>(Pickup) != none)
            return Flk.firebugAchievement;

         if (class<AA12ExplosivePickupBaseFalk>(Pickup) != none)
            return Flk.demoAchievement;

         if (class<XMV850PickupBaseFalk>(Pickup) != none)
            return Flk.artiAchievement;
      }
   }

   return true;
}

// Hardcode listen server level, disallow perk leveling in single player since that game mode is broken and we don't want people to use it
static function bool LevelIsFinished(ClientPerkRepLink StatOther, byte CurLevel)
{
   if (StatOther.Level.NetMode == NM_Standalone)
      return False;

   if (StatOther.Level.NetMode == NM_ListenServer)
   {
      if (CurLevel < 10)
         return True;

      return False;
   }

	return Super.LevelIsFinished(StatOther, CurLevel);
}

defaultproperties
{
   huskDiv=0.5625
   DisableDescription="This weapon is locked by an achievement."
}
