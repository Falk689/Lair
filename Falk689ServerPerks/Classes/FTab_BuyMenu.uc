//=============================================================================
// The actual trader menu
//=============================================================================
class FTab_BuyMenu extends SRKFTab_BuyMenu;

var localized   string                          fKevText;  // worn kevlar info text
var             int                             fPrevN;    // used to unselect the current weapon when needed

function Timer()
{
    local int i;

    ++iFrameCounter;

    i = FGetInventoryHash();

    if (i != InventoryHash)
    {
        //warn("current"@i@"previous"@InventoryHash@"iframecounter"@iFrameCounter@"thingy"@iFrameCounter>>7);
        InventoryHash = i;
        MoneyLabel.Caption = MoneyCaption $ int(PlayerOwner().PlayerReplicationInfo.Score);
        UpdateAll();
    }
}

// Falkified to unselect the current weapon when needed
function int FGetInventoryHash()
{
    local int i,n;
    local Inventory Inv;
    local KFWeapon W;
    local PlayerController PC;
    local class<KFVeterancyTypes> V;

    PC = PlayerOwner();

    if (PC.Pawn == None || KFPlayerReplicationInfo(PC.PlayerReplicationInfo) == None)
        return (iFrameCounter >> 3);

    // Force update hash if perk is changed.
    if (KFPlayerReplicationInfo(PC.PlayerReplicationInfo) != None)
        V = KFPlayerReplicationInfo(PC.PlayerReplicationInfo).ClientVeteranSkill;

    if (OldPerkClass != V)
    {
        OldPerkClass = V;
        InventoryHash = 0;
    }

    i = (int(PC.PlayerReplicationInfo.Score) << 6) ^ int(PC.Pawn.ShieldStrength);

    for (Inv=PC.Pawn.Inventory; Inv!=None; Inv=Inv.Inventory)
    {
        ++n;
        W = KFWeapon(Inv);

        if (W != None)
        {
            n += 15;

            if (Syringe(Inv) != None || ArmorWelderBaseFalk(inv) != None || SparkGunBaseFalk(inv) != None)
                continue;

            i = i ^ (int(W.Weight)>>1) ^ (W.SleeveNum<<3) ^ (int(W.PlayerViewOffset.X)<<4) ^ (W.AmmoAmount(0)<<16);
        }
    }

    if (n != fPrevN)
        InvSelect.List.Index = -1;

    fPrevN = n;

    return (i ^ n);
}

// shady testing stuff
function ShowPanel(bool bShow)
{
   super(UT2K4TabPanel).ShowPanel(bShow);

   bClosed = false;

   if (KFPlayerController(PlayerOwner()) != none)
      KFPlayerController(PlayerOwner()).bDoTraderUpdate = true;

   InvSelect.SetPosition(InvBG.WinLeft + 7.0 / float(Controller.ResX),
         InvBG.WinTop + 55.0 / float(Controller.ResY),
         InvBG.WinWidth - 15.0 / float(Controller.ResX),
         InvBG.WinHeight - 45.0 / float(Controller.ResY),
         true);

   SaleSelect.SetPosition(SaleBG.WinLeft + 7.0 / float(Controller.ResX),
         SaleBG.WinTop + 55.0 / float(Controller.ResY),
         SaleBG.WinWidth - 15.0 / float(Controller.ResX),
         SaleBG.WinHeight - 63.0 / float(Controller.ResY),
         true);
}

function SetInfoText()
{
   //local string                  TempString;
   local class<FVeterancyTypes>  FVet;
   local KFPlayerReplicationInfo KFPRI;

   if (TheBuyable == none && !bDidBuyableUpdate)
   {
      InfoScrollText.SetContent(InfoText[0]);
      bDidBuyableUpdate = true;

      return;
   }

   if (TheBuyable != none && OldPickupClass != TheBuyable.ItemPickupClass)
   {
      KFPRI = KFPlayerReplicationInfo(PlayerOwner().PlayerReplicationInfo);

      if (KFPRI != none)
         FVet  = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

      if (TheBuyable.bIsVest)
         InfoScrollText.SetContent(fKevText);

      else if (FVet != none && !FVet.static.AllowWeaponInTrader(TheBuyable.ItemPickupClass, KFPRI, 0))
         InfoScrollText.SetContent(ArchivementGetInfo);

      else
         InfoScrollText.SetContent(TheBuyable.ItemWeaponClass.Default.Description);

      bDidBuyableUpdate = false;
      OldPickupClass    = TheBuyable.ItemPickupClass;
   }
}

// attempt to remove double click event to buy weapons
function bool SaleDblClick(GUIComponent Sender)
{
    return false;
}

// attempt to remove double click event to sell weapons
function bool InvDblClick(GUIComponent Sender)
{
    return false;
}

// implement partial armor
function DoBuy()
{
    if (TheBuyable != none && KFPawn(PlayerOwner().Pawn) != none && TheBuyable.ItemPickupClass != none)
    {
        if (class<FVestPickup>(TheBuyable.ItemPickupClass) != none)
        {
            if (FHumanPawn(PlayerOwner().Pawn) != none)
            {
                FHumanPawn(PlayerOwner().Pawn).FalkServerBuyKevlar(class<FVestPickup>(TheBuyable.ItemPickupClass));
                MakeSomeBuyNoise(class'Vest');
            }
        }

        else// if (TheBuyable.ItemWeaponClass != none)
        {
            KFPawn(PlayerOwner().Pawn).ServerBuyWeapon(TheBuyable.ItemWeaponClass, 0);
            MakeSomeBuyNoise();
        }

        SaleSelect.List.Index = -1;
        TheBuyable            = none;
        LastBuyable           = none;
    }
}

// don't call the superclass
function bool InternalOnClick(GUIComponent Sender)
{
    RefreshSelection();

   if (Sender == PurchaseButton)
    {
        if (TheBuyable != none)
        {
            DoBuy();
            TheBuyable = none;
        }
    }

    else if (Sender == SaleButton)
    {
        if (TheBuyable.bSellable)
        {
            DoSell();
            TheBuyable = none;
        }
    }

    else if (Sender == AutoFillButton)
        DoFillAllAmmo();

    else if (Sender == ExitButton)
        GUIBuyMenu(OwnerPage()).CloseSale(false);

    UpdateAll();

    return true;
}

defaultproperties
{
   ArchivementGetInfo="This weapon is locked by an achievement."
   InfoText(1)="This weapon is either too expensive or too heavy."
   InfoText(2)="This weapon is either too expensive or too heavy."
   fKevText="Kevlar armor for personal protection. The wearer will not take direct damage until the whole vest is torn apart. Sonic damage will fully bypass it. You can also buy partial armor."

   Begin Object Class=FBuyMenuInvListBox Name=InventoryBox
   OnCreateComponent=InventoryBox.InternalOnCreateComponent
   WinTop=0.070841
   WinLeft=0.000108
   WinWidth=0.328204
   WinHeight=0.521856
   End Object
   InvSelect=FBuyMenuInvListBox'FTab_BuyMenu.InventoryBox'

   Begin Object Class=FGUIBuyWeaponInfoPanel Name=ItemInf
   WinTop=0.193730
   WinLeft=0.332571
   WinWidth=0.333947
   WinHeight=0.489407
   End Object
   ItemInfo=FGUIBuyWeaponInfoPanel'FTab_BuyMenu.ItemInf'

   Begin Object Class=FBuyMenuSaleListBox Name=SaleBox
   OnCreateComponent=SaleBox.InternalOnCreateComponent
   WinTop=0.064312
   WinLeft=0.672632
   WinWidth=0.325857
   WinHeight=0.674039
   End Object
   SaleSelect=FBuyMenuSaleListBox'FTab_BuyMenu.SaleBox'

   Begin Object class=GUIImage Name=Cash
   WinTop=0.026828
   WinLeft=0.393095
   WinWidth=0.107313
   WinHeight=0.077172
   Image=Texture'LairTextures_T.equipment.BanknoteSkin'
   ImageStyle=ISTY_Scaled
   End Object
   BankNote=Cash
}
