class FVestPickup extends KFWeaponPickup
   notplaceable;

var int         ShieldCapacity; 

defaultproperties
{
   Weight=0.0
   cost=25
   Description="Minimal amount of kevlar armor. The wearer will not take direct damage until the whole vest is torn apart. Sonic damage will fully bypass it."
   ItemName="Worn Kevlar"
   ItemShortName="Worn Kevlar"
   InventoryType=Class'FVest'
   CorrespondingPerkIndex=8
   EquipmentCategoryID=5
   PickupMessage="You got a Kevlar"
   PickupSound=Sound'KF_InventorySnd.Vest_Pickup'
   StaticMesh=StaticMesh'KillingFloorStatics.Vest'
   DrawScale3D=(Z=0.400000)
   TransientSoundVolume=150.000000
   CollisionRadius=30.000000
   CollisionHeight=5.000000
   ShieldCapacity=10
}
