class FDefaultVoicePack extends KFVoicePack;

defaultproperties
{
    SupportSound(0)=none
    SupportSound(1)=none
    SupportSound(2)=none
    SupportSound(3)=none

    AcknowledgmentSound(0)=none
    AcknowledgmentSound(1)=none
    AcknowledgmentSound(2)=none
    AcknowledgmentSound(3)=none

    AlertSound(0)=none
    AlertSound(1)=none
    AlertSound(2)=none
    AlertSound(3)=none
    AlertSound(4)=none
    AlertSound(5)=none

    DirectionSound(0)=none
    DirectionSound(1)=none
    DirectionSound(2)=none
    DirectionSound(3)=none
    DirectionSound(4)=none

    InsultSound(0)=none
    InsultSound(1)=none

    AutomaticSound(0)=none
    AutomaticSound(1)=none
    AutomaticSound(2)=none
    AutomaticSound(3)=none
    AutomaticSound(4)=none
    AutomaticSound(5)=none
    AutomaticSound(6)=none
    AutomaticSound(7)=none
    AutomaticSound(8)=none
    AutomaticSound(9)=none
    AutomaticSound(10)=none
    AutomaticSound(11)=none
    AutomaticSound(12)=none
    AutomaticSound(13)=none
    AutomaticSound(14)=none
    AutomaticSound(15)=none
    AutomaticSound(16)=none
    AutomaticSound(17)=none
    AutomaticSound(18)=none
    AutomaticSound(19)=none
    AutomaticSound(20)=none
    AutomaticSound(21)=none
    AutomaticSound(22)=none
    AutomaticSound(23)=none
    AutomaticSound(24)=none

    TraderSound(0)=none
    TraderSound(1)=none
    TraderSound(2)=none
    TraderSound(3)=none
    TraderSound(4)=none
    TraderSound(5)=none
    TraderSound(6)=none
    TraderSound(7)=none
    TraderSound(8)=none
    TraderSound(9)=none
    TraderSound(10)=none
    TraderSound(11)=none
}
