class FBuyMenuInvListBox extends SRKFBuyMenuInvListBox;

function InitComponent(GUIController MyController, GUIComponent MyOwner)
{
    DefaultListClass = string(Class'FBuyMenuInvList');
    Super(KFBuyMenuInvListBox).InitComponent(MyController,MyOwner);
}

function GUIBuyable GetSelectedBuyable()
{
    if (List.Index < 0)
        return none;

    if (List.MyBuyables[List.Index] != none)
        return List.MyBuyables[List.Index];
}

defaultproperties
{
}
