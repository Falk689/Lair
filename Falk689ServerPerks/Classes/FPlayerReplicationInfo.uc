class FPlayerReplicationInfo extends KF_StoryPRI;

var int                 PlayerShield; // player armor level, used by FScoreboard.

replication
{
    reliable if (bNetDirty && Role == Role_Authority)
        PlayerShield;
}

function Timer()
{
    local Controller C;

    SetTimer(0.5 + FRand(), False);
    UpdatePlayerLocation();
    C = Controller(Owner);

    if (C == None)
        Return;

    if (C.Pawn == None)
    {
        PlayerHealth = 0;
        PlayerShield = 0;
    }

    else
    {
        PlayerHealth = C.Pawn.Health;

        if (FHumanPawn(C.Pawn) != none)
            PlayerShield = FHumanPawn(C.Pawn).fShield;

        else
        {
            PlayerShield = 0;
        }
    }

    if (!bBot)
    {
        if (!bReceivedPing)
            Ping = Min(int(0.25 * float(C.ConsoleCommand("GETPING"))),255);
    }
}

// store dosh on heal
function ReceiveRewardForHealing(int MedicReward, KFPawn Healee)
{
    local Falk689GameTypeBase Flk;

    Flk = Falk689GameTypeBase(Level.Game);

    // only give reward if healee is not Ringmaster
    if(!Healee.IsA('KF_RingMasterNPC'))
    {
        Score += MedicReward;
        ThreeSecondScore += MedicReward;
        Team.Score += MedicReward;

        if (Flk != none)
            Flk.FalkSetDosh(PlayerController(Owner), Max(0, MedicReward), F_Other);
    }
}
