class HuskGunPickupFalk extends HuskGunPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
   if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
   {
      if (KFGameType(Level.Game) != none)
         KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
   }

   super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}


defaultproperties
{
    Weight=8
    cost=4000
    AmmoCost=90
    BuyClipSize=30
    PowerValue=52
    SpeedValue=20
    RangeValue=100
    Description="A fireball cannon ripped from the arm of a dead Husk. Does more damage when charged up."
    ItemName="Husk Fireball Launcher"
    ItemShortName="Husk Cannon"
    AmmoItemName="Husk Gun Fuel"
    AmmoMesh=StaticMesh'KillingFloorStatics.FT_AmmoMesh'
    MaxDesireability=0.790000
    InventoryType=Class'KFMod.HuskGun'
    PickupMessage="You got a Husk Fireball Launcher"
    PickupForce="AssaultRiflePickup"
    StaticMesh=StaticMesh'KF_pickups3_Trip.HuskGun_Pickup'
    CollisionRadius=25.000000
    CollisionHeight=10.000000
    PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
    EquipmentCategoryID=3
    CorrespondingPerkIndex=5
}
