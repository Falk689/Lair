class FVetBerserker extends VeterancyTypesFalk;

static function AddCustomStats(ClientPerkRepLink Other)
{
   Other.AddCustomValue(Class'Falk689Neckshots.FNeckshots');
}

static function int GetPerkProgressInt(ClientPerkRepLink StatOther, out int FinalInt, byte CurLevel, byte ReqNum)
{
   switch(CurLevel)
   {
   case 0:
      if(ReqNum == 0)
         FinalInt = 1;         
      else
         FinalInt = 1;
      break;
         
   case 1:
      if(ReqNum == 0)
         FinalInt = 5;
      else
         FinalInt = 100;
      break;
         
   case 2:
      if(ReqNum == 0)
         FinalInt = 10;
      else
         FinalInt = 1000;
      break;

   case 3:
      if(ReqNum == 0)
         FinalInt = 30;
      else
         FinalInt = 3500;
      break;  
      
   case 4:
      if(ReqNum == 0)
         FinalInt = 85;
      else
         FinalInt = 8000;
      break;
         
   case 5:
      if(ReqNum == 0)
         FinalInt = 150;
      else
         FinalInt = 15000;
      break;
      
   case 6:
      if(ReqNum == 0)
         FinalInt = 250;
      else
         FinalInt = 30000;
      break;
      
   case 7:
      if(ReqNum == 0)
         FinalInt = 400;
      else
         FinalInt = 70000;
      break;
      
   case 8:
      if(ReqNum == 0)
         FinalInt = 600;
      else
         FinalInt = 150000;
      break;
      
   case 9:
      if(ReqNum == 0)
         FinalInt = 850;
      else
         FinalInt = 300000;
      break;
      
   case 10:
      if(ReqNum == 0)
         FinalInt = 1200;
      else
         FinalInt = 500000;
      break;
      
   case 11:
      if(ReqNum == 0)
         FinalInt = 1700;
      else
         FinalInt = 750000;
      break;
         
   case 12:
      if(ReqNum == 0)
         FinalInt = 2500;
      else
         FinalInt = 1100000;
      break; 
         
   case 13:
      if(ReqNum == 0)
         FinalInt = 3500;
      else
         FinalInt = 1500000;
      break; 
         
   case 14:
      if(ReqNum == 0)
         FinalInt = 4700;
      else
         FinalInt = 2200000;
      break; 
         
   case 15:
      if(ReqNum == 0)
         FinalInt = 6000;
      else
         FinalInt = 3000000;
      break; 
         
   case 16:
      if(ReqNum == 0)
         FinalInt = 7500;
      else
         FinalInt = 4000000;
      break; 
         
   case 17:
      if(ReqNum == 0)
         FinalInt = 9200;
      else
         FinalInt = 5200000;
      break; 
         
   case 18:
      if(ReqNum == 0)
         FinalInt = 11000;
      else
         FinalInt = 6500000;
      break; 
         
   case 19:
      if(ReqNum == 0)
         FinalInt = 13000;
      else
         FinalInt = 8000000;
      break; 
         
   case 20:
      if(ReqNum == 0)
         FinalInt = 15500;
      else
         FinalInt = 10000000;
      break;
		
   default:
      if(ReqNum == 0)
         FinalInt = 15500 + GetDoubleScaling(CurLevel, 350);
      else
         FinalInt = 10000000 + GetDoubleScaling(CurLevel, 500000);
      break;
   }
   
   if(ReqNum == 0)
      return Min(StatOther.GetCustomValueInt(Class'Falk689Neckshots.FNeckshots'), FinalInt);
    
   return Min(StatOther.RMeleeDamageStat, FinalInt);
}


// More damage on melee weapons
static function int AddDamage(KFPlayerReplicationInfo KFPRI, KFMonster Injured, KFPawn DamageTaker, int InDamage, class<DamageType> DmgType)
{
   if((class<KFWeaponDamageType>(DmgType) != none && class<KFWeaponDamageType>(DmgType).default.bIsMeleeDamage) ||
       class<DamTypeThrowingKnifeFalk>(DmgType) != none)
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return InDamage * 1.90;
      
      return InDamage * (KFPRI.ClientVeteranSkillLevel * 0.04 + 1.04);
   }
   
   return Super.AddDamage(KFPRI, Injured, DamageTaker, InDamage, DmgType);
}


// Faster fire on melee and buzzsaw
static function float GetFireSpeedMod(KFPlayerReplicationInfo KFPRI, Weapon Other)
{
	if (KFMeleeGun(Other) != none || CrossbuzzsawFalk(Other) != none)
	{
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.30;

      if (KFPRI.ClientVeteranSkillLevel <= 14)
         return KFPRI.ClientVeteranSkillLevel * 0.01 + 1.01;
      
      return (KFPRI.ClientVeteranSkillLevel - 14) * 0.02 + 1.15;
	}

	return 1.0;
}


// Faster melee movement speed
static function float GetMeleeMovementSpeedModifier(KFPlayerReplicationInfo KFPRI)
{
   local float zAdd;
   
   zAdd = 0.0;

   if (KFPRI.ClientVeteranSkillLevel >= 6 && static.InZedTime(KFPRI))
      zAdd = 4.5;

   if (KFPRI.ClientVeteranSkillLevel >= 20)
      return 0.30 + zAdd;
   
   else if (KFPRI.ClientVeteranSkillLevel >= 0 && KFPRI.ClientVeteranSkillLevel <= 10)
      return KFPRI.ClientVeteranSkillLevel * 0.01 + 0.01 + zAdd;
   
   switch (KFPRI.ClientVeteranSkillLevel)
	{
   case 11:
      return 0.12 + zAdd;
   
   case 12:
      return 0.13 + zAdd;
      
   case 13:
      return 0.14 + zAdd;
      
   case 14:
      return 0.15 + zAdd;
      
   case 15:
      return 0.17 + zAdd;
      
   case 16:
      return 0.19 + zAdd;
      
   case 17:
      return 0.21 + zAdd;
      
   case 18:
      return 0.23 + zAdd;
      
   case 19:
      return 0.25 + zAdd;
	}
	
	return 0.0 + zAdd;
}


// Reduce damage from bloat, sirens and others
static function int ReduceDamage(KFPlayerReplicationInfo KFPRI, KFPawn Injured, Pawn Instigator, int InDamage, class<DamageType> DmgType)
{

   // no damage from the freezer gun
   if (class<DamTypeFreezerGun>(DmgType) != none)
      return 0;

   
   //if (class<DamTypeBloatVomitFalk>(DmgType) != none)
   //   warn("level:"@KFPRI.ClientVeteranSkillLevel@"InZedTime:"@static.InZedTime(KFPRI));

   // vomit immunity in zed time over lvl 6
   if (class<DamTypeBloatVomitFalk>(DmgType) != none && KFPRI.ClientVeteranSkillLevel >= 6 && static.InZedTime(KFPRI))
   {  
      return 0;
   }

   // siren damage math
   else if (DmgType == class'SirenScreamDamage')
   {
      // siren immunity in zed time over lvl 6
      if (KFPRI.ClientVeteranSkillLevel >= 6 && static.InZedTime(KFPRI))
        return 0;

      else if (KFPRI.ClientVeteranSkillLevel >= 20)
         return InDamage * 0.60;

      else if (KFPRI.ClientVeteranSkillLevel >= 15) 
         return InDamage * 0.70;

      else if (KFPRI.ClientVeteranSkillLevel >= 12) 
         return InDamage * 0.75;

      else if (KFPRI.ClientVeteranSkillLevel >= 9) 
         return InDamage * 0.80;

      else if (KFPRI.ClientVeteranSkillLevel >= 6) 
         return InDamage * 0.85;

      else if (KFPRI.ClientVeteranSkillLevel >= 3) 
         return InDamage * 0.90;
   }
   
   return InDamage;
}


// Change the cost of zerk weapons
static function float GetCostScaling(KFPlayerReplicationInfo KFPRI, class<Pickup> Item)
{
   local float result;

   result = Super.GetCostScaling(KFPRI, Item);

   if (result != 1.0)
      return result;

   switch (KFPRI.ClientVeteranSkillLevel)
   {
      case 0:
         result = 0.97;
         break;
            
      case 1:
         result = 0.94;
         break;
            
      case 2:
         result = 0.91;
         break;
            
      case 3:
         result = 0.88;
         break;
            
      case 4:
         result = 0.85;
         break;
            
      case 5:
         result = 0.82;
         break;
            
      case 6:
         result = 0.79;
         break;
            
      case 7:
         result = 0.76;
         break;
            
      case 8:
         result = 0.73;
         break;
            
      case 9:
         result = 0.70;
         break;
            
      case 10:
         result = 0.67;
         break;
            
      case 11:
         result = 0.64;
         break;
            
      case 12:
         result = 0.60;
         break;

      case 13:
         result = 0.56;
         break;
            
      case 14:
         result = 0.52;
         break;

      case 15:
         result = 0.48;
         break;

      case 16:
         result = 0.44;
         break;

      case 17:
         result = 0.40;
         break;
            
      case 18:
         result = 0.36;
         break;
            
      case 19:
         result = 0.32;
         break;
            
      default:
         result = 0.25;
         break;
   }

   if (Item == class'ChainsawPickupFalk'      ||
       Item == class'KatanaPickupFalk'        ||
       Item == class'ClaymoreSwordPickupFalk' ||
       Item == class'CrossbuzzsawPickupFalk'  ||
       Item == class'ScythePickupFalk'        ||
       Item == class'MachetePickupFalk'       ||
       Item == class'AxePickupFalk'           ||
       Item == class'DwarfAxePickupFalk'      ||
	    Item == class'BaseballBatPickupFalk'  ||
	    Item == class'MenogPickupFalk'        ||
	    Item == class'BusterSworddPickupFalk' ||
		 Item == class'AngelicPickupFalk'      ||
	    Item == class'Caius_SwordPickupFalk')
   {
	       return result;
   }
	       
	return Super.GetCostScaling(KFPRI, Item);
}

// SPOILER ALERT: nope
static function bool CanBeGrabbed(KFPlayerReplicationInfo KFPRI, KFMonster Other)
{
   if (KFPRI.ClientVeteranSkillLevel >= 3)
      return False;

   return True;
}


// Add throwing knife
static function class<Grenade> GetNadeType(KFPlayerReplicationInfo KFPRI)
{
   return class'ThrowingKnifeFalk';	
}


// Give Extra Items as Default
static function AddDefaultInventory(KFPlayerReplicationInfo KFPRI, Pawn P)
{ 
   // If Level 20 or higher give them Buster Sword
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      FalkAddPerkedWeapon(class'BusterSworddFalk', KFPRI, P);
      
   // If Level 15 or higher give them Menog Mace
   else if (KFPRI.ClientVeteranSkillLevel >= 15)
      FalkAddPerkedWeapon(class'MenogFalk', KFPRI, P);
      
	// If Level 12 or higher give them Nailed Baseball Bat
   else if (KFPRI.ClientVeteranSkillLevel >= 12)
      FalkAddPerkedWeapon(class'BaseballBatFalk', KFPRI, P);
   
	// If Level 9 or higher give them Machete
   else if (KFPRI.ClientVeteranSkillLevel >= 9)
      FalkAddPerkedWeapon(class'MacheteFalk', KFPRI, P);

   super.AddDefaultInventory(KFPRI, P);
}


// Called on zed time start
static function StartZedTime(KFPlayerReplicationInfo KFPRI)
{ 
   local FHumanPawn FP;

   if (KFPRI.ClientVeteranSkillLevel >= 6)
   {
      FP = FHumanPawn(Controller(KFPRI.Owner).Pawn);

      if (FP != none && static.CanUseZedTimeSkill(KFPRI) && static.ZedTimeSkillUsed(KFPRI))
         FP.StartScaleMeleeSpeedMod();
   }
}

// Called on zed time end
static function EndZedTime(KFPlayerReplicationInfo KFPRI)
{  
   local FHumanPawn FP;

   FP = FHumanPawn(Controller(KFPRI.Owner).Pawn);

   if (FP != none)
      FP.StartScaleMeleeSpeedMod();
}


// return max health for this perk
static function int fGetMaxHealth(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      return 135;

   else if (KFPRI.ClientVeteranSkillLevel >= 15) 
      return 125;

   else if (KFPRI.ClientVeteranSkillLevel >= 12) 
      return 120;

   else if (KFPRI.ClientVeteranSkillLevel >= 9) 
      return 115;

   else if (KFPRI.ClientVeteranSkillLevel >= 6) 
      return 110;

   else if (KFPRI.ClientVeteranSkillLevel >= 3) 
      return 105;

   return 100;
}

// needed for new best weapon switch method
static function bool IsMeleeBased()
{
   return true;
}

// make us immune from bloat dot
static function bool BloatDotImmunity(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel >= 12) 
      return True;

   return False;
}

defaultproperties
{
   PerkIndex=4

   OnHUDIcon=Texture'KillingFloorHUD.Perks.Perk_Berserker'
   OnHUDGoldIcon=Texture'KillingFloor2HUD.Perk_Icons.Perk_Berserker_Gold'
   VeterancyName="Berserker"
   
   NumRequirements=2
   
   Requirements[0]="Perform %x decapitations"
   Requirements[1]="Deal %x damage with berserker weapons and throwing knives"

   SRLevelEffects(0)="4% extra damage with berserker weapons|1% faster melee attacks|1% faster fire rate with Buzzbow|1% faster melee movement|3% discount on berserker weapons|Equipped with throwing knives"
   SRLevelEffects(1)="8% extra damage with berserker weapons|2% faster melee attacks|2% faster fire rate with Buzzbow|2% faster melee movement|6% discount on berserker weapons|Equipped with throwing knives"
   SRLevelEffects(2)="12% extra damage with berserker weapons|3% faster melee attacks|3% faster fire rate with Buzzbow|3% faster melee movement|9% discount on berserker weapons|Equipped with throwing knives"
   SRLevelEffects(3)="16% extra damage with berserker weapons|4% faster melee attacks|4% faster fire rate with Buzzbow|4% faster melee movement|5% extra maximum health|10% sonic damage resistance|12% discount on berserker weapons|Equipped with throwing knives|Can't be grabbed by clots"
   SRLevelEffects(4)="20% extra damage with berserker weapons|5% faster melee attacks|5% faster fire rate with Buzzbow|5% faster melee movement|5% extra maximum health|10% sonic damage resistance|15% discount on berserker weapons|Equipped with throwing knives|Can't be grabbed by clots"
   SRLevelEffects(5)="24% extra damage with berserker weapons|6% faster melee attacks|6% faster fire rate with Buzzbow|6% faster melee movement|5% extra maximum health|10% sonic damage resistance|18% discount on berserker weapons|10% discount on tools|Equipped with throwing knives|Can't be grabbed by clots"
   SRLevelEffects(6)="28% extra damage with berserker weapons|7% faster melee attacks|7% faster fire rate with Buzzbow|7% faster melee movement|10% extra maximum health|15% sonic damage resistance|21% discount on berserker weapons|10% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
   SRLevelEffects(7)="32% extra damage with berserker weapons|8% faster melee attacks|8% faster fire rate with Buzzbow|8% faster melee movement|10% extra maximum health|15% sonic damage resistance|24% discount on berserker weapons|10% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
   SRLevelEffects(8)="36% extra damage with berserker weapons|9% faster melee attacks|9% faster fire rate with Buzzbow|9% faster melee movement|10% extra maximum health|15% sonic damage resistance|27% discount on berserker weapons|10% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
   SRLevelEffects(9)="40% extra damage with berserker weapons|10% faster melee attacks|10% faster fire rate with Buzzbow|10% faster melee movement|15% extra maximum health|20% sonic damage resistance|30% discount on berserker weapons|10% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Spawn with a Machete|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
   SRLevelEffects(10)="44% extra damage with berserker weapons|11% faster melee attacks|11% faster fire rate with Buzzbow|11% faster melee movement|15% extra maximum health|20% sonic damage resistance|33% discount on berserker weapons|20% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Spawn with a Machete|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
   SRLevelEffects(11)="48% extra damage with berserker weapons|12% faster melee attacks|12% faster fire rate with Buzzbow|12% faster melee movement|15% extra maximum health|20% sonic damage resistance|36% discount on berserker weapons|20% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Spawn with a Machete|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
   SRLevelEffects(12)="52% extra damage with berserker weapons|13% faster melee attacks|13% faster fire rate with Buzzbow|13% faster melee movement|20% extra maximum health|25% sonic damage resistance|40% discount on berserker weapons|20% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Immune to bloat bile damage over time|Spawn with a Nailbat|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
   SRLevelEffects(13)="56% extra damage with berserker weapons|14% faster melee attacks|14% faster fire rate with Buzzbow|14% faster melee movement|20% extra maximum health|25% sonic damage resistance|44% discount on berserker weapons|20% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Immune to bloat bile damage over time|Spawn with a Nailbat|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
   SRLevelEffects(14)="60% extra damage with berserker weapons|15% faster melee attacks|15% faster fire rate with Buzzbow|15% faster melee movement|20% extra maximum health|25% sonic damage resistance|48% discount on berserker weapons|20% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Immune to bloat bile damage over time|Spawn with a Nailbat|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
   SRLevelEffects(15)="64% extra damage with berserker weapons|17% faster melee attacks|17% faster fire rate with Buzzbow|17% faster melee movement|25% extra maximum health|30% sonic damage resistance|52% discount on berserker weapons|30% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Immune to bloat bile damage over time|Spawn with a Menog|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
   SRLevelEffects(16)="68% extra damage with berserker weapons|19% faster melee attacks|19% faster fire rate with Buzzbow|19% faster melee movement|25% extra maximum health|30% sonic damage resistance|56% discount on berserker weapons|30% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Immune to bloat bile damage over time|Spawn with a Menog|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
   SRLevelEffects(17)="72% extra damage with berserker weapons|21% faster melee attacks|21% faster fire rate with Buzzbow|21% faster melee movement|25% extra maximum health|30% sonic damage resistance|60% discount on berserker weapons|30% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Immune to bloat bile damage over time|Spawn with a Menog|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
   SRLevelEffects(18)="76% extra damage with berserker weapons|23% faster melee attacks|23% faster fire rate with Buzzbow|23% faster melee movement|25% extra maximum health|30% sonic damage resistance|64% discount on berserker weapons|30% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Immune to bloat bile damage over time|Spawn with a Menog|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
   SRLevelEffects(19)="80% extra damage with berserker weapons|25% faster melee attacks|25% faster fire rate with Buzzbow|25% faster melee movement|25% extra maximum health|30% sonic damage resistance|68% discount on berserker weapons|30% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Immune to bloat bile damage over time|Spawn with a Menog|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
   SRLevelEffects(20)="90% extra damage with berserker weapons|30% faster melee attacks|30% faster fire rate with Buzzbow|30% faster melee movement|35% extra maximum health|40% sonic damage resistance|75% discount on berserker weapons|40% discount on tools|Equipped with throwing knives|Can't be grabbed by clots|Immune to bloat bile damage over time|Spawn with a Buster Sword|Zed-time skill: immunity to toxic and sonic damage, and if you're currently using a melee weapon, you suffer a lesser speed reduction and you gain a brief sprint bonus when the zed time is about to wear off"
}
