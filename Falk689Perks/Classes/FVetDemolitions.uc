class FVetDemolitions extends VeterancyTypesFalk;

var int LastAwardID;  // last hitID that awarded us a nade

static function AddCustomStats(ClientPerkRepLink Other)
{
   // Needed for custom brutes kills progress
   Other.AddCustomValue(Class'Falk689ZedsFix.FBrutesKills');
}

static function int GetPerkProgressInt(ClientPerkRepLink StatOther, out int FinalInt, byte CurLevel, byte ReqNum)
{
   switch(CurLevel)
   {
   case 0:
      if(ReqNum == 0)
         FinalInt = 1;         
      else
         FinalInt = 1;
      break;
         
   case 1:
      if(ReqNum == 0)
         FinalInt = 1;
      else
         FinalInt = 100;
      break;
         
   case 2:
      if(ReqNum == 0)
         FinalInt = 2;
      else
         FinalInt = 1000;
      break;

   case 3:
      if(ReqNum == 0)
         FinalInt = 4;
      else
         FinalInt = 3500;
      break;  
      
   case 4:
      if(ReqNum == 0)
         FinalInt = 8;
      else
         FinalInt = 8000;
      break;
         
   case 5:
      if(ReqNum == 0)
         FinalInt = 15;
      else
         FinalInt = 15000;
      break;
      
   case 6:
      if(ReqNum == 0)
         FinalInt = 25;
      else
         FinalInt = 30000;
      break;
      
   case 7:
      if(ReqNum == 0)
         FinalInt = 37;
      else
         FinalInt = 70000;
      break;
      
   case 8:
      if(ReqNum == 0)
         FinalInt = 50;
      else
         FinalInt = 150000;
      break;
      
   case 9:
      if(ReqNum == 0)
         FinalInt = 65;
      else
         FinalInt = 300000;
      break;
      
   case 10:
      if(ReqNum == 0)
         FinalInt = 85;
      else
         FinalInt = 500000;
      break;
      
   case 11:
      if(ReqNum == 0)
         FinalInt = 110;
      else
         FinalInt = 750000;
      break;
         
   case 12:
      if(ReqNum == 0)
         FinalInt = 140;
      else
         FinalInt = 1100000;
      break; 
         
   case 13:
      if(ReqNum == 0)
         FinalInt = 180;
      else
         FinalInt = 1500000;
      break; 
         
   case 14:
      if(ReqNum == 0)
         FinalInt = 230;
      else
         FinalInt = 2200000;
      break; 
         
   case 15:
      if(ReqNum == 0)
         FinalInt = 300;
      else
         FinalInt = 3000000;
      break; 
         
   case 16:
      if(ReqNum == 0)
         FinalInt = 380;
      else
         FinalInt = 4000000;
      break; 
         
   case 17:
      if(ReqNum == 0)
         FinalInt = 480;
      else
         FinalInt = 5200000;
      break; 
         
   case 18:
      if(ReqNum == 0)
         FinalInt = 600;
      else
         FinalInt = 6500000;
      break; 
         
   case 19:
      if(ReqNum == 0)
         FinalInt = 750;
      else
         FinalInt = 8000000;
      break; 
         
   case 20:
      if(ReqNum == 0)
         FinalInt = 950;
      else
         FinalInt = 10000000;
      break;
		
   default:
      if(ReqNum == 0)
         FinalInt = 1100 + GetDoubleScaling(CurLevel, 350);
      else
         FinalInt = 10000000 + GetDoubleScaling(CurLevel, 500000);
      break;
   }

   if (ReqNum == 0)
      return Min(StatOther.GetCustomValueInt(Class'Falk689ZedsFix.FBrutesKills'), FinalInt);

   return Min(StatOther.RExplosivesDamageStat, FinalInt);
}

// Add damage to demoman weapons
static function int AddDamage(KFPlayerReplicationInfo KFPRI, KFMonster Injured, KFPawn DamageTaker, int InDamage, class<DamageType> DmgType)
{ 
   if (class<DamTypeFrag>(DmgType)                           != none ||
       class<DamTypePipeBomb>(DmgType)                       != none ||
       class<DamTypeM79Grenade>(DmgType)                     != none ||
       class<DamTypeM32Grenade>(DmgType)                     != none ||
       class<DamTypeM203GrenadeFalk>(DmgType)                != none ||
       class<DamTypeLAWFalk>(DmgType)                        != none ||
       class<DamTypeSPGrenade>(DmgType)                      != none ||
       class<DamTypeSealSquealExplosion>(DmgType)            != none ||
       class<DamTypeSeekerSixRocketFalk>(DmgType)            != none ||
       class<DamTypeATMine>(DmgType)                         != none ||
       class<DamTypeCLGLGrenadeFalk>(DmgType)                != none ||
       class<DamTypeTrigunFalk>(DmgType)                     != none)
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return InDamage * 1.9;

      return InDamage * (KFPRI.ClientVeteranSkillLevel * 0.04 + 1.04);
   }

   else if (class<DamTypeM4203AssaultRifleFalk>(DmgType) != none)
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return InDamage * 1.40;

      else if (KFPRI.ClientVeteranSkillLevel >= 18)
         return InDamage * 1.30;

      else if (KFPRI.ClientVeteranSkillLevel >= 15)
         return InDamage * 1.24;

      else if (KFPRI.ClientVeteranSkillLevel >= 12)
         return InDamage * 1.18;

      else if (KFPRI.ClientVeteranSkillLevel >= 9)
         return InDamage * 1.12;

      else if (KFPRI.ClientVeteranSkillLevel >= 6)
         return InDamage * 1.06;
   }

   return Super.AddDamage(KFPRI, Injured, DamageTaker, InDamage, DmgType);
}


// reduce recoil with M4+M203
static function float ModifyRecoilSpread(KFPlayerReplicationInfo KFPRI, WeaponFire Other, out float Recoil)
{
   Recoil = 1.0;

   if (AA12ExplosiveFalk(Other.Weapon) == none && M4203AssaultRifleFalk(Other.Weapon) != none)
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         Recoil -= 0.65;

      else if (KFPRI.ClientVeteranSkillLevel >= 6)
         Recoil -= 0.98 - ((KFPRI.ClientVeteranSkillLevel - 6) * 0.02);
   }

   Return Recoil;
}

// Reduce explosives and brute damage
static function int ReduceDamage(KFPlayerReplicationInfo KFPRI, KFPawn Injured, Pawn Instigator, int InDamage, class<DamageType> DmgType)
{
   // no damage from clusters
   if (class<DamTypeExplosiveBulletFalk>(DmgType) != none)
      return 0;
   
   // explosive damage reduction
   else if (class<DamTypeFrag>(DmgType)                      != none ||
            class<DamTypePipeBomb>(DmgType)                  != none ||
            class<DamTypeM79Grenade>(DmgType)                != none ||
            class<DamTypeM32Grenade>(DmgType)                != none ||
            class<DamTypeM203GrenadeFalk>(DmgType)           != none ||
            class<DamTypeLAWFalk>(DmgType)                   != none ||
            class<DamTypeRocketImpactFalk>(DmgType)          != none ||
            class<DamTypeSPGrenade>(DmgType)                 != none ||
            class<DamTypeSealSquealExplosion>(DmgType)       != none ||
            class<DamTypeSeekerSixRocketFalk>(DmgType)       != none ||
            class<DamTypeATMine>(DmgType)                    != none ||
            class<DamTypeCLGLGrenadeFalk>(DmgType)           != none ||
            class<DamTypeTrigunFalk>(DmgType)                != none)
   {
      // no explosive damage during zed time
      if (static.InZedTime(KFPRI))
         return 0;

      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return InDamage * 0.30;

      return InDamage * (0.97 - (KFPRI.ClientVeteranSkillLevel * 0.03));
   }

   // brute damage reduction
   else if (class<DamTypeBruteAttackFalk>(DmgType) != none)
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return InDamage * 0.25;

      else if (KFPRI.ClientVeteranSkillLevel >= 15)
         return InDamage * 0.50;

      else if (KFPRI.ClientVeteranSkillLevel >= 12)
         return InDamage * 0.60;

      else if (KFPRI.ClientVeteranSkillLevel >= 9)
         return InDamage * 0.70;

      else if (KFPRI.ClientVeteranSkillLevel >= 6)
         return InDamage * 0.80;

      else if (KFPRI.ClientVeteranSkillLevel >= 3)
         return InDamage * 0.90;
   }

   return Super.ReduceDamage(KFPRI, Injured, Instigator, InDamage, DmgType);
}


// Faster reload with M4+M203, M32, Seal squeal
static function float GetReloadSpeedModifier(KFPlayerReplicationInfo KFPRI, KFWeapon Other)
{
   if (M4203AssaultRifleFalk(Other) != none)
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.35;

      else if (KFPRI.ClientVeteranSkillLevel >= 6)
         return 1.02 + ((KFPRI.ClientVeteranSkillLevel - 6) * 0.02);
   }

   if (M32GrenadeLauncher(Other) != none || SealSquealHarpoonBomberFalk(Other) != none)
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.25;

      return 1.01 + KFPRI.ClientVeteranSkillLevel * 0.01;
   }

   return 1.0;
}


// Faster fire on M79/Orca Bomber/LAW
static function float GetFireSpeedMod(KFPlayerReplicationInfo KFPRI, Weapon Other)
{
   if (M4203AssaultRifleFalk(Other) != none || M79GrenadeLauncher(Other) != none || SPGrenadeLauncher(Other) != none || LAW(Other) != none || CLGLFalk(Other) != none)
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.45;

      return KFPRI.ClientVeteranSkillLevel * 0.02 + 1.02;
   }

   return 1.0;
}

// Discount on demoman stuff
static function float GetCostScaling(KFPlayerReplicationInfo KFPRI, class<Pickup> Item)
{
   local float result;

   result = Super.GetCostScaling(KFPRI, Item);

   if (result != 1.0)
      return result;

   switch (KFPRI.ClientVeteranSkillLevel)
   {
      case 0:
         result = 0.97;
         break;

      case 1:
         result = 0.94;
         break;

      case 2:
         result = 0.91;
         break;

      case 3:
         result = 0.88;
         break;

      case 4:
         result = 0.85;
         break;

      case 5:
         result = 0.82;
         break;

      case 6:
         result = 0.79;
         break;

      case 7:
         result = 0.76;
         break;

      case 8:
         result = 0.73;
         break;

      case 9:
         result = 0.70;
         break;

      case 10:
         result = 0.67;
         break;

      case 11:
         result = 0.64;
         break;

      case 12:
         result = 0.60;
         break;

      case 13:
         result = 0.56;
         break;

      case 14:
         result = 0.52;
         break;

      case 15:
         result = 0.48;
         break;

      case 16:
         result = 0.44;
         break;

      case 17:
         result = 0.40;
         break;

      case 18:
         result = 0.36;
         break;

      case 19:
         result = 0.32;
         break;

      default:
         result = 0.25;
         break;
   }

   if (Item == class'M79PickupFalk'                  ||
         Item == class'M32PickupFalk'                ||
         Item == class'LAWPickupFalk'                ||
         Item == class'LocalLAWPickupFalk'           ||
         Item == class'M4203PickupFalk'              ||
         Item == class'SPGrenadePickupFalk'          ||
         Item == class'SealSquealPickupFalk'         ||
         Item == class'SeekerSixPickupFalk'          ||
         Item == class'PipeBombPickupFalk'           ||
         Item == class'ATMinePickupFalk'             ||
         Item == class'AA12ExplosivePickupFalk'      ||
         Item == class'CLGLPickupFalk'               ||
         Item == class'TrigunPickupFalk')
   {
      return result;
   }

   return Super.GetCostScaling(KFPRI, Item);
}

// additional ammo on explosives
static function float AddNadeExtraAmmo(KFPlayerReplicationInfo KFPRI, KFAmmunition Other)
{
   if (FragAmmo(Other) != none)
      return AddExtraAmmoFor(KFPRI, class'FragAmmo');

   if (ATMineAmmoFalk(Other) != none)
      return AddExtraAmmoFor(KFPRI, class'ATMineAmmoFalk');

   if (PipeBombAmmoFalk(Other) != none)
      return AddExtraAmmoFor(KFPRI, class'PipeBombAmmoFalk');

   return 1.0;
}


// extra ammo
static function float AddExtraAmmoFor(KFPlayerReplicationInfo KFPRI, Class<Ammunition> AmmoType)
{ 
   if (AmmoType == class'FragAmmo')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 2.4;

      else if (KFPRI.ClientVeteranSkillLevel >= 15)
         return 2.0;

      else if (KFPRI.ClientVeteranSkillLevel >= 12)
         return 1.8;

      else if (KFPRI.ClientVeteranSkillLevel >= 9)
         return 1.6;

      else if (KFPRI.ClientVeteranSkillLevel >= 6)
         return 1.4;

      else if (KFPRI.ClientVeteranSkillLevel >= 3)
         return 1.2;
   }

   if (AmmoType == class'ATMineAmmoFalk')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 4.5;

      else if (KFPRI.ClientVeteranSkillLevel >= 15)
         return 3.5;

      else if (KFPRI.ClientVeteranSkillLevel >= 12)
         return 3.0;

      else if (KFPRI.ClientVeteranSkillLevel >= 9)
         return 2.5;

      else if (KFPRI.ClientVeteranSkillLevel >= 6)
         return 2.0;

      else if (KFPRI.ClientVeteranSkillLevel >= 3)
         return 1.5;
   }

   if (AmmoType == class'PipeBombAmmoFalk')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 4.5;

      else if (KFPRI.ClientVeteranSkillLevel >= 15)
         return 3.5;

      else if (KFPRI.ClientVeteranSkillLevel >= 12)
         return 3.0;

      else if (KFPRI.ClientVeteranSkillLevel >= 9)
         return 2.5;

      else if (KFPRI.ClientVeteranSkillLevel >= 6)
         return 2.0;

      else if (KFPRI.ClientVeteranSkillLevel >= 3)
         return 1.5;
   }

   if (AmmoType == class'M203AmmoFalk')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 2.0;

      else if (KFPRI.ClientVeteranSkillLevel >= 15)
         return 1.67;

      else if (KFPRI.ClientVeteranSkillLevel >= 12)
         return 1.5;

      else if (KFPRI.ClientVeteranSkillLevel >= 9)
         return 1.34;

      else if (KFPRI.ClientVeteranSkillLevel >= 6)
         return 1.17;
   }

   if (AmmoType == class'M32AmmoFalk' || AmmoType == class'SealSquealAmmoFalk')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.25;

      else if (KFPRI.ClientVeteranSkillLevel >= 15)
         return 1.18;

      else if (KFPRI.ClientVeteranSkillLevel >= 12)
         return 1.15;

      else if (KFPRI.ClientVeteranSkillLevel >= 9)
         return 1.12;

      else if (KFPRI.ClientVeteranSkillLevel >= 6)
         return 1.09;

      else if (KFPRI.ClientVeteranSkillLevel >= 3)
         return 1.06;

      else
         return 1.03;
   }


   if (AmmoType == class'AA12ExplosiveAmmoFalk')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.3;

      else if (KFPRI.ClientVeteranSkillLevel >= 15)
         return 1.2;

      else if (KFPRI.ClientVeteranSkillLevel >= 12)
         return 1.15;

      else if (KFPRI.ClientVeteranSkillLevel >= 9)
         return 1.1;

      else if (KFPRI.ClientVeteranSkillLevel >= 6)
         return 1.05;
   }


   if (AmmoType == class'M79AmmoFalk' || AmmoType == class'CLGLAmmoFalk' || AmmoType == class'SPGrenadeAmmoFalk')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 2.20;

      return 1.05 + KFPRI.ClientVeteranSkillLevel * 0.05;
   }  


   if (AmmoType == class'LAWAmmoFalk')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 2.0;

      else if (KFPRI.ClientVeteranSkillLevel >= 15)
         return 1.7;

      else if (KFPRI.ClientVeteranSkillLevel >= 12)
         return 1.5;

      else if (KFPRI.ClientVeteranSkillLevel >= 9)
         return 1.4;

      else if (KFPRI.ClientVeteranSkillLevel >= 6)
         return 1.3;

      else if (KFPRI.ClientVeteranSkillLevel >= 3)
         return 1.2;

      else
         return 1.1;
   }

   return 1.0;
}

// discount on ammo for pipebombs, mines, nades
static function float GetAmmoCostScaling(KFPlayerReplicationInfo KFPRI, class<Pickup> Item)
{
   if (Item == class'FragPickupFalk')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 0.25;

      else if (KFPRI.ClientVeteranSkillLevel >= 15)
         return 0.50;

      else if (KFPRI.ClientVeteranSkillLevel >= 9)
         return 0.75;
   }

   if (Item == class'PipeBombPickupFalk' || Item == class'ATMinePickupFalk')
   {
      switch (KFPRI.ClientVeteranSkillLevel)
      {
      case 0:
         return 0.97;

      case 1:
         return 0.94;

      case 2:
         return 0.91;

      case 3:
         return 0.88;

      case 4:
         return 0.85;

      case 5:
         return 0.82;

      case 6:
         return 0.79;

      case 7:
         return 0.76;

      case 8:
         return 0.73;

      case 9:
         return 0.70;

      case 10:
         return 0.67;

      case 11:
         return 0.64;

      case 12:
         return 0.60;

      case 13:
         return 0.56;

      case 14:
         return 0.52;

      case 15:
         return 0.48;

      case 16:
         return 0.44;

      case 17:
         return 0.40;

      case 18:
         return 0.36;

      case 19:
         return 0.32;

      default:
         return 0.25;
     }
   }

   return 1.0;
}


// Give extra items as default
static function AddDefaultInventory(KFPlayerReplicationInfo KFPRI, Pawn P)
{
   // If Level 20 or higher give them M4+M203
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      FalkAddPerkedWeapon(class'M4203AssaultRifleFalk', KFPRI, P);

   // If Level 15 or higher give them Seeker Six
   else if (KFPRI.ClientVeteranSkillLevel >= 15)
      FalkAddPerkedWeapon(class'SeekerSixRocketLauncherFalk', KFPRI, P); 

   // If Level 12 or higher give them CLGL
   else if (KFPRI.ClientVeteranSkillLevel >= 12)
      FalkAddPerkedWeapon(class'CLGLFalk', KFPRI, P);

   // If Level 9 or higher give them Trigun
   else if (KFPRI.ClientVeteranSkillLevel >= 9)
      FalkAddPerkedWeapon(class'TrigunFalk', KFPRI, P);

   super.AddDefaultInventory(KFPRI, P);
}

// skill based detonate explosives control on zed time
static function bool DetonateBulletsControlBonus(KFPlayerReplicationInfo KFPRI, optional bool fIgnoreZedTime)
{
   if ((fIgnoreZedTime || static.InZedTime(KFPRI)) && KFPRI.ClientVeteranSkillLevel >= 6)
      return True;

   return False;
}

// skill based additional explosive explosive pellet
static function int GetExplosiveProjPerFire(KFPlayerReplicationInfo KFPRI, int ProjPerFire)
{
   if (static.InZedTime(KFPRI) && KFPRI.ClientVeteranSkillLevel >= 6)
      return ProjPerFire + 3;

   return ProjPerFire;
}

// should the LAW always spawn clusters?
static function bool LAWClustersBonus(KFPlayerReplicationInfo KFPRI)
{
   return True;
}

// should our explosives be immune to sirens? 
static function bool KaboomSirenImmunity(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel >= 15)
      return True;

   return False;
}

defaultproperties
{

   PerkIndex=6

   OnHUDIcon=Texture'KillingFloor2HUD.Perk_Icons.Perk_Demolition'
   OnHUDGoldIcon=Texture'KillingFloor2HUD.Perk_Icons.Perk_Demolition_Gold'
   VeterancyName="Demoman"

   NumRequirements=2

   Requirements[0]="Kill %x brutes with explosive weaponry"
   Requirements[1]="Deal %x damage with explosive weaponry"

   SRLevelEffects(0)="4% extra explosives damage|2% faster fire rate with M79/Orca Bomber/CL/LAW80|1% faster reload with M32/Harpoon Bomber|3% extra M32/Harpoon Bomber ammo|5% extra M79/Orca Bomber/CL ammo|10% extra LAW80 ammo|3% resistance to explosives|20% additional resistance to explosive clusters|3% discount on demoman weapons|3% discount on proximity explosives|Equipped with HE grenades"
   SRLevelEffects(1)="8% extra explosives damage|4% faster fire rate with M79/Orca Bomber/CL/LAW80|2% faster reload with M32/Harpoon Bomber|3% extra M32/Harpoon Bomber ammo|10% extra M79/Orca Bomber/CL ammo|10% extra LAW80 ammo|6% resistance to explosives|20% additional resistance to explosive clusters|6% discount on demoman weapons|6% discount on proximity explosives|Equipped with HE grenades"
   SRLevelEffects(2)="12% extra explosives damage|6% faster fire rate with M79/Orca Bomber/CL/LAW80|3% faster reload with M32/Harpoon Bomber|3% extra M32/Harpoon Bomber ammo|15% extra M79/Orca Bomber/CL ammo|10% extra LAW80 ammo|9% resistance to explosives|20% additional resistance to explosive clusters|9% discount on demoman weapons|9% discount on proximity explosives|Equipped with HE grenades"
   SRLevelEffects(3)="16% extra explosives damage|8% faster fire rate with M79/Orca Bomber/CL/LAW80|4% faster reload with M32/Harpoon Bomber|6% extra M32/Harpoon Bomber ammo|20% extra M79/Orca Bomber/CL ammo|20% extra LAW80 ammo|12% resistance to explosives|20% additional resistance to explosive clusters|10% less damage from brutes|Can carry 6 HE grenades|Can carry 3 pipe bombs and antitank mines|12% discount on demoman weapons|12% discount on proximity explosives|Equipped with HE grenades"
   SRLevelEffects(4)="20% extra explosives damage|10% faster fire rate with M79/Orca Bomber/CL/LAW80|5% faster reload with M32/Harpoon Bomber|6% extra M32/Harpoon Bomber ammo|25% extra M79/Orca Bomber/CL ammo|20% extra LAW80 ammo|15% resistance to explosives|20% additional resistance to explosive clusters|10% less damage from brutes|Can carry 6 HE grenades|Can carry 3 pipe bombs and antitank mines|15% discount on demoman weapons|15% discount on proximity explosives|Equipped with HE grenades"
   SRLevelEffects(5)="24% extra explosives damage|12% faster fire rate with M79/Orca Bomber/CL/LAW80|6% faster reload with M32/Harpoon Bomber|6% extra M32/Harpoon Bomber ammo|30% extra M79/Orca Bomber/CL ammo|20% extra LAW80 ammo|18% resistance to explosives|20% additional resistance to explosive clusters|10% less damage from brutes|Can carry 6 HE grenades|Can carry 3 pipe bombs and antitank mines|18% discount on demoman weapons|18% discount on proximity explosives|10% discount on tools|Equipped with HE grenades"
   SRLevelEffects(6)="28% extra explosives damage|14% faster fire rate with M79/Orca Bomber/CL/LAW80|7% faster reload with M32/Harpoon Bomber|9% extra M32/Harpoon Bomber ammo|35% extra M79/Orca Bomber/CL ammo|30% extra LAW80 ammo|5% extra Explosive AA12 ammo|6% more damage with M4+M203 primary fire|2% faster reload with M4+M203|2% faster under-barrel reload with M4+M203|2% less recoil with M4+M203|Can carry 14 M4+M203 grenades|21% resistance to explosives|20% additional resistance to explosive clusters|20% less damage from brutes|Can carry 7 HE grenades|Can carry 4 pipe bombs and antitank mines|21% discount on demoman weapons|21% discount on proximity explosives|10% discount on tools|Equipped with HE grenades|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"
   SRLevelEffects(7)="32% extra explosives damage|16% faster fire rate with M79/Orca Bomber/CL/LAW80|8% faster reload with M32/Harpoon Bomber|9% extra M32/Harpoon Bomber ammo|40% extra M79/Orca Bomber/CL ammo|30% extra LAW80 ammo|5% extra Explosive AA12 ammo|6% more damage with M4+M203 primary fire|4% faster reload with M4+M203|4% faster under-barrel reload with M4+M203|4% less recoil with M4+M203|Can carry 14 M4+M203 grenades|24% resistance to explosives|20% additional resistance to explosive clusters|20% less damage from brutes|Can carry 7 HE grenades|Can carry 4 pipe bombs and antitank mines|24% discount on demoman weapons|24% discount on proximity explosives|10% discount on tools|Equipped with HE grenades|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"
   SRLevelEffects(8)="36% extra explosives damage|18% faster fire rate with M79/Orca Bomber/CL/LAW80|9% faster reload with M32/Harpoon Bomber|9% extra M32/Harpoon Bomber ammo|45% extra M79/Orca Bomber/CL ammo|30% extra LAW80 ammo|5% extra Explosive AA12 ammo|6% more damage with M4+M203 primary fire|6% faster reload with M4+M203|6% faster under-barrel reload with M4+M203|6% less recoil with M4+M203|Can carry 14 M4+M203 grenades|27% resistance to explosives|20% additional resistance to explosive clusters|20% less damage from brutes|Can carry 7 HE grenades|Can carry 4 pipe bombs and antitank mines|27% discount on demoman weapons|27% discount on proximity explosives|10% discount on tools|Equipped with HE grenades|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"
   SRLevelEffects(9)="40% extra explosives damage|20% faster fire rate with M79/Orca Bomber/CL/LAW80|10% faster reload with M32/Harpoon Bomber|12% extra M32/Harpoon Bomber ammo|50% extra M79/Orca Bomber/CL ammo|40% extra LAW80 ammo|10% extra Explosive AA12 ammo|12% more damage with M4+M203 primary fire|8% faster reload with M4+M203|8% faster under-barrel reload with M4+M203|8% less recoil with M4+M203|Can carry 16 M4+M203 grenades|30% resistance to explosives|20% additional resistance to explosive clusters|30% less damage from brutes|Can carry 8 HE grenades|25% discount on grenades|Can carry 5 pipe bombs and antitank mines|30% discount on demoman weapons|30% discount on proximity explosives|10% discount on tools|Equipped with HE grenades|Spawn with a Trigun|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"
   SRLevelEffects(10)="44% extra explosives damage|22% faster fire rate with M79/Orca Bomber/CL/LAW80|11% faster reload with M32/Harpoon Bomber|12% extra M32/Harpoon Bomber ammo|55% extra M79/Orca Bomber/CL ammo|40% extra LAW80 ammo|10% extra Explosive AA12 ammo|12% more damage with M4+M203 primary fire|10% faster reload with M4+M203|10% faster under-barrel reload with M4+M203|10% less recoil with M4+M203|Can carry 16 M4+M203 grenades|33% resistance to explosives|20% additional resistance to explosive clusters|30% less damage from brutes|Can carry 8 HE grenades|25% discount on grenades|Can carry 5 pipe bombs and antitank mines|33% discount on demoman weapons|33% discount on proximity explosives|20% discount on tools|Equipped with HE grenades|Spawn with a Trigun|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"
   SRLevelEffects(11)="48% extra explosives damage|24% faster fire rate with M79/Orca Bomber/CL/LAW80|12% faster reload with M32/Harpoon Bomber|12% extra M32/Harpoon Bomber ammo|60% extra M79/Orca Bomber/CL ammo|40% extra LAW80 ammo|10% extra Explosive AA12 ammo|12% more damage with M4+M203 primary fire|12% faster reload with M4+M203|12% faster under-barrel reload with M4+M203|12% less recoil with M4+M203|Can carry 16 M4+M203 grenades|36% resistance to explosives|20% additional resistance to explosive clusters|30% less damage from brutes|Can carry 8 HE grenades|25% discount on grenades|Can carry 5 pipe bombs and antitank mines|36% discount on demoman weapons|36% discount on proximity explosives|20% discount on tools|Equipped with HE grenades|Spawn with a Trigun|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"
   SRLevelEffects(12)="52% extra explosives damage|26% faster fire rate with M79/Orca Bomber/CL/LAW80|13% faster reload with M32/Harpoon Bomber|15% extra M32/Harpoon Bomber ammo|65% extra M79/Orca Bomber/CL ammo|50% extra LAW80 ammo|15% extra Explosive AA12 ammo|18% more damage with M4+M203 primary fire|14% faster reload with M4+M203|14% faster under-barrel reload with M4+M203|14% less recoil with M4+M203|Can carry 18 M4+M203 grenades|39% resistance to explosives|20% additional resistance to explosive clusters|40% less damage from brutes|Can carry 9 HE grenades|25% discount on grenades|Can carry 6 pipe bombs and antitank mines|40% discount on demoman weapons|40% discount on proximity explosives|20% discount on tools|Equipped with HE grenades|Spawn with a CL|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"
   SRLevelEffects(13)="56% extra explosives damage|28% faster fire rate with M79/Orca Bomber/CL/LAW80|14% faster reload with M32/Harpoon Bomber|15% extra M32/Harpoon Bomber ammo|70% extra M79/Orca Bomber/CL ammo|50% extra LAW80 ammo|15% extra Explosive AA12 ammo|18% more damage with M4+M203 primary fire|16% faster reload with M4+M203|16% faster under-barrel reload with M4+M203|16% less recoil with M4+M203|Can carry 18 M4+M203 grenades|42% resistance to explosives|20% additional resistance to explosive clusters|40% less damage from brutes|Can carry 9 HE grenades|25% discount on grenades|Can carry 6 pipe bombs and antitank mines|44% discount on demoman weapons|44% discount on proximity explosives|20% discount on tools|Equipped with HE grenades|Spawn with a CL|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"
   SRLevelEffects(14)="60% extra explosives damage|30% faster fire rate with M79/Orca Bomber/CL/LAW80|15% faster reload with M32/Harpoon Bomber|15% extra M32/Harpoon Bomber ammo|75% extra M79/Orca Bomber/CL ammo|50% extra LAW80 ammo|15% extra Explosive AA12 ammo|18% more damage with M4+M203 primary fire|18% faster reload with M4+M203|18% faster under-barrel reload with M4+M203|18% less recoil with M4+M203|Can carry 18 M4+M203 grenades|45% resistance to explosives|20% additional resistance to explosive clusters|40% less damage from brutes|Can carry 9 HE grenades|25% discount on grenades|Can carry 6 pipe bombs and antitank mines|48% discount on demoman weapons|48% discount on proximity explosives|20% discount on tools|Equipped with HE grenades|Spawn with a CL|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"
   SRLevelEffects(15)="64% extra explosives damage|32% faster fire rate with M79/Orca Bomber/CL/LAW80|16% faster reload with M32/Harpoon Bomber|18% extra M32/Harpoon Bomber ammo|80% extra M79/Orca Bomber/CL ammo|70% extra LAW80 ammo|20% extra Explosive AA12 ammo|24% more damage with M4+M203 primary fire|20% faster reload with M4+M203|20% faster under-barrel reload with M4+M203|20% less recoil with M4+M203|Can carry 20 M4+M203 grenades|48% resistance to explosives|20% additional resistance to explosive clusters|50% less damage from brutes|Can carry 10 HE grenades|50% discount on grenades|Grenades, proximity bombs, and their clusters are immune to sonic damage|Can carry 7 pipe bombs and antitank mines|52% discount on demoman weapons|52% discount on proximity explosives|30% discount on tools|Equipped with HE grenades|Spawn with a Seeker Six|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"
   SRLevelEffects(16)="68% extra explosives damage|34% faster fire rate with M79/Orca Bomber/CL/LAW80|17% faster reload with M32/Harpoon Bomber|18% extra M32/Harpoon Bomber ammo|85% extra M79/Orca Bomber/CL ammo|70% extra LAW80 ammo|20% extra Explosive AA12 ammo|24% more damage with M4+M203 primary fire|22% faster reload with M4+M203|22% faster under-barrel reload with M4+M203|22% less recoil with M4+M203|Can carry 20 M4+M203 grenades|51% resistance to explosives|20% additional resistance to explosive clusters|50% less damage from brutes|Can carry 10 HE grenades|50% discount on grenades|Grenades, proximity bombs, and their clusters are immune to sonic damage|Can carry 7 pipe bombs and antitank mines|56% discount on demoman weapons|56% discount on proximity explosives|30% discount on tools|Equipped with HE grenades|Spawn with a Seeker Six|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"
   SRLevelEffects(17)="72% extra explosives damage|36% faster fire rate with M79/Orca Bomber/CL/LAW80|18% faster reload with M32/Harpoon Bomber|18% extra M32/Harpoon Bomber ammo|90% extra M79/Orca Bomber/CL ammo|70% extra LAW80 ammo|20% extra Explosive AA12 ammo|24% more damage with M4+M203 primary fire|24% faster reload with M4+M203|24% faster under-barrel reload with M4+M203|24% less recoil with M4+M203|Can carry 20 M4+M203 grenades|54% resistance to explosives|20% additional resistance to explosive clusters|50% less damage from brutes|Can carry 10 HE grenades|50% discount on grenades|Grenades, proximity bombs, and their clusters are immune to sonic damage|Can carry 7 pipe bombs and antitank mines|60% discount on demoman weapons|60% discount on proximity explosives|30% discount on tools|Equipped with HE grenades|Spawn with a Seeker Six|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"
   SRLevelEffects(18)="76% extra explosives damage|38% faster fire rate with M79/Orca Bomber/CL/LAW80|19% faster reload with M32/Harpoon Bomber|18% extra M32/Harpoon Bomber ammo|95% extra M79/Orca Bomber/CL ammo|70% extra LAW80 ammo|20% extra Explosive AA12 ammo|30% more damage with M4+M203 primary fire|26% faster reload with M4+M203|26% faster under-barrel reload with M4+M203|26% less recoil with M4+M203|Can carry 20 M4+M203 grenades|57% resistance to explosives|20% additional resistance to explosive clusters|50% less damage from brutes|Can carry 10 HE grenades|50% discount on grenades|Grenades, proximity bombs, and their clusters are immune to sonic damage|Can carry 7 pipe bombs and antitank mines|64% discount on demoman weapons|64% discount on proximity explosives|30% discount on tools|Equipped with HE grenades|Spawn with a Seeker Six|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"
   SRLevelEffects(19)="80% extra explosives damage|40% faster fire rate with M79/Orca Bomber/CL/LAW80|20% faster reload with M32/Harpoon Bomber|18% extra M32/Harpoon Bomber ammo|100% extra M79/Orca Bomber/CL ammo|70% extra LAW80 ammo|20% extra Explosive AA12 ammo|30% more damage with M4+M203 primary fire|28% faster reload with M4+M203|28% faster under-barrel reload with M4+M203|28% less recoil with M4+M203|Can carry 20 M4+M203 grenades|60% resistance to explosives|20% additional resistance to explosive clusters|50% less damage from brutes|Can carry 10 HE grenades|50% discount on grenades|Grenades, proximity bombs, and their clusters are immune to sonic damage|Can carry 7 pipe bombs and antitank mines|68% discount on demoman weapons|68% discount on proximity explosives|30% discount on tools|Equipped with HE grenades|Spawn with a Seeker Six|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"
   SRLevelEffects(20)="90% extra explosives damage|45% faster fire rate with M79/Orca Bomber/CL/LAW80|25% faster reload with M32/Harpoon Bomber|25% extra M32/Harpoon Bomber ammo|120% extra M79/Orca Bomber/CL ammo|100% extra LAW80 ammo|30% extra Explosive AA12 ammo|40% more damage with M4+M203 primary fire|35% faster reload with M4+M203|35% faster under-barrel reload with M4+M203|35% less recoil with M4+M203|Can carry 24 M4+M203 grenades|70% resistance to explosives|20% additional resistance to explosive clusters|75% less damage from brutes|Can carry 12 HE grenades|75% discount on grenades|Grenades, proximity bombs, and their clusters are immune to sonic damage|Can carry 9 pipe bombs and antitank mines|75% discount on demoman weapons|75% discount on proximity explosives|40% discount on tools|Equipped with HE grenades|Spawn with an M4+M203|Zed-time skill: grenade launchers, rocket launchers, explosive grenades, and proximity bombs will generate explosive clusters on blast, explosive pellet-based weaponry will gain three additional explosive pellets, and you get immunity to explosive damage"

   LastAwardID=689
}
