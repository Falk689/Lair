class FVetSupportSpec extends VeterancyTypesFalk;

static function AddCustomStats( ClientPerkRepLink Other )
{
   // Needed for custom support damage progress
   Other.AddCustomValue(Class'Falk689WeaponsFix.FArmorWeld');
}

static function int GetPerkProgressInt(ClientPerkRepLink StatOther, out int FinalInt, byte CurLevel, byte ReqNum)
{
   switch(CurLevel)
   {
   case 0:
      if(ReqNum == 0)
         FinalInt = 1;         
      else
         FinalInt = 1;
      break;
         
   case 1:
      if(ReqNum == 0)
         FinalInt = 10;
      else
         FinalInt = 100;
      break;
         
   case 2:
      if(ReqNum == 0)
         FinalInt = 30;
      else
         FinalInt = 1000;
      break;

   case 3:
      if(ReqNum == 0)
         FinalInt = 85;
      else
         FinalInt = 3500;
      break;  
      
   case 4:
      if(ReqNum == 0)
         FinalInt = 150;
      else
         FinalInt = 8000;
      break;
         
   case 5:
      if(ReqNum == 0)
         FinalInt = 250;
      else
         FinalInt = 15000;
      break;
      
   case 6:
      if(ReqNum == 0)
         FinalInt = 400;
      else
         FinalInt = 30000;
      break;
      
   case 7:
      if(ReqNum == 0)
         FinalInt = 600;
      else
         FinalInt = 70000;
      break;
      
   case 8:
      if(ReqNum == 0)
         FinalInt = 850;
      else
         FinalInt = 150000;
      break;
      
   case 9:
      if(ReqNum == 0)
         FinalInt = 1200;
      else
         FinalInt = 300000;
      break;
      
   case 10:
      if(ReqNum == 0)
         FinalInt = 1700;
      else
         FinalInt = 500000;
      break;
      
   case 11:
      if(ReqNum == 0)
         FinalInt = 2500;
      else
         FinalInt = 750000;
      break;
         
   case 12:
      if(ReqNum == 0)
         FinalInt = 3500;
      else
         FinalInt = 1100000;
      break; 
         
   case 13:
      if(ReqNum == 0)
         FinalInt = 4700;
      else
         FinalInt = 1500000;
      break; 
         
   case 14:
      if(ReqNum == 0)
         FinalInt = 6000;
      else
         FinalInt = 2200000;
      break; 
         
   case 15:
      if(ReqNum == 0)
         FinalInt = 7500;
      else
         FinalInt = 3000000;
      break; 
         
   case 16:
      if(ReqNum == 0)
         FinalInt = 9200;
      else
         FinalInt = 4000000;
      break; 
         
   case 17:
      if(ReqNum == 0)
         FinalInt = 11000;
      else
         FinalInt = 5200000;
      break; 
         
   case 18:
      if(ReqNum == 0)
         FinalInt = 13000;
      else
         FinalInt = 6500000;
      break; 
         
   case 19:
      if(ReqNum == 0)
         FinalInt = 15500;
      else
         FinalInt = 8000000;
      break; 
         
   case 20:
      if(ReqNum == 0)
         FinalInt = 18500;
      else
         FinalInt = 10000000;
      break;
		
   default:
      if(ReqNum == 0)
         FinalInt = 15500 + GetDoubleScaling(CurLevel, 350);
      else
         FinalInt = 10000000 + GetDoubleScaling(CurLevel, 500000);
      break;	
   }
   
   if(ReqNum == 0)
      return Min(StatOther.GetCustomValueInt(Class'Falk689WeaponsFix.FArmorWeld'), FinalInt);
         
   return Min(StatOther.RShotgunDamageStat, FinalInt);
}


// Increase carry weight
static function int AddCarryMaxWeight(KFPlayerReplicationInfo KFPRI)
{
   if(KFPRI.ClientVeteranSkillLevel >= 20)
      return 6;
      
   else if(KFPRI.ClientVeteranSkillLevel >= 15)
      return 4;
   
   else if(KFPRI.ClientVeteranSkillLevel >= 12)
      return 3;
   
   else if(KFPRI.ClientVeteranSkillLevel >= 9)
      return 2;
   
   else if(KFPRI.ClientVeteranSkillLevel >= 6)
      return 1;
   
   return 0;
}


// Reset cost of nades
static function float GetAmmoCostScaling(KFPlayerReplicationInfo KFPRI, class<Pickup> Item)
{
   return 1.0;
}


// Change the cost of shotguns
static function float GetCostScaling(KFPlayerReplicationInfo KFPRI, class<Pickup> Item)
{
   local float result;

   result = Super.GetCostScaling(KFPRI, Item);

   if (result != 1.0)
      return result;

   switch (KFPRI.ClientVeteranSkillLevel)
   {
      case 0:
         result = 0.97;
         break;
            
      case 1:
         result = 0.94;
         break;
            
      case 2:
         result = 0.91;
         break;
            
      case 3:
         result = 0.88;
         break;
            
      case 4:
         result = 0.85;
         break;
            
      case 5:
         result = 0.82;
         break;
            
      case 6:
         result = 0.79;
         break;
            
      case 7:
         result = 0.76;
         break;
            
      case 8:
         result = 0.73;
         break;
            
      case 9:
         result = 0.70;
         break;
            
      case 10:
         result = 0.67;
         break;
            
      case 11:
         result = 0.64;
         break;
            
      case 12:
         result = 0.60;
         break;

      case 13:
         result = 0.56;
         break;
            
      case 14:
         result = 0.52;
         break;

      case 15:
         result = 0.48;
         break;

      case 16:
         result = 0.44;
         break;

      case 17:
         result = 0.40;
         break;
            
      case 18:
         result = 0.36;
         break;
            
      case 19:
         result = 0.32;
         break;
            
      default:
         result = 0.25;
         break;
   }

	if (Item == class'ShotgunPickupFalk'                    ||
       Item == class'LocalShotgunPickupFalk'               ||
       Item == class'AshotPickupFalk'                      ||
	    Item == class'KSGPickupFalk'                        ||
	    Item == class'BoomStickPickupFalk'                  ||
	    Item == class'BenelliPickupFalk'                    ||
	    Item == class'LocalBenelliPickupFalk'               ||
	    Item == class'AA12PickupFalk'                       ||
	    Item == class'NailGunPickupFalk'                    ||
	    Item == class'SPShotGunPickupFalk'                  ||
	    Item == class'Spas12PickupFalk'                     ||
	    Item == class'LocalSpas12PickupFalk'                ||
	    Item == class'LocalSpas12LinuxPickupFalk'           ||
		 Item == class'MTS255PickupFalk'                     ||
		 Item == class'W1300_Compact_EditionPickupFalk'      ||
		 Item == class'LocalW1300_Compact_EditionPickupFalk' ||
		 Item == class'Express870PickupFalk'                 ||
		 Item == class'LocalExpress870PickupFalk'            ||
		 Item == class'Moss12PickupFalk'                     ||
		 Item == class'LocalMoss12PickupFalk'                ||
       Item == class'FNTPSPickupFalk'                      ||
       Item == class'LocalFNTPSPickupFalk'                 ||
       Item == class'KS23SAPickupFalk'                     ||
       Item == class'LocalKS23SAPickupFalk')
	       return result;
	       
	return Super.GetCostScaling(KFPRI, Item);
}

// additional damage on medium zeds with shotguns
static function int AddShotgunDamage(class<DamageType> DmgType, int InDamage)
{
   return InDamage * 1.5;
}

// Add damage to shotguns
static function int AddDamage(KFPlayerReplicationInfo KFPRI, KFMonster Injured, KFPawn DamageTaker, int InDamage, class<DamageType> DmgType)
{
	if (DmgType == class'DamTypeShotgunFalk'               ||
       DmgType == class'DamTypeAshotFalk'                 ||
	    DmgType == class'DamTypeBoomStickFalk'             ||
	    DmgType == class'DamTypeBoomStickAltFalk'          ||
	    DmgType == class'DamTypeAA12Falk'                  ||
       DmgType == class'DamTypeBenelliFalk'               ||
       DmgType == class'DamTypeKSGFalk'                   ||
       DmgType == class'DamTypeNailGunFalk'               ||
       DmgType == class'DamTypeSPShotgunFalk'             ||
       DmgType == class'DamTypeSpas12Falk'                ||
		 DmgType == class'DamTypeMTS255Falk'                ||
		 DmgType == class'DamTypeW1300_Compact_EditionFalk' ||
		 DmgType == class'DamTypeExpress870Falk'            ||
		 DmgType == class'DamTypeMoss12Falk'                ||
       DmgType == class'DamTypeFNTPSFalk'                 ||
       DmgType == class'DamTypeKS23SAFalk')
	{
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return InDamage * 1.9;
	   
      return InDamage * (0.04 * KFPRI.ClientVeteranSkillLevel + 1.04);
   }
    
   return Super.AddDamage(KFPRI, Injured, DamageTaker, InDamage, DmgType);
}

// Improve penetration damage
static function float GetShotgunPenetrationDamageMulti(KFPlayerReplicationInfo KFPRI, float DefaultPenDamageReduction)
{
	local float PenDamageInverse;
   
	PenDamageInverse = 1.0 - FMax(0, DefaultPenDamageReduction);
   
   if (KFPRI.ClientVeteranSkillLevel >= 20)
	   return PenDamageInverse * 0.95 + DefaultPenDamageReduction;
      
   else if (KFPRI.ClientVeteranSkillLevel > 10)
   {
      switch (KFPRI.ClientVeteranSkillLevel)
      {
         case 11:
            return PenDamageInverse * 0.49 + DefaultPenDamageReduction;
            
         case 12:
            return PenDamageInverse * 0.54 + DefaultPenDamageReduction;
            
         case 13:
            return PenDamageInverse * 0.59 + DefaultPenDamageReduction;
            
         case 14:
            return PenDamageInverse * 0.64 + DefaultPenDamageReduction;
            
         case 15:
            return PenDamageInverse * 0.69 + DefaultPenDamageReduction;
            
         case 16:
            return PenDamageInverse * 0.74 + DefaultPenDamageReduction;
            
         case 17:
            return PenDamageInverse * 0.79 + DefaultPenDamageReduction;
            
         case 18:
            return PenDamageInverse * 0.84 + DefaultPenDamageReduction;
            
         case 19:
            return PenDamageInverse * 0.89 + DefaultPenDamageReduction;
      }
   }
   
   return PenDamageInverse * (0.04 * (KFPRI.ClientVeteranSkillLevel + 1)) + DefaultPenDamageReduction;
}


// Add extra ammo for shotguns
static function float AddExtraAmmoFor(KFPlayerReplicationInfo KFPRI, Class<Ammunition> AmmoType)
{
	if (AmmoType == class'ShotgunAmmoFalk'               || 
       AmmoType == class'AshotAmmoFalk'                 ||
	    AmmoType == class'BoomStickAmmoFalk'             ||
	    AmmoType == class'AA12AmmoFalk'                  ||
       AmmoType == class'BenelliAmmoFalk'               ||
       AmmoType == class'KSGAmmoFalk'                   || 
       AmmoType == class'KSGAmmoFalk'                   || 
       AmmoType == class'NailGunAmmoFalk'               ||
       AmmoType == class'SPShotgunAmmoFalk'             ||
       AmmoType == class'SpasAmmoFalk'                  ||
	    AmmoType == class'MTS255AmmoFalk'                ||
		 AmmoType == class'W1300_Compact_EditionAmmoFalk' ||
		 AmmoType == class'Express870AmmoFalk'            ||
		 AmmoType == class'Moss12AmmoFalk'                ||
       AmmoType == class'FNTPSAmmoFalk'                 ||
       AmmoType == class'KS23SAAmmoFalk')
   {
      if(KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.45;
      
      return 0.02 * KFPRI.ClientVeteranSkillLevel + 1.02;
   }
	
	return 1.0;
}

// Change fire rate of pump-action shotguns
static function float GetFireSpeedMod(KFPlayerReplicationInfo KFPRI, Weapon Other)
{
	if (ShotgunFalk(Other) != none     || LocalShotgunFalk(Other) != none           || KSGShotgunFalk(Other) != none                  || FNTPSFalk(Other) != none            || Spas12Falk(Other) != none             ||
       Express870Falk(Other) != none  || W1300_Compact_EditionFalk(Other) != none  || KS23SAShotgunFalk(Other) != none               || Moss12Falk(Other) != none           || LocalMoss12Falk(Other) != none        ||
       LocalSpas12Falk(Other) != none || LocalFNTPSFalk(Other) != none             || LocalW1300_Compact_EditionFalk(Other) != none  || LocalExpress870Falk(Other) != none  || LocalKS23SAShotgunFalk(Other) != none || LocalSpas12LinuxFalk(Other) != none)
	{
	   if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.25;
	   
      return 0.01 * KFPRI.ClientVeteranSkillLevel + 1.01;
	}
	
	return 1.0;
}

// Faster welding and unwelding
static function float GetWeldSpeedModifier(KFPlayerReplicationInfo KFPRI)
{
   local float result;
   
   switch (KFPRI.ClientVeteranSkillLevel)
   {
      case 0:
         result = 1.06;
         break;
            
      case 1:
         result = 1.12;
         break;
            
      case 2:
         result = 1.18;
         break;
            
      case 3:
         result = 1.25;
         break;
            
      case 4:
         result = 1.32;
         break;
            
      case 5:
         result = 1.38;
         break;
            
      case 6:
         result = 1.45;
         break;
            
      case 7:
         result = 1.51;
         break;
            
      case 8:
         result = 1.57;
         break;
            
      case 9:
         result = 1.64;
         break;
            
      case 10:
         result = 1.70;
         break;
            
      case 11:
         result = 1.76;
         break;
            
      case 12:
         result = 1.85;
         break;

      case 13:
         result = 1.92;
         break;
            
      case 14:
         result = 2.0;
         break;

      case 15:
         result = 2.06;
         break;

      case 16:
         result = 2.13;
         break;

      case 17:
         result = 2.2;
         break;
            
      case 18:
         result = 2.27;
         break;
            
      case 19:
         result = 2.35;
         break;
            
      default:
         result = 2.60;
         break;
   }
   
   return result;
}

// Give Extra Items as Default
static function AddDefaultInventory(KFPlayerReplicationInfo KFPRI, Pawn P)
{ 
   // If Level 20 or higher give them Express 870
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      FalkAddPerkedWeapon(class'Express870Falk', KFPRI, P);
      
   // If Level 15 or higher give them SPAS12
   else if (KFPRI.ClientVeteranSkillLevel >= 15)
      FalkAddPerkedWeapon(class'Spas12Falk', KFPRI, P);
      
	// If Level 12 or higher give them Moss12
   else if (KFPRI.ClientVeteranSkillLevel >= 12)
      FalkAddPerkedWeapon(class'Moss12Falk', KFPRI, P);
   
	// If Level 9 or higher give them Cyclope
   else if (KFPRI.ClientVeteranSkillLevel >= 9)
      FalkAddPerkedWeapon(class'AshotFalk', KFPRI, P);

   super.AddDefaultInventory(KFPRI, P);
}


// shotgun spread multiplier - replaced by more pellets on shotguns and armor welder
static function float GetShotgunSpreadMultiplier(KFPlayerReplicationInfo KFPRI, Weapon fW)
{
   if (static.InZedTime(KFPRI) && KFPRI.ClientVeteranSkillLevel >= 6 && SparkGunBaseFalk(fW) != none)
   {
      return 3000.0;
   }

   return 1.0;
}

// skill based extra shotgun pellet
static function int GetShotgunProjPerFire(KFPlayerReplicationInfo KFPRI, int ProjPerFire, Weapon fW)
{
   if (static.InZedTime(KFPRI) && KFPRI.ClientVeteranSkillLevel >= 6)
   {
      if (SparkGunBaseFalk(fW) != none)
         return 5;

      return int(ProjPerFire * 1.5);
   }

   return ProjPerFire;
}

// can this perk use the armor welder?
static function bool CanWeldArmor()
{
   return true;
}

// armor welder boost based on skill level
static function float GetArmorWeldBoost(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      return 0.75;
      
   else if (KFPRI.ClientVeteranSkillLevel >= 15)
      return 0.55;
      
   else if (KFPRI.ClientVeteranSkillLevel >= 12)
      return 0.4;

   else if (KFPRI.ClientVeteranSkillLevel >= 9)
      return 0.27;
   
   else if (KFPRI.ClientVeteranSkillLevel >= 6)
      return 0.15;

   else if (KFPRI.ClientVeteranSkillLevel >= 3)
      return 0.05;

   return 0.0;
}

// armor welder ammo usage reduction
static function int GetArmorWelderAmmoReduction(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      return 10;
      
   else if (KFPRI.ClientVeteranSkillLevel >= 15)
      return 5;
   
   else if (KFPRI.ClientVeteranSkillLevel >= 6)
      return 2;

   return 0;
}

// spark gun reload speed modifier
static function float GetSparkGunChargeRate(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      return 2.5;
      
   else if (KFPRI.ClientVeteranSkillLevel >= 3)
      return 0.08 * (KFPRI.ClientVeteranSkillLevel - 2) + 1.0;

   return 1.0;
}

// Add nail bombs
static function class<Grenade> GetNadeType(KFPlayerReplicationInfo KFPRI)
{
	return class'NailBombFalk';
}


// reduce recoil for SPShotgun alt fire
static function float ModifyRecoilSpread(KFPlayerReplicationInfo KFPRI, WeaponFire Other, out float Recoil)
{
	Recoil = 1.0;
	
	if (SPShotgunAltFire(Other) != none)
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         Recoil -= 0.9;
      
      else
         Recoil -= (KFPRI.ClientVeteranSkillLevel + 1) * 0.04;
	}
	
	Return Recoil;
}

// Return the spark radius modifier
static function float GetSparkRadiusMod(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      return 2.0;

   else if (KFPRI.ClientVeteranSkillLevel >= 15)
      return 1.80;

   else if (KFPRI.ClientVeteranSkillLevel >= 12)
      return 1.50;

   else if (KFPRI.ClientVeteranSkillLevel >= 6)
      return 1.25;

   return 1.0;
}

defaultproperties
{
   PerkIndex=1

   OnHUDIcon=Texture'KillingFloorHUD.Perks.Perk_Support'
   OnHUDGoldIcon=Texture'KillingFloor2HUD.Perk_Icons.Perk_Support_Gold'
   VeterancyName="Support"

   NumRequirements=2
	
   Requirements[0]="Repair %x armor points"
   Requirements[1]="Deal %x damage with shotguns and nail bombs"
	
   SRLevelEffects(0)="4% more damage with shotguns|4% better penetration with shotguns|1% faster fire rate with pump-action shotguns|2% extra shotguns ammo|4% reduced recoil with MZT secondary fire|6% faster door welding/unwelding|3% discount on shotguns|Equipped with nail bombs|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds"
   SRLevelEffects(1)="8% more damage with shotguns|8% better penetration with shotguns|2% faster fire rate with pump-action shotguns|4% extra shotguns ammo|8% reduced recoil with MZT secondary fire|12% faster door welding/unwelding|6% discount on shotguns|Equipped with nail bombs|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds"
   SRLevelEffects(2)="12% more damage with shotguns|12% better penetration with shotguns|3% faster fire rate with pump-action shotguns|6% extra shotguns ammo|12% reduced recoil with MZT secondary fire|18% faster door welding/unwelding|9% discount on shotguns|Equipped with nail bombs|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds"
   SRLevelEffects(3)="16% more damage with shotguns|16% better penetration with shotguns|4% faster fire rate with pump-action shotguns|8% extra shotguns ammo|16% reduced recoil with MZT secondary fire|25% faster door welding/unwelding|33% more efficiency with armor welder|8% faster Spark recharge|12% discount on shotguns|Equipped with nail bombs|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds"
   SRLevelEffects(4)="20% more damage with shotguns|20% better penetration with shotguns|5% faster fire rate with pump-action shotguns|10% extra shotguns ammo|20% reduced recoil with MZT secondary fire|32% faster door welding/unwelding|33% more efficiency with armor welder|16% faster Spark recharge|15% discount on shotguns|Equipped with nail bombs|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds"
   SRLevelEffects(5)="24% more damage with shotguns|24% better penetration with shotguns|6% faster fire rate with pump-action shotguns|12% extra shotguns ammo|24% reduced recoil with MZT secondary fire|38% faster door welding/unwelding|33% more efficiency with armor welder|24% faster Spark recharge|18% discount on shotguns|10% discount on tools|Equipped with nail bombs|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds"
   SRLevelEffects(6)="28% more damage with shotguns|28% better penetration with shotguns|7% faster fire rate with pump-action shotguns|14% extra shotguns ammo|28% reduced recoil with MZT secondary fire|45% faster door welding/unwelding|100% more efficiency with armor welder|32% faster Spark recharge|25% extra Spark darts area of effect|10% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 16kg|21% discount on shotguns|10% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
   SRLevelEffects(7)="32% more damage with shotguns|32% better penetration with shotguns|8% faster fire rate with pump-action shotguns|16% extra shotguns ammo|32% reduced recoil with MZT secondary fire|51% faster door welding/unwelding|100% more efficiency with armor welder|40% faster Spark recharge|25% extra Spark darts area of effect|10% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 16kg|24% discount on shotguns|10% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
   SRLevelEffects(8)="36% more damage with shotguns|36% better penetration with shotguns|9% faster fire rate with pump-action shotguns|18% extra shotguns ammo|36% reduced recoil with MZT secondary fire|57% faster door welding/unwelding|100% more efficiency with armor welder|48% faster Spark recharge|25% extra Spark darts area of effect|10% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 16kg|27% discount on shotguns|10% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
   SRLevelEffects(9)="40% more damage with shotguns|40% better penetration with shotguns|10% faster fire rate with pump-action shotguns|20% extra shotguns ammo|40% reduced recoil with MZT secondary fire|64% faster door welding/unwelding|180% more efficiency with armor welder|56% faster Spark recharge|50% extra Spark darts area of effect|10% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 17kg|30% discount on shotguns|10% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Spawn with a Cyclope|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
   SRLevelEffects(10)="44% more damage with shotguns|44% better penetration with shotguns|11% faster fire rate with pump-action shotguns|22% extra shotguns ammo|44% reduced recoil with MZT secondary fire|70% faster door welding/unwelding|180% more efficiency with armor welder|64% faster Spark recharge|50% extra Spark darts area of effect|10% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 17kg|33% discount on shotguns|20% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Spawn with a Cyclope|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
   SRLevelEffects(11)="48% more damage with shotguns|49% better penetration with shotguns|12% faster fire rate with pump-action shotguns|24% extra shotguns ammo|48% reduced recoil with MZT secondary fire|76% faster door welding/unwelding|180% more efficiency with armor welder|72% faster Spark recharge|50% extra Spark darts area of effect|10% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 17kg|36% discount on shotguns|20% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Spawn with a Cyclope|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
   SRLevelEffects(12)="52% more damage with shotguns|54% better penetration with shotguns|13% faster fire rate with pump-action shotguns|26% extra shotguns ammo|52% reduced recoil with MZT secondary fire|85% faster door welding/unwelding|266% more efficiency with armor welder|80% faster Spark recharge|80% extra Spark darts area of effect|10% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 18kg|40% discount on shotguns|20% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Spawn with a Moss12|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
   SRLevelEffects(13)="56% more damage with shotguns|59% better penetration with shotguns|14% faster fire rate with pump-action shotguns|28% extra shotguns ammo|56% reduced recoil with MZT secondary fire|92% faster door welding/unwelding|266% more efficiency with armor welder|88% faster Spark recharge|80% extra Spark darts area of effect|10% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 18kg|44% discount on shotguns|20% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Spawn with a Moss12|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
   SRLevelEffects(14)="60% more damage with shotguns|64% better penetration with shotguns|15% faster fire rate with pump-action shotguns|30% extra shotguns ammo|60% reduced recoil with MZT secondary fire|100% faster door welding/unwelding|266% more efficiency with armor welder|96% faster Spark recharge|80% extra Spark darts area of effect|10% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 18kg|48% discount on shotguns|20% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Spawn with a Moss12|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
   SRLevelEffects(15)="64% more damage with shotguns|69% better penetration with shotguns|16% faster fire rate with pump-action shotguns|32% extra shotguns ammo|64% reduced recoil with MZT secondary fire|106% faster door welding/unwelding|366% more efficiency with armor welder|104% faster Spark recharge|125% extra Spark darts area of effect|25% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 19kg|52% discount on shotguns|30% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Spawn with a SPAS12|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
   SRLevelEffects(16)="68% more damage with shotguns|74% better penetration with shotguns|17% faster fire rate with pump-action shotguns|34% extra shotguns ammo|68% reduced recoil with MZT secondary fire|113% faster door welding/unwelding|366% more efficiency with armor welder|112% faster Spark recharge|125% extra Spark darts area of effect|25% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 19kg|56% discount on shotguns|30% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Spawn with a SPAS12|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
   SRLevelEffects(17)="72% more damage with shotguns|79% better penetration with shotguns|18% faster fire rate with pump-action shotguns|36% extra shotguns ammo|72% reduced recoil with MZT secondary fire|120% faster door welding/unwelding|366% more efficiency with armor welder|120% faster Spark recharge|125% extra Spark darts area of effect|25% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 19kg|60% discount on shotguns|30% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Spawn with a SPAS12|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
   SRLevelEffects(18)="76% more damage with shotguns|84% better penetration with shotguns|19% faster fire rate with pump-action shotguns|38% extra shotguns ammo|76% reduced recoil with MZT secondary fire|127% faster door welding/unwelding|366% more efficiency with armor welder|128% faster Spark recharge|125% extra Spark darts area of effect|25% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 19kg|64% discount on shotguns|30% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Spawn with a SPAS12|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
   SRLevelEffects(19)="80% more damage with shotguns|89% better penetration with shotguns|20% faster fire rate with pump-action shotguns|40% extra shotguns ammo|80% reduced recoil with MZT secondary fire|135% faster door welding/unwelding|366% more efficiency with armor welder|136% faster Spark recharge|125% extra Spark darts area of effect|25% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 19kg|68% discount on shotguns|30% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Spawn with a SPAS12|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
   SRLevelEffects(20)="90% more damage with shotguns|95% better penetration with shotguns|25% faster fire rate with pump-action shotguns|45% extra shotguns ammo|90% reduced recoil with MZT secondary fire|160% faster door welding/unwelding|500% more efficiency with armor welder|150% faster Spark recharge|200% extra Spark darts area of effect|50% reduced ammo consumption on welding/unwelding/repairing|Can carry up to 21kg|75% discount on shotguns|40% discount on tools|Welder is capable of repairing armors|50% extra damage with shotguns against medium zeds|Equipped with nail bombs|Spawn with an Express 870|Zed-time skill: pellets count is increased by 50% (rounded down) and Spark fires five pellets"
}
