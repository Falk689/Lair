class FVetArtillerist extends VeterancyTypesFalk;

static function AddCustomStats(ClientPerkRepLink Other)
{   
   // Needed for artillerist damage progress
   Other.AddCustomValue(Class'Falk689WeaponsFixBase.FArtilleristDamage');
   Other.AddCustomValue(Class'Falk689ArtilleristKills.FArtilleristKills');   
}

static function int GetPerkProgressInt(ClientPerkRepLink StatOther, out int FinalInt, byte CurLevel, byte ReqNum)
{
   switch(CurLevel)
   {
   case 0:
      if(ReqNum == 0)
         FinalInt = 1;         
      else
         FinalInt = 1;
      break;
         
   case 1:
      if(ReqNum == 0)
         FinalInt = 10;
      else
         FinalInt = 100;
      break;
         
   case 2:
      if(ReqNum == 0)
         FinalInt = 30;
      else
         FinalInt = 1000;
      break;

   case 3:
      if(ReqNum == 0)
         FinalInt = 85;
      else
         FinalInt = 3500;
      break;  
      
   case 4:
      if(ReqNum == 0)
         FinalInt = 150;
      else
         FinalInt = 8000;
      break;
         
   case 5:
      if(ReqNum == 0)
         FinalInt = 250;
      else
         FinalInt = 15000;
      break;
      
   case 6:
      if(ReqNum == 0)
         FinalInt = 400;
      else
         FinalInt = 30000;
      break;
      
   case 7:
      if(ReqNum == 0)
         FinalInt = 600;
      else
         FinalInt = 70000;
      break;
      
   case 8:
      if(ReqNum == 0)
         FinalInt = 850;
      else
         FinalInt = 150000;
      break;
      
   case 9:
      if(ReqNum == 0)
         FinalInt = 1200;
      else
         FinalInt = 300000;
      break;
      
   case 10:
      if(ReqNum == 0)
         FinalInt = 1700;
      else
         FinalInt = 500000;
      break;
      
   case 11:
      if(ReqNum == 0)
         FinalInt = 2500;
      else
         FinalInt = 750000;
      break;
         
   case 12:
      if(ReqNum == 0)
         FinalInt = 3500;
      else
         FinalInt = 1100000;
      break; 
         
   case 13:
      if(ReqNum == 0)
         FinalInt = 4700;
      else
         FinalInt = 1500000;
      break; 
         
   case 14:
      if(ReqNum == 0)
         FinalInt = 6000;
      else
         FinalInt = 2200000;
      break; 
         
   case 15:
      if(ReqNum == 0)
         FinalInt = 7500;
      else
         FinalInt = 3000000;
      break; 
         
   case 16:
      if(ReqNum == 0)
         FinalInt = 9200;
      else
         FinalInt = 4000000;
      break; 
         
   case 17:
      if(ReqNum == 0)
         FinalInt = 11000;
      else
         FinalInt = 5200000;
      break; 
         
   case 18:
      if(ReqNum == 0)
         FinalInt = 13000;
      else
         FinalInt = 6500000;
      break; 
         
   case 19:
      if(ReqNum == 0)
         FinalInt = 15500;
      else
         FinalInt = 8000000;
      break; 
         
   case 20:
      if(ReqNum == 0)
         FinalInt = 18500;
      else
         FinalInt = 10000000;
      break;
		
   default:
      if(ReqNum == 0)
         FinalInt = 18500 + GetDoubleScaling(CurLevel, 350);
      else
         FinalInt = 10000000 + GetDoubleScaling(CurLevel, 500000);
      break;
   }
   
   if(ReqNum == 0)
         return Min(StatOther.GetCustomValueInt(Class'Falk689ArtilleristKills.FArtilleristKills'), FinalInt);
   
   else
      return Min(StatOther.GetCustomValueInt(Class'Falk689WeaponsFixBase.FArtilleristDamage'), FinalInt);
}



// Change the cost of artillerist stuff
static function float GetCostScaling(KFPlayerReplicationInfo KFPRI, class<Pickup> Item)
{
   local float result;

   result = Super.GetCostScaling(KFPRI, Item);

   if (result != 1.0)
      return result;

   switch (KFPRI.ClientVeteranSkillLevel)
   {
      case 0:
         result = 0.97;
         break;
            
      case 1:
         result = 0.94;
         break;
            
      case 2:
         result = 0.91;
         break;
            
      case 3:
         result = 0.88;
         break;
            
      case 4:
         result = 0.85;
         break;
            
      case 5:
         result = 0.82;
         break;
            
      case 6:
         result = 0.79;
         break;
            
      case 7:
         result = 0.76;
         break;
            
      case 8:
         result = 0.73;
         break;
            
      case 9:
         result = 0.70;
         break;
            
      case 10:
         result = 0.67;
         break;
            
      case 11:
         result = 0.64;
         break;
            
      case 12:
         result = 0.60;
         break;

      case 13:
         result = 0.56;
         break;
            
      case 14:
         result = 0.52;
         break;

      case 15:
         result = 0.48;
         break;

      case 16:
         result = 0.44;
         break;

      case 17:
         result = 0.40;
         break;
            
      case 18:
         result = 0.36;
         break;
            
      case 19:
         result = 0.32;
         break;
            
      default:
         result = 0.25;
         break;
   }

   if (Item == class'RPK47PickupFalk'                   ||
       Item == class'HK23EPickupFalk'                   ||
       Item == class'XMV850PickupFalk'                  ||
       Item == class'M249PickupFalk'                    ||
       Item == class'SW76PickupFalk'                    ||
       Item == class'MercyPickupFalk'                   ||
       Item == class'M60PickupFalk'                     ||
       Item == class'THR40DTPickupFalk'                 ||
       Item == class'StingerPickupFalk'                 ||
       Item == class'GPMGPickup')
          return result;
	       
	return Super.GetCostScaling(KFPRI, Item);
}

// Add damage to artillerist weapons
static function int AddDamage(KFPlayerReplicationInfo KFPRI, KFMonster Injured, KFPawn DamageTaker, int InDamage, class<DamageType> DmgType)
{
   if (DmgType == class'DamTypeRPK47Falk'               ||
       DmgType == class'DamTypeHK23EFalk'               ||
       DmgType == class'DamTypeXMV850Falk'              ||
       DmgType == class'DamTypeM249Falk'                ||
       DmgType == class'DamTypeSW76Falk'                ||
       DmgType == class'DamTypeMercyFalk'               ||
       DmgType == class'DamTypeM60Falk'                 ||
       DmgType == class'DamTypeTHR40DTFalk'             ||
       DmgType == class'DamTypeStingerFalk'             ||
       DmgType == class'DamTypeGPMG')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return InDamage * 1.9;
	   
      return InDamage * (0.04 * KFPRI.ClientVeteranSkillLevel + 1.04);   
   }
   
   return Super.AddDamage(KFPRI, Injured, DamageTaker, InDamage, DmgType);
}

// Cap to 3 nades
static function float AddNadeExtraAmmo(KFPlayerReplicationInfo KFPRI, KFAmmunition Other)
{
   if (FragAmmo(Other) != none)
      return AddExtraAmmoFor(KFPRI, class'FragAmmo');
}

// Add extra ammo for artillerist stuff
static function float AddExtraAmmoFor(KFPlayerReplicationInfo KFPRI, Class<Ammunition> AmmoType)
{
   if (AmmoType == class'FragAmmo')
      return 0.6;

	if (AmmoType == class'RPK47AmmoFalk'                 ||
       AmmoType == class'HK23EAmmoFalk'                 ||
       AmmoType == class'XMV850AmmoFalk'                ||
       AmmoType == class'M249AmmoFalk'                  ||
       AmmoType == class'SW76AmmoFalk'                  ||
       AmmoType == class'MercyAmmoFalk'                 ||
       AmmoType == class'M60AmmoFalk'                   ||
       AmmoType == class'THR40DTAmmoFalk'               ||
       AmmoType == class'StingerAmmoFalk'               ||
       AmmoType == class'GPMGAmmo')
   {
      if(KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.25;
      
      return 0.01 * KFPRI.ClientVeteranSkillLevel + 1.01;
   }
	
	return 1.0;
}

// Larger magazines
static function float GetMagCapacityMod(KFPlayerReplicationInfo KFPRI, KFWeapon Other)
{
   // stinger code, since all the ammo are in the magazine
   if (StingerFalk(Other) != none || MercyFalk(Other) != none)
   {
      if(KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.25;
      
      return 0.01 * KFPRI.ClientVeteranSkillLevel + 1.01;
   }

   // other weapons code
	if (RPK47Falk(Other)                    != none      ||
       HK23EFalk(Other)                    != none      ||
       XMV850Falk(Other)                   != none      ||
       M249Falk(Other)                     != none      ||
       SW76Falk(Other)                     != none      ||
       M60Falk(Other)                      != none      ||
       THR40DTFalk(Other)                  != none      ||
       GPMG(Other)                         != none)
	{
	   if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.25;
	   
	   if (KFPRI.ClientVeteranSkillLevel >= 19)
         return 1.20;
      
      if (KFPRI.ClientVeteranSkillLevel >= 18)
         return 1.19;
      
      if (KFPRI.ClientVeteranSkillLevel >= 17)
         return 1.18;
         
      if (KFPRI.ClientVeteranSkillLevel >= 16)
         return 1.17;
      
      if (KFPRI.ClientVeteranSkillLevel >= 15)
         return 1.16;
      
      if (KFPRI.ClientVeteranSkillLevel >= 14)
         return 1.15;
      
      if (KFPRI.ClientVeteranSkillLevel >= 13)
         return 1.14;
      
      if (KFPRI.ClientVeteranSkillLevel >= 12)
         return 1.13;
      
      if (KFPRI.ClientVeteranSkillLevel >= 11)
         return 1.12;
         
      if (KFPRI.ClientVeteranSkillLevel >= 10)
         return 1.11;
         
      if (KFPRI.ClientVeteranSkillLevel >= 9)
         return 1.10;
         
      if (KFPRI.ClientVeteranSkillLevel >= 8)
         return 1.09;
         
      if (KFPRI.ClientVeteranSkillLevel >= 7)
         return 1.08;
         
      if (KFPRI.ClientVeteranSkillLevel >= 6)
         return 1.07;
         
      if (KFPRI.ClientVeteranSkillLevel >= 5)
         return 1.06;
         
      if (KFPRI.ClientVeteranSkillLevel >= 4)
         return 1.05;
         
      if (KFPRI.ClientVeteranSkillLevel >= 3)
         return 1.04;
         
      if (KFPRI.ClientVeteranSkillLevel >= 2)
         return 1.03;
         
      if (KFPRI.ClientVeteranSkillLevel >= 1)
         return 1.02;
	   
	   return 1.01;
	}
	
	return 1.0;
}

// Reduce recoil of artillerist weapons
static function float ModifyRecoilSpread(KFPlayerReplicationInfo KFPRI, WeaponFire Other, out float Recoil)
{
	Recoil = 1.0;
	
	if (RPK47Falk(Other.Weapon)     != none             ||
       XMV850Falk(Other.Weapon)     != none             ||
       HK23EFalk(Other.Weapon)      != none             ||
       M249Falk(Other.Weapon)       != none             ||
       SW76Falk(Other.Weapon)       != none             ||
       MercyFalk(Other.Weapon)      != none             ||
       THR40DTFalk(Other.Weapon)    != none             ||
       M60Falk(Other.Weapon)        != none             ||
       StingerFalk(Other.Weapon)    != none             ||
       GPMG(Other.Weapon)           != none)
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         Recoil -= 0.25;
      
      else
         Recoil -= (KFPRI.ClientVeteranSkillLevel + 1) * 0.01;
	}
	
	Return Recoil;
}


// Increased carry capacity
static function int AddCarryMaxWeight(KFPlayerReplicationInfo KFPRI)
{
   if(KFPRI.ClientVeteranSkillLevel >= 20)
      return 11;

   else if(KFPRI.ClientVeteranSkillLevel >= 17)
      return 9;
      
   else if(KFPRI.ClientVeteranSkillLevel >= 14)
      return 7;
   
   else if(KFPRI.ClientVeteranSkillLevel >= 11)
      return 5;
   
   else if(KFPRI.ClientVeteranSkillLevel >= 9)
      return 4;
      
   else if(KFPRI.ClientVeteranSkillLevel >= 7)
      return 3;
   
   else if(KFPRI.ClientVeteranSkillLevel >= 5)
      return 2;
   
   else if(KFPRI.ClientVeteranSkillLevel >= 3)
     return 1;
   
   return 0;
}

// Increased ammopickup modifier
static function float GetAmmoPickupMod(KFPlayerReplicationInfo KFPRI, KFAmmunition Other)
{
   // stinger code, since all the ammo are in the magazine
   if (StingerAmmoFalk(Other) != none || MercyAmmoFalk(Other)       != none)
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.25;
      
      return 0.01 * KFPRI.ClientVeteranSkillLevel + 1.01;
   }

   // other weapons code
	if ((RPK47AmmoFalk(Other)       != none  ||
        XMV850AmmoFalk(Other)      != none  ||
        HK23EAmmoFalk(Other)       != none  ||
        M249AmmoFalk(Other)        != none  ||
        SW76AmmoFalk(Other)        != none  ||
        M60AmmoFalk(Other)         != none  ||
        THR40DTAmmoFalk(Other)     != none  ||
        GPMGAmmo(Other)            != none) &&
        KFPRI.ClientVeteranSkillLevel > 0)
	{
	   if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.25;
	   
      if (KFPRI.ClientVeteranSkillLevel >= 19)
         return 1.20;
         
      if (KFPRI.ClientVeteranSkillLevel >= 18)
         return 1.19;
         
      if (KFPRI.ClientVeteranSkillLevel >= 17)
         return 1.18;
         
      if (KFPRI.ClientVeteranSkillLevel >= 16)
         return 1.17;
         
      if (KFPRI.ClientVeteranSkillLevel >= 15)
         return 1.16;
         
      if (KFPRI.ClientVeteranSkillLevel >= 14)
         return 1.15;

      if (KFPRI.ClientVeteranSkillLevel >= 13)
         return 1.14;
      
      if (KFPRI.ClientVeteranSkillLevel >= 12)
         return 1.13;
      
      if (KFPRI.ClientVeteranSkillLevel >= 11)
         return 1.12;
         
      if (KFPRI.ClientVeteranSkillLevel >= 10)
         return 1.11;
      
      if (KFPRI.ClientVeteranSkillLevel >= 9)
         return 1.10;
      
      if (KFPRI.ClientVeteranSkillLevel >= 8)
         return 1.09;
         
      if (KFPRI.ClientVeteranSkillLevel >= 7)
         return 1.08;
         
      if (KFPRI.ClientVeteranSkillLevel >= 6)
         return 1.07;
         
      if (KFPRI.ClientVeteranSkillLevel >= 5)
         return 1.06;
         
      if (KFPRI.ClientVeteranSkillLevel >= 4)
         return 1.05;
         
      if (KFPRI.ClientVeteranSkillLevel >= 3)
         return 1.84;
         
      if (KFPRI.ClientVeteranSkillLevel >= 2)
         return 1.03;
      
      if (KFPRI.ClientVeteranSkillLevel >= 1)
         return 1.02;
	   
	   return 1.01;
	}
	
	return 1.0;
}

// skill based artillerist bonus on zed time
static function bool ArtilleristZedTimeBonus(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel >= 6)
      return static.InZedTime(KFPRI);

   return false;
}

// Give extra items as default
static function AddDefaultInventory(KFPlayerReplicationInfo KFPRI, Pawn P)
{ 
   // If Level 20 or higher give them RPK47
    if (KFPRI.ClientVeteranSkillLevel >= 20)
      FalkAddPerkedWeapon(class'RPK47Falk', KFPRI, P);
      
   // If Level 15 or higher give them THR40DT 
    else if (KFPRI.ClientVeteranSkillLevel >= 15)
      FalkAddPerkedWeapon(class'THR40DTFalk', KFPRI, P);
      
	// If Level 12 or higher give them HK23E
    else if (KFPRI.ClientVeteranSkillLevel >= 12)
      FalkAddPerkedWeapon(class'HK23EFalk', KFPRI, P);
   
	// If Level 9 or higher give them Mercy
    else if (KFPRI.ClientVeteranSkillLevel >= 9)
      FalkAddPerkedWeapon(class'MercyFalk', KFPRI, P);

   super.AddDefaultInventory(KFPRI, P);
}


// Add freeze bomb
static function class<Grenade> GetNadeType(KFPlayerReplicationInfo KFPRI)
{
   return class'FreezeBombFalk';	
}


// return max health for this perk
static function int fGetMaxHealth(KFPlayerReplicationInfo KFPRI)
{
   return 70;
}


defaultproperties
{
    PerkIndex=7

    OnHUDIcon=Texture'LairTextures_T.Artillerist.Perk_Artillerist'
    OnHUDGoldIcon=Texture'LairTextures_T.Artillerist.Perk_Artillerist_Gold'
    VeterancyName="Artillerist"

    NumRequirements=2

    Requirements[0]="Kill %x specimens while at 70 health points or above with artillerist weapons"
    Requirements[1]="Deal %x damage with artillerist weapons"

    SRLevelEffects(0)="4% extra damage with artillerist weapons|1% less recoil with artillerist weapons|1% larger artillerist weapons magazines|1% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|3% discount on artillerist weapons|Equipped with cryogenic grenades"
    SRLevelEffects(1)="8% extra damage with artillerist weapons|2% less recoil with artillerist weapons|2% larger artillerist weapons magazines|2% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades||6% discount on artillerist weapons|Equipped with cryogenic grenades"
    SRLevelEffects(2)="12% extra damage with artillerist weapons|3% less recoil with artillerist weapons|3% larger artillerist weapons magazines|3% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|9% discount on artillerist weapons|Equipped with cryogenic grenades"
    SRLevelEffects(3)="16% extra damage with artillerist weapons|4% less recoil with artillerist weapons|4% larger artillerist weapons magazines|4% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 16kg|12% discount on artillerist weapons|Equipped with cryogenic grenades"
    SRLevelEffects(4)="20% extra damage with artillerist weapons|5% less recoil with artillerist weapons|5% larger artillerist weapons magazines|5% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 16kg|15% discount on artillerist weapons|Equipped with cryogenic grenades"
    SRLevelEffects(5)="24% extra damage with artillerist weapons|6% less recoil with artillerist weapons|6% larger artillerist weapons magazines|6% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 17kg|18% discount on artillerist weapons|10% discount on tools|Equipped with cryogenic grenades"
    SRLevelEffects(6)="28% extra damage with artillerist weapons|7% less recoil with artillerist weapons|7% larger artillerist weapons magazines|7% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 17kg|21% discount on artillerist weapons|10% discount on tools|Equipped with cryogenic grenades|Zed-time skill: projectiles of perked weapons freeze targets"
    SRLevelEffects(7)="32% extra damage with artillerist weapons|8% less recoil with artillerist weapons|8% larger artillerist weapons magazines|8% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 18kg|24% discount on artillerist weapons|10% discount on tools|Equipped with cryogenic grenades|Zed-time skill: projectiles of perked weapons freeze targets"
    SRLevelEffects(8)="36% extra damage with artillerist weapons|9% less recoil with artillerist weapons|9% larger artillerist weapons magazines|9% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 18kg|27% discount on artillerist weapons|10% discount on tools|Equipped with cryogenic grenades|Zed-time skill: projectiles of perked weapons freeze targets"
    SRLevelEffects(9)="40% extra damage with artillerist weapons|10% less recoil with artillerist weapons|10% larger artillerist weapons magazines|10% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 19kg|30% discount on artillerist weapons|10% discount on tools|Equipped with cryogenic grenades|Spawn with a Mercy|Zed-time skill: projectiles of perked weapons freeze targets"
    SRLevelEffects(10)="44% extra damage with artillerist weapons|11% less recoil with artillerist weapons|11% larger artillerist weapons magazines|11% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 19kg|33% discount on artillerist weapons|20% discount on tools|Equipped with cryogenic grenades|Spawn with a Mercy|Zed-time skill: projectiles of perked weapons freeze targets"
    SRLevelEffects(11)="48% extra damage with artillerist weapons|12% less recoil with artillerist weapons|12% larger artillerist weapons magazines|12% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 20kg|36% discount on artillerist weapons|20% discount on tools|Equipped with cryogenic grenades|Spawn with a Mercy|Zed-time skill: projectiles of perked weapons freeze targets"
    SRLevelEffects(12)="52% extra damage with artillerist weapons|13% less recoil with artillerist weapons|13% larger artillerist weapons magazines|13% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 20kg|40% discount on artillerist weapons|20% discount on tools|Equipped with cryogenic grenades|Spawn with an HK23|Zed-time skill: projectiles of perked weapons freeze targets"
    SRLevelEffects(13)="56% extra damage with artillerist weapons|14% less recoil with artillerist weapons|14% larger artillerist weapons magazines|14% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 20kg|44% discount on artillerist weapons|20% discount on tools|Equipped with cryogenic grenades|Spawn with an HK23|Zed-time skill: projectiles of perked weapons freeze targets"
    SRLevelEffects(14)="60% extra damage with artillerist weapons|15% less recoil with artillerist weapons|15% larger artillerist weapons magazines|15% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 22kg|48% discount on artillerist weapons|20% discount on tools|Equipped with cryogenic grenades|Spawn with an HK23|Zed-time skill: projectiles of perked weapons freeze targets"
    SRLevelEffects(15)="64% extra damage with artillerist weapons|16% less recoil with artillerist weapons|16% larger artillerist weapons magazines|16% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 22kg|52% discount on artillerist weapons|30% discount on tools|Equipped with cryogenic grenades|Spawn with an THR40|Zed-time skill: projectiles of perked weapons freeze targets"
    SRLevelEffects(16)="68% extra damage with artillerist weapons|17% less recoil with artillerist weapons|17% larger artillerist weapons magazines|17% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 22kg|56% discount on artillerist weapons|30% discount on tools|Equipped with cryogenic grenades|Spawn with an THR40|Zed-time skill: projectiles of perked weapons freeze targets"
    SRLevelEffects(17)="72% extra damage with artillerist weapons|18% less recoil with artillerist weapons|18% larger artillerist weapons magazines|18% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 24kg|60% discount on artillerist weapons|30% discount on tools|Equipped with cryogenic grenades|Spawn with an THR40|Zed-time skill: projectiles of perked weapons freeze targets"
    SRLevelEffects(18)="76% extra damage with artillerist weapons|19% less recoil with artillerist weapons|19% larger artillerist weapons magazines|19% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 24kg|64% discount on artillerist weapons|30% discount on tools|Equipped with cryogenic grenades|Spawn with an THR40|Zed-time skill: projectiles of perked weapons freeze targets"
    SRLevelEffects(19)="80% extra damage with artillerist weapons|20% less recoil with artillerist weapons|20% larger artillerist weapons magazines|20% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 24kg|68% discount on artillerist weapons|30% discount on tools|Equipped with cryogenic grenades|Spawn with an THR40|Zed-time skill: projectiles of perked weapons freeze targets"
    SRLevelEffects(20)="90% extra damage with artillerist weapons|25% less recoil with artillerist weapons|25% larger artillerist weapons magazines|25% extra artillerist weapons ammo|30% less maximum health|Can only carry 3 grenades|Can carry up to 26kg|75% discount on artillerist weapons|40% discount on tools|Equipped with cryogenic grenades|Spawn with an RPK47|Zed-time skill: projectiles of perked weapons freeze targets"
}
