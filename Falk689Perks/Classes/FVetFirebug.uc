class FVetFirebug extends VeterancyTypesFalk;

static function AddCustomStats(ClientPerkRepLink Other)
{
   // Needed for custom husks kills progress
   Other.AddCustomValue(Class'Falk689ZedsFix.FHusksKills');   
}

static function class<DamageType> GetMAC10DamageType(KFPlayerReplicationInfo KFPRI)
{
   return class'DamTypeMAC10MPIncFalk';
}


static function int GetPerkProgressInt(ClientPerkRepLink StatOther, out int FinalInt, byte CurLevel, byte ReqNum)
{
   switch(CurLevel)
   {
   case 0:
      if(ReqNum == 0)
         FinalInt = 1;         
      else
         FinalInt = 1;
      break;
         
   case 1:
      if(ReqNum == 0)
         FinalInt = 1;
      else
         FinalInt = 100;
      break;
         
   case 2:
      if(ReqNum == 0)
         FinalInt = 2;
      else
         FinalInt = 1000;
      break;

   case 3:
      if(ReqNum == 0)
         FinalInt = 5;
      else
         FinalInt = 3500;
      break;  
      
   case 4:
      if(ReqNum == 0)
         FinalInt = 10;
      else
         FinalInt = 8000;
      break;
         
   case 5:
      if(ReqNum == 0)
         FinalInt = 17;
      else
         FinalInt = 15000;
      break;
      
   case 6:
      if(ReqNum == 0)
         FinalInt = 27;
      else
         FinalInt = 30000;
      break;
      
   case 7:
      if(ReqNum == 0)
         FinalInt = 40;
      else
         FinalInt = 70000;
      break;
      
   case 8:
      if(ReqNum == 0)
         FinalInt = 58;
      else
         FinalInt = 150000;
      break;
      
   case 9:
      if(ReqNum == 0)
         FinalInt = 80;
      else
         FinalInt = 300000;
      break;
      
   case 10:
      if(ReqNum == 0)
         FinalInt = 105;
      else
         FinalInt = 500000;
      break;
      
   case 11:
      if(ReqNum == 0)
         FinalInt = 135;
      else
         FinalInt = 750000;
      break;
         
   case 12:
      if(ReqNum == 0)
         FinalInt = 175;
      else
         FinalInt = 1100000;
      break; 
         
   case 13:
      if(ReqNum == 0)
         FinalInt = 225;
      else
         FinalInt = 1500000;
      break; 
         
   case 14:
      if(ReqNum == 0)
         FinalInt = 290;
      else
         FinalInt = 2200000;
      break; 
         
   case 15:
      if(ReqNum == 0)
         FinalInt = 380;
      else
         FinalInt = 3000000;
      break; 
         
   case 16:
      if(ReqNum == 0)
         FinalInt = 480;
      else
         FinalInt = 4000000;
      break; 
         
   case 17:
      if(ReqNum == 0)
         FinalInt = 590;
      else
         FinalInt = 5200000;
      break; 
         
   case 18:
      if(ReqNum == 0)
         FinalInt = 710;
      else
         FinalInt = 6500000;
      break; 
         
   case 19:
      if(ReqNum == 0)
         FinalInt = 840;
      else
         FinalInt = 8000000;
      break; 
         
   case 20:
      if(ReqNum == 0)
         FinalInt = 1000;
      else
         FinalInt = 10000000;
      break;
		
   default:
      if(ReqNum == 0)
         FinalInt = 1100 + GetDoubleScaling(CurLevel, 350);
      else
         FinalInt = 10000000 + GetDoubleScaling(CurLevel, 500000);
      break;
   }
   
   if (ReqNum == 0)
      return Min(StatOther.GetCustomValueInt(Class'Falk689ZedsFix.FHusksKills'), FinalInt);
         
   return Min(StatOther.RFlameThrowerDamageStat, FinalInt);
}

// Larger magazines
static function float GetMagCapacityMod(KFPlayerReplicationInfo KFPRI, KFWeapon Other)
{
	if (FlamethrowerFalk(Other)                != none ||
	    MAC10MPFalk(Other)                     != none ||
	    AAR525SFalk(Other)                     != none ||
       M4A1IronBeastSAAssaultRifleFalk(Other) != none)
	{
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.70;
      
      return 0.03 * KFPRI.ClientVeteranSkillLevel + 1.03;
   }
	
	return 1.0;
}

// Add extra ammo pickup
static function float GetAmmoPickupMod(KFPlayerReplicationInfo KFPRI, KFAmmunition Other)
{
   if (SpitfireAmmoFalk(Other)                == none   &&
      (FlameAmmoFalk(Other)                   != none   ||
	    MAC10AmmoFalk(Other)                   != none   ||
       M4A1IronBeastSAAmmoFalk(Other)         != none   ||
	    AAR525SAmmoFalk(Other)                 != none))
	{
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.70;
      
      return 0.03 * KFPRI.ClientVeteranSkillLevel + 1.03;
   }

	return 1.0;
}


// Add extra ammo for firebug stuff
static function float AddExtraAmmoFor(KFPlayerReplicationInfo KFPRI, Class<Ammunition> AmmoType)
{
	if (AmmoType == class'MAC10AmmoFalk'             ||
       AmmoType == class'FlareRevolverAmmo'         ||
       AmmoType == class'TrenchgunAmmoFalk'         ||
	 	 AmmoType == class'FlareRevolverAmmoFalk'     ||
		 AmmoType == class'DualFlareRevolverAmmoFalk' ||
       AmmoType == class'M4A1IronBeastSAAmmoFalk')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.45;
      
      return 0.02 * KFPRI.ClientVeteranSkillLevel + 1.02;
   }
	
	return 1.0;
}

// Add damage to fire weapons
static function int AddDamage(KFPlayerReplicationInfo KFPRI, KFMonster Injured, KFPawn DamageTaker, int InDamage, class<DamageType> DmgType)
{
   local FalkZombieHuskBase FHS;
   local float fDamage;

   fDamage = float(InDamage);

   FHS     = FalkZombieHuskBase(Injured);

   // full damage to the husk during zed time
   if (KFPRI.ClientVeteranSkillLevel >= 6 && FHS != none && static.InZedTime(KFPRI))
   {
      if (class<DamTypeBurnWeakFalk>(DmgType)            != none ||
          class<DamTypeFlamethrower>(DmgType)            != none ||
          class<DamTypeFlamethrowerFalk>(DmgType)        != none ||
          class<DamTypeHuskGun>(DmgType)                 != none ||
          class<DamTypeHuskGunProjectileImpact>(DmgType) != none ||
          class<DamTypeFlameNadeFalk>(DmgType)           != none ||
          class<DamTypeInstantFlameNadeFalk>(DmgType)    != none ||
          class<DamTypeAAR525SFalk>(DmgType)             != none)
      {
         fDamage /= FHS.fWFireMultiplier;
      }

      else if (class<DamTypeBurnStrongFalk>(DmgType)              != none ||
               class<DamTypeTrenchgunFalk>(DmgType)               != none ||
               class<DamTypeFlareRevolver>(DmgType)               != none ||
               class<DamTypeFlareProjectileImpactFalk>(DmgType)   != none ||
               class<DamTypeM4A1IronBeastSAFalk>(DmgType)         != none ||
               class<DamTypeSpitfireFalk>(DmgType)                != none ||
               class<DamTypeMAC10MPIncFalk>(DmgType)              != none)
      {
         fDamage /= FHS.fSFireMultiplier;
      }
   }

   // normal damage scaling
   if (class<DamTypeBurned>(DmgType)                    != none ||
       class<DamTypeMAC10MPIncFalk>(DmgType)            != none ||
       class<DamTypeFlamethrower>(DmgType)              != none ||
       class<DamTypeBurnStrongFalk>(DmgType)            != none ||
       class<DamTypeBurnWeakFalk>(DmgType)              != none ||
       class<DamTypeHuskGunProjectileImpact>(DmgType)   != none ||
       class<DamTypeFlareProjectileImpactFalk>(DmgType) != none ||
       class<DamTypeTrenchgunFalk>(DmgType)             != none || 
       class<DamTypeFlareRevolver>(DmgType)             != none || 
       class<DamTypeAAR525S>(DmgType)                   != none ||
       class<DamTypeM4A1IronBeastSAFalk>(DmgType)       != none ||
       class<DamTypeM4A1IronBeastSABurnFalk>(DmgType)   != none ||
       class<DamTypeSpitfireFalk>(DmgType)              != none)
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return int(fDamage * 1.9);
	   
      return int(fDamage * (0.04 * KFPRI.ClientVeteranSkillLevel + 1.04));
   }

   return Super.AddDamage(KFPRI, Injured, DamageTaker, InDamage, DmgType);
}


// Change effective range on Spitfire/Flamer/AFS525
static function int ExtraRange(KFPlayerReplicationInfo KFPRI)
{
	if (KFPRI.ClientVeteranSkillLevel >= 20)
		return 2.20;
		
   return 1.05 + KFPRI.ClientVeteranSkillLevel * 0.05;
}


// Reduce fire damage taken
static function int ReduceDamage(KFPlayerReplicationInfo KFPRI, KFPawn Injured, Pawn Instigator, int InDamage, class<DamageType> DmgType)
{
   // Immunity to instant flame nades
   if (class<DamTypeInstantFlameNadeFalk>(DmgType)    != none)
      return 0;

   if (class<DamTypeBurned>(DmgType)                     != none ||
       class<DamTypeFlamethrower>(DmgType)               != none ||
       class<DamTypeHuskGunProjectileImpact>(DmgType)    != none ||
       class<DamTypeFlareProjectileImpactFalk>(DmgType)  != none ||
       class<DamTypeSpitfireFalk>(DmgType)               != none)
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 0;

      if (KFPRI.ClientVeteranSkillLevel >= 16)
         return InDamage * 0.1;

      if (KFPRI.ClientVeteranSkillLevel >= 12)
         return InDamage * 0.2;

      if (KFPRI.ClientVeteranSkillLevel >= 8)
         return InDamage * 0.3;

      if (KFPRI.ClientVeteranSkillLevel >= 4)
         return InDamage * 0.4;

      return InDamage * 0.5;
		
   }
	
   return Super.ReduceDamage(KFPRI, Injured, Instigator, InDamage, DmgType);
}


// Add fire nades
static function class<Grenade> GetNadeType(KFPlayerReplicationInfo KFPRI)
{
    return class'FlameNadeFalk';
}


// Faster reload
static function float GetReloadSpeedModifier(KFPlayerReplicationInfo KFPRI, KFWeapon Other)
{
	if (FlameThrowerFalk(Other)                != none ||
	    TrenchGunFalk(Other)                   != none ||
	    LocalTrenchGunFalk(Other)              != none ||
	    MAC10MPFalk(Other)                     != none ||
		 FlareRevolverFalk(Other)               != none ||
       DualFlareRevolverFalk(Other)           != none ||
       M4A1IronBeastSAAssaultRifleFalk(Other) != none ||
       AAR525SFalk(Other)                     != none)
	{
	   if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.65;
	   
      return 0.03 * KFPRI.ClientVeteranSkillLevel + 1.03;  
	}
	
	return 1.0;
}

// Change the cost of firebug stuff
static function float GetCostScaling(KFPlayerReplicationInfo KFPRI, class<Pickup> Item)
{
    local float result;

    result = Super.GetCostScaling(KFPRI, Item);

    if (result != 1.0 && result != Default.huskDiv)
        return result;

    switch (KFPRI.ClientVeteranSkillLevel)
    {
        case 0:
            result = 0.97;
            break;

        case 1:
            result = 0.94;
            break;

        case 2:
            result = 0.91;
            break;

        case 3:
            result = 0.88;
            break;

        case 4:
            result = 0.85;
            break;

        case 5:
            result = 0.82;
            break;

        case 6:
            result = 0.79;
            break;

        case 7:
            result = 0.76;
            break;

        case 8:
            result = 0.73;
            break;

        case 9:
            result = 0.70;
            break;

        case 10:
            result = 0.67;
            break;

        case 11:
            result = 0.64;
            break;

        case 12:
            result = 0.60;
            break;

        case 13:
            result = 0.56;
            break;

        case 14:
            result = 0.52;
            break;

        case 15:
            result = 0.48;
            break;

        case 16:
            result = 0.44;
            break;

        case 17:
            result = 0.40;
            break;

        case 18:
            result = 0.36;
            break;

        case 19:
            result = 0.32;
            break;

        default:
            result = 0.25;
            break;
    }

    if (class<HuskGunPickup>(Item) != none)
        return result * Default.huskDiv;

    else if (Item == class'FlameThrowerPickupFalk'      ||
             Item == class'MAC10PickupFalk'             ||
             Item == class'TrenchgunPickupFalk'         ||
             Item == class'LocalTrenchgunPickupFalk'    ||
             Item == class'FlareRevolverPickupFalk'     ||
             Item == class'DualFlareRevolverPickupFalk' ||
             Item == class'AAR525SPickupFalk'           ||
             Item == class'M4A1IronBeastSAPickupFalk'   ||
             Item == class'SpitfirePickupFalk')
    {
        return result;
    }

        return 1.0;
}


// Give extra items as default
static function AddDefaultInventory(KFPlayerReplicationInfo KFPRI, Pawn P)
{ 
   // If Level 20 or higher give them Husk Cannon
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      FalkAddPerkedWeapon(class'HuskGun', KFPRI, P);
      
   // If Level 15 or higher give them Spitfire
   else if (KFPRI.ClientVeteranSkillLevel >= 15)
      FalkAddPerkedWeapon(class'SpitfireFalk', KFPRI, P);
      
	// If Level 12 or higher give them Flare Revolver
   else if (KFPRI.ClientVeteranSkillLevel >= 12)
      FalkAddPerkedWeapon(class'FlareRevolverFalk', KFPRI, P);
   
	// If Level 9 or higher give them MAC-10
   else if (KFPRI.ClientVeteranSkillLevel >= 9)
      FalkAddPerkedWeapon(class'MAC10MPFalk', KFPRI, P);

   super.AddDefaultInventory(KFPRI, P);
}


// Flame explosion on zed time start
static function StartZedTime(KFPlayerReplicationInfo KFPRI)
{
   local Emitter fE;
   local Pawn fP;

   fP  = Controller(KFPRI.Owner).Pawn;

   if (KFPRI.ClientVeteranSkillLevel >= 6 && static.CanUseZedTimeSkill(KFPRI) &&
       fP != none && static.ZedTimeSkillUsed(KFPRI))
   {
      fP.Weapon.Spawn(class'InstantFlameNadeFalk',,, fP.Location, rotator(vect(0,0,1)));

      // fire explosion
      fE = fP.Spawn(Class'KFIncendiaryExplosion');
      fE.SetBase(fP);

      fE.Emitters[0].SkeletalMeshActor     = fP;
      fE.Emitters[0].UseSkeletalLocationAs = PTSU_SpawnOffset;
      fE.Emitters[1].SkeletalMeshActor     = fP;
      fE.Emitters[1].UseSkeletalLocationAs = PTSU_SpawnOffset;
   }
}

defaultproperties
{

	PerkIndex=5

	OnHUDIcon=Texture'KillingFloorHUD.Perks.Perk_Firebug'
	OnHUDGoldIcon=Texture'KillingFloor2HUD.Perk_Icons.Perk_Firebug_Gold'
	VeterancyName="Firebug"
	
   NumRequirements=2
   
   Requirements[0]="Kill %x husks with incendiary weaponry"
	Requirements[1]="Deal %x damage with incendiary weaponry"

   ZTECD=3.0

   SRLevelEffects(0)="4% extra damage with firebug weapons|3% faster reload with firebug weapons|2% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|3% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|50% resistance to fire|5% extra Flamer/Spitfire/AFS525 range|3% discount on firebug weapons|Equipped with incendiary grenades"
   SRLevelEffects(1)="8% extra damage with firebug weapons|6% faster reload with firebug weapons|4% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|6% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|50% resistance to fire|10% extra Flamer/Spitfire/AFS525 range|6% discount on firebug weapons|Equipped with incendiary grenades"
   SRLevelEffects(2)="12% extra damage with firebug weapons|9% faster reload with firebug weapons|6% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|9% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|50% resistance to fire|15% extra Flamer/Spitfire/AFS525 range|9% discount on firebug weapons|Equipped with incendiary grenades"
   SRLevelEffects(3)="16% extra damage with firebug weapons|12% faster reload with firebug weapons|8% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|12% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|50% resistance to fire|20% extra Flamer/Spitfire/AFS525 range|12% discount on firebug weapons|Equipped with incendiary grenades"
   SRLevelEffects(4)="20% extra damage with firebug weapons|15% faster reload with firebug weapons|10% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|15% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|60% resistance to fire|25% extra Flamer/Spitfire/AFS525 range|15% discount on firebug weapons|Equipped with incendiary grenades"
   SRLevelEffects(5)="24% extra damage with firebug weapons|18% faster reload with firebug weapons|12% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|18% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|60% resistance to fire|30% extra Flamer/Spitfire/AFS525 range|18% discount on firebug weapons|10% discount on tools|Equipped with incendiary grenades"
   SRLevelEffects(6)="28% extra damage with firebug weapons|21% faster reload with firebug weapons|14% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|21% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|60% resistance to fire|35% extra Flamer/Spitfire/AFS525 range|21% discount on firebug weapons|10% discount on tools|Equipped with incendiary grenades|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
   SRLevelEffects(7)="32% extra damage with firebug weapons|24% faster reload with firebug weapons|16% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|24% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|60% resistance to fire|40% extra Flamer/Spitfire/AFS525 range|24% discount on firebug weapons|10% discount on tools|Equipped with incendiary grenades|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
   SRLevelEffects(8)="36% extra damage with firebug weapons|27% faster reload with firebug weapons|18% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|27% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|70% resistance to fire|45% extra Flamer/Spitfire/AFS525 range|27% discount on firebug weapons|10% discount on tools|Equipped with incendiary grenades|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
   SRLevelEffects(9)="40% extra damage with firebug weapons|30% faster reload with firebug weapons|20% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|30% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|70% resistance to fire|50% extra Flamer/Spitfire/AFS525 range|30% discount on firebug weapons|10% discount on tools|Equipped with incendiary grenades|Spawn with a MAC-10|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
   SRLevelEffects(10)="44% extra damage with firebug weapons|33% faster reload with firebug weapons|22% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|33% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|70% resistance to fire|55% extra Flamer/Spitfire/AFS525 range|33% discount on firebug weapons|20% discount on tools|Equipped with incendiary grenades|Spawn with a MAC-10|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
   SRLevelEffects(11)="48% extra damage with firebug weapons|36% faster reload with firebug weapons|24% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|36% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|70% resistance to fire|60% extra Flamer/Spitfire/AFS525 range|36% discount on firebug weapons|20% discount on tools|Equipped with incendiary grenades|Spawn with a MAC-10|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
   SRLevelEffects(12)="52% extra damage with firebug weapons|39% faster reload with firebug weapons|26% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|39% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|80% resistance to fire|65% extra Flamer/Spitfire/AFS525 range|40% discount on firebug weapons|20% discount on tools|Equipped with incendiary grenades|Spawn with a Flare Revolver|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
   SRLevelEffects(13)="56% extra damage with firebug weapons|42% faster reload with firebug weapons|28% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|42% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|80% resistance to fire|70% extra Flamer/Spitfire/AFS525 range|44% discount on firebug weapons|20% discount on tools|Equipped with incendiary grenades|Spawn with a Flare Revolver|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
   SRLevelEffects(14)="60% extra damage with firebug weapons|45% faster reload with firebug weapons|30% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|45% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|80% resistance to fire|75% extra Flamer/Spitfire/AFS525 range|48% discount on firebug weapons|20% discount on tools|Equipped with incendiary grenades|Spawn with a Flare Revolver|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
   SRLevelEffects(15)="64% extra damage with firebug weapons|48% faster reload with firebug weapons|32% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|48% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|80% resistance to fire|80% extra Flamer/Spitfire/AFS525 range|52% discount on firebug weapons|30% discount on tools|Equipped with incendiary grenades|Spawn with a Spitfire|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
   SRLevelEffects(16)="68% extra damage with firebug weapons|51% faster reload with firebug weapons|34% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|51% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|90% resistance to fire|85% extra Flamer/Spitfire/AFS525 range|56% discount on firebug weapons|30% discount on tools|Equipped with incendiary grenades|Spawn with a Spitfire|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
   SRLevelEffects(17)="72% extra damage with firebug weapons|54% faster reload with firebug weapons|36% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|54% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|90% resistance to fire|90% extra Flamer/Spitfire/AFS525 range|60% discount on firebug weapons|30% discount on tools|Equipped with incendiary grenades|Spawn with a Spitfire|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
   SRLevelEffects(18)="76% extra damage with firebug weapons|57% faster reload with firebug weapons|38% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|57% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|90% resistance to fire|95% extra Flamer/Spitfire/AFS525 range|64% discount on firebug weapons|30% discount on tools|Equipped with incendiary grenades|Spawn with a Spitfire|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
   SRLevelEffects(19)="80% extra damage with firebug weapons|60% faster reload with firebug weapons|40% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|60% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|90% resistance to fire|100% extra Flamer/Spitfire/AFS525 range|68% discount on firebug weapons|30% discount on tools|Equipped with incendiary grenades|Spawn with a Spitfire|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
   SRLevelEffects(20)="90% extra damage with firebug weapons|65% faster reload with firebug weapons|45% extra ammo for MAC-10/Flare Revolvers/Disclosure/M4A1 IB|70% larger MAC-10/Flamer/M4A1 IB/AFS525 magazines|Immunity to fire|120% extra Flamer/Spitfire/AFS525 range|75% discount on firebug weapons|40% discount on tools|Equipped with incendiary grenades|Spawn with a Husk Cannon|Zed-time skill: you release a flaming blast that stuns on hit and doesn't repeat with extensions, your perked weapons also ignore husks resistance to fire"
}
