class FVetCommando extends VeterancyTypesFalk;

static function AddCustomStats(ClientPerkRepLink Other)
{   
   // Needed for custom stalkers kills progress
   Other.AddCustomValue(Class'Falk689ZedsFix.FStalkersKills');   
}

static function int GetPerkProgressInt(ClientPerkRepLink StatOther, out int FinalInt, byte CurLevel, byte ReqNum)
{
   switch(CurLevel)
   {
   case 0:
      if(ReqNum == 0)
         FinalInt = 1;         
      else
         FinalInt = 1;
      break;
         
   case 1:
      if(ReqNum == 0)
         FinalInt = 5;
      else
         FinalInt = 100;
      break;
         
   case 2:
      if(ReqNum == 0)
         FinalInt = 10;
      else
         FinalInt = 1000;
      break;

   case 3:
      if(ReqNum == 0)
         FinalInt = 20;
      else
         FinalInt = 3500;
      break;  
      
   case 4:
      if(ReqNum == 0)
         FinalInt = 35;
      else
         FinalInt = 8000;
      break;
         
   case 5:
      if(ReqNum == 0)
         FinalInt = 60;
      else
         FinalInt = 15000;
      break;
      
   case 6:
      if(ReqNum == 0)
         FinalInt = 100;
      else
         FinalInt = 30000;
      break;
      
   case 7:
      if(ReqNum == 0)
         FinalInt = 150;
      else
         FinalInt = 70000;
      break;
      
   case 8:
      if(ReqNum == 0)
         FinalInt = 215;
      else
         FinalInt = 150000;
      break;
      
   case 9:
      if(ReqNum == 0)
         FinalInt = 300;
      else
         FinalInt = 300000;
      break;
      
   case 10:
      if(ReqNum == 0)
         FinalInt = 400;
      else
         FinalInt = 500000;
      break;
      
   case 11:
      if(ReqNum == 0)
         FinalInt = 520;
      else
         FinalInt = 750000;
      break;
         
   case 12:
      if(ReqNum == 0)
         FinalInt = 650;
      else
         FinalInt = 1100000;
      break; 
         
   case 13:
      if(ReqNum == 0)
         FinalInt = 800;
      else
         FinalInt = 1500000;
      break; 
         
   case 14:
      if(ReqNum == 0)
         FinalInt = 980;
      else
         FinalInt = 2200000;
      break; 
         
   case 15:
      if(ReqNum == 0)
         FinalInt = 1200;
      else
         FinalInt = 3000000;
      break; 
         
   case 16:
      if(ReqNum == 0)
         FinalInt = 1500;
      else
         FinalInt = 4000000;
      break; 
         
   case 17:
      if(ReqNum == 0)
         FinalInt = 1900;
      else
         FinalInt = 5200000;
      break; 
         
   case 18:
      if(ReqNum == 0)
         FinalInt = 2400;
      else
         FinalInt = 6500000;
      break; 
         
   case 19:
      if(ReqNum == 0)
         FinalInt = 3000;
      else
         FinalInt = 8000000;
      break; 
         
   case 20:
      if(ReqNum == 0)
         FinalInt = 3800;
      else
         FinalInt = 10000000;
      break;
		
   default:
      if(ReqNum == 0)
         FinalInt = 6000 + GetDoubleScaling(CurLevel, 350);
      else
         FinalInt = 10000000 + GetDoubleScaling(CurLevel, 500000);
      break;
   }
   
   if(ReqNum == 0)
      return Min(StatOther.GetCustomValueInt(Class'Falk689ZedsFix.FStalkersKills'), FinalInt);
         
   return Min(StatOther.RBullpupDamageStat, FinalInt);
}

// remove commando ammo cost scaling
static function float GetAmmoCostScaling(KFPlayerReplicationInfo KFPRI, class<Pickup> Item)
{
   return 1.0;
}


// Change the cost of commando stuff
static function float GetCostScaling(KFPlayerReplicationInfo KFPRI, class<Pickup> Item)
{
   local float result;

   result = Super.GetCostScaling(KFPRI, Item);

   if (result != 1.0)
      return result;

   switch (KFPRI.ClientVeteranSkillLevel)
   {
      case 0:
         result = 0.97;
         break;
            
      case 1:
         result = 0.94;
         break;
            
      case 2:
         result = 0.91;
         break;
            
      case 3:
         result = 0.88;
         break;
            
      case 4:
         result = 0.85;
         break;
            
      case 5:
         result = 0.82;
         break;
            
      case 6:
         result = 0.79;
         break;
            
      case 7:
         result = 0.76;
         break;
            
      case 8:
         result = 0.73;
         break;
            
      case 9:
         result = 0.70;
         break;
            
      case 10:
         result = 0.67;
         break;
            
      case 11:
         result = 0.64;
         break;
            
      case 12:
         result = 0.60;
         break;

      case 13:
         result = 0.56;
         break;
            
      case 14:
         result = 0.52;
         break;

      case 15:
         result = 0.48;
         break;

      case 16:
         result = 0.44;
         break;

      case 17:
         result = 0.40;
         break;
            
      case 18:
         result = 0.36;
         break;
            
      case 19:
         result = 0.32;
         break;
            
      default:
         result = 0.25;
         break;
   }

   if (Item == class'BullpupPickupFalk'      ||
	    Item == class'AK47LLIPickupFalk'      ||
       Item == class'SCARMK17PickupFalk'     ||
       Item == class'M4PickupFalk'           ||
       Item == class'FNFAL_ACOGPickupFalk'   ||
       Item == class'MKb42PickupFalk'        ||
       Item == class'ThompsonPickupFalk'     ||
       Item == class'ThompsonDrumPickupFalk' ||
       Item == class'SPThompsonPickupFalk'   ||
       Item == class'AAR525PickupFalk'       ||
       Item == class'cz805bPickupFalk'       ||
       Item == class'AK12SAPickupFalk'       ||
	    Item == class'Slayer_AUGPickupFalk'   ||
	    Item == class'PDWPickupFalk'          ||
	    Item == class'Glock18SAPickupFalk'    ||
	    Item == class'AK12LLIPickupFalk'      ||
       Item == class'AR15SAPickupFalk'       ||
	    Item == class'HKG36CSAPickupFalk')
          return result;
	       
	return Super.GetCostScaling(KFPRI, Item);
}


// Add damage to commando weapons
static function int AddDamage(KFPlayerReplicationInfo KFPRI, KFMonster Injured, KFPawn DamageTaker, int InDamage, class<DamageType> DmgType)
{
   if (DmgType == class'DamTypeBullpupFalk'               ||
       DmgType == class'DamTypeAK47LLIAssaultRifleFalk'   ||
       DmgType == class'DamTypeSCARMK17AssaultRifleFalk'  ||
       DmgType == class'DamTypeM4AssaultRifleFalk'        ||
       DmgType == class'DamTypeFNFALAssaultRifleFalk'     ||
       DmgType == class'DamTypeMKb42AssaultRifleFalk'     ||
       DmgType == class'DamTypeThompsonFalk'              ||
       DmgType == class'DamTypeThompsonDrumFalk'          ||
       DmgType == class'DamTypeSPThompsonFalk'            ||
       DmgType == class'DamTypeAAR525Falk'                ||
       DmgType == class'DamTypecz805bFalk'                ||
       DmgType == class'DamTypeAR15SAFalk'                ||
       DmgType == class'DamTypeAK12SAFalk'                ||
	    DmgType == class'DamTypeSlayer_AUGFalk'            ||
       DmgType == class'DamTypeAcidNadeFalk'              ||
	    DmgType == class'DamTypePDWAssaultRifleFalk'       ||
	    DmgType == class'DamTypeGlock18SAFalk'             ||
	    DmgType == class'DamTypeAK12LLIFalk'               ||
	    DmgType == class'DamTypeHKG36CSAAssaultRifleFalk')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return InDamage * 1.9;
	   
      return InDamage * (0.04 * KFPRI.ClientVeteranSkillLevel + 1.04);
   }
   
   return Super.AddDamage(KFPRI, Injured, DamageTaker, InDamage, DmgType);
}

// Add extra ammo for commando stuff
static function float AddExtraAmmoFor(KFPlayerReplicationInfo KFPRI, Class<Ammunition> AmmoType)
{
	if (AmmoType == class'BullpupAmmoFalk'      ||
	    AmmoType == class'AK47LLIAmmoFalk'      ||
       AmmoType == class'SCARMK17AmmoFalk'     || 
       AmmoType == class'M4AmmoFalk'           ||
       AmmoType == class'FNFALAmmoFalk'        ||
       AmmoType == class'MKb42AmmoFalk'        ||
       AmmoType == class'ThompsonAmmoFalk'     ||
       AmmoType == class'ThompsonDrumAmmoFalk' ||
       AmmoType == class'SPThompsonAmmoFalk'   ||
       AmmoType == class'AR15SAAmmoFalk'       ||
       AmmoType == class'AAR525AmmoFalk'       ||
       AmmoType == class'cz805bAmmoFalk'       ||
       AmmoType == class'AK12SAAmmoFalk'       ||
       AmmoType == class'Slayer_AUGAmmoFalk'   ||
	 	 AmmoType == class'PDWAmmoFalk'          ||
	 	 AmmoType == class'Glock18SAAmmoFalk'    ||
		 AmmoType == class'AK12LLIAmmoFalk'      ||
		 AmmoType == class'HKG36CSAAmmoFalk')
   {
      if(KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.25;
      
      return 0.01 * KFPRI.ClientVeteranSkillLevel + 1.01;
   }
	
	return 1.0;
}

// Larger clips
static function float GetMagCapacityMod(KFPlayerReplicationInfo KFPRI, KFWeapon Other)
{
	if (M4203AssaultRifle(Other)             == none &&
	   (BullpupFalk(Other)                   != none ||
	    AK47LLIAssaultRifleFalk(Other)       != none ||
	    SCARMK17AssaultRifleFalk(Other)      != none ||
		 M4AssaultRifleFalk(Other)            != none ||
       FNFAL_ACOGAssaultRifleFalk(Other)    != none ||
       MKb42AssaultRifleFalk(Other)         != none ||
       ThompsonSMGFalk(Other)               != none ||
       ThompsonDrumSMGFalk(Other)           != none ||
       SPThompsonSMGFalk(Other)             != none ||
       AAR525Falk(Other)                    != none ||
       AR15SAFalk(Other)                    != none ||
       cz805bFalk(Other)                    != none ||
       AK12SAAssaultRifleFalk(Other)        != none ||
		 Slayer_AUGFalk(Other)                != none ||
		 PDWAssaultRifleFalk(Other)           != none ||
		 Glock18SAFalk(Other)                 != none ||
		 AK12LLIAssaultRifleFalk(Other)       != none ||
		 HKG36CSAAssaultRifleFalk(Other)      != none))
	{
	   if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.45;
	   
	   if (KFPRI.ClientVeteranSkillLevel >= 18)
         return 1.40;
      
      if (KFPRI.ClientVeteranSkillLevel >= 16)
         return 1.36;
      
      if (KFPRI.ClientVeteranSkillLevel >= 14)
         return 1.32;
         
      if (KFPRI.ClientVeteranSkillLevel >= 12)
         return 1.28;
      
      if (KFPRI.ClientVeteranSkillLevel >= 10)
         return 1.24;
      
      if (KFPRI.ClientVeteranSkillLevel >= 8)
         return 1.20;
      
      if (KFPRI.ClientVeteranSkillLevel >= 6)
         return 1.16;
      
      if (KFPRI.ClientVeteranSkillLevel >= 4)
         return 1.12;
      
      if (KFPRI.ClientVeteranSkillLevel >= 2)
         return 1.08;
	   
	   return 1.04;
	}
	
	return 1.0;
}


// Faster reload
static function float GetReloadSpeedModifier(KFPlayerReplicationInfo KFPRI, KFWeapon Other)
{
	if (M4203AssaultRifle(Other)           == none &&
	   (BullpupFalk(Other)                 != none ||
	    AK47LLIAssaultRifleFalk(Other)     != none ||
		 SCARMK17AssaultRifleFalk(Other)    != none ||
	    M4AssaultRifleFalk(Other)          != none ||
       FNFAL_ACOGAssaultRifleFalk(Other)  != none ||
       MKb42AssaultRifleFalk(Other)       != none ||
       ThompsonSMGFalk(Other)             != none ||
       ThompsonDrumSMGFalk(Other)         != none ||
       SPThompsonSMGFalk(Other)           != none ||
       AR15SAFalk(Other)                  != none ||
       AAR525Falk(Other)                  != none ||
       cz805bFalk(Other)                  != none ||
       AK12SAAssaultRifleFalk(Other)      != none ||
	    Slayer_AUGFalk(Other)              != none ||
		 PDWAssaultRifleFalk(Other)         != none ||
		 Glock18SAFalk(Other)               != none ||
		 AK12LLIAssaultRifleFalk(Other)     != none ||
		 HKG36CSAAssaultRifleFalk(Other)    != none))
	{
	   if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.45;
	   
      return 0.02 * KFPRI.ClientVeteranSkillLevel + 1.02;
	}
	
	return 1.0;
}


// reduce recoil with commando weapons
static function float ModifyRecoilSpread(KFPlayerReplicationInfo KFPRI, WeaponFire Other, out float Recoil)
{
	Recoil = 1.0;
	
	if (M4203AssaultRifle(Other.Weapon)            == none &&
	   (BullpupFalk(Other.Weapon)                  != none ||
	    AK47AssaultRifle(Other.Weapon)             != none ||
	    SCARMK17AssaultRifleFalk(Other.Weapon)     != none ||
	    M4AssaultRifleFalk(Other.Weapon)           != none ||
       FNFAL_ACOGAssaultRifleFalk(Other.Weapon)   != none ||
       MKb42AssaultRifleFalk(Other.Weapon)        != none ||
       ThompsonSMGFalk(Other.Weapon)              != none ||
       ThompsonDrumSMGFalk(Other.Weapon)          != none ||
       SPThompsonSMGFalk(Other.Weapon)            != none ||
       AAR525Falk(Other.Weapon)                   != none ||
       AR15SAFalk(Other.Weapon)                   != none ||
       cz805bFalk(Other.Weapon)                   != none ||
       AK12SAAssaultRifleFalk(Other.Weapon)       != none ||
	    Slayer_AUGFalk(Other.Weapon)               != none ||
	    PDWAssaultRifleFalk(Other.Weapon)          != none ||
	    Glock18SAFalk(Other.Weapon)                != none ||
		 AK12LLIAssaultRifleFalk(Other.Weapon)      != none ||
		 HKG36CSAAssaultRifleFalk(Other.Weapon)     != none))
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         Recoil -= 0.65;
      
      else
         Recoil -= (KFPRI.ClientVeteranSkillLevel + 1) * 0.03;
	}
	
	Return Recoil;
}


// Less damage from stalkers and acid nade
static function int ReduceDamage(KFPlayerReplicationInfo KFPRI, KFPawn Injured, Pawn Instigator, int InDamage, class<DamageType> DmgType)
{
   if (DmgType == class'DamTypeSlashingAttackFalk')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return InDamage * 0.25;
      
      else if (KFPRI.ClientVeteranSkillLevel >= 15)
         return InDamage * 0.50;
   
      else if (KFPRI.ClientVeteranSkillLevel >= 12)
         return InDamage * 0.60;
    
      else if (KFPRI.ClientVeteranSkillLevel >= 9)
         return InDamage * 0.70;
      
      else if (KFPRI.ClientVeteranSkillLevel >= 6)
         return InDamage * 0.80;
      
      else if (KFPRI.ClientVeteranSkillLevel >= 3)
         return InDamage * 0.90;
	}

   if (DmgType == class'DamTypeAcidNadeFalk')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return InDamage / 1.9;
	   
      return InDamage / (0.04 * KFPRI.ClientVeteranSkillLevel + 1.04);
	}

   return Super.ReduceDamage(KFPRI, Injured, Instigator, InDamage, DmgType);
}

static function bool ShowStalkers(KFPlayerReplicationInfo KFPRI)
{
   return true;
}

// Stalkers distance multiplier
static function float GetStalkerViewDistanceMulti(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel >= 3)
   {
      switch (KFPRI.ClientVeteranSkillLevel)
      {
      case 3:
	      return 2500.0;
            
      case 4:
	      return 10000.0;
            
      case 5:
	      return 22500.0;
            
      case 6:
	      return 40000.0;
            
      case 7:
	      return 62500.0;
            
      case 8:
	      return 90000.0;
            
      case 9:
	      return 122500.0;

      case 10:
	      return 160000.0;
            
      case 11:
	      return 202500.0;

      case 12:
	      return 250000.0;

      case 13:
	      return 302500.0;

      case 14:
	      return 360000.0;
            
      case 15:
	      return 490000.0;
            
      case 16:
	      return 640000.0;

      case 17:
	      return 810000.0;

      case 18:
	      return 1000000.0;
      
      case 19:
	      return 1210000.0;

      default:
         return 1562500.0; // this is 25m, because each unit is 2cm, which means 1m is equal to 2500, since it's squared 50^2
      }
   }
}



// Display enemy health bars
static function SpecialHUDInfo(KFPlayerReplicationInfo KFPRI, Canvas C)
{
   local KFMonster KFEnemy;
   local HUDKillingFloor HKF;
   local float MaxDistanceSquared;
   
   if (KFPRI.ClientVeteranSkillLevel >= 3)
	{
	   HKF = HUDKillingFloor(C.ViewPort.Actor.myHUD);
	   
      if (HKF == none || C.ViewPort.Actor.Pawn == none) 
         return;
         
      switch (KFPRI.ClientVeteranSkillLevel)
      {
      case 3:
	      MaxDistanceSquared = 2500.0;
         break;
            
      case 4:
	      MaxDistanceSquared = 10000.0;
         break;
            
      case 5:
	      MaxDistanceSquared = 22500.0;
         break;
            
      case 6:
	      MaxDistanceSquared = 40000.0;
         break;
            
      case 7:
	      MaxDistanceSquared = 62500.0;
         break;
            
      case 8:
	      MaxDistanceSquared = 90000.0;
         break;
            
      case 9:
	      MaxDistanceSquared = 122500.0;
         break;

      case 10:
	      MaxDistanceSquared = 160000.0;
         break;
            
      case 11:
	      MaxDistanceSquared = 202500.0;
         break;

      case 12:
	      MaxDistanceSquared = 250000.0;
         break;

      case 13:
	      MaxDistanceSquared = 302500.0;
         break;

      case 14:
	      MaxDistanceSquared = 360000.0;
         break;
            
      case 15:
	      MaxDistanceSquared = 490000.0;
         break;
            
      case 16:
	      MaxDistanceSquared = 640000.0;
         break;

      case 17:
	      MaxDistanceSquared = 810000.0;
         break;

      case 18:
	      MaxDistanceSquared = 1000000.0;
         break;
      
      case 19:
	      MaxDistanceSquared = 1210000.0;
         break;

      default:
         MaxDistanceSquared = 1562500.0; // this is 25m, because each unit is 2cm, which means 1m is equal to 2500, since it's squared 50^2
         break;
      }
     
      foreach C.ViewPort.Actor.DynamicActors(class'KFMonster', KFEnemy)
      {
         if (KFEnemy.Health > 0 && (!KFEnemy.Cloaked() || KFEnemy.bZapped || KFEnemy.bSpotted) && VSizeSquared(KFEnemy.Location - C.ViewPort.Actor.Pawn.Location) < MaxDistanceSquared) 
            HKF.DrawHealthBar(C, KFEnemy, KFEnemy.Health, KFEnemy.HealthMax , 50.0); 
      }
   }
}

// Increase pickup mode
static function float GetAmmoPickupMod(KFPlayerReplicationInfo KFPRI, KFAmmunition Other)
{
	if ((BullpupAmmoFalk(Other)      != none  ||
	     AK47LLIAmmoFalk(Other)      != none  ||
        SCARMK17AmmoFalk(Other)     != none  ||
        M4AmmoFalk(Other)           != none  ||
        FNFALAmmoFalk(Other)        != none  || 
        MKb42AmmoFalk(Other)        != none  ||
        ThompsonAmmoFalk(Other)     != none  || 
        ThompsonDrumAmmoFalk(Other) != none  || 
        SPThompsonAmmoFalk(Other)   != none  ||
        AAR525AmmoFalk(Other)       != none  ||
        cz805bAmmoFalk(Other)       != none  ||
        AR15SAAmmoFalk(Other)       != none  ||
        AK12SAAmmoFalk(Other)       != none  ||
        Slayer_AUGAmmoFalk(Other)   != none  ||
        PDWAmmoFalk(Other)          != none  ||
        Glock18SAAmmoFalk(Other)    != none  ||
	     AK12LLIAmmoFalk(Other)      != none  ||
		  HKG36CSAAmmoFalk(Other)     != none) &&
        KFPRI.ClientVeteranSkillLevel > 0)
	{
	   if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.45;
	   
	   if (KFPRI.ClientVeteranSkillLevel >= 18)
         return 1.40;
      
      if (KFPRI.ClientVeteranSkillLevel >= 16)
         return 1.36;
      
      if (KFPRI.ClientVeteranSkillLevel >= 14)
         return 1.32;
         
      if (KFPRI.ClientVeteranSkillLevel >= 12)
         return 1.28;
      
      if (KFPRI.ClientVeteranSkillLevel >= 10)
         return 1.24;
      
      if (KFPRI.ClientVeteranSkillLevel >= 8)
         return 1.20;
      
      if (KFPRI.ClientVeteranSkillLevel >= 6)
         return 1.16;
      
      if (KFPRI.ClientVeteranSkillLevel >= 4)
         return 1.12;
      
      if (KFPRI.ClientVeteranSkillLevel >= 2)
         return 1.08;
	   
	   return 1.04;
	}
	
	return 1.0;
}


// Set number times Zed Time can be extended
static function int ZedTimeExtensions(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      return 5;
   
   else if (KFPRI.ClientVeteranSkillLevel >= 15)
      return 4;
   
   else if (KFPRI.ClientVeteranSkillLevel >= 12)
      return 3;
    
   else if (KFPRI.ClientVeteranSkillLevel >= 9)
      return 2;
      
   else if (KFPRI.ClientVeteranSkillLevel >= 6)
      return 1;
   
   return 0;
}


// Give Extra Items as Default
static function AddDefaultInventory(KFPlayerReplicationInfo KFPRI, Pawn P)
{ 
   // If Level 20 or Higher give them AUG A3
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      FalkAddPerkedWeapon(class'Slayer_AUGFalk', KFPRI, P);
      
   // If Level 15 or Higher give them PDW
   else if (KFPRI.ClientVeteranSkillLevel >= 15)
      FalkAddPerkedWeapon(class'PDWAssaultRifleFalk', KFPRI, P);
      
	// If Level 12 or Higher give them HK G36C
   else if (KFPRI.ClientVeteranSkillLevel >= 12)
      FalkAddPerkedWeapon(class'HKG36CSAAssaultRifleFalk', KFPRI, P);
   
	// If Level 9 or Higher give them AK12
   else if (KFPRI.ClientVeteranSkillLevel >= 9)
      FalkAddPerkedWeapon(class'AK12SAAssaultRifleFalk', KFPRI, P);

   super.AddDefaultInventory(KFPRI, P);
}


// skill based one shot on stalkers during zed time
static function bool ShouldOneShotStalkers(KFPlayerReplicationInfo KFPRI, class<DamageType> DmgType)
{
   //local Inventory CurInv; 
   //local Pawn Pwn; 

   if (static.InZedTime(KFPRI) && KFPRI.ClientVeteranSkillLevel >= 6 &&
      (DmgType == class'DamTypeBullpupFalk'               ||
       DmgType == class'DamTypeAK47LLIAssaultRifleFalk'   ||
       DmgType == class'DamTypeSCARMK17AssaultRifleFalk'  ||
       DmgType == class'DamTypeM4AssaultRifleFalk'        ||
       DmgType == class'DamTypeFNFALAssaultRifleFalk'     ||
       DmgType == class'DamTypeMKb42AssaultRifleFalk'     ||
       DmgType == class'DamTypeThompsonFalk'              ||
       DmgType == class'DamTypeThompsonDrumFalk'          ||
       DmgType == class'DamTypeSPThompsonFalk'            ||
       DmgType == class'DamTypeAAR525Falk'                ||
       DmgType == class'DamTypecz805bFalk'                ||
       DmgType == class'DamTypeAR15SAFalk'                ||
       DmgType == class'DamTypeAK12SAFalk'                ||
	    DmgType == class'DamTypeSlayer_AUGFalk'            ||
	    DmgType == class'DamTypePDWAssaultRifleFalk'       ||
	    DmgType == class'DamTypeGlock18SAFalk'             ||
	    DmgType == class'DamTypeAK12LLIFalk'               ||
	    DmgType == class'DamTypeHKG36CSAAssaultRifleFalk'))
   {
      return true;
 
      /*Pwn = Controller(KFPRI.Owner).Pawn;

      // Add some ammo to every commando weapon when a stalker is killed during zed time using onperk guns - replaced by shared stalker vision
      if (Pwn != None)
      {
         for (CurInv = Pwn.Inventory; CurInv != none; CurInv = CurInv.Inventory)
         {
            if (KFAmmunition(CurInv)            != none  && KFAmmunition(CurInv).bAcceptsAmmoPickups && 
                  (BullpupAmmoFalk(CurInv)      != none  ||
                   AK47LLIAmmoFalk(CurInv)      != none  ||
                   SCARMK17AmmoFalk(CurInv)     != none  ||
                   M4AmmoFalk(CurInv)           != none  ||
                   FNFALAmmoFalk(CurInv)        != none  || 
                   MKb42AmmoFalk(CurInv)        != none  ||
                   ThompsonAmmoFalk(CurInv)     != none  || 
                   ThompsonDrumAmmoFalk(CurInv) != none  || 
                   SPThompsonAmmoFalk(CurInv)   != none  ||
                   AAR525AmmoFalk(CurInv)       != none  ||
                   cz805bAmmoFalk(CurInv)       != none  ||
                   AR15SAAmmoFalk(CurInv)       != none  ||
                   AK12SAAmmoFalk(CurInv)       != none  ||
                   Slayer_AUGAmmoFalk(CurInv)   != none  ||
                   PDWAmmoFalk(CurInv)          != none  ||
                   Glock18SAAmmoFalk(CurInv)    != none  ||
                   AK12LLIAmmoFalk(CurInv)      != none  ||
                   HKG36CSAAmmoFalk(CurInv)     != none))
            {
               KFAmmunition(CurInv).AmmoAmount = Min(KFAmmunition(CurInv).MaxAmmo, KFAmmunition(CurInv).AmmoAmount + 5);
            }
         }

         return true;
      }*/
   }

   return false;
}

// should we have unlimited stalker viewing skill and share it during zed time?
static function bool ShareStalkerVisibility(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel >= 6)
      return True;

   return False;
}


// Add acid nade
static function class<Grenade> GetNadeType(KFPlayerReplicationInfo KFPRI)
{
   return class'AcidNadeFalk';	
}

defaultproperties
{
	PerkIndex=3

	OnHUDIcon=Texture'KillingFloorHUD.Perks.Perk_Commando'
	OnHUDGoldIcon=Texture'KillingFloor2HUD.Perk_Icons.Perk_Commando_Gold'
	VeterancyName="Commando"

   NumRequirements=2
   
   Requirements[0]="Kill %x stalkers with commando weapons and corrosive grenades"
   Requirements[1]="Deal %x damage with commando weapons"
	
   SRLevelEffects(0)="4% more damage with commando weapons and corrosive grenades|3% less recoil with commando weapons|4% larger commando weapons magazines|1% extra commando weapons ammo|2% faster reload with commando weapons|4% resistance to corrosive grenades|3% discount on commando weapons|Equipped with corrosive grenades"
   SRLevelEffects(1)="8% more damage with commando weapons and corrosive grenades|6% less recoil with commando weapons|4% larger commando weapons magazines|2% extra commando weapons ammo|4% faster reload with commando weapons|8% resistance to corrosive grenades|6% discount on commando weapons|Equipped with corrosive grenades"
   SRLevelEffects(2)="12% more damage with commando weapons and corrosive grenades|9% less recoil with commando weapons|8% larger commando weapons magazines|3% extra commando weapons ammo|6% faster reload with commando weapons|12% resistance to corrosive grenades|9% discount on commando weapons|Equipped with corrosive grenades"
   SRLevelEffects(3)="16% more damage with commando weapons and corrosive grenades|12% less recoil with commando weapons|8% larger commando weapons magazines|4% extra commando weapons ammo|8% faster reload with commando weapons|16% resistance to corrosive grenades|10% less damage from stalkers|12% discount on commando weapons|Equipped with corrosive grenades"
   SRLevelEffects(4)="20% more damage with commando weapons and corrosive grenades|15% less recoil with commando weapons|12% larger commando weapons magazines|5% extra commando weapons ammo|10% faster reload with commando weapons|20% resistance to corrosive grenades|10% less damage from stalkers|15% discount on commando weapons|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 2m|Can see enemy health from 2m"
   SRLevelEffects(5)="24% more damage with commando weapons and corrosive grenades|18% less recoil with commando weapons|12% larger commando weapons magazines|6% extra commando weapons ammo|12% faster reload with commando weapons|24% resistance to corrosive grenades|10% less damage from stalkers|18% discount on commando weapons|10% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 3m|Can see enemy health from 3m"
   SRLevelEffects(6)="28% more damage with commando weapons and corrosive grenades|21% less recoil with commando weapons|16% larger commando weapons magazines|7% extra commando weapons ammo|14% faster reload with commando weapons|28% resistance to corrosive grenades|20% less damage from stalkers|21% discount on commando weapons|10% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 4m|Can see enemy health from 4m|Up to 1 zed-time extension|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
   SRLevelEffects(7)="32% more damage with commando weapons and corrosive grenades|24% less recoil with commando weapons|16% larger commando weapons magazines|8% extra commando weapons ammo|16% faster reload with commando weapons|32% resistance to corrosive grenades|20% less damage from stalkers|24% discount on commando weapons|10% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 5m|Can see enemy health from 5m|Up to 1 zed-time extension|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
   SRLevelEffects(8)="36% more damage with commando weapons and corrosive grenades|27% less recoil with commando weapons|20% larger commando weapons magazines|9% extra commando weapons ammo|18% faster reload with commando weapons|36% resistance to corrosive grenades|20% less damage from stalkers|27% discount on commando weapons|10% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 6m|Can see enemy health from 6m|Up to 1 zed-time extension|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
   SRLevelEffects(9)="40% more damage with commando weapons and corrosive grenades|30% less recoil with commando weapons|20% larger commando weapons magazines|10% extra commando weapons ammo|20% faster reload with commando weapons|40% resistance to corrosive grenades|30% less damage from stalkers|30% discount on commando weapons|10% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 7m|Can see enemy health from 7m|Up to 2 zed-time extensions|Spawn with an AK12|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
   SRLevelEffects(10)="44% more damage with commando weapons and corrosive grenades|33% less recoil with commando weapons|24% larger commando weapons magazines|11% extra commando weapons ammo|22% faster reload with commando weapons|44% resistance to corrosive grenades|30% less damage from stalkers|33% discount on commando weapons|20% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 8m|Can see enemy health from 8m|Up to 2 zed-time extensions|Spawn with an AK12|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
   SRLevelEffects(11)="48% more damage with commando weapons and corrosive grenades|36% less recoil with commando weapons|24% larger commando weapons magazines|12% extra commando weapons ammo|24% faster reload with commando weapons|48% resistance to corrosive grenades|30% less damage from stalkers|36% discount on commando weapons|20% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 9m|Can see enemy health from 9m|Up to 2 zed-time extensions|Spawn with an AK12|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
   SRLevelEffects(12)="52% more damage with commando weapons and corrosive grenades|39% less recoil with commando weapons|28% larger commando weapons magazines|13% extra commando weapons ammo|26% faster reload with commando weapons|52% resistance to corrosive grenades|40% less damage from stalkers|40% discount on commando weapons|20% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 10m|Can see enemy health from 10m|Up to 3 zed-time extensions|Spawn with a G36C|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
   SRLevelEffects(13)="56% more damage with commando weapons and corrosive grenades|42% less recoil with commando weapons|28% larger commando weapons magazines|14% extra commando weapons ammo|28% faster reload with commando weapons|56% resistance to corrosive grenades|40% less damage from stalkers|44% discount on commando weapons|20% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 11m|Can see enemy health from 11m|Up to 3 zed-time extensions|Spawn with a G36C|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
   SRLevelEffects(14)="60% more damage with commando weapons and corrosive grenades|45% less recoil with commando weapons|32% larger commando weapons magazines|15% extra commando weapons ammo|30% faster reload with commando weapons|60% resistance to corrosive grenades|40% less damage from stalkers|48% discount on commando weapons|20% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 12m|Can see enemy health from 12m|Up to 3 zed-time extensions|Spawn with a G36C|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
   SRLevelEffects(15)="64% more damage with commando weapons and corrosive grenades|48% less recoil with commando weapons|32% larger commando weapons magazines|16% extra commando weapons ammo|32% faster reload with commando weapons|64% resistance to corrosive grenades|50% less damage from stalkers|52% discount on commando weapons|30% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 14m|Can see enemy health from 14m|Up to 4 zed-time extensions|Spawn with a PDW|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
   SRLevelEffects(16)="68% more damage with commando weapons and corrosive grenades|51% less recoil with commando weapons|36% larger commando weapons magazines|17% extra commando weapons ammo|34% faster reload with commando weapons|68% resistance to corrosive grenades|50% less damage from stalkers|56% discount on commando weapons|30% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 16m|Can see enemy health from 16m|Up to 4 zed-time extensions|Spawn with a PDW|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
   SRLevelEffects(17)="72% more damage with commando weapons and corrosive grenades|54% less recoil with commando weapons|36% larger commando weapons magazines|18% extra commando weapons ammo|36% faster reload with commando weapons|72% resistance to corrosive grenades|50% less damage from stalkers|60% discount on commando weapons|30% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 18m|Can see enemy health from 18m|Up to 4 zed-time extensions|Spawn with a PDW|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
   SRLevelEffects(18)="76% more damage with commando weapons and corrosive grenades|57% less recoil with commando weapons|40% larger commando weapons magazines|19% extra commando weapons ammo|38% faster reload with commando weapons|76% resistance to corrosive grenades|50% less damage from stalkers|64% discount on commando weapons|30% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 20m|Can see enemy health from 20m|Up to 4 zed-time extensions|Spawn with a PDW|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
   SRLevelEffects(19)="80% more damage with commando weapons and corrosive grenades|60% less recoil with commando weapons|40% larger commando weapons magazines|20% extra commando weapons ammo|40% faster reload with commando weapons|80% resistance to corrosive grenades|50% less damage from stalkers|68% discount on commando weapons|30% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 22m|Can see enemy health from 22m|Up to 4 zed-time extensions|Spawn with a PDW|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
   SRLevelEffects(20)="90% more damage with commando weapons and corrosive grenades|65% less recoil with commando weapons|45% larger commando weapons magazines|25% extra commando weapons ammo|45% faster reload with commando weapons|90% resistance to corrosive grenades|75% less damage from stalkers|75% discount on commando weapons|40% discount on tools|Equipped with corrosive grenades|Can see cloaked stalkers and Patriarch from 25m|Can see enemy health from 25m|Up to 5 zed-time extensions|Spawn with an AUG A3|Zed-time skill: commando weapons kill stalkers in one hit, and your squad can see cloaked stalkers at any distance"
}
