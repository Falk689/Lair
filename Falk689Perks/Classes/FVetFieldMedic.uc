class FVetFieldMedic extends VeterancyTypesFalk;

static function AddCustomStats( ClientPerkRepLink Other )
{
   // Needed for damage perk progress
   Other.AddCustomValue(Class'Falk689WeaponsFix.FMedicDamage');
}

static function int GetPerkProgressInt(ClientPerkRepLink StatOther, out int FinalInt, byte CurLevel, byte ReqNum)
{
   switch(CurLevel)
   {
   case 0:
      if(ReqNum == 0)
         FinalInt = 1;         
      else
         FinalInt = 1;
      break;
         
   case 1:
      if(ReqNum == 0)
         FinalInt = 10;
      else
         FinalInt = 100;
      break;
         
   case 2:
      if(ReqNum == 0)
         FinalInt = 100;
      else
         FinalInt = 1000;
      break;

   case 3:
      if(ReqNum == 0)
         FinalInt = 400;
      else
         FinalInt = 3500;
      break;  
      
   case 4:
      if(ReqNum == 0)
         FinalInt = 850;
      else
         FinalInt = 8000;
      break;
         
   case 5:
      if(ReqNum == 0)
         FinalInt = 1500;
      else
         FinalInt = 15000;
      break;
      
   case 6:
      if(ReqNum == 0)
         FinalInt = 2500;
      else
         FinalInt = 30000;
      break;
      
   case 7:
      if(ReqNum == 0)
         FinalInt = 4000;
      else
         FinalInt = 70000;
      break;
      
   case 8:
      if(ReqNum == 0)
         FinalInt = 6000;
      else
         FinalInt = 150000;
      break;
      
   case 9:
      if(ReqNum == 0)
         FinalInt = 8500;
      else
         FinalInt = 300000;
      break;
      
   case 10:
      if(ReqNum == 0)
         FinalInt = 12000;
      else
         FinalInt = 500000;
      break;
      
   case 11:
      if(ReqNum == 0)
         FinalInt = 17000;
      else
         FinalInt = 750000;
      break;
         
   case 12:
      if(ReqNum == 0)
         FinalInt = 25000;
      else
         FinalInt = 1100000;
      break; 
         
   case 13:
      if(ReqNum == 0)
         FinalInt = 35000;
      else
         FinalInt = 1500000;
      break; 
         
   case 14:
      if(ReqNum == 0)
         FinalInt = 50000;
      else
         FinalInt = 2200000;
      break; 
         
   case 15:
      if(ReqNum == 0)
         FinalInt = 68000;
      else
         FinalInt = 3000000;
      break; 
         
   case 16:
      if(ReqNum == 0)
         FinalInt = 90000;
      else
         FinalInt = 4000000;
      break; 
         
   case 17:
      if(ReqNum == 0)
         FinalInt = 115000;
      else
         FinalInt = 5200000;
      break; 
         
   case 18:
      if(ReqNum == 0)
         FinalInt = 143000;
      else
         FinalInt = 6500000;
      break; 
         
   case 19:
      if(ReqNum == 0)
         FinalInt = 188000;
      else
         FinalInt = 8000000;
      break; 
         
   case 20:
      if(ReqNum == 0)
         FinalInt = 230000;
      else
         FinalInt = 10000000;
      break;
		
   default:
      if(ReqNum == 0)
         FinalInt = 230000 + GetDoubleScaling(CurLevel, 350);
      else
         FinalInt = 10000000 + GetDoubleScaling(CurLevel, 500000);
      break;
   }
   
   if(ReqNum == 0)
      return Min(StatOther.RDamageHealedStat, FinalInt);
         
   return Min(StatOther.GetCustomValueInt(Class'Falk689WeaponsFix.FMedicDamage'), FinalInt);
}

// Change the syringe charge rate
static function float GetSyringeChargeRate(KFPlayerReplicationInfo KFPRI)
{
   local float result;
   
	switch (KFPRI.ClientVeteranSkillLevel)
	{
      case 0:
         result = 1.08;
         break;

      case 1:
         result = 1.16;
         break;
	   
      case 2:
         result = 1.24;
         break;
	   
      case 3:
         result = 1.32;
         break;
	   
      case 4:
         result = 1.40;
         break;

      case 5:
         result = 1.48;
         break;
      
      case 6:
         result = 1.56;
         break;
      
      case 7:
         result = 1.64;
         break;
      
      case 8:
         result = 1.72;
         break;
     
      case 9:
         result = 1.80;
         break;
      
      case 10:
         result = 1.88;
         break;
      
      case 11:
         result = 1.96;
         break;
      
      case 12:
         result = 2.04;
         break;
      
      case 13:
         result = 2.12;
         break;
      
      case 14:
         result = 2.20;
         break;
      
      case 15:
         result = 2.28;
         break;
      
      case 16:
         result = 2.36;
         break;
      
      case 17:
         result = 2.44;
         break;
      
      case 18:
         result = 2.52;
         break;
      
      case 19:
         result = 2.60;
         break;
      
      case 20:
         result = 2.80;
         break;
         
      default:
         result = 3.0 + (0.1 * KFPRI.ClientVeteranSkillLevel);
         break;
	}
	
   return result;
}

// Add medic grenades
static function class<Grenade> GetNadeType(KFPlayerReplicationInfo KFPRI)
{
   return class'MedicNadeFalk';	
}


// Change heal potency
static function float GetHealPotency(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel < 20)
      return 1.05 + KFPRI.ClientVeteranSkillLevel * 0.05;
	
   else if (KFPRI.ClientVeteranSkillLevel == 20)
      return 2.25;
	
   return 1.25 + KFPRI.ClientVeteranSkillLevel * 0.05;
}


// Change movement speed
static function float GetMovementSpeedModifier(KFPlayerReplicationInfo KFPRI, KFGameReplicationInfo KFGRI)
{
   local float result;
   
	switch (KFPRI.ClientVeteranSkillLevel)
	{
      case 0:
         result = 1.01;
         break;

      case 1:
         result = 1.02;
         break;
	   
      case 2:
         result = 1.03;
         break;
	   
      case 3:
         result = 1.04;
         break;
	   
      case 4:
         result = 1.05;
         break;

      case 5:
         result = 1.06;
         break;
      
      case 6:
         result = 1.07;
         break;
      
      case 7:
         result = 1.08;
         break;
      
      case 8:
         result = 1.09;
         break;
     
      case 9:
         result = 1.10;
         break;
      
      case 10:
         result = 1.11;
         break;
      
      case 11:
         result = 1.12;
         break;
      
      case 12:
         result = 1.13;
         break;
      
      case 13:
         result = 1.14;
         break;
      
      case 14:
         result = 1.15;
         break;
      
      case 15:
         result = 1.17;
         break;
      
      case 16:
         result = 1.19;
         break;
      
      case 17:
         result = 1.21;
         break;
      
      case 18:
         result = 1.23;
         break;
      
      case 19:
         result = 1.25;
         break;
         
      default:
         result = 1.30;
         break;
	}
	
   return result;
}

/*
Reduce damage from Bile Launcher
static function int ReduceDamage(KFPlayerReplicationInfo KFPRI, KFPawn Injured, Pawn Instigator, int InDamage, class<DamageType> DmgType)
{   
   if (DmgType == class'DamTypeBlowerThrowerFalk')
   {
      switch (KFPRI.ClientVeteranSkillLevel)
      {
         case 0:
            return InDamage * 0.96;
            
         case 1:
            return InDamage * 0.92;
            
         case 2:
            return InDamage * 0.88;
            
         case 3:
            return InDamage * 0.84;
            
         case 4:
            return InDamage * 0.80;
            
         case 5:
            return InDamage * 0.76;
            
         case 6:
            return InDamage * 0.72;
            
         case 7:
            return InDamage * 0.68;
            
         case 8:
            return InDamage * 0.64;
            
         case 9:
            return InDamage * 0.60;
            
         case 10:
            return InDamage * 0.56;
            
         case 11:
            return InDamage * 0.52;
            
         case 12:
            return InDamage * 0.48;

         case 13:
            return InDamage * 0.44;
            
         case 14:
            return InDamage * 0.40;

         case 15:
            return InDamage * 0.35;

         case 16:
            return InDamage * 0.30;

         case 17:
            return InDamage * 0.25;
            
         case 18:
            return InDamage * 0.20;
            
         case 19:
            return InDamage * 0.15;
      }
      
      return InDamage * 0.00;
   }

   return Super.ReduceDamage(KFPRI, Injured, Instigator, InDamage, DmgType);
}
*/

// Larger magazines for medic guns
static function float GetMagCapacityMod(KFPlayerReplicationInfo KFPRI, KFWeapon Other)
{
   if (MediShotFalk(Other)            == none &&
       LocalMediShotFalk(Other)       == none &&
       MedicBrowningFalk(Other)       == none &&
       (MP7MMedicGun(Other)           != none ||
	    Lr300M203MedGun(Other)         != none ||
       BlowerThrower(Other)           != none ||
	    RU556AssaultRifle(Other)       != none))
   {
      if(KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.9;
      
      return 0.04 * KFPRI.ClientVeteranSkillLevel + 1.04;
   }
   
   return 1.0;
}


// Change pickup for medic guns
static function float GetAmmoPickupMod(KFPlayerReplicationInfo KFPRI, KFAmmunition Other)
{
   if (MP7MAmmoFalk(Other)           != none ||
       MP5MAmmoFalk(Other)           != none ||
       M7A3MAmmoFalk(Other)          != none ||
       KrissMAmmoFalk(Other)         != none ||
       BlowerThrowerAmmoFalk(Other)  != none ||
	    Lr300M203MedGunAmmo(Other)     != none ||
	    RU556Ammo(Other)               != none)
   {
      if(KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.9;
      
      return 0.04 * KFPRI.ClientVeteranSkillLevel + 1.04;
   }
   
   return 1.0;
}


// Add damage to medic nades dot
static function float MedicDotDamage(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      return 0.6;

   else if (KFPRI.ClientVeteranSkillLevel >= 15)
      return 0.5;

   else if (KFPRI.ClientVeteranSkillLevel >= 12)
      return 0.4;

   else if (KFPRI.ClientVeteranSkillLevel >= 9)
      return 0.3;
	  
   else if (KFPRI.ClientVeteranSkillLevel >= 6)
      return 0.2; 

   return 0.1;
}


// Change the cost of medic guns
static function float GetCostScaling(KFPlayerReplicationInfo KFPRI, class<Pickup> Item)
{
   local float result;

   result = Super.GetCostScaling(KFPRI, Item);

   if (result != 1.0)
      return result;

   switch (KFPRI.ClientVeteranSkillLevel)
   {
      case 0:
         result = 0.97;
         break;
            
      case 1:
         result = 0.94;
         break;
            
      case 2:
         result = 0.91;
         break;
            
      case 3:
         result = 0.88;
         break;
            
      case 4:
         result = 0.85;
         break;
            
      case 5:
         result = 0.82;
         break;
            
      case 6:
         result = 0.79;
         break;
            
      case 7:
         result = 0.76;
         break;
            
      case 8:
         result = 0.73;
         break;
            
      case 9:
         result = 0.70;
         break;
            
      case 10:
         result = 0.67;
         break;
            
      case 11:
         result = 0.64;
         break;
            
      case 12:
         result = 0.60;
         break;

      case 13:
         result = 0.56;
         break;
            
      case 14:
         result = 0.52;
         break;

      case 15:
         result = 0.48;
         break;

      case 16:
         result = 0.44;
         break;

      case 17:
         result = 0.40;
         break;
            
      case 18:
         result = 0.36;
         break;
            
      case 19:
         result = 0.32;
         break;
            
      default:
         result = 0.25;
         break;
   }

	if (Item == class'MP7MPickupFalk'           ||
	    Item == class'MP5MPickupFalk'           ||
	    Item == class'M7A3MPickupFalk'          || 
	    Item == class'KrissMPickupFalk'         || 
	    Item == class'BlowerThrowerPickupFalk'  ||
	    Item == class'MediShotPickupFalk'       ||
	    Item == class'LocalMediShotPickupFalk'  ||
	    Item == class'Lr300M203MedGunPickup'    ||
		 Item == class'RU556Pickup'              ||
        Item == class'MedicBrowningPickupFalk')
	       return result;

	return Super.GetCostScaling(KFPRI, Item);
}


// Faster bile launcher reload
static function float GetReloadSpeedModifier(KFPlayerReplicationInfo KFPRI, KFWeapon Other)
{
   if (BlowerThrowerFalk(Other) != none)
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.65;
   
      return 0.03 * KFPRI.ClientVeteranSkillLevel + 1.03;
   }
   
   return 1.0;
}

// Give Extra Items as Default
static function AddDefaultInventory(KFPlayerReplicationInfo KFPRI, Pawn P)
{
   // If Level 20 or Higher give them Vector-M
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      FalkAddPerkedWeapon(class'KrissMMedicGunFalk', KFPRI, P);

   // If Level 15 or Higher give them MP5-M
   else if (KFPRI.ClientVeteranSkillLevel >= 15)
      FalkAddPerkedWeapon(class'MP5MMedicGunFalk', KFPRI, P);

   // If Level 12 or Higher give them MP7-M
   else if (KFPRI.ClientVeteranSkillLevel >= 12)
      FalkAddPerkedWeapon(class'MP7MMedicGunFalk', KFPRI, P);

   // If Level 9 or Higher, give them HP-M
   else if (KFPRI.ClientVeteranSkillLevel >= 9)
      FalkAddPerkedWeapon(class'MedicBrowningFalk', KFPRI, P);

   super.AddDefaultInventory(KFPRI, P);
}

// Return the medic dart heal radius modifier
static function float GetHealRadiusMod(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      return 2.0;

   else if (KFPRI.ClientVeteranSkillLevel >= 15)
      return 1.80;

   else if (KFPRI.ClientVeteranSkillLevel >= 12)
      return 1.50;

   else if (KFPRI.ClientVeteranSkillLevel >= 6)
      return 1.25;

   return 1.0;
}

// alt fire speed mode
static function float GetAltFireSpeedMod(KFPlayerReplicationInfo KFPRI)
{ 
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      return 3.0;

   if (KFPRI.ClientVeteranSkillLevel >= 15)
      return 2.5;

   if (KFPRI.ClientVeteranSkillLevel >= 12)
      return 2.2;

   if (KFPRI.ClientVeteranSkillLevel >= 9)
      return 1.9;

   if (KFPRI.ClientVeteranSkillLevel >= 6)
      return 1.6;

   if (KFPRI.ClientVeteranSkillLevel >= 3)
      return 1.4;

   return 1.2;
}

// should we fire infinite medic darts?
static function bool InfiniteMedicDarts(KFPlayerReplicationInfo KFPRI)
{
   if (KFPRI.ClientVeteranSkillLevel >= 6)
      return static.InZedTime(KFPRI);

   return false;
}

// Called on zed time start
static function StartZedTime(KFPlayerReplicationInfo KFPRI)
{
   local Inventory CurInv; 
   local Pawn Pwn; 


   if (KFPRI.ClientVeteranSkillLevel >= 6)
   {
      Pwn = Controller(KFPRI.Owner).Pawn;

      for (CurInv = Pwn.Inventory; CurInv != none; CurInv = CurInv.Inventory)
      {
         if (MP7MMedicGunFalk(CurInv) != None)
         {
            MP7MMedicGunFalk(CurInv).ForceHealingReload();
            MP7MMedicGunFalk(CurInv).StartForceReload();
         }

         else if (FMediShotBaseFalk(CurInv) != None)
         {
            FMediShotBaseFalk(CurInv).ForceHealingReload();
            FMediShotBaseFalk(CurInv).StartForceReload();
         }

         else if (SyringeFalk(CurInv) != None)
            SyringeFalk(CurInv).StartForceReload();
      }
   }
}

// Reduced carry capacity
static function int AddCarryMaxWeight(KFPlayerReplicationInfo KFPRI)
{ 
   return -3;
}

defaultproperties
{  
   PerkIndex=0

	OnHUDIcon=Texture'KillingFloorHUD.Perks.Perk_Medic'
	OnHUDGoldIcon=Texture'KillingFloor2HUD.Perk_Icons.Perk_Medic_Gold'
	VeterancyName="Medic"
	
	NumRequirements=2
	
	Requirements[0]="Heal %x health points on your teammates"
   Requirements[1]="Deal %x damage with medic guns and medical smoke grenades"
 
   SRLevelEffects(0)="8% faster syringe/darts recharge|5% better healing|20% faster darts fire rate|10% damage over time with healing grenades|1% faster movement speed|4% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|3% faster Bile Launcher reload|Can carry up to 12kg|3% discount on medic guns|Equipped with medical smoke grenades"
   SRLevelEffects(1)="16% faster syringe/darts recharge|10% better healing|20% faster darts fire rate|10% damage over time with healing grenades|2% faster movement speed|8% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|6% faster Bile Launcher reload|Can carry up to 12kg|6% discount on medic guns|Equipped with medical smoke grenades"
   SRLevelEffects(2)="24% faster syringe/darts recharge|15% better healing|20% faster darts fire rate|10% damage over time with healing grenades|3% faster movement speed|12% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|9% faster Bile Launcher reload|Can carry up to 12kg|9% discount on medic guns|Equipped with medical smoke grenades"
   SRLevelEffects(3)="32% faster syringe/darts recharge|20% better healing|40% faster darts fire rate|10% damage over time with healing grenades|4% faster movement speed|16% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|12% faster Bile Launcher reload|Can carry up to 12kg|12% discount on medic guns|Equipped with medical smoke grenades"
   SRLevelEffects(4)="40% faster syringe/darts recharge|25% better healing|40% faster darts fire rate|10% damage over time with healing grenades|5% faster movement speed|20% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|15% faster Bile Launcher reload|Can carry up to 12kg|15% discount on medic guns|Equipped with medical smoke grenades"
   SRLevelEffects(5)="48% faster syringe/darts recharge|30% better healing|40% faster darts fire rate|10% damage over time with healing grenades|6% faster movement speed|24% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|18% faster Bile Launcher reload|Can carry up to 12kg|18% discount on medic guns|10% discount on tools|Equipped with medical smoke grenades"
   SRLevelEffects(6)="56% faster syringe/darts recharge|35% better healing|60% faster darts fire rate|25% extra radius on darts area of effect|20% damage over time with healing grenades|7% faster movement speed|28% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|21% faster Bile Launcher reload|Can carry up to 12kg|21% discount on medic guns|10% discount on tools|Equipped with medical smoke grenades|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
   SRLevelEffects(7)="64% faster syringe/darts recharge|40% better healing|60% faster darts fire rate|25% extra radius on darts area of effect|20% damage over time with healing grenades|8% faster movement speed|32% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|24% faster Bile Launcher reload|Can carry up to 12kg|24% discount on medic guns|10% discount on tools|Equipped with medical smoke grenades|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
   SRLevelEffects(8)="72% faster syringe/darts recharge|45% better healing|60% faster darts fire rate|25% extra radius on darts area of effect|20% damage over time with healing grenades|9% faster movement speed|36% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|27% faster Bile Launcher reload|Can carry up to 12kg|27% discount on medic guns|10% discount on tools|Equipped with medical smoke grenades|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
   SRLevelEffects(9)="80% faster syringe/darts recharge|50% better healing|90% faster darts fire rate|50% extra radius on darts area of effect|30% damage over time with healing grenades|10% faster movement speed|40% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|30% faster Bile Launcher reload|Can carry up to 12kg|30% discount on medic guns|10% discount on tools|Equipped with medical smoke grenades|Spawn with an HP-M|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
   SRLevelEffects(10)="88% faster syringe/darts recharge|55% better healing|90% faster darts fire rate|50% extra radius on darts area of effect|30% damage over time with healing grenades|11% faster movement speed|44% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|33% faster Bile Launcher reload|Can carry up to 12kg|33% discount on medic guns|20% discount on tools|Equipped with medical smoke grenades|Spawn with an HP-M|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
   SRLevelEffects(11)="96% faster syringe/darts recharge|60% better healing|90% faster darts fire rate|50% extra radius on darts area of effect|30% damage over time with healing grenades|12% faster movement speed|48% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|36% faster Bile Launcher reload|Can carry up to 12kg|36% discount on medic guns|20% discount on tools|Equipped with medical smoke grenades|Spawn with an HP-M|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
   SRLevelEffects(12)="104% faster syringe/darts recharge|65% better healing|120% faster darts fire rate|80% extra radius on darts area of effect|40% damage over time with healing grenades|13% faster movement speed|52% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|39% faster Bile Launcher reload|Can carry up to 12kg|40% discount on medic guns|20% discount on tools|Equipped with medical smoke grenades|Spawn with an MP7-M|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
   SRLevelEffects(13)="112% faster syringe/darts recharge|70% better healing|120% faster darts fire rate|80% extra radius on darts area of effect|40% damage over time with healing grenades|14% faster movement speed|56% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|42% faster Bile Launcher reload|Can carry up to 12kg|44% discount on medic guns|20% discount on tools|Equipped with medical smoke grenades|Spawn with an MP7-M|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
   SRLevelEffects(14)="120% faster syringe/darts recharge|75% better healing|120% faster darts fire rate|80% extra radius on darts area of effect|40% damage over time with healing grenades|15% faster movement speed|60% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|45% faster Bile Launcher reload|Can carry up to 12kg|48% discount on medic guns|20% discount on tools|Equipped with medical smoke grenades|Spawn with an MP7-M|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
   SRLevelEffects(15)="128% faster syringe/darts recharge|80% better healing|150% faster darts fire rate|125% extra radius on darts area of effect|50% damage over time with healing grenades|17% faster movement speed|64% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|48% faster Bile Launcher reload|Can carry up to 12kg|52% discount on medic guns|30% discount on tools|Equipped with medical smoke grenades|Spawn with an MP5-M|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
   SRLevelEffects(16)="136% faster syringe/darts recharge|85% better healing|150% faster darts fire rate|125% extra radius on darts area of effect|50% damage over time with healing grenades|19% faster movement speed|68% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|51% faster Bile Launcher reload|Can carry up to 12kg|56% discount on medic guns|30% discount on tools|Equipped with medical smoke grenades|Spawn with an MP5-M|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
   SRLevelEffects(17)="144% faster syringe/darts recharge|90% better healing|150% faster darts fire rate|125% extra radius on darts area of effect|50% damage over time with healing grenades|21% faster movement speed|72% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|54% faster Bile Launcher reload|Can carry up to 12kg|60% discount on medic guns|30% discount on tools|Equipped with medical smoke grenades|Spawn with an MP5-M|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
   SRLevelEffects(18)="152% faster syringe/darts recharge|95% better healing|150% faster darts fire rate|125% extra radius on darts area of effect|50% damage over time with healing grenades|23% faster movement speed|76% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|57% faster Bile Launcher reload|Can carry up to 12kg|64% discount on medic guns|30% discount on tools|Equipped with medical smoke grenades|Spawn with an MP5-M|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
   SRLevelEffects(19)="160% faster syringe/darts recharge|100% better healing|150% faster darts fire rate|125% extra radius on darts area of effect|50% damage over time with healing grenades|25% faster movement speed|80% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|60% faster Bile Launcher reload|Can carry up to 12kg|68% discount on medic guns|30% discount on tools|Equipped with medical smoke grenades|Spawn with an MP5-M|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
   SRLevelEffects(20)="180% faster syringe/darts recharge|125% better healing|200% faster darts fire rate|200% extra radius on darts area of effect|60% damage over time with healing grenades|30% faster movement speed|90% larger MP7-M/Bile Launcher/MP5-M/M7A3-M/Vector-M/SR556-M/LR300-M magazines|65% faster Bile Launcher reload|Can carry up to 12kg|75% discount on medic guns|40% discount on tools|Equipped with medical smoke grenades|Spawn with a Vector-M|Zed-time skill: medic guns healing capacity is instantly refilled and medic darts don't consume healing gauge, medical syringe also gains these bonuses"
}
