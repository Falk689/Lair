class Falk689GameTypeBase extends KFMod.KFGameType;

const fNopeHash =  "20b300195d48c2ccc2651885cfea1a2f";

enum FalkIdState                                      // possible fStates passed to ZedTimeIndex
{
    F_None,                                           // no state specified
    F_Disconnected,                                   // disconnected or spectator
    F_Died,                                           // a player died
    F_ZedTime,                                        // a zed time event started
    F_Other,                                          // other unspecified events that should skip F_None checks
};

enum FalkWaveState                                    // current state of the wave
{
    F_Wave_Init,                                      // the wave is initializing
    F_Wave_In_Progress,                               // the wave is currently in progress
    F_Wave_Ending,                                    // the wave is about to and (as there are no spawns left)
    F_Wave_Boss,                                      // we're in the boss wave but not spawning zeds, used to prevent spawning the patriarch multiple times
    F_Wave_Boss_Ended,                                // used to trigger a winning condition after the pat is killed
};

var int                        FWaveBonusNRM;         // bonus when we survive at normal difficulty
var int                        FWaveBonusHRD;         // bonus when we survive at hard difficulty
var int                        FWaveBonusSUI;         // bonus when we survive at suicidal difficulty
var int                        FWaveBonusHOE;         // bonus when we survive at hell on earth difficulty
var int                        FWaveBonusBB;          // bonus when we survive at bloodbath difficulty
var int                        FLastWaveBonusNRM;     // last wave bonus, how much to award before the pat wave at normal difficulty
var int                        FLastWaveBonusHRD;     // last wave bonus, how much to award before the pat wave at hard difficulty
var int                        FLastWaveBonusSUI;     // last wave bonus, how much to award before the pat wave at suicidal difficulty
var int                        FLastWaveBonusHOE;     // last wave bonus, how much to award before the pat wave at hell on earth difficulty
var int                        FLastWaveBonusBB;      // last wave bonus, how much to award before the pat wave at bloodbath difficulty
var int                        FStartingCashNRM;      // starting cash at normal difficulty
var int                        FStartingCashHRD;      // starting cash at hard difficulty
var int                        FStartingCashSUI;      // starting cash at suicidal difficulty
var int                        FStartingCashHOE;      // starting cash at hell on earth difficulty
var int                        FStartingCashBB;       // starting cash at bloodbath difficulty
var int                        FWaveAddCashNRM;       // how much cash we add every wave for late joiners at normal difficulty
var int                        FWaveAddCashHRD;       // how much cash we add every wave for late joiners at hard difficulty
var int                        FWaveAddCashSUI;       // how much cash we add every wave for late joiners at suicidal difficulty
var int                        FWaveAddCashHOE;       // how much cash we add every wave for late joiners at hell on earth difficulty
var int                        FWaveAddCashBB;        // how much cash we add every wave for late joiners at bloodbath difficulty
var int                        FLastWaveAddCashNRM;   // additional cash we add the last wave for late joiners at normal difficulty
var int                        FLastWaveAddCashHRD;   // additional cash we add the last wave for late joiners at hard difficulty
var int                        FLastWaveAddCashSUI;   // additional cash we add the last wave for late joiners at suicidal difficulty
var int                        FLastWaveAddCashHOE;   // additional cash we add the last wave for late joiners at hell on earth difficulty
var int                        FLastWaveAddCashBB;    // additional cash we add the last wave for late joiners at bloodbath difficulty
var int                        FAlivePlayers;         // how many players are still breathing
var int                        FWavePlayers;          // how many players were alive at the start of the wave
var int                        FSkipVoters;           // how many players have voted to skip this trader
var int                        FZedDebugID;           // debug ID, used to make log more readable when multiple zeds log at the same time
var int                        FZSpawnTries;          // default amount of zombievolume spawn retries
var float                      fZedTimeEnd;           // when the last zedtime ended
var float                      fLastFDeleteTime;      // when we deleted the last flare
var float                      fLastKilledZedTimer;   // timer, restarted after a zed is killed, when zero we should increase fNoZedHeal and fNoZedWeld and eventually stop farming money
var int                        fMaxNoZedHeal;         // maximum number of no zed heals before blocking money reward
var int                        fMaxNoZedWeld;         // maximum number of no zed welded armor points before blocking money reward
var int                        fCurrentFlares;        // how many are currently active in the map
var int                        FCurrentWave;          // current wave used to set dosh properly in trader time
var int                        fSStalkerSkills;       // amount of alive players sharing the shared unlimited stalker viewing skill
var bool                       fChangeCurWave;        // prevents changing current wave multiple times
var bool                       fFakeZedTime;          // fake zed time, does the same shit as the other one but doesn't trigger skills or extensions
var bool                       fFakeNoSound;          // fake zed time without sounds, only for short ones
var bool                       fShouldSkipTrader;     // if true, we delay a skip trader
var bool                       fBossWaveStarted;      // if true, we started the boss wave
var float                      fSkipTraderDelay;      // amount of delay to skip the trader
var string                     fThrowPlayerMsg;       // message when the trader is skipped with only one player alive
var string                     fThrowMaxMsg;          // message when the trader is skipped when more than one players are alive
var byte                       fZedsSpawnIdx;         // player index we spawn the zeds for
var float                      fZedRespawnTime;       // time in seconds to consider viable a zombievolume after a squad was spawned
var float                      fSpawnMinDistance;     // minimum distance to spawn a normal zed to players
var float                      fSpawnDistanceRand;    // maximum additional random unreal units added to zombie volumes scores to randomize the spawn system a bit
var float                      fBossMinDistance;      // minimum distance to spawn a boss to players
var float                      fSpawnMaxZDistance;    // maximum distance on the z axis to allow a zombievolume to spawn
var int                        FWaveInitZeds;         // minimum amount of zeds spawned to consider the wave initialization done
var float                      FWaveInitSpawnPeriod;  // how many seconds to wait before spawning the next squad during the wave initialization
var float                      FWaveSpawnPeriod;      // how many seconds to wait before spawning the next squad
var float                      FWaveSpawnPeriodSeven; // how many seconds to wait before spawning the next squad with seven or more players
var float                      FWaveSpawnPeriodTen;   // how many seconds to wait before spawning the next squad with ten or more players
var float                      FSkipMessageDelay;     // skip trader message delay in seconds
var bool                       FSkipMessageQueued;    // is the skip trader message queued?
var Pawn                       FZedSpawnTarget;       // player to be targeted by newly spawned zeds
var float                      FDoorClearTime;        // how much time we take between door hack clears, used to prevent doors from messing with the path nodes
var byte                       FDoorClearAttempts;    // how many times we clear trigger the doors hack

// zed time skills array struct
struct fZedTimeSkillUsed
{
    var PlayerController PC;                          // used to identify the player when we can't use SteamID (on disconnect)
    var string           SteamID;                     // used to identify the player
    var bool             SkillUsed;                   // have we used our zed time skill?
    var bool             Camping;                     // are we camping?
    var bool             Disconnected;                // have we disconnected from the server?
    var bool             ShouldRes;                   // should we reset this zed time skill?
    var bool             ExitedThisWave;              // we disconnected this wave, used for the dosh system
    var bool             DiedThisWave;                // we died this wave, used for the perk change system
    var bool             CanChangePerk;               // can this dude change perk?
    var bool             WantsTraderSkip;             // does this dude want a trader skip?
    var bool             fShareStalkers;              // shared stalker visibility status
    var bool             fSpectatorInTrader;          // if true this player became a spectator during the current trader time
    var int              fNoZedHeal;                  // if we don't kill a zed for a set time, increase this number to stop people from farming money while healing
    var int              fNoZedWeld;                  // if we don't kill a zed for a set time, increase this number to stop people from farming money while welding
    var int              zTExtUsed;                   // how many zed time extensions we used
    var int              dWave;                       // disconnect wave (corresponds to WaveNum in trader time and WaveNum + 1 during a wave)
    var int              fDWave;                      // disconnect wave (corresponds to FCurrentWave)
    var int              LastPlayedWave;              // last wave that we played
    var int              Dosh;                        // current dosh
    var int              FPatHit;                     // pat hit index
    var int              fSpawnWave;                  // spawned from spectator this wave
    var int              fKills;                      // stored amount of kills
    var byte             nextWeap;                    // next weapon to equip after syringe is used
    var string fVetName;                              // current veterancy type, used to allow perk change if you died and haven't changed before
};

var transient array<fZedTimeSkillUsed> fZedUsed;

replication
{
    reliable if(Role == ROLE_Authority)
        fZedTimeEnd, FAlivePlayers, FWavePlayers, fChangeCurWave, fFakeZedTime, fFakeNoSound, fBossWaveStarted, fCurrentFlares;
}

event InitGame(string Options, out string Error)
{
    Super.InitGame(Options, Error);

    if (Level.Game.GameDifficulty >= 8.0)       // BB
        StartingCash = FStartingCashBB;

    else if (Level.Game.GameDifficulty >= 7.0)  // HOE
        StartingCash = FStartingCashHOE;

    else if (Level.Game.GameDifficulty >= 5.0)  // suicidal
        StartingCash = FStartingCashSUI;

    else if (Level.Game.GameDifficulty >= 4.0)  // hard
        StartingCash = FStartingCashHRD;

    else                                        // normal
        StartingCash = FStartingCashNRM;
}

// calculate next squad spawn time the falk way
function float CalcNextSquadSpawnTime()
{
    local float result;

    result = FWaveSpawnPeriod;

    if (WaveMonsters < FWaveInitZeds)
        result = FWaveInitSpawnPeriod;

    else if (FAlivePlayers >= 10)
        result = FWaveSpawnPeriodTen;

    else if (FAlivePlayers >= 7)
        result = FWaveSpawnPeriodSeven;

    return result;
}

// fZedUsed stuff
function int ZedTimeIndex(PlayerController PC, optional FalkIdState fState)
{
    local int fI;
    local String fID;

    fID = PC.GetPlayerIDHash();

    //warn("current ID: "@fID);

    // don't store anything if we're spectators except on death and don't store stuff with the default hash
    if ((fState == F_None && fID == fNopeHash) || ((fState == F_None || fState == F_ZedTime) && (PC.Pawn == None || PC.Pawn.Health < 0 || PC.PlayerReplicationInfo.bOnlySpectator)))
        return -1;

    for (fI=0; fI<fZedUsed.length; fI++)
    {
        //log("existing ID: "@fZedUsed[i].SteamID);

        if ((fState == F_Disconnected || fState == F_Died) && fZedUsed[fI].PC == PC) // disconnected or dead
        {
            //log("Disconnected Found");
            return fI;
        }


        else if (fZedUsed[fI].SteamID == fID)
        {
            //warn("Normal Found");
            //log(fID);
            //log(fZedUsed[fI].SteamID);

            if (fZedUsed[fI].PC == none)
            {
                //warn("setting up new playercontroller");
                fZedUsed[fI].PC = PC;
            }

            if (fState == F_ZedTime && fZedUsed[fI].ShouldRes) // zed time start
            {
                fZedUsed[fI].ShouldRes = False;
                fZedUsed[fI].SkillUsed = False;
                fZedUsed[fI].zTExtUsed = 0;
            }

            return fI;
        }
    }

    if (fID == fNopeHash)
        return -1;

    fZedUsed.insert(fI, 1);
    fZedUsed[fI].PC             = PC;
    fZedUsed[fI].SteamID        = fID;
    fZedUsed[fI].Disconnected   = True;
    fZedUsed[fI].fSpawnWave     = -1;
    fZedUsed[fI].LastPlayedWave = -1;
    fZedUsed[fI].CanChangePerk  = True;

    if (Level.Game.GameDifficulty >= 8.0)       // BB
        fZedUsed[fI].Dosh = FStartingCashBB;

    else if (Level.Game.GameDifficulty >= 7.0)  // HOE
        fZedUsed[fI].Dosh = FStartingCashHOE;

    else if (Level.Game.GameDifficulty >= 5.0)  // suicidal
        fZedUsed[fI].Dosh = FStartingCashSUI;

    else if (Level.Game.GameDifficulty >= 4.0)  // hard
        fZedUsed[fI].Dosh = FStartingCashHRD;

    else                                        // normal
        fZedUsed[fI].Dosh = FStartingCashNRM;

    fZedUsed[fI].dWave          = -1;
    fZedUsed[fI].fDWave         = -1;
    fZedUsed[fI].fVetName       = "None";

    if (KFPlayerController(PC) != none && KFPlayerController(PC).SelectedVeterancy != None)
        fZedUsed[fI].fVetName = KFPlayerController(PC).SelectedVeterancy.Default.VeterancyName;

    //log("Adding new player: "@fID@" - ID: "@fI);

    return fI;
}


// reset zed time skills
function ZedTimeEndReset()
{
    local int i;

    //warn("ZedTimeReset");

    for (i=0; i<fZedUsed.length; i++)
        fZedUsed[i].ShouldRes = True;
}


// Return true if we can use our zed time skill
function bool CanUseZedTimeSkill(PlayerController P, float ZTECD, float ZTSCD)
{
    local int i;

    if (InZedTime())
    {
        i = ZedTimeIndex(P);

        if (i > -1                                        &&
                !fZedUsed[i].SkillUsed                         &&
                Level.TimeSeconds >= fZedTimeEnd + ZTECD       &&
                Level.TimeSeconds >= LastZedTimeEvent + ZTSCD)
            return True;
    }

    return false;
}


// We used the zed time skill and set it to True
function ZedTimeSkillUsed(PlayerController P)
{
    local int i;

    i = ZedTimeIndex(P);

    if (i > -1)
        fZedUsed[i].SkillUsed = True;
}


// Set Camping state
simulated function SetCamping(PlayerController P, bool fCamp)
{
    local int i;

    i = ZedTimeIndex(P);

    if (i > -1)
        fZedUsed[i].Camping = fCamp;
}


// return True if the pawn is camping
simulated function bool IsCamping(Pawn P)
{
    local int i;
    local PlayerController PC;

    PC = PlayerController(P.Controller);

    if (PC != none)
    {
        i = ZedTimeIndex(PC);

        if (i > -1)
            return fZedUsed[i].Camping;
    }

    return false;
}


// return True if we're in zedtime (or the perk should still be)
function bool InZedTime()
{
    return (bZEDTimeActive && !fFakeZedTime);
}

// return True if we have a zed time extension and increase zTExtUsed
function bool HasZedTimeExtension(PlayerController P, KFPlayerReplicationInfo KFPRI, class<KFVeterancyTypes> Vet)
{
    local int i;

    i = ZedTimeIndex(P);

    if (i > -1 && Vet.static.ZedTimeExtensions(KFPRI) > fZedUsed[i].zTExtUsed)
    {
        fZedUsed[i].zTExtUsed++;
        //log(i @ fZedUsed[i].zTExtUsed);
        return True;
    }

    return false;
}


// return torch battery drain
function int GetTorchBatteryDrain(KFWeapon fWeapon)
{
    return 10;
}


// return dosh amount for this player or -1 if not found
function int FalkGetDosh(string fID)
{
    local int i;
    local int fDosh;
    local int fDWave;

    if (fID == fNopeHash)
        return -1;

    //log(fID);

    for (i=0; i<fZedUsed.length; i++)
    {
        if (fZedUsed[i].SteamID == fID)
        {
            //warn("Player found");
            fDosh = fZedUsed[i].Dosh;

            //warn("Stored dosh: "@fDosh);

            if (fZedUsed[i].Disconnected) // add some cash if we reconnect on a later wave
            {
                fZedUsed[i].Disconnected = False;

                fDWave = Max(0, fZedUsed[i].dWave);

                //warn("ID:"@fID@"Disconnected dosh:"@fDosh@"- dwave:"@fDWave@"- cwave:"@WaveNum);

                if (WaveNum > fDWave && !fZedUsed[i].ExitedThisWave)
                {
                    fDosh += FalkGetWaveDosh(fDWave);
                    //warn("Additional dosh:"@FalkGetWaveDosh(fDWave));
                }

                fZedUsed[i].ExitedThisWave = False;
            }

            //warn("Total dosh: "@fDosh);
            return Max(0, fDosh);
        }
    }

    //log("Player not found");
    return -1;
}

// set dosh amount for this player
function FalkSetDosh(PlayerController P, int fAmount, optional FalkIdState fState)
{
    local int i;//, FCurW;

    i = ZedTimeIndex(P, fState);

    //log("index: "@i@" - len: "@fZedUsed.Length);

    if (i > -1 && i < fZedUsed.Length)
    {
        // we killed, healed, welded or got dosh from an assist, store it
        /*if (fState == F_Other)
          {
          warn("Idx:"@i@"- got dosh.");
          fZedUsed[i].fGotDosh = true;
          }*/

        // this was intended as a fix for a bug that apparently doesn't exist, keeping it until confirming it really was a ghost
        /*if (fZedUsed[i].LastPlayedWave > -1)
          fAmount = FStartingCash;*/

        //warn("Setting:"@fAmount);

        if (fState == F_Disconnected || fState == F_Died) // we died or disconnected
        {
            //warn("WaveNum: "@WaveNum@"Currently Stored Dosh: "@fZedUsed[i].Dosh@" - Current Amount: "@fAmount);

            if (fState == F_Disconnected || !bWaveInProgress) // we disconnected or died in trader time
            {

                if (!bWaveInProgress)
                    fZedUsed[i].dWave     = WaveNum;

                else
                    fZedUsed[i].dWave     = WaveNum + 1;

                fZedUsed[i].fDWave       = FCurrentWave;

                //warn("Setting"@i@"Disconnect or in trader wave:"@fZedUsed[i].dWave);

                fZedUsed[i].Disconnected = True;

                if (!bWaveInProgress)
                    fZedUsed[i].ExitedThisWave  = True;
            }

            else
            {
                fZedUsed[i].DiedThisWave       = True;

                if (KFPlayerController(fZedUsed[i].PC) != none && KFPlayerController(fZedUsed[i].PC).SelectedVeterancy != None)
                    fZedUsed[i].fVetName = KFPlayerController(fZedUsed[i].PC).SelectedVeterancy.Default.VeterancyName;

                //warn(P@"Died");
            }
        }

        if (fState == F_Other)
        {
            fZedUsed[i].Dosh += Max(0, fAmount);
            //log("Adding: "@fAmount@" to "@i@" dosh, total: "@fZedUsed[i].Dosh@" - Pawn: "@P.Pawn);
        }

        else
        {
            //log("Setting "@i@" Dosh to: "@fAmount@" - Pawn: "@P.Pawn);
            fZedUsed[i].Dosh = Max(0, fAmount);
        }
    }
}


// return additional dosh for a new player based on current wave
function int FalkGetWaveDosh(optional int fStartWave)
{
    local int result;
    local int FUsedWaveAddCash;

    if (Level.Game.GameDifficulty >= 8.0)       // BB
        FUsedWaveAddCash = FWaveAddCashBB;

    else if (Level.Game.GameDifficulty >= 7.0)  // HOE
        FUsedWaveAddCash = FWaveAddCashHOE;

    else if (Level.Game.GameDifficulty >= 5.0)  // suicidal
        FUsedWaveAddCash = FWaveAddCashSUI;

    else if (Level.Game.GameDifficulty >= 4.0)  // hard
        FUsedWaveAddCash = FWaveAddCashHRD;

    else                                        // normal
        FUsedWaveAddCash = FWaveAddCashNRM;

    //warn("WaveNum:"@WaveNum@"- fStartWave:"@fStartWave);

    result = FUsedWaveAddCash * Max(0, WaveNum - fStartWave);

    if (FCurrentWave >= FinalWave)
    {
        if (Level.Game.GameDifficulty >= 8.0)       // BB
            result += FLastWaveAddCashBB;

        else if (Level.Game.GameDifficulty >= 7.0)  // HOE
            result += FLastWaveAddCashHOE;

        else if (Level.Game.GameDifficulty >= 5.0)  // suicidal
            result += FLastWaveAddCashSUI;

        else if (Level.Game.GameDifficulty >= 4.0)  // hard
            result += FLastWaveAddCashHRD;

        else                                        // normal
            result += FLastWaveAddCashNRM;
    }

    return result;
}

// code resides into the falkgametype
function SetUnlocked(PlayerController PC, byte idx, bool uStatus)
{
}

// this is here so I can call this typecasting Level.Game as Falk689GameTypeBase
function int GetAlivePlayers()
{
    return 0;
}

// this is also here so I can call this typecasting Level.Game as Falk689GameTypeBase
function int GetConnectedPlayers()
{
    return 0;
}


// return True if we already spawned this wave
function bool fGetSpawnedThisWave(PlayerController PC)
{
    local int i;

    if (PC != none)
    {
        i = ZedTimeIndex(PC, F_Disconnected);

        if (i > -1)
        {
            //warn("SW: " @ fZedUsed[i].fSpawnWave @ " -> CW: " @ FCurrentWave @ " | FDW: " @ fZedUsed[i].fDWave @ " -> FCW: "@FCurrentWave);
            return (fZedUsed[i].fSpawnWave == FCurrentWave || fZedUsed[i].fDWave >= WaveNum);
        }

        return true;
    }

    return false;
}

// set spectator spawn wave
function fSetSpawnedThisWave(PlayerController PC)
{
    local int i;

    if (PC != none)
    {
        i = ZedTimeIndex(PC);

        if (i > -1)
        {
            fZedUsed[i].fSpawnWave = FCurrentWave;
        }
    }
}

// prevent exploits on our respawn system
function RestartPlayer(Controller aPlayer)
{
    if (PlayerController(aPlayer) != none && !fGetSpawnedThisWave(PlayerController(aPlayer)))
        Super.RestartPlayer(aPlayer);
}


// delay on voteskip and door related fixes
event Tick(float DeltaTime)
{
    local KFDoorMover D;

    if (FDoorClearAttempts > 0 && Level.TimeSeconds > FDoorClearTime)
    {
        FDoorClearAttempts--;
        FDoorClearTime = Level.TimeSeconds + 1.0;

        foreach DynamicActors(class'KFDoorMover', D)
        {
            D.Disable('Tick');
            D.DamageThreshold             = 0;
            D.MaxWeld                     = 460;
            D.ZombieDamageReductionFactor = 1.f;

            if (D.MyTrigger != none)
            {
                D.MyTrigger.CombatSealReduction=1.f;
                D.MyTrigger.MaxWeldStrength = 460;
            }

            if (D.DoorPathNode != none)
                D.DoorPathNode.ExtraCost = D.InitExtraCost;

            D.DoorPathNode = none;
        }

    }

    if (Role == ROLE_Authority && fShouldSkipTrader)
    {
        if (fSkipTraderDelay > 0)
            fSkipTraderDelay -= DeltaTime;

        else
        {
            fSkipTraderDelay  = Default.fSkipTraderDelay;
            fShouldSkipTrader = False;
            FalkSkipTrader();
        }
    }

    if (!FSkipMessageQueued)
        return;

    if (FSkipMessageDelay > 0)
        FSkipMessageDelay -= DeltaTime;

    else
    {
        FSkipMessageQueued = false;
        FSkipMessageDelay  = Default.FSkipMessageDelay;
        SkipTraderMessage();
    }
}

// Vote a trader skip
function bool VoteSkipTrader(PlayerController PC, optional FalkIdState fState)
{
    local int i;

    if (PC != none)
    {
        i = ZedTimeIndex(PC, fState);

        if (i > -1)
        {
            // we already voted, there's no need to check
            if (fZedUsed[i].WantsTraderSkip)
            {
                // just in case this gets called multiple times on disconnect or other spooky shit like that
                if (fState == F_Disconnected)
                    fZedUsed[i].WantsTraderSkip = False;

                return False;
            }

            if (fState != F_Disconnected)
            {
                fZedUsed[i].WantsTraderSkip = True;
                FSkipVoters++;
            }

            CheckSkipTrader(fState);

            return True;
        }
    }

    return False;
}

// check if we can skip the trader already and eventually queue it
function CheckSkipTrader(FalkIdState fState)
{
    if (FSkipVoters >= GetAlivePlayers())
    {
        fShouldSkipTrader = True;

        if (fState == F_Disconnected)
            FSkipMessageQueued = true;
    }
}

// set the exited in trader variable to true
function SetSpectatorInTrader(PlayerController PC)
{
    local int i;

    if (PC != none)
    {
        i = ZedTimeIndex(PC, F_Other);

        if (i > -1)
        {
            //warn("FOUND"@i);
            fZedUsed[i].fSpectatorInTrader = True;
            return;
        }
    }

    //warn("NOT FOUND");
}

// handle players that became spectators during the current trader time
function HandleSpectatorsInTrader()
{
    local int i;

    for (i=0; i<fZedUsed.Length; i++)
    {
        if (fZedUsed[i].fSpectatorInTrader)
        {
            //warn("SPECTATOR IN TRADER"@i);

            fZedUsed[i].fSpectatorInTrader = false;

            if (fZedUsed[i].PC != none)
            {
                if (!VoteSkipTrader(fZedUsed[i].PC, F_Disconnected))
                {
                    //warn("REMOVE VOTE");
                    FSkipVoters--;
                }
            }

            else
            {
                //warn("WEIRD BUT CHECK ANYWAY");
                CheckSkipTrader(F_Disconnected);
            }
        }
    }
}


// tell everyone we've skipped this trader one player disconnect if the conditions are right - implemented in Falk689GameType
function SkipTraderMessage()
{
}

// Skip trader time
exec function FalkSkipTrader()
{
    local int i;

    FSkipVoters = -1;

    if (bTradingDoorsOpen && WaveCountDown > 3)
    {
        WaveCountDown = 3;
        InvasionGameReplicationInfo(GameReplicationInfo).WaveNumber = WaveNum;
    }

    // clear zombievolumes if needed
    if (fZedRespawnTime < 2.f)
        return;

    for (i=0; i<ZedSpawnList.Length; i++)
        ZedSpawnList[i].LastCheckTime = 0;
}

// give everybody a chance to change perk (and reset skip trader voting)
function FalkEnablePerkSelect()
{
    local int i;

    FSkipVoters = 0;

    for (i=0; i<fZedUsed.Length; i++)
    {
        if ((fZedUsed[i].PC != none && fZedUsed[i].PC.Pawn != none) || fZedUsed[i].LastPlayedWave == FCurrentWave - 1)
        {
            //warn(fZedUsed[i].LastPlayedWave @ " " @ FCurrentWave);
            fZedUsed[i].CanChangePerk      = True;
            fZedUsed[i].WantsTraderSkip    = False;
            fZedUsed[i].fSpectatorInTrader = False;
            fZedUsed[i].fNoZedHeal         = 0;
            fZedUsed[i].fNoZedWeld         = 0;
        }
    }
}

// set last played wave and reset died this wave state
function FalkSetLastPlayedWave()
{
    local int i;

    //warn("SetLastPlayedWave");

    for (i=0; i<fZedUsed.length; i++)
    {
        if (fZedUsed[i].ExitedThisWave && fZedUsed[i].LastPlayedWave + 1 < FCurrentWave)
        {
            //warn("RESET ExitedThisWave FOR ID:"@i);
            fZedUsed[i].ExitedThisWave = False;
        }

        if (fZedUsed[i].PC != none && fZedUsed[i].PC.Pawn != none)
            fZedUsed[i].LastPlayedWave = FCurrentWave;

        fZedUsed[i].DiedThisWave   = False;
        //warn(fZedUsed[i].PC@"Reset");
    }
}


// revoke the chance to change perk
function FalkDisablePerkSelect(optional PlayerController PC)
{
    local int i;
    //warn("Disable");
    if (PC != none)
    {
        //warn(PC);
        i = ZedTimeIndex(PC);

        if (i > -1)
            fZedUsed[i].CanChangePerk = False;
    }

    else
    {
        for (i=0; i<fZedUsed.length; i++)
            fZedUsed[i].CanChangePerk = False;
    }

    //warn("Disable STOP");
}


// return true if the specified player is allowed to change perk
function bool CanChangePerk(PlayerController PC)
{
    local int i;
    local string fVName;

    //warn("CanChangePerk");

    if (PC != none)
    {
        fVName = "Falk689";
        i      = ZedTimeIndex(PC);

        if (KFPlayerController(PC) != none)
        {
            // if we had no perk, let us select one regardless
            if (KFPlayerReplicationInfo(PC.PlayerReplicationInfo).ClientVeteranSkill == none)
                return True;

            fVName = KFPlayerController(PC).SelectedVeterancy.Default.VeterancyName;
        }


        if (i > -1)
        {
            //warn("Normal Check:"@fZedUsed[i].CanChangePerk@"Died This Wave:"@fZedUsed[i].DiedThisWave@"Old Vet Name:"@fZedUsed[i].fVetName@"Current Vet Name:"@fVName);

            if (fZedUsed[i].CanChangePerk || (fZedUsed[i].DiedThisWave && fZedUsed[i].fVetName == fVName))
            {
                if (fVName == "Falk689")
                    fVName = "None";

                //if (!fZedUsed[i].CanChangePerk)
                //warn(fZedUsed[i].PC@"New System");

                //warn(fZedUsed[i].PC@"Setting Vet Name to:"@fVName);

                fZedUsed[i].DiedThisWave = False;
                fZedUsed[i].fVetName     = fVName;
                return True;
            }

            return False;
        }

        //warn("Default to True");
        return True;
    }

    //warn("No PlayerController");

    return False;
}

// attempt to remove some log spam
function ShowPathTo(PlayerController P, int TeamNum)
{
    if (KFGameReplicationInfo(GameReplicationInfo).CurrentShop == none)
        return;

    KFGameReplicationInfo(GameReplicationInfo).CurrentShop.InitTeleports();

    if (KFGameReplicationInfo(GameReplicationInfo).CurrentShop.TelList.Length > 0  &&
            KFGameReplicationInfo(GameReplicationInfo).CurrentShop.TelList[0] != None  &&
            P.FindPathToward(KFGameReplicationInfo(GameReplicationInfo).CurrentShop.TelList[0], false) != None)
    {
        Spawn(class'RedWhisp', P,, P.Pawn.Location);
    }
}

// this will return if we can change perk once per downtime
function bool FalkAllowOnePerkChange()
{
    return False;
}

// used to update the unlimited stalker visibility from classes located before our veterancy types - code resides in Falk689GameType
function ExtStalkerViewShareUpdate(PlayerController PC, class<KFVeterancyTypes> KFVet)
{
}

// this should update the zed time unlimited stalker visibility on one player, preparing for the skill to actually be triggered
function StalkerViewShareUpdate(PlayerController PC, bool fSharing)
{
    local int i;

    if (PC != none)
    {
        i = ZedTimeIndex(PC, F_Other);

        if (i > -1)
        {
            //warn("Setting Stalker Sharing to:"@fSharing);
            fZedUsed[i].fShareStalkers = fSharing;
        }
    }
}

// called on death to check and eventually decrease how many share the unlimited shared zed time stalker skill
function DecStalkerViewSharers(PlayerController PC)
{
    local int i;

    if (PC != none)
    {
        i = ZedTimeIndex(PC, F_Other);

        //warn("DECREASE:"@i);

        if (i > -1 && fZedUsed[i].fShareStalkers)
        {
            fSStalkerSkills--;
            //warn("Decreasing counter:"@fSStalkerSkills);
        }
    }
}

// return how many share the unlimited shared zed time stalker skill - code resides in Falk689GameType
function int GetStalkerViewSharers()
{
    return 0;
}


// a flare was spawned, update the counter - code resides in Falk689GameType
function FlareSpawned(PlayerController PC)
{
}

// a flare was destroyed, update the counter - code resides in Falk689GameType
function FlareDestroyed(PlayerController PC)
{
}

// increase the ID value and return it for a new zed
function int FalkGetZedDebugID()
{
    FZedDebugID++;

    return FZedDebugID;
}

// check if we should reward this medic and, eventually, increase its heal count
function bool ShouldRewardMedic(PlayerController PC)
{
    local int i;

    if (fLastKilledZedTimer <= 0)
    {
        if (PC != none)
        {
            i = ZedTimeIndex(PC);

            if (i > -1)
            {
                if (fZedUsed[i].fNoZedHeal >= fMaxNoZedHeal)
                {
                    //warn("Idx:"@i@"False:"@Level.TimeSeconds);
                    return false;
                }

                fZedUsed[i].fNoZedHeal++;
                //warn("Idx:"@i@"Count:"@fZedUsed[i].fNoZedHeal@"Max:"@fMaxNoZedHeal@"True:"@Level.TimeSeconds);
            }
        }
    }

    /*else
      {
      warn(fLastKilledZedTimer@"Seconds of timer left:"@Level.TimeSeconds);
      }


      warn("Return True"@Level.TimeSeconds);*/

    return true;
}

// tweak the weld reward based on how many points we can cash on, increase the count accordingly
function int GetSupportReward(PlayerController PC, int fWeldPoints)
{
    local int i;
    local int reward;

    reward = fWeldPoints;

    if (fLastKilledZedTimer <= 0)
    {
        if (PC != none)
        {
            i = ZedTimeIndex(PC);

            if (i > -1)
            {
                // no reward
                if (fZedUsed[i].fNoZedWeld >= fMaxNoZedWeld)
                {
                    reward = 0;
                }

                // partial reward
                else if (fZedUsed[i].fNoZedWeld + fWeldPoints > fMaxNoZedWeld)
                {
                    reward = fMaxNoZedWeld - fZedUsed[i].fNoZedWeld;
                    fZedUsed[i].fNoZedWeld = fMaxNoZedWeld;
                }

                // total reward
                else
                    fZedUsed[i].fNoZedWeld += fWeldPoints;


                //warn("Idx:"@i@"Reward:"@reward@"Time"@Level.TimeSeconds);
            }
        }
    }

    /*else
      {
      warn(fLastKilledZedTimer@"Seconds of timer left:"@Level.TimeSeconds);
      }

      warn("Return"@reward@"Time:"@Level.TimeSeconds);*/

    return reward;
}

// reset healed and welded points with no zeds kills for everyone
function ResetNoZedVars()
{
    local int i;

    //warn("RESET:"@Level.TimeSeconds);

    for (i=0; i<fZedUsed.length; i++)
    {
        fZedUsed[i].fNoZedHeal = 0;
        fZedUsed[i].fNoZedWeld = 0;
    }

    //warn("ZedTimeReset STOP");
}

// implemented in Falk689GameType
function Controller FGetNewZedSpawnPlayer()
{
    return none;
}

// overhauled spawning system
function ZombieVolume FindSpawningVolume(optional bool bIgnoreFailedSpawnTime, optional bool bBossSpawning)
{
    local ZombieVolume BestZ;
    local float BestScore, tScore;
    local int i;
    local Controller C;

    // no player connected? this probably shouldn't happen
    if (Level.NetMode == NM_DedicatedServer && fZedUsed.Length <= 0)
    {
        //warn("NONE 1");
        return none;
    }

    C = FGetNewZedSpawnPlayer();

    // no alive player left?
    if (C == none)
    {
        //warn("NONE 2");
        return none;
    }

    if (C.Pawn != none)
        FZedSpawnTarget = C.Pawn;

    // get the best spawn point by distance
    for (i=0; i<ZedSpawnList.Length; i++)
    {
        tScore = RateZombieVolume(ZedSpawnList[i], C, bIgnoreFailedSpawnTime, bBossSpawning);

        if (tScore < 0)
            continue;

        if (BestZ == None || tScore < BestScore)
        {
            BestScore = tScore;
            BestZ     = ZedSpawnList[i];
        }
    }

    if (BestZ == none)
    {
        //warn("NONE 3");
        return none;
    }

    BestZ.LastCheckTime = Level.TimeSeconds; // start the spawn cooldown

    //warn("BestZ:"@BestZ@"- Score:"@BestScore);
    return BestZ;
}

// return true if we can spawn zeds in a specific zombie volume
function bool CanSpawnInZombieVolume(array< class<KFMonster> > zombies, ZombieVolume ZV, optional bool bBossSpawning)
{
    local array< class<KFMonster> > SpawnedZeds;

    if (ZV.bObjectiveModeOnly)
        return false;

    if (!ZV.bVolumeIsEnabled)
    {
        if (ZV.bDebugZombieSpawning)
            warn("Nope, volume disabled");

        return false;
    }

    if (ZV.SpawnPos.Length == 0)
    {
        if (ZV.bDebugZombieSpawning)
            warn("Nope, no spawn points");

        return false;
    }

    // using LastCheckTime as a spawn cooldown
    if (!bBossSpawning && ZV.LastCheckTime + fZedRespawnTime >= Level.TimeSeconds)
    {
        if (ZV.bDebugZombieSpawning)
            warn("Nope, cooldown time:"@Level.TimeSeconds@"LastCheck"@ZV.LastCheckTime + fZedRespawnTime);

        return false;
    }

    return SpawnInZombieVolume(ZV, zombies, SpawnedZeds, true);
}


// implemented in Falk689GameType
function float RateZombieVolume(ZombieVolume ZV, Controller SpawnCloseTo, bool bIgnoreFailedSpawnTime, bool bBossSpawning)
{
    return -1.f;
}

// edited zombievolume code - implemented in Falk689GameType
function bool SpawnInZombieVolume(ZombieVolume ZV, out array< class<KFMonster> > zombies, out array< class<KFMonster> > SpawnedZeds, optional bool test, optional out int numspawned, optional int ZombiesAtOnceLeft, optional out int TotalZombiesValue, optional int NumTries, optional bool bBossSpawning)
{
    return false;
}

// add a kill to the player list
simulated function AddKill(PlayerController P)
{
    local int i;

    i = ZedTimeIndex(P);

    if (i > -1)
        fZedUsed[i].fKills++;
}

// return amount of kills for this player
simulated function int GetKills(PlayerController P)
{
    local int i;

    i = ZedTimeIndex(P);

    if (i > -1)
        return fZedUsed[i].fKills;

    return 0;
}

defaultproperties
{
    FLastWaveBonusNRM=500
    FLastWaveBonusHRD=400
    FLastWaveBonusSUI=300
    FLastWaveBonusHOE=200
    FLastWaveBonusBB=100
    FWaveBonusNRM=250
    FWaveBonusHRD=200
    FWaveBonusSUI=150
    FWaveBonusHOE=100
    FWaveBonusBB=50
    FStartingCashNRM=500
    FStartingCashHRD=400
    FStartingCashSUI=300
    FStartingCashHOE=200
    FStartingCashBB=100
    FWaveAddCashNRM=150
    FWaveAddCashHRD=120
    FWaveAddCashSUI=90
    FWaveAddCashHOE=60
    FWaveAddCashBB=30
    FLastWaveAddCashNRM=150
    FLastWaveAddCashHRD=120
    FLastWaveAddCashSUI=90
    FLastWaveAddCashHOE=60
    FLastWaveAddCashBB=30
    fLastKilledZedTimer=30.0
    fMaxNoZedHeal=10
    fMaxNoZedWeld=50
    fSkipTraderDelay=1.5
    fZedRespawnTime=3.0           // how many seconds before we can use the same spawn volume again
    fSpawnMinDistance=600         // 12 meters
    fSpawnDistanceRand=300        // 6 meters
    fBossMinDistance=750          // 15 meters
    fSpawnMaxZDistance=400        // 8 meters
    FWaveInitZeds=16
    FWaveInitSpawnPeriod=2.0
    FWaveSpawnPeriod=0.5          // set all of them to 0 to make it more hardcore cause im a cunt with attention deficit issues that can't wait a couple seconds for zombies to spawn..... oh wait that's Snake - in 2024 we set this higher cause we reworked the spawn system but i remain a cunt with attention deficit issues
    FWaveSpawnPeriodSeven=0.5
    FWaveSpawnPeriodTen=0.5
    FSkipMessageDelay=0.1
    FZSpawnTries=6
    MapPrefix="KFL"
    Acronym="KFL"
    MapListType="Falk689GameTypeBase.FMapList"
    GameName="Lair Base Structure"
    Description="Base structure to make 'Lair Single Player' work. Do not select this dud if you're looking to host a game for yourself, choose the one mentioned."
    ScreenShotName="LairTextures_T.Thumbnails.DONOTSELECTME"
    FDoorClearAttempts=5
}
