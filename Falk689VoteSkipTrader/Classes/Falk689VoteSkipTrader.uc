class Falk689VoteSkipTrader extends Mutator;

// Commands
var string VoteSkipCmd;             // register your vote to skip the trader

// Messages
var string VoteMessage;             // message to show when you registered a vote
var string SingleSkipMessage;       // message when the trader is skipped with only one player alive
var string MultiSkipMessage;        // message when the trader is skipped when more than one players are alive

// Errors
var string VoteError;               // error to show when there's no trader to skip
var string DoubleError;             // error to show when we already voted
var string SpectError;              // error to show when we try to vote as spectator

// Colors
struct ColorRecord
{
    var config string ColorName;      // Color name, for comfort
    var config string ColorTag;       // Color tag
    var config Color Color;           // RGBA values
};

var() array<ColorRecord> ColorList; // Color list

// implement vote skip trader
function Mutate(string command, PlayerController Sender)
{
    local Falk689GameTypeBase Flk;
    local string fPlayerName;
    local string fPlayerColor;
    local KFPlayerReplicationInfo KFPRI;
    local int lAlivePlayers;
    local int lConnectedPlayers;

    if (command ~= VoteSkipCmd)
    {
        Flk = Falk689GameTypeBase(Level.Game);

        if (Flk != none)
        {
            // if we're not in trader time, just say nope
            if (Flk.bWaveInProgress || Flk.bWaitingToStartMatch || Flk.FCurrentWave <= 0 || Flk.fBossWaveStarted || Flk.IsInState('GameEnded'))
            {
                Sender.ClientMessage(VoteError);
                return;
            }

            // first check if we have a pawn, if we don't just tell us nope
            if (Sender.Pawn == none)
            {
                Sender.ClientMessage(SpectError);
                return;
            }

            // else ask the gametype if we already voted/revoked, if we haven't, notify the other players
            else if (Flk.VoteSkipTrader(Sender))
            {
                KFPRI        = KFPlayerReplicationInfo(Sender.PlayerReplicationInfo);
                fPlayerColor = "%r";

                // select an appropriate color for our veterancy level
                if (KFPRI != none)
                {
                    if (KFPRI.ClientVeteranSkillLevel >= 16)
                        fPlayerColor = "%p";

                    else if (KFPRI.ClientVeteranSkillLevel >= 11)
                        fPlayerColor = "%v";

                    else if (KFPRI.ClientVeteranSkillLevel >= 6)
                        fPlayerColor = "%g";
                }

                fPlayerName = fPlayerColor $ Sender.PlayerReplicationInfo.PlayerName;
                SetColor(fPlayerName);

                lAlivePlayers     = Flk.GetAlivePlayers();
                lConnectedPlayers = Flk.GetConnectedPlayers();

                // there are several players alive, say who and how many have voted
                if (lConnectedPlayers > 1)
                {
                    if (Flk.FSkipVoters < 0)
                    {
                        ServerMessage(fPlayerName@VoteMessage@"-"@lAlivePlayers$"/"$lAlivePlayers@"votes cast");
                        ServerMessage(MultiSkipMessage);
                    }

                    else if (Flk.FSkipVoters >= lAlivePlayers)
                    {
                        ServerMessage(fPlayerName@VoteMessage@"-"@Flk.FSkipVoters$"/"$lAlivePlayers@"votes cast");
                        ServerMessage(MultiSkipMessage);
                    }


                    else
                        ServerMessage(fPlayerName@VoteMessage@"-"@Flk.FSkipVoters$"/"$lAlivePlayers@"votes cast");
                }

                // there's only one player alive, just say we've skipped this trader
                else
                    ServerMessage(SingleSkipMessage);

                return;
            }

            // if we have already voted, just tell us to chill
            else
            {
                Sender.ClientMessage(DoubleError);
                return;
            }
        }
    }

    if (NextMutator != None)
        NextMutator.Mutate(command, Sender);
}

// Send a message to everyone
function ServerMessage(string Msg)
{
    local Controller C;
    local PlayerController PC;

    SetColor(Msg);

    for (C = Level.ControllerList; C != none; C = C.nextController)
    {
        PC = PlayerController(C);

        if (PC != none)
            PC.ClientMessage(Msg);
    }
}

// START - Apparently, someone named NikC wrote this code. Hi NikC, thanks NikC. I would kinda suck your... DikC. (ba dum tss) - Falk689 //

// Apply Color Tags To Message
function SetColor(out string Msg)
{
    local int i;
    for(i=0; i<ColorList.Length; i++)
    {
        if(ColorList[i].ColorTag!="" && InStr(Msg, ColorList[i].ColorTag)!=-1)
        {
            ReplaceText(Msg, ColorList[i].ColorTag, FormatTagToColorCode(ColorList[i].ColorTag, ColorList[i].Color));
        }
    }
}

// Format Color Tag to ColorCode
function string FormatTagToColorCode(string Tag, Color Clr)
{
    Tag=Class'GameInfo'.Static.MakeColorCode(Clr);
    Return Tag;
}

function string RemoveColor(string S)
{
    local int P;
    P=InStr(S,Chr(27));
    While(P>=0)
    {
        S=Left(S,P)$Mid(S,P+4);
        P=InStr(S,Chr(27));
    }
    Return S;
}

// END - Thanks again, NikC (and Vel-San... wink wink.) //

defaultproperties
{
    GroupName="KF-SkipTrader"
    FriendlyName="Lair - Vote Skip Trader"
    Description="Adds skip trader voting"
    bAddToServerPackages=true
    RemoteRole=ROLE_SimulatedProxy
    bAlwaysRelevant=true
    bNetNotify=true

    VoteSkipCmd="voteskip"

    VoteMessage="%wvoted to skip this trader"
    SingleSkipMessage="You've voted to skip this trader"
    MultiSkipMessage="All players have voted to skip this trader"

    VoteError="There's no trader to skip at this time"
    DoubleError="You've already voted to skip this trader"
    SpectError="You can't vote to skip trading time as a spectator"

    ColorList(0)=(ColorName="Red",ColorTag="%r",Color=(B=65,G=65,R=255,A=0))
    ColorList(1)=(ColorName="Gold",ColorTag="%g",Color=(B=75,G=200,R=250,A=0))
    ColorList(2)=(ColorName="Green",ColorTag="%v",Color=(B=,G=205,R=0,A=0))
    ColorList(3)=(ColorName="Purple",ColorTag="%p",Color=(B=140,G=62,R=69,A=0))
    ColorList(4)=(ColorName="White",ColorTag="%w",Color=(B=255,G=255,R=255,A=0))
}
