/**
 * Creates a TCP link with the remote database to store achievement data on another machine
 * @author etsai (Scary Ghost)
 */
class ServerTcpLink extends TcpLink;

var string separator, bodySeparator, header, protocol, hostname;
var int version, reqId;
var IpAddr serverAddr;
var bool isOpen;
var bool fOpening;

replication
{
   reliable if(Role == ROLE_Authority)
      isOpen, fOpening;
}

enum StatusCode {
   OK,
   INVALID_PASSWORD,
   ERROR_SAVING,
   ERROR_RETRIEVING
};

struct PendingResponse {
   var int reqId;
   var string request;
   var string steamid64;
   var AchievementPack achvObj;
};

var array<PendingResponse> pendingResponses;

struct fPendingRequests {
   var string request;
   var string body;
   var string steamid64;
   var AchievementPack achvObj;
};

var array<fPendingRequests> pendingRequestsList;


state WaitingState
{
   // just append the request to the list while waiting for the connection to happen
   function sendRequest(string request, string body, optional string steamid64, optional AchievementPack achvObj) {
      //local int i;

      // send connection requests normally
      if (request == "connect")
      {
         global.sendRequest(request, body);
         return;
      }

      // don't append the same request twice --- maybe shit works better without this
      /*while (i < pendingRequestsList.Length)
      {
         if (pendingRequestsList[i].request == request && pendingRequestsList[i].body == body)
            return;

         i++;
      }*/

      pendingRequestsList.Length = pendingRequestsList.Length + 1;
      pendingRequestsList[pendingRequestsList.Length - 1].request   = request;
      pendingRequestsList[pendingRequestsList.Length - 1].body      = body;
      pendingRequestsList[pendingRequestsList.Length - 1].steamid64 = steamid64;
      pendingRequestsList[pendingRequestsList.Length - 1].achvObj   = achvObj;
   }

   // process the requests queue
   function EndState() {
      warn("SEND PENDING REQUESTS");

      if (LinkState == STATE_Connected) {
         while (pendingRequestsList.Length > 0) {
            global.sendRequest(pendingRequestsList[0].request, pendingRequestsList[0].body, pendingRequestsList[0].steamid64, pendingRequestsList[0].achvObj);
            pendingRequestsList.Remove(0, 1);
         }
      }
   }


Begin:
   while (LinkState == STATE_Connecting)
   {
      // print the link state while waiting to enstablish the connection, the corresponding enum should be documented here -> https://wiki.beyondunreal.com/UE2:TcpLink_(UT2004)
      warn("Connecting...");
      Sleep(0.02f);
   }

   GoToState('');
}

function sendRequest(string request, string body, optional string steamid64, optional AchievementPack achvObj) {
   if (!fOpening) { 
      warn("BINDING PORT");
      fOpening = true;

      if (LinkState == STATE_Initialized)
         BindPort();

      OpenNoSteam(serverAddr);

      pendingRequestsList.Length = pendingRequestsList.Length + 1;
      pendingRequestsList[pendingRequestsList.Length - 1].request   = request;
      pendingRequestsList[pendingRequestsList.Length - 1].body      = body;
      pendingRequestsList[pendingRequestsList.Length - 1].steamid64 = steamid64;
      pendingRequestsList[pendingRequestsList.Length - 1].achvObj   = achvObj;

      GoToState('WaitingState');
      return;
   }

   warn("LinkState:"@LinkState@"- request:"@request@"- body:"@body@"- reqId:"@reqId);

   pendingResponses.Length                                 = pendingResponses.Length + 1;
   pendingResponses[pendingResponses.Length - 1].reqId     = reqId;
   pendingResponses[pendingResponses.Length - 1].request   = request;
   pendingResponses[pendingResponses.Length - 1].steamid64 = steamid64;
   pendingResponses[pendingResponses.Length - 1].achvObj   = achvObj;

   SendText(header $ separator $ reqId $ separator $ request $ separator $ body);
   reqId++;
}

function PostBeginPlay() {
   LinkMode= MODE_Line;
   ReceiveMode= RMODE_Event;
   BindPort();
   Resolve(class'SAMutator'.default.hostname);
   header= protocol $ "," $ version $ ",request";
}

event Resolved(IpAddr addr) {
   serverAddr= addr;
   serverAddr.port= class'SAMutator'.default.tcpPort;

   if (!OpenNoSteam(serverAddr)) {
      log("Cannot reach remote host"@IpAddrToString(serverAddr));
   }
}

event Opened() {
   isOpen = true;
   fOpening = true;
   sendRequest("connect", class'SAMutator'.default.serverPassword);
}

event Closed() {
   super.Closed();
   isOpen = false;
   fOpening = false;
   log("Connection to remote database closed");
}

event ReceivedLine(string Line) {
   local AchievementDataObject dataObj;
   local array<string> parts, respHeader;
   local int i, respId;

   log("Response:"@Line);
   Split(Line, separator, parts);
   Split(parts[0], ",", respHeader);

   if (respHeader.Length >= 3 && respHeader[0] == protocol && int(respHeader[1]) == version) {
      if (respHeader[2] == "response") {
         respId= int(parts[1]);
         for(i= 0; i < pendingResponses.Length && pendingResponses[i].reqId != respId; i++) {
         }

         if (i < pendingResponses.Length) {
            switch (int(parts[2])) {
               case StatusCode.OK:
                  if (pendingResponses[i].request == "retrieve") {
                     if (parts.Length >= 4) {
                        pendingResponses[i].achvObj.deserializeUserData(parts[3]);
                     } else {
                        dataObj= new(None, pendingResponses[i].steamid64) class'AchievementDataObject';
                        pendingResponses[i].achvObj.deserializeUserData(dataObj.getSerializedData(pendingResponses[i].achvObj.getPackName()));
                     }
                  }
                  break;
               case StatusCode.INVALID_PASSWORD:
                  log("Invalid password!");
                  break;
               case StatusCode.ERROR_SAVING:
                  log("Error saving achievement data");
                  break;
               case StatusCode.ERROR_RETRIEVING:
                  log("Error retrieving achievement data");
                  break;
               default:
                  log("Unrecognized status code="@parts[2]);
            }
         }
         pendingResponses.remove(i, 1);
      }
   }
}

function getAchievementData(string steamid64, AchievementPack achvObj) {
   sendRequest("retrieve", steamid64 $ bodySeparator $ achvObj.getPackName(), steamid64, achvObj);
   //pendingResponses[pendingResponses.Length - 1].steamid64= steamid64;
   //pendingResponses[pendingResponses.Length - 1].achvObj= achvObj;
}

function saveAchievementData(string steamid64, string packName, string data) {
   local string body;

   body = steamid64 $ bodySeparator $ packName;
   if (Len(data) > 0) {
      body$= bodySeparator $ data;
      sendRequest("save",  body);
   }
}


defaultproperties {
   separator= "|"
      bodySeparator= "."
      protocol= "server-achievements"
      version= 1
}
