class VeterancyTypesFalk extends Falk689ServerPerks.FVeterancyTypes
   abstract;

// Custom damage fix
static function int ReduceDamage(KFPlayerReplicationInfo KFPRI, KFPawn Injured, Pawn Instigator, int InDamage, class<DamageType> DmgType)
{
   if (class<DamTypeFreezerGun>(DmgType) != none || class<DamTypeAcidClusterFalk>(DmgType) != none)
      return 0;

   return InDamage;
}

// Add NadeFalk
static function class<Grenade> GetNadeType(KFPlayerReplicationInfo KFPRI)
{
   return class'NadeFalk';
}

// HuskGun value fix
static function float GetCostScaling(KFPlayerReplicationInfo KFPRI, class<Pickup> Item)
{
   local float result;

   if (class<HuskGunPickup>(Item) != none)
      return Default.huskDiv;

   result = 1.0;

   if (KFPRI.ClientVeteranSkillLevel >= 20)
      result = 0.6;

   else if (KFPRI.ClientVeteranSkillLevel >= 15)
      result = 0.7;

   else if (KFPRI.ClientVeteranSkillLevel >= 10)
      result = 0.8;

   else if (KFPRI.ClientVeteranSkillLevel >= 5)
      result = 0.9;

   if (Item == class'SyringePickupFalk'      ||
       Item == class'ArmorWelderPickupFalk'  ||
       Item == class'FVestPickup'            ||
       Item == class'Vest'                   ||
       Item == class'FlaresPickupFalk'       ||
       Item == class'SparkGunPickupFalk'     ||
       Item == class'ZEDGunPickupFalk'       ||
       Item == class'ZEDMKIIPickupFalk'      ||
       Item == class'FreezerPickup')
          return result;


   return 1.0;
}

// bloat bloodbath skill (aka bloatbath skill), vomit deals more damage to kevlars
static function float FGetBodyArmorDamageModifier(KFPlayerReplicationInfo KFPRI, class<DamageType> damageType)
{
   if (KFPRI.Level.Game.GameDifficulty >= 8.0 && class<DamTypeBloatVomitFalk>(damageType) != none)
      return 2.0;

   return static.GetBodyArmorDamageModifier(KFPRI);
}

defaultproperties
{
}
