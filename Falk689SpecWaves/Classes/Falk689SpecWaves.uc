class Falk689SpecWaves extends Mutator;

var float TouchDisableTimeOverride;

function PostBeginPlay()
{
    SetTimer(0.1, false);
}

// replace pickups with falk pickups
function bool CheckReplacement(Actor Other, out byte bSuperRelevant)
{
    local int i;
    local byte disallowed, allowed;
    local array< class<KFMonster> > editedAllowedZeds;

    local KFGameType KF;

    KF  = KFGameType(Level.Game);

    // random pickups replacement
    if (Other.class == class'KFRandomItemSpawn')
    {
        //log("Random item spawn replaced");
        ReplaceWith(Other, "Falk689SpecWaves.Falk689RandomItemSpawn");
        return false;
    }

    // ammo packs replacement
    if (Other.class == class'KFAmmoPickup')
    {
        //log("Ammo pack replaced");
        ReplaceWith(Other, "Falk689WeaponsFix.FalkAmmoPickup");
        return false;
    }

    // replace our classes in zombievolumes and clear any other onlyAllowedZeds configuration since it could fuck up with our volume rating code
    else if (ZombieVolume(Other) != none)
    {
        //log("ZombieVolume");

        ZombieVolume(Other).TouchDisableTime = TouchDisableTimeOverride;

        for (i=0; i<ZombieVolume(Other).DisallowedZeds.Length; i++)
        {
            if (ClassIsChildOf(ZombieVolume(Other).DisallowedZeds[i], class'ZombieBossBase'))
            {
                //warn("NO BOSS NO SPARTY");
                disallowed++;
                ZombieVolume(Other).DisallowedZeds[i] = class'FalkZombieBoss';
            }
        }

        for (i=0; i<ZombieVolume(Other).OnlyAllowedZeds.Length; i++)
        {
            if (ClassIsChildOf(ZombieVolume(Other).OnlyAllowedZeds[i], class'ZombieCrawlerBase'))
            {
                //log("J CRAWLIN'");
                allowed++;
                editedAllowedZeds[editedAllowedZeds.Length] = class'FalkZombieCrawler';
            }

            else if (ClassIsChildOf(ZombieVolume(Other).OnlyAllowedZeds[i], class'ZombieStalkerBase'))
            {
                //log("F.A.L.K.E.R.");
                allowed++;
                editedAllowedZeds[editedAllowedZeds.Length] = class'FalkZombieStalker';
            }

            else if (ClassIsChildOf(ZombieVolume(Other).OnlyAllowedZeds[i], class'ZombieBossBase'))
            {
                //log("LIKE A BOSS");
                allowed++;
                editedAllowedZeds[editedAllowedZeds.Length] = class'FalkZombieBoss';
            }
        }

        if (allowed > 0)
        {
//             warn("ALLOWED");
//
//             for (i=0; i<editedAllowedZeds.Length; i++)
//                 warn(editedAllowedZeds[i]);
//
//             warn(" ");

            ZombieVolume(Other).OnlyAllowedZeds = editedAllowedZeds;
        }

        if (disallowed > 0 || allowed > 0)
            return false;

        return true;
    }


    return true;
}

function Timer()
{
    Destroy(); // Needed for webadmin fix
}

defaultproperties
{
    GroupName="KFFalk689Mut"
    FriendlyName="Lair - Special Squads"
    Description="Sends custom specimen waves when the Patriarch heals. Inspired by the work of 'FluX', 'King Sumo' and 'max-tweak'."
    bAddToServerPackages=true
    TouchDisableTimeOverride=1.1 // this means 2.5 seconds since 1.4 seconds are added in the Falk689GameTypeBase code
}
