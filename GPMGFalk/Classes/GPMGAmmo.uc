class GPMGAmmo extends KFAmmunition;

#EXEC OBJ LOAD FILE=InterfaceContent.utx

defaultproperties
{
   AmmoPickupAmount=32 // one quarter of magazine, heavy weapons standard
   MaxAmmo=375
   InitialAmount=125
   PickupClass=Class'GPMGFalk.GPMGAmmoPickup'
   IconMaterial=Texture'KillingFloorHUD.Generic.HUD'
   IconCoords=(X1=413,Y1=82,X2=457,Y2=125)
   ItemName="Light Machine Gun Belts"
}