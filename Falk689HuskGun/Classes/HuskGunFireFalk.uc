class HuskGunFireFalk extends KFMod.HuskGunFire;

var byte BID;

// modified stuff
function class<Projectile> GetDesiredProjectileClass()
{
   if (HoldTime < (MaxChargeTime * 0.33))
      return WeakProjectileClass;

   else if (HoldTime < MaxChargeTime)
      return default.ProjectileClass;

   else
      return StrongProjectileClass;
}

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   local class<HuskGunProjectileFalk> fP;

   BID++;

   if (BID > 200)
      BID = 1;

   fP = class<HuskGunProjectileFalk>(ProjectileClass);


   if (fP != none) 
   {
      if(HoldTime < MaxChargeTime)
      {
         class<HuskGunProjectileFalk_Weak>(WeakProjectileClass).default.Damage       = fP.Default.fDDamage       * (1.0 + (HoldTime / (MaxChargeTime / 2.0)));
         class<HuskGunProjectileFalk_Weak>(WeakProjectileClass).default.DamageRadius = fP.Default.fDDamageRadius * (1.0 + (HoldTime / MaxChargeTime));
         class<HuskGunProjectileFalk>(ProjectileClass).default.Damage                = fP.Default.fDDamage       * (1.0 + (HoldTime / (MaxChargeTime / 2.0)));
         class<HuskGunProjectileFalk>(ProjectileClass).default.DamageRadius          = fP.Default.fDDamageRadius * (1.0 + (HoldTime / MaxChargeTime)); 
      }

      else
      {
         class<HuskGunProjectileFalk_Strong>(StrongProjectileClass).default.Damage       = fP.Default.fDDamage       * 3.0;
         class<HuskGunProjectileFalk_Strong>(StrongProjectileClass).default.DamageRadius = fP.Default.fDDamageRadius * 2.0;
      }
   }

   class<HuskGunProjectileFalk_Weak>(WeakProjectileClass).default.BulletID     = BID;
   class<HuskGunProjectileFalk_Strong>(StrongProjectileClass).default.BulletID = BID;
   class<HuskGunProjectileFalk>(ProjectileClass).default.BulletID              = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// set the var on the pawn so it will know we're charging the husk gun
function ModeHoldFire()
{
   local int    fDir;
   local vector fSetVector;

   fDir       = 1;
   fSetVector = vect(689, 689, 689);

   if (Instigator != none)
      Instigator.PerformDodge(Instigator.eDoubleClickDir.DCLICK_None, fSetVector, fSetVector);

   Super.ModeHoldFire();
}

// this is bugged as hell, just ignore it, also reset the var on the pawn
function PostSpawnProjectile(Projectile P)
{
   local int    fDir;
   local vector fSetVector;

   fDir       = 0;
   fSetVector = vect(-689, -689, -689);

   Super(KFShotgunFire).PostSpawnProjectile(P);

   if (Instigator != none)
      Instigator.PerformDodge(Instigator.eDoubleClickDir.DCLICK_None, fSetVector, fSetVector);
}

defaultproperties
{
   WeakProjectileClass=Class'HuskGunProjectileFalk_Weak'
   StrongProjectileClass=Class'HuskGunProjectileFalk_Strong'
   ProjectileClass=Class'HuskGunProjectileFalk'
   FireRate=0.400000
}
