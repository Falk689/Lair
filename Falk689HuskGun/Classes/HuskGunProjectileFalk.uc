class HuskGunProjectileFalk extends KFMod.HuskGunProjectile;

var byte  BulletID;            // bullet ID, passed at takedamage to prevent multiple shots
var Actor FLastDamaged;        // last damaged zed, used to attempt to fix multiple shots on the same zed
var float fDDamage;            // default damage, used to edit stuff before spawning the projectile
var float fDDamageRadius;      // default damage radius, used to edit stuff before spawning the projectile
var float fDamageReduction;    // multiplier used to reduce the AOE damage amount
var Controller fCInstigator;   // instigator controller, I don't use vanilla InstigatorController 'cause of reasons

replication
{
   reliable if(Role == ROLE_Authority)
      BulletID, FLastDamaged, fCInstigator, FClientSetLastDamaged;
}

// Post death kill fix 
simulated function PostBeginPlay()
{
   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
      fCInstigator = Instigator.Controller;

   Super.PostBeginPlay();
}

// basically just explode, don't hurt stuff on touch 
simulated function ProcessTouch(Actor Other, vector HitLocation)
{
   if (Other == none || Other == Instigator || Other.Base == Instigator || Other == FLastDamaged || !Other.bBlockHitPointTraces || KFHumanPawn(Other) != none || KFHumanPawn(Other.Base) != none)
      return;

   FLastDamaged = Other;
   FClientSetLastDamaged(Other);

   Explode(HitLocation,Normal(HitLocation-Other.Location));
}

// replicated function to force FLastDamaged on client
simulated function FClientSetLastDamaged(Actor Other)
{
   FLastDamaged = Other;
}

// use bulletID to hit zeds on explosion, mostly copied from the Nade class
simulated function HurtRadius( float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation )
{
   local actor Victims;
   local float damageScale, dist;
   local vector fDir;
   local int NumKilled;
   local KFMonster KFMonsterVictim;
   local Pawn P;
   local KFPawn KFP;
   local array<Pawn> CheckedPawns;
   local int i;
   local bool bAlreadyChecked;

   if (bHurtEntry)
      return;

   bHurtEntry = true;

   foreach CollidingActors (class 'Actor', Victims, DamageRadius, HitLocation)
   {
      // don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
      if( (Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo')
            && ExtendedZCollision(Victims)==None )
      {
         if (((Instigator==None || Instigator.Health <= 0) && KFPawn(Victims) != None) || KFHumanPawn(Victims) != None || Victims == Instigator)
            Continue;

         fDir = Victims.Location - HitLocation;
         dist = FMax(1,VSize(fDir));
         fDir = fDir/dist;
         damageScale = 1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius);

         if (Instigator == None || Instigator.Controller == None)
            Victims.SetDelayedDamageInstigatorController(InstigatorController);

         P = Pawn(Victims);

         if (P != none)
         {
            for (i = 0; i < CheckedPawns.Length; i++)
            {
               if (CheckedPawns[i] == P)
               {
                  bAlreadyChecked = true;
                  break;
               }
            }

            if (bAlreadyChecked)
            {
               bAlreadyChecked = false;
               P = none;
               continue;
            }

            KFMonsterVictim = KFMonster(Victims);

            if (KFMonsterVictim != none && KFMonsterVictim.Health <= 0)
               KFMonsterVictim = none;

            KFP = KFPawn(Victims);

            if(KFMonsterVictim != none)
               damageScale *= KFMonsterVictim.GetExposureTo(Location + 15 * -Normal(PhysicsVolume.Gravity));

            else if( KFP != none )
               damageScale *= KFP.GetExposureTo(Location + 15 * -Normal(PhysicsVolume.Gravity));

            CheckedPawns[CheckedPawns.Length] = P;

            if (damageScale <= 0)
            {
               P = none;
               continue;
            }

            else
               P = none;
         }

         Victims.TakeDamage(damageScale * DamageAmount,Instigator,Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius)
               * fDir,(damageScale * Momentum * fDir), DamageType, BulletID);

         if (Role == ROLE_Authority && KFMonsterVictim != none && KFMonsterVictim.Health <= 0)
            NumKilled++;
      }
   }

   bHurtEntry = false;
}

// don't explode / pop from sirens
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex){}

defaultproperties
{
   MyDamageType=Class'DamTypeHuskGunFalk'
   HeadShotDamageMult=1.100000
   Speed=4500.000000
   MaxSpeed=4500.000000
   Damage=35.000000
   fDDamage=35.000000
   DamageRadius=90.000000
   fDDamageRadius=90.000000
   ExplosionDecal=Class'BurnMarkMediumFalk'
   // fDamageReduction=0.5
}
