class FBrutesKills extends SRCustomProgressInt;

function NotifyPlayerKill(Pawn Killed, class<DamageType> damageType)
{  
   if (FalkZombieBrute(Killed)                                 != None &&
       (class<DamTypeFrag>(damageType)                         != None ||
        class<DamTypePipeBomb>(damageType)                     != None ||
        class<DamTypeCLGLGrenadeFalk>(damageType)              != None ||
        class<DamTypeM79Grenade>(damageType)                   != None ||
        class<DamTypeM32Grenade>(damageType)                   != None ||
        class<DamTypeM203GrenadeFalk>(damageType)              != None ||
        class<DamTypeSPGrenade>(damageType)                    != None ||
        class<DamTypeSealSquealExplosion>(damageType)          != None ||
        class<DamTypeSeekerSixRocketFalk>(damageType)          != None ||
        class<DamTypeSeekerRocketImpactFalk>(damageType)       != None ||
        class<DamTypeLAWFalk>(damageType)                      != None ||
        class<DamTypeRocketImpactFalk>(damageType)             != None ||
        class<DamTypeGrenadeImpactFalk>(damageType)            != None || 
        class<DamTypeLawRocketImpactFalk>(damageType)          != None ||
        class<DamTypeTrigunFalk>(damageType)                   != None))
   {
      IncrementProgress(1);
   }
}

defaultproperties 
{
   ProgressName = "Custom brutes kills"
}
