class FMonsterController extends KFMonsterController;

var Controller  fLastDamagedByController;
var float       fLastSpeedChange;
var float       fSpeedChangeCD;
var Pawn        fStoredEnemy;

replication
{
    reliable if (Role==ROLE_Authority)
        fLastDamagedByController, fLastSpeedChange, fStoredEnemy;
}

// just assume a check every 10 frames
function SetCombatTimer()
{
    SetTimer(0.16, True);
}

// dirty fix to prevent weird stuff from happening on unfreeze
function Tick(float DeltaTime)
{
    //local KFDoorMover D, newTargetDoor;
    //local float eDist, dDist, tDist;

    Super.Tick(DeltaTime);

    if (FalkMonster(Pawn) != none && !FalkMonster(Pawn).fFrozen && Enemy == None && fStoredEnemy != None)
    {
        Enemy = fStoredEnemy;
        fStoredEnemy = None;
    }

    if (FalkMonster(Pawn) != none && !FalkMonster(Pawn).fFrozen && Enemy == None && FalkMonster(Pawn).fSpawnTarget != none)
    {
        Enemy                          = FalkMonster(Pawn).fSpawnTarget;
        Target                         = Enemy;
        FalkMonster(Pawn).fSpawnTarget = none;
    }
}

// prevent ranged attacks while zapped
function bool FireWeaponAt(Actor A)
{
    if (KFMonster(Pawn) != None && KFMonster(Pawn).bZapped)
        return false;

    return Super.FireWeaponAt(A);
}


// NOPE
function bool CanKillMeYet()
{
    return false;
}

// don't commit suicide
function bool FindRoamDest()
{
    local actor BestPath;

    if (Pawn.FindAnchorFailedTime == Level.TimeSeconds)
    {
        // couldn't find an anchor.
        GoalString = "No anchor "$Level.TimeSeconds;

        if (Pawn.LastValidAnchorTime > 5)
        {
            if ( bSoaking )
                SoakStop("NO PATH AVAILABLE!!!");
            else if (Physics != PHYS_Falling)
            {
                Pawn.SetPhysics(PHYS_Falling);
                // Pawn.Velocity = 0.5 * Pawn.GroundSpeed * VRand();
                Pawn.Velocity.Z = Pawn.JumpZ;
            }
        }
        //log(self$" Find Anchor failed!");
        return false;
    }

    NumRandomJumps = 0;
    GoalString = "Find roam dest "$Level.TimeSeconds;

    // find random NavigationPoint to roam to
    if ((RouteGoal == None) || (Pawn.Anchor == RouteGoal)
            || Pawn.ReachedDestination(RouteGoal))
    {
        RouteGoal = FindRandomDest();
        BestPath = RouteCache[0];
        if (RouteGoal == None)
        {
            if (bSoaking && (Physics != PHYS_Falling))
                SoakStop("COULDN'T FIND ROAM DESTINATION");
            return false;
        }
    }
    if (BestPath == None)
        BestPath = FindPathToward(RouteGoal,false);

    if (BestPath != None)
    {
        MoveTarget = BestPath;
        GotoState('ZombieRoam');
        return true;
    }

    if (bSoaking && (Physics != PHYS_Falling))
        SoakStop("COULDN'T FIND ROAM PATH TO "$RouteGoal);

    RouteGoal = None;
    return false;
}

// re-implemented same species as to prevent zeds from fighting each others in an uncontrolled way
function bool SetEnemy(Pawn NewEnemy, optional bool bHateMonster, optional float MonsterHateChanceOverride)
{
    if (!Monster(Pawn).SameSpeciesAs(NewEnemy))
        return Super.SetEnemy(NewEnemy, bHateMonster, MonsterHateChanceOverride);

    return false;
}

// stairs bug fix attempt, jump on players while stuck on stairs
function FStairsFixJump()
{
    if (!IsInState('FStairsFixJumpState'))
        GotoState('FStairsFixJumpState');
}

// pick a target the falk way
function pawn FalkPickTarget(out float bestAim, out float bestDist, vector FireDir, vector projStart, float MaxRange, optional int fZedID)
{
    /*local pawn fStored;

    if (fStoredSpawnTarget != none)
    {
        fStored            = fStoredSpawnTarget;
        fStoredSpawnTarget = none;
        warn("SETTING SPAWN TARGET FROM FALKPICKTARGET");
        return fStored;
    }*/

    if (Enemy != none)
    {
        //warn("ZED"@fZedID@"Set KFHumanPawn Enemy"@Level.TimeSeconds);
        return Enemy;
    }

    if (target != none && pawn(target) != none)
    {
        //warn("ZED"@fZedID@"Set KFHumanPawn Target"@Level.TimeSeconds);
        return pawn(target);
    }

    if (focus != none && pawn(focus) != none)
    {
        //warn("ZED"@fZedID@"Set KFHumanPawn Focus"@Level.TimeSeconds);
        return pawn(focus);
    }

    //warn("ZED"@fZedID@"Vanilla"@Level.TimeSeconds);

    return PickTarget(bestAim, bestDist, FireDir, projStart, MaxRange);
}

// this is broken af and we're too lazy to fix it... bye.
function bool FindFreshBody()
{
    return False;
}

// fixed slowrage mechanics after freezehack was activated
state WaitForAnim
{
    Ignores SeePlayer,HearNoise,Timer,EnemyNotVisible,NotifyBump,Startle;

    // Don't do this in this state
    function GetOutOfTheWayOfShot(vector ShotDirection, vector ShotOrigin){}

    event AnimEnd(int Channel)
    {
        /*if (FalkMonster(Pawn).fFrozen)
          warn("Controller AnimEnd:"@Level.TimeSeconds);*/

        Pawn.AnimEnd(Channel);

        if (!Monster(Pawn).bShotAnim)
            WhatToDoNext(99);
    }

    function BeginState()
    {
        bUseFreezeHack = False;
    }

    function Tick(float Delta)
    {
        Global.Tick(Delta);

        if (bUseFreezeHack)
        {
            MoveTarget = None;
            MoveTimer = -1;
            Pawn.Acceleration = vect(0,0,0);

            if (KFMonster(Pawn) != none)
                KFMonster(Pawn).SetGroundSpeed(1);

            else
                Pawn.GroundSpeed = 1;

            Pawn.AccelRate = 0;
        }
    }

    function EndState()
    {
        if (fLastSpeedChange > 0 && fLastSpeedChange > Level.TimeSeconds + fSpeedChangeCD) // try to setup a cooldown for speed changes
        {
            if (FalkMonster(Pawn) == none || FalkMonster(Pawn).fGroundSpeed <= Pawn.GroundSpeed) // ignore cooldown if we gotta go fast
                return;
        }

        fLastSpeedChange = Level.TimeSeconds;

        if (Pawn != None)
        {
            Pawn.AccelRate = Pawn.Default.AccelRate;

            // try to properly reset ground speed
            if (FalkMonster(Pawn) != none)
            {
                // the zed was decapitated, take it into account
                if (FalkMonster(Pawn).DecapitatedBy != None || FalkMonster(Pawn).bDecapitated)
                {
                    Pawn.GroundSpeed = FalkMonster(Pawn).GetOriginalGroundSpeed() * FalkMonster(Pawn).fHeadlessSpeed;
                    //warn("Headless Set:"@Pawn.GroundSpeed@"Time:"@Level.TimeSeconds);
                }

                // the zed was burning
                else if (FalkMonster(Pawn).BurnDown > 0)
                {
                    Pawn.GroundSpeed = FalkMonster(Pawn).GetOriginalGroundSpeed() * FalkMonster(Pawn).fBurningSpeed;
                    //warn("Burning Set:"@Pawn.GroundSpeed@"Time:"@Level.TimeSeconds);
                }

                // this zed can actually rage
                else if (FalkMonster(Pawn).Default.fShouldRage)
                {
                    if (FalkMonster(Pawn).fGroundSpeed > 0)
                    {
                        // don't set weird results, only decent values
                        if (FalkMonster(Pawn).fGroundSpeed <= FalkMonster(Pawn).GetOriginalGroundSpeed() * FalkMonster(Pawn).fRageSpeed)
                        {
                            Pawn.GroundSpeed = FalkMonster(Pawn).fGroundSpeed;
                            //warn("Raging Normal Set:"@FalkMonster(Pawn).fGroundSpeed@"Time:"@Level.TimeSeconds);
                        }

                        else
                        {
                            FalkMonster(Pawn).fGroundSpeed = FalkMonster(Pawn).GetOriginalGroundSpeed();
                            Pawn.GroundSpeed = FalkMonster(Pawn).GetOriginalGroundSpeed();
                            //warn("Raging Weird Set:"@Pawn.GroundSpeed@"Time:"@Level.TimeSeconds);
                        }
                    }

                    else
                    {
                        FalkMonster(Pawn).fGroundSpeed = FalkMonster(Pawn).GetOriginalGroundSpeed();
                        Pawn.GroundSpeed               = FalkMonster(Pawn).GetOriginalGroundSpeed();
                        //warn("Raging to Default Set:"@Pawn.GroundSpeed@"Time:"@Level.TimeSeconds);
                    }
                }

                else
                {
                    FalkMonster(Pawn).fGroundSpeed = FalkMonster(Pawn).GetOriginalGroundSpeed();
                    Pawn.GroundSpeed               = FalkMonster(Pawn).GetOriginalGroundSpeed();
                }

                // this zed was zapped, take it into account
                if (FalkMonster(Pawn).bZapped)
                    Pawn.GroundSpeed *= FalkMonster(Pawn).ZappedSpeedMod;
            }

            else if (KFMonster(Pawn) != none)
                Pawn.GroundSpeed = KFMonster(Pawn).GetOriginalGroundSpeed();

            else
                Pawn.GroundSpeed = Pawn.Default.GroundSpeed;
        }

        bUseFreezeHack = False;
    }

Begin:
    while (KFM.bShotAnim)
    {
        Sleep(0.15);
    }
    WhatToDoNext(99);
}

// more attempts to fix slowrage
state DoorBashing
{
    ignores EnemyNotVisible,SeeMonster;

    function Timer()
    {
        Disable('NotifyBump');
    }

    function AttackDoor()
    {
        Target = TargetDoor;
        KFM.Acceleration = vect(0,0,0);
        KFM.DoorAttack(Target);
    }
    function SeePlayer( Pawn Seen )
    {
        if( KFM.Intelligence == BRAINS_Human && ActorReachable(Seen) && SetEnemy(Seen))
            WhatToDoNext(23);
    }
    function DamageAttitudeTo(Pawn Other, float Damage)
    {
        if (KFM.Intelligence >= BRAINS_Mammal && Other != None && ActorReachable(Other) && SetEnemy(Other))
            WhatToDoNext(32);
    }
    function HearNoise(float Loudness, Actor NoiseMaker)
    {
        if (KFM.Intelligence == BRAINS_Human && NoiseMaker != None && NoiseMaker.Instigator != None && ActorReachable(NoiseMaker.Instigator) && SetEnemy(NoiseMaker.Instigator))
            WhatToDoNext(32);
    }

    function Tick( float Delta )
    {
        Global.Tick(Delta);

        // Don't move while we are bashing a door!
        MoveTarget = None;
        MoveTimer = -1;
        Pawn.Acceleration = vect(0,0,0);

        if (KFMonster(Pawn) != none)
            KFMonster(Pawn).SetGroundSpeed(1);

        else
            Pawn.GroundSpeed = 1;

        Pawn.AccelRate = 0;
    }

    function EndState()
    {
        if (Pawn!=None)
        {
            Pawn.AccelRate = Pawn.Default.AccelRate;

            // try to properly reset ground speed
            if (FalkMonster(Pawn) != none)
            {
                // the zed was decapitated, take it into account
                if (FalkMonster(Pawn).DecapitatedBy != None || FalkMonster(Pawn).bDecapitated)
                    Pawn.GroundSpeed = FalkMonster(Pawn).GetOriginalGroundSpeed() * FalkMonster(Pawn).fHeadlessSpeed;

                // the zed was burning
                else if (FalkMonster(Pawn).BurnDown > 0)
                    Pawn.GroundSpeed = FalkMonster(Pawn).GetOriginalGroundSpeed() * FalkMonster(Pawn).fBurningSpeed;

                // this zed can actually rage
                else if (FalkMonster(Pawn).Default.fShouldRage)
                {
                    if (FalkMonster(Pawn).fGroundSpeed > 0)
                    {
                        // don't set weird results, only decent values
                        if (FalkMonster(Pawn).fGroundSpeed <= FalkMonster(Pawn).GetOriginalGroundSpeed() * FalkMonster(Pawn).fRageSpeed)
                            Pawn.GroundSpeed = FalkMonster(Pawn).fGroundSpeed;

                        else
                        {
                            FalkMonster(Pawn).fGroundSpeed = FalkMonster(Pawn).GetOriginalGroundSpeed();
                            Pawn.GroundSpeed = FalkMonster(Pawn).GetOriginalGroundSpeed();
                        }
                    }

                    else
                    {
                        FalkMonster(Pawn).fGroundSpeed = FalkMonster(Pawn).GetOriginalGroundSpeed();
                        Pawn.GroundSpeed               = FalkMonster(Pawn).GetOriginalGroundSpeed();
                    }
                }

                else
                {
                    FalkMonster(Pawn).fGroundSpeed = FalkMonster(Pawn).GetOriginalGroundSpeed();
                    Pawn.GroundSpeed               = FalkMonster(Pawn).GetOriginalGroundSpeed();
                }

                // this zed was zapped, take it into account
                if (FalkMonster(Pawn).bZapped)
                    Pawn.GroundSpeed *= FalkMonster(Pawn).ZappedSpeedMod;
            }

            else if (KFMonster(Pawn) != none)
                Pawn.GroundSpeed = KFMonster(Pawn).GetOriginalGroundSpeed();

            else
                Pawn.GroundSpeed = Pawn.Default.GroundSpeed;
        }
    }

Begin:
    WaitForLanding();

KeepMoving:
    //MoveTarget = TargetDoor;
    //if(MoveTarget!=none)
    //	MoveToward(MoveTarget,FaceActor(1),,false ); //,GetDesiredOffset(),ShouldStrafeTo(MoveTarget));

    While( KFM.bShotAnim )
        Sleep(0.25);
    While(TargetDoor != none && !TargetDoor.bHidden && TargetDoor.bSealed)
    {
        AttackDoor();
        While( KFM.bShotAnim )
            Sleep(0.25);
        Sleep(0.1);
        if (KFM.Intelligence >= BRAINS_Mammal && Enemy != None && ActorReachable(Enemy))
            WhatToDoNext(14);
    }
    WhatToDoNext(152);

Moving:
    MoveToward(TargetDoor);
    WhatToDoNext(17);
    if (bSoaking)
        SoakStop("STUCK IN CHARGING!");
}

// try to reach the players as soon as possible
state ZombieHunt
{
    function PickDestination()
    {
        if (Enemy != none && PathFindState == 0)
        {
            InitialPathGoal = Enemy;
            FindBestPathToward(Enemy, true, false);
            PathFindState   = 1;
            return;
        }

        Super.PickDestination();
    }
}

// based on GettingOutOfTheWayOfShot state, hopefully jump on players instead of getting stuck
state FStairsFixJumpState
{
    ignores EnemyNotVisible, SeePlayer, HearNoise;

    function HitTheDirt()
    {
        local vector dir;

        GoalString = "AVOID Shot!   Jumping!!!";

        dir = Vector(Rotation);

        Pawn.Velocity = Pawn.JumpZ * 0.5 * Normal(dir);
        Pawn.Velocity.Z = Pawn.JumpZ * 0.25;

        bPlannedJump=True;

        Pawn.SetPhysics(PHYS_Falling);
    }

Begin:
    WaitForLanding();
    HitTheDirt();
    Sleep(0.2);
    WhatToDoNext(11);
}

defaultproperties
{
    fSpeedChangeCD=1.0
}
