/* HEAVY ZED */

class FalkZombieFleshpound extends FalkZombieFleshpoundBase
   abstract;
   
#exec OBJ LOAD FILE=LairFleshpound_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

// rage and freeze related stuff
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
   local int OldHealth;

   if (class<DamTypeBossLAWProj>(damageType) != none)
      Damage *= 1.5;

   if (LastDamagedTime < Level.TimeSeconds)
      TwoSecondDamageTotal = 0;

   LastDamagedTime = Level.TimeSeconds + 2;
   OldHealth = Health;

   if (Damage >= Health)
      PostNetReceive();

   Super.TakeDamage(Damage, InstigatedBy, Hitlocation, Momentum, damageType, HitIndex);

   // Don't rage for a while
   if (Level.TimeSeconds <= FUnfreezeTime)
      return;

   TwoSecondDamageTotal += OldHealth - Health;

   if (fFrozen || bZapped || fIsStunned || bDecapitated || bChargingPlayer || IsInState('RageCharging') || IsInState('ChargeToMarker'))
      TwoSecondDamageTotal = 0;

   else if (TwoSecondDamageTotal > CurRDThreshold)
   {
      LastPainAnim    = Level.TimeSeconds;
      StartCharging();
   }
}

simulated function PostBeginPlay()
{
   local Falk689GameTypeBase Flk;

   Super.PostBeginPlay();

   if (Role < ROLE_Authority)
      return;

   // Difficulty Scaling
   if (Level.Game != none)
   {
      if(Level.Game.GameDifficulty < 3.0) // normal
         CurRDThreshold = NrmRDThreshold;

      else if(Level.Game.GameDifficulty < 5.0) // hard
         CurRDThreshold = HrdRDThreshold;

      else if(Level.Game.GameDifficulty < 6.0) // suicidal
         CurRDThreshold = SuiRDThreshold;

      else if(Level.Game.GameDifficulty < 8.0) // hoe
         CurRDThreshold = HOERDThreshold;

      else                                    // bb
         CurRDThreshold = BBRDThreshold;

      Flk = Falk689GameTypeBase(Level.Game);

      // set debug ID
      if (Flk != none)
         fFleshpoundID  = Flk.FalkGetZedDebugID();
   }
}

simulated function PostNetBeginPlay()
{
   /*if (AvoidArea == None)
      AvoidArea = Spawn(class'FleshPoundAvoidArea',self);

   if (AvoidArea != None)
      AvoidArea.InitFor(Self);*/

   EnableChannelNotify ( 1,1);
   AnimBlendParams(1, 1.0, 0.0,, SpineBone1);
   super.PostNetBeginPlay();
}

// This zed has been taken control of, not sure what this does but nah, don't boost anything
function SetMindControlled(bool bNewMindControlled)
{
   if( bNewMindControlled )
   {
      NumZCDHits++;

      // if we hit him a couple of times, make him rage!
      if(NumZCDHits > 1)
      {
         if( !IsInState('ChargeToMarker') )
         {
            GotoState('ChargeToMarker');
         }
         else
         {
            NumZCDHits = 1;
            if( IsInState('ChargeToMarker') )
            {
               GotoState('');
            }
         }
      }
      else
      {
         if( IsInState('ChargeToMarker') )
         {
            GotoState('');
         }
      }
   }
   else
   {
      NumZCDHits=0;
   }

   bZedUnderControl = bNewMindControlled;
}


// Handle the zed being commanded to move to a new location
function GivenNewMarker()
{
   if( bChargingPlayer && NumZCDHits > 1  )
   {
      GotoState('ChargeToMarker');
   }
   else
   {
      GotoState('');
   }
}

function PlayTakeHit(vector HitLocation, int Damage, class<DamageType> DamageType)
{
   local name Sequence;
   local float Frame, Rate;

   if (fFrozen)
      return;

   if (Level.TimeSeconds - LastPainAnim < MinTimeBetweenPainAnims)
      return;

   GetAnimParams(0, Sequence, Frame, Rate);

   if (Sequence == 'PoundRage')
      return;

   // Don't interrupt the controller if its waiting for an animation to end
   if (!Controller.IsInState('WaitForAnim') && Damage >= 10)
      PlayDirectionalHit(HitLocation);

   LastPainAnim = Level.TimeSeconds;

   if (Level.TimeSeconds - LastPainSound < MinTimeBetweenPainSounds)
      return;

   LastPainSound = Level.TimeSeconds;
   PlaySound(HitSound[0], SLOT_Pain,1.25,,400);
}


// Shouldn't fight with other fleshies, scrakes, brutes, and pat
function bool SameSpeciesAs(Pawn P)
{
   return (FalkZombieFleshpoundBase(P) != none || FalkZombieBruteBase(P) != none || FalkZombieScrakeBase(P) != none || Super.SameSpeciesAs(P));
}


// changes colors on Device (notified in anim)
simulated function DeviceGoRed()
{
   Skins[1]=Shader'KFCharacters.FPRedBloomShader';
}

simulated function DeviceGoNormal()
{
   Skins[1]=Shader'KFCharacters.FPAmberBloomShader';
}

// use our shader on crisped up zeds
simulated function ZombieCrispUp()
{
   bAshen = true;
   bCrispified = true;

   SetBurningBehavior();

   if (Level.NetMode == NM_DedicatedServer || class'GameInfo'.static.UseLowGore())
      Return;

   Skins[0]=Shader'LairTextures_T.ZedsUpscale.ZedBurnSkinShader';
   Skins[2]=Shader'LairTextures_T.ZedsUpscale.ZedBurnSkinShader';
   Skins[3]=Shader'LairTextures_T.ZedsUpscale.ZedBurnSkinShader';
}

// Sets the FP in a berserk charge state until he either strikes his target, or hits timeout
function StartCharging()
{
   local float RageAnimDur;
   
   if (Health <= 0 || fFrozen || DecapitatedBy != none || fIsStunned || Level.TimeSeconds <= FUnfreezeTime)
      return;

   LastPainAnim    = Level.TimeSeconds;
   FChargeMoveTime = Level.TimeSeconds + FRageAnimDuration;

   SetAnimAction('PoundRage');

   Acceleration    = vect(0,0,0);
   bShotAnim       = true;
   Velocity.X      = 0;
   Velocity.Y      = 0;

   Controller.GoToState('WaitForAnim');
   KFMonsterController(Controller).bUseFreezeHack = True;

   RageAnimDur     = GetAnimDuration('PoundRage');
   FalkFleshpoundController(Controller).SetPoundRageTimout(RageAnimDur);
   GoToState('BeginRaging');
}

state BeginRaging
{
   Ignores StartCharging;

   // Set the zed to the zapped behavior
   simulated function SetZappedBehavior()
   {
      Global.SetZappedBehavior();
      GoToState('');
   }

   // no stun for you
   function bool FlipOver()
   {
      return False;
   }

   // yeah, no.
   function FStairFixJump()
   {
   }

   function RemoveHead()
   {
      SetGroundSpeed(GetOriginalGroundSpeed());
      GoToState('');
      Global.RemoveHead();
   }

   // YOU should fucking get out of the way
   function bool CanGetOutOfWay()
   {
      return false;
   }

   // nah, maybe later.
   simulated function bool HitCanInterruptAction()
   {
      return false;
   }

   function Tick(float Delta)
   {
      Acceleration = vect(0,0,0);
      LastPainAnim = Level.TimeSeconds;

      global.Tick(Delta);
   }

Begin:
   if (!fIsStunned)
   {
      Sleep(GetAnimDuration('PoundRage'));
      GotoState('RageCharging');
   }

   else
      GotoState('');
}


state RageCharging
{
   Ignores StartCharging;


   function bool FlipOver()
   {
      return False;
   }

   // Set the zed to the zapped behavior
   simulated function SetZappedBehavior()
   {
      Global.SetZappedBehavior();
      GoToState('');
   }

   function RemoveHead()
   {
      SetGroundSpeed(GetOriginalGroundSpeed());
      GoToState('');
      Global.RemoveHead();
   }

   function PlayDirectionalHit(Vector HitLoc)
   {
      if( !bShotAnim )
      {
         super.PlayDirectionalHit(HitLoc);
      }
   }

   function bool CanGetOutOfWay()
   {
      return false;
   }

   // Don't override speed in this state
   function bool CanSpeedAdjust()
   {
      return false;
   }

   function BeginState()
   {
      local float DifficultyModifier;

      if (bZapped || fIsStunned)
         GoToState('');

      else
      {
         bChargingPlayer = true;
         if (Level.NetMode != NM_DedicatedServer)
            ClientChargingAnims();

         // Scale rage length by difficulty
         if (Level.Game.GameDifficulty <= 3.0) // normal
            DifficultyModifier = 1.0;

         else if( Level.Game.GameDifficulty <= 4.0 ) // hard
            DifficultyModifier = 1.2;

         else if( Level.Game.GameDifficulty <= 5.0 ) // suicidal
            DifficultyModifier = 1.4;

         else // HOE
            DifficultyModifier = 1.6;

         RageEndTime   = (Level.TimeSeconds + Default.FMinRageTime * DifficultyModifier) + (FRand() * Default.FRandRageTime);
         NetUpdateTime = Level.TimeSeconds - 1;

         //warn("RageEndTime:"@RageEndTime@"Time:"@Level.TimeSeconds);
      }
   }

   function EndState()
   {
      bChargingPlayer      = False;
      bFrustrated          = false;
      TwoSecondDamageTotal = 0;

      FalkFleshpoundController(Controller).RageFrustrationTimer = 0;

      if (Health > 0 && !bZapped)
         SetGroundSpeed(GetOriginalGroundSpeed());

      if( Level.NetMode!=NM_DedicatedServer )
         ClientChargingAnims();


      NetUpdateTime = Level.TimeSeconds - 1;
   }

   function Tick( float Delta )
   {
      if (bZapped || fFrozen || bDecapitated || DecapitatedBy != none)
      {
         GoToState('');
      }

      global.Tick(Delta);

      if (Level.TimeSeconds < FChargeMoveTime)
      {
         SetGroundSpeed(1);
         Acceleration = vect(0,0,0);
      }

      else
      {
         if (!bShotAnim)
         {
            SetGroundSpeed(OriginalGroundSpeed * fRageSpeed);

            // keep raging at bloodbath
            if (Level.Game.GameDifficulty < 8.0 && Level.TimeSeconds > RageEndTime)
               GoToState('');
         }

         // Keep the flesh pound moving toward its target when attacking
         if( Role == ROLE_Authority && bShotAnim)
         {
            if( LookTarget!=None )
            {
               Acceleration = AccelRate * Normal(LookTarget.Location - Location);
            }
         }
      }
   }

   function Bump( Actor Other )
   {
      local KFMonster KFMonst;

      KFMonst = KFMonster(Other);

      // Hurt/Kill enemies that we run into while raging
      if(!bShotAnim && KFMonst!=None && FalkZombieFleshPound(Other) == None && Pawn(Other).Health>0)
         Other.TakeDamage(1000, self, Other.Location, Velocity * Other.Mass, class'DamTypePoundCrushedFalk');

      else Global.Bump(Other);
   }

   // If fleshie hits his target on a charge, then he should settle down for abit.
   function bool MeleeDamageTarget(int hitdamage, vector pushdir)
   {
      local bool RetVal,bWasEnemy;

      bWasEnemy = (Controller.Target==Controller.Enemy);
      RetVal = Super.MeleeDamageTarget(hitdamage * fRageDamageMult, pushdir*3);
      if(RetVal && bWasEnemy)
         GoToState('');
      return RetVal;
   }
}

// reset rage timer on zap
simulated function SetZappedBehavior()
{
   if (fFrozen)
      return;

   FalkFleshpoundController(Controller).RageFrustrationTimer = 0;
   TwoSecondDamageTotal                                      = 0;

   Super.SetZappedBehavior();
}


// reset rage timer on unzap
simulated function UnSetZappedBehavior()
{
   FalkFleshpoundController(Controller).RageFrustrationTimer = 0;
   TwoSecondDamageTotal                                      = 0;

   Super.UnSetZappedBehavior();
}

// alternative claw damage implementation
function ClawDamageTarget()
{
    local vector PushDir;
    local KFHumanPawn HumanTarget;
    local KFPlayerController HumanTargetController;
    local float UsedMeleeDamage;
    local name  Sequence;
    local float Frame, Rate;

    if (fFrozen)
        return;

    GetAnimParams(ExpectingChannel, Sequence, Frame, Rate);

    UsedMeleeDamage = FalkGetDamage(MeleeDamage);

    // reduce the melee damage for anims with repeated attacks, since it does repeated damage over time
    if (Sequence == 'PoundAttack1')
        UsedMeleeDamage *= 0.5;

    else if (Sequence == 'PoundAttack2')
        UsedMeleeDamage *= 0.2;

    if (Controller != none && Controller.Target != none)
    {
        //calculate based on relative positions
        PushDir = (damageForce * Normal(Controller.Target.Location - Location));
    }

    else
    {
        //calculate based on way Monster is facing
        PushDir = damageForce * vector(Rotation);
    }

    if (!fFrozen && !bZapped && !fIsStunned && MeleeDamageTarget(UsedMeleeDamage, PushDir))
    {
        HumanTarget         = KFHumanPawn(Controller.Target);
        fLastClawDamageTime = Level.TimeSeconds;

        if(HumanTarget != None)
            HumanTargetController = KFPlayerController(HumanTarget.Controller);

        if(HumanTargetController != None)
            HumanTargetController.ShakeView(RotMag, RotRate, RotTime, OffsetMag, OffsetRate, OffsetTime);

        PlaySound(MeleeAttackHitSound, SLOT_Interact, 1.25);
    }
}

// alternative spin damage implementation
function SpinDamage(actor Target)
{
   local vector HitLocation;
   local Name TearBone;
   local Float dummy;
   local vector PushDir;
   local KFHumanPawn HumanTarget;

   if (target == none || fFrozen)
      return;

   PushDir = (damageForce * Normal(Target.Location - Location));

   if (Target.IsA('KFHumanPawn') && Pawn(Target).Health <= MeleeDamage)
   {
      KFHumanPawn(Target).RagDeathVel    *= 3;
      KFHumanPawn(Target).RagDeathUpKick *= 1.5;
   }

   if (Target !=none && Target.IsA('KFDoorMover'))
   {
      Target.TakeDamage(FalkGetDamage(MeleeDamage), self, HitLocation, pushdir, class'KFmod.ZombieMeleeDamage');
      PlaySound(MeleeAttackHitSound, SLOT_Interact, 1.25);
   }

   // block double hits here too
   if (KFHumanPawn(Target) != none && Level.TimeSeconds > fLastMeleeDamage + fMeleeDamageCD)
   {
      fLastMeleeDamage = Level.TimeSeconds;
      HumanTarget      = KFHumanPawn(Target);

      if (HumanTarget.Controller != none)
         HumanTarget.Controller.ShakeView(RotMag, RotRate, RotTime, OffsetMag, OffsetRate, OffsetTime);

      KFHumanPawn(Target).TakeDamage(MeleeDamage, self, HitLocation, pushdir, class'KFmod.ZombieMeleeDamage');

      if (KFHumanPawn(Target).Health <=0)
      {
         KFHumanPawn(Target).SpawnGibs(rotator(pushdir), 1);
         TearBone = KFPawn(Target).GetClosestBone(HitLocation, Velocity, dummy);
         KFHumanPawn(Controller.Target).HideBone(TearBone);
      }
   }
}


// no burning behavior
simulated function SetBurningBehavior(){}
simulated function UnSetBurningBehavior(){}


// State where the zed is charging to a marked location.
// Not sure if we need this since its just like RageCharging,
// but keeping it here for now in case we need to implement some
// custom behavior for this state
state ChargeToMarker extends RageCharging
{
   Ignores StartCharging;

   function Tick(float Delta)
   {
      global.Tick(Delta);

      if (Level.TimeSeconds < FChargeMoveTime)
      {
         SetGroundSpeed(1);
         Acceleration = vect(0,0,0);
      }

      else 
      {
         if (!bShotAnim)
         {
            SetGroundSpeed(OriginalGroundSpeed * fRageSpeed);

            // keep raging at bloodbath
            if (Level.Game.GameDifficulty < 8.0 && Level.TimeSeconds > RageEndTime)
            {
               GoToState('');
            }
         }

         // Keep the flesh pound moving toward its target when attacking
         if( Role == ROLE_Authority && bShotAnim)
         {
            if( LookTarget!=None )
            {
               Acceleration = AccelRate * Normal(LookTarget.Location - Location);
            }
         }
      }
   }
}

simulated function PostNetReceive()
{
   if (bClientCharge != bChargingPlayer/*&& !bZapped*/)
   {
      bClientCharge = bChargingPlayer;

      if (bChargingPlayer)
      {
         MovementAnims[0]=ChargingAnim;
         MeleeAnims[0]='FPRageAttack';
         MeleeAnims[1]='FPRageAttack';
         MeleeAnims[2]='FPRageAttack';
         DeviceGoRed();
      }
      else
      {
         MovementAnims[0]=default.MovementAnims[0];
         MeleeAnims[0]=default.MeleeAnims[0];
         MeleeAnims[1]=default.MeleeAnims[1];
         MeleeAnims[2]=default.MeleeAnims[2];
         DeviceGoNormal();
      }
   }
}

simulated function PlayDyingAnimation(class<DamageType> DamageType, vector HitLoc)
{
   Super.PlayDyingAnimation(DamageType,HitLoc);
   if( Level.NetMode!=NM_DedicatedServer )
      DeviceGoNormal();
}

simulated function ClientChargingAnims()
{
   PostNetReceive();
}

// do the same shit as the falkmonster but reset TwoSecondDamageTotal too
simulated function AnimEnd(int Channel)
{
   AnimAction  = '';

   if (Level.NetMode != NM_Client)
   {
      bResetAnimAct    = True;
      ResetAnimActTime = Level.TimeSeconds + 0.3;
   }

   if (bShotAnim && Channel == ExpectingChannel)
   {
      bShotAnim            = false;
      fIsStunned           = false;  
      TwoSecondDamageTotal = 0;
      //warn("fIsStunned = False - Time:"@Level.TimeSeconds);

      if (Controller != none)
      {
         //warn("CONTROLLER RESTORE:"@Level.TimeSeconds);

         if (Controller.Enemy == none && fStoredStunEnemy != none)
            Controller.Enemy = fStoredStunEnemy;

         if (Controller.Focus == none && fStoredStunEnemy != none)
            Controller.Focus = fStoredStunFocus;
      }

      SetGroundSpeed(GetOriginalGroundSpeed());

      if (Controller != None)
         Controller.bPreparingMove = false;
   }

   if (!bPhysicsAnimUpdate && Channel==0)
      bPhysicsAnimUpdate = Default.bPhysicsAnimUpdate;


   Super(xPawn).AnimEnd(Channel);
}


simulated function int DoAnimAction( name AnimName )
{
   if (fFrozen)
      return 0;

   if (AnimName=='PoundAttack1' || AnimName=='FPRageAttack' || AnimName=='ZombieFireGun') 
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;
      AnimBlendParams(1, 1.0, 0.0,, FireRootBone);
      PlayAnim(AnimName,, 0.1, 1);
      Return 1;
   }

   else if (AnimName=='PoundAttack2' || AnimName=='PoundAttack3')
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 0.9) + fAnimEndAddDelay;
      AnimBlendParams(1, 1.0, 0.0,, FireRootBone);
      PlayAnim(AnimName,, 0.1, 1);
      Return 1;
   }

   else if (AnimName=='KnockDown')
      FalkFleshpoundController(Controller).RageFrustrationTimer = 0;

   Return Super.DoAnimAction(AnimName);
}

simulated event SetAnimAction(name NewAction)
{
   local int meleeAnimIndex;

   if (NewAction=='' || fFrozen)
      return;

   if (NewAction == 'Claw')
   {
      meleeAnimIndex = Rand(3);
      NewAction = meleeAnims[meleeAnimIndex];
      CurrentDamtype = ZombieDamType[meleeAnimIndex];
   }

   else if (NewAction == 'DoorBash')
      CurrentDamtype = ZombieDamType[Rand(3)];

   ExpectingChannel = DoAnimAction(NewAction);

   if (AnimNeedsWait(NewAction))
      bWaitForAnim = true;

   if (Level.NetMode!=NM_Client)
   {
      AnimAction = NewAction;
      bResetAnimAct = True;
      ResetAnimActTime = Level.TimeSeconds+0.3;
   }
}

// The animation is full body and should set the bWaitForAnim flag
simulated function bool AnimNeedsWait(name TestAnim)
{
   if (TestAnim == 'KnockDown' || TestAnim == 'PoundRage' || TestAnim == 'DoorBash')
   {
      return true;
   }

   return false;
}

simulated function Tick(float DeltaTime)
{
   super.Tick(DeltaTime);

   if (Role == ROLE_Authority && !bZapped && !fIsStunned && !fFrozen)
   {
      // new attack implementation
      if (fFleshpoundAttackDelay > 0)
         fFleshpoundAttackDelay -= DeltaTime;

      else
      {
         fFleshpoundAttackDelay = Default.fFleshpoundAttackDelay;
         FalkFire(); // FIRE here instead of wherever?
      }

      if (Level.TimeSeconds < FChargeMoveTime)
      {
         SetGroundSpeed(1);
         Acceleration    = vect(0,0,0);
      }

      if (!bDecapitated && GroundSpeed > 1)
      {
         // workaround to reset ground speed after charging
         if (!bChargingPlayer)
         {
            SetGroundSpeed(GetOriginalGroundSpeed());
         }

         // Keep the flesh pound moving toward its target when attacking
         if (bShotAnim)
         {
            if (LookTarget!=None)
               Acceleration = AccelRate * Normal(LookTarget.Location - Location);
         }
      }

      else
      {
         //warn("HEADLESS");
         SetGroundSpeed(GetOriginalGroundSpeed() * Default.fHeadlessSpeed);
      }
   }

   // Turn yellow on freeze pretty fucking pleeze
   if (Level.NetMode != NM_DedicatedServer && fFrozen)
   {
      DeviceGoNormal();
   }
}

function bool FlipOver()
{
   FalkFleshpoundController(Controller).RageFrustrationTimer = 0;
   TwoSecondDamageTotal = 0;

   if (!bChargingPlayer)
      return Super.FlipOver();

   return False;
}

simulated function Destroyed()
{
   /*if( AvoidArea!=None )
      AvoidArea.Destroy();*/

   Super.Destroyed();
}


static simulated function PreCacheMaterials(LevelInfo myLevel)
{
   myLevel.AddPrecacheMaterial(Combiner'LairTextures_T.ZedsUpscale.FleshpoundCombinerFinal');
   myLevel.AddPrecacheMaterial(Combiner'LairTextures_T.ZedsUpscale.FleshpoundEnvironmentCombiner');
   myLevel.AddPrecacheMaterial(Texture'LairTextures_T.ZedsUpscale.fleshpound_diff');
}

// award a nade on LAW kill
function Died(Controller Killer, class<DamageType> damageType, vector HitLocation)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   if (Killer != none && damageType != none && (class<DamTypeLAWExplosiveBulletFalk>(damageType) != none || class<DamTypeLAWFalk>(damageType) != none))
   {
      KFPRI = KFPlayerReplicationInfo(Killer.PlayerReplicationInfo);

      if (KFPRI != none)
      {
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none) 
            Vet.Static.LAWKillNadeAward(KFPRI, LastBulletHitID);
      }
   }

   Super.Died(Killer, damageType, HitLocation);
}

// attempt to reset the device to a yellow state, failed but still we set vars and shit
function fUnfreezeFix()
{
   bClientCharge        = True;
   bChargingPlayer      = False;
   bFrustrated          = False;
   TwoSecondDamageTotal = 0;
   NetUpdateTime        = Level.TimeSeconds - 1;

   FalkFleshpoundController(Controller).RageFrustrationTimer = 0;

   Super.fUnfreezeFix();

   PostNetReceive();

   FUnfreezeTime    = Level.TimeSeconds + FFreezeRageWait;

   bClientCharge    = False;
   DeviceGoNormal();
}

// this should turn that device yellow but does horse shit, still resets vars, maybe we need vars
simulated function fClientUnfreezeFix()
{
   bFrustrated          = False;
   bClientCharge        = False;
   bChargingPlayer      = False;
   TwoSecondDamageTotal = 0;

   DeviceGoNormal();
}

// return zed class
function FalkZedClass FalkGetZedClass()
{
   return Falk_Fleshpound;
}

// kinda like don't do this
function RangedAttack(Actor A)
{
}

// RangedAttack is sometimes deactivated without any reason, FalkRangedAttack always happens
function FalkRangedAttack(Actor A)
{
   if (Level.TimeSeconds > fAnimEndTime)
      bShotAnim = False;

   if (fFrozen || bShotAnim)
   {
      //warn("FLESHPOUND"@fFleshpoundID@"RETURN..."@Level.TimeSeconds);
      return;
   }

   else if (CanAttack(A))
   {
      //warn("FLESHPOUND"@fFleshpoundID@" Ranged Attack Call Time:"@Level.TimeSeconds);
      FalkFleshpoundController(Controller).RageFrustrationTimer = 0;
      bShotAnim = true;
      SetAnimAction('Claw');
   }
}

// Return true if we can attack, debug in the meantime
function bool CanAttack(Actor A)
{
   local bool result;

	if (A == none)
   {
      //warn("FLESHPOUND"@fFleshpoundID@"CAN'T ATTACK 1"@Level.TimeSeconds);
		return false;
   }

	if (bSTUNNED)
   {
      //warn("FLESHPOUND"@fFleshpoundID@"CAN'T ATTACK 2"@Level.TimeSeconds);
		return false;
   }

	if (KFDoorMover(A) != none)
   {
      //warn("FLESHPOUND"@fFleshpoundID@"DOOR"@Level.TimeSeconds);
		return true;
   }

	else if (KFHumanPawn(A) != none && KFHumanPawn(A).Health <= 0)
   {
      result = (VSize(A.Location - Location) < MeleeRange + CollisionRadius);

      //warn("FLESHPOUND"@fFleshpoundID@"CanAttack Return 3:"@result@"- Time:"@Level.TimeSeconds);

		return result;
   }

	else
   {
      result = (VSize(A.Location - Location) < MeleeRange + CollisionRadius + A.CollisionRadius);

      //warn("FLESHPOUND"@fFleshpoundID@"CanAttack Return 4:"@result@"- Time:"@Level.TimeSeconds);

      return result;
   }
}

// don't do shit here
function Fire(optional float F)
{
}

// basically called on tick instead of who knows when
function FalkFire(optional float F)
{
	local Actor BestTarget;
   local float bestAim, bestDist;
   local vector FireDir, X,Y,Z;
   local FMonsterController FC;

   if (Controller == none)
      return;

   //warn("FLESHPOUND"@fFleshpoundID@" FALK FIRE"@Level.TimeSeconds);

   bestAim = 0.90;
   GetAxes(Controller.Rotation,X,Y,Z);
   FireDir = X;

   FC = FMonsterController(Controller);

   if (FC != none)
      BestTarget = FC.FalkPickTarget(bestAim, bestDist, FireDir, GetFireStart(X,Y,Z), 6000, fFleshpoundID);
 
   else
      BestTarget = Controller.PickTarget(bestAim, bestDist, FireDir, GetFireStart(X,Y,Z), 6000);

   if (KFHumanPawn(BestTarget) != none || FalkMonster(BestTarget) != none)
      FalkRangedAttack(BestTarget);
}

// ignore FalkRangedAttack too
State ZombieDying
{
   ignores AnimEnd, Trigger, Bump, HitWall, HeadVolumeChange, PhysicsVolumeChange, Falling, BreathTimer, Died, RangedAttack, FalkRangedAttack;
}

// additional double hit prevention
function bool MeleeDamageTarget(int hitdamage, vector pushdir)
{
   if (Level.TimeSeconds > fLastMeleeDamage + fMeleeDamageCD)
   {
      fLastMeleeDamage = Level.TimeSeconds;
      return Super.MeleeDamageTarget(hitdamage, pushdir);
   }

   return False;
}

defaultproperties
{
   EventClasses(0)="Falk689ZedsFix.FZombieFleshpound_STANDARD"
   ControllerClass=Class'FalkFleshpoundController'
}
