class BossLAWProjFalk extends LAWProjFalk;

var int               FNumPlayers;     // store numplayers so our rocket isn't nerfed if someone quits during boss wave
var class<Projectile> sFClass;         // Sound fix class, a projectile that just goes kaboom without damage

// testing fix for weird bugs making the server crash on explode and cluster spawn
var float             fCheckExp;          // time between checks in seconds
var float             fCurExpT;           // current check time
var float             FCDamageMulti;      // cluster damage multiplier
var byte              fMaxStillT;         // force explode after we stand still this much
var byte              fCurStillT;         // how many checks we stood still
var vector            fStoredLoc;         // stored location
var float             fSpawnDelay;        // cluster spawn delay, so they don't bite your server's ass
var float             fCurSpawnDelay;     // current spawn delay 
var float             fDoorDamagePerc;    // damage percentage applied to doors

// difficulty scalings
var float             FMultiScalingAdd;   // additional difficulty scaling used for multiplayer
var float             FPlayerScalingAdd;  // additional difficulty scaling added per player alive at pat spawn
var float             FNormalScalingVal;  // base difficulty scaling used at normal difficulty
var float             FHardScalingVal;    // base difficulty scaling used at hard difficulty
var float             FSuiScalingVal;     // base difficulty scaling used at suicidal difficulty
var float             FHOEScalingVal;     // base difficulty scaling used at hell on earth difficulty


simulated function PostBeginPlay()
{
   local float FUsedScalingVal;

   // Setting base difficulty scale value
   if (Level.Game.GameDifficulty < 3.0) 
      FUsedScalingVal = FNormalScalingVal;

   else if (Level.Game.GameDifficulty < 5.0)
      FUsedScalingVal = FHardScalingVal;

   else if (Level.Game.GameDifficulty < 6.0)
      FUsedScalingVal = FSuiScalingVal;

   else
      FUsedScalingVal = FHOEScalingVal;

   // Adding multiplayer fixed difficulty bonus
   if (FNumPlayers > 1)
      FUsedScalingVal += FMultiScalingAdd;

   // Adding per player difficulty bonus
   FUsedScalingVal += (FPlayerScalingAdd * (FNumPlayers - 1));

   //warn("Alive Players:"@FNumPlayers@"- Missile Scaling Value:"@FUsedScalingVal);

   FDamage = default.FDamage * FUsedScalingVal;

   Super(LAWProj).PostBeginPlay();
}

// Sound fix on kaboom and spawn clusters 
simulated function Explode(vector HitLocation, vector HitNormal)
{
   local Projectile P;
   local vector SpawnCorrection;

   if (bHasExploded)
      return;

   // trigger a small fake zed time on kaboom to prevent clusters from killing your family and friends, oh and the server itself
   if (fShouldKaboom)
      KFGameType(Level.Game).DramaticEvent(1.0, 0.3);

   // sound fix
   if (Role == ROLE_Authority)
   {
      // Kaboom
      P = Spawn(sFClass,,, Location, rotator(vect(0,0,1)));

      if (P == none) // Not spawned, retry
      {
         P = Spawn(sFClass,,, Location + fCZRetryLoc, rotator(vect(0,0,1)));

         if (P == none) // last kaboom attempt
            P = Spawn(sFClass,,, Location - fCZRetryLoc, rotator(vect(0,0,1)));
      }
   }

   SpawnCorrection = vect(0,0,0);

   /*if (Role == ROLE_Authority && fShouldKaboom)
   {
      // attempt to fix ground cluster spawn
      fPLocation = Instigator.Location + fCZRetryLoc;

      //log(Location.Z);
      //log(fPLocation.Z);

      if (Location.Z <= fPLocation.Z) // if the nade if under the pat, correct spawn position
         SpawnCorrection = fCSpawnLoc;

      for(i=0; i<fNClusters; i++)
      {
         class<M79ClusterFalk>(fCClass).Default.BulletID = BulletID * BulletID + fSClusters;

         P = Spawn(fCClass,,, Location + SpawnCorrection, RotRand(True));

         if (P != none) 
         {
            fSClusters++;
            P.Instigator = Instigator; 
            P.default.Damage = FDamage * 0.25; 
         }
      }

      //log("Initial spawn");
      //log(fSClusters);
   }*/

   FalkExplode(HitLocation, HitNormal);
}


// Explode but check if we spawned all clusters before destroy
/*simulated function FalkExplode(vector HitLocation, vector HitNormal)
{
   local Controller C;
   local PlayerController  LocalPlayer;

   bHasExploded = True;

   // Don't explode if this is a dud
   if(bDud)
   {
      Velocity = vect(0,0,0);
      LifeSpan=1.0;
      SetPhysics(PHYS_Falling);
   }

   PlaySound(ExplosionSound,,2.0);

   Spawn(class'KFMod.KFNadeLExplosion',,,HitLocation + HitNormal*20,rotator(HitNormal));
   Spawn(ExplosionDecal,self,,HitLocation, rotator(-HitNormal));

   BlowUp(HitLocation);

   //log("fSClusters "@fSClusters);
   //log("fNClusters "@fNClusters);

   // Shake nearby players screens
   LocalPlayer = Level.GetLocalPlayerController();
   if ( (LocalPlayer != None) && (VSize(Location - LocalPlayer.ViewTarget.Location) < DamageRadius) )
      LocalPlayer.ShakeView(RotMag, RotRate, RotTime, OffsetMag, OffsetRate, OffsetTime);

   for ( C=Level.ControllerList; C!=None; C=C.NextController )
      if ( (PlayerController(C) != None) && (C != LocalPlayer)
            && (VSize(Location - PlayerController(C).ViewTarget.Location) < DamageRadius) )
         C.ShakeView(RotMag, RotRate, RotTime, OffsetMag, OffsetRate, OffsetTime);
}*/


// Overridden to prevent clusters from blowing up our nade
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{ 
}

// Backup cluster spawn, here we'll need a zed time based fix with cluster a cluster spawn delay of 0.1
simulated function Tick(float DeltaTime)
{
   local Projectile P;
   local byte fAttempt;

   Super(LAWProj).Tick(DeltaTime);

   // this should make this rocket explode if it doesn't move, I suppose there was a really weird bug here. - Falk689
   if (Role != ROLE_Authority)
   {
      if (fCurExpT < fCheckExp)
         fCurExpT += DeltaTime;

      else
      {
         fCurExpT = 0;

         if (Location == fStoredLoc)
         {
            fCurStillT++;

            if (fCurStillT >= fMaxStillT)
	            Explode(Location, vect(0,0,1)); 

         }

         else
            fCurStillT = 0;

         fStoredLoc = Location;
      }
   }

   // and this should spawn clusters
   else if (bHasExploded && fShouldKaboom)
   {
      if (fCurSpawnDelay < fSpawnDelay)
         fCurSpawnDelay += DeltaTime;

      else if (fSClusters < fNClusters)
      {
         // Try to spawn remaining clusters
         if (fTry < fMaxTry)
         {
            class<M79ClusterFalk>(fCClass).Default.BulletID = BulletID + fSClusters;

            if (fSuccessSpawn)
            {
               P = Spawn(fCClass,,, fSuccessPos, RotRand(True));

               if (P == None)
                  fSuccessSpawn = False;
            }

            if (!fSuccessSpawn)
            {
               While (P == None && fAttempt < 27)
               { 
                  fAttempt++; // welp, for the future testing 0 here would be cool but meh - Falk689

                  if (fAttempt >= 27)
                  {
                     //warn("FAIL"@fTry);
                     fACZRetryLoc += fCZRetryLoc;
                     fACXRetryLoc += fCXRetryLoc;
                     fACYRetryLoc += fCYRetryLoc;
                     fTry++;
                  }


                  else if (fAttempt >= 18)
                  {
                     fTempPos = FClusterQuad(Location - fACZRetryLoc, fAttempt - 18);
                     //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
                  }

                  else if (fAttempt >= 9)
                  {
                     fTempPos = FClusterQuad(Location + fACZRetryLoc, fAttempt - 9);
                     //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
                  }

                  else
                  {
                     fTempPos = FClusterQuad(Location, fAttempt);
                     //warn("First:"@fAttempt@"Location:"@fTempPos);
                  }

                  P = Spawn(fCClass,,, fTempPos, RotRand(True));
               }
            }

            if (P != none)
            {
               //warn("Spawned");
               fSuccessPos   = fTempPos; 
               fSuccessSpawn = True;
               P.Damage      = FDamage * FCDamageMulti;
               P.Instigator  = Instigator; 

               fSClusters++;

               fTry          = 0;

               if (M79ClusterFalk(P) != none)
                  M79ClusterFalk(P).fCInstigator = fCInstigator;
            }
         }

         // Give up
         else
         {
            //warn("Giving up");
            fACZRetryLoc  = fCZRetryLoc;
            fACXRetryLoc  = fCXRetryLoc;
            fACYRetryLoc  = fCYRetryLoc;
            fSClusters    = fNClusters;
            fShouldKaboom = False;
            fTry = 0;
            Destroy();
         }
      }

      else
      {
         //warn("Giving up 2");
         fShouldKaboom = False;
         Destroy();
      }
   }
}

simulated function ProcessTouch(Actor Other, Vector HitLocation)
{
   // Don't let it hit this player, or blow up on another player
   if (Other == Instigator || Other.Base == Instigator)
      return;

   // Don't collide with bullet whip attachments
   if (KFBulletWhipAttachment(Other) != none)
      return;

   Explode(HitLocation,Normal(HitLocation-Other.Location));
}

simulated function HitWall(vector HitNormal, actor Wall)
{
   // Don't let it hit this player, or blow up on another player
   if (Wall == none || Wall == Instigator || Wall.Base == Instigator)
      return;

   // Don't collide with bullet whip attachments
   if (KFBulletWhipAttachment(Wall) != none)
      return;

   Explode(Location, HitNormal);
}

// use FDamage and cap damage
simulated function HurtRadius(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation)
{
   local actor Victims;
   local float damageScale, dist;
   local vector dirs;
   local int NumKilled;
   local KFMonster KFMonsterVictim;
   local Pawn P;
   local KFPawn KFP;
   local array<Pawn> CheckedPawns;
   local int i;
   local bool bAlreadyChecked;

   if (bHurtEntry)
      return;

   bHurtEntry   = true;

   foreach CollidingActors (class 'Actor', Victims, DamageRadius, HitLocation)
   {
      // don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
      if ((Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo') && ExtendedZCollision(Victims)==None )
      {
         dirs = Victims.Location - HitLocation;
         dist = FMax(1,VSize(dirs));
         dirs = dirs/dist;

         if (Instigator == None || Instigator.Controller == None )
            Victims.SetDelayedDamageInstigatorController( InstigatorController);

         if (Victims == LastTouched)
            LastTouched = None;

         P = Pawn(Victims);

         // deal a fixed amount of damage to doors
         if (KFDoorMover(Victims) != none)
         {
            DamageAmount = KFDoorMover(Victims).MaxWeld * fDoorDamagePerc;
            damageScale  = 1.0;
            //warn("DOOR:"@DamageAmount@"scale:"@damageScale);
         }

         else if (P != none)
         {
            DamageAmount = FDamage;
            damageScale  = 1 - FMax(0, (dist - Victims.CollisionRadius) / DamageRadius);

            //warn("PAWN:"@DamageAmount@"scale:"@damageScale);

            for (i = 0; i < CheckedPawns.Length; i++)
            {
               if (CheckedPawns[i] == P)
               {
                  bAlreadyChecked = true;
                  break;
               }
            }

            if (bAlreadyChecked)
            {
               bAlreadyChecked = false;
               P = none;
               continue;
            }

            KFMonsterVictim = KFMonster(Victims);

            if (KFMonsterVictim != none && KFMonsterVictim.Health <= 0)
               KFMonsterVictim = none;

            KFP = KFPawn(Victims);

            if (KFMonsterVictim != none)
               damageScale *= KFMonsterVictim.GetExposureTo(HitLocation);

            else if (KFP != none)
               damageScale *= KFP.GetExposureTo(HitLocation);

            if (damageScale > 1.0)
               damageScale = 1.0;

            CheckedPawns[CheckedPawns.Length] = P;

            if (damageScale <= 0)
            {
               P = none;
               continue;
            }

            else
            {
               //Victims = P;
               P = none;
            }
         }

         Victims.TakeDamage
            (
             damageScale * DamageAmount,
             Instigator,
             Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs,
             (damageScale * Momentum * dirs),
             MyDamageType,
             BulletID
            );

         if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
            Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);

         if( Role == ROLE_Authority && KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
         {
            NumKilled++;
         }
      }
   }

   bHurtEntry = false;
}


defaultproperties
{
   ArmDistSquared=0.000000
   Damage=0.000000
   FDamage=135.000000                      // nerfed from vanilla's 200 because our rocket can behave far more dangerously
   ImpactDamageType=Class'DamTypeBossLawRocketImpactFalk'
   MyDamageType=Class'DamTypeBossLAWProj'
   sFClass=class'BossLawProjSoundFix'
   //fCZRetryLoc=(Z=100.000000)
   fShouldCheck=True
   fCClass=class'BossLAWClusterFalk'
   fCheckExp=0.1
   fMaxStillT=3
   fSpawnDelay=0.1
   fNClusters=3
   fMaxTry=5
   FCDamageMulti=0.2
   FMultiScalingAdd=0.1
   FPlayerScalingAdd=0.05
   FNormalScalingVal=0.4
   FHardScalingVal=0.5
   FSuiScalingVal=0.6
   FHOEScalingVal=0.7
   fDoorDamagePerc=0.35 // in this weird universe this means 60%
}
