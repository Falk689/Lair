/* LIGHT ZED */

class FalkZombieStalker extends FalkZombieStalkerBase
   abstract;

#exec OBJ LOAD FILE=KFX.utx
#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=LairStalker_A.ukx
#exec OBJ LOAD FILE=KF_BaseStalker.uax

simulated function PostBeginPlay()
{
   CloakStalker();

   if (Role == ROLE_Authority && Level.Game.GameDifficulty >= 8.0)
      fStealthAttack = true;

   super.PostBeginPlay();
}

simulated function PostNetBeginPlay()
{
   local PlayerController PC;

   super.PostNetBeginPlay();

   if (Level.NetMode != NM_DedicatedServer)
   {
      PC = Level.GetLocalPlayerController();

      if (PC != none && PC.Pawn != none)
         LocalKFHumanPawn = KFHumanPawn(PC.Pawn);
   }
}

simulated event SetAnimAction(name NewAction)
{
   local int meleeAnimIndex;

   if (fFrozen)
      return;

   // select a new anim here so we know how many uncloak calls we have to mask
   if (NewAction == 'Claw')
   {
      meleeAnimIndex = Rand(2);
      NewAction      = MeleeAnims[meleeAnimIndex];
      CurrentDamtype = ZombieDamType[meleeAnimIndex];
   }

   if (fStealthAttack && NewAction == fMeleeAnims[0])
   {
      fUncloakCallsLeft = 1;
      super.SetAnimAction(NewAction);
      return;
   }

   else if (fStealthAttack && NewAction == fMeleeAnims[1])
   {
      fUncloakCallsLeft = 2;
      super.SetAnimAction(NewAction);
      return;
   }

   else if (fStealthAttack && NewAction == fMeleeAnims[2])
   {
      fUncloakCallsLeft = 1;
      super.SetAnimAction(NewAction);
      return;
   }

   else if (fStealthAttack && NewAction == 'Claw')
   {
      super.SetAnimAction(NewAction);
      return;
   }

   if (NewAction == fMeleeAnims[0] || NewAction == fMeleeAnims[1] || NewAction == fMeleeAnims[2])
      UncloakStalker();

   super.SetAnimAction(NewAction);
}

simulated function Tick(float DeltaTime)
{
   local float fViewDistanceMulti;

   Super.Tick(DeltaTime);

   if(Level.NetMode == NM_DedicatedServer)
      Return; // Servers aren't intrested in this info.

   if( bZapped )
   {
      // Make sure we check if we need to be cloaked as soon as the zap wears off
      NextCheckTime = Level.TimeSeconds;
   }

   else if( Level.TimeSeconds > NextCheckTime && Health > 0 )
   {
      NextCheckTime = Level.TimeSeconds + 0.5;

      // We can see invisible stalkers
      if (LocalKFHumanPawn != none && LocalKFHumanPawn.Health > 0 && LocalKFHumanPawn.ShowStalkers())
      {
         fViewDistanceMulti = LocalKFHumanPawn.GetStalkerViewDistanceMulti();

         //warn("Distance:"@VSizeSquared(Location - LocalKFHumanPawn.Location)@"- fViewDistanceMulti:"@fViewDistanceMulti);

         // the zed time skill allows it
         if (fViewDistanceMulti == -689 || VSizeSquared(Location - LocalKFHumanPawn.Location) < fViewDistanceMulti)
         {
            //warn("SKILL SPOTTED true:"@Level.TimeSeconds);
            bSpotted = True;
         }

         // nope, too far
         else
         {
            //warn("SKILL SPOTTED false:"@Level.TimeSeconds);
            bSpotted = false;
         }
      }

      // we can't see invisible stalkers
      else
         bSpotted = false;

      if (!bSpotted && !bCloaked && Skins[0] != Combiner'LairTextures_T.ZedsUpscale.StalkerCombinerFinal')
      {
         //warn("!bSpotted Uncloak:"@Level.TimeSeconds);
         UncloakStalker();
      }

      else if (Level.TimeSeconds - LastUncloakTime > 1.2)
      {
         // if we're uberbrite, turn down the light
         if (bSpotted && Skins[0] != Finalblend'LairTextures_T.ZedsUpscale.GhostOverlayFinalBlend')
         {
            //warn("SpottedCloak:"@Level.TimeSeconds);
            bUnlit = false;
            CloakStalker();
         }

         else if ( Skins[0] != Shader'KF_Specimens_Trip_T.stalker_invisible' )
         {
            //warn("!SpottedCloak:"@Level.TimeSeconds);
            CloakStalker();
         }
      }
   }
}

// Cloak Functions

simulated function CloakStalker()
{
   // No cloaking if zapped or frozen
   if (bZapped || fFrozen)
      return;

   if (bSpotted)
   {
      if (Level.NetMode == NM_DedicatedServer)
         return;

      //warn("Internal Spotted:"@Level.TimeSeconds);

      Skins[0] = Finalblend'LairTextures_T.ZedsUpscale.GhostOverlayFinalBlend';
      Skins[1] = Finalblend'LairTextures_T.ZedsUpscale.GhostOverlayFinalBlend';
      bUnlit = true;
      return;
   }

   if (!bDecapitated && !bCrispified) // No head, no cloak, honey.  updated :  Being charred means no cloak either :D
   {

      Visibility = 1;
      bCloaked = true;

      if (Level.NetMode == NM_DedicatedServer)
         Return;

      //warn("Internal NOT Spotted:"@Level.TimeSeconds);

      Skins[0] = Shader'KF_Specimens_Trip_T.stalker_invisible';
      Skins[1] = Shader'KF_Specimens_Trip_T.stalker_invisible';

      // Invisible - no shadow
      if(PlayerShadow != none)
         PlayerShadow.bShadowActive = false;

      if(RealTimeShadow != none)
         RealTimeShadow.Destroy();

      // Remove/disallow projectors on invisible people
      Projectors.Remove(0, Projectors.Length);
      bAcceptsProjectors = false;
      SetOverlayMaterial(Material'KFX.FBDecloakShader', 0.25, true);
   }
}

simulated function UncloakStalker()
{
   if (fUncloakCallsLeft > 0)
   {
      //warn("fUncloakCallsLeft"@fUncloakCallsLeft);
      fUncloakCallsLeft--;
      return;
   }

   if (bZapped)
      return;

   if (!bCrispified)
   {
      LastUncloakTime = Level.TimeSeconds;

      Visibility = default.Visibility;
      bCloaked = false;
      bUnlit = false;

      // 25% chance of our Enemy saying something about us being invisible
      if( Level.NetMode!=NM_Client && !KFGameType(Level.Game).bDidStalkerInvisibleMessage && FRand() < 0.25 && Controller.Enemy!=none &&
            PlayerController(Controller.Enemy.Controller)!=none )
      {
         PlayerController(Controller.Enemy.Controller).Speech('AUTO', 17, "");
         KFGameType(Level.Game).bDidStalkerInvisibleMessage = true;
      }

      if (Level.NetMode == NM_DedicatedServer)
         Return;

      if (Skins[0] != Combiner'LairTextures_T.ZedsUpscale.StalkerCombinerFinal')
      {
         Skins[1] = FinalBlend'KF_Specimens_Trip_T.stalker_fb';
         Skins[0] = Combiner'LairTextures_T.ZedsUpscale.StalkerCombinerFinal';

         if (PlayerShadow != none)
            PlayerShadow.bShadowActive = true;

         bAcceptsProjectors = true;

         SetOverlayMaterial(Material'KFX.FBDecloakShader', 0.25, true);
      }
   }
}

// Set the zed to the zapped behavior
simulated function SetZappedBehavior()
{
   super.SetZappedBehavior();

   bUnlit = false;

   // Handle setting the zed to uncloaked so the zapped overlay works properly
   if( Level.Netmode != NM_DedicatedServer )
   {
      Skins[1] = FinalBlend'KF_Specimens_Trip_T.stalker_fb';
      Skins[0] = Combiner'LairTextures_T.ZedsUpscale.StalkerCombinerFinal';

      if (PlayerShadow != none)
         PlayerShadow.bShadowActive = true;

      bAcceptsProjectors = true;
      SetOverlayMaterial(Material'LairTextures_T.ZedsUpscale.PlasmaShader', 999, true);
   }
}

// Turn off the zapped behavior
simulated function UnSetZappedBehavior()
{
   super.UnSetZappedBehavior();

   // Handle getting the zed back cloaked if need be
   if( Level.Netmode != NM_DedicatedServer )
   {
      NextCheckTime = Level.TimeSeconds;
      SetOverlayMaterial(None, 0.0f, true);
   }
}

// Overridden because we need to handle the overlays differently for zombies that can cloak
function SetZapped(float ZapAmount, Pawn Instigator)
{
   LastZapTime = Level.TimeSeconds;

   if( bZapped )
   {
      TotalZap = ZapThreshold;
      RemainingZap = ZapDuration;
   }
   else
   {
      TotalZap += ZapAmount;

      if( TotalZap >= ZapThreshold )
      {
         RemainingZap = ZapDuration;
         bZapped = true;
      }
   }
   ZappedBy = Instigator;
}

function RemoveHead()
{
   Super.RemoveHead();

   if (!bCrispified)
   {
      Skins[1] = FinalBlend'KF_Specimens_Trip_T.stalker_fb';
      Skins[0] = Combiner'LairTextures_T.ZedsUpscale.StalkerCombinerFinal';
   }
}

simulated function PlayDying(class<DamageType> DamageType, vector HitLoc)
{
   Super.PlayDying(DamageType,HitLoc);

   if(bUnlit)
      bUnlit=!bUnlit;

   LocalKFHumanPawn = none;

   if (!bCrispified)
   {
      Skins[1] = FinalBlend'KF_Specimens_Trip_T.stalker_fb';
      Skins[0] = Combiner'LairTextures_T.ZedsUpscale.StalkerCombinerFinal';
   }
}

// Give her the ability to spring.
function bool DoJump( bool bUpdating )
{
   if (fFrozen)
      return false;

   if ( !bIsCrouched && !bWantsToCrouch && ((Physics == PHYS_Walking) || (Physics == PHYS_Ladder) || (Physics == PHYS_Spider)) )
   {
      if (Role == ROLE_Authority)
      {
         if ( (Level.Game != None) && (Level.Game.GameDifficulty > 2) )
            MakeNoise(0.1 * Level.Game.GameDifficulty);
         if ( bCountJumps && (Inventory != None) )
            Inventory.OwnerEvent('Jumped');
      }
      if ( Physics == PHYS_Spider )
         Velocity = JumpZ * Floor;
      else if ( Physics == PHYS_Ladder )
         Velocity.Z = 0;
      else if ( bIsWalking )
      {
         Velocity.Z = Default.JumpZ;
         Velocity.X = (Default.JumpZ * 0.6);
      }
      else
      {
         Velocity.Z = JumpZ;
         Velocity.X = (JumpZ * 0.6);
      }
      if ( (Base != None) && !Base.bWorldGeometry )
      {
         Velocity.Z += Base.Velocity.Z;
         Velocity.X += Base.Velocity.X;
      }
      SetPhysics(PHYS_Falling);
      return true;
   }
   return false;
}

// Commando stalker one shot zed time skill
function TakeDamage(int Damage, Pawn instigatedBy, Vector hitlocation, Vector momentum, class<DamageType> damageType, optional int HitIndex)
{
   local KFPlayerReplicationInfo KFPRI;
   local Class<FVeterancyTypes> Vet;
   local FHumanPawn FP;

   FP = FHumanPawn(instigatedBy);

   // zed time commando skill
   if (FP != none)
   {
      KFPRI = KFPlayerReplicationInfo(FP.OwnerPRI);

      if (KFPRI != none)
      {
         Vet = Class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none && Vet.Static.ShouldOneShotStalkers(KFPRI, damageType))
            Damage = Max(Health, HeadHealth);
      }
   }

   Super.TakeDamage(Damage, instigatedBy, hitlocation, momentum, damageType, HitIndex);
}

// Shouldn't fight with our own and boss
function bool SameSpeciesAs(Pawn P)
{
   return (FalkZombieStalker(P) != none || Super.SameSpeciesAs(P));
}

static simulated function PreCacheMaterials(LevelInfo myLevel)
{
   myLevel.AddPrecacheMaterial(Combiner'LairTextures_T.ZedsUpscale.StalkerCombinerFinal');
   myLevel.AddPrecacheMaterial(Combiner'LairTextures_T.ZedsUpscale.StalkerEnvironmentCombiner');
   myLevel.AddPrecacheMaterial(Texture'LairTextures_T.ZedsUpscale.stalker_diff');
   myLevel.AddPrecacheMaterial(Texture'KF_Specimens_Trip_T.stalker_spec');
   myLevel.AddPrecacheMaterial(Material'KF_Specimens_Trip_T.stalker_invisible');
   myLevel.AddPrecacheMaterial(Combiner'KF_Specimens_Trip_T.StalkerCloakOpacity_cmb');
   myLevel.AddPrecacheMaterial(Material'KF_Specimens_Trip_T.StalkerCloakEnv_rot');
   myLevel.AddPrecacheMaterial(Material'KF_Specimens_Trip_T.stalker_opacity_osc');
   myLevel.AddPrecacheMaterial(Material'KFCharacters.StalkerSkin');
}

// return zed class
function FalkZedClass FalkGetZedClass()
{
   return Falk_Stalker;
}

// use our shader on crisped up zeds
simulated function ZombieCrispUp()
{
	bAshen = true;
	bCrispified = true;

	SetBurningBehavior();

	if (Level.NetMode == NM_DedicatedServer || class'GameInfo'.static.UseLowGore())
		Return;

	Skins[0]=Shader'LairTextures_T.ZedsUpscale.ZedBurnSkinShader';
	Skins[1]=Texture'LairTextures_T.CustomReskins.Transparency';
}

defaultproperties
{
   EventClasses(0)="Falk689ZedsFix.FZombieStalker_STANDARD"
   ControllerClass=Class'FMonsterController'
}
