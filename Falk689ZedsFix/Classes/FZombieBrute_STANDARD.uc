/* MEDIUM ZED */

class FZombieBrute_STANDARD extends FalkZombieBrute;

#exec OBJ LOAD FILE=LairBrute_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

defaultproperties
{
   MoanVoice=SoundGroup'WPC_Brute_S.Brute.Brute_Talk'
   MeleeAttackHitSound=SoundGroup'WPC_Brute_S.Brute.Brute_HitPlayer'
   JumpSound=SoundGroup'WPC_Brute_S.Brute.Brute_Jump'
   DetachedArmClass=Class'SeveredArmBrute'
   DetachedLegClass=Class'SeveredLegBrute'
   DetachedHeadClass=Class'SeveredHeadBrute'
   BurntDetachedArmClass=Class'BruteSeveredBurntArm'
   BurntDetachedLegClass=Class'BruteSeveredBurntLeg'
   BurntDetachedHeadClass=Class'BruteSeveredBurntHead'
   HitSound(0)=SoundGroup'WPC_Brute_S.Brute.Brute_Pain'
   DeathSound(0)=SoundGroup'WPC_Brute_S.Brute.Brute_Death'
   ChallengeSound(0)=SoundGroup'WPC_Brute_S.Brute.Brute_Challenge'
   ChallengeSound(1)=SoundGroup'WPC_Brute_S.Brute.Brute_Challenge'
   ChallengeSound(2)=SoundGroup'WPC_Brute_S.Brute.Brute_Challenge'
   ChallengeSound(3)=SoundGroup'WPC_Brute_S.Brute.Brute_Challenge'
   AmbientSound=Sound'WPC_Brute_S.Idle.FP_IdleLoop'
   Mesh=SkeletalMesh'LairBrute_A.Brute_Freak'
   Skins(0)=Combiner'LairTextures_T.ZedsUpscale.BruteCombinerFinal'
}
