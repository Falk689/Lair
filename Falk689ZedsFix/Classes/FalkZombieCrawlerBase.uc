/* LIGHT ZED */

class FalkZombieCrawlerBase extends FalkMonster
   abstract;

#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=LairCrawler_A.ukx

var()      float PounceSpeed;
var        bool  bPouncing;
var        bool  fPouncing;                  // used for the controller


var(Anims) name MeleeAirAnims[3];            // Attack anims for when flying through the air

var        float fNormalPounceCooldown;      // pounce cooldown at normal difficulties
var        float fBloodbathPounceCooldown;   // pounce cooldown at bloodbath
var        float fCurrentPounceCooldown;     // currently used pounce cooldown

replication
{
   reliable if (Role == ROLE_Authority)
      bPouncing, fCurrentPounceCooldown;
}

event Landed(vector HitNormal)
{
   super.Landed(HitNormal);
}

event Bump(actor Other)
{
   if(bPouncing && KFHumanPawn(Other)!=none )
   {
      KFHumanPawn(Other).TakeDamage(((MeleeDamage - (MeleeDamage * 0.05)) + (MeleeDamage * (FRand() * 0.1))), self ,self.Location,self.velocity, class 'KFmod.ZombieMeleeDamage');
      if (KFHumanPawn(Other).Health <=0)
      {
         KFHumanPawn(Other).SpawnGibs(self.rotation, 1);
      }
      //After impact, there'll be no momentum for further bumps
      bPouncing=false;
   }
}

// Blend his attacks so he can hit you in mid air.
simulated function int DoAnimAction(name AnimName)
{
   if (AnimName=='InAir_Attack1' || AnimName=='InAir_Attack2')
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;
      AnimBlendParams(1, 1.0, 0.0,, FireRootBone);
      PlayAnim(AnimName,, 0.0, 1);
      return 1;
   }

   if (AnimName=='HitF')
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;
      AnimBlendParams(1, 1.0, 0.0,, NeckBone);
      PlayAnim(AnimName,, 0.0, 1);
      return 1;
   }

   if (AnimName=='ZombieSpring')
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;
      PlayAnim(AnimName,,0.02);
      return 0;
   }

   return Super.DoAnimAction(AnimName);
}

simulated event SetAnimAction(name NewAction)
{
   local int meleeAnimIndex;

   if( NewAction=='' )
      Return;
   if(NewAction == 'Claw')
   {
      meleeAnimIndex = Rand(2);
      if( Physics == PHYS_Falling )
      {
         NewAction = MeleeAirAnims[meleeAnimIndex];
      }
      else
      {
         NewAction = meleeAnims[meleeAnimIndex];
      }
      CurrentDamtype = ZombieDamType[meleeAnimIndex];
   }
   ExpectingChannel = DoAnimAction(NewAction);

   if( AnimNeedsWait(NewAction) )
   {
      bWaitForAnim = true;
   }

   if( Level.NetMode!=NM_Client )
   {
      AnimAction = NewAction;
      bResetAnimAct = True;
      ResetAnimActTime = Level.TimeSeconds+0.3;
   }
}

// The animation is full body and should set the bWaitForAnim flag
simulated function bool AnimNeedsWait(name TestAnim)
{
   if( TestAnim == 'ZombieSpring' || TestAnim == 'DoorBash' )
   {
      return true;
   }

   return false;
}

defaultproperties
{
   PounceSpeed=330.000000
   MeleeAirAnims(0)="InAir_Attack1"
   MeleeAirAnims(1)="InAir_Attack2"
   MeleeAnims(0)="ZombieLeapAttack"
   MeleeAnims(1)="ZombieLeapAttack2"
   HitAnims(1)="HitF"
   HitAnims(2)="HitF"
   KFHitFront="HitF"
   KFHitBack="HitF"
   KFHitLeft="HitF"
   KFHitRight="HitF"
   bHarpoonToBodyStuns=False
   bHarpoonToHeadStuns=False
   bCannibal=True
   ZombieFlag=2
   damageForce=5000
   KFRagdollName="Crawler_Trip"
   CrispUpThreshhold=10
   Intelligence=BRAINS_Mammal
   SeveredArmAttachScale=0.800000
   SeveredLegAttachScale=0.850000
   SeveredHeadAttachScale=1.100000
   OnlineHeadshotOffset=(X=28.000000,Z=7.000000)
   OnlineHeadshotScale=1.200000
   IdleHeavyAnim="ZombieLeapIdle"
   IdleRifleAnim="ZombieLeapIdle"
   bCrawler=True
   GroundSpeed=140.000000
   WaterSpeed=140.000000
   JumpZ=350.000000
   HeadHeight=2.500000
   HeadScale=1.050000
   MenuName="Crawler"
   bDoTorsoTwist=False
   MovementAnims(0)="ZombieScuttle"
   MovementAnims(1)="ZombieScuttleB"
   MovementAnims(2)="ZombieScuttleL"
   MovementAnims(3)="ZombieScuttleR"
   WalkAnims(0)="ZombieScuttle"
   WalkAnims(1)="ZombieScuttleB"
   WalkAnims(2)="ZombieScuttleL"
   WalkAnims(3)="ZombieScuttleR"
   AirAnims(0)="ZombieSpring"
   AirAnims(1)="ZombieSpring"
   AirAnims(2)="ZombieSpring"
   AirAnims(3)="ZombieSpring"
   TakeoffAnims(0)="ZombieSpring"
   TakeoffAnims(1)="ZombieSpring"
   TakeoffAnims(2)="ZombieSpring"
   TakeoffAnims(3)="ZombieSpring"
   AirStillAnim="ZombieSpring"
   TakeoffStillAnim="ZombieLeapIdle"
   IdleCrouchAnim="ZombieLeapIdle"
   IdleWeaponAnim="ZombieLeapIdle"
   IdleRestAnim="ZombieLeapIdle"
   bOrientOnSlope=True
   DrawScale=1.100000
   PrePivot=(Z=0.000000)
   CollisionHeight=25.000000
   fHSKThreshold=0.25            // instakill at 18 damage at 1man normal
   HeadHealth=15.000000
   Health=70                  
   HealthMax=70
   MeleeDamage=6
   Mass=150.000000               // light zeds standard
   MotionDetectorThreat=0.250000 // two light zeds trigger a pipe, four trigger an ATM
   ScoringValue=10               // light zeds standard
   fDeathDrama=0.05              // light zeds standard
   PlayerCountHealthScale=0.0    // light zeds standard
   PlayerNumHeadHealthScale=0.0  // light zeds standard
   BleedOutDuration=3.0          // light zeds standard
   ZappedDamageMod=1.000000
   ZapResistanceScale=3.000000
   ZapThreshold=5.000000         // light zeds standard
   FZMultiFix=True
   fStunThresh=200               // light zeds standard
   bMeleeStunImmune=false
   bStunImmune=false
   fNormalPounceCooldown=3.0
   fBloodbathPounceCooldown=1.5
}
