class FalkCrawlerController extends FMonsterController;

var float NextPounceTime;
var bool bDoneSpottedCheck;
var bool fLanded;

state ZombieHunt
{
   event SeePlayer(Pawn SeenPlayer)
   {
      if ( !bDoneSpottedCheck && PlayerController(SeenPlayer.Controller) != none )
      {
         // 25% chance of first player to see this Crawler saying something
         if ( !KFGameType(Level.Game).bDidSpottedCrawlerMessage && FRand() < 0.25 )
         {
            PlayerController(SeenPlayer.Controller).Speech('AUTO', 18, "");
            KFGameType(Level.Game).bDidSpottedCrawlerMessage = true;
         }

         bDoneSpottedCheck = true;
      }

      super.SeePlayer(SeenPlayer);
   }
}

function bool IsInPounceDist(actor PTarget)
{
   local vector DistVec;
   local float time;

   local float HeightMoved;
   local float EndHeight;

   //work out time needed to reach target

   DistVec = pawn.location - PTarget.location;
   DistVec.Z=0;

   time = vsize(DistVec)/FalkZombieCrawler(pawn).PounceSpeed;

   // vertical change in that time

   //assumes downward grav only
   HeightMoved = Pawn.JumpZ*time + 0.5*pawn.PhysicsVolume.Gravity.z*time*time;

   EndHeight = pawn.Location.z +HeightMoved;

   //log(Vsize(Pawn.Location - PTarget.Location));


   if((abs(EndHeight - PTarget.Location.Z) < Pawn.CollisionHeight + PTarget.CollisionHeight) &&
         VSize(pawn.Location - PTarget.Location) < KFMonster(pawn).MeleeRange * 5)
      return true;
   else
      return false;
}

function bool FireWeaponAt(Actor A)
{
   local vector aFacing,aToB;
   local float RelativeDir;
   local FalkZombieCrawler fCrawler;

   if (A == None)
      A = Enemy;

   if ((A == None) || (Focus != A))
      return false;

   if (CanAttack(A))
   {
      Target = A;
      Monster(Pawn).RangedAttack(Target);
   }

   else
   {
      if (fLanded && NextPounceTime < Level.TimeSeconds)
      {
         aFacing     = Normal(Vector(Pawn.Rotation));
         aToB        = A.Location-Pawn.Location;
         RelativeDir = aFacing dot aToB;

         if (RelativeDir > 0.85)
         {
            //Facing enemy
            if (IsInPounceDist(A))
            {
               fCrawler = FalkZombieCrawler(Pawn);

               // might as well jump (Jump!)
               if (fCrawler != None && fCrawler.DoPounce())
                  fLanded     = false;
            }
         }
      }
   }
   return false;
}

function bool NotifyLanded(vector HitNormal)
{
   if (!fLanded)
   {
      FNotifyLanded();
      GotoState('hunting');
      return false;
   }

   fLanded           = true;
   return super.NotifyLanded(HitNormal);
}

// called on landing to set my variables
function FNotifyLanded()
{
   local FalkZombieCrawler fCrawler;

   if (fLanded)
      return;

   fCrawler = FalkZombieCrawler(pawn);

   if (fCrawler != None)
      NextPounceTime = Level.TimeSeconds + fCrawler.fCurrentPounceCooldown;

   else
      NextPounceTime = Level.TimeSeconds + 3.0;

   fLanded        = true;
}

// no animation, just hunt them
function fUnfreezeFix()
{
   GotoState('hunting');
}

defaultproperties
{
   fLanded=true
}
