/* MEDIUM ZED */

class FalkZombieHusk extends FalkZombieHuskBase
   abstract;


// don't do this
function FStairFixJump()
{
}

function DoorAttack(Actor A)
{
    if (bShotAnim || Physics == PHYS_Swimming)
        return;

    else if (A != None)
    {
        bShotAnim = true;
        SetAnimAction('DoorBash');
        GotoState('DoorBashing');
    }
}

// bloodbath skill start and husk damage immunity
function TakeDamage(int Damage, Pawn instigatedBy, Vector hitlocation, Vector momentum, class<DamageType> damageType, optional int HitIndex)
{
    // no damage from ourselves or other husks
    if (FalkZombieHusk(instigatedBy) != none)
        return;

    Super.TakeDamage(Damage, instigatedBy, hitlocation, momentum, damageType, HitIndex);

    // bloodbath skill start
    if (fBloodbathSkillState == BBS_Inactive && Level.Game.GameDifficulty >= 8.0 && Health <= HealthMax * fBloodbathSkillHealthMulti)
    {
        if (LastFireProjectileTime + fBloodbathSkillMeleeCooldown > Level.TimeSeconds)
        {
            fCurBloodbathSkillMeleeCooldown = fBloodbathSkillMeleeCooldown - (Level.TimeSeconds - LastFireProjectileTime);
            fBloodbathSkillState            = BBS_Active;
            fBloodbathSkillActivationTime   = Level.TimeSeconds;
            fNextFireProjectileTime         = NextFireProjectileTime;
            NextFireProjectileTime          = 0;
            //warn("SKILL ACTIVE MID FIRE"@Level.TimeSeconds@"COOLDOWN"@fCurBloodbathSkillMeleeCooldown);
        }

        else
        {
            fBloodbathSkillState          = BBS_Started;
            //warn("SKILL STARTED"@Level.TimeSeconds);
        }
    }
}

// handle attacks and bloodbath skill end
function RangedAttack(Actor A)
{
   if (bShotAnim || bZapped || fFrozen)
      return;

   if (Physics == PHYS_Swimming && (bDecapitated || fMeleeHits < fMaxMeleeHits) && (fBloodbathSkillState == BBS_Inactive || fBloodbathSkillState == BBS_Used))
   {
      fMeleeHits++;
      SetAnimAction('Claw');
      bShotAnim = true;
      fLastMeleeTime = Level.TimeSeconds;
   }

   else if ((bDecapitated || fMeleeHits < fMaxMeleeHits) && (fBloodbathSkillState == BBS_Inactive || fBloodbathSkillState == BBS_Used) && VSize(A.Location - Location) < MeleeRange + CollisionRadius + A.CollisionRadius)
   {
      fMeleeHits++;
      bShotAnim = true;
      fLastMeleeTime = Level.TimeSeconds;
      SetAnimAction('Claw');
      Controller.bPreparingMove = true;
      Acceleration = vect(0,0,0);
   }

   else if ((KFDoorMover(A) != none ||
            (!Region.Zone.bDistanceFog && VSize(A.Location-Location) <= 65535) ||
            (Region.Zone.bDistanceFog && VSizeSquared(A.Location-Location) < (Square(Region.Zone.DistanceFogEnd) * 0.8)))  // Make him come out of the fog a bit
            && !bDecapitated)
   {
      bShotAnim                 = true;
      fMeleeHits                = 0;
      SetAnimAction('ShootBurns');
      Controller.bPreparingMove = true;
      Acceleration              = vect(0,0,0);
      LastFireProjectileTime    = Level.TimeSeconds;

      // the skill was started during a takedamage, activate it now
      if (fBloodbathSkillState == BBS_Started)
      {
         fCurBloodbathSkillMeleeCooldown = fBloodbathSkillMeleeCooldown;
         fBloodbathSkillState            = BBS_Active;
         fBloodbathSkillActivationTime   = Level.TimeSeconds;
         fNextFireProjectileTime         = Level.TimeSeconds + ProjectileFireInterval + FRand() * 2.0;
         NextFireProjectileTime          = 0;
         //warn("SKILL ACTIVE NORMAL"@Level.TimeSeconds);
      }

      // we managed to trigger the skill, stop
      else if (fBloodbathSkillState == BBS_Active)
      {
         fBloodbathSkillState          = BBS_Used;
         NextFireProjectileTime        = Level.TimeSeconds + ProjectileFireInterval + FRand() * 2.0;
         fBloodbathSkillActivationTime = 0;
         //warn("SKILL USED"@Level.TimeSeconds);
      }

      // normal refire times
      else
      {
         NextFireProjectileTime        = Level.TimeSeconds + ProjectileFireInterval + FRand() * 2.0;
         fBloodbathSkillActivationTime = 0;
      }
   }
}


simulated event SetAnimAction(name NewAction)
{
   local int meleeAnimIndex;
   local bool bWantsToAttackAndMove;

   if (NewAction == '' || fFrozen)
      return;

   if (NewAction == 'Claw')
   {
      meleeAnimIndex = Rand(3);
      NewAction = meleeAnims[meleeAnimIndex];
      CurrentDamtype = ZombieDamType[meleeAnimIndex];
   }

   else if (NewAction == 'DoorBash')
      CurrentDamtype = ZombieDamType[Rand(3)];

   ExpectingChannel = DoAnimAction(NewAction);

   if (!bWantsToAttackAndMove && AnimNeedsWait(NewAction))
      bWaitForAnim = true;

   else
      bWaitForAnim = false;

   if (Level.NetMode != NM_Client)
   {
      AnimAction = NewAction;
      bResetAnimAct = True;
      ResetAnimActTime = Level.TimeSeconds+0.3;
   }
}


// spawn a falk projectile
function SpawnTwoShots()
{
   local vector X,Y,Z, FireStart;
   local rotator FireRotation;

   if (bZapped || fFrozen)
      return;

   GetAxes(Rotation,X,Y,Z);
   FireStart = GetBoneCoords('Barrel').Origin;
   huskFireProjClass = Class'HuskFireProjectileFalk';

   if (!SavedFireProperties.bInitialized)
   {
      SavedFireProperties.AmmoClass = Class'SkaarjAmmo';
      SavedFireProperties.ProjectileClass = HuskFireProjClass;
      SavedFireProperties.WarnTargetPct = 1;
      SavedFireProperties.MaxRange = 65535;
      SavedFireProperties.bTossed = False;
      SavedFireProperties.bTrySplash = true;
      SavedFireProperties.bLeadTarget = True;
      SavedFireProperties.bInstantHit = False;
      SavedFireProperties.bInitialized = True;
   }

   // Turn off extra collision before spawning the projectile, otherwise spawn fails
   ToggleAuxCollision(false);

   if (Controller != none)
      FireRotation = Controller.AdjustAim(SavedFireProperties,FireStart,600);

   else
      FireRotation = Rotation;

   /*foreach DynamicActors(class'KFMonsterController', KFMonstControl)
   {
      if (KFMonstControl != Controller)
      {
         if (PointDistToLine(KFMonstControl.Pawn.Location, vector(FireRotation), FireStart) < 75)
            KFMonstControl.GetOutOfTheWayOfShot(vector(FireRotation),FireStart);
      }
   }*/

   BulletID++;
   
   if (BulletID > 10)
      BulletID = 0;

   class<HuskFireProjectileFalk>(huskFireProjClass).Default.BulletID = BulletID;
   Spawn(HuskFireProjClass,,,FireStart,FireRotation);


   // Turn extra collision back on
   ToggleAuxCollision(true);
}

// Get the closest point along a line to another point
simulated function float PointDistToLine(vector Point, vector Line, vector Origin, optional out vector OutClosestPoint)
{
   local vector SafeDir;

   SafeDir = Normal(Line);
   OutClosestPoint = Origin + (SafeDir * ((Point-Origin) dot SafeDir));
   return VSize(OutClosestPoint-Point);
}


// Hack to force animation updates on the server for the husk if he is relevant to someone
simulated function Tick(float deltatime)
{
   Super.tick(deltatime);

   if (Health > 0 && Level.NetMode != NM_Client && Level.NetMode != NM_Standalone)
   {
      if ((Level.TimeSeconds-LastSeenOrRelevantTime) < 1.0)
         bForceSkelUpdate=true;

      else
         bForceSkelUpdate=false;
   }

   // reset melee hits if we don't take action for a while
   if (fMeleeHits > 0 && Level.TimeSeconds > fLastMeleeTime + fMeleeHitReset)
      fMeleeHits = 0;

   // we failed to trigger the skill in time, better luck next time
   if (fBloodbathSkillState == BBS_Active && fBloodbathSkillActivationTime + fCurBloodbathSkillMeleeCooldown < Level.TimeSeconds)
   {
      fBloodbathSkillState          = BBS_Started;
      NextFireProjectileTime        = fNextFireProjectileTime;
      fBloodbathSkillActivationTime = 0;
      //warn("SKILL RESET"@Level.TimeSeconds);
   }
}

// can't fire at doors anymore
function RemoveHead()
{
   bCanDistanceAttackDoors = False;
   Super.RemoveHead();
}


function PlayHit(float Damage, Pawn InstigatedBy, vector HitLocation, class<DamageType> damageType, vector Momentum, optional int HitIdx)
{
   local Vector HitNormal;
   local Vector HitRay;
   local Name HitBone;
   local float HitBoneDist;
   local PlayerController PC;
   local bool bShowEffects, bRecentHit;
   local ProjectileBloodSplat BloodHit;
   local rotator SplatRot;
   local KFPlayerReplicationInfo   KFPRI;
   local class<FVeterancyTypes>    Vet;

   if (class<DamTypeFreezerGun>(damageType) != none) // don't animate when hit by a freeze nade
      return;


   bRecentHit = Level.TimeSeconds - LastPainTime < 0.2;

   LastDamageAmount = Damage;

   // Call the modified version of the original Pawn playhit
   OldPlayHit(Damage, InstigatedBy, HitLocation, DamageType,Momentum);

   if (Damage <= 0)
      return;

   // artillerist zed time bonus, don't stun if we're freezing
   if (class<DamTypeSW76Falk>(damageType) != none)
   {
      if (instigatedBy != none)
      {
         KFPRI = KFPlayerReplicationInfo(instigatedBy.PlayerReplicationInfo);

         if (KFPRI != none)
         {
            Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

            if (Vet != none && Vet.Static.ArtilleristZedTimeBonus(KFPRI))
               return;
         }
      }
   }

   // stun based on fixed or relative (to HealthMax) amount of damage, implemented stun by husk gun and throwing knife
	if (Health > 0 && (HitIdx == -689 || class<DamTypeHuskGunStrongFalk>(damageType) != none || ((HitIdx != 689 || class<KFWeaponDamageType>(damageType) == none || !class<KFWeaponDamageType>(damageType).default.bIsPowerWeapon) && (Damage > fUsedThresh || Damage > HealthMax * fStunMult))))
		FlipOver();

   PC = PlayerController(Controller);
   bShowEffects = ((Level.NetMode != NM_Standalone) || (Level.TimeSeconds - LastRenderTime < 2.5)
         || ((InstigatedBy != None) && (PlayerController(InstigatedBy.Controller) != None))
         || (PC != None));

   if (!bShowEffects)
      return;

   if (BurnDown > 0 && !bBurnified)
   {
      bBurnified = true;
   }

   HitRay = vect(0,0,0);
   if (InstigatedBy != None)
      HitRay = Normal(HitLocation-(InstigatedBy.Location+(vect(0,0,1)*InstigatedBy.EyeHeight)));

   if (DamageType.default.bLocationalHit)
   {
      CalcHitLoc(HitLocation, HitRay, HitBone, HitBoneDist);

      // Do a zapped effect is someone shoots us and we're zapped to help show that the zed is taking more damage
      if (bZapped && DamageType.name != 'DamTypeZEDGun')
      {
         PlaySound(class'ZedGunProjectile'.default.ExplosionSound,,class'ZedGunProjectile'.default.ExplosionSoundVolume);
         Spawn(class'ZedGunProjectile'.default.ExplosionEmitter,,,HitLocation + HitNormal*20,rotator(HitNormal));
      }
   }
   else
   {
      HitLocation = Location;
      HitBone = FireRootBone;
      HitBoneDist = 0.0f;
   }

   if (DamageType.default.bAlwaysSevers && DamageType.default.bSpecial)
      HitBone = 'head';

   if (InstigatedBy != None)
      HitNormal = Normal(Normal(InstigatedBy.Location-HitLocation) + VRand() * 0.2 + vect(0,0,2.8));
   else
      HitNormal = Normal(Vect(0,0,1) + VRand() * 0.2 + vect(0,0,2.8));

   //log("HitLocation "$Hitlocation);

   if (DamageType.Default.bCausesBlood && (!bRecentHit || (bRecentHit && (FRand() > 0.8))))
   {
      if (!class'GameInfo'.static.NoBlood() && !class'GameInfo'.static.UseLowGore())
      {
         if (Momentum != vect(0,0,0))
            SplatRot = rotator(Normal(Momentum));
         else
         {
            if (InstigatedBy != None)
               SplatRot = rotator(Normal(Location - InstigatedBy.Location));
            else
               SplatRot = rotator(Normal(Location - HitLocation));
         }

         BloodHit = Spawn(ProjectileBloodSplatClass,InstigatedBy,, HitLocation, SplatRot);
      }
   }

   if (InstigatedBy != none && InstigatedBy.PlayerReplicationInfo != none &&
         KFSteamStatsAndAchievements(InstigatedBy.PlayerReplicationInfo.SteamStatsAndAchievements) != none &&
         Health <= 0 && Damage > DamageType.default.HumanObliterationThreshhold && Damage != 1000 && (!bDecapitated || bPlayBrainSplash))
   {
      KFSteamStatsAndAchievements(InstigatedBy.PlayerReplicationInfo.SteamStatsAndAchievements).AddGibKill(class<DamTypeM79Grenade>(damageType) != none);
   }

   DoDamageFX(HitBone, Damage, DamageType, Rotator(HitNormal));

   if (DamageType.default.DamageOverlayMaterial != None && Damage > 0) // additional check in case shield absorbed
      SetOverlayMaterial(DamageType.default.DamageOverlayMaterial, DamageType.default.DamageOverlayTime, false);
}

// Shouldn't fight with our own and boss
function bool SameSpeciesAs(Pawn P)
{
   return (FalkZombieHusk(P) != none || Super.SameSpeciesAs(P)); 
}

// no burning behavior with fire, we are fire
simulated function SetBurningBehavior()
{
   if (!bZapped && !bHarpoonStunned)
      return;

   Super.SetBurningBehavior();
}

static simulated function PreCacheMaterials(LevelInfo myLevel)
{
   myLevel.AddPrecacheMaterial(Texture'KF_Specimens_Trip_T_Two.burns_diff');
   myLevel.AddPrecacheMaterial(Texture'KF_Specimens_Trip_T_Two.burns_emissive_mask');
   myLevel.AddPrecacheMaterial(Combiner'KF_Specimens_Trip_T_Two.burns_energy_cmb');
   myLevel.AddPrecacheMaterial(Combiner'KF_Specimens_Trip_T_Two.burns_env_cmb');
   myLevel.AddPrecacheMaterial(Combiner'KF_Specimens_Trip_T_Two.burns_fire_cmb');
   myLevel.AddPrecacheMaterial(Material'KF_Specimens_Trip_T_Two.burns_shdr');
   myLevel.AddPrecacheMaterial(Combiner'KF_Specimens_Trip_T_Two.burns_cmb');
}

// return zed class
function FalkZedClass FalkGetZedClass()
{
   return Falk_Husk;
}

// spawn the special arm when hit on the cannon
simulated function ProcessHitFX()
{
   local Coords boneCoords;
   local class<xEmitter> HitEffects[4];
   local int i,j;
   local float GibPerterbation;

   if( (Level.NetMode == NM_DedicatedServer) || bSkeletized || (Mesh == SkeletonMesh))
   {
      SimHitFxTicker = HitFxTicker;
      return;
   }

   for ( SimHitFxTicker = SimHitFxTicker; SimHitFxTicker != HitFxTicker; SimHitFxTicker = (SimHitFxTicker + 1) % ArrayCount(HitFX) )
   {
      j++;
      if ( j > 30 )
      {
         SimHitFxTicker = HitFxTicker;
         return;
      }

      if( (HitFX[SimHitFxTicker].damtype == None) || (Level.bDropDetail && (Level.TimeSeconds - LastRenderTime > 3) && !IsHumanControlled()) )
         continue;

      //log("Processing effects for damtype "$HitFX[SimHitFxTicker].damtype);

      if( HitFX[SimHitFxTicker].bone == 'obliterate' && !class'GameInfo'.static.UseLowGore())
      {
         SpawnGibs( HitFX[SimHitFxTicker].rotDir, 1);
         bGibbed = true;
         // Wait a tick on a listen server so the obliteration can replicate before the pawn is destroyed
         if( Level.NetMode == NM_ListenServer )
         {
            bDestroyNextTick = true;
            TimeSetDestroyNextTickTime = Level.TimeSeconds;
         }
         else
         {
            Destroy();
         }
         return;
      }

      boneCoords = GetBoneCoords( HitFX[SimHitFxTicker].bone );

      if ( !Level.bDropDetail && !class'GameInfo'.static.NoBlood() && !bSkeletized && !class'GameInfo'.static.UseLowGore() )
      {
         //AttachEmitterEffect( BleedingEmitterClass, HitFX[SimHitFxTicker].bone, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir );

         HitFX[SimHitFxTicker].damtype.static.GetHitEffects( HitEffects, Health );

         if( !PhysicsVolume.bWaterVolume ) // don't attach effects under water
         {
            for( i = 0; i < ArrayCount(HitEffects); i++ )
            {
               if( HitEffects[i] == None )
                  continue;

               AttachEffect( HitEffects[i], HitFX[SimHitFxTicker].bone, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir );
            }
         }
      }
      if ( class'GameInfo'.static.UseLowGore() )
         HitFX[SimHitFxTicker].bSever = false;

      if( HitFX[SimHitFxTicker].bSever )
      {
         GibPerterbation = HitFX[SimHitFxTicker].damtype.default.GibPerterbation;

         switch( HitFX[SimHitFxTicker].bone )
         {
            case 'obliterate':
               break;

            case LeftThighBone:
               if( !bLeftLegGibbed )
               {
                  if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
                     SpawnSeveredGiblet(BurntDetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  else
                     SpawnSeveredGiblet(DetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  KFSpawnGiblet( class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  bLeftLegGibbed=true;
               }
               break;

            case RightThighBone:
               if( !bRightLegGibbed )
               {
                  if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
                     SpawnSeveredGiblet(BurntDetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  else
                     SpawnSeveredGiblet(DetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  KFSpawnGiblet( class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  bRightLegGibbed=true;
               }
               break;

            case LeftFArmBone:
               if( !bLeftArmGibbed )
               {
                  if (bCrispified && BurntDetachedArmClass != Class'KFChar.SeveredArmClot')
                     SpawnSeveredGiblet(BurntDetachedArmClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  else
                     SpawnSeveredGiblet(DetachedArmClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  KFSpawnGiblet( class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );;
                  bLeftArmGibbed=true;
               }
               break;

            case RightFArmBone:
               if( !bRightArmGibbed )
               {
                  if (bCrispified && BurntDetachedSpecialArmClass != Class'KFChar.SeveredArmClot')
                     SpawnSeveredGiblet(BurntDetachedSpecialArmClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  else
                     SpawnSeveredGiblet(DetachedSpecialArmClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  KFSpawnGiblet( class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  bRightArmGibbed=true;
               }
               break;

            case 'head':
               if( !bHeadGibbed )
               {
                  if ( HitFX[SimHitFxTicker].damtype == class'DamTypeDecapitation' )
                  {
                     DecapFX( boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false);
                  }
                  else if( HitFX[SimHitFxTicker].damtype == class'DamTypeProjectileDecap' )
                  {
                     DecapFX( boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false, true);
                  }
                  else if( HitFX[SimHitFxTicker].damtype == class'DamTypeMeleeDecapitation' )
                  {
                     DecapFX( boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, true);
                  }

                  bHeadGibbed=true;
               }
               break;
         }


         if( HitFX[SimHitFXTicker].bone != 'Spine' && HitFX[SimHitFXTicker].bone != FireRootBone &&
               HitFX[SimHitFXTicker].bone != 'head' && Health <=0 )
            HideBone(HitFX[SimHitFxTicker].bone);
      }
   }
}

defaultproperties
{
   EventClasses(0)="Falk689ZedsFix.FZombieHusk_STANDARD"
   ControllerClass=Class'FalkHuskController'
}
