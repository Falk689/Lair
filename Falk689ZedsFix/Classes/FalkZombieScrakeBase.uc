/* HEAVY ZED */

class FalkZombieScrakeBase extends FalkMonster
   abstract;

#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=LairSounds_S.uax
#exec OBJ LOAD FILE=LairScrake_A.ukx

var(Sounds) sound   SawAttackLoopSound; // The sound for the saw revved up, looping
var(Sounds) sound   ChainSawOffSound;   // The sound of this zombie dieiajxxx69xoxoyng without a head
var(Sounds) sound   FScrakeTaunt;       // taunt sound, played in bloodbath before frustration rage

var         float   FTauntVolume;       // volume for the taunt
var         bool    bCharging;          // Scrake charges when his health gets low
var()       float   AttackChargeRate;   // Ratio to increase scrake movement speed when charging and attacking

// Exhaust effects
var()	class<VehicleExhaustEffect>	ExhaustEffectClass; // Effect class for the exhaust emitter
var()	VehicleExhaustEffect 		   ExhaustEffect;
var 	bool	                        bNoExhaustRespawn;

// rage levels, make him rage at less than HealthMax * multipliers
var  float                          fNormalRageMulti;
var  float                          fHardRageMulti;
var  float                          fSuicidalRageMulti;
var  float                          fHOERageMulti;
var  float                          fBBRageMulti;
var  float                          fCurrentRageMulti; // currently used multiplier, editing isn't needed

// bloodbath vars
var  float                          fMinFrustration;       // minimum frustration timer in seconds
var  float                          fRandFrustration;      // maximum frustration timer in seconds
var  float                          fFrustrationRageTime;  // computed frustration rage time
var  bool                           fRageSoundPlayed;      // have we played the frustration sound yet?

// flip over fix
var float                           fFlipOverRecoverTime; // when to recover from flipover state
var float                           fFlipOverAddTime;     // additional time to wait after a flipover to run

replication
{
   reliable if(Role == ROLE_Authority)
      bCharging, fCurrentRageMulti;
}

defaultproperties
{
   AttackChargeRate=2.500000
   ExhaustEffectClass=Class'KFMod.ChainsawExhaust'
   MeleeAnims(0)="SawZombieAttack1"
   MeleeAnims(1)="SawZombieAttack2"
   StunsRemaining=1
   bHarpoonToBodyStuns=False
   bHarpoonToHeadStuns=False
   DamageToMonsterScale=8.000000
   ZombieFlag=3
   MeleeDamage=16
   damageForce=-75000
   bFatAss=True
   KFRagdollName="Scrake_Trip"
   Intelligence=BRAINS_Mammal
   bUseExtendedCollision=True
   ColOffset=(Z=55.000000)
   ColRadius=29.000000
   ColHeight=18.000000
   SeveredArmAttachScale=1.100000
   SeveredLegAttachScale=1.100000
   PoundRageBumpDamScale=0.010000
   OnlineHeadshotOffset=(X=22.000000,Y=5.000000,Z=58.000000)
   OnlineHeadshotScale=1.500000
   IdleHeavyAnim="SawZombieIdle"
   IdleRifleAnim="SawZombieIdle"
   MeleeRange=40.000000
   GroundSpeed=85.000000
   WaterSpeed=85.000000
   HeadHeight=2.200000
   MenuName="Scrake"
   MovementAnims(0)="SawZombieWalk"
   MovementAnims(1)="SawZombieWalk"
   MovementAnims(2)="SawZombieWalk"
   MovementAnims(3)="SawZombieWalk"
   WalkAnims(0)="SawZombieWalk"
   WalkAnims(1)="SawZombieWalk"
   WalkAnims(2)="SawZombieWalk"
   WalkAnims(3)="SawZombieWalk"
   IdleCrouchAnim="SawZombieIdle"
   IdleWeaponAnim="SawZombieIdle"
   IdleRestAnim="SawZombieIdle"
   DrawScale=1.050000
   PrePivot=(Z=3.000000)
   SoundVolume=175
   SoundRadius=100.000000
   RotationRate=(Yaw=45000,Roll=0)
   Mass=500.000000               // heavy zeds standard
   MotionDetectorThreat=1.000000 // a single heavy zed will trigger both a pipe and an ATM
   HeadHealth=500.0              
   Health=1000                   
   HealthMax=1000                // should be 6557 at 12men hoe
   fHSKThreshold=0.65            // instakill at 650 damage at 1man normal
   fDeathDrama=0.08              // heavy zeds standard
   ScoringValue=40               // heavy zeds standard
   PlayerCountHealthScale=0.25   // heavy zeds standard
   PlayerNumHeadHealthScale=0.25 // heavy zeds standard
   BleedOutDuration=7.0          // heavy zeds standard
   ZappedDamageMod=1.000000
   ZapResistanceScale=3.000000
   ZapThreshold=20.000000         // heavy zeds standard
   ZapDuration=3.0                // heavy zeds standard
   fSFireMultiplier=1.25
   fWFireMultiplier=1.25
   fStunThresh=600               // heavy zeds standard
   bMeleeStunImmune=false
   bStunImmune=false
   fCurrentRageMulti=0.65        // leave this value to default
   fNormalRageMulti=0.65
   fHardRageMulti=0.70
   fSuicidalRageMulti=0.75
   fHOERageMulti=0.80
   fBBRageMulti=0.85
   fShouldRage=True
   fRageSpeed=2.9
   fNormalRageSpeed=2.9
   fHardRageSpeed=2.9
   fSuicidalRageSpeed=2.9
   fHOERageSpeed=2.9
   fRageSpeedCap=305.0
   fFlipOverAddTime=0.2
   fFlinchCooldown=3.0
   fFleshpoundRageImmune=True
   fMinFrustration=150
   fRandFrustration=60
   fFrustrationRageTime=-1
   FScrakeTaunt=Sound'LairSounds_S.Scrake.ScrakeFrustTaunt'
   FTauntVolume=2.0
   fDoorDamageStep=1
}
