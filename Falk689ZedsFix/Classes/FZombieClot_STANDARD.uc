/* LIGHT ZED */

#exec OBJ LOAD FILE=LairClot_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

class FZombieClot_STANDARD extends FalkZombieClot;

defaultproperties
{
   MoanVoice=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Talk'
   MeleeAttackHitSound=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_HitPlayer'
   JumpSound=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Jump'
   DetachedArmClass=Class'KFChar.SeveredArmClot'
   DetachedLegClass=Class'KFChar.SeveredLegClot'
   DetachedHeadClass=Class'KFChar.SeveredHeadClot'
   BurntDetachedHeadClass=Class'ClotSeveredBurntHead'
   BurntDetachedArmClass=Class'ClotSeveredBurntArm'
   BurntDetachedLegClass=Class'ClotSeveredBurntLeg'
   HitSound(0)=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Pain'
   DeathSound(0)=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Death'
   ChallengeSound(0)=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Challenge'
   ChallengeSound(1)=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Challenge'
   ChallengeSound(2)=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Challenge'
   ChallengeSound(3)=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Challenge'
   AmbientSound=Sound'KF_BaseClot.Clot_Idle1Loop'
   Mesh=SkeletalMesh'LairClot_A.CLOT_Freak'
   Skins(0)=Combiner'LairTextures_T.ZedsUpscale.ClotCombinerFinal'
}
