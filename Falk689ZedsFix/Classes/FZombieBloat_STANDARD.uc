/* MEDIUM ZED */

#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=LairBloat_A.ukx

class FZombieBloat_STANDARD extends FalkZombieBloat;

defaultproperties
{
   //BileExplosion=Class'KFMod.BileExplosion'
   //BileExplosionHeadless=Class'KFMod.BileExplosionHeadless'
   MoanVoice=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_Talk'
   MeleeAttackHitSound=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_HitPlayer'
   JumpSound=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_Jump'
   DetachedArmClass=Class'KFChar.SeveredArmBloat'
   DetachedLegClass=Class'KFChar.SeveredLegBloat'
   DetachedHeadClass=Class'KFChar.SeveredHeadBloat'
   BurntDetachedHeadClass=Class'BloatSeveredBurntHead'
   BurntDetachedLegClass=Class'BloatSeveredBurntLeg'
   HitSound(0)=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_Pain'
   DeathSound(0)=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_Death'
   ChallengeSound(0)=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_Challenge'
   ChallengeSound(1)=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_Challenge'
   ChallengeSound(2)=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_Challenge'
   ChallengeSound(3)=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_Challenge'
   AmbientSound=Sound'KF_BaseBloat.Bloat_Idle1Loop'
   Mesh=SkeletalMesh'LairBloat_A.Bloat_Freak'
   Skins(0)=Combiner'LairTextures_T.ZedsUpscale.BloatCombinerFinal'
}
