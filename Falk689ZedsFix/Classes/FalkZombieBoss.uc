class FalkZombieBoss extends FalkZombieBossBase
   abstract;

#exec OBJ LOAD FILE=KFPatch2.utx
#exec OBJ LOAD FILE=KF_Specimens_Trip_T.utx
#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=KF_BasePatriarch.uax
#exec OBJ LOAD FILE=LairSounds_S.uax

var BossHPNeedle CurrentNeedle;

// Last time we checked if a player was melee exploiting us
var float LastMeleeExploitCheckTime;
// Used to track what type of melee exploiters you have
var int NumLumberJacks;
var int NumNinjas;

// stuff for the zedtime on radial
var bool fZedTimeRadial;

// kill taunt stuff
var float  fTauntStateEndTime;   // when the state ended
var vector fTFocalPoint;         // where are we looking at while taunting
var byte   FShouldTaunt;         // used to start the taunt after the attack

// other stuff
var bool   FBlockStairsFix;      // if true, stairs fix doesn't happen
//var bool   fBlockRageAnimChange; // block rage anim change once
var bool   fEscapeToCharging;    // we're going from escape state directly to charging state

replication
{
	reliable if(Role == ROLE_Authority)
      fZedTimeRadial;//, fBlockRageAnimChange;
}

// Make the Boss's ambient scale higher, since there is only 1, doesn't matter if he's relevant almost all the time
simulated function CalcAmbientRelevancyScale()
{
   // Make the zed only relevant by thier ambient sound out to a range of 100 meters
   CustomAmbientRelevancyScale = 5000 / (100 * SoundRadius);
}

function vector ComputeTrajectoryByTime(vector StartPosition, vector EndPosition, float fTimeEnd)
{
   local vector NewVelocity;

   NewVelocity = Super.ComputeTrajectoryByTime(StartPosition, EndPosition, fTimeEnd);

   if (PhysicsVolume.IsA('KFPhysicsVolume') && StartPosition.Z < EndPosition.Z)
   {
      if (PhysicsVolume.Gravity.Z < class'PhysicsVolume'.default.Gravity.Z)
      {
         // Just checking mass to be extra-cautious.
         if (Mass > 900)
         {
            // Extra velocity boost to counter oversized mass weighing the boss down.
            NewVelocity.Z += 90;
         }
      }
   }
   return NewVelocity;
}

function ZombieMoan()
{
   if (!bShotAnim && FShouldTalk) // Do not moan while taunting
      Super.ZombieMoan();
}

// Be slightly more polite when knocked down
function PatriarchKnockDown()
{
   local float fSoundChance;

   fSoundChance = FRand();

   if (fSoundChance <= FKDSwearChance)
      PlaySound(Sound'KF_BasePatriarch.KnockedDown.Kev_KnockedDown2', SLOT_Talk, 2.0,true,500.0);

   else if (fSoundChance <= FKDSwearChance * 2)
      PlaySound(Sound'KF_BasePatriarch.KnockedDown.Kev_KnockedDown3', SLOT_Talk, 2.0,true,500.0);

   else if (fSoundChance <= FKDSwearChance * 3)
      PlaySound(Sound'KF_BasePatriarch.KnockedDown.Kev_KnockedDown4', SLOT_Talk, 2.0,true,500.0);

   else if (fSoundChance <= FKDSwearChance * 4)
      PlaySound(Sound'KF_BasePatriarch.KnockedDown.Kev_KnockedDown5', SLOT_Talk, 2.0,true,500.0);

   else if (fSoundChance <= FKDSwearChance * 5)
      PlaySound(Sound'KF_BasePatriarch.KnockedDown.Kev_KnockedDown6', SLOT_Talk, 2.0,true,500.0);

   else if (fSoundChance <= FKDSwearChance * 6)
      PlaySound(Sound'KF_BasePatriarch.KnockedDown.Kev_KnockedDown7', SLOT_Talk, 2.0,true,500.0);

   else
      PlaySound(Sound'KF_BasePatriarch.KnockedDown.Kev_KnockedDown1', SLOT_Talk, 2.0,true,500.0);
}

function PatriarchEntrance()
{
   PlaySound(SoundGroup'KF_EnemiesFinalSnd.Patriarch.Kev_Entrance', SLOT_Talk, 2.0,true,500.0);
}

function PatriarchVictory()
{
   local Falk689GameTypeBase Flk;

   if (Role == ROLE_Authority)
   {
      Flk = Falk689GameTypeBase(Level.Game);

      if (Flk != none)
      {
         // talk now
         if (!Flk.fFakeZedTime)
         {
            if (FShouldTaunt >= 3 && FTauntAlivePlayers > 0)
            {
               //warn("1 Patriarch Victory:"@Level.TimeSeconds@"FShouldTaunt:"@FShouldTaunt@"Alive Players:"@FTauntAlivePlayers);
               PlaySound(Sound'LairSounds_S.Patriarch.KillTaunt', SLOT_Talk, 2.0,true,500.0);
            }

            else if (FShouldTalk)
            {
               //warn("2 Patriarch Victory:"@Level.TimeSeconds@"FShouldTaunt:"@FShouldTaunt@"Alive Players:"@FTauntAlivePlayers);

               PlaySound(SoundGroup'KF_EnemiesFinalSnd.Patriarch.Kev_Victory', SLOT_Talk, 2.0,true,500.0);
               FShouldTalk = False;
            }

            FShouldTaunt = 0;
         }

         // delay this a bit since we're into a fake zed time
         else
         {
            //warn("FAKE ZED TIME:"@Level.TimeSeconds);
            FFakeZTTime  = Level.TimeSeconds;
            FShouldTaunt = 4;
         }
      }

      else
      {
         FShouldTaunt = 0;
      }
   }
}

function PatriarchMGPreFire()
{
   PlaySound(SoundGroup'KF_EnemiesFinalSnd.Patriarch.Kev_WarnGun', SLOT_Talk, 2.0,true,1000.0);
}

// Be slightly more family friendly while firing a rocket
function PatriarchMissilePreFire()
{
   if (FGetSyringeCount() > 1)
      PlaySound(Sound'LairSounds_S.Patriarch.TwoInThePipe', SLOT_Talk, 2.0,true,1000.0);

   else if (FRand() <= FRSwearChance)
      PlaySound(Sound'KF_BasePatriarch.WarnGun.Kev_Warn1', SLOT_Talk, 2.0,true,1000.0);

   else
      PlaySound(SoundGroup'KF_EnemiesFinalSnd.Patriarch.Kev_WarnRocket', SLOT_Talk, 2.0,true,1000.0);
}

// Taunt to use when doing the melee exploit radial attack
function PatriarchRadialTaunt()
{
   if( NumNinjas > 0 && NumNinjas > NumLumberJacks )
   {
      PlaySound(SoundGroup'KF_EnemiesFinalSnd.Patriarch.Kev_TauntNinja', SLOT_Talk, 2.0,true,500.0);
   }
   else if( NumLumberJacks > 0 && NumLumberJacks > NumNinjas )
   {
      PlaySound(SoundGroup'KF_EnemiesFinalSnd.Patriarch.Kev_TauntLumberJack', SLOT_Talk, 2.0,true,500.0);
   }
   else
   {
      PlaySound(SoundGroup'KF_EnemiesFinalSnd.Patriarch.Kev_TauntRadial', SLOT_Talk, 2.0,true,500.0);
   }
}

// Don't do this for the Patriarch
simulated function SetBurningBehavior(){}
simulated function UnSetBurningBehavior(){}

function bool CanGetOutOfWay()
{
   return false;
}


// actually start a chaingun fire, called by the tick on a slight delay
function FalkStartFireChaingun()
{
   local byte fChaingunBullets;
   bShotAnim       = false;
   FBlockStairsFix = true;

   AnimEnd(0);
   AnimEnd(1);
   AnimEnd(2);
   AnimEnd(3);

   //warn("Minigun F Start: "@Level.TimeSeconds);

   bShotAnim       = true;
   bWaitForAnim    = true;
   Acceleration    = vect(0,0,0);

   KFMonsterController(Controller).bUseFreezeHack = True;
   SetAnimAction('PreFireMG');

   Controller.GoToState('WaitForAnim');

   // scale chaingun bullets based on healing wave
   if (FGetSyringeCount() > 2)
      fChaingunBullets = FCBaseThree;

   else if (SyringeCount > 1)
      fChaingunBullets = FCBaseTwo;

   else if (SyringeCount > 0)
      fChaingunBullets = FCBaseOne;

   else
      fChaingunBullets = FCBaseZero;

   MGFireCounter =  Rand(FCRandom) + fChaingunBullets;
   FCStopMoving  = false;

   //warn("MGFireCounter:"@MGFireCounter@"Time:"@Level.TimeSeconds);

   GoToState('FireChaingun');
}


// actually start a missile fire, called by the tick on a slight delay
function FalkStartFireMissile()
{
   local vector Start;
   local int fMissilePrec;

   Start = GetBoneCoords('tip').Origin;

   if (!SavedFireProperties.bInitialized)
   {
      SavedFireProperties.AmmoClass       = class'LAWAmmo';
      SavedFireProperties.ProjectileClass = class'BossLAWProjFalk';
      SavedFireProperties.WarnTargetPct   = 0.15;
      SavedFireProperties.MaxRange        = 10000;
      SavedFireProperties.bTossed         = False;
      SavedFireProperties.bTrySplash      = False;
      SavedFireProperties.bLeadTarget     = True;
      SavedFireProperties.bInstantHit     = True;
      SavedFireProperties.bInitialized    = true;
   }

   // higher precision against doors
   if (KFDoorMover(Controller.Target) != none)
      fMissilePrec = FMissileDoorPrec;

   // normal precision
   else
   {
      fMissilePrec = FMissileZeroPrec;

      // scale precision based on syringe count
      if (FGetSyringeCount() > 2)
         fMissilePrec = FMissileThreePrec;

      else if (SyringeCount == 2)
         fMissilePrec = FMissileTwoPrec; 

      else if (SyringeCount == 1)
         fMissilePrec = FMissileOnePrec;
   }

   FStartMissileAim = AdjustAim(SavedFireProperties, Start, fMissilePrec);

   //warn("START AIM:"@FStartMissileAim@"TIME:"@Level.TimeSeconds);

   bShotAnim        = false;
   FBlockStairsFix  = true;

   AnimEnd(0);
   AnimEnd(1);
   AnimEnd(2);
   AnimEnd(3);

   bShotAnim      = true;
   bWaitForAnim   = true;
   Acceleration   = vect(0,0,0);

   KFMonsterController(Controller).bUseFreezeHack = True;

   SetAnimAction('PreFireMissile');
   Controller.GoToState('WaitForAnim');

   GoToState('FireMissile'); 
}

function FalkStartRadial()
{
   bShotAnim = false;

   AnimEnd(0);
   AnimEnd(1);
   AnimEnd(2);
   AnimEnd(3);

   FChaingunEndTime = 0;
   bShotAnim        = true;
   bWaitForAnim     = true;
   Acceleration     = vect(0,0,0);

   KFMonsterController(Controller).bUseFreezeHack = True;

   GoToState('RadialAttack'); 
}

function FalkStartHealing()
{
   bShotAnim = true;
   Acceleration = vect(0,0,0);
   SetAnimAction('Heal');
   KFMonsterController(Controller).bUseFreezeHack = True;
   Controller.GoToState('WaitForAnim');
   FHealEndTime = Level.TimeSeconds;

   GoToState('Healing');
}

simulated function Tick(float DeltaTime)
{
   local KFHumanPawn HP;
   local FHumanPawn  FHP;
   local PlayerController PC;
   local byte i;
   local float sWTCurTime;
   local Falk689GameTypeBase Flk;

   sWTCurTime = fWTCurTime + DeltaTime;
   Super.Tick(DeltaTime);

   // reset total zap after a while
   if (!bZapped && TotalZap > 0 && Level.TimeSeconds >= LastZapTime + fZapResetTime)
      TotalZap = 0;

   // pipe bomb damage and delayed state change
   if (Role == ROLE_Authority)
   {
      // reset damage against doors after a while
      if (FDoorDamageReset > 0)
      {
         FDoorDamageReset -= DeltaTime;

         if (FDoorDamageReset <= 0)
            FCurDoorDamagePerc = Default.FCurDoorDamagePerc;
      }

      // reduce the entrance fix
      if (FEntranceStartCFix > 0)
         FEntranceStartCFix -= DeltaTime;

      // the entrance animation is over, be mortal again
      else if (FBlockAllDamage)
      {
         //warn("Mortal:"@Level.TimeSeconds);
         FBlockAllDamage = False;
      }

      // after the entrance, start calling the fire
      else if (!bZapped && !fIsStunned && !fFrozen)
      {
         if (fBossAttackDelay > 0)
            fBossAttackDelay -= DeltaTime;

         else
         {
            //warn("Boss FalkFire:"@Level.TimeSeconds);

            fBossAttackDelay = Default.fBossAttackDelay;
            FalkFire();
         }
      }

      // delay patriarch taunt
      if (FShouldTaunt == 4 && FFakeZTTime + FFakeZTTDelay > Level.TimeSeconds)
         PatriarchVictory();

      // delayed state change
      if (FNextState != '' && FNextStateTime < Level.TimeSeconds)
      {
         if (!bZapped && !fFrozen)
         {
            if (FNextState == 'FireChaingun')
            {
               //warn("Start Chaingun Fire:"@Level.TimeSeconds);
               FalkStartFireChaingun();
            }

            else if (FNextState == 'FireMissile')
            {
               //warn("Start Missile Fire:"@Level.TimeSeconds);
               FalkStartFireMissile();
            }

            else if (FNextState == 'RadialAttack')
            {
               //warn("Start Radial:"@Level.TimeSeconds);
               FalkStartRadial();
            }

            else if (FNextState == 'Healing')
            {
               //warn("Start Delayed Healing:"@Level.TimeSeconds);
               FalkStartHealing();
            }

            else
            {
               //warn("Start Delayed State:"@FNextState@"-"@Level.TimeSeconds);
               GoToState(FNextState); 
            }
         }

         FNextState = '';
      }

      // pipe bomb damage stuff
      PipeBombDamageScale -= DeltaTime * 0.33;

      if (PipeBombDamageScale < 0)
         PipeBombDamageScale = 0;

      if (!IsInState('FireChaingun') && !IsInState('FireMissile') && !IsInState('RadialAttack') && !IsInState('Healing') && !IsInState('Escaping') && !IsInState('Charging') && !IsInState('InitialSneak'))
      {
         if (FFrustration < FCurMaxFrustration)
            FFrustration += DeltaTime;

         else if (ShouldChargeFromDamage())
         {
            FFrustration = 0;
            FCurMaxFrustration = FBaseFrustration + Rand(FAddFrustration);

            Flk = Falk689GameTypeBase(Level.Game);

            // charge only if there's at least one breathing player
            if (Flk != none && Flk.GetAlivePlayers() > 0 && FShouldTaunt == 0)
            {
               if (!FLockedChargeTgt && Controller != none && FalkBossController(Controller) != none)
               {
                  FLockedChargeTgt = True;
                  FalkBossController(Controller).FindNewEnemy(); 
               }

               //warn("Charging 1"@Level.TimeSeconds);
               GotoState('Charging');
            }
         }
      }

      if (FTauntAlivePlayers <= 0 && FShouldTaunt > 0)
      {
         //warn("Tick Reset:"@Level.TimeSeconds);
         FShouldTaunt = 0;
      }

      else if (!IsInState('RadialAttack') && !IsInState('FireChaingun') /*&& !IsInState('PreFireMissile')*/ && !IsInState('FireMissile') && !IsInState('Escaping') )
      {
         if (FShouldTaunt == 1)
         {
            //warn("Backup Endstate:"@Level.TimeSeconds);

            if (fTauntStateEndTime < Level.TimeSeconds)
               fTauntStateEndTime = Level.TimeSeconds;

            FShouldTaunt = 2;
         }

         // start the taunt after you fired your chaingun or rocket
         else if (!fFrozen && !bZapped && FShouldTaunt == 2 && Level.TimeSeconds > fTauntStateEndTime + FTauntDelay && Level.TimeSeconds > FMissileLaunchTime + FMissileTDelay + FTauntDelay)
         {
            //warn("Starting Actual Delayed Taunt:"@Level.TimeSeconds);
            FShouldTaunt = 3;
            FKillTaunt();
         }
      }

      // exploit fixes and stuff
      if (fFrozen || bZapped)
         FMCurCheck = 0;

      else if (FMCurCheck < FMExpCheck)
         FMCurCheck += DeltaTime;
      
      else 
      {
         FMCurCheck = 0;

         // check for special squad to spawn
         if ((!FBuddies && Health <= FHealLevel && KFGameType(Level.Game).FinalSquadNum == SyringeCount) || (FLastBuddies < SyringeCount))
         {
            FBuddies = True;
            FLastBuddies++;
            //warn("Tick Squad Spawn");
            KFGameType(Level.Game).AddBossBuddySquad();
         }

         // look for guys attacking us within 3 meters
         foreach DynamicActors(class'KFHumanPawn', HP)
         {
            if (HP != none)
            {
               PC = PlayerController(HP.Controller);

               if (PC != none)
               {
                  i = ExpIndex(PC);

                  if (VSize(HP.Location - Location) < 150)
                  {
                     fExpList[i].NTicks = Min(fExpList[i].NTicks + 1, FSTDudeCheck);
                     //log(HP@" is a bit too close since "@fExpList[i].Ticks@" ticks, max: "@FSTDudeCheck);

                     if (!IsInState('Escaping') && !IsInState('Charging') && !IsInState('RadialAttack') &&
                           fExpList[i].NTicks >= FSTDudeCheck && Level.TimeSeconds > FLastMCTime + MCCooldown && ShouldChargeFromDamage())
                     {
                        FLastMCTime = Level.TimeSeconds;
                        fExpList[i].NTicks = 0;

                        if (Controller != none)
                           Controller.Target = HP;

                        if (Health > FHealLevel || SyringeCount > 2)
                        {
                           //log("CHARGE");

                           if (Controller != none && FalkBossController(Controller) != none)
                           {
                              FalkBossController(Controller).fRandomEnemy = true;
                              FalkBossController(Controller).FindNewEnemy(); 
                           }

                           //warn("Charging 2"@Level.TimeSeconds);
                           GotoState('Charging');
                        }

                        else
                        {
                           //log("ESCAPE 1");
                           GotoState('Escaping');
                        }

                        break;
                     }
                  }

                  else
                  {
                     fExpList[i].NTicks = Max(fExpList[i].NTicks--, 0);
                     fExpList[i].MTicks = 0;
                  }
               }
            }
         }
      }

      if (!fFrozen && !bZapped && sWTCurTime >= fWTCheck && !IsInState('Escaping') && !IsInState('KnockDown') && !IsInState('FireMissile') && !IsInState('FireChaingun') && FNextState == '' && Health <= FHealLevel && SyringeCount < 3)
      {
         //log("ESCAPE 2");
         GotoState('Escaping');
      }

   }

   if(Level.NetMode == NM_DedicatedServer)
      Return; // Servers aren't intrested in this info.

   bSpecialCalcView = bIsBossView;

   if (bZapped)
   {
      // Make sure we check if we need to be cloaked as soon as the zap wears off
      LastCheckTimes = Level.TimeSeconds;
   }

   else if (bCloaked && Level.TimeSeconds > LastCheckTimes)
   {
      LastCheckTimes = Level.TimeSeconds + 0.5;

      ForEach DynamicActors(Class'FHumanPawn', FHP)
      {
         if (FHP.Health <= 0 || !FHP.VanillaShowStalkers())
            continue;

         // same shit as the stalker
         if (VSizeSquared(Location - FHP.Location) < FHP.VanillaGetStalkerViewDistanceMulti())
         {
            bSpotted = True;
            CloakBoss();
         }

         else
            bSpotted = false;
      }

      // if we're uberbrite, turn down the light... sorry wut?
      if (bSpotted && Skins[0] != Finalblend'LairTextures_T.ZedsUpscale.GhostOverlayFinalBlend')
      {
         //warn("C1");
         //bSpotted = False;
         bUnlit = false;
         CloakBoss();
      }

      else if (Skins[1] != Shader'KF_Specimens_Trip_T.patriarch_invisible' )
      {
         //warn("C2");
         CloakBoss();
      }
   }
}

simulated function CloakBoss()
{
   local Controller C;
   local int Index;

   // No cloaking if zapped or frozen
   if (bZapped || fFrozen)
      return;

   if (bSpotted)
   {
      //warn("Spotted");

      Visibility = 120;

      if (Level.NetMode == NM_DedicatedServer)
         Return;

      Skins[0] = Finalblend'LairTextures_T.ZedsUpscale.GhostOverlayFinalBlend';
      Skins[1] = Finalblend'LairTextures_T.ZedsUpscale.GhostOverlayFinalBlend';
      bUnlit = true;
      return;
   }

   Visibility = 1;
   bCloaked   = true;

   if (Level.NetMode!=NM_Client)
   {
      For (C=Level.ControllerList; C!=None; C=C.NextController)
      {
         if (C.bIsPlayer && C.Enemy==Self)
            C.Enemy = None; // Make bots lose sight with me.
      }
   }
   if (Level.NetMode == NM_DedicatedServer)
      Return;

   Skins[0] = Shader'KF_Specimens_Trip_T.patriarch_invisible_gun';
   Skins[1] = Shader'KF_Specimens_Trip_T.patriarch_invisible';

   // Invisible - no shadow
   if (PlayerShadow != none)
      PlayerShadow.bShadowActive = false;

   // Remove/disallow projectors on invisible people
   Projectors.Remove(0, Projectors.Length);
   bAcceptsProjectors = false;
   SetOverlayMaterial(FinalBlend'KF_Specimens_Trip_T.patriarch_fizzle_FB', 1.0, true);

   // Randomly send out a message about Patriarch going invisible(10% chance)
   if (FRand() < 0.10)
   {
      // Pick a random Player to say the message
      Index = Rand(Level.Game.NumPlayers);

      for ( C = Level.ControllerList; C != none; C = C.NextController )
      {
         if ( PlayerController(C) != none )
         {
            if ( Index == 0 )
            {
               PlayerController(C).Speech('AUTO', 8, "");
               break;
            }

            Index--;
         }
      }
   }
}

simulated function UnCloakBoss()
{
   //warn("UnCloakBoss Time"@Level.TimeSeconds);

   Visibility = default.Visibility;
   bCloaked = false;
   bSpotted = False;
   bUnlit = False;

   if (Level.NetMode==NM_DedicatedServer)
      Return;

   Skins = Default.Skins;

   if (PlayerShadow != none)
      PlayerShadow.bShadowActive = true;

   bAcceptsProjectors = true;

   if (fFrozen)
      return;

   if (bZapped)
      SetOverlayMaterial(Material'LairTextures_T.ZedsUpscale.PlasmaShader', 999, true);

   else
      SetOverlayMaterial(none, 0.0, true);
}

// Set the zed to the zapped behavior
simulated function SetZappedBehavior()
{
   FShouldTaunt = 0;
   FNextState   = '';

   if (fFrozen)
      return;

   // don't be zapped while fleeing
   if (FSyringeCount < 3 && Health <= FHealLevel)
   {
      bZapped = False;
      return;
   }

   super.SetZappedBehavior();

   // Handle setting the zed to uncloaked so the zapped overlay works properly
   if (Level.Netmode != NM_DedicatedServer)
   {
      bUnlit = false;
      Skins = Default.Skins;

      if (PlayerShadow != none)
         PlayerShadow.bShadowActive = true;

      bAcceptsProjectors = true;
      SetOverlayMaterial(Material'LairTextures_T.ZedsUpscale.PlasmaShader', 999, true);
   }
}

// Turn off the zapped behavior
simulated function UnSetZappedBehavior()
{
   super.UnSetZappedBehavior();

   // Handle getting the zed back cloaked if need be
   if( Level.Netmode != NM_DedicatedServer )
   {
      LastCheckTimes = Level.TimeSeconds;
      SetOverlayMaterial(None, 0.0f, true);
   }

   Controller.GoToState('ZombieHunt');
}

// Overridden because we need to handle the overlays differently for zombies that can cloak
function SetZapped(float ZapAmount, Pawn Instigator)
{
   FShouldTaunt = 0;

   if (fFrozen)
      return;

   // don't be zapped while fleeing
   if (FSyringeCount < 3 && Health <= FHealLevel)
   {
      bZapped = False;
      return;
   }

   LastZapTime = Level.TimeSeconds;

   if (bZapped)
   {
      TotalZap = ZapThreshold;
      RemainingZap = ZapDuration;
   }
   else
   {
      TotalZap += ZapAmount;

      if( TotalZap >= ZapThreshold )
      {
         RemainingZap = ZapDuration;
         bZapped = true;
      }
   }
   ZappedBy = Instigator;
}

simulated function PostBeginPlay()
{
   local Falk689GameTypeBase Flk;
   local float FUsedScalingVal;

   Super.PostBeginPlay();

   if (Role < ROLE_Authority)
      return;

   Flk = Falk689GameTypeBase(Level.Game);

   if (Flk != none)
   {
      FTauntAlivePlayers  = Flk.GetAlivePlayers();
      //warn("Setting Initial FTauntAlivePlayers to:"@FTauntAlivePlayers);
   }

   // Difficulty Scaling
   if (Level.Game != none)
   {
      FNumPlayers = Falk689GameTypeBase(Level.Game).GetAlivePlayers();

      // Setting base difficulty scale value
      if (Level.Game.GameDifficulty < 3.0) 
         FUsedScalingVal = FNormalScalingVal;

      else if (Level.Game.GameDifficulty < 5.0)
         FUsedScalingVal = FHardScalingVal;

      else if (Level.Game.GameDifficulty < 6.0)
         FUsedScalingVal = FSuiScalingVal;

      else if (Level.Game.GameDifficulty < 8.0)
         FUsedScalingVal = FHOEScalingVal;

      else 
      {
         FUsedScalingVal = FBBScalingVal;
         FBloodbath      = True;
      }

      // Adding multiplayer fixed difficulty bonus
      if (FNumPlayers > 1)
         FUsedScalingVal += FMultiScalingAdd;
      
      // Adding per player difficulty bonus
      FUsedScalingVal += (FPlayerScalingAdd * (FNumPlayers - 1));

      //warn("Alive Players:"@FNumPlayers@"- Pat Scaling Value:"@FUsedScalingVal);

      FMGDamage      = default.FMGDamage     * FUsedScalingVal;
      FClawDamage    = default.FClawDamage   * FUsedScalingVal;
      FImpaleDamage  = default.FImpaleDamage * FUsedScalingVal;
      FRadialDamage  = default.FRadialDamage * FUsedScalingVal;

      //log(FMGDamage);
      //log(FMeleeDamage);
      //log(FRadialDamage);
   }

   FHealthMax       = Health;
   FHealLevel       = Health * FHealLevelPercent;

   // we probably don't need this but who knows what weird shit may happen
   HealingLevels[0] = FHealLevel;
   HealingLevels[1] = FHealLevel;
   HealingLevels[2] = FHealLevel;

   HealingAmount = Health * 0.75; // first overheal fix

   FCurMaxFrustration = FBaseFrustration + Rand(FAddFrustration); // initial maximum setting frustration level

   // this was probably overriding the boss ground speed at default, not needed anymore
   /*GroundSpeed         = Default.GroundSpeed;
   fCurGroundSpeed     = Default.GroundSpeed;
   AirSpeed            = Default.AirSpeed;
   WaterSpeed          = Default.WaterSpeed;
   OriginalGroundSpeed = GroundSpeed;*/
}

function bool MakeGrandEntry()
{
   bShotAnim = true;
   Acceleration = vect(0,0,0);
   SetAnimAction('Entrance');
   Controller.GoToState('WaitForAnim');
   fBossAttackDelay = 1.0; // testing
   GotoState('MakingEntrance');

   return True;
}

// State of playing the initial entrance anim
state MakingEntrance
{
   Ignores RangedAttack;

   // ignore this too
   function FalkFire(optional float F)
   {
   }

   // for some reason I can't ignore this... sadface.
   function FalkRangedAttack(Actor A)
   {
   }

   // don't do this
   function FStairFixJump()
   {
   }

   function Tick(float Delta)
   {
      Acceleration = vect(0,0,0);

      global.Tick(Delta);

      FEntranceStartCFix = Default.FEntranceEndCFix;
      fStairsSecondCheck = False;
      fLastCanAttackTime = Level.TimeSeconds;

   }

Begin:
   Sleep(GetAnimDuration('Entrance'));
   FEntranceStartCFix = Default.FEntranceEndCFix;
   GotoState('InitialSneak');
}

// State of doing a radial damaging attack that we do when people are trying to melee-exploit us
state RadialAttack
{
   Ignores RangedAttack;

   // for some reason I can't ignore this... sadface.
   function FalkRangedAttack(Actor A)
   {
   }

   // don't do this
   function FStairFixJump()
   {
   }

   // just update the count without doing anything
   function FKillTauntStart(int FAlivePlayers)
   {
      //warn("From:"@FTauntAlivePlayers@"To:"@FAlivePlayers@"Time:"@Level.TimeSeconds);
      FTauntAlivePlayers = FAlivePlayers - 1;
   }

   function bool ShouldChargeFromDamage()
   {
      return false;
   }

   function Tick( float Delta )
   {
      Acceleration = vect(0,0,0);

      //DrawDebugSphere( Location, 150, 12, 0, 255, 0);

      global.Tick(Delta);

      // reset total zap after a while
      if (!bZapped && TotalZap > 0 && Level.TimeSeconds >= LastZapTime + fZapResetTime)
         TotalZap = 0;

      FRadialEndTime     = Level.TimeSeconds;
      fStairsSecondCheck = False;
      fLastCanAttackTime = Level.TimeSeconds;
   }

   function ClawDamageTarget()
   {
      local vector PushDir;
      local bool bDamagedSomeone, bDamagedThisHit;
      local KFHumanPawn P;
      local KFMonster M;
      local Actor OldTarget;

      fLastCanAttackTime = Level.TimeSeconds;

      // block the second zedtime on radial
      if (!fZedTimeRadial)
      {
         fZedTimeRadial = True;
         KFGameType(Level.Game).DramaticEvent(1.0, 2.0);
      }

      else
         fZedTimeRadial = False;

      MeleeRange = FRadialRange;

      if(Controller!=none && Controller.Target!=none)
         PushDir = (damageForce * Normal(Controller.Target.Location - Location));
      else
         PushDir = damageForce * vector(Rotation);


      OldTarget = Controller.Target;

      CurrentDamtype = ZombieDamType[0];

      // Damage all players within a radius
      foreach DynamicActors(class'KFHumanPawn', P)
      {
         if (VSize(P.Location - Location) < MeleeRange)
         {
            Controller.Target = P;

            bDamagedThisHit =  MeleeDamageTarget(FRadialDamage, damageForce * Normal(P.Location - Location));

            if( !bDamagedSomeone && bDamagedThisHit )
            {
               bDamagedSomeone = true;
            }

            MeleeRange = FRadialRange;
         }
      }

      // Damage all zeds within a radius
      foreach DynamicActors(class'KFMonster', M)
      {
         if (M != Self && VSize(M.Location - Location) < MeleeRange)
         {
            Controller.Target = M;

            bDamagedThisHit =  MeleeDamageTarget(FRadialDamage, damageForce * Normal(M.Location - Location));

            if (!bDamagedSomeone && bDamagedThisHit)
            {
               bDamagedSomeone = true;
            }

            MeleeRange = FRadialRange;
         }
      }

      Controller.Target = OldTarget;

      MeleeRange = Default.MeleeRange;

      if (bDamagedSomeone && !fFrozen && !bZapped && !fIsStunned)
      {
         // Cause zedtime when the patriarch does his radial attack
         PlaySound(MeleeAttackHitSound, SLOT_Interact, 2.0);
      }
   }

   function EndState()
   {
      if (FRadialResetChance >= 1.0 || FRand() < FRadialResetChance)
      {
         //warn("RADIAL RESET:"@Level.TimeSeconds);
         FFrustration       = 0;
      }

      FLockedChargeTgt   = False;
      NumLumberJacks     = 0;
      NumNinjas          = 0;
      FRadialEndTime     = Level.TimeSeconds;
      fStairsSecondCheck = False;
      fLastCanAttackTime = Level.TimeSeconds;

      // force a new enemy on the controller
      if (FalkBossController(Controller) != none)
      {
         FalkBossController(Controller).fRandomEnemy = False;
         FalkBossController(Controller).fRandomDist  = False;
         FalkBossController(Controller).FindNewEnemy(); 
      }
   }

Begin:
   // Don't let the zed move and play the radial attack
   //FFrustration  = 0;
   FLockedChargeTgt   = False;
   bShotAnim          = true;
   bWaitForAnim       = true;
   Acceleration       = vect(0,0,0);
   SetAnimAction('RadialAttack');
   KFMonsterController(Controller).bUseFreezeHack = True;
   Controller.GoToState('WaitForAnim');
   Sleep(GetAnimDuration('RadialAttack'));
   FRadialEndTime = Level.TimeSeconds;
   GotoState('');
}

simulated function Destroyed()
{
   if( mTracer!=None )
      mTracer.Destroy();
   if( mMuzzleFlash!=None )
      mMuzzleFlash.Destroy();
   Super.Destroyed();
}

simulated Function PostNetBeginPlay()
{
   EnableChannelNotify ( 1,1);
   AnimBlendParams(1, 1.0, 0.0,, SpineBone1);
   super.PostNetBeginPlay();
   TraceHitPos = vect(0,0,0);
   bNetNotify = True;
}

function PlayTakeHit(vector HitLocation, int Damage, class<DamageType> DamageType)
{
   local KFPlayerReplicationInfo   KFPRI;
   local class<FVeterancyTypes>    Vet;

   if (fFrozen)
      return;

   if( Level.TimeSeconds - LastPainAnim < MinTimeBetweenPainAnims )
      return;

   // we're freezing, hide the pain
   if (class<DamTypeFreezeBomb>(damageType) != none)
      return;

   if (class<DamTypeSW76Falk>(damageType) != none)
   {
      if (LastDamagedBy != none)
      {
         KFPRI = KFPlayerReplicationInfo(LastDamagedBy.PlayerReplicationInfo);

         if (KFPRI != none)
         {
            Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

            if (Vet != none && Vet.Static.ArtilleristZedTimeBonus(KFPRI))
               return;
         }
      }
   }

   if( Damage>=150 || (DamageType.name=='DamTypeStunNade' && rand(5)>3) || (DamageType.name=='DamTypeCrossbowHeadshot' && Damage>=200) )
      PlayDirectionalHit(HitLocation);

   LastPainAnim = Level.TimeSeconds;

   if( Level.TimeSeconds - LastPainSound < MinTimeBetweenPainSounds )
      return;

   LastPainSound = Level.TimeSeconds;
   PlaySound(HitSound[0], SLOT_Pain,2*TransientSoundVolume,,400);
}

function bool OnlyEnemyAround(Pawn Other)
{
   /*local Controller C;

   For (C=Level.ControllerList; C!=None; C=C.NextController)
   {
      if( C.bIsPlayer && C.Pawn!=None && C.Pawn!=Other && ((VSize(C.Pawn.Location-Location)<1500 && FastTrace(C.Pawn.Location,Location))
               || (VSize(C.Pawn.Location-Other.Location)<1000 && FastTrace(C.Pawn.Location,Other.Location))) )
         Return False;
   }
   Return True;*/
   
   Return False;
}

function bool IsCloseEnuf(Actor A)
{
   local vector V;
   local bool result;

   if (A == None)
   {
      //warn("IsCloseEnuf False 1");
      Return False;
   }

   V = A.Location-Location;

   if (Abs(V.Z) > (CollisionHeight + A.CollisionHeight))
   {
      //warn("IsCloseEnuf False 2");
      Return False;
   }

   V.Z = 0;

   result = (VSize(V) < (CollisionRadius + A.CollisionRadius+25));

   /*if (!result)
      warn("IsCloseEnuf False 3");*/

   Return result;
}

// don't do shit here
function Fire(optional float F)
{
}

// basically called on tick instead of who knows when
function FalkFire(optional float F)
{
	local Actor BestTarget;
   local float bestAim, bestDist;
   local vector FireDir, X,Y,Z;
   local FMonsterController FC;

   if (Controller == none)
      return;

  // warn("BOSS FALK FIRE"@Level.TimeSeconds);

   bestAim = 0.90;
   GetAxes(Controller.Rotation,X,Y,Z);
   FireDir = X;

   FC = FMonsterController(Controller);

   if (FC != none)
      BestTarget = FC.FalkPickTarget(bestAim, bestDist, FireDir, GetFireStart(X,Y,Z), 6000);
 
   else
      BestTarget = Controller.PickTarget(bestAim, bestDist, FireDir, GetFireStart(X,Y,Z), 6000);

   if (KFHumanPawn(BestTarget) != none && KFHumanPawn(BestTarget).Health > 0)
      FalkRangedAttack(BestTarget);
}

// ignore FalkRangedAttack too
State ZombieDying
{
   // for some reason I can't ignore this... sadface.
   function FalkRangedAttack(Actor A)
   {
   }
}

// let's falkify this
function RangedAttack(Actor A)
{
}

// falkified ranged attack
function FalkRangedAttack(Actor A)
{
   local float D, fAttackChance;
   //local bool bOnlyE;
   local bool bDesireChainGun;
   //local byte fChaingunBullets;
   local name fMeleeAnimName;
   local float fDist;
   
   if (fFrozen || bZapped)
   {
      //warn("RangedAttack Return 1 time:"@Level.TimeSeconds);
      return;
   }

   if (FEntranceStartCFix > 0)
   {
      //warn("RangedAttack Return 2"@Level.TimeSeconds);
      return;
   }

   if (Level.TimeSeconds < FChaingunEndTime + FChaingunEndCFix)
   {
      //warn("RangedAttack Return 3"@Level.TimeSeconds);
      return;
   }

   if (Level.TimeSeconds < FFreezeEndTime + FFreezeEndCFix)
   {
      //warn("RangedAttack Return 4"@Level.TimeSeconds);
      return;
   }

   if (Level.TimeSeconds < FMissileEndTime + FMissileEndCFix)
   {
      //warn("RangedAttack Return 5"@Level.TimeSeconds);
      return;
   }

   if (Level.TimeSeconds < FJumpEndTime + FJumpEndCFix)
   {
      //warn("RangedAttack Return 6"@Level.TimeSeconds);
      //FFrustration = 0;
      return;
   }

   if (Level.TimeSeconds < FHealEndTime + FHealEndCFix)
   {
      //warn("RangedAttack Return 7"@Level.TimeSeconds);
      return;
   }

   if (Level.TimeSeconds < FRadialEndTime + FRadialEndCFix)
   {
      //warn("RangedAttack Return 8"@Level.TimeSeconds);
      return;
   }

   if (bShotAnim || Level.TimeSeconds < fAnimEndTime || Level.TimeSeconds < FLastAttackTime + FAttackCooldown || FNextState != '')
   {
      //warn("RangedAttack Return 9"@Level.TimeSeconds);
      return;
   }

   //warn("RangedAttack:"@Level.TimeSeconds);

   if (!IsInState('FireChaingun') && !IsInState('RadialAttack') && !IsInState('MakingEntrance') && !IsInState('Healing'))
   {

      fDist    = VSizeSquared(Location - FLastPos);
      FLastPos = Location;

      //warn("DIST:" @ fDist);

      if (fDist < FStuckDist)
      {
         if (FStuckTime > 0)
            FStuckTime -= 1;

         else
         {
            //warn("STUCK FIX:"@Level.TimeSeconds);
            GoToState('SneakAround');
         }
      }

      else
         FStuckTime = Default.FStuckTime;
   }

   // Randomly make him want to chaingun more
   if (Controller.LineOfSightTo(A) && FRand() < 0.15 && LastChainGunTime < Level.TimeSeconds)
      bDesireChainGun = true;

   D = VSize(A.Location-Location);
   //bOnlyE = (Pawn(A)!=None && OnlyEnemyAround(Pawn(A)));

   if (IsCloseEnuf(A))
   {
      //bShotAnim = true;
      FLastAttackTime = Level.TimeSeconds;

      //warn("MeleeAttack:"@Level.TimeSeconds@"- Cooldown:"@FAttackCooldown@" - Next Attack Time:"@FLastAttackTime + FAttackCooldown);

      if (FMeleeResetChance >= 1.0 || FRand() < FMeleeResetChance)
      {
         FFrustration          = 0;
         //warn("MELEE RESET:"@Level.TimeSeconds);
      }

      fAttackChance = FRand();

      //warn("Attack: "@fAttackChance);

      if (fAttackChance <= 0.4)        // 40% chance to impale
         fMeleeAnimName = 'MeleeImpale';

      else if (fAttackChance <= 0.8)   // 40% chance to do a standard claw attack
         fMeleeAnimName = 'MeleeClaw';

      else                             // 20% chance to blunt attack with the minigun
         fMeleeAnimName = 'MeleeClaw2';

      SetAnimAction(fMeleeAnimName);
   }

   else if (Level.TimeSeconds - LastSneakedTime > 20.0)
   {
      if( FRand() < 0.3 )
      {
         // Wait another 20 to try this again
         LastSneakedTime = Level.TimeSeconds;//+FRand()*120;

         //warn("RangedAttack Return 10"@Level.TimeSeconds);
         Return;
      }

      //SetAnimAction('transition');
      GoToState('SneakAround');
   }

   // keep charging only if we still want to melee attack the player
   else if (bChargingPlayer && NumChargeAttacks > 0)
      Return;

   else if (FShouldTaunt == 0 && !bDesireChainGun && !bChargingPlayer && /*(*/D<300/* || (D<700 && bOnlyE))*/ && (Level.TimeSeconds - LastChargeTime > (5.0 + 5.0 * FRand()))) // Don't charge again for a few seconds
   {
      //SetAnimAction('transition');

      //warn("Charging 3"@Level.TimeSeconds);
      GoToState('Charging');
   }

   else if (!fFrozen && FChaingunEndTime + fChainMissileCD < Level.TimeSeconds && FRadialEndTime + fChainMissileCD < Level.TimeSeconds && LastMissileTime < Level.TimeSeconds && D > 500)
   {
      if (!Controller.LineOfSightTo(A) || FRand() > 0.75 || VSizeSquared(Location - A.Location) < FMinMissileDist)
      {
         LastMissileTime = Level.TimeSeconds + FRand() * 5;
         //warn("RangedAttack Return 11"@Level.TimeSeconds);
         Return;
      }

      LastMissileTime = Level.TimeSeconds + 10 + FRand() * 15;

      bShotAnim = false;

      AnimEnd(0);
      AnimEnd(1);
      AnimEnd(2);
      AnimEnd(3);

      bShotAnim      = true;
      bWaitForAnim   = true;
      Acceleration   = vect(0,0,0);
      FNextState     = 'FireMissile';
      FNextStateTime = Level.TimeSeconds + FStateAnimDelay;

      //warn("Missile Start: "@Level.TimeSeconds);

      //KFMonsterController(Controller).bUseFreezeHack = True;

      //SetAnimAction('PreFireMissile');
      //Controller.GoToState('WaitForAnim');

      //GoToState('FireMissile');
   }

   else if (!fFrozen && !bWaitForAnim && !bShotAnim && LastChainGunTime < Level.TimeSeconds)
   {
      if (!Controller.LineOfSightTo(A) || FRand() > 0.85)
      {
         LastChainGunTime = Level.TimeSeconds+FRand()*4;
         //warn("RangedAttack Return 12"@Level.TimeSeconds);
         Return;
      }

      LastChainGunTime = Level.TimeSeconds + 5 + FRand() * 10;

      bShotAnim = false;

      AnimEnd(0);
      AnimEnd(1);
      AnimEnd(2);
      AnimEnd(3);

      //warn("Minigun Start: "@Level.TimeSeconds);

      bShotAnim      = true;
      bWaitForAnim   = true;
      Acceleration   = vect(0,0,0);

      FNextState     = 'FireChaingun';
      FNextStateTime = Level.TimeSeconds + FStateAnimDelay;
   }
}

event Bump(actor Other)
{
   Super(Monster).Bump(Other);
   if( Other==none )
      return;

   if( Other.IsA('NetKActor') && Physics != PHYS_Falling && !bShotAnim && Abs(Other.Location.Z-Location.Z)<(CollisionHeight+Other.CollisionHeight) )
   { // Kill the annoying deco brat.
      Controller.Target = Other;
      Controller.Focus = Other;
      bShotAnim = true;
      Acceleration = (Other.Location-Location);

      if (FRand() < 0.2)
      {
         //warn("MeleeClaw2 B");
         SetAnimAction('MeleeClaw2');
         Controller.GoToState('WaitForAnim');
      }

      else
      {
         //warn("MeleeClaw B");
         SetAnimAction('MeleeClaw');
         Controller.GoToState('WaitForAnim');
      }
   }
}

simulated function AddTraceHitFX( vector HitPos )
{
   local vector Start,SpawnVel,SpawnDir;
   local float hitDist;

   Start = GetBoneCoords('tip').Origin;
   if( mTracer==None )
      mTracer = Spawn(Class'KFMod.KFNewTracer',,,Start);
   else mTracer.SetLocation(Start);
   if( mMuzzleFlash==None )
   {
      mMuzzleFlash = Spawn(Class'MuzzleFlash3rdMG');
      AttachToBone(mMuzzleFlash, 'tip');
   }
   else mMuzzleFlash.SpawnParticle(1);
   hitDist = VSize(HitPos - Start) - 50.f;

   if( hitDist>10 )
   {
      SpawnDir = Normal(HitPos - Start);
      SpawnVel = SpawnDir * 10000.f;
      mTracer.Emitters[0].StartVelocityRange.X.Min = SpawnVel.X;
      mTracer.Emitters[0].StartVelocityRange.X.Max = SpawnVel.X;
      mTracer.Emitters[0].StartVelocityRange.Y.Min = SpawnVel.Y;
      mTracer.Emitters[0].StartVelocityRange.Y.Max = SpawnVel.Y;
      mTracer.Emitters[0].StartVelocityRange.Z.Min = SpawnVel.Z;
      mTracer.Emitters[0].StartVelocityRange.Z.Max = SpawnVel.Z;
      mTracer.Emitters[0].LifetimeRange.Min = hitDist / 10000.f;
      mTracer.Emitters[0].LifetimeRange.Max = mTracer.Emitters[0].LifetimeRange.Min;
      mTracer.SpawnParticle(1);
   }
   Instigator = Self;

   if( HitPos != vect(0,0,0) )
   {
      Spawn(class'ROBulletHitEffect',,, HitPos, Rotator(Normal(HitPos - Start)));
   }
}

simulated function AnimEnd(int Channel)
{
   local name  Sequence;
   local float Frame, Rate;

   if (Role == ROLE_Authority)
   {
      GetAnimParams(Channel, Sequence, Frame, Rate);

      // second attempt to put a cooldown on jump
      if (Sequence == 'JumpInAir' || Sequence == 'JumpLanded' || Sequence == 'JumpTakeOff')
      {
         //FFrustration = 0;
         FJumpEndTime = Level.TimeSeconds;
      }
   }

   if (Level.NetMode == NM_Client && bMinigunning)
   {
      GetAnimParams(Channel, Sequence, Frame, Rate);

      if(fFrozen || (Sequence != 'PreFireMG' && Sequence != 'FireMG'))
      {
         //SetBoneDirection(FireRootBone,rot(0,0,0),,0,0);
         Super(KFMonster).AnimEnd(Channel);
         return;
      }

      if (FGetSyringeCount() > 0)
         DoAnimAction('FireMG');

      else
      {
         PlayAnim('FireMG');
         bWaitForAnim = true;
         bShotAnim = true;
         IdleTime = Level.TimeSeconds;
      }
   }

   else
   {
      //bShotAnim = false;
      Super(KFMonster).AnimEnd(Channel);
   }
}

state DoorBashing
{
   function BeginState()
   {
      FDoorBashStartTime = Level.TimeSeconds;
   }

	simulated function bool HitCanInterruptAction()
	{
		return false;
	}

	function Tick(float Delta)
	{
		Acceleration = vect(0,0,0);

      if (Level.TimeSeconds >= FDoorBashStartTime + FDoorBashMaxTime)
      {
         EndState();
         GoToState('');
      }

		global.Tick(Delta);
	}

Begin:
   while (KFDoorMover(Controller.Target) != none && KFDoorMover(Controller.Target).bSealed && !KFDoorMover(Controller.Target).bDoorIsDead && KFDoorMover(Controller.Target).Health > 0)
   {
      bShotAnim = true;
      Controller.GoToState('WaitForAnim');
      SetAnimAction('MeleeClaw');
	   Sleep(GetAnimDuration('MeleeClaw') + 0.1);
   }

   Controller.GoToState('ZombieHunt');
	GotoState('');
}

state FireChaingun
{
   // don't do this
   function FStairFixJump()
   {
      fStairsSecondCheck = False;
      fLastCanAttackTime = Level.TimeSeconds;
   }

   function FalkRangedAttack(Actor A)
   {
      Controller.Target = A;
      Controller.Focus = A;
   }

   // prepare for the taunt
   function FKillTauntStart(int FAlivePlayers)
   {
      if (!bZapped && !fFrozen)
      {
         FTauntAlivePlayers = FAlivePlayers - 1;

         if (FTauntAlivePlayers > 0)
         {
            //warn("Chaingun Setting to 1:"@Level.TimeSeconds);
            FShouldTaunt = 1;
         }

         else
         {
            //warn("Chaingun Setting to 0:"@Level.TimeSeconds);
            FShouldTaunt = 0;
         }

      }
   }

   // Chaingun mode handles this itself
   function bool ShouldChargeFromDamage()
   {
      return false;
   }

   function EndState()
   {
      if (FChainResetChance >= 1.0 || FRand() < FChainResetChance)
      {
         FFrustration          = 0;
         //warn("MINIGUN RESET:"@Level.TimeSeconds);
      }

      FLockedChargeTgt      = False;
      //warn("bChargingPlayer FALSE 1 time:"@Level.TimeSeconds);
      bChargingPlayer       = false;
      TraceHitPos           = vect(0,0,0);
      bMinigunning          = False;

      AmbientSound          = default.AmbientSound;
      SoundVolume           = default.SoundVolume;
      SoundRadius           = default.SoundRadius;
      MGFireCounter         = 0;

      FChaingunEndTime      = Level.TimeSeconds;
      LastChainGunTime      = Level.TimeSeconds + 5 + (FRand()*10);
      bCanStrafe            = false;
      FBlockStairsFix       = false;
      fStairsSecondCheck    = false;
      fLastCanAttackTime    = Level.TimeSeconds;

      if (bZapped)
         SetGroundSpeed(GetOriginalGroundSpeed() * ZappedSpeedMod);

      else
         SetGroundSpeed(GetOriginalGroundSpeed());

      if (bZapped || fFrozen)
      {
         //warn("Chaingun Reset Taunt at endstate:"@Level.TimeSeconds);
         FShouldTaunt = 0;
      }

      if (FShouldTaunt == 1)
      {
         //warn("Chaingun Start Taunt at end state:"@Level.TimeSeconds);
         fTauntStateEndTime = Level.TimeSeconds + FChaingunTDelay;
         FShouldTaunt = 2;
      }

      if (Level.NetMode != NM_DedicatedServer)
         PostNetReceive();

      fResetGroundSpeed();
   }

   function BeginState()
   {
      if (bZapped)
      {
         EndState();
         GoToState('');
      }

      FLastAttackTime = Level.TimeSeconds;

      bFireAtWill = False;
      Acceleration = vect(0,0,0);
      MGLostSightTimeout = 0.0;
      bMinigunning = True;
      bCanStrafe = true;
   }

   function AnimEnd(int Channel)
   {
      if (MGFireCounter <= 0)
      {
         bShotAnim = true;
         Acceleration = vect(0,0,0);
         KFMonsterController(Controller).bUseFreezeHack = True;
         SetAnimAction('FireEndMG');
         Controller.GoToState('WaitForAnim');

         EndState();
         GoToState('');
      }

      else if (FGetSyringeCount() > 0)
      {
         if (bFireAtWill && Channel != 1)
            return;

         if (Controller.Target != None)
            Controller.Focus = Controller.Target;

         bShotAnim = false;
         bFireAtWill = True;
         SetAnimAction('FireMG');
      }

      else
      {
         if (!fFrozen && Controller.Enemy != none)
         {
            if (Controller.LineOfSightTo(Controller.Enemy) && FastTrace(GetBoneCoords('tip').Origin,Controller.Enemy.Location))
            {
               MGLostSightTimeout = 0.0;
               Controller.Focus = Controller.Enemy;
               Controller.FocalPoint = Controller.Enemy.Location;
            }

            else
            {
               MGLostSightTimeout = Level.TimeSeconds + (0.25 + FRand() * 0.35);
               //Controller.Focus = None;
            }
            Controller.Target = Controller.Enemy;
         }

         else
         {
            MGLostSightTimeout = Level.TimeSeconds + (0.25 + FRand() * 0.35);
            //Controller.Focus = None;
         }

         if (!fFrozen && !bFireAtWill)
         {
            MGFireDuration = Level.TimeSeconds + (0.75 + FRand() * 0.5);
         }

         else if (!fFrozen && FRand() < 0.03 && Controller.Enemy != none && PlayerController(Controller.Enemy.Controller) != none)
         {
            // Randomly send out a message about Patriarch shooting chain gun(3% chance)
            PlayerController(Controller.Enemy.Controller).Speech('AUTO', 9, "");
         }

         bFireAtWill = True;
         bShotAnim = true;
         Acceleration = vect(0,0,0);

         SetAnimAction('FireMG');
         bWaitForAnim = true;
      }
   }

   function Tick(float Delta)
   {
      local float fSpeedMulti;
      local bool bDidRadialAttack;
      local KFHumanPawn P;
      local PlayerController PC;
      local byte i;
      local float fTempSpeed;

      Super.Tick(Delta);

      // reset total zap after a while
      if (!bZapped && TotalZap > 0 && Level.TimeSeconds >= LastZapTime + fZapResetTime)
         TotalZap = 0;

      fStairsSecondCheck = False;
      fLastCanAttackTime = Level.TimeSeconds;

      if (fFrozen || bZapped)
      {
         EndState();
         GoToState('');
      }

      if (FGetSyringeCount() > 0)
      {
         // Setting minigun walk speed based on difficulty
         if (Level.Game.GameDifficulty >= 8.0) // Bloodbath
            fSpeedMulti = FBBMWS;

         else if (Level.Game.GameDifficulty >= 7.0) // Hell on Earth
            fSpeedMulti = FHOEMWS;

         else if (Level.Game.GameDifficulty >= 5.0) // Suicidal
            fSpeedMulti = FSuicidalMWS;

         else if (Level.Game.GameDifficulty >= 4.0) // Hard
            fSpeedMulti = FHardMWS;

         else
            fSpeedMulti = FNormalMWS;

         fTempSpeed = OriginalGroundSpeed * fSpeedMulti;

         // additional speed multiplier
         if (FGetSyringeCount() > 1)
            fTempSpeed *= 1.0 + FHealMWS * (FGetSyringeCount() - 1);

         if (bWaitForAnim || FCStopMoving || MGFireCounter <= 0)
         {
            Acceleration          = vect(0, 0, 0);
            Velocity.X            = 0;
            Velocity.Y            = 0;
            WaterSpeed            = 0;
            AirSpeed              = 0;
            SetGroundSpeed(1);
         }

         else
         {
            WaterSpeed  = fTempSpeed; 
            AirSpeed    = fTempSpeed;
            //warn("CHAINGUN");
            SetGroundSpeed(fTempSpeed);
         }
      }

      else
      {
         Acceleration = vect(0, 0, 0);
         Velocity.X   = 0;
         Velocity.Y   = 0;
         WaterSpeed   = 0;
         AirSpeed     = 0;
         SetGroundSpeed(1);
      }

      if (FMCurCheck < FMExpCheck)
         FMCurCheck += Delta;

      else
      {
         FMCurCheck = 0;

         // look for guys attacking us within 3 meters
         foreach DynamicActors(class'KFHumanPawn', P)
         {
            if (P != none)
            {
               PC = PlayerController(P.Controller);

               if (PC != none)
               {
                  i = ExpIndex(PC);

                  if (VSize(P.Location - Location) < 150)
                  {
                     fExpList[i].MTicks++;

                     if (fExpList[i].MTicks >= FMCDudeCheck && !bDidRadialAttack)
                     {

                        bDidRadialAttack   = true;
                        fExpList[i].MTicks = 0;
                        //FNextState         = 'RadialAttack';
                        //FNextStateTime     = Level.TimeSeconds + FStateAnimDelay * 2;
                        //GoToState('RadialAttack');
                        //FChaingunEndTime   = 0;

                        bShotAnim = false;
                        AnimEnd(0);
                        AnimEnd(1);
                        AnimEnd(2);
                        AnimEnd(3);

                        FChaingunEndTime   = 0;
                        GoToState('RadialAttack');
                        break;
                     }
                  }

                  else
                     fExpList[i].MTicks = Max(fExpList[i].MTicks--, 0);
               }
            }
         }
      }
   }

   function FireMGShot()
   {
      local vector Start,End,HL,HN,Dir;
      local rotator R;
      local Actor A;
      local float fPrec;
      local float fDoorDamage;
      local FalkBossController FBC;

      MGFireCounter--;

      if( AmbientSound != MiniGunFireSound )
      {
         SoundVolume=255;
         SoundRadius=400;
         AmbientSound = MiniGunFireSound;
      }

      Start = GetBoneCoords('tip').Origin;
      if( Controller.Focus!=None )
         R = rotator(Controller.Focus.Location-Start);
      else R = rotator(Controller.FocalPoint-Start);
      if( NeedToTurnFor(R) )
         R = Rotation;

      if (FGetSyringeCount() > 2)
         fPrec = FMThreePM;

      else if (SyringeCount == 2)
         fPrec = FMTwoPM;

      else if (SyringeCount == 1)
         fPrec = FMOnePM;

      else
         fPrec = FMZeroPM;

      Dir = Normal(vector(R)+VRand()*fPrec);
      End = Start+Dir*10000;

      // Have to turn of hit point collision so trace doesn't hit the Human Pawn's bullet whiz cylinder
      bBlockHitPointTraces = false;
      A = Trace(HL,HN,End,Start,True);
      bBlockHitPointTraces = true;

      if (A == None)
         Return;

      TraceHitPos = HL;

      if (Level.NetMode != NM_DedicatedServer)
         AddTraceHitFX(HL);

      if (Pawn(A) != none && Pawn(A).Health > 0)
      {
         A.TakeDamage(FMGDamage, Self, HL, Dir*500, Class'DamTypeBossChainGunFalk');

         FBC = FalkBossController(Controller);

         // taunt (and find another target) if one died
         if (Pawn(A).Health <= 0 && Controller != none && FBC != none)
         {
            FBC.FindNewEnemy(); 

            if (!FBC.fSeeNewEnemy)
            {
               EndState();
               GotoState('');
            }
         }
      }

      else if (KFDoorMover(A) != none && KFDoorMover(A).bSealed)
      {
         fDoorDamage = FMax(1, KFDoorMover(A).MaxWeld * FDoorMGDamagePerc);
         A.TakeDamage(fDoorDamage, Self, HL, Dir*500, Class'DamTypeBossChainGunFalk');
      }

      else if (Pawn(A) == none) // shatter glasses and damage stuff
         A.TakeDamage(FMGDamage, Self, HL, Dir*500, Class'DamTypeBossChainGunFalk');
   }

   function bool NeedToTurnFor(rotator targ)
   {
      local int YawErr;

      targ.Yaw = DesiredRotation.Yaw & 65535;
      YawErr = (targ.Yaw - (Rotation.Yaw & 65535)) & 65535;
      return !((YawErr < 2000) || (YawErr > 64535));
   }

Begin:

   While(True)
   {
      //FFrustration = 0;
      FLockedChargeTgt = False;

      if (FGetSyringeCount() <= 0)
      {
         Acceleration = vect(0,0,0);
         Velocity = vect(0,0,0);
      }

      if (MGLostSightTimeout > 0 && Level.TimeSeconds > MGLostSightTimeout)
      {
         bShotAnim = true;
         Acceleration = vect(0,0,0);
         SetAnimAction('FireEndMG');
         Controller.GoToState('WaitForAnim');
         EndState();
         GoToState('');
      }

      if (MGFireCounter <= 0)
      {
         bShotAnim = true;
         Acceleration = vect(0,0,0);
         SetAnimAction('FireEndMG');
         Controller.GoToState('WaitForAnim');
         EndState();
         GoToState('');
      }

      // Give some randomness to the patriarch's firing
      if(Level.TimeSeconds > MGFireDuration)
      {
         if(AmbientSound != MiniGunSpinSound)
         {
            SoundVolume=185;
            SoundRadius=200;
            AmbientSound = MiniGunSpinSound;
         }
         Sleep(0.5 + FRand() * 0.75);
         MGFireDuration = Level.TimeSeconds + (0.75 + FRand() * 0.5);
      }

      else
      {
         if (bFireAtWill)
            FireMGShot();

         Sleep(0.05);
      }
   }
}

state FireMissile
{
   // don't do this
   function FStairFixJump()
   {
      fStairsSecondCheck = False;
      fLastCanAttackTime = Level.TimeSeconds;
   }

   function FalkRangedAttack(Actor A)
   {
      if (MissilesLeft > 1)
      {
         Controller.Target = A;
         Controller.Focus = A;
      }
   }

   // prepare for the taunt
   function FKillTauntStart(int FAlivePlayers)
   {
      if (!bZapped && !fFrozen)
      {
         FTauntAlivePlayers = FAlivePlayers - 1;

         if (FTauntAlivePlayers > 0)
         {
            //warn("Missile Setting to 1:"@Level.TimeSeconds);
            FShouldTaunt = 1;
         }

         else
         {
            //warn("Missile Setting to 0:"@Level.TimeSeconds);
            FShouldTaunt = 0;
         }
      }
   }

   function bool ShouldChargeFromDamage()
   {
      return false;
   }

   function BeginState()
   {
      if (bZapped)
      {
         EndState();
         GoToState('');
      } 
    
      // try to actually look at a target
      if (Controller != none && Controller.Focus != none)
      {
         if (Controller.Enemy != none)
         {
            Controller.Focus = Controller.Enemy;
            //warn("FOCUS ENEMY:"@Level.TimeSeconds);
         }

         else if (Pawn(Controller.Target) != none)
         {
            Controller.Focus = Pawn(Controller.Target);   
            //warn("FOCUS TARGET:"@Level.TimeSeconds);
         }

         else if(FalkBossController(Controller) != none)
         {
            FalkBossController(Controller).FindNewEnemy(); 

            if (Controller.Enemy != none)
            {
               Controller.Focus = Controller.Enemy;
               //warn("FOCUS ENEMY 2:"@Level.TimeSeconds);
            }

            else if (Pawn(Controller.Target) != none)
            {
               Controller.Focus = Pawn(Controller.Target);   
               //warn("FOCUS TARGET 2:"@Level.TimeSeconds);
            }
         }
      }

      FLastAttackTime = Level.TimeSeconds;

      MissilesLeft = MissilesAmount[FGetSyringeCount()];
      Acceleration = vect(0,0,0);

      fStairsSecondCheck = false;
      fLastCanAttackTime = Level.TimeSeconds + 3.0;
   }

   function EndState()
   {
      FMissileEndTime = Level.TimeSeconds;

      if (bZapped || fFrozen)
      {
         //warn("Missile Reset Taunt at endstate:"@Level.TimeSeconds);
         FShouldTaunt = 0;
      }

      if (FShouldTaunt == 1)
      {
         //warn("Missile Start Taunt at end state:"@Level.TimeSeconds@"Delayed at:"@fTauntStateEndTime);
         fTauntStateEndTime = Level.TimeSeconds + FMissileTDelay;
         FShouldTaunt = 2;
      }

      if (FRocketResetChance >= 1.0 || FRand() < FRocketResetChance)
      {
         FFrustration          = 0;
         //warn("MISSILE RESET:"@Level.TimeSeconds);
      } 

      //warn("bChargingPlayer FALSE 2 time:"@Level.TimeSeconds);
      bChargingPlayer    = false;
      FBlockStairsFix    = false;
      fStairsSecondCheck = false;
      fLastCanAttackTime = Level.TimeSeconds;
      
      if (Level.NetMode != NM_DedicatedServer)
         PostNetReceive();

      Super.EndState();
   }

   function Tick(float Delta)
   {
      Super.Tick(Delta);

      // reset total zap after a while
      if (!bZapped && TotalZap > 0 && Level.TimeSeconds >= LastZapTime + fZapResetTime)
         TotalZap = 0;

      if (bZapped)
      {
         EndState();
         GoToState('');
      }
   }

   function AnimEnd(int Channel)
   {
      // TEST
      local vector Start;
      local Rotator R;
      local int fMissilePrec;

      Start = GetBoneCoords('tip').Origin;

      if (!SavedFireProperties.bInitialized)
      {
         SavedFireProperties.AmmoClass       = class'LAWAmmo';
         SavedFireProperties.ProjectileClass = class'BossLAWProjFalk';
         SavedFireProperties.WarnTargetPct   = 0.15;
         SavedFireProperties.MaxRange        = 10000;
         SavedFireProperties.bTossed         = False;
         SavedFireProperties.bTrySplash      = False;
         SavedFireProperties.bLeadTarget     = True;
         SavedFireProperties.bInstantHit     = True;
         SavedFireProperties.bInitialized    = true;
      }

      fMissilePrec = FMissileZeroPrec;

      // scale precision based on syringe count
      if (FGetSyringeCount() > 2)
         fMissilePrec = FMissileThreePrec;

      else if (SyringeCount == 2)
         fMissilePrec = FMissileTwoPrec; 

      else if (SyringeCount == 1)
         fMissilePrec = FMissileOnePrec;

      if (Controller != none && Pawn(Controller.Target) != none && Controller.LineOfSightTo(Controller.Target))
      {
         R = AdjustAim(SavedFireProperties,Start,fMissilePrec);
         //warn("NEW AIM: "@R@"OLD: "@FStartMissileAim@"Time:"@Level.TimeSeconds);
      }

      else
      {
         R = FStartMissileAim;
         //warn("RESTORE: "@FStartMissileAim@"Time:"@Level.TimeSeconds);
      }

      PlaySound(RocketFireSound,SLOT_Interact,2.0,,TransientSoundRadius,,false);

      // multi hit fix
      if (!fFrozen && !bZapped)
      {
         BulletID += class<BossLAWProjFalk>(FBossProjClass).Default.fNClusters;

         if (BulletID > 151)
            BulletID = 1;

         class<BossLAWProjFalk>(FBossProjClass).default.BulletID    = BulletID;
         class<BossLAWProjFalk>(FBossProjClass).default.FNumPlayers = FNumPlayers;

         if (FGetSyringeCount() > 2) // give the rocket some clusters on last syringe
            class<BossLAWProjFalk>(FBossProjClass).default.FShouldKaboom = True;

         else                  // otherwise disable it, or we end up with clusters in unwanted situations
            class<BossLAWProjFalk>(FBossProjClass).default.FShouldKaboom = False;

         Spawn(FBossProjClass,,,Start,R);
      }

      bShotAnim = true;
      Acceleration = vect(0,0,0);
      SetAnimAction('FireEndMissile');
      Controller.GoToState('WaitForAnim');

      // Randomly send out a message about Patriarch shooting a rocket(5% chance)
      if (FRand() < 0.05 && Controller.Enemy != none && PlayerController(Controller.Enemy.Controller) != none)
         PlayerController(Controller.Enemy.Controller).Speech('AUTO', 10, "");

      FMissileLaunchTime = Level.TimeSeconds;

      if (--MissilesLeft <= 0 || fFrozen || bZapped)
      {
         EndState();
         GoToState('');
      }

      else
         GoToState(,'SecondMissile');
   }

Begin:
   // attempt to fix a bug with walking animations after zap
   MovementAnims[0] = default.MovementAnims[0];
   MovementAnims[1] = default.MovementAnims[1];
   MovementAnims[2] = default.MovementAnims[2];
   MovementAnims[3] = default.MovementAnims[3];

   while (true)
   {
      FLockedChargeTgt = False;
      Acceleration = vect(0,0,0);
      sleep(0.1);
   }

SecondMissile:
   //FFrustration = 0;
   FLockedChargeTgt = False;
   Acceleration = vect(0,0,0);
   sleep(1.0);
   AnimEnd(0);
}

function bool MeleeDamageTarget(int hitdamage, vector pushdir)
{
   if (fFrozen)
      return false;

	if (Controller.Target != none && Controller.Target.IsA('KFDoorMover'))
	{
      FDoorDamageReset = Default.FDoorDamageReset;
      hitdamage = KFDoorMover(Controller.Target).MaxWeld * FCurDoorDamagePerc;
		Controller.Target.TakeDamage(hitdamage, self, Controller.Target.Location, pushdir, CurrentDamType);

      FCurDoorDamagePerc += FDoorDamageIncr;

		Return True;
	}

   if (Controller.Target != None && Controller.Target.IsA('NetKActor'))
      pushdir = Normal(Controller.Target.Location-Location) * 100000; // Fly bitch!

   //FFrustration = 0;
   FLockedChargeTgt = False;

   return Super.MeleeDamageTarget(hitdamage, pushdir);
}

state Charging
{
   // Don't override speed in this state
   function bool CanSpeedAdjust()
   {
      return false;
   }

   function bool ShouldChargeFromDamage()
   {
      return false;
   }

   function BeginState()
   {
      if (bZapped || fFrozen)
      {
         EndState();
         GoToState('');
      }

      else
      {
         bChargingPlayer = True;

         FChaingunEndTime = 0;
         FMissileEndTime  = 0;

         //warn("CHARGING ANIM 2"@Level.TimeSeconds);
         MovementAnims[0] = ChargingAnim;
         MovementAnims[1] = ChargingAnim;
         MovementAnims[2] = ChargingAnim;
         MovementAnims[3] = ChargingAnim;

         if (Level.NetMode != NM_DedicatedServer)
            PostNetReceive();

         // How many charge attacks we can do randomly 1-3
         NumChargeAttacks = Rand(2) + 1;

         SetGroundSpeed(FMin(fRageSpeedCap, GetOriginalGroundSpeed() * fRageSpeed));
      }
   }
 
   // gotta go fast
   simulated function SetGroundSpeed(float NewGroundSpeed)
   {
      GroundSpeed = FMin(fRageSpeedCap, GetOriginalGroundSpeed() * fRageSpeed);
      fCurGroundSpeed = GroundSpeed;

      //warn("Speed:"@GroundSpeed@"Time:"@Level.TimeSeconds);
   }

   // prepare for the taunt
   function FKillTauntStart(int FAlivePlayers)
   {
      if (!bZapped && !fFrozen)
      {
         FTauntAlivePlayers = FAlivePlayers - 1;

         if (FTauntAlivePlayers > 0)
         {
            //warn("Charging Setting to 1:"@Level.TimeSeconds);
            FShouldTaunt = 1;
         }

         else
         {
            //warn("Charging Setting to 0:"@Level.TimeSeconds);
            FShouldTaunt = 0;
         }
      }

      EndState();
      GoToState('');
   }


   function EndState()
   {
      if (bZapped)
         Global.SetGroundSpeed(GetOriginalGroundSpeed() * ZappedSpeedMod);

      else
         Global.SetGroundSpeed(GetOriginalGroundSpeed());

      //warn("bChargingPlayer FALSE 3 time:"@Level.TimeSeconds);
      bChargingPlayer  = False;
      ChargeDamage     = 0;
      FFrustration     = 0;

      if (bZapped || fFrozen)
      {
         //warn("Charging reset Taunt at endstate:"@Level.TimeSeconds);
         FShouldTaunt = 0;
      }

      if (FShouldTaunt == 1)
      {
         //warn("Charging Start Taunt at end state:"@Level.TimeSeconds);
         fTauntStateEndTime = Level.TimeSeconds;
         FShouldTaunt = 2;
      }

      if (Level.NetMode != NM_DedicatedServer)
         PostNetReceive();

      LastChargeTime = Level.TimeSeconds;

      //warn("WALKING ANIM 3"@Level.TimeSeconds);
      MovementAnims[0] = default.MovementAnims[0];
      MovementAnims[1] = default.MovementAnims[1];
      MovementAnims[2] = default.MovementAnims[2];
      MovementAnims[3] = default.MovementAnims[3];
   }

   // attempt to fix charging animations still 
   simulated function SetZappedBehavior()
   {
      bChargingPlayer  = False;

      //warn("WALKING ANIM 4"@Level.TimeSeconds);
      MovementAnims[0] = default.MovementAnims[0];
      MovementAnims[1] = default.MovementAnims[1];
      MovementAnims[2] = default.MovementAnims[2];
      MovementAnims[3] = default.MovementAnims[3];

      if (Level.NetMode != NM_DedicatedServer)
         PostNetReceive();

      Global.SetZappedBehavior();
   }


   function Tick(float Delta)
   {  
      // reset total zap after a while
      if (!bZapped && TotalZap > 0 && Level.TimeSeconds >= LastZapTime + fZapResetTime)
         TotalZap = 0;

      if (NumChargeAttacks <= 0)
      {
         //warn("END 1"@Level.TimeSeconds);
         EndState();
         GoToState('');
      }

      if (bZapped || fFrozen)
      {

         NumChargeAttacks = 0;
         //warn("END 2"@Level.TimeSeconds);
         EndState();
         GoToState('');
      }

      else
      {

         SetGroundSpeed(FMin(fRageSpeedCap, GetOriginalGroundSpeed() * fRageSpeed));

         // Keep the pat moving toward its target when attacking
         if (Role == ROLE_Authority && bShotAnim)
         {
            if(bChargingPlayer)
            {
               bChargingPlayer = false;

               if (Level.NetMode!=NM_DedicatedServer)
                  PostNetReceive();
            }

            if (LookTarget != None)
               Acceleration = AccelRate * Normal(LookTarget.Location - Location);
         }

         else
         {
            if (!bChargingPlayer)
            {
               bChargingPlayer = true;

               if (Level.NetMode != NM_DedicatedServer)
                  PostNetReceive();
            }
         }
      }

      Global.Tick(Delta);
   }

   function bool MeleeDamageTarget(int hitdamage, vector pushdir)
   {
      local bool RetVal;

      NumChargeAttacks--;

      RetVal = Global.MeleeDamageTarget(hitdamage, pushdir*1.5);

      if (RetVal)
         GoToState('');

      return RetVal;
   }

Begin:
   if (!bZapped && !fFrozen)
   {
      FFrustration     = 0;
      FChaingunEndTime = 0;
      FMissileEndTime  = 0;
      NumChargeAttacks = Rand(2) + 1;


      //warn("CHARGING ANIM 3"@Level.TimeSeconds);
      MovementAnims[0] = ChargingAnim;
      MovementAnims[1] = ChargingAnim;
      MovementAnims[2] = ChargingAnim;
      MovementAnims[3] = ChargingAnim;

      SetGroundSpeed(FMin(fRageSpeedCap, GetOriginalGroundSpeed() * fRageSpeed));

      // modify charge time based on difficulty
      if (Level.Game != none)
      {
         if (Level.Game.GameDifficulty < 3.0)  // normal
            FCurChargeTime = FNormalChargeTime;

         else if (Level.Game.GameDifficulty < 5.0) // hard
            FCurChargeTime = FHardChargeTime;

         else if (Level.Game.GameDifficulty < 6.0) // suicidal
            FCurChargeTime = FSuiChargeTime;

         else if (Level.Game.GameDifficulty < 8.0) // hoe
            FCurChargeTime = FHOEChargeTime;

         else                                      // bb
            FCurChargeTime = FBBChargeTime;

         FCurChargeTime += Rand(RandomChargeTime); // give the charge time some randomness

         if (FLockedChargeTgt) // frustration addition
            FCurChargeTime += FFrustChargeTime;
      }

      //warn("CHARGE START:"@FCurChargeTime@"- Time:"@Level.TimeSeconds);

      Sleep(FCurChargeTime);
      FFrustration     = 0;
   }

   EndState();
   GoToState('');
}

state AfterTaunt extends Charging
{
Begin:
   FChaingunEndTime   = 0;
   FMissileEndTime    = 0;
   fStairsSecondCheck = False;
   fLastCanAttackTime = Level.TimeSeconds;

   SetGroundSpeed(FMin(fRageSpeedCap, GetOriginalGroundSpeed() * fRageSpeed));

   FCurChargeTime = 0.2;

   Sleep(FCurChargeTime);
   FFrustration   = 0;

   EndState();
   GoToState('');

}

function BeginHealing()
{
   MonsterController(Controller).WhatToDoNext(55);
}


state Healing // Healing, trying to ignore more stuff
{
   Ignores RangedAttack, ClawDamageTarget;

   // for some reason I can't ignore this... sadface.
   function FalkRangedAttack(Actor A)
   {
   }

   // don't do this
   function FStairFixJump()
   {
      fStairsSecondCheck = False;
      fLastCanAttackTime = Level.TimeSeconds;
   }

   function Tick(float Delta)
   {
      Super(FalkMonster).Tick(Delta);

      fStairsSecondCheck = False;
      fLastCanAttackTime = Level.TimeSeconds;
   }

   function bool ShouldChargeFromDamage()
   {
      return false;
   }

   function BeginState()
   {
      //warn("WALKING ANIM 5"@Level.TimeSeconds);
      MovementAnims[0] = default.MovementAnims[0];
      MovementAnims[1] = default.MovementAnims[1];
      MovementAnims[2] = default.MovementAnims[2];
      MovementAnims[3] = default.MovementAnims[3];

      //warn("bChargingPlayer FALSE 4 time:"@Level.TimeSeconds);
      bChargingPlayer = false;

      SetGroundSpeed(GetOriginalGroundSpeed());

      if (Level.NetMode != NM_DedicatedServer)
         PostNetReceive();
   }

Begin:

   if (Health <= FHealLevel)
      Sleep(GetAnimDuration('Heal'));

   FHealEndTime = Level.TimeSeconds;

   GoToState('');
}

state KnockDown // Knocked
{
   // don't do this
   function FStairFixJump()
   {
      fStairsSecondCheck = False;
      fLastCanAttackTime = Level.TimeSeconds;
   }

   function bool ShouldChargeFromDamage()
   {
      return false;
   }

   function EndState()
   {
      fStairsSecondCheck = False;
      fLastCanAttackTime = Level.TimeSeconds;
   }

Begin:
   if (Health > 0 && !fFrozen && !bZapped)
   {
      Sleep(GetAnimDuration('KnockDown'));
      CloakBoss();
      PlaySound(sound'KF_EnemiesFinalSnd.Patriarch.Kev_SaveMe', SLOT_Talk, 2.0,,500.0);

      if (KFGameType(Level.Game).FinalSquadNum == SyringeCount)
      {
         FLastBuddies++;
         KFGameType(Level.Game).AddBossBuddySquad();
      }

      GotoState('Escaping');
   }
   else
   {
      GotoState('');
   }
}

// lolol u ded
state KillTaunt
{
   Ignores RangedAttack, ClawDamageTarget;

   // for some reason I can't ignore this... sadface.
   function FalkRangedAttack(Actor A)
   {
   }

   // don't do this
   function FStairFixJump()
   {
      fStairsSecondCheck = False;
      fLastCanAttackTime = Level.TimeSeconds;
   }

   function bool ShouldChargeFromDamage()
   {
      return false;
   }

   simulated function Tick(float DeltaTime)
   {
      Super(FalkMonster).Tick(DeltaTime);
      Acceleration = vect(0,0,0);
      Velocity = vect(0,0,0);
      AirSpeed = 0;
      WaterSpeed = 0;
      SetGroundSpeed(1);
      fStairsSecondCheck = False;
      fLastCanAttackTime = Level.TimeSeconds;

      if (Controller != none)
         Controller.FocalPoint = fTFocalPoint;

      if (FTauntDuration > 0)
      {
         FTauntDuration -= DeltaTime;
         //warn(FTauntDuration@"Time:"@Level.TimeSeconds);
      }

      else
      {
         //warn("END"@Level.TimeSeconds);
         FTauntDuration = Default.FTauntDuration;

         if (Controller != none && FalkBossController(Controller) != none)
         {
            Controller.GoToState('ZombieHunt');

            FalkBossController(Controller).fRandomEnemy = true;
            FalkBossController(Controller).FindNewEnemy(); 
         }

         EndState();
         //GoToState('');
         //GotoState('Charging');
         GotoState('AfterTaunt');
      }
   }

Begin:
   if (Health > 0)
   {
      //warn("KillTaunt State Start:"@Level.TimeSeconds);
      FTauntDuration = GetAnimDuration('KillTaunt') + 0.3;
   }

   else
   {
      //warn("KillTaunt State End:"@Level.TimeSeconds);
      GoToState('');
   }
}


// lololol u all ded
state fVictoryLaugh extends KillTaunt
{
Begin:
   if (Health > 0)
   {
      //warn("VictoryLaugh State Start:"@Level.TimeSeconds);
      FTauntDuration = GetAnimDuration('VictoryLaugh');
   }

   else
      GoToState('');
}

// don't charge while escaping
state Escaping extends Charging
{
   // If we are really escaping, just update the count
   function FKillTauntStart(int FAlivePlayers)
   {
      if (Health <= FHealLevel)
      {
         //warn("From:"@FTauntAlivePlayers@"To:"@FAlivePlayers@"Time:"@Level.TimeSeconds);
         FTauntAlivePlayers = FAlivePlayers - 1;
      }

      else if (!bZapped && !fFrozen)
      {
         FTauntAlivePlayers = FAlivePlayers - 1;

         if (FTauntAlivePlayers > 0)
         {
            //warn("Escaping Setting to 1:"@Level.TimeSeconds);
            FShouldTaunt = 1;
         }

         else
         {
            //warn("Escaping Setting to 0:"@Level.TimeSeconds);
            FShouldTaunt = 0;
         }

         EndState();
         GoToState('');
      }
   }

   // call charging tick instead of global
   function FChargingTick(float Delta)
   {
      Super.Tick(Delta);

      // reset total zap after a while
      if (!bZapped && TotalZap > 0 && Level.TimeSeconds >= LastZapTime + fZapResetTime)
         TotalZap = 0;
   }

   function BeginHealing()
   {
      if (Health <= FHealLevel)
      {
         //warn("Heal Start: "@Level.TimeSeconds);
         //bShotAnim = true;
         //Acceleration = vect(0,0,0);
         //SetAnimAction('Heal');
         //Controller.GoToState('WaitForAnim');
         //FHealEndTime = Level.TimeSeconds;

         //GoToState('Healing');

         //warn("bChargingPlayer FALSE 5 time:"@Level.TimeSeconds);
         bChargingPlayer = False;
         Global.SetGroundSpeed(GetOriginalGroundSpeed());

         FNextStateTime = Level.TimeSeconds + FStateAnimDelay;
         FNextState = 'Healing';

         if (Level.NetMode != NM_DedicatedServer)
            PostNetReceive();

         EndState();
         GoToState('');
      }

      else if (!fFrozen && !bZapped)
      {
         fEscapeToCharging = True;
         //warn("Charging 6"@Level.TimeSeconds);
         GoToState('Charging');
      }
   }

   function FalkRangedAttack(Actor A)
   {
      if (bShotAnim)
         return;

      else if (Level.TimeSeconds > FLastHCTime + HCCooldown && IsCloseEnuf(A))
      {
         if (bCloaked)
            UnCloakBoss();

         FLastHCTime  = Level.TimeSeconds;
         bShotAnim    = true;
         Acceleration = vect(0,0,0);
         Acceleration = (A.Location-Location);

         if (FRand() <= 0.2)
            SetAnimAction('MeleeClaw2');

         else
            SetAnimAction('MeleeClaw');

         //PlaySound(sound'Claw2s', SLOT_None); Claw2s
      }
   }

   function bool MeleeDamageTarget(int hitdamage, vector pushdir)
   {
      return Global.MeleeDamageTarget(hitdamage, pushdir*1.5);
   }

   function bool ShouldChargeFromDamage()
   {
      return false;
   }

   function Tick(float Delta)
   {
      // Keep the pat moving toward its target when attacking
      if(Role == ROLE_Authority && bShotAnim)
      {
         if (bChargingPlayer)
         {
            bChargingPlayer = false;

            if (Level.NetMode != NM_DedicatedServer)
               PostNetReceive();
         }

         SetGroundSpeed(GetOriginalGroundSpeed());
      }

      else
      {
         if( !bChargingPlayer )
         {
            bChargingPlayer = true;

            if (Level.NetMode != NM_DedicatedServer)
               PostNetReceive();
         }

         SetGroundSpeed(GetOriginalGroundSpeed() * 2.5);
      }

      Global.Tick(Delta);
   }

   function EndState()
   {
      FFrustration     = 0;
      FLockedChargeTgt = False;
      SetGroundSpeed(GetOriginalGroundSpeed());

      // don't set bChargingPlayer to false if we're charging anyway
      if (fEscapeToCharging)
      {
         //warn("bChargingPlayer SKIP time:"@Level.TimeSeconds);
         fEscapeToCharging = False;
      }

      else
      {
         //warn("bChargingPlayer FALSE 6 time:"@Level.TimeSeconds);
         bChargingPlayer   = False;
      }

      if (bZapped || fFrozen || Health <= FHealLevel)
      {
         //warn("Escaping reset Taunt at endstate:"@Level.TimeSeconds);
         FShouldTaunt = 0;
      }

      if (FShouldTaunt == 1)
      {
         //warn("Escaping Start Taunt at end state:"@Level.TimeSeconds);
         fTauntStateEndTime = Level.TimeSeconds;
         FShouldTaunt = 2;
      }

      //fBlockRageAnimChange = True;

      if (Level.NetMode != NM_DedicatedServer)
         PostNetReceive();

      //fBlockRageAnimChange = False;

      if (bCloaked)
         UnCloakBoss();

   }

Begin:
   FLockedChargeTgt = False;
   FFrustration     = 0;
   FShouldTaunt     = 0;

   if (Health > FHealLevel)
   {
      if (!fFrozen && !bZapped)
      {
         fEscapeToCharging = True;
         //warn("Escape Charging:"@Level.TimeSeconds);
         GoToState('Charging');
      }
   }

   else if (!FBuddies && KFGameType(Level.Game).FinalSquadNum == SyringeCount)
   {
      FBuddies = True;
      FLastBuddies++;
      PatriarchKnockDown();
      KFGameType(Level.Game).AddBossBuddySquad();
   }

   While (true)
   {
      Sleep(0.5);
      if (!bCloaked && !bShotAnim)
         CloakBoss();

      if (!Controller.IsInState('SyrRetreat') && !Controller.IsInState('WaitForAnim'))
         Controller.GoToState('SyrRetreat');
   }
}

// marco stuff, should enable sneak around (tweak speed here)
state SneakAround extends Escaping
{
   function BeginHealing()
   {
      MonsterController(Controller).WhatToDoNext(56);
      GoToState('');
   }

   function bool MeleeDamageTarget(int hitdamage, vector pushdir)
   {
      local bool RetVal;

      NumChargeAttacks = 0;

      RetVal = super.MeleeDamageTarget(hitdamage, pushdir);

      GoToState('');
      return RetVal;
   }

   // call charging tick instead of global
   function Tick(float Delta)
   {  
      FChargingTick(Delta);
   }

   function BeginState()
   {
      NumChargeAttacks = 1;
      super.BeginState();
      SneakStartTime   = Level.TimeSeconds+10.f+FRand()*15.f;
   }

   function EndState()
   {
      if (!fEscapeToCharging)
      {
         if (bZapped)
            Global.SetGroundSpeed(GetOriginalGroundSpeed() * ZappedSpeedMod);

         else
            Global.SetGroundSpeed(GetOriginalGroundSpeed());
      }

      //warn("SNEAK END:"@Level.TimeSeconds);

      FFrustration      = 0;
      FLockedChargeTgt  = False;
      Super.EndState();
      bHidden = false;
      LastSneakedTime = Level.TimeSeconds+20.f+FRand()*30.f;

      if (Controller != None && Controller.IsInState('PatFindWay'))
         Controller.GoToState('ZombieHunt');
   }


   function TakeDamage( int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
   {
      global.TakeDamage(Damage,instigatedBy,hitlocation,vect(0,0,0),damageType,HitIndex);

      if (Health <= 0)
         return;

      // if someone close up is shooting us, just charge them
      if (InstigatedBy != none && VSizeSquared(Location - InstigatedBy.Location) < 62500 && !fFrozen && !bZapped)
      {
         //warn("Charging Sneak:"@Level.TimeSeconds);
         fEscapeToCharging = True;
         GoToState('Charging');
      }
   }

   simulated function UnCloakBoss()
   {
      bHidden = false;
      Super.UnCloakBoss();
   }

Begin:
   CloakBoss();
   FFrustration     = 0;
   FLockedChargeTgt = False;
   SneakStartTime   = Level.TimeSeconds+30.f;
   NumChargeAttacks = 1;

   if (FGetSyringeCount() >= SneakStartWave && Rand(10) <= AltPathChance)
      FalkHardPatController(Controller).FindPathAround();

   While(true)
   {
      Sleep(0.5);

      if (!bCloaked && !bShotAnim)
         CloakBoss();

      else if (bCloaked && FGetSyringeCount() >= 2)
         bHidden = true;

      if (!fFrozen && !bZapped && Level.TimeSeconds > SneakStartTime || NumChargeAttacks <= 0)
      {
         EndState();
         fEscapeToCharging = True;
         GoToState('Charging');
      }

      if(!Controller.IsInState('WaitForAnim') && !Controller.IsInState('ZombieHunt'))
         Controller.GoToState('ZombieHunt');
   }
}

State InitialSneak extends Charging
{
Begin:
   While(true)
   {
      Sleep(0.5);
      SneakCount++;

      // no loitering for more than one minute at the beginning - Falk689
      if (FalkBossController(Controller) != none)
      {
         if (SneakCount > 120 || FalkBossController(Controller).bAlreadyFoundEnemy)
            GoToState('Charging');

         else
            FalkBossController(Controller).FindNewEnemy(); 
      }

      if (!Controller.IsInState('InitialHunting') && !Controller.IsInState('WaitForAnim'))
         Controller.GoToState('InitialHunting');
   }
}

simulated function DropNeedle()
{
   if( CurrentNeedle!=None )
   {
      DetachFromBone(CurrentNeedle);
      CurrentNeedle.SetLocation(GetBoneCoords('Rpalm_MedAttachment').Origin);
      CurrentNeedle.DroppedNow();
      CurrentNeedle = None;
   }
}

simulated function NotifySyringeA()
{
   if (Level.NetMode != NM_Client)
   {
      //log("Heal Part 1");

      if (SyringeCount < 3)
         SyringeCount++;

      if (Level.NetMode != NM_DedicatedServer)
         PostNetReceive();
   }

   if (Level.NetMode != NM_DedicatedServer)
   {
      DropNeedle();
      CurrentNeedle = Spawn(Class'BossHPNeedle');
      AttachToBone(CurrentNeedle,'Rpalm_MedAttachment');
   }
}

// never overheal again pls
function NotifySyringeB()
{
   if (Level.NetMode != NM_Client)
   {
      //log("Heal Part 2");

      Health      += Min(FHealthMax - Health, HealingAmount);
      HeadHealth   = Health;
      bHealed      = True;
      FKnockedDown = False;

      if (SyringeCount < 3)
         FBuddies     = False;
   }
}

simulated function NotifySyringeC()
{
   if (Level.NetMode != NM_Client)
   {
      //log("Heal Part 3");
      FSyringeCount = SyringeCount;

      if (Controller != none && FalkBossController(Controller) != none && !fFrozen && !bZapped)
      {
         FalkBossController(Controller).fRandomEnemy = true;
         FalkBossController(Controller).FindNewEnemy(); 
         //warn("Heal Charging"@Level.TimeSeconds);
         GotoState('Charging');
      }
   }

   else if  (CurrentNeedle != None)
   {
      CurrentNeedle.Velocity = vect(-45,300,-90) >> Rotation;
      DropNeedle();
   }
}

simulated function PostNetReceive()
{ 
   if (bClientMiniGunning != bMinigunning)
   {
      bClientMiniGunning = bMinigunning;
      // Hack so Patriarch won't go out of MG Firing to play his idle anim online
      if( bMinigunning )
      {
         IdleHeavyAnim='FireMG';
         IdleRifleAnim='FireMG';
         IdleCrouchAnim='FireMG';
         IdleWeaponAnim='FireMG';
         IdleRestAnim='FireMG';
      }
      else
      {
         IdleHeavyAnim='BossIdle';
         IdleRifleAnim='BossIdle';
         IdleCrouchAnim='BossIdle';
         IdleWeaponAnim='BossIdle';
         IdleRestAnim='BossIdle';
      }
   }


   if (bClientCharg != bChargingPlayer)
   {
      bClientCharg = bChargingPlayer;

      if (bChargingPlayer)
      {
         //warn("CHARGING ANIM 1"@Level.TimeSeconds);
         MovementAnims[0] = ChargingAnim;
         MovementAnims[1] = ChargingAnim;
         MovementAnims[2] = ChargingAnim;
         MovementAnims[3] = ChargingAnim;
      }

      else if (!bChargingPlayer)
      {
         //warn("WALKING ANIM 1"@Level.TimeSeconds);
         MovementAnims[0] = default.MovementAnims[0];
         MovementAnims[1] = default.MovementAnims[1];
         MovementAnims[2] = default.MovementAnims[2];
         MovementAnims[3] = default.MovementAnims[3];
      }
   }

   else if(ClientSyrCount != SyringeCount)
   {
      ClientSyrCount = SyringeCount;
      Switch( SyringeCount )
      {
         Case 1:
            SetBoneScale(3,0,'Syrange1');
         Break;
         Case 2:
            SetBoneScale(3,0,'Syrange1');
         SetBoneScale(4,0,'Syrange2');
         Break;
         Case 3:
            SetBoneScale(3,0,'Syrange1');
         SetBoneScale(4,0,'Syrange2');
         SetBoneScale(5,0,'Syrange3');
         Break;
Default: // WTF? reset...?
         SetBoneScale(3,1,'Syrange1');
         SetBoneScale(4,1,'Syrange2');
         SetBoneScale(5,1,'Syrange3');
         Break;
      }
   }
   else if( TraceHitPos!=vect(0,0,0) )
   {
      AddTraceHitFX(TraceHitPos);
      TraceHitPos = vect(0,0,0);
   }
   else if( bClientCloaked!=bCloaked )
   {
      bClientCloaked = bCloaked;
      bCloaked = !bCloaked;

      if (bCloaked)
         UnCloakBoss();

      else
         CloakBoss();

      bCloaked = bClientCloaked;
   }
}

simulated function int DoAnimAction(name AnimName)
{
   if (fFrozen)
      return 0;

   // additional time for the pre fire since it can still glitch when freezing
   if (AnimName=='PreFireMG')
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay + fPreFireAddDelay;
      Super(FalkMonsterBase).DoAnimAction(AnimName);
   }

   if (AnimName=='FireMG' && FGetSyringeCount() > 0)
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;
      AnimBlendParams(1, 1.0, 0.0,, FireRootBone, True);
      PlayAnim('FireMG',, 0.f, 1);
      return 1;
   }

   else if (AnimName=='FireEndMG')
   {
      FCStopMoving = true;
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;
      //SetBoneDirection(FireRootBone,rot(0,0,0),,0,0);
      AnimBlendParams(1, 0);
   }

   if (AnimName=='MeleeImpale' || AnimName=='MeleeClaw' || AnimName=='MeleeClaw2'|| AnimName=='transition')
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;
      AnimBlendParams(1, 1.0, 0.0,, SpineBone1);
      PlayAnim(AnimName,, 0.1, 1);
      Return 1;
   }

   else if (AnimName=='RadialAttack')
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;
      // Get rid of blending, this is a full body anim
      AnimBlendParams(1, 0.0);
      PlayAnim(AnimName,,0.1);
      return 0;
   }

   Return Super.DoAnimAction(AnimName);
}


simulated event SetAnimAction(name NewAction)
{
   local int meleeAnimIndex;

   if (NewAction == '' || fFrozen)
      return;

   if (NewAction == 'Claw')
   {
      meleeAnimIndex = Rand(3);

      if (meleeAnimIndex <= 0)
         meleeAnimIndex = 1;

      NewAction = meleeAnims[meleeAnimIndex];
      CurrentDamtype = ZombieDamType[meleeAnimIndex];
   }

   ExpectingChannel = DoAnimAction(NewAction);

   if (Controller != none)
   {
      FalkBossController(Controller).AnimWaitChannel = ExpectingChannel;
   }

   if(AnimNeedsWait(NewAction))
   {
      //warn("Anim Needs Wait:"@NewAction@"Time:"@Level.TimeSeconds);
      bWaitForAnim = true;
   }

   else
   {
      //warn("Anim Doesn't Need Wait:"@NewAction@"Time:"@Level.TimeSeconds);
      bWaitForAnim = false;
   }

   if (Level.NetMode != NM_Client)
   {
      AnimAction = NewAction;
      bResetAnimAct = True;

      ResetAnimActTime = Level.TimeSeconds+0.3;
   }
}

// The animation is full body and should set the bWaitForAnim flag
simulated function bool AnimNeedsWait(name TestAnim)
{
   if (TestAnim == 'FireMG' /*|| TestAnim == 'FireEndMG'*/)
      return FGetSyringeCount() <= 0;

   if (/*TestAnim == 'FireMG' ||*/ TestAnim == 'PreFireMG' || TestAnim == 'PreFireMissile' || TestAnim == 'FireEndMG' || TestAnim == 'FireEndMissile' ||
         TestAnim == 'Heal' || TestAnim == 'KnockDown' || TestAnim == 'Entrance' || TestAnim == 'VictoryLaugh' || TestAnim == 'RadialAttack' || TestAnim == 'KillTaunt')
   {
      return true;
   }

   return false;
}

simulated function HandleBumpGlass()
{
}

// nope
function bool FlipOver()
{
   Return False;
}

// Return true if we want to charge from taking too much damage
function bool ShouldChargeFromDamage()
{
   // we're frozen and should kinda stay put
   if (fFrozen || bZapped)
      return false;

   // we just ended the entrance
   if (FEntranceStartCFix > 0)
      return false;

   // we just fired the chaingun, don't charge for a while
   if (Level.TimeSeconds < FChaingunEndTime + FChaingunEndCFix)
      return false;

   // we just melted, don't charge 
   if (Level.TimeSeconds < FFreezeEndTime + FFreezeEndCFix)
      return false;

   // we just fired a missile, don't charge for a while
   if (Level.TimeSeconds < FMissileEndTime + FMissileEndCFix)
      return false;

   // we just fired a missile, don't charge for a while
   if (Level.TimeSeconds < FRadialEndTime + FRadialEndCFix)
      return false;

   // we just jumped, don't charge for a while
   if (Level.TimeSeconds < FJumpEndTime + FJumpEndCFix)
   {
      //FFrustration = 0;
      return false;
   }

   // we just healed, don't charge for a while
   if (Level.TimeSeconds < FHealEndTime + FHealEndCFix)
      return false;

   // don't charge if we want to heal
   if (SyringeCount < 3 && Health <= FHealLevel)
      return false;

   // do your thing
   else if (!bChargingPlayer && Level.TimeSeconds - LastForceChargeTime > (5.0 + 5.0 * FRand()))
      return true;

   return false;
}

function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
   local float DamagerDistSq;
   local float UsedPipeBombDamScale;
   local KFHumanPawn P;
   local int NumPlayersSurrounding;
   local bool bDidRadialAttack;

   // not sure we need this but better safe than taco
   if (damageType == class'DamTypeBossLAWProj')
      return;

   // Prevent damage from other zeds
   if (FalkMonster(InstigatedBy) == none)
   {
      // Check for melee exploiters trying to surround the patriarch
      if (Level.TimeSeconds - LastMeleeExploitCheckTime > 1.0 && (class<DamTypeMelee>(damageType) != none || class<KFProjectileWeaponDamageType>(damageType) != none))
      {
         LastMeleeExploitCheckTime = Level.TimeSeconds;
         NumLumberJacks = 0;
         NumNinjas = 0;

         foreach DynamicActors(class'KFHumanPawn', P)
         {
            // look for guys attacking us within 3 meters
            if (VSize(P.Location - Location) < 150)
            {
               NumPlayersSurrounding++;

               if(P != none && P.Weapon != none)
               {
                  if(Axe(P.Weapon) != none || Chainsaw(P.Weapon) != none)
                     NumLumberJacks++;

                  else if(Katana(P.Weapon) != none)
                     NumNinjas++;
               }

               if(!bDidRadialAttack && NumPlayersSurrounding >= 3)
               {
                  bDidRadialAttack = true;
                  GotoState('RadialAttack');
                  break;
               }
            }
         }
      }

      // Scale damage from the pipebomb down a bit if lots of pipe bomb damage happens
      // at around the same times. Prevent players from putting all their pipe bombs
      // in one place and owning the patriarch in one blow.
      if ( class<DamTypePipeBomb>(damageType) != none )
      {
         UsedPipeBombDamScale = FMax(0,(1.0 - PipeBombDamageScale));

         PipeBombDamageScale += 0.075;

         if (PipeBombDamageScale > 1.0)
            PipeBombDamageScale = 1.0;

         Damage *= UsedPipeBombDamScale;
      }

      Super.TakeDamage(Damage, instigatedBy, hitlocation, Momentum, damageType, HitIndex);

      if (Level.TimeSeconds - LastDamageTime > 10)
         ChargeDamage = 0;

      else
      {
         LastDamageTime = Level.TimeSeconds;
         ChargeDamage += Damage;
      }

      if (!fFrozen && !bZapped && !IsInState('Charging') && Level.TimeSeconds > FChaingunEndTime + FChaingunEndCFix && Level.TimeSeconds > FMissileEndTime + FMissileEndCFix &&
            Level.TimeSeconds > FRadialEndTime + FRadialEndCFix && Level.TimeSeconds > FJumpEndTime + FJumpEndCFix && Class<KFWeaponDamageType>(damageType).Default.bIsMeleeDamage) // check for meleelockers and charge them
      {
         if (Level.TimeSeconds - FLastMeleeTime < 3 && FLastMeleeHitBy == InstigatedBy)
         {
            if (Health > FHealLevel || SyringeCount > 2)
            { 
               //warn("MeleeLockCharge"@Level.TimeSeconds);
               FMeleeLockCharge  = True;
               GoToState('Charging');
            }

            else
               GotoState('Escaping');
         }

         else
         {
            FLastMeleeHitBy = InstigatedBy; 
            FLastMeleeTime  = Level.TimeSeconds;
         }
      }

      if (!IsInState('Charging') && ShouldChargeFromDamage() && ChargeDamage > 200 && LastForceChargeTime + 10.0 < Level.TimeSeconds)
      {
         // If someone close up is shooting us, just charge them
         if( InstigatedBy != none )
         {
            DamagerDistSq = VSizeSquared(Location - InstigatedBy.Location);

            if( DamagerDistSq < (700 * 700) )
            {
               //SetAnimAction('transition');
               ChargeDamage=0;
               LastForceChargeTime = Level.TimeSeconds;
               //warn("Charging 7"@Level.TimeSeconds);
               GoToState('Charging');
               return;
            }
         }
      }

      if (Health <= 0 || SyringeCount >= 3 || IsInState('Escaping') || IsInState('KnockDown') || IsInState('RadialAttack'))
         return;

      // new knock down implementation with attempted fix for a weird bug happening with damage resistance
      if (Health <= FHealLevel)
      { 
         if (!FKnockedDown)
         {
            FLastKnockDownTime = Level.TimeSeconds;
            FKnockedDown = True;
            bShotAnim = true;
            Acceleration = vect(0,0,0);
            SetAnimAction('KnockDown');
            KFMonsterController(Controller).bUseFreezeHack = True;
            Controller.GoToState('WaitForAnim');
            GoToState('KnockDown');
         }

         else if (!fFrozen && !bZapped)
            GotoState('Escaping');

         if (!FBuddies && KFGameType(Level.Game).FinalSquadNum == SyringeCount)
         {
            FBuddies = True;
            FLastBuddies++;
            KFGameType(Level.Game).AddBossBuddySquad();
         }
      }
   }

   //log(Health);
}

function DoorAttack(Actor A)
{
   if (bShotAnim)
      return;

   else if (A != none)
   {
      Controller.Target = A;
      GotoState('DoorBashing');
   }
   
   //Controller.Target = A;
   //FalkStartFireMissile();

   /*else if (A != None)
   {
      Controller.Target = A;
      bShotAnim = true;
      bWaitForAnim = true;
      Acceleration = vect(0,0,0);
      KFMonsterController(Controller).bUseFreezeHack = True;
      SetAnimAction('PreFireMissile');
      Controller.GoToState('WaitForAnim');
      GoToState('FireMissile');
   }*/
}

function PlayDirectionalHit(Vector HitLoc);

// Shouldn't fight with anything but humans
function bool SameSpeciesAs(Pawn P)
{
   return (KFHumanPawn(P) == none);
}

// Creapy endgame camera when the evil wins, reset vars and do useful stuff
function bool SetBossLaught()
{
   local Controller C;

   FShouldTaunt       = 0;
   FTauntAlivePlayers = 0;
   bShotAnim = true;
   Acceleration = vect(0,0,0);
   SetAnimAction('VictoryLaugh');
   Controller.GoToState('WaitForAnim');
   bIsBossView = True;
   bSpecialCalcView = True;
   GoToState('fVictoryLaugh');
   For( C=Level.ControllerList; C!=None; C=C.NextController )
   {
      if( PlayerController(C)!=None )
      {
         PlayerController(C).SetViewTarget(Self);
         PlayerController(C).ClientSetViewTarget(Self);
         PlayerController(C).ClientSetBehindView(True);
      }
   }
   Return True;
}

simulated function bool SpectatorSpecialCalcView(PlayerController Viewer, out Actor ViewActor, out vector CameraLocation, out rotator CameraRotation)
{
   Viewer.bBehindView = True;
   ViewActor = Self;
   CameraRotation.Yaw = Rotation.Yaw-32768;
   CameraRotation.Pitch = 0;
   CameraRotation.Roll = Rotation.Roll;
   CameraLocation = Location + (vect(80,0,80) >> Rotation);
   Return True;
}

// Overridden to do a cool slomo death view of the patriarch dying
function Died(Controller Killer, class<DamageType> damageType, vector HitLocation)
{
   local Controller C;

   local Class<KFWeaponDamageType> KFWDT;

   if (HeadHealth <= 0 && SeveredHead == none)
   {
      KFWDT = Class<KFWeaponDamageType>(damageType);

      if (KFWDT != none)
         DecapFX(HitLocation, Rotation, KFWDT.Default.bIsMeleeDamage);
   }

   AmbientSound = none;

   Super.Died(Killer,damageType,HitLocation);

   KFGameType(Level.Game).DoBossDeath();

   For( C=Level.ControllerList; C!=None; C=C.NextController )
   {
      if( PlayerController(C)!=None )
      {
         PlayerController(C).SetViewTarget(Self);
         PlayerController(C).ClientSetViewTarget(Self);
         PlayerController(C).bBehindView = true;
         PlayerController(C).ClientSetBehindView(True);
      }
   }
}

// less weird shit here
function ClawDamageTarget()
{
    local vector PushDir;
    local name Anim;
    local float frame,rate;
    local bool bDamagedSomeone;
    local KFHumanPawn P;
    local Actor OldTarget;
    local int FMeleeDamage;

    fLastCanAttackTime = Level.TimeSeconds;

    if (fFrozen)
        return;

    // just damage doors pls
    if (Controller.Target != none && Controller.Target.IsA('KFDoorMover'))
    {
        MeleeDamageTarget(FalkGetDamage(MeleeDamage), damageForce * Normal(P.Location - Location));
        PlaySound(MeleeAttackHitSound, SLOT_Interact, 2.0);
        return;
    }

    GetAnimParams(1, Anim,frame,rate);
    //FFrustration     = 0;
    FLockedChargeTgt = False;

    if (Anim == 'MeleeImpale')
        MeleeRange = ImpaleMeleeDamageRange;

    else
        MeleeRange = ClawMeleeDamageRange;

    if (Controller!=none && Controller.Target!=none)
        PushDir = (damageForce * Normal(Controller.Target.Location - Location));

    else
        PushDir = damageForce * vector(Rotation);

    if (Anim == 'MeleeImpale')
    {
        FMeleeDamage = FImpaleDamage;
        CurrentDamtype = ZombieDamType[1];
        bDamagedSomeone = MeleeDamageTarget(FMeleeDamage, PushDir);
    }

    else
    {
        FMeleeDamage = FClawDamage;

        if (Anim == 'MeleeClaw')
            CurrentDamtype = ZombieDamType[2];
        else
            CurrentDamtype = FMinigunBluntDT;

        OldTarget = Controller.Target;

        foreach DynamicActors(class'KFHumanPawn', P)
        {
            if ((P.Location - Location) dot PushDir > 0.0)
            {
                Controller.Target = P;
                bDamagedSomeone = bDamagedSomeone || MeleeDamageTarget(FMeleeDamage, damageForce * Normal(P.Location - Location));
            }
        }

        Controller.Target = OldTarget;
    }

    MeleeRange = Default.MeleeRange;

    if (bDamagedSomeone)
    {
        if (Anim == 'MeleeImpale')
            PlaySound(MeleeImpaleHitSound, SLOT_Interact, 2.0);

        else
            PlaySound(MeleeAttackHitSound, SLOT_Interact, 2.0);
    }
}

simulated function ProcessHitFX()
{
   local Coords boneCoords;
   local class<xEmitter> HitEffects[4];
   local int i,j;
   local float GibPerterbation;

   if( (Level.NetMode == NM_DedicatedServer) || bSkeletized || (Mesh == SkeletonMesh))
   {
      SimHitFxTicker = HitFxTicker;
      return;
   }

   for ( SimHitFxTicker = SimHitFxTicker; SimHitFxTicker != HitFxTicker; SimHitFxTicker = (SimHitFxTicker + 1) % ArrayCount(HitFX) )
   {
      j++;
      if ( j > 30 )
      {
         SimHitFxTicker = HitFxTicker;
         return;
      }

      if( (HitFX[SimHitFxTicker].damtype == None) || (Level.bDropDetail && (Level.TimeSeconds - LastRenderTime > 3) && !IsHumanControlled()) )
         continue;

      //log("Processing effects for damtype "$HitFX[SimHitFxTicker].damtype);

      if( HitFX[SimHitFxTicker].bone == 'obliterate' && !class'GameInfo'.static.UseLowGore())
      {
         SpawnGibs( HitFX[SimHitFxTicker].rotDir, 1);
         bGibbed = true;
         // Wait a tick on a listen server so the obliteration can replicate before the pawn is destroyed
         if( Level.NetMode == NM_ListenServer )
         {
            bDestroyNextTick = true;
            TimeSetDestroyNextTickTime = Level.TimeSeconds;
         }
         else
         {
            Destroy();
         }
         return;
      }

      boneCoords = GetBoneCoords( HitFX[SimHitFxTicker].bone );

      if ( !Level.bDropDetail && !class'GameInfo'.static.NoBlood() && !bSkeletized && !class'GameInfo'.static.UseLowGore() )
      {
         //AttachEmitterEffect( BleedingEmitterClass, HitFX[SimHitFxTicker].bone, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir );

         HitFX[SimHitFxTicker].damtype.static.GetHitEffects( HitEffects, Health );

         if( !PhysicsVolume.bWaterVolume ) // don't attach effects under water
         {
            for( i = 0; i < ArrayCount(HitEffects); i++ )
            {
               if( HitEffects[i] == None )
                  continue;

               AttachEffect( HitEffects[i], HitFX[SimHitFxTicker].bone, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir );
            }
         }
      }
      if ( class'GameInfo'.static.UseLowGore() )
         HitFX[SimHitFxTicker].bSever = false;

      if( HitFX[SimHitFxTicker].bSever )
      {
         GibPerterbation = HitFX[SimHitFxTicker].damtype.default.GibPerterbation;

         switch( HitFX[SimHitFxTicker].bone )
         {
            case 'obliterate':
               break;

            case LeftThighBone:
               if( !bLeftLegGibbed )
               {
                  SpawnSeveredGiblet( DetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone) );
                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  KFSpawnGiblet( class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  bLeftLegGibbed=true;
               }
               break;

            case RightThighBone:
               if( !bRightLegGibbed )
               {
                  SpawnSeveredGiblet( DetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone) );
                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  KFSpawnGiblet( class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  bRightLegGibbed=true;
               }
               break;

            case LeftFArmBone:
               if( !bLeftArmGibbed )
               {
                  SpawnSeveredGiblet( DetachedSpecialArmClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone) );
                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  KFSpawnGiblet( class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;;
                  bLeftArmGibbed=true;
               }
               break;

            case RightFArmBone:
               if( !bRightArmGibbed )
               {
                  SpawnSeveredGiblet( DetachedArmClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone) );
                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  KFSpawnGiblet( class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  bRightArmGibbed=true;
               }
               break;

            case 'head':
               if( !bHeadGibbed )
               {
                  if ( HitFX[SimHitFxTicker].damtype == class'DamTypeDecapitation' )
                  {
                     DecapFX( boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false);
                  }
                  else if( HitFX[SimHitFxTicker].damtype == class'DamTypeProjectileDecap' )
                  {
                     DecapFX( boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false, true);
                  }
                  else if( HitFX[SimHitFxTicker].damtype == class'DamTypeMeleeDecapitation' )
                  {
                     DecapFX( boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, true);
                  }

                  bHeadGibbed=true;
               }
               break;
         }


         if( HitFX[SimHitFXTicker].bone != 'Spine' && HitFX[SimHitFXTicker].bone != FireRootBone &&
               HitFX[SimHitFXTicker].bone != 'head' && Health <=0 )
            HideBone(HitFX[SimHitFxTicker].bone);
      }
   }
}

// Give the Patriarch his own health variables
function float DifficultyHealthModifier()
{
   if (Level.Game.GameDifficulty >= 8.0) // bb
      return 2.0;

   else if (Level.Game.GameDifficulty >= 7.0) // hoe
      return 1.75;

   else if (Level.Game.GameDifficulty >= 5.0) // suicidal
      return 1.5;

   else if (Level.Game.GameDifficulty >= 4.0) // hard
      return 1.25;

   return 1.0;
}

// nothing here
static simulated function PreCacheMaterials(LevelInfo myLevel)
{
}

// fZedUsed stuff
function byte ExpIndex(PlayerController PC)
{
   local byte i;

   for (i=0; i<fExpList.length; i++) 
   {
      //log("existing ID: "@fZedUsed[i].SteamID);

      if (fExpList[i].PC == PC)
      {
         //log("found");
         return i;
      }
   }

   fExpList.insert(i, 1);
   fExpList[i].PC           = PC;
   fExpList[i].NTicks       = 0;
   fExpList[i].MTicks       = 0;

   //log("adding new player");
   //log(i);

   return i;
}

// I like my Pat rare
simulated function ZombieCrispUp()
{
   bAshen = true;
   bCrispified = true;
   SetBurningBehavior();
}

// Don't do shit for a while on unfreeze
function fUnfreezeFix()
{
   FFreezeEndTime = Level.TimeSeconds;
}

// start a delayed kill taunt
function FKillTauntStart(int FAlivePlayers)
{
   FTauntAlivePlayers = FAlivePlayers - 1;
   FShouldTaunt = 2;

   if (fTauntStateEndTime < Level.TimeSeconds)
      fTauntStateEndTime = Level.TimeSeconds;
}

// taunt players when you kill someone
function FKillTaunt()
{
   local Falk689GameTypeBase Flk;

   Flk = Falk689GameTypeBase(Level.Game);

   // taunt only if there's at least one breathing player
   if (Flk != none)
   {
      FTauntAlivePlayers = Flk.GetAlivePlayers();
      //warn("Last Update:"@FTauntAlivePlayers@"Time:"@Level.TimeSeconds);
   }

   fTFocalPoint = Location + 512 * vector(Rotation);
   FShouldTaunt = 3;

   if (FTauntAlivePlayers <= 0)
   {
      //warn("Taunt Return");
      return;
   }

   // reset ignore pawn so we really search for a new enemy
   if (FalkBossController(Controller) != None)
   {
      //FalkBossController(Controller).fIgnorePawn  = none;
      FalkBossController(Controller).fRandomEnemy = True;
      FalkBossController(Controller).fRandomDist  = False;
   }

   bShotAnim        = false;
   bWaitForAnim     = false;
   AnimEnd(2);
   AnimEnd(1);
   AnimEnd(0);
   //fClientKillTaunt();

   if (Physics==PHYS_Falling)
      SetPhysics(PHYS_Walking);

   bShotAnim = true;
   SetAnimAction('KillTaunt');
   Acceleration = vect(0, 0, 0);
   Velocity.X = 0;
   Velocity.Y = 0;
   Controller.GoToState('WaitForAnim');
   KFMonsterController(Controller).bUseFreezeHack = True;
   AirSpeed = 0;
   WaterSpeed = 0; 
   SetGroundSpeed(1);
   Controller.FocalPoint = fTFocalPoint;
   GoToState('KillTaunt');
}

// taunt replicated on client, hopefully it should work like knockdown on other zeds
simulated function fClientKillTaunt()
{
   bShotAnim        = false;
   bWaitForAnim     = false;
   AnimEnd(2);
   AnimEnd(1);
   AnimEnd(0);

   if (Physics == PHYS_Falling)
      SetPhysics(PHYS_Walking);

   bShotAnim        = true;
   SetAnimAction('KillTaunt');
   Acceleration     = vect(0, 0, 0);
   Velocity.X       = 0;
   Velocity.Y       = 0;
}


// reset taunt state on freeze
function fFreezeFix()
{
   //warn("Freeze Reset");
   FShouldTaunt = 0;
   Super.fFreezeFix();
}

// revert ground speed after state end
function fResetGroundSpeed()
{
   if (fFrozen)
      return;

   if (bZapped)
      SetGroundSpeed(GetOriginalGroundSpeed() * ZappedSpeedMod);

   else
      SetGroundSpeed(GetOriginalGroundSpeed());
}

// return zed class
function FalkZedClass FalkGetZedClass()
{
   return Falk_Boss;
}

// vanilla function since we don't burn and we don't want to introduce bugs
simulated function DecapFX(Vector DecapLocation, Rotator DecapRotation, bool bSpawnDetachedHead, optional bool bNoBrainBits)
{
   local float GibPerterbation;
   local BrainSplash SplatExplosion;
   local int i;

   // Do the cute version of the Decapitation
   if (class'GameInfo'.static.UseLowGore())
   {
      CuteDecapFX();
      return;
   }

   bNoBrainBitEmitter = bNoBrainBits;

   GibPerterbation = 0.060000; // damageType.default.GibPerterbation;

   if (bSpawnDetachedHead)
      SpecialHideHead();

   else
      HideBone(HeadBone);

   if (bSpawnDetachedHead)
      SpawnSeveredGiblet(DetachedHeadClass, DecapLocation, DecapRotation, GibPerterbation, GetBoneRotation(HeadBone));

   // Plug in headless anims if we have them
   for (i = 0; i < 4; i++)
   {
      if (HeadlessWalkAnims[i] != '' && HasAnim(HeadlessWalkAnims[i]))
      {
         MovementAnims[i] = HeadlessWalkAnims[i];
         WalkAnims[i]     = HeadlessWalkAnims[i];
      }
   }

   if (!bSpawnDetachedHead && !bNoBrainBits && EffectIsRelevant(DecapLocation,false))
   {
      KFSpawnGiblet(class 'KFMod.KFGibBrain',DecapLocation, self.Rotation, GibPerterbation, 250);
      KFSpawnGiblet(class 'KFMod.KFGibBrainb',DecapLocation, self.Rotation, GibPerterbation, 250);
      KFSpawnGiblet(class 'KFMod.KFGibBrain',DecapLocation, self.Rotation, GibPerterbation, 250);
   }

   SplatExplosion = Spawn(class 'BrainSplash',self,, DecapLocation);
}

// more vanilla stuff to hopefully prevent more bugs
simulated function SpawnGibs(Rotator HitRotation, float ChunkPerterbation)
{
   bGibbed = true;
   PlayDyingSound();

   if (class'GameInfo'.static.UseLowGore())
      return;

   if (FlamingFXs != none)
   {
      FlamingFXs.Emitters[0].SkeletalMeshActor = none;
      FlamingFXs.Destroy();
   }

   if(ObliteratedEffectClass != none)
      Spawn(ObliteratedEffectClass,,, Location, HitRotation);

   //super.SpawnGibs(HitRotation,ChunkPerterbation);

   if (FRand() < 0.1)
   {
      KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);

      SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
      SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
      SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);

      if (DetachedSpecialArmClass != None)
      {
         SpawnSeveredGiblet(DetachedSpecialArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
      }
      else
      {
         SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
      }
   }

   else if (FRand() < 0.25)
   {
      KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);

      SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
      SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
      if (FRand() < 0.5)
      {
         KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
         SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
      }
   }

   else if (FRand() < 0.35)
   {
      KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
      SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
   }
   else if (FRand() < 0.5)
   {
      KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
      SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
   }
}

// No more File cabinet surfing zombies please..
singular event BaseChange()
{
   //warn("BaseChange:"@Level.TimeSeconds);

   // we've set this delay too many times, let's chill a bit
   if (FJumpEndSetAmount >= FJumpEndSetLimit)
   {
      if (Level.TimeSeconds > FJumpEndTime + FJumpEndRFix)
         FJumpEndSetAmount = 0;

      else
      {
         if (KActor(Base) != None || Pawn(Base) != None)
            Super.JumpOffPawn();

         return;
      }
   }

   // increase the tick if we're setting this var kinda fast
   if (Level.TimeSeconds < FJumpEndTime + FJumpEndCFix)
      FJumpEndSetAmount++;

   else
      FJumpEndSetAmount = 0;

   // set the cooldown var
   FJumpEndTime = Level.TimeSeconds;

   if (KActor(Base) != None || Pawn(Base) != None)
      JumpOffPawn();
}

// Block this during rocket and chaingun animations
function FStairFixJump()
{
   if (!FBlockStairsFix)
      Super.FStairFixJump();
}


// always returns 3 on bloodbath, else returns the actual var
simulated function int FGetSyringeCount()
{
   if (FBloodbath || (Level.NetMode != NM_Client && Level.Game.GameDifficulty >= 8.0))
      return 3;

   return SyringeCount;
}

// this isn't needed but I may have a nightmare where this happens if I don't do this
function bool fSlayMe()
{
   return false;
}

// no honorable death for you
function FSeppuku()
{
}

defaultproperties
{
   ControllerClass=Class'FalkHardPatController'
}
