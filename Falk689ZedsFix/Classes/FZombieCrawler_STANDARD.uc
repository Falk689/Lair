/* LIGHT ZED */

class FZombieCrawler_STANDARD extends FalkZombieCrawler;

#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=LairCrawler_A.ukx

defaultproperties
{
   MoanVoice=SoundGroup'KF_EnemiesFinalSnd.Crawler.Crawler_Talk'
   MeleeAttackHitSound=SoundGroup'KF_EnemiesFinalSnd.Crawler.Crawler_HitPlayer'
   JumpSound=SoundGroup'KF_EnemiesFinalSnd.Crawler.Crawler_Jump'
   DetachedArmClass=Class'KFChar.SeveredArmCrawler'
   DetachedLegClass=Class'KFChar.SeveredLegCrawler'
   DetachedHeadClass=Class'KFChar.SeveredHeadCrawler'
   BurntDetachedHeadClass=Class'CrawlerSeveredBurntHead'
   BurntDetachedArmClass=Class'CrawlerSeveredBurntArm'
   BurntDetachedLegClass=Class'CrawlerSeveredBurntLeg'
   HitSound(0)=SoundGroup'KF_EnemiesFinalSnd.Crawler.Crawler_Pain'
   DeathSound(0)=SoundGroup'KF_EnemiesFinalSnd.Crawler.Crawler_Death'
   ChallengeSound(0)=SoundGroup'KF_EnemiesFinalSnd.Crawler.Crawler_Acquire'
   ChallengeSound(1)=SoundGroup'KF_EnemiesFinalSnd.Crawler.Crawler_Acquire'
   ChallengeSound(2)=SoundGroup'KF_EnemiesFinalSnd.Crawler.Crawler_Acquire'
   ChallengeSound(3)=SoundGroup'KF_EnemiesFinalSnd.Crawler.Crawler_Acquire'
   AmbientSound=Sound'KF_BaseCrawler.Crawler_Idle'
   Mesh=SkeletalMesh'LairCrawler_A.Crawler_Freak'
   Skins(0)=Combiner'LairTextures_T.ZedsUpscale.CrawlerCombinerFinal'
}
