/* BOSS ZED */

class FalkZombieBossBase extends FalkMonster
   abstract;

#exec OBJ LOAD FILE=KFPatch2.utx
#exec OBJ LOAD FILE=KF_Specimens_Trip_T.utx
#exec OBJ LOAD FILE=LairTextures_T.utx

var bool bChargingPlayer,bClientCharg,bFireAtWill,bMinigunning,bIsBossView;
var float RageStartTime,LastChainGunTime,LastMissileTime,LastSneakedTime;

var bool bClientMiniGunning;

var name ChargingAnim;		// How he runs when charging the player.
var byte ClientSyrCount;

var int MGFireCounter;

var vector TraceHitPos;
var Emitter mTracer,mMuzzleFlash;
var bool bClientCloaked;
var float LastCheckTimes;
var int HealingLevels[3],HealingAmount;

var(Sounds)     sound   RocketFireSound;    // The sound of the rocket being fired
var(Sounds)     sound   MiniGunFireSound;   // The sound of the minigun being fired
var(Sounds)     sound   MiniGunSpinSound;   // The sound of the minigun spinning
var(Sounds)     sound   MeleeImpaleHitSound;// The sound of melee impale attack hitting the player

var             float   MGFireDuration;     // How long to fire for this burst
var             float   MGLostSightTimeout; // When to stop firing because we lost sight of the target
var()           float   MGDamage;           // How much damage the MG will do

var()           float   ClawMeleeDamageRange;// How long his arms melee strike is
var()           float   ImpaleMeleeDamageRange;// How long his spike melee strike is

var             float   LastChargeTime;     // Last time the patriarch charged
var             float   LastForceChargeTime;// Last time patriarch was forced to charge
var             int     NumChargeAttacks;   // Number of attacks this charge
var             float   ChargeDamage;       // How much damage he's taken since the last charge
var             float   LastDamageTime;     // Last Time we took damage

// Sneaking
var             float   SneakStartTime;     // When did we start sneaking
var             int     SneakCount;         // Keep track of the loop that sends the boss to initial hunting state

// PipeBomb damage
var()           float    PipeBombDamageScale;// Scale the pipe bomb damage over time

// Falk689 stuff

var byte                       FMCDudeCheck;       // how many ticks they have to stick close while minigunning to piss us off
var byte                       FSTDudeCheck;       // how many ticks they have to stick close while doing other stuff to piss us off
var byte                       MCCooldown;         // melee charge exploit cooldown in seconds
var byte                       HCCooldown;         // healing claw attack cooldown in seconds
var byte                       BulletID;           // rocket bullet ID
var byte                       FNormalChargeTime;  // base charge time on normal
var byte                       FHardChargeTime;    // base charge time on hard
var byte                       FSuiChargeTime;     // base charge time on suicidal
var byte                       FHOEChargeTime;     // base charge time on hell on earth
var byte                       FBBChargeTime;      // base charge time on bloodbath
var byte                       FFrustChargeTime;   // frustration charge time addition
var byte                       RandomChargeTime;   // random charge time addition
var byte                       FCurChargeTime;     // current computed charge time
var int                        FHealthMax;         // initial health
var int                        FNumPlayers;        // store numplayers so our rocket isn't nerfed if someone quits
var int                        MissilesAmount[4];  // how many missiles we fire in a row
var int                        AltPathChance;      // random alt path chance (min=0 max=10)
var int                        SneakStartWave;     // starting wave to sneak behind players
var int                        FMGDamage;          // chaingun damage override to prevent weird shit to happen
var int                        FClawDamage;        // melee damage override to prevent weird shit to happen
var int                        FImpaleDamage;      // melee damage override to prevent weird shit to happen
var int                        FRadialDamage;      // radial damage override to prevent weird shit to happen
var int                        FLastMCTime;        // last time we charged melee exploiters
var int                        FLastHCTime;        // last time we attacked while escaping
var int                        FBaseFrustration;   // base frustration value for the random setting of FCurMaxFrustration
var int                        FAddFrustration;    // additional frustration value for the random setting of FCurMaxFrustration
var int                        FRadialRange;       // radial attack range
var int                        FMissileZeroPrec;   // rocket precision on healing wave zero
var int                        FMissileOnePrec;    // rocket precision on healing wave one
var int                        FMissileTwoPrec;    // rocket precision on healing wave two
var int                        FMissileThreePrec;  // rocket precision on healing wave three
var int                        FMissileDoorPrec;   // rocket precision against doors
var int                        FStuckTime;         // max stuck time before trying to free ourselves
var int                        FJumpEndSetAmount;  // how many time we've set JumpEndTime in a rapid succession
var int                        FJumpEndSetLimit;   // limit to set JumpEndTime in a rapid succession
var vector                     FLastPos;           // stored position to check if we're stuck
var float                      FStuckDist;         // tolerance use to consider ourselves stuck
var float                      FHealLevelPercent;  // how much health should remain us to go healing, expressed in a float zero to one where one is one hundred percent
var float                      FEntranceEndCFix;   // entrance end charge fix time, basically a cooldown to stop us from charging while putting down the chaingun
var float                      FEntranceStartCFix; // entrance start fix time, used to keep immortality till the entrance is over
var float                      FChaingunEndTime;   // when the chaingun state ended
var float                      FChaingunEndCFix;   // chaingun end charge fix time, basically a cooldown to stop us from charging while putting down the chaingun
var float                      FRadialEndTime;     // when the chaingun state ended
var float                      FRadialEndCFix;     // radial end charge fix time, basically a cooldown to stop us from charging after a radial
var float                      FMissileEndTime;    // when the missile state ended
var float                      FMissileEndCFix;    // missile end charge fix time, basically a cooldown to stop us from charging while putting down the rocket launcher
var float                      FJumpEndTime;       // when the jump anim was called
var float                      FJumpEndCFix;       // jump delay rocket time, basically a cooldown to stop us from firing a rocket or a chaingun or charging or welp, kinda whatever
var float                      FJumpEndRFix;       // how much time to wait to reset FJumpEndSetAmount
var float                      FFreezeEndTime;     // when the freeze ended
var float                      FFreezeEndCFix;     // fix time after freeze
var float                      FFakeZTTime;        // last time we checked and we were still in fake zed time
var float                      FFakeZTTDelay;      // fake zed time taunt delay, we speak with a small delay 
var float                      FHealEndCFix;       // heal end charge fix time, basically a cooldown to stop us from charging while putting down the rocket launcher
var float                      FHealEndTime;       // heal end time, to prevent the pat from sliding after heal
var float                      FCurMaxFrustration; // current max frustration before charging to the closest enemy
var float                      FFrustration;       // current frustration level
var float                      FLastMeleeTime;     // last time this dude hit me
var float                      fPreFireAddDelay;   // additional freeze delay for the pre fire animation
var Pawn                       FLastMeleeHitBy;    // dude that hit me with melee last time
var float                      FLastCMeleeTime;    // last time this dude hit me while charging
var Pawn                       FLastCMeleeHitBy;   // dude that hit me with melee last time while charging
var byte                       FCMeleeHits;        // how many time the dude hit me while charging
var float                      FLastKnockDownTime; // last time we knocked down
var float                      FNormalMWS;         // minigun walk speed multiplier on normal
var float                      FHardMWS;           // minigun walk speed multiplier on hard
var float                      FSuicidalMWS;       // minigun walk speed multiplier on suicidal
var float                      FHOEMWS;            // minigun walk speed multiplier on hell on earth
var float                      FBBMWS;             // minigun walk speed multiplier on bloodbath
var float                      FHealMWS;           // minigun walk speed additional multiplier for healing waves
var float                      FMZeroPM;           // minigun no heal precision multiplier
var float                      FMOnePM;            // minigun first heal precision multiplier
var float                      FMTwoPM;            // minigun second heal zero precision multiplier
var float                      FMThreePM;          // minigun third heal zero precision multiplier
var float                      FMExpCheck;         // how much time we wait before checking minigun exploiters
var float                      FMCurCheck;         // how much time just passed since last check
var float                      FMERandTgt;         // chance for random target while melee exploting
var float                      FKDSwearChance;     // how likely is our pat to say the F and A words when his asshole is knocked the fuck down 
var float                      FRSwearChance;      // how likely is our pat to say the B word when firing the bitchin' rocket 
var bool                       FLockedChargeTgt;   // have we locked a target for frustration charging?
var bool                       FKnockedDown;       // was the pat already knocked down
var bool                       FBuddies;           // got zeds? basically used as a first fix to always spawn the healing wave
var byte                       FLastBuddies;       // last spawned healing wave
var class<Projectile>          FBossProjClass;     // our projectile class
var float                      FAttackCooldown;    // cooldown between attacks since apparently waitforanim does horse shit with this
var float                      FLastAttackTime;    // how much time ago we attacked last time
var class<DamTypeZombieAttack> FMinigunBluntDT;    // damage type to use when the pat breaks your face with his minigun
var float                      FTauntDelay;        // delay after the state ended to start the taunt
var float                      FTauntDuration;     // kill taunt duration
var bool                       FShouldTalk;        // used to mute the pat after he won
var int                        FTauntAlivePlayers; // how many players are currently alive
var float                      FChaingunTDelay;    // chaingun taunt additional delay
var float                      FMissileTDelay;     // missile taunt additional delay
var float                      FMissileLaunchTime; // when the last missile was launched
var float                      FChargingTDelay;    // charging delay, don't change state randomly
var name                       FNextState;         // next state name
var float                      FMinMissileDist;    // minimum distance to fire a missile against a player
var bool                       FCStopMoving;       // stop moving while firing the minigun
var byte                       FCBaseZero;         // base chaingun bullets numbers on healing wave zero
var byte                       FCBaseOne;          // base chaingun bullets numbers on healing wave one
var byte                       FCBaseTwo;          // base chaingun bullets numbers on healing wave two
var byte                       FCBaseThree;        // base chaingun bullets numbers on healing wave three
var byte                       FCRandom;           // random chaingun bullets amount
var float                      FMLChargeTime;      // charge time when running against a meleelocker
var bool                       FMeleeLockCharge;   // was the last charge a melee lock one?
var float                      FStateAnimDelay;    // slight delay to trigger animations before a change state (minigun, missile)
var float                      FNextStateTime;     // when to trigger the next state
var float                      FMultiScalingAdd;   // additional difficulty scaling used for multiplayer
var float                      FPlayerScalingAdd;  // additional difficulty scaling added per player alive at pat spawn
var float                      FNormalScalingVal;  // base difficulty scaling used at normal difficulty
var float                      FHardScalingVal;    // base difficulty scaling used at hard difficulty
var float                      FSuiScalingVal;     // base difficulty scaling used at suicidal difficulty
var float                      FHOEScalingVal;     // base difficulty scaling used at hell on earth difficulty
var float                      FBBScalingVal;      // base difficulty scaling used at bloodbath difficulty
var float                      fBossAttackDelay;   // time between FalkFire calls
var float                      fChainMissileCD;    // cooldown between chaingun and missile fire
var rotator                    FStartMissileAim;   // missile aim computed before the delay
var float                      FChainResetChance;  // FFrustration reset chance while firing the minigun, checked against a fRand (0 to 1)
var float                      FRocketResetChance; // FFrustration reset chance while firing the missile, checked against a fRand (0 to 1)
var float                      FMeleeResetChance;  // FFrustration reset chance while firing the missile, checked against a fRand (0 to 1)
var float                      FRadialResetChance; // FFrustration reset chance while firing the missile, checked against a fRand (0 to 1)
var bool                       FBloodbath;         // if true we're on bloodbath, replicated to the client to fix the minigun anim
var float                      FDoorDamageIncr;    // damage increment against doors per hit
var float                      FCurDoorDamagePerc; // current damage increment on doors
var float                      FDoorDamageReset;   // how much time in seconds we need
var float                      FDoorMGDamagePerc;  // percentage of damage dealt to doors from chaingun bullets
var float                      FDoorBashStartTime; // time when the door bash state started
var float                      FDoorBashMaxTime;   // time in seconds to stop bashing the door if we haven't destroyed it yet


// exploiters list struct
struct fExploitStr
{
   var PlayerController PC; // our lovely exploiter
   var byte NTicks;         // how much he pissed us off during daily activities
   var byte MTicks;         // how much he pissed us off while minigunning
};

var transient array<fExploitStr> fExpList;

// marco stuff
var transient float GiveUpTime;
var byte            MissilesLeft;

replication
{
   reliable if (Role==ROLE_Authority)
      bChargingPlayer, TraceHitPos, bMinigunning, bIsBossView, FKnockedDown, FRadialDamage, FLastKnockDownTime, FLastMeleeTime, FMGDamage, FMCurCheck, FLastMCTime, FTauntDuration,
      FBuddies, FClawDamage, FImpaleDamage, FCurMaxFrustration, FLockedChargeTgt, FCurChargeTime, FLastBuddies, FShouldTalk, FTauntAlivePlayers, MGFireCounter, FNextStateTime, FBloodbath;
}

defaultproperties
{
   fDeathDrama=1.0
   AltPathChance=2               // 20% so he doesn't do that too often
   MissilesAmount[0]=1
   MissilesAmount[1]=1
   MissilesAmount[2]=2
   MissilesAmount[3]=2
   SneakStartWave=1
   Mass=950.000000
   ScoringValue=1000             // not like you'll ever get to spend them
   MotionDetectorThreat=1.000000 // the patriarch is capable of triggering both a pipe and an ATM
   BleedOutDuration=10.000000    // boss only, even though this isn't actually supposed to kick in with our current settings
   HeadHealth=4000
   fHSKThreshold=1.0             
   PlayerCountHealthScale=0.75
   PlayerNumHeadHealthScale=0.75
   FMGDamage=5.000000
   MeleeDamage=50
   FKnockedDown=False
   FClawDamage=50
   FImpaleDamage=60
   FRadialDamage=135
   ChargingAnim="RunF"
   HealingLevels(0)=5600
   HealingLevels(1)=3500
   HealingLevels(2)=2187
   HealingAmount=1750
   MGDamage=6.000000
   ClawMeleeDamageRange=75.000000
   ImpaleMeleeDamageRange=85.000000
   ZapResistanceScale=3.000000
   ZapThreshold=40.000000
   ZappedDamageMod=1.000000
   ZapDuration=2.5
   bHarpoonToHeadStuns=False
   bHarpoonToBodyStuns=False
   DamageToMonsterScale=5.000000
   ZombieFlag=3
   damageForce=170000
   bFatAss=True
   KFRagdollName="Patriarch_Trip"
   bMeleeStunImmune=True
   CrispUpThreshhold=1
   bCanDistanceAttackDoors=False
   bUseExtendedCollision=True
   ColOffset=(Z=65.000000)
   ColRadius=27.000000
   ColHeight=25.000000
   SeveredArmAttachScale=1.100000
   SeveredLegAttachScale=1.200000
   SeveredHeadAttachScale=1.500000
   BurningWalkFAnims(0)="WalkF"
   BurningWalkFAnims(1)="WalkF"
   BurningWalkFAnims(2)="WalkF"
   BurningWalkAnims(0)="WalkF"
   BurningWalkAnims(1)="WalkF"
   BurningWalkAnims(2)="WalkF"
   OnlineHeadshotOffset=(X=28.000000,Z=75.000000)
   OnlineHeadshotScale=1.200000 
   bOnlyDamagedByCrossbow=True
   bBoss=True
   IdleHeavyAnim="BossIdle"
   IdleRifleAnim="BossIdle"
   RagDeathVel=80.000000
   RagDeathUpKick=100.000000
   MeleeRange=10.000000
   GroundSpeed=130.000000
   WaterSpeed=130.000000
   HealthMax=4000.000000         // should be 64750 at 12men HoE
   Health=4000
   HeadScale=1.300000
   MenuName="Patriarch"
   MovementAnims(0)="WalkF"
   MovementAnims(1)="WalkF"
   MovementAnims(2)="WalkF"
   MovementAnims(3)="WalkF"
   AirAnims(0)="JumpInAir"
   AirAnims(1)="JumpInAir"
   AirAnims(2)="JumpInAir"
   AirAnims(3)="JumpInAir"
   TakeoffAnims(0)="JumpTakeOff"
   TakeoffAnims(1)="JumpTakeOff"
   TakeoffAnims(2)="JumpTakeOff"
   TakeoffAnims(3)="JumpTakeOff"
   LandAnims(0)="JumpLanded"
   LandAnims(1)="JumpLanded"
   LandAnims(2)="JumpLanded"
   LandAnims(3)="JumpLanded"
   AirStillAnim="JumpInAir"
   TakeoffStillAnim="JumpTakeOff"
   IdleCrouchAnim="BossIdle"
   IdleWeaponAnim="BossIdle"
   IdleRestAnim="BossIdle"
   DrawScale=1.050000
   PrePivot=(Z=3.000000)
   SoundVolume=75
   bNetNotify=False
   RotationRate=(Yaw=36000,Roll=0)
   FIsBoss=True
   fHeadScale=1.1
   fNoHeadScale=1.1
   FNormalMWS=0.5
   FHardMWS=0.55
   FSuicidalMWS=0.6
   FHOEMWS=0.65
   FBBMWS=0.7
   FHealMWS=0.05
   FMExpCheck=0.5
   FMCDudeCheck=6
   FSTDudeCheck=4
   MCCooldown=6
   HCCooldown=4
   FMERandTgt=0.3
   FMZeroPM=0.125               // ideally short range only
   FMOnePM=0.1                  // ideally short and rarely medium range
   FMTwoPM=0.075                // ideally medium and rarely long range
   FMThreePM=0.05               // ideally any range
   FRadialRange=170
   FBossProjClass=class'BossLAWProjFalk'
   ZombieDamType(0)=Class'DamTypeBossRadialFalk'
   ZombieDamType(1)=Class'DamTypeBossImpaleFalk'
   ZombieDamType(2)=Class'DamTypeBossClawFalk'
   FMinigunBluntDT=Class'DamTypeBossMinigunBluntFalk'
   FBaseFrustration=15
   FAddFrustration=5
   FChainResetChance=0.5
   FRocketResetChance=0.5
   FMeleeResetChance=1.0
   FRadialResetChance=1.0
   FMissileZeroPrec=600
   FMissileOnePrec=500
   FMissileTwoPrec=400
   FMissileThreePrec=300
   FMissileDoorPrec=10
   FNormalChargeTime=10
   FHardChargeTime=13
   FSuiChargeTime=16
   FHOEChargeTime=19
   FBBChargeTime=22
   FFrustChargeTime=10
   RandomChargeTime=10
   FKDSwearChance=0.14
   FRSwearChance=0.4
   fAlreadyStunned=True
   FZMultiFix=True
   fStunMult=1.0               // no relative stun for you
   FAttackCooldown=2.0
   FTauntDelay=0.1
   FTauntDuration=2.0
   FShouldTalk=True
   FChaingunTDelay=1.7
   FMissileTDelay=0.8
   FNextState=''
   FChargingTDelay=0.8
   FMinMissileDist=400000.0
   FCBaseZero=30
   FCBaseOne=50
   FCBaseTwo=70
   FCBaseThree=90
   FCRandom=30
   FMLChargeTime=5.0
   FStateAnimDelay=0.05
   FChaingunEndCFix=2.3
   FHealEndCFix=5.0
   FMissileEndCFix=2.6
   FJumpEndCFix=0.5
   FJumpEndRFix=1.0
   FJumpEndSetLimit=10
   fRageSpeed=2.4
   fNormalRageSpeed=2.4
   fHardRageSpeed=2.4
   fSuicidalRageSpeed=2.4
   fHOERageSpeed=2.4
   fBBRageSpeed=2.4
   fRageSpeedCap=312.0
   FHealLevelPercent=0.25
   FFakeZTTDelay=0.2
   FRadialEndCFix=0.7
   FEntranceStartCFix=10.0
   FEntranceEndCFix=0.8
   FBlockAllDamage=True      // should take damage only after the entrance is over
   FStuckTime=500            // this roughly means 5 seconds
   FStuckDist=30
   FFreezeEndCFix=2.0
   FMultiScalingAdd=0.1      // remember to edit BossLAWProjFalk too when tweaking difficulty scalings, if needed - The Wise Old Falk689
   FPlayerScalingAdd=0.05   
   FNormalScalingVal=0.4
   FHardScalingVal=0.5
   FSuiScalingVal=0.6
   FHOEScalingVal=0.7
   FBBScalingVal=0.8
   fStairsTimeFix=3.0
   fBossAttackDelay=0.2
   fChainMissileCD=5.0
   fFleshpoundRageImmune=True
   fPreFireAddDelay=0.3
   FDoorDamageReset=5.0
   FDoorDamageIncr=0.1     // 10%
   FCurDoorDamagePerc=0.42 // 40%
   FDoorMGDamagePerc=0.01  // 1%
   FDoorBashMaxTime=5.0
}
