/* MEDIUM ZED */

class FalkZombieSiren extends FalkZombieSirenBase
   abstract;
   
#exec OBJ LOAD FILE=LairTextures_T.utx

replication
{
   reliable if(Role == ROLE_Authority)
      fClientSetfStunReset;
}

// set screen shake vars for bloodbath skill
simulated function PostBeginPlay()
{
   if (Role == ROLE_Authority && Level.Game.GameDifficulty >= 8.0)
   {
      curRotMag              = bbRotMag;
      curRotRate             = bbRotRate;
      curOffsetMag           = bbOffsetMag;
      curOffsetRate          = bbOffsetRate;
      curShakeTime           = bbShakeTime;
      curShakeFadeTime       = bbShakeFadeTime;
      curShakeEffectScalar   = bbShakeEffectScalar;
      curMinShakeEffectScale = bbMinShakeEffectScale;
      curScreamBlurScale     = bbScreamBlurScale;
   }

   super.PostBeginPlay();

   fDoorDamage    = FScreamDoorDamage;
   fAddDoorDamage = fDoorDamage * fAddDoorDamageMulti;
}

simulated function fClientSetfStunReset()
{
   fIsStunned = False;
   fStunReset = 1;
   //warn("StunReset Start Client");
}

simulated event SetAnimAction(name NewAction)
{
   local int meleeAnimIndex;

   if (NewAction=='' || fFrozen)
   {
      //warn(NewAction@"- Time:"@Level.TimeSeconds);
      return;
   }

   if (NewAction == 'Claw')
   {
      meleeAnimIndex = Rand(3);
      NewAction = meleeAnims[meleeAnimIndex];
      CurrentDamtype = ZombieDamType[meleeAnimIndex];
   }

   ExpectingChannel = DoAnimAction(NewAction);

   if (AnimNeedsWait(NewAction))
      bWaitForAnim = true;

   else
      bWaitForAnim = false;

   /*if (Level.NetMode != NM_Client)
   {
      AnimAction = NewAction;
      bResetAnimAct = True;
      ResetAnimActTime = Level.TimeSeconds + 0.3;
   }*/

   Super(FalkMonster).SetAnimAction(NewAction);
}

// don't do this
function FStairFixJump()
{
}

simulated function bool AnimNeedsWait(name AnimName)
{
   if (AnimName == 'KnockDown')
      return true;

   return false;
}

function DoorAttack(Actor A)
{
   if (bShotAnim || Physics == PHYS_Swimming || bDecapitated || A==None)
      return;

   bShotAnim = true;
   SetAnimAction('Siren_Scream');
}


// don't interrupt the siren while screaming
/*simulated function bool HitCanInterruptAction()
{
   if ((bShotAnim || FIsScreaming || Level.TimeSeconds <= FScreamEndTime) && !bZapped)
      return false;

   return true;
}*/

function RangedAttack(Actor A)
{
   local int LastFireTime;
   local float Dist;

   //warn("bShotAnim:"@bShotAnim@"- Time:"@Level.TimeSeconds);

   if (bShotAnim || fFrozen)
      return;

   // don't do shit while headless
   if (bDecapitated && DecapitatedBy != none)
      return;

   Dist = VSize(A.Location - Location);

   if ( Physics == PHYS_Swimming )
   {
      SetAnimAction('Claw');
      bShotAnim = true;
      LastFireTime = Level.TimeSeconds;
   }
   else if ( Dist < MeleeRange + CollisionRadius + A.CollisionRadius )
   {
      bShotAnim = true;
      LastFireTime = Level.TimeSeconds;
      SetAnimAction('Claw');
      Controller.bPreparingMove = true;
      Acceleration = vect(0,0,0);
   }
   else if( Dist <= ScreamRadius && !bDecapitated && !bZapped )
   {
      bShotAnim=true;
      SetAnimAction('Siren_Scream');
      // Only stop moving if we are close
      if( Dist < ScreamRadius * 0.25 )
      {
         Controller.bPreparingMove = true;
         Acceleration = vect(0,0,0);
      }
      else
      {
         Acceleration = AccelRate * Normal(A.Location - Location);
      }
   }
}

simulated function int DoAnimAction(name AnimName)
{
   if (fFrozen)
      return 0;

   fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;

   if (AnimName=='Siren_Scream' || AnimName=='Siren_Bite' || AnimName=='Siren_Bite2')
   {
      if (AnimName=='Siren_Scream')
         FScreamEndTime = fAnimEndTime;

      AnimBlendParams(1, 1.0, 0.0,, SpineBone1);
      PlayAnim(AnimName,, 0.1, 1);
      return 1;
   }

   PlayAnim(AnimName,,0.1);
   Return 0;
}

// start the stun reset when needed
simulated function AnimEnd(int Channel)
{
   if (fStunReset == 0 && bShotAnim && Channel==ExpectingChannel)
   {
      //warn("Stun anim end");
      fIsStunned = False;
      fStunReset = 1;
      fClientSetfStunReset();
   }

   Super.AnimEnd(Channel);
}

// Scream Time
simulated function SpawnTwoShots()
{
    if (bZapped || fFrozen)
        return;

    DoShakeEffect();

    if (Level.NetMode != NM_Client)
        HurtRadius(ScreamDamage, ScreamRadius, ScreamDamageType, ScreamForce, Location);

    FIsScreaming     = True;
    FScreamSpawnTime = Level.TimeSeconds + FScreamDelay;
}

// Shake nearby players screens
simulated function DoShakeEffect()
{
   local PlayerController PC;
   local float Dist, scale, BlurScale;

   if (fFrozen)
      return;

   //viewshake
   if (Level.NetMode != NM_DedicatedServer)
   {
      PC = Level.GetLocalPlayerController();

      if (PC != None && PC.ViewTarget != None)
      {
         Dist = VSize(Location - PC.ViewTarget.Location);
         if (Dist < ScreamRadius )
         {
            scale = (ScreamRadius - Dist) / (ScreamRadius);
            scale *= ShakeEffectScalar;
            BlurScale = scale;

            // Reduce blur if there is something between us and the siren
            if( !FastTrace(PC.ViewTarget.Location,Location) )
            {
               scale *= 0.25;
               BlurScale = scale;
            }

            else
               scale = Lerp(scale,MinShakeEffectScale,1.0);

            PC.SetAmbientShake(Level.TimeSeconds + ShakeFadeTime, ShakeTime, OffsetMag * Scale, OffsetRate, RotMag * Scale, RotRate);

            if(KFHumanPawn(PC.ViewTarget) != none)
               KFHumanPawn(PC.ViewTarget).AddBlur(ShakeTime, BlurScale * ScreamBlurScale);

            // 10% chance of player saying something about our scream
            if (Level != none && Level.Game != none && !KFGameType(Level.Game).bDidSirenScreamMessage && FRand() < 0.10)
            {
               PC.Speech('AUTO', 16, "");
               KFGameType(Level.Game).bDidSirenScreamMessage = true;
            }
         }
      }
   }
}


simulated function HurtRadius(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation)
{
    local actor Victims;
    local float damageScale, dist;
    local vector dir;
    local float UsedDamageAmount;
    local array<KFDoorMover> fHitDoors;
    local KFDoorMover fDoor;
    local int i;

    if (bHurtEntry || fFrozen)
        return;

    bHurtEntry = true;

    foreach VisibleCollidingActors(class'Actor', Victims, DamageRadius, HitLocation)
    {
        // don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
        // Or Karma actors in this case. Self inflicted Death due to flying chairs is uncool for a zombie of your stature.
        if (Victims != self && !Victims.IsA('FluidSurfaceInfo') && !Victims.IsA('KFMonster') && !Victims.IsA('ExtendedZCollision'))
        {
            dir         = Victims.Location - HitLocation;
            dist        = FMax(1,VSize(dir));
            dir         = dir/dist;
            damageScale = 1 - FMax(0,(dist - Victims.CollisionRadius) / DamageRadius);
            Momentum    = 0;

            if (Victims.IsA('KFGlassMover'))
                UsedDamageAmount = 100000; // Siren always shatters glass

            else if (Victims.IsA('KFDoorMover') && KFDoorMover(Victims).bSealed && !KFDoorMover(Victims).bDoorIsDead && KFDoorMover(Victims).Health > 0)
            {
                warn("SEALED DOOR");
                fHitDoors[fHitDoors.Length] = KFDoorMover(Victims);
                UsedDamageAmount = FalkGetDamage(FScreamDoorDamage, KFDoorMover(Victims));
            }

            else
                UsedDamageAmount = DamageAmount * damageScale;

            Victims.TakeDamage(UsedDamageAmount, Instigator, Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dir, (damageScale * Momentum * dir), DamageType);

            if (Instigator != None && Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
                Vehicle(Victims).DriverRadiusDamage(UsedDamageAmount, DamageRadius, Instigator.Controller, DamageType, Momentum, HitLocation);
        }
    }

    bHurtEntry = false;

    // make sure we hit the targeted door
    if (Controller != none && KFDoorMover(Controller.Target) != none)
    {
        fDoor = KFDoorMover(Controller.Target);

        warn("SECOND");

        // don't hit it twice
        for (i=0; i<fHitDoors.Length; i++)
        {
            if (fDoor == fHitDoors[i])
                return;
        }

        fDoor.TakeDamage(FalkGetDamage(FScreamDoorDamage, fDoor), Instigator, fDoor.Location - 0.5 * (fDoor.CollisionHeight + fDoor.CollisionRadius) * dir, (damageScale * Momentum * dir), DamageType);
    }
}

// Don't do random shit and don't scream without a head 
function RemoveHead()
{
   FIsScreaming = False;
   MeleeRange   = -500;

   Super.RemoveHead();

   /*if (FRand() < 0.5)
      KilledBy(LastDamagedBy);

   else
   {
      bAboutToDie = True;
      DeathTimer  = Level.TimeSeconds+10*FRand();
   }*/
}

// reset the door damage after a while
function FalkResetDoorDamage()
{
    //warn("DELAYED RESET"@Level.TimeSeconds);
    fDoorDamage = FScreamDoorDamage;
}

simulated function Tick(float Delta)
{
   local vector FireStart;

   Super.Tick(Delta);

   if( bAboutToDie && Level.TimeSeconds>DeathTimer )
   {
      if( Health>0 && Level.NetMode!=NM_Client )
         KilledBy(LastDamagedBy);
      bAboutToDie = False;
   }

   // restart after a stun pls
   if (fStunReset > 0 && fStunReset <= 3 && !fFrozen)
   {
      fIsStunned = False;

      if (CurSirenSecStunCheck < SirenStunCheck)
         CurSirenSecStunCheck += Delta;

      else
      {
         CurSirenSecStunCheck = 0;
         fStunReset++;

         GroundSpeed  = GetOriginalGroundSpeed();
         SetGroundSpeed(GetOriginalGroundSpeed());

         if (bBurnified)
            SetGroundSpeed(GroundSpeed * fBurningSpeed);

         if (bZapped)
            SetGroundSpeed(GroundSpeed * ZappedSpeedMod);
      }
   }

   if (Role == ROLE_Authority)
   {
      if (Level.TimeSeconds > fAnimEndTime)
         bShotAnim = False;

      if (FIsScreaming && Level.TimeSeconds >= FScreamSpawnTime)
      {
         FIsScreaming = False;

         if (!fFrozen)
         { 
            FireStart      = GetBoneCoords(default.FHeadBoneName).Origin;

            FireStart.X   += FScreamDist * cos(Rotation.Yaw   * (pi / 32768));
            FireStart.Y   += FScreamDist * sin(Rotation.Yaw   * (pi / 32768));
            FireStart.Z   += FScreamDist * sin(Rotation.Pitch * (pi / 32768));

            ToggleAuxCollision(false);
            spawn(FScreamClass,,,FireStart, Rotation);
            ToggleAuxCollision(True);
         }

      }

      // attempt to start the stun reset here too
      if (fIsStunned)
      {
         if (fSirenStunTime + fStunDuration >= Level.TimeSeconds)
         {
            SetGroundSpeed(1);
            //warn("ZERO");
         }

         else
         {
            fIsStunned   = False;

            GroundSpeed  = GetOriginalGroundSpeed();
            SetGroundSpeed(GetOriginalGroundSpeed());

            if (fStunReset == 0)
            {
               fStunReset = 1;
               fClientSetfStunReset();
               //warn("StunReset Start");
            }
         }
      }

      if( bShotAnim )
      {
         SetGroundSpeed(GetOriginalGroundSpeed() * 0.65);

         if( LookTarget!=None )
         {
            Acceleration = AccelRate * Normal(LookTarget.Location - Location);
         }
      }
      else
      {
         SetGroundSpeed(GetOriginalGroundSpeed());
      }
   }
}

function PlayDyingSound()
{
   if( !bAboutToDie )
      Super.PlayDyingSound();
}

simulated function ProcessHitFX()
{
   local Coords boneCoords;
   local class<xEmitter> HitEffects[4];
   local int i,j;
   local float GibPerterbation;

   if (fFrozen)
      return;

   if( (Level.NetMode == NM_DedicatedServer) || bSkeletized || (Mesh == SkeletonMesh))
   {
      SimHitFxTicker = HitFxTicker;
      return;
   }

   for ( SimHitFxTicker = SimHitFxTicker; SimHitFxTicker != HitFxTicker; SimHitFxTicker = (SimHitFxTicker + 1) % ArrayCount(HitFX) )
   {
      j++;
      if ( j > 30 )
      {
         SimHitFxTicker = HitFxTicker;
         return;
      }

      if( (HitFX[SimHitFxTicker].damtype == None) || (Level.bDropDetail && (Level.TimeSeconds - LastRenderTime > 3) && !IsHumanControlled()) )
         continue;

      //log("Processing effects for damtype "$HitFX[SimHitFxTicker].damtype);

      if( HitFX[SimHitFxTicker].bone == 'obliterate' && !class'GameInfo'.static.UseLowGore())
      {
         SpawnGibs( HitFX[SimHitFxTicker].rotDir, 1);
         bGibbed = true;
         // Wait a tick on a listen server so the obliteration can replicate before the pawn is destroyed
         if( Level.NetMode == NM_ListenServer )
         {
            bDestroyNextTick = true;
            TimeSetDestroyNextTickTime = Level.TimeSeconds;
         }
         else
         {
            Destroy();
         }
         return;
      }

      boneCoords = GetBoneCoords( HitFX[SimHitFxTicker].bone );

      if ( !Level.bDropDetail && !class'GameInfo'.static.NoBlood() && !bSkeletized && !class'GameInfo'.static.UseLowGore())
      {
         //AttachEmitterEffect( BleedingEmitterClass, HitFX[SimHitFxTicker].bone, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir );

         HitFX[SimHitFxTicker].damtype.static.GetHitEffects( HitEffects, Health );

         if( !PhysicsVolume.bWaterVolume ) // don't attach effects under water
         {
            for( i = 0; i < ArrayCount(HitEffects); i++ )
            {
               if( HitEffects[i] == None )
                  continue;

               AttachEffect( HitEffects[i], HitFX[SimHitFxTicker].bone, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir );
            }
         }
      }
      if ( class'GameInfo'.static.UseLowGore() )
         HitFX[SimHitFxTicker].bSever = false;

      if( HitFX[SimHitFxTicker].bSever )
      {
         GibPerterbation = HitFX[SimHitFxTicker].damtype.default.GibPerterbation;

         switch( HitFX[SimHitFxTicker].bone )
         {
            case 'obliterate':
               break;

            case LeftThighBone:
               if( !bLeftLegGibbed )
               {
                  if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
                     SpawnSeveredGiblet(BurntDetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  else
                     SpawnSeveredGiblet(DetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  KFSpawnGiblet( class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  bLeftLegGibbed=true;
               }
               break;

            case RightThighBone:
               if( !bRightLegGibbed )
               {
                  if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
                     SpawnSeveredGiblet(BurntDetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  else
                     SpawnSeveredGiblet(DetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  KFSpawnGiblet( class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 ) ;
                  bRightLegGibbed=true;
               }
               break;

            case LeftFArmBone:
               break;

            case RightFArmBone:
               break;

            case 'head':
               if( !bHeadGibbed )
               {
                  if ( HitFX[SimHitFxTicker].damtype == class'DamTypeDecapitation' )
                  {
                     DecapFX( boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false);
                  }
                  else if( HitFX[SimHitFxTicker].damtype == class'DamTypeProjectileDecap' )
                  {
                     DecapFX( boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false, true);
                  }
                  else if( HitFX[SimHitFxTicker].damtype == class'DamTypeMeleeDecapitation' )
                  {
                     DecapFX( boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, true);
                  }

                  bHeadGibbed=true;
               }
               break;
         }


         if( HitFX[SimHitFXTicker].bone != 'Spine' && HitFX[SimHitFXTicker].bone != FireRootBone &&
               HitFX[SimHitFXTicker].bone != LeftFArmBone && HitFX[SimHitFXTicker].bone != RightFArmBone &&
               HitFX[SimHitFXTicker].bone != 'head' && Health <=0 )
            HideBone(HitFX[SimHitFxTicker].bone);
      }
   }
}

// don't scream while frozen
function fFreezeFix()
{
   FIsScreaming = False;

   Super.fFreezeFix();
}

// don't scream while zapped
simulated function SetZappedBehavior()
{
   FIsScreaming = False;
   Super.SetZappedBehavior();
}

// Shouldn't fight with our own and boss
function bool SameSpeciesAs(Pawn P)
{
   return (FalkZombieSiren(P) != none || Super.SameSpeciesAs(P));
}

static simulated function PreCacheMaterials(LevelInfo myLevel)
{
   myLevel.AddPrecacheMaterial(Combiner'LairTextures_T.ZedsUpscale.SirenCombinerFinal');
   myLevel.AddPrecacheMaterial(Combiner'LairTextures_T.ZedsUpscale.SirenEnvironmentCombiner');
   myLevel.AddPrecacheMaterial(Texture'LairTextures_T.ZedsUpscale.siren_diffuse');
   myLevel.AddPrecacheMaterial(Material'KF_Specimens_Trip_T.siren_hair_fb');
}

// don't stun while screaming
function bool FlipOver()
{
   if (!FIsScreaming && Level.TimeSeconds > FScreamEndTime)
         return Super.FlipOver();

   return False;
}

// return zed class
function FalkZedClass FalkGetZedClass()
{
   return Falk_Siren;
}

// don't flinch or interrupt by fire or acid and don't do shit while screaming
function PlayHit(float Damage, Pawn InstigatedBy, vector HitLocation, class<DamageType> damageType, vector Momentum, optional int HitIdx)
{
   if (HitIdx != 689 || FIsScreaming || Level.TimeSeconds <= FScreamEndTime)
      Super.PlayHit(Damage, InstigatedBy, HitLocation, damageType, Momentum, HitIdx);
}

// use our shader on crisped up zeds
simulated function ZombieCrispUp()
{
	bAshen = true;
	bCrispified = true;

	SetBurningBehavior();

	if (Level.NetMode == NM_DedicatedServer || class'GameInfo'.static.UseLowGore())
		Return;

	Skins[0]=Texture'LairTextures_T.CustomReskins.Transparency';
	Skins[1]=Shader'LairTextures_T.ZedsUpscale.ZedBurnSkinShader';
	Skins[2]=Shader'LairTextures_T.ZedsUpscale.ZedBurnSkinShader';
	Skins[3]=Shader'LairTextures_T.ZedsUpscale.ZedBurnSkinShader';
}


defaultproperties
{
   SirenStunCheck=0.1
   EventClasses(0)="Falk689ZedsFix.FZombieSiren_STANDARD"
   ControllerClass=Class'FalkSirenController'
}
