class FalkBruteController extends FMonsterController;

var     float     RageAnimTimeout;            // How long until the RageAnim is completed; Hack so the server doesn't get stuck in idle when its doing the Rage anim
var     float     RageFrustrationTimer;       // Tracks how long we have been walking toward a visible enemy
var     float     RageFrustrationThreshold;   // Base value for how long the FP should walk torward an enemy without reaching them before getting frustrated and raging
var     Pawn      fChargeEnemy;               // used to prevent switching target while charging

var     bool      bDoneSpottedCheck;

// try and set the charge target on spawn
function Tick(float DeltaTime)
{
    if (FalkMonster(Pawn) != none && !FalkMonster(Pawn).fFrozen && Enemy == None && FalkMonster(Pawn).fSpawnTarget != none)
    {
        Enemy                          = FalkMonster(Pawn).fSpawnTarget;
        Target                         = Enemy;
        fChargeEnemy                   = Enemy;
        FalkMonster(Pawn).fSpawnTarget = none;

        Super(KFMonsterController).Tick(DeltaTime);
        return;
    }

    Super.Tick(DeltaTime);
}


function TimedFireWeaponAtEnemy()
{
    if ((Enemy == None) || FireWeaponAt(Enemy))
        SetCombatTimer();

    else
        SetTimer(0.01, True);
}

function SeePlayer(Pawn Seen)
{
    if (fChargeEnemy != none && fChargeEnemy.Health > 0)
        return;

    Super.SeePlayer(Seen);
}

function DamageAttitudeTo(Pawn Other, float Damage)
{
    if (fChargeEnemy != none && fChargeEnemy.Health > 0)
        return;

    Super.DamageAttitudeTo(Other, Damage);
}

function HearNoise(float Loudness, Actor NoiseMaker)
{
    if (fChargeEnemy != none && fChargeEnemy.Health > 0)
        return;

    Super.HearNoise(Loudness, NoiseMaker);
}

event bool NotifyBump(actor Other)
{
    if (fChargeEnemy != none && fChargeEnemy.Health > 0)
    {
        Disable('NotifyBump');
        return false;
    }

    return Super.NotifyBump(Other);
}

function NotifyTakeHit(pawn InstigatedBy, vector HitLocation, int Damage, class<DamageType> damageType, vector Momentum)
{
    if (fChargeEnemy != none && fChargeEnemy.Health > 0)
    {
        SetEnemy(fChargeEnemy);
        return;
    }


    //warn("YEP 5"@Level.TimeSeconds);
    Super.NotifyTakeHit(InstigatedBy, HitLocation, Damage, damageType, Momentum);
}

function ChangeEnemy(Pawn NewEnemy, bool bCanSeeNewEnemy)
{
    if (fChargeEnemy != none && fChargeEnemy.Health > 0)
    {
        SetEnemy(fChargeEnemy);
        return;
    }

    //warn("CHANGE ENEMY"@Level.TimeSeconds);
    Super.ChangeEnemy(NewEnemy, bCanSeeNewEnemy);
}

function bool FindNewEnemy()
{
    if (fChargeEnemy != none && fChargeEnemy.Health > 0)
    {
        Enemy = fChargeEnemy;
        return true;
    }

    //warn("NEW ENEMY"@Level.TimeSeconds);
    return Super.FindNewEnemy();
}

function bool SetEnemy(Pawn NewEnemy, optional bool bHateMonster, optional float MonsterHateChanceOverride)
{
    if (fChargeEnemy != none && fChargeEnemy.Health > 0)
    {
        Enemy = fChargeEnemy;
        return true;
    }

    if (Super.SetEnemy(NewEnemy, bHateMonster, MonsterHateChanceOverride))
    {
        fChargeEnemy = NewEnemy;
        //warn("SET ENEMY"@Level.TimeSeconds);
        return true;
    }

    //warn("WUT"@Level.TimeSeconds);
    return false;
}

// select fChargeEnemy if possible
function pawn FalkPickTarget(out float bestAim, out float bestDist, vector FireDir, vector projStart, float MaxRange, optional int fZedID)
{
    if (fChargeEnemy != none && fChargeEnemy.Health > 0)
        return fChargeEnemy;

    return Super.FalkPickTarget(bestAim, bestDist, FireDir, projStart, MaxRange, fZedID);
}

// just call the superclass
function pawn SuperFalkPickTarget(out float bestAim, out float bestDist, vector FireDir, vector projStart, float MaxRange, optional int fZedID)
{
    return Super.FalkPickTarget(bestAim, bestDist, FireDir, projStart, MaxRange, fZedID);
}

function DoCharge()
{
    if(pawn != none)
    {
        if (Enemy.PhysicsVolume.bWaterVolume)
        {
            if (!Pawn.bCanSwim)
            {
                DoTacticalMove();
                return;
            }
        }

        else if (!FalkZombieBruteBase(Pawn).bChargingPlayer)
        {
            if (KFM.MeleeRange != KFM.default.MeleeRange)
                KFM.MeleeRange = KFM.default.MeleeRange;

            GotoState('BruteWalk');
        }

        else
        {
            if (KFM.MeleeRange != KFM.default.MeleeRange)
                KFM.MeleeRange = KFM.default.MeleeRange;

            GotoState('BruteCharge');
        }
    }
}

state BruteCharge extends ZombieCharge
{
    ignores EnemyNotVisible;

    function bool StrafeFromDamage(float Damage, class<DamageType> DamageType, bool bFindDest)
    {
        return false;
    }

    function bool TryStrafe(vector sideDir)
    {
        return false;
    }

    function Tick(float Delta)
    {
        Global.Tick(Delta);

        if (Pawn != none && !FalkZombieBruteBase(Pawn).bChargingPlayer)
            GotoState('BruteWalk');

        if (fChargeEnemy != none && fChargeEnemy.Health > 0)
        {
            Enemy = fChargeEnemy;
            Target = Enemy;
            Focus  = Enemy;
        }

        else if (Enemy != none && Enemy.Health > 0)
            fChargeEnemy = Enemy;
    }

    function Timer()
    {
        if (Pawn != none && !FalkZombieBruteBase(Pawn).bChargingPlayer)
        {
            GotoState('BruteWalk');
            return;
        }

        Disable('NotifyBump');

        if (fChargeEnemy != none && fChargeEnemy.Health > 0)
            Enemy = fChargeEnemy;

        else if (Enemy != none && Enemy.Health > 0)
            fChargeEnemy = Enemy;

        Target = Enemy;
        Focus  = Enemy;
        TimedFireWeaponAtEnemy();
    }

    function BeginState()
    {
        fChargeEnemy = Enemy;
        super.BeginState();
        RageFrustrationThreshold = default.RageFrustrationThreshold + (Frand() * 5);
    }
}

state BruteWalk extends ZombieCharge
{
    function bool StrafeFromDamage(float Damage, class<DamageType> DamageType, bool bFindDest)
    {
        return false;
    }

    function bool TryStrafe(vector sideDir)
    {
        return false;
    }

    function SeePlayer(Pawn Seen)
    {
        Super.SeePlayer(Seen);
    }

    function DamageAttitudeTo(Pawn Other, float Damage)
    {
        Super.DamageAttitudeTo(Other, Damage);
    }

    function HearNoise(float Loudness, Actor NoiseMaker)
    {
        Super.HearNoise(Loudness, NoiseMaker);
    }

    event bool NotifyBump(actor Other)
    {
        return Super.NotifyBump(Other);
    }

    function NotifyTakeHit(pawn InstigatedBy, vector HitLocation, int Damage, class<DamageType> damageType, vector Momentum)
    {
        Super.NotifyTakeHit(InstigatedBy, HitLocation, Damage, damageType, Momentum);
    }

    function ChangeEnemy(Pawn NewEnemy, bool bCanSeeNewEnemy)
    {
        Super.ChangeEnemy(NewEnemy, bCanSeeNewEnemy);
    }

    function bool FindNewEnemy()
    {
        return Super.FindNewEnemy();
    }

    function bool SetEnemy(Pawn NewEnemy, optional bool bHateMonster, optional float MonsterHateChanceOverride)
    {
        return Super.SetEnemy(NewEnemy, bHateMonster, MonsterHateChanceOverride);
    }

    function pawn FalkPickTarget(out float bestAim, out float bestDist, vector FireDir, vector projStart, float MaxRange, optional int fZedID)
    {
        return SuperFalkPickTarget(bestAim, bestDist, FireDir, projStart, MaxRange, fZedID);
    }

    function Timer()
    {
        Disable('NotifyBump');
        Target = Enemy;
        TimedFireWeaponAtEnemy();
    }

    function BeginState()
    {
        super.BeginState();
        RageFrustrationThreshold = default.RageFrustrationThreshold + (Frand() * 5);
    }
}


defaultproperties
{
    RageFrustrationThreshold=10.000000
}
