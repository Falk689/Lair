
#exec OBJ LOAD FILE=LairTextures_T.utx

class FalkMonster extends FalkMonsterBase
    abstract;

var Pawn                              DecapitatedBy;                // who decapitated me
var bool                              DecapitatedByMelee;           // was i decapitated by melee?
var int                               DecapitatedByID;              // bullet id who decapitated me
var int                               LastBulletHitID;              // last bullet hit id for every shot

// plasma kaboom stuff
var bool                              fPlasmaSpawned;               // have we spawned a plasma nade from our head today?
var bool                              fShouldPlasma;                // well, should we? 
var bool                              fShouldSetHitLoc;             // used to prevent changing where to spawn the plasma nade
var bool                              fZedTimeBlock;                // trigger zed time chance only if false
var Vector                            fHitLoc;                      // where to spawn the plasma nade

// TakeDamage fix stuff
var bool                              fShotgunWeakness;             // if true, we'll get more damage from shotgun if the perk says so
var float                             fHSKThreshold;                // multiplier for how much damage it takes to instakill a zed with an headshot
var float                             fDamageMultiplier;            // damage multiplier applied to all damage except shotguns, explosives, freeze and fire
var float                             fFreezingMultiplier;          // damage multiplier applied to freezing damage
var float                             fKaboomMultiplier;            // damage multiplier applied to explosive damage
var float                             fSFireMultiplier;             // damage multiplier applied to strong fire damage
var float                             fWFireMultiplier;             // damage multiplier applied to weak fire damage
var float                             fHeadScale;                   // default head scale to use when we have a head 
var float                             fMeleeHeadScale;              // default melee head scale to use when we have a head
var float                             fNoHeadScale;                 // head scale after decapitation, used to fix high firerate weapons and invisible heads 
var float                             fShotgunClearListTime;        // how much time has to pass before clearing the shotgun hit list
var float                             fFlareRevolverHitCD;          // cooldown for flare revolver flame damage from the same dude
var float                             fMaxBurnDown;                 // amount to set burndown when we catch fire
var class<DamageType>                 fLastBurnClass;               // last burn class, used to whatever
var int                               LastAcidDamage;               // store last acid damage for the tick
var Pawn                              fSpitFiredBy;                 // spitfire damage instigator
var bool                              fSpitFire;                    // this was a spitfire damage

// misc stuff
var float                             fDeathDrama;                  // how dramatic the death of this zed is (zed time chance on kill)
var float                             fBurnDamage;                  // multiplier for the dot damage
var float                             fAcidDamage;                  // multiplier for the acid dot damage
var float                             fZapResetTime;                // time in seconds to wait to reset the total zap amount to zero
var bool                              fZappedBySkill;               // was zapped by an instant nade
var bool                              fAlreadyStunned;              // was this zed already stunned?
var bool                              fFleshpoundRageImmune;        // are we immune to fleshpound rage damage?
var float                             fLastClawDamageTime;          // last time we've damaged something - used for heavy zeds
var float                             fSlayMeTime;                  // how many seconds since we last damaged someone to consider us slayable - used for heavy zeds
var float                             fDoorDamage;                  // how much damage we currently deal to doors
var int                               fDoorDamageStep;              // how many times we have to damage a door to increase our damage against it
var float                             fAddDoorDamage;               // additional door damage applied at every increase
var float                             fAddDoorDamageMulti;          // additional door damage multiplier
var KFDoorMover                       fLastDoorTarget;              // last door targeted by this zed
var float                             fDoorDamageResetDelay;        // after how many seconds of not attacking a door we reset the door damage
var float                             fDoorDamageResetTime;         // used to set and enforce the door damage reset timer


// freeze fix stuff
var float                             fFrozenAnimFixTime;           // time in seconds to unstuck the animation after freeze
var float                             fCurFrozenAnimFixTime;        // time in seconds to unstuck the animation after freeze
var float                             fAnimEndTime;                 // when this animation will end
var float                             fAnimEndAddDelay;             // additional delay to anim end time, as a second failsafe to make sure the anim is over
var float                             fFreezeCheck;                 // time in seconds between checks for freezing
var float                             fFreezeDelay;                 // freeze delay in seconds, so we wait a bit for the anim to actually end
var class<Emitter>                    ShatteredIce;                 // shattered ice emitter
var float                             FrozenDamageMult;             // frozen damage multipler

// pat stuff (here cause I don't want to override TakeDamage too much)
var int                               FHealLevel;                   // standardized heal level, no more different healing levels
var byte                              SyringeCount;                 // pat syringe count
var byte                              FSyringeCount;                // pat syringe count for resistence

// slowrage fix stuff
var float                             fCurGroundSpeed;              // current ground speed, only changed by SetGroundSpeed method, so I can get uncontrolled speed changes and fix them
var float                             fGroundSpeed;                 // store ground speed before zap
var float                             fWaterSpeed;                  // store water speed before zap
var float                             fAirSpeed;                    // store air speed before zap 
var float                             fHeadlessSpeed;               // how fast we move while headless, default.GroundSpeed * this
var float                             fBurningSpeed;                // how fast we move while burning
var float                             fRageSpeed;                   // how fast we move while raging
var float                             fRageSpeedCap;                // max speed limit of this zed
var bool                              fShouldRage;                  // should this zed rage? used to check if we should store speed on speed change
var bool                              fShouldExitRage;              // should this zed exit rage after being stunned?

// wrong targeting fix stuff
var float                             fWTCheck;                     // seconds between checks if we're attacking the wrong target
var float                             fWTCurTime;                   // tick variable to compare to the check

// spawn targeting stuff
var Pawn                              fSpawnTarget;                 // selected target on spawn

// shock time, to prevent zeds from attacking just after decapitated
var float                             fShockTime;                   // how long we should be shocked after our head popped
var float                             fCurShockTime;                // current shock time

// movement speed multipliers
var float                             fMovementScaleBB;             // movement speed multiplier at bloodbath
var float                             fMovementScaleHOE;            // movement speed multiplier at hell on earth
var float                             fMovementScaleSucidal;        // movement speed multiplier at suicidal
var float                             fMovementScaleHard;           // movement speed multiplier at hard
var float                             fMovementScaleNormal;         // movement speed multiplier at normal

// rage speed scaling
var float                             fNormalRageSpeed;             // rage speed scaling at normal
var float                             fHardRageSpeed;               // rage speed scaling at hard
var float                             fSuicidalRageSpeed;           // rage speed scaling at suicidal
var float                             fHOERageSpeed;                // rage speed scaling at hell on earth
var float                             fBBRageSpeed;                 // rage speed scaling at bloodbath

// flinch fix
var float                             fFlinchTime;                  // time in seconds this zed is flinching
var float                             fFlinchCooldown;              // time between flinch animations
var float                             fNextFlinch;                  // used to stop flinching, set to current time + fFlinchCooldown each flinch

// burnt limbs
var class<SeveredAppendage>           BurntDetachedArmClass;        // class used for detached arms while crispified
var class<SeveredAppendage>           BurntDetachedLegClass;        // class used for detached legs while crispified
var class<SeveredAppendage>           BurntDetachedSpecialArmClass; // class used for special detached arms while crispified
var class<SeveredAppendage>           BurntDetachedHeadClass;       // class used for the detached head while crispified
var bool                              bPlayBileSplash;              // used to prevent spwning limbs while burnt (and to go kaboom, for the bloat)

// stairs bug fix
var Vector                            fStoredStairLoc;              // stored location for our stairs fix
var float                             fLastCanAttackTime;           // last time CanAttack returned true
var float                             fStairsTimeFix;               // time in seconds to trigger the stairs fix
var float                             fStairsCheckTime;             // time between checks for location to see if we haven't moved
var bool                              fStairsSecondCheck;           // used to enforce the two seconds on zeds stop

// stun rotation block
var pawn                              fStoredStunEnemy;             // stored controller enemy for stun
var actor                             fStoredStunFocus;             // stored controller focus for stun
var vector                            fFocalPoint;                  // stored focal point for stun

// head remove bug fix
var int                               fHeadRemoveRetry;             // how many times we should retry to hide the head
var float                             fHeadRemoveDelay;             // seconds to wait between head remove checks


// multi hit fix (basically zeds are hit twice or more by bullets and stuff, we don't want it)
struct fMultiFix
{
    var Pawn              fPawn;                                     // dude that hurt me
    var int               HitID;                                     // projectile ID (optional)
    var array<int>        ShotID;                                    // shotgun hit id
    var class<DamageType> damageType;                                // what kind of damage was
    var float             ShotHSTime;                                // when to compute this shotgun headshot damage 
    var float             ShotHitTime;                               // when this shotgun hit was fired
    var float             FlareHitTime;                              // when this flare revolver hit was fired
    var int               TotalHSDam;                                // total head damage with this shotgun
    var Vector            momentum;                                  // shotgun hs damage momentum
    var Vector            hitlocation;                               // shotgun hs damage hitlocation 
};

var transient array<fMultiFix> fHitList;                            // we keep our players here with whatever they fired at us


replication
{
    reliable if(Role == ROLE_Authority)
        DecapitatedBy, DecapitatedByMelee, DecapitatedByID, LastBulletHitID, fPlasmaSpawned, fShouldPlasma, fShouldSetHitLoc, fHitLoc, SyringeCount, FSyringeCount, fHeadScale, fCurGroundSpeed, fLastBurnClass, LastAcidDamage,
        fAcidDamage, fSpitFiredBy, fSpitFire, fWTCurTime, fZedTimeBlock, fZappedBySkill, fCurShockTime, fAlreadyStunned, fClientFlipOver, fClientFreezeZed, fClientFreezeFix, fClientUnfreezeFix, fRageSpeed;
}

// edited ground speed multipliers
simulated function PostBeginPlay()
{
    local float MovementSpeedDifficultyScale;
    local vector AttachPos;

    if (ROLE == ROLE_Authority)
    {
        if ((ControllerClass != None) && (Controller == None))
            Controller = spawn(ControllerClass);

        if (Controller != None)
            Controller.Possess(self);

        SplashTime = 0;
        SpawnTime = Level.TimeSeconds;
        EyeHeight = BaseEyeHeight;
        OldRotYaw = Rotation.Yaw;

        if (HealthModifer != 0)
            Health = HealthModifer;

        if (bUseExtendedCollision && MyExtCollision == none)
        {
            MyExtCollision = Spawn(class 'ExtendedZCollision',self);
            MyExtCollision.SetCollisionSize(ColRadius,ColHeight);

            MyExtCollision.bHardAttach = true;
            AttachPos = Location + (ColOffset >> Rotation);
            MyExtCollision.SetLocation(AttachPos);
            MyExtCollision.SetPhysics(PHYS_None);
            MyExtCollision.SetBase(self);
            SavedExtCollision = MyExtCollision.bCollideActors;
        }
    }

    AssignInitialPose();

    // Let's randomly alter the position of our zombies' spines, to give their animations
    // the appearance of being somewhat unique.
    SetTimer(1.0, false);

    //Set Karma Ragdoll skeleton for this character.
    if (KFRagdollName != "")
        RagdollOverride = KFRagdollName; //ClotKarma

    //Log("Ragdoll Skeleton name is :"$RagdollOverride);

    if (bActorShadows && bPlayerShadows && (Level.NetMode != NM_DedicatedServer))
    {
        // decide which type of shadow to spawn
        if (!bRealtimeShadows)
        {
            PlayerShadow = Spawn(class'ShadowProjector',Self,'',Location);
            PlayerShadow.ShadowActor = self;
            PlayerShadow.bBlobShadow = bBlobShadow;
            PlayerShadow.LightDirection = Normal(vect(1,1,3));
            PlayerShadow.LightDistance = 320;
            PlayerShadow.MaxTraceDistance = 350;
            PlayerShadow.InitShadow();
        }

        else
        {
            RealtimeShadow = Spawn(class'Effect_ShadowController',self,'',Location);
            RealtimeShadow.Instigator = self;
            RealtimeShadow.Initialize();
        }
    }

    bSTUNNED = false;
    DECAP = false;

    // Difficulty Scaling
    if (Level.Game != none && !bDiffAdjusted)
    {
        // rage speed scaling based on difficulty
        if (Level.Game.GameDifficulty < 3.0) 
        {
            fRageSpeed  = fNormalRageSpeed;
            fUsedThresh = fStunThresh;
        }

        else if  (Level.Game.GameDifficulty < 5.0)
        {
            fRageSpeed  = fHardRageSpeed;
            fUsedThresh = int(float(fStunThresh) * fUsedHardMult);
        }

        else if  (Level.Game.GameDifficulty < 6.0)
        {
            fRageSpeed  = fSuicidalRageSpeed;
            fUsedThresh = int(float(fStunThresh) * fUsedSuiMult);
        }

        else if  (Level.Game.GameDifficulty < 8.0)
        {
            fRageSpeed  = fHOERageSpeed;
            fUsedThresh = int(float(fStunThresh) * fUsedHoeMult);
        }

        else
        {
            fRageSpeed  = fBBRageSpeed;
            fUsedThresh = int(float(fStunThresh) * fUsedBBMult);
        }

        //warn("fUsedThresh:"@fUsedThresh);
        //warn(self$" Beginning ground speed "$default.GroundSpeed);

        // hidden ground speed, probably broken but welp...
        if (Level.Game.NumPlayers <= 3)
            HiddenGroundSpeed = default.HiddenGroundSpeed;

        else if(Level.Game.NumPlayers <= 5)
            HiddenGroundSpeed = default.HiddenGroundSpeed * 1.3;

        else if(Level.Game.NumPlayers >= 6)
            HiddenGroundSpeed = default.HiddenGroundSpeed * 1.65;

        //log(self$" Randomized ground speed "$GroundSpeed$" RandomGroundSpeedScale "$RandomGroundSpeedScale);


        // general movement speed multiplier

        if (Level.Game.GameDifficulty >= 8.0) // Bloodbath
            MovementSpeedDifficultyScale = fMovementScaleBB;

        else if (Level.Game.GameDifficulty >= 7.0) // Hell on Earth
            MovementSpeedDifficultyScale = fMovementScaleHOE;

        else if (Level.Game.GameDifficulty >= 5.0) // Suicidal
            MovementSpeedDifficultyScale = fMovementScaleSucidal;

        else if (Level.Game.GameDifficulty >= 4.0) // Hard
            MovementSpeedDifficultyScale = fMovementScaleHard;

        else
            MovementSpeedDifficultyScale = fMovementScaleNormal;


        GroundSpeed *= MovementSpeedDifficultyScale;
        AirSpeed    *= MovementSpeedDifficultyScale;
        WaterSpeed  *= MovementSpeedDifficultyScale;

        // Store the difficulty adjusted ground speed to restore if we change it elsewhere
        OriginalGroundSpeed = GroundSpeed;

        //log(self$" Scaled ground speed "$GroundSpeed$" Difficulty "$Level.Game.GameDifficulty$" MovementSpeedDifficultyScale "$MovementSpeedDifficultyScale);

        // Scale health by difficulty
        Health       *= DifficultyHealthModifer();
        HealthMax    *= DifficultyHealthModifer();
        HeadHealth   *= DifficultyHeadHealthModifer();

        // Scale health by number of players
        Health       *= NumPlayersHealthModifer();
        HealthMax    *= NumPlayersHealthModifer();
        HeadHealth   *= NumPlayersHeadHealthModifer();

        MeleeDamage   = Max((DifficultyDamageModifer() * MeleeDamage), 1);
        SpinDamConst  = Max((DifficultyDamageModifer() * SpinDamConst), 1);
        SpinDamRand   = Max((DifficultyDamageModifer() * SpinDamRand), 1);
        fDoorDamage   = MeleeDamage;
        fAddDoorDamage = fDoorDamage * fAddDoorDamageMulti;

        ScreamDamage  = Max((DifficultyDamageModifer() * ScreamDamage), 1);

        bDiffAdjusted = true;
    }

    if(Level.NetMode!=NM_DedicatedServer)
    {
        AdditionalWalkAnims[AdditionalWalkAnims.length] = default.MovementAnims[0];
        MovementAnims[0] = AdditionalWalkAnims[Rand(AdditionalWalkAnims.length)];
    }
}

// manage fHitList indexes
function int AttackerIndex(Pawn fPawn)
{
    local int i;

    for (i=0; i<fHitList.length; i++)
    {
        if (fHitList[i].fPawn == fPawn)
        {
            //warn("Pawn Found");
            return i;
        }
    }

    // 404 dude not found, add it
    //warn("Adding New Pawn: "@fPawn);

    fHitList.insert(i, 1);
    fHitList[i].fPawn = fPawn;
    return i;
}


// store shotgun headshot damage to be applied later
function AddShotgunHeadDamage(Pawn fPawn, int Damage, class<DamageType> damageType, Vector hitlocation, Vector momentum)
{
    local int i;

    i = AttackerIndex(fPawn);

    //warn("Adding Head Damage: "@Damage@" Total: "@fHitList[i].TotalHSDam);

    fHitList[i].TotalHSDam += Damage;
    fHitList[i].ShotHSTime  = Level.TimeSeconds;
    fHitList[i].damageType  = damageType;
    fHitList[i].momentum    = momentum;
    fHitList[i].hitlocation = hitlocation;
}


// was I already hit by this attack?
function bool AlreadyHitByThisAttack(Pawn fPawn, class<DamageType> damageType, int HitID)
{
    local int i;
    local int x;
    local class<KFWeaponDamageType> KDT;

    i   = AttackerIndex(fPawn);
    KDT = class<KFWeaponDamageType>(damageType);

    // shotgun fix
    if (KDT != none && KDT.default.bIsPowerWeapon && class<DamTypeNailBombFalk>(damageType) == none)
    {
        // periodic list clear
        if (fHitList[i].ShotID.length > 0 && fHitList[i].ShotHitTime + fShotgunClearListTime <= Level.TimeSeconds)
        {
            //warn("Clearing list");

            for (x=fHitList[i].ShotID.length - 1; x>=0; x--)
                fHitList[i].ShotID.Remove(x, 1);

            x = 0;
        }

        // actual shotgun fix
        else if (fHitList[i].damageType == damageType)
        {
            for (x=0; x<fHitList[i].ShotID.length; x++)
            {
                if (fHitList[i].ShotID[x] == HitID)
                {
                    //warn("BID: "@HitID@" Found, blocked");
                    return True;
                }
            }
        }

        fHitList[i].ShotID.insert(x, 1);
        fHitList[i].ShotID[x]   = HitID;
        fHitList[i].ShotHitTime = Level.TimeSeconds;
    }

    // flare revolver flame fix
    else if (class<DamTypeFlareRevolver>(damageType) != none)
    {
        if (fHitList[i].FlareHitTime + fFlareRevolverHitCD >= Level.TimeSeconds)
        {
            //warn("Skipped Flare Revolver Flame Damage:"@Level.TimeSeconds);
            return True;
        }

        fHitList[i].FlareHitTime = Level.TimeSeconds;
    }

    // other weapons fix
    else if (fHitList[i].damageType == damageType && fHitList[i].HitID == HitID)
    {
        //warn("Blocked "@HitID);
        return True;
    }


    if (class<DamTypeFlareRevolver>(damageType) == none)
    {
        //warn("Hit "@HitID);
        fHitList[i].HitID      = HitID;
        fHitList[i].damageType = damageType;
    }

    return False;
}


// alternative takedamage implementation to log proper damage even on head remove
function TakeDamage(int Damage, Pawn instigatedBy, Vector hitlocation, Vector momentum, class<DamageType> damageType, optional int HitIndex)
{
    // vanilla vars
    local int        actualDamage; 
    local Controller Killer;
    local float      HeadShotCheckScale;
    local bool       bIsHeadShot;

    // falk vars
    local Class<KFWeaponDamageType> KFWDT;
    local KFPlayerReplicationInfo   KFPRI;
    local class<FVeterancyTypes>    Vet;
    local FalkMonsterBase           fM;

    // don't take damage from nade impacts
    if (Health <= 0 || (damageType != none && class<DamTypeFragImpactFalk>(damageType) != none))
        return;

    //warn("TakeDamage:"@damageType@"HitIdx:"@HitIndex);

    if (damageType == None)
    {
        if (instigatedBy != None)
            warn("No damagetype for damage by "$instigatedby$" with weapon "$InstigatedBy.Weapon);

        damageType = class'KFWeaponDamageType';
    }

    if (instigatedBy != none)
    {
        // take no damage from stunned or frozen zeds
        if (FalkMonster(InstigatedBy) != None && (FalkMonster(InstigatedBy).bStunned || FalkMonster(InstigatedBy).fFrozen))
            return;

        LastDamagedBy = instigatedBy;

        if (instigatedBy.Controller != none)
        {
            Killer = instigatedBy.Controller;

            if (Controller != none && FMonsterController(Controller) != none)
                FMonsterController(Controller).fLastDamagedByController = instigatedBy.Controller;
        }
    }

    else if ((instigatedBy == None || instigatedBy.Controller == None) && DelayedDamageInstigatorController != None)
    {
        Killer = DelayedDamageInstigatorController;

        if (DamageType.default.bDelayedDamage)
            instigatedBy   = DelayedDamageInstigatorController.Pawn;

        if (Controller != none && FMonsterController(Controller) != none && DelayedDamageInstigatorController != none)
            FMonsterController(Controller).fLastDamagedByController = DelayedDamageInstigatorController;
    }

    LastDamagedByType = damageType;
    HitMomentum       = VSize(momentum);
    LastHitLocation   = hitlocation;
    LastMomentum      = momentum;
    LastBulletHitID   = HitIndex;
    KFWDT             = Class<KFWeaponDamageType>(damageType);

    // falk stuff //
    if (fFrozen && damageType != class'DamTypeFreezeBomb' && damageType != class'DamTypeFreezerGun')
        Damage *= FrozenDamageMult;

    if (KFWDT != none)
    {
        // fleshpound rage immunity
        if (fFleshpoundRageImmune && class<DamTypePoundCrushedFalk>(damageType) != none)
        {
            Damage = 0;
            return;
        }

        fM = FalkMonsterBase(instigatedBy);

        // block damage from dead, stunned and frozen zeds
        if (fM != none && (fM.fFrozen || fM.fIsStunned || fM.bZapped || fM.fFlinching))
        {
            Damage = 0;
            return;
        }

        // boost pat chaingun damage on other zeds
        else if (class<DamTypeBossChainGunFalk>(damageType) != none)
            Damage *= 8;

        // boost pat rocket damage on other zeds and reduce it on the pat itself
        else if (class<DamTypeBossLAWProj>(damageType) != none)
        {
            // this doesn't work by now, pat rockets don't affect him for some unknown reason
            // still leaving the code untouched cause it's kawaii and doesn't mean any harm - past Falk
            if (FIsBoss)
                Damage *= 0.2;

            // this works btw - future past Falk
            else
                Damage *= 4;
        }

        else if (class<DamTypeBossRadialFalk>(damageType) != none)
        {
            Damage = FalkGetRadialDamage();
        }

        // take some more damage from the bloat
        else if (class<DamTypeBloatVomitFalk>(damageType) != none)
            Damage *= 4;

        if (HitIndex != 689) // let's skip this code when we call TakeDamage from Tick
        {
            // medic nade damage fix
            //if (class<DamTypeMedicNadeFalk>(damageType)  != none)
            //Damage = Class<DamTypeMedicNadeFalk>(damageType).default.Damage * fDamageMultiplier; // multiplied by normal damage mult

            // weak fire damage multiplier, check only if needed
            if (fWFireMultiplier != 1.0                                   &&
                    (class<DamTypeBurnWeakFalk>(damageType)            != none ||
                     class<DamTypeFlamethrower>(damageType)            != none ||
                     class<DamTypeFlamethrowerFalk>(damageType)        != none ||
                     class<DamTypeHuskGun>(damageType)                 != none ||
                     class<DamTypeHuskGunProjectileImpact>(damageType) != none ||
                     class<DamTypeFlameNadeFalk>(damageType)           != none ||
                     class<DamTypeInstantFlameNadeFalk>(damageType)    != none ||
                     class<DamTypeAAR525SFalk>(damageType)             != none))
            {
                Damage *= fWFireMultiplier;
            }

            // strong fire damage multiplier, check only if needed
            else if (fSFireMultiplier != 1.0                                     &&
                    (class<DamTypeBurnStrongFalk>(damageType)              != none ||
                     class<DamTypeTrenchgunFalk>(damageType)               != none ||
                     class<DamTypeFlareRevolver>(damageType)               != none ||
                     class<DamTypeFlareProjectileImpactFalk>(damageType)   != none ||
                     class<DamTypeM4A1IronBeastSAFalk>(damageType)         != none ||
                     class<DamTypeSpitfireFalk>(damageType)                != none ||
                     class<DamTypeMAC10MPIncFalk>(damageType)              != none))
            {
                Damage *= fSFireMultiplier;
            }

            // explosive damage multiplier, this should always be checked anyway
            else if (KFWDT.default.bIsExplosive && Killer != none && KFPlayerController(Killer) != none)
                Damage *= fKaboomMultiplier;

            // damage multiplier for freezing weapons and freeze stop
            else if (damageType == class'DamTypeFreezeBomb' || damageType == class'DamTypeFreezerGun' || damagetype == class'DamTypeFreezeStop')
                Damage *= fFreezingMultiplier;

            // shotguns point blank fire fix and damage multiplier
            else if (KFWDT.default.bIsPowerWeapon && Killer != none && KFPlayerController(Killer) != none)
            {
                if (fShotgunWeakness && damageType != class'DamTypeNailBombFalk')
                {
                    KFPRI = KFPlayerReplicationInfo(KFHumanPawn(instigatedBy).PlayerReplicationInfo);

                    if (KFPRI != none)
                    {
                        Vet = Class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

                        if (Vet != none)
                            Damage = Vet.Static.AddShotgunDamage(damageType, Damage);
                    }
                }

                Damage *= fDamageMultiplier; 
            }

            // other damage multiplier
            else
                Damage *= fDamageMultiplier;

            // don't take two hits from a single shot
            if (instigatedBy != none && (KFHumanPawn(instigatedBy) != none || class<DamTypeBloatVomitFalk>(damageType) != none || class<DamTypeBossLAWProj>(damageType) != none ||
                        class<DamTypeHuskFire>(damageType) != none) && AlreadyHitByThisAttack(instigatedBy, damageType, HitIndex))
            {
                return;
            }

            //warn("Damage from:"@instigatedBy@"- Type:"@damageType@"- BulletID:"@HitIndex@"- Damage:"@Damage@"- Time:"@Level.TimeSeconds);

            /*if (class<DamTypeBloatVomitFalk>(damageType) != none)
              {
              warn("Vomit Damage:"@Damage@"Time:"@Level.TimeSeconds);
              }*/

            // used to spawn a plasma nade on sharp zedtime headshot
            if (fShouldSetHitLoc)
                fHitLoc = hitlocation;

            // more (edited) vanilla stuff //

            if (Killer != none && KFPlayerController(Killer) != none && Killer.PlayerReplicationInfo != none)
            {
                KFPRI = KFPlayerReplicationInfo(Killer.PlayerReplicationInfo);

                // ugly fix for freeze bombs doing weird shit
                if (damageType == class'DamTypeFrag' && KFPRI.ClientVeteranSkill != none &&
                        KFPRI.ClientVeteranSkill.static.GetNadeType(KFPRI) == class'FreezeBombFalk')
                {
                    Damage = 0;
                }
            }

            // Scale damage if the Zed has been zapped
            if(bZapped)
                Damage *= ZappedDamageMod;

            // this thing should always deal a fixed amount of damage so we don't fuck up its math
            if (damageType == class'DamTypeFreezeBomb')
            {
                //log("FreezeBomb");
                Damage = class'DamTypeFreezeBomb'.Default.Damage;
            }

            // some zeds should resist this kind of damage 
            else if (damageType == class'DamTypeFreezeStop')
            {
                //log("FreezeStop");

                if (Damage == 2)
                    Damage = 0;

                else
                    Damage = 2;
            }

            // Burn behavior
            if (KFWDT.default.bDealBurningDamage)
            {
                if (KFPRI != none)
                    Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, self, KFPawn(instigatedBy), Damage, DamageType);

                //warn("LastBurnDamage:"@LastBurnDamage@"- New:"@Damage + (Damage * fBurnDamage)@"- instigator:"@instigatedBy@"- DamType:"@damageType);

                // the zed wasn't burning, or it was burning from another zed and we're human, or we're applying a stronger damage
                if (BurnDown <= 0                                                         ||
                        (KFMonster(BurnInstigator) != none && KFMonster(instigatedBy) == none) ||
                        (KFMonster(instigatedBy)                                      == none  &&
                         damageType != class'DamTypeBurnStrongFalk'                            &&
                         damageType != class'DamTypeBurnWeakFalk'                              &&
                         Damage + (Damage * fBurnDamage) > LastBurnDamage))                       
                {
                    //warn("Overwrite");

                    // reinit burn tick count if we're already burning
                    if (BurnDown > 0)
                    {
                        bBurnified                  = true;
                        BurnDown                    = Default.fMaxBurnDown; // Inits burn tick count

                        if (instigatedBy != none)
                            BurnInstigator           = instigatedBy;

                        SetTimer(1.0,false); // Sets timer function to be executed each second
                    }

                    // LastBurnDamage variable is storing last burn damage (perked) received,
                    LastBurnDamage = Damage + (Damage * fBurnDamage);
                    fLastBurnClass = damageType;

                    // FireDamageClass is used to store if the damage was a strong or weak flame
                    if (class<DamTypeTrenchgunFalk>(damageType)                 != none ||
                            class<DamTypeFlareRevolver>(damageType)                 != none ||
                            class<DamTypeFlareProjectileImpactFalk>(damageType)     != none ||
                            class<DamTypeM4A1IronBeastSAFalk>(damageType)           != none ||
                            class<DamTypeMAC10MPIncFalk>(damageType)                != none)
                    {
                        FireDamageClass = class'DamTypeBurnStrongFalk';
                        fSpitFire       = false;
                    }

                    else if (instigatedBy != none && damageType == class'DamTypeSpitfireFalk')
                    {
                        FireDamageClass = class'DamTypeBurnWeakFalk';
                        fSpitFire       = true;
                        fSpitFiredBy    = instigatedBy;
                    }

                    else
                    {
                        FireDamageClass = class'DamTypeBurnWeakFalk';
                        fSpitFire       = false;
                    }

                    if (instigatedBy != none)
                        BurnInstigator              = instigatedBy;
                }

                // BurnDown variable indicates how many ticks are remaining for zed to burn.
                // It is 0 when zed isn't burning (or stopped burning).
                // So all the code below will be executed only, if zed isn't already burning
                if (BurnDown <= 0)
                {
                    bBurnified                  = true;
                    BurnDown                    = Default.fMaxBurnDown; // Inits burn tick count

                    if (instigatedBy != none)
                        BurnInstigator              = instigatedBy;

                    SetTimer(1.0,false); // Sets timer function to be executed each second
                }
            }

            // vomit damage behavior
            if (class<DamTypeVomit>(DamageType) != none && class<DamTypeBloatVomitFalk>(damageType) == none &&
                    (BileCount <= 0 || Damage * fAcidDamage > LastAcidDamage))
            {
                BileCount = 4;

                if (instigatedBy != none)
                    BileInstigator = instigatedBy;

                LastBileDamagedByType = class<DamTypeVomit>(DamageType);
                LastAcidDamage  = Damage * fAcidDamage;

                if (class<DamTypeMedicNadeFalk>(DamageType) != none)
                {
                    LastAcidDamage = Damage * class<DamTypeMedicNadeFalk>(DamageType).Default.fDotMultipler;

                    //warn("Medic DOT: "@class<DamTypeMedicNadeFalk>(DamageType).Default.fDotMultipler@" - "@LastAcidDamage);
                    /*KFPRI = KFPlayerReplicationInfo(instigatedBy.PlayerReplicationInfo);

                      if (KFPRI != none)
                      {
                      Vet = Class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

                      if (Vet != none)
                      LastAcidDamage = Vet.Static.MedicDotDamage(KFPRI, Damage);
                      } */
                }

                SetTimer(1.0, false); // Sets timer function to be executed each second
            }

            if (/*!fFrozen &&*/ KFWDT.default.bCheckForHeadShots)
            {
                //warn("Check for Headshot");

                HeadShotCheckScale = fHeadScale;

                // invisible head workaround
                if (bDecapitated || DecapitatedBy != none)
                {
                    // Do larger headshot checks if it is a melee attack
                    if (KFWDT.Default.bIsMeleeDamage || class<DamTypeMelee>(damageType) != none)
                        HeadShotCheckScale = Max(fMeleeHeadScale, fNoHeadScale);

                    else
                        HeadShotCheckScale = fNoHeadScale;
                }

                // Do larger headshot checks if it is a melee attack
                else if (KFWDT.Default.bIsMeleeDamage || class<DamTypeMelee>(damageType) != none)
                    HeadShotCheckScale = fMeleeHeadScale;

                bIsHeadShot = IsHeadShot(hitlocation, normal(momentum), HeadShotCheckScale);

                //warn("IsHeadShot:"@bIsHeadShot);

                if (bIsHeadShot)
                {
                    //warn("HeadShot");

                    if (KFWDT.default.bIsPowerWeapon) // attempt to fix headshot damage with with shotguns
                    {
                        //warn("Shotgun");
                        AddShotgunHeadDamage(instigatedBy, Damage, damageType, hitlocation, momentum);
                        return;
                    }

                    if (bDecapitated || DecapitatedBy != none) // invisible head fix, hopefully
                    {
                        //warn("Headless");
                        return;
                    }
                }
            }
        }

        // this was an headshot from a shotgun applied from tick
        else if (instigatedBy != none)
        {
            if (/*!fFrozen && */KFWDT.default.bCheckForHeadShots)
                bIsHeadShot = True;

            KFPRI       = KFPlayerReplicationInfo(instigatedBy.PlayerReplicationInfo);
        }

        // perk damage multiplier, don't do it on explosives, nades and fire dot since they take care of it
        if (!KFWDT.default.bIsExplosive && !KFWDT.default.bDealBurningDamage && class<DamTypeMedicNadeFalk>(DamageType) == none && class<DamTypeFlameNadeFalk>(DamageType) == none &&
                class<DamTypeNailBombFalk>(DamageType) == none && class<DamTypeAcidNadeFalk>(DamageType) == none && class<DamTypeAcidClusterFalk>(DamageType) == none &&
                class<DamTypeBurnStrongFalk>(damageType) == none && class<DamTypeBurnWeakFalk>(damageType) == none && KFPRI != none)
        {
            if (KFPRI.ClientVeteranSkill != none)
                Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, self, KFPawn(instigatedBy), Damage, DamageType);
        }

        // add kills to player controllers, not sure if we need it but shouldn't harm anything
        if (damageType != none && LastDamagedBy != none && LastDamagedBy.IsPlayerPawn() && LastDamagedBy.Controller != none)
        {
            if (KFMonsterController(Controller) != none)
                KFMonsterController(Controller).AddKillAssistant(Killer, FMin(Health, Damage));
        }

        // headshot multiplier
        if (!bDecapitated && bIsHeadShot && KFWDT.default.bCheckForHeadShots)
        {
            Damage *= KFWDT.default.HeadShotDamageMult;

            if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
                Damage *= KFPRI.ClientVeteranSkill.Static.GetHeadShotDamMulti(KFPRI, KFPawn(instigatedBy), DamageType);

            LastDamageAmount = Damage;

            // Play a sound when someone gets a headshot 
            PlaySound(sound'KF_EnemyGlobalSndTwo.Impact_Skull', SLOT_None, 2.0, true, 500);

            HeadHealth -= Damage;

            // shatter on headhealth <= 0
            if (fFrozen && HeadHealth <= 0)
            {
                Health = 0;

                Spawn(ShatteredIce,,,Location);
                bHidden = true;

                super.Died(Killer, damageType, HitLocation);
            }

            //log("HeadHealth: "@HeadHealth);

            if (!FIsBoss && (HeadHealth <= 0 || Damage >= Health))
                RemoveHead();

            // Award headshot here, not when zombie died.
            if(bDecapitated && KFWDT != none && Killer != none && KFPlayerController(Killer) != none)
                KFWDT.Static.ScoredHeadshot(KFSteamStatsAndAchievements(PlayerController(Killer).SteamStatsAndAchievements), self.Class, bLaserSightedEBRM14Headshotted);
        }

        // explosives always apply momentum
        if(Health - Damage > 0 /*&& !*/ || KFWDT.default.bIsExplosive)
            momentum = vect(0,0,0);

        // Skaarj stuff
        if (Physics == PHYS_Walking && damageType.default.bExtraMomentumZ)
            momentum.Z = FMax(momentum.Z, 0.4 * VSize(momentum));

        if (instigatedBy != none && instigatedBy == self)
            momentum *= 0.6;

        momentum = momentum / Mass;

        if (Weapon != None)
            Weapon.AdjustPlayerDamage(Damage, InstigatedBy, HitLocation, momentum, DamageType);

        //warn("Damage:"@Damage@"Time:"@Level.TimeSeconds);

        // boss resist damage, checked after every damage multiplier
        Damage = fBossResist(Damage, Health, damageType);

        //warn("Resist:"@Damage@"Time:"@Level.TimeSeconds);

        if (Damage == 0)
            return;

        actualDamage = Level.Game.ReduceDamage(Damage, self, instigatedBy, HitLocation, Momentum, DamageType);

        /*log("Damage: "@actualDamage);
          log("DamageType: "@DamageType);*/
        //log("HealthBefore: "@Health);

        /*if (class<DamTypePipeBomb>(damageType) != none)
          warn("Pipebomb:"@actualDamage);*/

        Health -= actualDamage;

        //log("HealthAfter: "@Health);

        if (HitLocation == vect(0,0,0))
            HitLocation = Location;

        if (FIsBoss && bIsHeadShot && Health <= 0)
            RemoveHead();

        // this was an headshot with a throwing knife, tell playhit to stun the zed
        if (class<DamTypeThrowingKnifeFalk>(damageType) != none && bIsHeadShot)
            PlayHit(actualDamage, instigatedBy, hitLocation, damageType, Momentum, -689);

        else
            PlayHit(actualDamage, instigatedBy, hitLocation, damageType, Momentum, HitIndex);

        if (Health <= 0 || (!FIsBoss && bDecapitated && Damage >= HealthMax * fHSKThreshold))
        {
            if (DamageType.default.bCausedByWorld && (instigatedBy == None || instigatedBy == self) && LastHitBy != None)
                Killer = LastHitBy;

            else if (instigatedBy != None)
                Killer = instigatedBy.GetKillerController();

            if (Killer == None && DamageType.Default.bDelayedDamage)
                Killer = DelayedDamageInstigatorController;

            if (bPhysicsAnimUpdate)
                SetTearOffMomemtum(momentum);

            if (KFPRI != none                    &&
                    KFPRI.ClientVeteranSkill != none &&
                    KFPRI.ClientVeteranSkill.static.KilledShouldExplode(KFPRI, KFPawn(instigatedBy)))
            {
                HurtRadius(500, 1000, class'DamTypeFrag', 100000, Location); // not sure what's this yet
            }

            Died(Killer, damageType, HitLocation);

            if (!fZedTimeBlock)
                KFGameType(Level.Game).DramaticEvent(fDeathDrama);
        }

        else
        {
            AddVelocity(momentum);

            if (Controller != None)
                Controller.NotifyTakeHit(instigatedBy, HitLocation, actualDamage, DamageType, Momentum);

            if (instigatedBy != None && instigatedBy != self)
                LastHitBy = instigatedBy.Controller;
        }

        MakeNoise(1.0);
    }

    bBackstabbed = false;
}

function RemoveHead()
{
    local int i;
    local KFPlayerReplicationInfo KFPRI;
    local class<FVeterancyTypes> Vet;
    local Projectile P;
    local class<KFWeaponDamageType> KFWDT;
    local float fTempGroundSpeed;

    // vanilla stuff
    bDecapitated     = True;
    fHeadScale       = 2.0;
    Intelligence     = BRAINS_Retarded;
    DECAP            = true;
    DecapTime        = Level.TimeSeconds;
    Velocity         = vect(0,0,0);
    AmbientSound     = MiscSound;

    fShouldSetHitLoc = False; // stop checking for new fHitLoc, no more head anyway

    if (LastDamagedBy != none)
    {
        DecapitatedBy = LastDamagedBy;

        if (Class<KFWeaponDamageType>(LastDamagedByType) != None && Class<KFWeaponDamageType>(LastDamagedByType).Default.bIsMeleeDamage)
            DecapitatedByMelee = True;

        else
        {
            DecapitatedByMelee = False;

            // setting decapitation ID
            KFWDT = class<KFWeaponDamageType>(LastDamagedByType);

            if (KFWDT != None && KFWDT.default.bIsPowerWeapon)
                DecapitatedByID = LastBulletHitID;

            // sharp stuff 
            KFPRI = KFPlayerReplicationInfo(LastDamagedBy.PlayerReplicationInfo);

            if (KFPRI != none)
            {
                Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

                if (Vet != none && Vet.Static.SharpZedTimeBonus(KFPRI, LastDamagedByType))
                {
                    fShouldPlasma = True;

                    P = Spawn(class'InstantPlasmaNadeFalk',,,fHitLoc,rotator(vect(0,0,1)));

                    if (P != none)
                    {
                        fPlasmaSpawned = True;
                        P.RemoteRole   = ROLE_None;
                    }
                }
            }
        }
    }

    // more vanilla stuff
    AirSpeed         *= fHeadlessSpeed;
    WaterSpeed       *= fHeadlessSpeed;
    fTempGroundSpeed  = GetOriginalGroundSpeed() * fHeadlessSpeed;
    SetAnimAction('HitF');
    SetGroundSpeed(fTempGroundSpeed);

    if (Controller != none)
        MonsterController(Controller).Accuracy = -5;  // More chance of missing. 

    if(Health > 0)
        BleedOutTime = Level.TimeSeconds +  BleedOutDuration;

    // He's got no head so biting is out.
    if (MeleeAnims[2] == 'Claw3')
        MeleeAnims[2] = 'Claw2';

    if (MeleeAnims[1] == 'Claw3')
        MeleeAnims[1] = 'Claw1';

    // Plug in headless anims if we have them
    for(i = 0; i < 4; i++)
    {
        if(HeadlessWalkAnims[i] != '' && HasAnim(HeadlessWalkAnims[i]))
        {
            MovementAnims[i] = HeadlessWalkAnims[i];
            WalkAnims[i]     = HeadlessWalkAnims[i];
        }
    }

    PlaySound(DecapitationSound, SLOT_Talk, 1.30, true, 525);
}


// jump to implement the stairs fix
function FStairFixJump()
{
    fLastCanAttackTime = Level.TimeSeconds;
    FMonsterController(Controller).FStairsFixJump(); //GetOutOfTheWayOfShot(vector(Rotation), Location);
}


// fallback plasma nade spawn if the first attempt failed and attempted shotgun hs damage fix
simulated function Tick(float DeltaTime)
{
    local Projectile Pr; 
    local int i;

    // door damage reset
    if (ROLE == Role_Authority && fDoorDamageResetTime > 0 && Level.TimeSeconds > fDoorDamageResetTime)
    {
        fDoorDamageResetTime = 0;
        FalkResetDoorDamage();
    }

    // attempt to reliably remove heads to decapitated zeds
    if (Level.NetMode != NM_DedicatedServer && fHeadRemoveRetry > 0 && (HeadHealth < 0 || bDecapitated || DecapitatedBy != none))
    {
        if (fHeadRemoveDelay > 0)
            fHeadRemoveDelay -= DeltaTime;

        else
        {
            fHeadRemoveRetry--;
            fHeadRemoveDelay = Default.fHeadRemoveDelay;
            //warn("Tick Remove Head:"@Default.fHeadRemoveRetry-fHeadRemoveRetry@"Time:"@Level.TimeSeconds);
            HideBone(HeadBone);
        }
    }

    if (fIsStunned && Controller != none)
    {
        //warn("STUNNED 1:"@Level.TimeSeconds);
        Controller.Enemy      = None;
        Controller.Focus      = None;
        Controller.FocalPoint = fFocalPoint;
    }

    // stairs fix
    if (ROLE == ROLE_Authority && !fFrozen && !fIsStunned && !bZapped && !bDecapitated && FMonsterController(Controller) != None)
    {
        if (fStairsCheckTime > 0)
            fStairsCheckTime -= DeltaTime;

        else
        {
            if (Location == fStoredStairLoc)
            {
                if (Level.TimeSeconds > fLastCanAttackTime + fStairsTimeFix)
                {
                    if (fStairsSecondCheck)
                    {
                        FStairFixJump();
                        fStairsSecondCheck = False;
                    }

                    else
                    {
                        fLastCanAttackTime = Level.TimeSeconds;
                        fStairsSecondCheck = True;
                    }
                }
            }

            fStairsCheckTime = Default.fStairsCheckTime;
            fStoredStairLoc  = Location;
        }
    }

    // don't move while frozen
    if (fFrozen)
    {
        GroundSpeed = 1;
        AirSpeed    = 0;
        WaterSpeed  = 0;
        return;
    }

    // if something made an uncontrollable speed change, put it under control. Not ideal but apparently works.
    else if (GroundSpeed != fCurGroundSpeed)
        SetGroundSpeed(GroundSpeed);

    // slow down you bastard (even if you were attacking while I zapped you <3)
    if (!fFrozen && bZapped && GroundSpeed > GetOriginalGroundSpeed() * ZappedSpeedMod)
        SetGroundSpeed(OriginalGroundSpeed * ZappedSpeedMod);

    // ...this doesn't make any sense? why should the head scale be reset after 0.2 seconds, past falk?! - sorry dood, can't remember shit about this - Past Falk
    //if (fHeadScale > default.fHeadScale && Level.TimeSeconds > DecapTime + 0.2)
    //  fHeadScale = default.fHeadScale;

    for (i=0; i<fHitList.length; i++) // apply shotgun head damage
    {
        if (fHitList[i].TotalHSDam > 0 && Level.TimeSeconds >= fHitList[i].ShotHSTime)
        {
            //log("Applying shotgun hs damage: "@fHitList[i].TotalHSDam);
            TakeDamage(fHitList[i].TotalHSDam, fHitList[i].fPawn, fHitList[i].hitlocation, fHitList[i].momentum, fHitList[i].damageType, 689);
            fHitList[i].TotalHSDam = 0;
        }
    }

    if (fShouldPlasma && !fPlasmaSpawned) // plasma backup spawn
    {
        Pr = Spawn(class'InstantPlasmaNadeFalk',,,fHitLoc,rotator(vect(0,0,1)));

        if (Pr != none)
        {
            fPlasmaSpawned = True;
            Pr.RemoteRole   = ROLE_None;
        }
    }

    // If we've flagged this character to be destroyed next tick, handle that
    if (bDestroyNextTick && TimeSetDestroyNextTickTime < Level.TimeSeconds)
    {
        Destroy();
    }

    if (bResetAnimAct && ResetAnimActTime<Level.TimeSeconds)
    {
        AnimAction = '';
        bResetAnimAct = False;
    }

    if (Controller != None)
    {
        LookTarget = Controller.Enemy;
    }

    if (Role == ROLE_Authority && bDecapitated)
    {
        // increase shock time
        if (fCurShockTime < fShockTime)
            fCurShockTime += DeltaTime;

        // If the Zed has been bleeding long enough, make it die
        if (BleedOutTime > 0 && Level.TimeSeconds - BleedOutTime >= 0)
        {
            // Make sure the bloat still counts for the achievement if killed by bleedout from a W1300
            if (FalkGetZedClass() == FALK_Bloat && class<DamTypeW1300_Compact_EditionFalk>(LastDamagedByType) != none)
                Died(LastDamagedBy.Controller,class'DamTypeW1300BleedOutFalk',Location);

            // use the lastdamagetype to make sure every zed that isn't a bloat counts for their achievements
            else if (FalkGetZedClass() != FALK_Bloat && LastDamagedByType != none)
                Died(LastDamagedBy.Controller,LastDamagedByType,Location);

            // vanilla fallback in case something weird is happening
            else
                Died(LastDamagedBy.Controller,class'DamTypeBleedOut',Location);

            BleedOutTime=0;
        }

    }

    if (Role == ROLE_Authority && fFlinching)
    {
        if (fFlinchTime > 0)
            fFlinchTime -= DeltaTime;

        else
        {
            fFlinchTime = Default.fFlinchTime;
            fFlinching  = False;
        }
    }

    if (Level.NetMode != NM_DedicatedServer)
    {
        TickFX(DeltaTime);

        if (bBurnified && !bBurnApplied)
        {
            if (!bGibbed)
            {
                StartBurnFX();
            }
        }
        else if (!bBurnified && bBurnApplied)
        {
            StopBurnFX();
        }

        if (bAshen && Level.NetMode == NM_Client && !class'GameInfo'.static.UseLowGore())
        {
            ZombieCrispUp();
            bAshen = False;
        }
    }

    if (DECAP)
    {
        if (Level.TimeSeconds > (DecapTime + 2.0) && Controller != none)
        {
            DECAP = false;
            MonsterController(Controller).ExecuteWhatToDoNext();
        }
    }

    if (bZapped && Role == ROLE_Authority)
    {
        RemainingZap -= DeltaTime;

        if(RemainingZap <= 0)
        {
            RemainingZap = 0;
            bZapped      = False;
            ZappedBy     = none;

            // The Zed can take more zap each time they get zapped
            if (!fZappedBySkill)
                ZapThreshold *= ZapResistanceScale;

            else
                fZappedBySkill = False;
        }
    }

    // reset total zap after a while
    if (!bZapped && TotalZap > 0 && Level.TimeSeconds >= LastZapTime + fZapResetTime)
        TotalZap = 0;

    if (bZapped != bOldZapped)
    {
        if(bZapped)
        {
            SetZappedBehavior();
        }
        else
        {
            UnSetZappedBehavior();
        }

        bOldZapped = bZapped;
    }

    if (fHarpoonHit)
    {
        SetAnimAction(HitAnims[Rand(3)]);
        bSTUNNED = true;
        SetTimer(StunTime,false);
        //StunsRemaining--;
        fHarpoonHit = False;
    }

    if (fWTCurTime < fWTCheck)
        fWTCurTime += DeltaTime;

    else
    {
        fWTCurTime = 0;

        if (KFMonsterController(Controller) != none && Controller.Target != none && SameSpeciesAs(Pawn(Controller.Target)))
        {
            //log("FIXED WRONG TARGET");
            KFMonsterController(Controller).FindNewEnemy();
        }
    }

    if (fIsStunned && Controller != none)
    {
        //warn("STUNNED 2:"@Level.TimeSeconds);
        Controller.Enemy      = None;
        Controller.Focus      = None;
        Controller.FocalPoint = fFocalPoint;
    }
}

// called on freeze to fix weird bugs, fix a bug with anims on attack after freeze
function fFreezeFix()
{
    bZapped          = False;
    fZappedBySkill   = True;
    bResetAnimAct    = False;
    ResetAnimActTime = Level.TimeSeconds-1;
}

// replicated on client to fix weird freeze bugs
simulated function fClientFreezeFix()
{
}

// called on unfreeze to fix weird bugs or restore variables
function fUnfreezeFix()
{
    fFrozen = False;
    fClientUnfreezeFix();
}

// replicated on client to fix weird unfreeze bugs or restore variables
simulated function fClientUnfreezeFix()
{
    fFrozen = False;
}

// shouldn't fight with our boss nor the metal clot
function bool SameSpeciesAs(Pawn P)
{
    return (FalkZombieBossBase(P) != none || FZombieMetalClot_STANDARD(P) != none || FalkZombieHuskBase(P) != none);
}


// return a fixed damage for the radial
function int FalkGetRadialDamage()
{
    if (fIsBoss)
        return 0;

    else if (Level.Game.GameDifficulty >= 8.0) // bb
        return 450;

    else if (Level.Game.GameDifficulty >= 7.0) // hoe
        return 400;

    else if (Level.Game.GameDifficulty >= 5.0) // suicidal
        return 350;

    else if (Level.Game.GameDifficulty >= 4.0) // hard
        return 300;

    return 250;
}

// scale health based on difficulty - modifer
function float DifficultyHealthModifer()
{
    if (Level.Game.GameDifficulty >= 8.0) // bb
        return 2.0;

    else if (Level.Game.GameDifficulty >= 7.0) // hoe
        return 1.75;

    else if (Level.Game.GameDifficulty >= 5.0) // suicidal
        return 1.5;

    else if (Level.Game.GameDifficulty >= 4.0) // hard
        return 1.25;

    return 1.0;
}


// scale damage based on difficulty - MODIFER
function float DifficultyDamageModifer()
{
    if (Level.Game.GameDifficulty >= 8.0) // Bloodbath
        return 1.8;

    else if (Level.Game.GameDifficulty >= 7.0) // Hell on Earth
        return 1.6;

    else if (Level.Game.GameDifficulty >= 5.0) // Suicidal
        return 1.4;

    else if (Level.Game.GameDifficulty >= 4.0) // Hard
        return 1.2;

    return 1.0; // Normal
}

// scale health using our variable if possible - MoDiFeR
function float NumPlayersHealthModifer()
{
    local Falk689GameTypeBase Flk;

    Flk = Falk689GameTypeBase(Level.Game);

    if (Flk == none)
        return Super.NumPlayersHealthModifer();

    return FMax(1.0, 1.0 + (Flk.FAlivePlayers - 1) * PlayerCountHealthScale);
}

// scale head health using our variable if possible - MOOOOODIFERRRRRR
function float NumPlayersHeadHealthModifer()
{
    local Falk689GameTypeBase Flk;

    Flk = Falk689GameTypeBase(Level.Game);

    if (Flk == none)
        return Super.NumPlayersHeadHealthModifer();

    return FMax(1.0, 1.0 + (Flk.FAlivePlayers - 1) * PlayerNumHeadHealthScale);
}


// alternative fire dot implementation
simulated function Timer()
{
    bSTUNNED = false;

    if (BurnDown > 0)
    {
        TakeFireDamage(LastBurnDamage, LastDamagedBy);
        SetTimer(1.0, false); // Sets timer function to be executed each second
    }

    else
    {
        UnSetBurningBehavior();
        RemoveFlamingEffects();
        StopBurnFX();
    }

    if (BileCount > 0)
    {
        TakeBileDamage();
        SetTimer(1.0, false); // Sets timer function to be executed each second
    }
}


// fix fire dealing excessive damage on this zed
function TakeFireDamage(int Damage, pawn Instigator)
{
    local Vector DummyHitLoc, DummyMomentum;

    TakeDamage(Damage, BurnInstigator, DummyHitLoc, DummyMomentum, FireDamageClass, 689);

    /*if (FireDamageClass == class'DamTypeBurnStrongFalk')
      {
      log("Strong Fire");
      if (fSFireMultiplier > 0)
      TakeDamage(Damage / fSFireMultiplier, BurnInstigator, DummyHitLoc, DummyMomentum, FireDamageClass, 689);

      else
      BurnDown = 0;
      }

      else
      {
      log("Weak Fire");
      log(FireDamageClass);
      if (fWFireMultiplier > 0)
      TakeDamage(Damage / fWFireMultiplier, BurnInstigator, DummyHitLoc, DummyMomentum, FireDamageClass, 689);

      else
      BurnDown = 0;
      }*/

    if (BurnDown > 0)
        BurnDown--;

    if (BurnDown < CrispUpThreshhold)
        ZombieCrispUp();

    if (BurnDown == 0)
        bBurnified = false;
}

// alternative acid damge 
function TakeBileDamage()
{
    TakeDamage(LastAcidDamage, BileInstigator, Location, vect(0,0,0), LastBileDamagedByType, 689);
    BileCount--;
}


// removed some weird code, made less likely to blow up zeds with random damage
simulated function DoDamageFX(Name boneName, int Damage, class<DamageType> DamageType, Rotator r)
{
    local float DismemberProbability;
    local int RandBone;
    local bool bDidSever;

    //log("DamageType: "@DamageType);
    //log("DamageFX bonename = "$boneName$" "$Level.TimeSeconds$" Damage "$Damage);

    if(bDecapitated && !bPlayBrainSplash)
    {
        if(DecapitatedByMelee)
            HitFX[HitFxTicker].damtype = class'DamTypeMeleeDecapitation';

        else if(class<DamTypeNailGunFalk>(DamageType) != none)
            HitFX[HitFxTicker].damtype = class'DamTypeProjectileDecap';

        else
            HitFX[HitFxTicker].damtype = class'DamTypeDecapitation';

        if (DamageType.default.bNeverSevers || class'GameInfo'.static.UseLowGore() ||
                (Level.Game != none && Level.Game.PreventSever(self, boneName, Damage, DamageType)))
        {
            HitFX[HitFxTicker].bSever = false;
        }

        else
            HitFX[HitFxTicker].bSever = true;

        HitFX[HitFxTicker].bone = HeadBone;
        HitFX[HitFxTicker].rotDir = r;
        HitFxTicker = HitFxTicker + 1;

        if(HitFxTicker > ArrayCount(HitFX) - 1)
            HitFxTicker = 0;

        bPlayBrainSplash = true;
    }

    if (FRand() > 0.3f || Damage > 30 || Health <= 0)
    {
        HitFX[HitFxTicker].damtype = DamageType;

        if(Health <= 0)
        {
            switch(boneName)
            {
                case 'neck':
                    boneName = HeadBone;
                    break;

                case LeftFootBone:
                case 'lleg':
                    boneName = LeftThighBone;
                    break;

                case RightFootBone:
                case 'rleg':
                    boneName = RightThighBone;
                    break;

                case RightHandBone:
                case RightShoulderBone:
                case 'rarm':
                    boneName = RightFArmBone;
                    break;

                case LeftHandBone:
                case LeftShoulderBone:
                case 'larm':
                    boneName = LeftFArmBone;
                    break;

                case 'None':
                case 'spine':
                    boneName = FireRootBone;
                    break;
            }

            if(DamageType.default.bAlwaysSevers || Damage == 10000)
            {
                //log("Force Sever");
                HitFX[HitFxTicker].bSever = true;
                bDidSever = true;

                if (boneName == 'None')
                    boneName = FireRootBone;
            }

            else if(DamageType.Default.GibModifier > 0.0)
            {
                DismemberProbability = Abs((Health - Damage*DamageType.Default.GibModifier) / 130.0f);

                if(FRand() < DismemberProbability)
                {
                    //log("Sever");
                    HitFX[HitFxTicker].bSever = true;
                    bDidSever = true;
                }
            }
        }

        if (DamageType.default.bNeverSevers || class'GameInfo'.static.UseLowGore() ||
                (Level.Game != none && Level.Game.PreventSever(self, boneName, Damage, DamageType)))
        {
            HitFX[HitFxTicker].bSever = false;
            bDidSever = false;
        }

        if (HitFX[HitFxTicker].bSever)
        {
            if(!DamageType.default.bLocationalHit && (boneName == 'None' || boneName == FireRootBone || boneName == 'Spine'))
            {
                RandBone = Rand(4);

                switch(RandBone)
                {
                    case 0:
                        boneName = LeftThighBone;
                        break;

                    case 1:
                        boneName = RightThighBone;
                        break;

                    case 2:
                        boneName = LeftFArmBone;
                        break;

                    case 3:
                        boneName = RightFArmBone;
                        break;

                    case 4:
                        boneName = HeadBone;
                        break;

                    default:
                        boneName = LeftThighBone;
                }
            }
        }

        if (DamageType != class'DamTypeGrenadeImpactFalk' && Health < 0 && Damage > DamageType.default.HumanObliterationThreshhold && Damage != 10000 && !class'GameInfo'.static.UseLowGore())
        {
            //log("Obliterate");
            boneName = 'obliterate';
        }

        HitFX[HitFxTicker].bone   = boneName;
        HitFX[HitFxTicker].rotDir = r;
        HitFxTicker               = HitFxTicker + 1;

        if (HitFxTicker > ArrayCount(HitFX) - 1)
            HitFxTicker = 0;

        // If this was a really hardcore damage from an explosion, randomly spawn some arms and legs
        if (bDidSever && !DamageType.default.bLocationalHit && Damage > 200 && Damage != 10000 && !class'GameInfo'.static.UseLowGore())
        {
            //log("High Damage");
            if ((Damage > 400 && FRand() < 0.3) || FRand() < 0.1)
            {
                //log("Kaboom");
                DoDamageFX(HeadBone,10000,DamageType,r);
                DoDamageFX(LeftThighBone,10000,DamageType,r);
                DoDamageFX(RightThighBone,10000,DamageType,r);
                DoDamageFX(LeftFArmBone,10000,DamageType,r);
                DoDamageFX(RightFArmBone,10000,DamageType,r);
            }

            if (FRand() < 0.25)
            {
                //log("Semi-Kaboom");
                DoDamageFX(LeftThighBone,10000,DamageType,r);
                DoDamageFX(RightThighBone,10000,DamageType,r);
                if (FRand() < 0.5)
                    DoDamageFX(LeftFArmBone,10000,DamageType,r);

                else
                    DoDamageFX(RightFArmBone,10000,DamageType,r);
            }

            else if (FRand() < 0.35)
            {
                //log("Left Thigh");
                DoDamageFX(LeftThighBone,10000,DamageType,r);
            }

            else if (FRand() < 0.5)
            {
                //log("Right Thigh");
                DoDamageFX(RightThighBone,10000,DamageType,r);
            }

            else if (FRand() < 0.75)
            {
                //log("Arms");
                if (FRand() < 0.5)
                    DoDamageFX(LeftFArmBone,10000,DamageType,r);

                else
                    DoDamageFX(RightFArmBone,10000,DamageType,r);
            }
        }
    }
}

// don't feel pain while frozen or about to be, or... just walk it off
simulated function PlayTakeHit(vector HitLocation, int Damage, class<DamageType> DamageType)
{
    local int FistStrikeStunChance;
    local Class<KFWeaponDamageType> KFWDT;
    local KFPlayerReplicationInfo   KFPRI;
    local class<FVeterancyTypes>    Vet;

    if (fFrozen)
        return;

    if (Level.TimeSeconds - LastPainAnim < MinTimeBetweenPainAnims)
        return;

    // we're freezing, hide the pain
    if (class<DamTypeFreezeBomb>(damageType) != none)
        return;

    if (class<DamTypeSW76Falk>(damageType) != none)
    {
        if (LastDamagedBy != none)
        {
            KFPRI = KFPlayerReplicationInfo(LastDamagedBy.PlayerReplicationInfo);

            if (KFPRI != none)
            {
                Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

                if (Vet != none && Vet.Static.ArtilleristZedTimeBonus(KFPRI))
                    return;
            }
        }
    }

    KFWDT = Class<KFWeaponDamageType>(DamageType);

    // No anim if we're burning, we're already playing an anim
    if (!(bCrispified || bBurnified) && KFWDT != none)
    {
        LastPainAnim = Level.TimeSeconds;

        if (Damage >= 5)
            PlayDirectionalHit(HitLocation);

        else if (KFWDT.default.bIsExplosive || KFWDT.default.bIsPowerWeapon || class<DamTypeBossRadialFalk>(damageType) != none)
            PlayDirectionalHit(HitLocation);

        else if (DamageType.name == 'DamTypeClaws')
        {
            FistStrikeStunChance = rand(10);

            if (FistStrikeStunChance > 5)
                PlayDirectionalHit(HitLocation);
        }

        else if (DamageType.name == 'DamTypeKnife')
        {
            FistStrikeStunChance = rand(10);

            if (FistStrikeStunChance > 7)
                PlayDirectionalHit(HitLocation);
        }

        else if (DamageType.name == 'DamTypeChainsaw')
            PlayDirectionalHit(HitLocation);

        else if (DamageType.name == 'DamTypeCrossbowHeadshot' ||
                DamageType.name == 'DamTypeCrossbuzzsaw'     ||
                DamageType.name == 'DamTypeCrossbowHeadShot')
        {
            PlayDirectionalHit(HitLocation);
        }
    }

    if (Level.TimeSeconds - LastPainSound < MinTimeBetweenPainSounds)
        return;

    LastPainSound = Level.TimeSeconds;

    if (class<DamTypeBurned>(DamageType) == none && class<DamTypeFlamethrower>(DamageType) == none)
        PlaySound(HitSound[0], SLOT_Pain,1.25,,400);
}

// Set the zed to the on fire behavior, tweaking the speed in a controlled way
simulated function SetBurningBehavior()
{
    if (Role == Role_Authority)
    {
        Intelligence = BRAINS_Retarded;

        SetGroundSpeed(OriginalGroundSpeed * fBurningSpeed);
        AirSpeed   *= fBurningSpeed;
        WaterSpeed *= fBurningSpeed;

        // Make them less accurate while they are burning
        if (Controller != none)
            MonsterController(Controller).Accuracy = -5;  // More chance of missing. (he's burning now, after all) B======D
    }

    // Set the forward movement anim to a random burning anim
    MovementAnims[0] = BurningWalkFAnims[Rand(3)];
    WalkAnims[0]     = BurningWalkFAnims[Rand(3)];

    // Set the rest of the movement anims to the headless anim (not sure if these ever even get played) - Ramm
    MovementAnims[1] = BurningWalkAnims[0];
    WalkAnims[1]     = BurningWalkAnims[0];
    MovementAnims[2] = BurningWalkAnims[1];
    WalkAnims[2]     = BurningWalkAnims[1];
    MovementAnims[3] = BurningWalkAnims[2];
    WalkAnims[3]     = BurningWalkAnims[2];
}

// fixed slowrage on burn
simulated function UnSetBurningBehavior()
{
    local int i;

    // Don't turn off this behavior until the harpoon stun is over
    /*if (bHarpoonStunned)
      return;*/

    if (Role == Role_Authority)
    {
        Intelligence = default.Intelligence;

        // Set normal accuracy
        if (Controller != none)
            MonsterController(Controller).Accuracy = MonsterController(Controller).default.Accuracy;
    }

    bAshen = False;

    // restore regular anims
    for (i = 0; i < 4; i++)
    {
        MovementAnims[i] = default.MovementAnims[i];
        WalkAnims[i]     = default.WalkAnims[i];
    }
}

// set if our zed was zapped by an instant nade
function SetZapped(float ZapAmount, Pawn Instigator)
{
    // don't zap if we're already frozen
    if (fFrozen)
        return;

    if (ZapAmount == 689)
    {
        bZapped        = True;
        fZappedBySkill = True;
    }

    LastZapTime = Level.TimeSeconds;

    if (bZapped)
    {
        TotalZap = ZapThreshold;
        RemainingZap = ZapDuration;
        SetOverlayMaterial(Material'LairTextures_T.ZedsUpscale.PlasmaShader', RemainingZap, true);
    }

    else
    {
        TotalZap += ZapAmount;

        if (TotalZap >= ZapThreshold)
        {
            RemainingZap = ZapDuration;
            SetOverlayMaterial(Material'LairTextures_T.ZedsUpscale.PlasmaShader', RemainingZap, true);
            bZapped = true;
        }
    }

    ZappedBy = Instigator;
}

// trying to fix slowrage on zap, first part (not sure if needed anymore but shouldn't hurt - future Falk)
simulated function SetZappedBehavior()
{
    if (fFrozen)
        return;

    if (Role == Role_Authority)
    {
        Intelligence = BRAINS_Retarded; // burning dumbasses!

        SetGroundSpeed(OriginalGroundSpeed * ZappedSpeedMod);
        AirSpeed    *= ZappedSpeedMod;
        WaterSpeed  *= ZappedSpeedMod;

        // Make them less accurate while they are zapped
        if (Controller != none)
            MonsterController(Controller).Accuracy = -5;
    }
}

// trying to fix slowrage on zap, second part
simulated function UnSetZappedBehavior()
{
    //local int i;

    if (Role == Role_Authority)
    {
        Intelligence = default.Intelligence;

        SetGroundSpeed(fGroundSpeed);

        AirSpeed     = fAirSpeed;
        WaterSpeed   = fWaterSpeed;

        // Set normal accuracy
        if (Controller != none)
            MonsterController(Controller).Accuracy = MonsterController(Controller).default.Accuracy;
    }

    // restore regular anims
    /*for (i = 0; i < 4; i++)
      {
      MovementAnims[i] = default.MovementAnims[i];
      WalkAnims[i]     = default.WalkAnims[i];
      }*/
}

// storing current speeds before editing but only if speeds are at least default or possible numbers, let's not store freezing states
simulated function SetGroundSpeed(float NewGroundSpeed)
{
    if (NewGroundSpeed == fCurGroundSpeed)
    {
        //warn("block");
        return;
    }

    if ((bDecapitated || DecapitatedBy != none) && NewGroundSpeed < GetOriginalGroundSpeed() && NewGroundSpeed > 0)
    {
        fGroundSpeed = NewGroundSpeed;
        fWaterSpeed  = NewGroundSpeed;
        fAirSpeed    = NewGroundSpeed;
    }

    else if (fShouldRage && ((GroundSpeed < GetOriginalGroundSpeed() * fRageSpeed && GroundSpeed >= GetOriginalGroundSpeed()) || 
                (NewGroundSpeed <= GetOriginalGroundSpeed() * fRageSpeed && NewGroundSpeed >= GetOriginalGroundSpeed() && NewGroundSpeed < GetOriginalGroundSpeed() * fRageSpeed)))
    {
        /*if (NewGroundSpeed > GroundSpeed)
          {
          fGroundSpeed = NewGroundSpeed;
          warn("Store New Speed, Zed Can Rage:"@NewGroundSpeed@"Time:"@Level.TimeSeconds);
          }

          else
          {
          fGroundSpeed = GroundSpeed;
          warn("Store Speed, Zed Can Rage:"@NewGroundSpeed@"Time:"@Level.TimeSeconds);
          }*/

        fGroundSpeed = FMin(fRageSpeedCap, GetOriginalGroundSpeed() * fRageSpeed);
        fWaterSpeed  = WaterSpeed;
        fAirSpeed    = AirSpeed;
    }

    else if (!fShouldRage)
    {
        fGroundSpeed = GetOriginalGroundSpeed();
        fWaterSpeed  = Default.WaterSpeed;
        fAirSpeed    = Default.AirSpeed;
    }

    GroundSpeed     = FMin(fRageSpeedCap, NewGroundSpeed);
    fCurGroundSpeed = GroundSpeed;

    //warn("Setting Speed to:"@GroundSpeed@"Time:"@Level.TimeSeconds);
}

// boss resist damage
function int fBossResist(int Damage, int fTempHealth, optional class<DamageType> damageType)
{
    local int tempDamage;

    // nnnnnope
    if (FBlockAllDamage)
        return 0;

    if (FIsBoss && FSyringeCount < 3)
    {
        //warn("Orig: "@Damage);

        // resist damage when fleeing/healing
        if (fTempHealth <= FHealLevel)
        {
            // just resist freezing while fleeing
            /*if (damageType == class'DamTypeFreezeBomb' || damageType == class'DamTypeFreezerGun')
              Damage = 0;

              else*/
            Damage = float(Damage) * 0.2f;
            //warn("Resist: "@Damage);
        }

        // resist part of heavy damages even if not healing/fleeing
        else if (fTempHealth - Damage < FHealLevel)
        {
            tempDamage = fTempHealth - FHealLevel;
            Damage     = tempDamage + (float(Damage) - tempDamage) * 0.2f;
            //warn("Resist: "@Damage);
        }
    }

    return Damage;
}

// fix some random errors
simulated function ToggleAuxCollision(bool newbCollision)
{
    if (MyExtCollision == none)
        return;

    Super.ToggleAuxCollision(newbCollision);
}

// choose an honorable death
function FSeppuku()
{
    fZedTimeBlock = True;
    Died(Controller, class'KFWeaponDamageType', Location);
}

// welp, this was needed but I'm not sure why - Falk689
function SetDelayedDamageInstigatorController(Controller C)
{
    if (Controller != none && FMonsterController(Controller) != none && C != none)
        FMonsterController(Controller).fLastDamagedByController = C;

    DelayedDamageInstigatorController = C;
}

// first attempt to prevent fake attacks from decapitated or frozen zeds, implement shock time
function bool CanAttack(Actor A)
{
    if (fFrozen || bDecapitated && fCurShockTime < fShockTime)
        return False;

    if (Super.CanAttack(A))
    {
        if (Role == ROLE_Authority)
        {
            //warn("Attack:"@Level.TimeSeconds);

            fLastCanAttackTime = Level.TimeSeconds;
        }

        return True;
    }

    return False;
}

// stun them only once to prevent exploits
function bool FlipOver()
{
    if (!fAlreadyStunned && !fFrozen && !bShotAnim)
    {
        fFocalPoint      = Location + 512 * vector(Rotation);

        if (Controller != none)
        {
            //warn("CONTROLLER STORE:"@Level.TimeSeconds);
            fStoredStunEnemy = Controller.Enemy;
            fStoredStunFocus = Controller.Focus;

            Controller.Enemy = none;
            Controller.Focus = none;
        }

        fAlreadyStunned  = True;
        bShotAnim        = false;
        bWaitForAnim     = false;
        //warn("AnimEnd1 Time:"@Level.TimeSeconds);
        AnimEnd(2);
        AnimEnd(1);
        AnimEnd(0);
        fIsStunned       = True;
        //warn("fIsStunned = True - Time:"@Level.TimeSeconds);
        fClientFlipOver();
        return Super.FlipOver();
    }

    return False;
}

simulated function fClientFlipOver()
{
    bShotAnim        = false;
    bWaitForAnim     = false;
    AnimEnd(2);
    AnimEnd(1);
    AnimEnd(0);
    fIsStunned        = True;
    //warn("fIsStunned Client = True - Time:"@Level.TimeSeconds);

    if (Physics == PHYS_Falling)
        SetPhysics(PHYS_Walking);

    bShotAnim        = true;
    SetAnimAction('KnockDown');
    Acceleration     = vect(0, 0, 0);
    Velocity.X       = 0;
    Velocity.Y       = 0;

    if (Controller != None)
    {
        Controller.GoToState('WaitForAnim');
        KFMonsterController(Controller).bUseFreezeHack = True;
    }
}

// stun zeds in a less retarded way
function PlayHit(float Damage, Pawn InstigatedBy, vector HitLocation, class<DamageType> damageType, vector Momentum, optional int HitIdx)
{
    local Vector HitNormal;
    local Vector HitRay;
    local Name HitBone;
    local float HitBoneDist;
    local PlayerController PC;
    local bool bShowEffects, bRecentHit;
    local ProjectileBloodSplat BloodHit;
    local rotator SplatRot;
    local KFPlayerReplicationInfo   KFPRI;
    local class<FVeterancyTypes>    Vet;
    local bool fASkill;

    if (class<DamTypeFreezerGun>(damageType) != none) // don't animate when hit by a freeze nade
        return;

    bRecentHit = Level.TimeSeconds - LastPainTime < 0.2;

    LastDamageAmount = Damage;

    // Call the modified version of the original Pawn playhit
    OldPlayHit(Damage, InstigatedBy, HitLocation, DamageType,Momentum);

    if (Damage <= 0)
        return;

    // artillerist zed time bonus, don't stun if we're freezing
    if (class<DamTypeSW76Falk>(damageType) != none)
    {
        if (instigatedBy != none)
        {
            KFPRI = KFPlayerReplicationInfo(instigatedBy.PlayerReplicationInfo);

            if (KFPRI != none)
            {
                Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

                if (Vet != none && Vet.Static.ArtilleristZedTimeBonus(KFPRI))
                    fASkill = True;
            }
        }
    }

    // stun based on fixed or relative (to HealthMax) amount of damage, implemented stun by husk gun and throwing knife
    if (Health > 0 && !fASkill && (HitIdx == -689 || class<DamTypeHuskGunStrongFalk>(damageType) != none || ((HitIdx != 689 || class<KFWeaponDamageType>(damageType) == none || !class<KFWeaponDamageType>(damageType).default.bIsPowerWeapon) && (Damage > fUsedThresh || Damage > HealthMax * fStunMult))))
        FlipOver();

    PC = PlayerController(Controller);
    bShowEffects = ((Level.NetMode != NM_Standalone) || (Level.TimeSeconds - LastRenderTime < 2.5)
            || ((InstigatedBy != None) && (PlayerController(InstigatedBy.Controller) != None))
            || (PC != None));

    if (!bShowEffects)
        return;

    if (BurnDown > 0 && !bBurnified)
    {
        bBurnified = true;
    }

    HitRay = vect(0,0,0);
    if(InstigatedBy != None)
        HitRay = Normal(HitLocation-(InstigatedBy.Location+(vect(0,0,1)*InstigatedBy.EyeHeight)));

    if(DamageType.default.bLocationalHit)
    {
        CalcHitLoc(HitLocation, HitRay, HitBone, HitBoneDist);

        // Do a zapped effect is someone shoots us and we're zapped to help show that the zed is taking more damage
        if (bZapped && DamageType.name != 'DamTypeZEDGun')
        {
            PlaySound(class'ZedGunProjectile'.default.ExplosionSound,,class'ZedGunProjectile'.default.ExplosionSoundVolume);
            Spawn(class'ZedGunProjectile'.default.ExplosionEmitter,,,HitLocation + HitNormal*20,rotator(HitNormal));
        }
    }
    else
    {
        HitLocation = Location;
        HitBone = FireRootBone;
        HitBoneDist = 0.0f;
    }

    if(DamageType.default.bAlwaysSevers && DamageType.default.bSpecial)
        HitBone = 'head';

    if(InstigatedBy != None)
        HitNormal = Normal(Normal(InstigatedBy.Location-HitLocation) + VRand() * 0.2 + vect(0,0,2.8));
    else
        HitNormal = Normal(Vect(0,0,1) + VRand() * 0.2 + vect(0,0,2.8));

    //log("HitLocation "$Hitlocation);

    if (DamageType.Default.bCausesBlood && (!bRecentHit || (bRecentHit && (FRand() > 0.8))))
    {
        if (!class'GameInfo'.static.NoBlood() && !class'GameInfo'.static.UseLowGore())
        {
            if (Momentum != vect(0,0,0))
                SplatRot = rotator(Normal(Momentum));
            else
            {
                if (InstigatedBy != None)
                    SplatRot = rotator(Normal(Location - InstigatedBy.Location));
                else
                    SplatRot = rotator(Normal(Location - HitLocation));
            }

            BloodHit = Spawn(ProjectileBloodSplatClass,InstigatedBy,, HitLocation, SplatRot);
        }
    }

    if(InstigatedBy != none && InstigatedBy.PlayerReplicationInfo != none &&
            KFSteamStatsAndAchievements(InstigatedBy.PlayerReplicationInfo.SteamStatsAndAchievements) != none &&
            Health <= 0 && Damage > DamageType.default.HumanObliterationThreshhold && Damage != 1000 && (!bDecapitated || bPlayBrainSplash))
    {
        KFSteamStatsAndAchievements(InstigatedBy.PlayerReplicationInfo.SteamStatsAndAchievements).AddGibKill(class<DamTypeM79Grenade>(damageType) != none);

        if (self.IsA('ZombieFleshPound'))
        {
            KFSteamStatsAndAchievements(InstigatedBy.PlayerReplicationInfo.SteamStatsAndAchievements).AddFleshpoundGibKill();
        }
    }

    DoDamageFX(HitBone, Damage, DamageType, Rotator(HitNormal));

    if (DamageType.default.DamageOverlayMaterial != None && Damage > 0) // additional check in case shield absorbed
        SetOverlayMaterial(DamageType.default.DamageOverlayMaterial, DamageType.default.DamageOverlayTime, false);
}

// set stun state to false after the anim ends
simulated function AnimEnd(int Channel)
{
    AnimAction  = '';

    if (Level.NetMode != NM_Client)
    {
        bResetAnimAct    = True;
        ResetAnimActTime = Level.TimeSeconds + 0.3;
    }

    if (bShotAnim && Channel == ExpectingChannel)
    {
        bShotAnim  = false;
        fIsStunned = false;  
        //warn("fIsStunned = False - Time:"@Level.TimeSeconds);

        if (Controller != none)
        {
            //warn("CONTROLLER RESTORE:"@Level.TimeSeconds);

            if (Controller.Enemy == none && fStoredStunEnemy != none)
                Controller.Enemy = fStoredStunEnemy;

            if (Controller.Focus == none && fStoredStunEnemy != none)
                Controller.Focus = fStoredStunFocus;
        }

        SetGroundSpeed(GetOriginalGroundSpeed());

        if (Controller != None)
            Controller.bPreparingMove = false;
    }

    if (!bPhysicsAnimUpdate && Channel==0)
        bPhysicsAnimUpdate = Default.bPhysicsAnimUpdate;


    Super(xPawn).AnimEnd(Channel);
}

// Used to complete a zed freeze after its animation is complete
function FalkFreezeZed()
{
    //warn("LOCAL FREEZE ZED:"@Level.TimeSeconds);

    fFrozen      = True;

    AnimEnd(2);
    AnimEnd(1);
    AnimEnd(0);

    //fWasFrozen     = False;
    fIsStunned     = False;
    Velocity       = PhysicsVolume.Gravity;

    bShotAnim      = True;
    bWaitForAnim   = True;

    Acceleration = vect(0, 0, 0);

    Disable('AnimEnd');

    SetTimer(0, false);
    StopAnimating();

    //if (HeadRadius > 0)
    //   HeadRadius = -HeadRadius; // disable headshots

    if (Controller != none)
    {
        Controller.Enemy = none;
        Controller.Focus = none;   

        if (!Controller.IsInState('WaitForAnim'))
            Controller.GoToState('WaitForAnim');        

        KFMonsterController(Controller).bUseFreezeHack = True;
    }

    fClientFreezeZed();
}

// make sure they stop animating on clients
simulated function fClientFreezeZed()
{
    Velocity       = PhysicsVolume.Gravity;

    bShotAnim      = True;
    bWaitForAnim   = True;

    Acceleration = vect(0, 0, 0);

    StopAnimating();
}

// don't set new animations while frozen
simulated event SetAnimAction(name NewAction)
{
    if (fFrozen)
        return;

    Super.SetAnimAction(NewAction);

}

// set fAnimAction to current anim and return while frozen
simulated function int DoAnimAction(name AnimName)
{
    if (fFrozen)
        return 0;

    //warn("DoAnimAction:"@AnimName@"Time:"@Level.TimeSeconds);

    //if (Level.NetMode != NM_Client)
    fAnimEndTime     = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;

    return Super.DoAnimAction(AnimName);
}

// use our shader on crisped up zeds
simulated function ZombieCrispUp()
{
    bAshen = true;
    bCrispified = true;

    SetBurningBehavior();

    if (Level.NetMode == NM_DedicatedServer || class'GameInfo'.static.UseLowGore())
        Return;

    Skins[0]=Shader'LairTextures_T.ZedsUpscale.ZedBurnSkinShader';
    Skins[1]=Shader'LairTextures_T.ZedsUpscale.ZedBurnSkinShader';
    Skins[2]=Shader'LairTextures_T.ZedsUpscale.ZedBurnSkinShader';
    Skins[3]=Shader'LairTextures_T.ZedsUpscale.ZedBurnSkinShader';
}

// if frozen, shatter
function Died(Controller Killer, class<DamageType> damageType, vector HitLocation)
{
    if (fFrozen)
    {
        Spawn(ShatteredIce,,,Location);
        bHidden = true;
    }

    super.Died(Killer, damageType, HitLocation);
}

// trying a new slot to interrupt the sound when needed
function ZombieMoan()
{
    PlaySound(MoanVoice, SLOT_Talk, MoanVolume, false, 250.0);
}

// just get the original ground speed till I figure why some zed should be controlled by what (so kinda like never... lol)
simulated function float GetOriginalGroundSpeed()
{
    return OriginalGroundSpeed;
}

// get the damage this zed should deal with attacks against humans and doors
function float FalkGetDamage(float inDamage, optional KFDoorMover fDoor)
{
    local float fOutDamage;

    fLastCanAttackTime = Level.TimeSeconds; // prevent stairs fix at this point too

    if (fDoor == none && Controller != none && Controller.Target != none && KFDoorMover(Controller.Target) != none)
        fDoor = KFDoorMover(Controller.Target);

    if (fDoor != none)
    {
        fOutDamage = fDoorDamage;

        if (fLastDoorTarget != fDoor && (fLastDoorTarget == none || fLastDoorTarget.MyTrigger != fDoor.MyTrigger))
        {
            //warn("ATTACK RESET"@fLastDoorTarget@"to"@fDoor@"time"@Level.TimeSeconds);
            fLastDoorTarget = fDoor;
            fDoorDamage     = inDamage;
            fOutDamage      = fDoorDamage;
        }

        if (fDoorDamageStep > 0)
            fDoorDamageStep--;

        else
        {
            fDoorDamageStep = Default.fDoorDamageStep;
            fDoorDamage    += fAddDoorDamage;
        }

        //warn("USED DOOR DAMAGE"@fOutDamage@"- Time"@Level.TimeSeconds);
        fDoorDamageResetTime = Level.TimeSeconds + fDoorDamageResetDelay;
        return fOutDamage;
    }

    return inDamage;
}

// reset the door damage after a while
function FalkResetDoorDamage()
{
    //warn("DELAYED RESET"@Level.TimeSeconds);
    fDoorDamage = MeleeDamage;
}

// don't play a sound while zapped, frozen or stunned
function ClawDamageTarget()
{
    local vector PushDir;

    if (Controller != none && Controller.Target != none)
        PushDir = (damageForce * Normal(Controller.Target.Location - Location));

    else
        PushDir = damageForce * vector(Rotation);

    if (!fFrozen && !bZapped && !fIsStunned && !fFlinching && MeleeDamageTarget(FalkGetDamage(MeleeDamage), PushDir))
    {
        fLastClawDamageTime = Level.TimeSeconds;
        PlaySound(MeleeAttackHitSound, SLOT_Interact, 2.0);
    }
}

// removed weird stuff, added flinch fix
function PlayDirectionalHit(Vector HitLoc)
{
    local Vector X,Y,Z, Dir;

    GetAxes(Rotation, X,Y,Z);
    HitLoc.Z = Location.Z;
    Dir = -Normal(Location - HitLoc);

    if (!HitCanInterruptAction() || fNextFlinch > Level.TimeSeconds)
        return;

    if (VSize(Location - HitLoc) < 1.0)
        Dir = VRand();

    else
        Dir = -Normal(Location - HitLoc);

    fFlinchTime     = Default.fFlinchTime;
    fFlinching      = True;
    fNextFlinch     = Level.TimeSeconds + Default.fFlinchTime + fFlinchCooldown;

    if (Dir dot X > 0.7 || Dir == vect(0,0,0))
        SetAnimAction(KFHitFront);

    else if (Dir Dot X < -0.7)
        SetAnimAction(KFHitBack);

    else if (Dir Dot Y > 0)
        SetAnimAction(KFHitRight);

    else
        SetAnimAction(KFHitLeft);
}

// By default don't interrupt actions
simulated function bool HitCanInterruptAction()
{
    return !bShotAnim;
}


// Added burnt head effect
simulated function DecapFX(Vector DecapLocation, Rotator DecapRotation, bool bSpawnDetachedHead, optional bool bNoBrainBits)
{
    local float GibPerterbation;
    local BrainSplash SplatExplosion;
    local int i;

    // don't detach limbs while exploding if you're a bloat
    if (bCrispified && bPlayBileSplash)
        return;

    // do the kawaii version of the Decapitation - how about... nope?
    /*if (class'GameInfo'.static.UseLowGore())
      {
      CuteDecapFX();

      return;
      }*/

    bNoBrainBitEmitter = bNoBrainBits;

    GibPerterbation = 0.060000;


    if (bSpawnDetachedHead)
        SpecialHideHead();

    else
        HideBone(HeadBone);

    if (bSpawnDetachedHead)
    {
        if (bCrispified && BurntDetachedHeadClass != Class'KFChar.SeveredHeadClot')
            SpawnSeveredGiblet(BurntDetachedHeadClass, DecapLocation, DecapRotation, GibPerterbation, GetBoneRotation(HeadBone));

        else
            SpawnSeveredGiblet(DetachedHeadClass, DecapLocation, DecapRotation, GibPerterbation, GetBoneRotation(HeadBone));
    }

    // Plug in headless anims if we have them
    for (i = 0; i < 4; i++)
    {
        if (HeadlessWalkAnims[i] != '' && HasAnim(HeadlessWalkAnims[i]))
        {
            MovementAnims[i] = HeadlessWalkAnims[i];
            WalkAnims[i]     = HeadlessWalkAnims[i];
        }
    }

    if (!bSpawnDetachedHead && !bNoBrainBits && EffectIsRelevant(DecapLocation,false))
    {
        KFSpawnGiblet(class 'KFMod.KFGibBrain',DecapLocation, self.Rotation, GibPerterbation, 250);
        KFSpawnGiblet(class 'KFMod.KFGibBrainb',DecapLocation, self.Rotation, GibPerterbation, 250);
        KFSpawnGiblet(class 'KFMod.KFGibBrain',DecapLocation, self.Rotation, GibPerterbation, 250);
    }

    SplatExplosion = Spawn(class 'BrainSplash',self,, DecapLocation);
}

simulated function HideBone(name boneName)
{
    local int BoneScaleSlot;
    local coords boneCoords;

    if (boneName == HeadBone)
    {

        //warn("Hide Head Call"@Level.TimeSeconds);
        // Only scale the bone down once
        if (SeveredHead == none)
        {
            //warn("OK HEAD"@Level.TimeSeconds);
            boneScaleSlot    = 4;
            SeveredHead      = Spawn(SeveredHeadAttachClass,self);
            SeveredHead.SetDrawScale(SeveredHeadAttachScale);
            boneCoords       = GetBoneCoords('neck');

            if (bNoBrainBitEmitter)
                AttachEmitterEffect(NeckSpurtNoGibEmitterClass, 'neck', boneCoords.Origin, rot(0,0,0));

            else
                AttachEmitterEffect(NeckSpurtEmitterClass, 'neck', boneCoords.Origin, rot(0,0,0));

            AttachToBone(SeveredHead, 'neck');
            SetBoneScale(BoneScaleSlot, 0.0, BoneName);

            return;
        }

        else
        {
            return;
        }
    }

    super.HideBone(BoneName);
}

// Added burnt limbs effect
simulated function ProcessHitFX()
{
    local Coords boneCoords;
    local class<xEmitter> HitEffects[4];
    local int i,j;
    local float GibPerterbation;

    if((Level.NetMode == NM_DedicatedServer) || bSkeletized || (Mesh == SkeletonMesh))
    {
        SimHitFxTicker = HitFxTicker;
        return;
    }

    for (SimHitFxTicker = SimHitFxTicker; SimHitFxTicker != HitFxTicker; SimHitFxTicker = (SimHitFxTicker + 1) % ArrayCount(HitFX))
    {
        j++;
        if (j > 30)
        {
            SimHitFxTicker = HitFxTicker;
            return;
        }

        if((HitFX[SimHitFxTicker].damtype == None) || (Level.bDropDetail && (Level.TimeSeconds - LastRenderTime > 3) && !IsHumanControlled()))
            continue;

        //log("Processing effects for damtype "$HitFX[SimHitFxTicker].damtype);

        if(HitFX[SimHitFxTicker].bone == 'obliterate' && !class'GameInfo'.static.UseLowGore())
        {
            SpawnGibs(HitFX[SimHitFxTicker].rotDir, 1);
            bGibbed = true;

            // Wait a tick on a listen server so the obliteration can replicate before the pawn is destroyed
            if(Level.NetMode == NM_ListenServer)
            {
                bDestroyNextTick = true;
                TimeSetDestroyNextTickTime = Level.TimeSeconds;
            }
            else
            {
                Destroy();
            }
            return;
        }

        boneCoords = GetBoneCoords(HitFX[SimHitFxTicker].bone);

        if (!Level.bDropDetail && !class'GameInfo'.static.NoBlood() && !bSkeletized && !class'GameInfo'.static.UseLowGore())
        {
            //AttachEmitterEffect(BleedingEmitterClass, HitFX[SimHitFxTicker].bone, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir);

            HitFX[SimHitFxTicker].damtype.static.GetHitEffects(HitEffects, Health);

            if(!PhysicsVolume.bWaterVolume) // don't attach effects under water
            {
                for(i = 0; i < ArrayCount(HitEffects); i++)
                {
                    if(HitEffects[i] == None)
                        continue;

                    AttachEffect(HitEffects[i], HitFX[SimHitFxTicker].bone, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir);
                }
            }
        }

        if (class'GameInfo'.static.UseLowGore())
        {
            HitFX[SimHitFxTicker].bSever = false;

            switch(HitFX[SimHitFxTicker].bone)
            {
                case 'head':
                    if(!bHeadGibbed)
                    {
                        if (HitFX[SimHitFxTicker].damtype == class'DamTypeDecapitation')
                        {
                            DecapFX(boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false);
                        }
                        else if(HitFX[SimHitFxTicker].damtype == class'DamTypeProjectileDecap')
                        {
                            DecapFX(boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false, true);
                        }
                        else if(HitFX[SimHitFxTicker].damtype == class'DamTypeMeleeDecapitation')
                        {
                            DecapFX(boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, true);
                        }

                        bHeadGibbed=true;
                    }
                    break;
            }
        }

        if(HitFX[SimHitFxTicker].bSever)
        {
            GibPerterbation = HitFX[SimHitFxTicker].damtype.default.GibPerterbation;

            switch(HitFX[SimHitFxTicker].bone)
            {
                case 'obliterate':
                    break;

                case LeftThighBone:

                    if (!bLeftLegGibbed)
                    {
                        if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
                            SpawnSeveredGiblet(BurntDetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                        else
                            SpawnSeveredGiblet(DetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                        KFSpawnGiblet(class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                        KFSpawnGiblet(class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                        KFSpawnGiblet(class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                        bLeftLegGibbed=true;
                    }
                    break;

                case RightThighBone:
                    if (!bRightLegGibbed)
                    {
                        if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
                            SpawnSeveredGiblet(BurntDetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                        else
                            SpawnSeveredGiblet(DetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                        KFSpawnGiblet(class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                        KFSpawnGiblet(class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                        KFSpawnGiblet(class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                        bRightLegGibbed=true;
                    }
                    break;

                case LeftFArmBone:
                    if (!bLeftArmGibbed)
                    {
                        if (bCrispified && BurntDetachedArmClass != Class'KFChar.SeveredArmClot')
                            SpawnSeveredGiblet(BurntDetachedArmClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                        else
                            SpawnSeveredGiblet(DetachedArmClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                        KFSpawnGiblet(class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                        KFSpawnGiblet(class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                        bLeftArmGibbed=true;
                    }
                    break;

                case RightFArmBone:
                    if(!bRightArmGibbed)
                    {
                        if (bCrispified && BurntDetachedArmClass != Class'KFChar.SeveredArmClot')
                            SpawnSeveredGiblet(BurntDetachedArmClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                        else
                            SpawnSeveredGiblet(DetachedArmClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                        KFSpawnGiblet(class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                        KFSpawnGiblet(class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                        bRightArmGibbed=true;
                    }
                    break;

                case 'head':
                    if(!bHeadGibbed)
                    {
                        if (HitFX[SimHitFxTicker].damtype == class'DamTypeDecapitation')
                        {
                            DecapFX(boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false);
                        }
                        else if(HitFX[SimHitFxTicker].damtype == class'DamTypeProjectileDecap')
                        {
                            DecapFX(boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false, true);
                        }
                        else if(HitFX[SimHitFxTicker].damtype == class'DamTypeMeleeDecapitation')
                        {
                            DecapFX(boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, true);
                        }

                        bHeadGibbed=true;
                    }
                    break;
            }


            if(HitFX[SimHitFXTicker].bone != 'Spine' && HitFX[SimHitFXTicker].bone != FireRootBone &&
                    HitFX[SimHitFXTicker].bone != 'head' && Health <=0)
                HideBone(HitFX[SimHitFxTicker].bone);
        }
    }
}

// Added burnt limbs effect
simulated function SpawnGibs(Rotator HitRotation, float ChunkPerterbation)
{
    bGibbed      = true;

    PlayDyingSound();

    if (class'GameInfo'.static.UseLowGore())
        return;

    if (FlamingFXs != none)
    {
        FlamingFXs.Emitters[0].SkeletalMeshActor = none;
        FlamingFXs.Destroy();
    }

    if (ObliteratedEffectClass != none)
        Spawn(ObliteratedEffectClass,,, Location, HitRotation);

    //super.SpawnGibs(HitRotation,ChunkPerterbation);

    if (FRand() < 0.1)
    {
        KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
        KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
        KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
        KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
        KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);


        if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
        {
            SpawnSeveredGiblet(BurntDetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
            SpawnSeveredGiblet(BurntDetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
        }

        else
        {
            SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
            SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
        }


        if (bCrispified && BurntDetachedArmClass != Class'KFChar.SeveredArmClot')
            SpawnSeveredGiblet(BurntDetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);

        else
            SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);

        if (DetachedSpecialArmClass != None)
        {
            if (bCrispified && BurntDetachedSpecialArmClass != Class'KFChar.SeveredArmClot')
                SpawnSeveredGiblet(BurntDetachedSpecialArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);

            else
                SpawnSeveredGiblet(DetachedSpecialArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
        }

        else
        {
            if (bCrispified && BurntDetachedArmClass != Class'KFChar.SeveredArmClot')
                SpawnSeveredGiblet(BurntDetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);

            else
                SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
        }
    }

    else if (FRand() < 0.25)
    {
        KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
        KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
        KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
        KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);

        if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
        {
            SpawnSeveredGiblet(BurntDetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
            SpawnSeveredGiblet(BurntDetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
        }

        else
        {
            SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
            SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
        }

        if (FRand() < 0.5)
        {
            KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);

            if (bCrispified && BurntDetachedArmClass != Class'KFChar.SeveredArmClot')
                SpawnSeveredGiblet(BurntDetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);

            else
                SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
        }
    }
    else if (FRand() < 0.35)
    {
        KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
        KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);

        if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
            SpawnSeveredGiblet(BurntDetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);

        else
            SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
    }

    else if (FRand() < 0.5)
    {
        KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
        KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);

        if (bCrispified && BurntDetachedArmClass != Class'KFChar.SeveredArmClot')
            SpawnSeveredGiblet(BurntDetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);

        else
            SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
    }
}

// don't regen health while feeding - this was removed on the controller so theoretically this is useless but still it overrides some broken code... let's leave it here.
function CorpseAttack(Actor A)
{
    if (bShotAnim || Physics == PHYS_Swimming)
        return;

    Velocity.X   = 0;
    Velocity.Y   = 0;
    Acceleration = vect(0,0,0);
    bShotAnim    = true;
    SetAnimAction('ZombieFeed');
}

// this doesn't take into account missiles and it's only used for heavy zeds
function bool fSlayMe()
{
    if (Level != none && Level.TimeSeconds - fLastClawDamageTime >= fSlayMeTime)
        return True;

    return false;
}


defaultproperties
{
    fDamageMultiplier=1.0
    fFreezingMultiplier=1.0
    fKaboomMultiplier=1.0
    fSFireMultiplier=1.0
    fWFireMultiplier=1.0
    fShouldSetHitLoc=True
    fHSKThreshold=0.6
    fBurnDamage=0.75
    fAcidDamage=0.60
    fDeathDrama=0.05
    fHeadScale=1.0
    fNoHeadScale=1.0
    fMeleeHeadScale=1.25
    fWTCheck=1.0
    fShotgunClearListTime=0.4
    fShockTime=2.0
    fCurShockTime=2.0
    fStunThresh=500
    fUsedThresh=500
    fStunMult=0.8
    fBurningSpeed=0.8
    fHeadlessSpeed=0.8
    ZappedSpeedMod=0.25   // was 0.20 in patch 13
    ZapDuration=4.0       // default for light zeds
    fZapResetTime=5.0
    fMaxBurnDown=7
    fFrozenAnimFixTime=0.1
    FeedThreshold=1.0
    fFreezeCheck=0.2
    fFreezeDelay=0.2
    ShatteredIce=Class'IceChunkEmitter'
    FrozenDamageMult=1.25
    HiddenGroundSpeed=300
    fMovementScaleBB=1.4
    fMovementScaleHOE=1.3
    fMovementScaleSucidal=1.2
    fMovementScaleHard=1.1
    fMovementScaleNormal=1.0
    fRageSpeedCap=310.0
    fRageSpeed=1.0
    fNormalRageSpeed=1.0
    fHardRageSpeed=1.0
    fSuicidalRageSpeed=1.0
    fHOERageSpeed=1.0
    fBBRageSpeed=1.0
    fFlareRevolverHitCD=0.1
    fFlinchTime=0.8
    fStairsTimeFix=2.0
    fStairsCheckTime=0.5
    fFlinchCooldown=3.0
    fUsedHardMult=1.25          // zed stun threshold mult for hard
    fUsedSuiMult=1.5            // zed stun threshold mult for suicidal
    fUsedHoeMult=1.75           // zed stun threshold mult for hell on earth
    fUsedBBMult=2.0             // zed stun threshold mult for bloodbath
    fHeadRemoveDelay=0.1
    fHeadRemoveRetry=5
    BurntDetachedArmClass=Class'KFChar.SeveredArmClot'
    BurntDetachedLegClass=Class'KFChar.SeveredLegClot'
    BurntDetachedSpecialArmClass=Class'KFChar.SeveredArmClot'
    BurntDetachedHeadClass=Class'KFChar.SeveredHeadClot'
    fSlayMeTime=130
    fDoorDamageStep=0
    fAddDoorDamageMulti=0.25
    fDoorDamageResetDelay=10.0
}
