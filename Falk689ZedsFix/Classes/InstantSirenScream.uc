class InstantSirenScream extends PlasmaNadeFalk;

// Don't load anything, just stay put
static function PreloadAssets()
{
   return;
}

// Do nothing here
function PostBeginPlay()
{
}

// Go Kaboom
function PostNetBeginPlay()
{
   SetTimer(ExplodeTimer, false);
   fTimerSet = true;
}

simulated function Explode(vector HitLocation, vector HitNormal)
{
   //local PlayerController  LocalPlayer;

   bHasExploded = True;
   BlowUp(HitLocation);

   Spawn(Class'SirenScream',,, HitLocation, rotator(vect(0,0,1)));
   Destroy();
}

simulated function HurtRadius(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation)
{
}

simulated function HitWall(vector HitNormal, actor Wall)
{
}


defaultproperties
{
   ExplodeTimer=0.010000
   DrawScale=0.000000
   DrawType=DT_None
   ZapAmount=0.000000
}
