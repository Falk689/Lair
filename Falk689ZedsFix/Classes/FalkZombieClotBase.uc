/* LIGHT ZED */

class FalkZombieClotBase extends FalkMonster
   abstract;

#exec OBJ LOAD FILE=LairClot_A.ukx
#exec OBJ LOAD FILE=KF_Freaks_Trip.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=KF_Specimens_Trip_T.utx

var     FHumanPawn  DisabledPawn;           // The pawn that has been disabled by this zombie's grapple
var     bool        bGrappling;             // This zombie is grappling someone
var     float       GrappleEndTime;         // When the current grapple should be over
var()   float       GrappleDuration;        // How long a grapple by this zombie should last

var     float       ClotGrabMessageDelay; // Amount of time between a player saying "I've been grabbed" message

replication
{
   reliable if(bNetDirty && Role == ROLE_Authority)
      bGrappling;
}

function BreakGrapple()
{
   if (DisabledPawn != none)
   {
      DisabledPawn.EnableMovement();
      DisabledPawn                   = none;
   }
}

defaultproperties
{
   GrappleDuration=1.500000
   ClotGrabMessageDelay=12.000000
   MeleeAnims(0)="ClotGrapple"
   MeleeAnims(1)="ClotGrappleTwo"
   MeleeAnims(2)="ClotGrappleThree"
   bHarpoonToBodyStuns=False
   bHarpoonToHeadStuns=False
   bCannibal=True
   damageForce=5000
   KFRagdollName="Clot_Trip"
   CrispUpThreshhold=9
   PuntAnim="ClotPunt"
   AdditionalWalkAnims(0)="ClotWalk2"
   Intelligence=BRAINS_Mammal
   bUseExtendedCollision=True
   ColOffset=(Z=48.000000)
   ColRadius=25.000000
   ColHeight=5.000000
   ExtCollAttachBoneName="Collision_Attach"
   SeveredArmAttachScale=0.800000
   SeveredLegAttachScale=0.800000
   SeveredHeadAttachScale=0.800000
   OnlineHeadshotOffset=(X=20.000000,Z=37.000000)
   OnlineHeadshotScale=1.30000
   MeleeRange=20.000000
   GroundSpeed=105.000000
   WaterSpeed=105.000000
   JumpZ=340.000000
   HeadHealth=25
   HealthMax=130
   Health=130
   MenuName="Clot"
   MovementAnims(0)="ClotWalk"
   WalkAnims(0)="ClotWalk"
   WalkAnims(1)="ClotWalk"
   WalkAnims(2)="ClotWalk"
   WalkAnims(3)="ClotWalk"
   DrawScale=1.100000
   PrePivot=(Z=5.000000)
   RotationRate=(Yaw=45000,Roll=0)
   fDeathDrama=0.05               // light zeds standard
   ScoringValue=10                // light zeds standard
   PlayerCountHealthScale=0.0     // light zeds standard
   PlayerNumHeadHealthScale=0.0   // light zeds standard
   BleedOutDuration=3.0           // light zeds standard
   MotionDetectorThreat=0.250000  // two light zeds trigger a pipe, four trigger an ATM
   fHSKThreshold=0.35             // instakill at 46 damage at 1man normal
   Mass=150.000000                // light zeds standard
   ZappedDamageMod=1.000000
   ZapResistanceScale=3.000000
   ZapThreshold=5.000000          // light zeds standard
   MeleeDamage=6
   fHeadScale=1.15
   fNoHeadScale=1.2
   fStunThresh=200                // light zeds standard
   bMeleeStunImmune=false
   bStunImmune=false
   fDoorDamageStep=1
}
