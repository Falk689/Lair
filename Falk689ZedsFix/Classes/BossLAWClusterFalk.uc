class BossLAWClusterFalk extends M79ClusterFalk;

// Overridden to prevent clusters from blowing up our nade
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{ 
}

defaultproperties
{
   Damage=27.000000
   ImpactDamage=0.000000
   MyDamageType=Class'DamTypeBossLAWProj'
}