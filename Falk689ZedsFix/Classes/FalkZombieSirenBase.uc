/* MEDIUM ZED */

class FalkZombieSirenBase extends FalkMonster
   abstract;

var () int ScreamRadius; // AOE for scream attack.

var () class <DamageType> ScreamDamageType;
var () int ScreamForce;

var(Shake)  rotator RotMag;                // how far to rot view (normal)
var(Shake)  float   RotRate;               // how fast to rot view (normal)
var(Shake)  vector  OffsetMag;             // max view offset vertically (normal)
var(Shake)  float   OffsetRate;            // how fast to offset view vertically (normal)
var(Shake)  float   ShakeTime;             // how long to shake for per scream (normal)
var(Shake)  float   ShakeFadeTime;         // how long after starting to shake to start fading out (normal)
var(Shake)  float   ShakeEffectScalar;     // Overall scale for shake/blur effect (normal)
var(Shake)  float   MinShakeEffectScale;   // The minimum that the shake effect drops off over distance (normal)
var(Shake)  float   ScreamBlurScale;       // How much motion blur to give from screams (normal)

var         rotator bbRotMag;              // how far to rot view (bloodbath)
var         float   bbRotRate;             // how fast to rot view (bloodbath)
var         vector  bbOffsetMag;           // max view offset vertically (bloodbath)
var         float   bbOffsetRate;          // how fast to offset view vertically (bloodbath)
var         float   bbShakeTime;           // how long to shake for per scream (bloodbath)
var         float   bbShakeFadeTime;       // how long after starting to shake to start fading out (bloodbath)
var         float   bbShakeEffectScalar;   // Overall scale for shake/blur effect (bloodbath)
var         float   bbMinShakeEffectScale; // The minimum that the shake effect drops off over distance (bloodbath)
var         float   bbScreamBlurScale;     // How much motion blur to give from screams (bloodbath)

var         rotator curRotMag;             // how far to rot view (current)
var         float   curRotRate;            // how fast to rot view (current)
var         vector  curOffsetMag;          // max view offset vertically (current)
var         float   curOffsetRate;         // how fast to offset view vertically (current)
var         float   curShakeTime;          // how long to shake for per scream (current)
var         float   curShakeFadeTime;      // how long after starting to shake to start fading out (current)
var         float   curShakeEffectScalar;  // Overall scale for shake/blur effect (current)
var         float   curMinShakeEffectScale;// The minimum that the shake effect drops off over distance (current)
var         float   curScreamBlurScale;    // How much motion blur to give from screams (current)


var float SirenStunCheck;
var float CurSirenStunCheck;
var float CurSirenSecStunCheck;


var bool bAboutToDie;
var float DeathTimer;

var bool   FIsScreaming;     // are we screaming?
var byte   fStunReset;       // used to set the speed back after a stun
var float  fSirenStunTime;   // when this siren was stunned
var float  fStunDuration;    // how many seconds the stuns lasts


var float             FScreamDoorDamage; // amount of damage dealt to doors by default
var class<Projectile> FScreamClass;      // what class to spawn for the scream effect
var float             FScreamEndTime;    // when the scream will hopefully stop
var float             FScreamSpawnTime;  // when we should spawn the scream effect
var float             FScreamDelay;      // delay between the start of the screaming animation and the effect
var int               FScreamDist;       // how distant from the face the scream is
var name              FHeadBoneName;     // head bone to spawn the scream around

replication
{
   reliable if(Role == ROLE_Authority)
      fSirenStunTime, curRotMag, curRotRate, curOffsetMag, curOffsetRate, curShakeTime, curShakeFadeTime, curShakeEffectScalar, curMinShakeEffectScale, curScreamBlurScale;
}

defaultproperties
{
   ScreamRadius=700
   ScreamDamageType=Class'KFMod.SirenScreamDamage'
   RotMag=(Pitch=150,Yaw=150,Roll=150)
   RotRate=500.000000
   OffsetMag=(Y=5.000000,Z=1.000000)
   OffsetRate=500.000000
   ShakeTime=1.000000
   ShakeFadeTime=0.25000
   ShakeEffectScalar=1.000000
   MinShakeEffectScale=0.600000
   ScreamBlurScale=0.85000
   curRotMag=(Pitch=150,Yaw=150,Roll=150)
   curRotRate=500.000000
   curOffsetMag=(Y=5.000000,Z=1.000000)
   curOffsetRate=500.000000
   curShakeTime=1.000000
   curShakeFadeTime=0.25000
   curShakeEffectScalar=1.000000
   curMinShakeEffectScale=0.600000
   curScreamBlurScale=0.85000
   bbRotMag=(Pitch=300,Yaw=300,Roll=300)
   bbRotRate=1000.000000
   bbOffsetMag=(Y=10.000000,Z=2.000000)
   bbOffsetRate=1000.000000
   bbShakeTime=2.000000
   bbShakeFadeTime=0.50000
   bbShakeEffectScalar=2.000000
   bbMinShakeEffectScale=1.200000
   bbScreamBlurScale=0.85000
   MeleeAnims(0)="Siren_Bite"
   MeleeAnims(1)="Siren_Bite2"
   MeleeAnims(2)="Siren_Bite"
   HitAnims(0)="HitReactionF"
   HitAnims(1)="HitReactionF"
   HitAnims(2)="HitReactionF"
   ZombieFlag=1
   MeleeDamage=13
   damageForce=5000
   KFRagdollName="Siren_Trip"
   ZombieDamType(0)=Class'KFMod.DamTypeSlashingAttack'
   ZombieDamType(1)=Class'KFMod.DamTypeSlashingAttack'
   ZombieDamType(2)=Class'KFMod.DamTypeSlashingAttack'
   ScreamDamage=8
   FScreamDoorDamage=2
   CrispUpThreshhold=7
   bCanDistanceAttackDoors=True
   bUseExtendedCollision=True
   ColOffset=(Z=48.000000)
   ColRadius=25.000000
   ColHeight=5.000000
   ExtCollAttachBoneName="Collision_Attach"
   SeveredLegAttachScale=0.700000
   OnlineHeadshotOffset=(X=6.000000,Z=41.000000)
   ChallengeSound(0)=SoundGroup'KF_EnemiesFinalSnd.siren.Siren_Challenge'
   ChallengeSound(1)=SoundGroup'KF_EnemiesFinalSnd.siren.Siren_Challenge'
   ChallengeSound(2)=SoundGroup'KF_EnemiesFinalSnd.siren.Siren_Challenge'
   ChallengeSound(3)=SoundGroup'KF_EnemiesFinalSnd.siren.Siren_Challenge'
   SoundGroupClass=Class'KFMod.KFFemaleZombieSounds'
   IdleHeavyAnim="Siren_Idle"
   IdleRifleAnim="Siren_Idle"
   MeleeRange=45.000000
   GroundSpeed=100.000000
   WaterSpeed=100.000000
   HeadHeight=1.000000
   HeadScale=1.000000
   MenuName="Siren"
   MovementAnims(0)="Siren_Walk"
   MovementAnims(1)="Siren_Walk"
   MovementAnims(2)="Siren_Walk"
   MovementAnims(3)="Siren_Walk"
   WalkAnims(0)="Siren_Walk"
   WalkAnims(1)="Siren_Walk"
   WalkAnims(2)="Siren_Walk"
   WalkAnims(3)="Siren_Walk"
   IdleCrouchAnim="Siren_Idle"
   IdleWeaponAnim="Siren_Idle"
   IdleRestAnim="Siren_Idle"
   DrawScale=1.050000
   PrePivot=(Z=3.000000)
   RotationRate=(Yaw=45000,Roll=0)
   Mass=150.000000                // light zeds standard, siren makes for an exception due to her slimness
   MotionDetectorThreat=0.500000  // a single medium zed triggers a pipe, two trigger an ATM
   ScreamForce=0
   Health=300
   HealthMax=300
   fHSKThreshold=0.55             // instakill at 165 damage at 1man normal
   ScoringValue=20                // medium zeds standard
   PlayerCountHealthScale=0.1     // medium zeds standard
   PlayerNumHeadHealthScale=0.1   // medium zeds standard
   fDeathDrama=0.06               // medium zeds standard
   HeadHealth=150
   BleedOutDuration=5.0           // medium zeds standard
   OnlineHeadshotScale=1.9        // to help hitting her head better, was 1.2 in vanilla
   ZappedDamageMod=1.000000
   ZapResistanceScale=3.000000
   ZapThreshold=10.000000         // medium zeds standard
   ZapDuration=3.5                // medium zeds standard
   fShotgunWeakness=True
   bHarpoonToBodyStuns=False
   bHarpoonToHeadStuns=False
   fStunThresh=400                // medium zeds standard
   bMeleeStunImmune=false
   bStunImmune=false
   FHeadBoneName="CHR_Head"
   FScreamDist=10
   FScreamDelay=1.6
   FScreamClass=class'InstantSirenScream'
   fShockTime=5.0
   fFleshpoundRageImmune=True
   fStunDuration=4.0
   fDoorDamageStep=5
}
