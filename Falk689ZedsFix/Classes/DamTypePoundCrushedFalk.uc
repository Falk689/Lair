class DamTypePoundCrushedFalk extends KFWeaponDamageType;

defaultproperties
{
     DeathString="%o was pounded by %k."
     FemaleSuicide="%o was pounded."
     MaleSuicide="%o was pounded."
     bAlwaysGibs=True
     bCheckForHeadShots=False
     bKUseOwnDeathVel=True
     bThrowRagdoll=True
     KDamageImpulse=7000.000000
     KDeathVel=350.000000
     KDeathUpKick=100.000000
     HumanObliterationThreshhold=500
}
