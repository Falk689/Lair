/* MEDIUM ZED */

class FalkZombieBrute extends FalkZombieBruteBase;

#exec OBJ LOAD FILE=LairBrute_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

var float BruteStunCheck;
var float CurBruteStunCheck;
var float CurBruteSecStunCheck;

replication
{
   reliable if(Role == ROLE_Authority)
      fClientSetfStunReset;
}

simulated function PostBeginPlay()
{
   local Falk689GameTypeBase Flk;

   fBruteZapFixLeft = 0; // don't raise the arm now

   Super.PostBeginPlay();

   if (Role < ROLE_Authority)
      return;

   // Difficulty Scaling
   if (Level.Game != none)
   {
      if (Level.Game.GameDifficulty < 3.0)       // normal
         CurRDThreshold = NrmRDThreshold;

      else if (Level.Game.GameDifficulty < 5.0) // hard
         CurRDThreshold = HrdRDThreshold;

      else if (Level.Game.GameDifficulty < 6.0) // suicidal
         CurRDThreshold = SuiRDThreshold;

      else if (Level.Game.GameDifficulty < 8.0) // hoe
         CurRDThreshold = HOERDThreshold;

      else                                     // bb
      {
         CurRDThreshold = BBRDThreshold;
         curRageAttack  = bbRageAttack;
      }

      Flk = Falk689GameTypeBase(Level.Game);

      // set debug ID
      if (Flk != none)
         fBruteID = Flk.FalkGetZedDebugID();
   }
}

simulated function PostNetBeginPlay()
{
   super.PostNetBeginPlay();
   EnableChannelNotify(1,1);
   EnableChannelNotify(2,1);
   AnimBlendParams(1, 1.0, 0.0,, SpineBone1);
}

function ServerRaiseBlock()
{
   if (bZapped || fFrozen)
      return;

   bServerBlock = true;
   SetAnimAction('BlockLoop');
}

function ServerLowerBlock()
{
   local name Sequence;
   local float Frame, Rate;

   bServerBlock = false;
   GetAnimParams(1, Sequence, Frame, Rate);

   if (Sequence == 'BlockLoop')
      AnimStopLooping(1);
}

simulated function PostNetReceive()
{
   local name Sequence;
   local float Frame, Rate;

   if (bClientCharge != bChargingPlayer && !bZapped)
   {
      bClientCharge = bChargingPlayer;

      if (bChargingPlayer)
      {
         MovementAnims[0] = ChargingAnim;
         MeleeAnims[0]    = curRageAttack;
         MeleeAnims[1]    = curRageAttack;
         MeleeAnims[2]    = curRageAttack;
      }

      else
      {
         MovementAnims[0] = default.MovementAnims[0];
         MeleeAnims[0]    = default.MeleeAnims[0];
         MeleeAnims[1]    = default.MeleeAnims[1];
         MeleeAnims[2]    = default.MeleeAnims[2];
      }
   }

   if (bClientBlock != bServerBlock && !bZapped)
   {
      bClientBlock = bServerBlock;

      if (bClientBlock)
         SetAnimAction('BlockLoop');

      else
      {
         GetAnimParams(1, Sequence, Frame, Rate);

         if (Sequence == 'BlockLoop')
            AnimStopLooping(1);
      }
   }
}

simulated function Tick(float DeltaTime)
{
   local name Sequence;
   local float Frame, Rate;

   super.Tick(DeltaTime);

   if (fBruteZapFixLeft > 0)
   {
      if (fBruteZapFixTick > 0)
         fBruteZapFixTick -= DeltaTime;

      else
      {
         fBruteZapFixTick = Default.fBruteZapFixTick;
         bClientBlock = False;
         PostNetReceive();
      }
      
   }

   // restart after a freeze pls
   if (fFreezeReset > 0 && !fIsStunned)
   {
      if (BruteFreezeCheck > 0)
         BruteFreezeCheck -= DeltaTime;

      else
      {
         BruteFreezeCheck = Default.BruteFreezeCheck;

         fFreezeReset--;

         GroundSpeed  = GetOriginalGroundSpeed();
         SetGroundSpeed(GetOriginalGroundSpeed());

         if (bBurnified)
            SetGroundSpeed(GroundSpeed * fBurningSpeed);
         
         if (bZapped)
            SetGroundSpeed(GroundSpeed * ZappedSpeedMod);
      }
   }

   // restart after a stun pls
   if (fStunReset > 0 && fStunReset <= 3 && !fFrozen)
   { 
      fIsStunned = False;

      if (CurBruteSecStunCheck < BruteStunCheck)
         CurBruteSecStunCheck += DeltaTime;
   
      else
      {
         CurBruteSecStunCheck = 0;
         fStunReset++;

         GroundSpeed  = GetOriginalGroundSpeed();
         SetGroundSpeed(GetOriginalGroundSpeed());

         if (bBurnified)
            SetGroundSpeed(GroundSpeed * fBurningSpeed);
         
         if (bZapped)
            SetGroundSpeed(GroundSpeed * ZappedSpeedMod);

         GetAnimParams(1, Sequence, Frame, Rate);

         if (Sequence != 'BlockLoop')
         {
            //warn(Sequence);
            SetAnimAction('BlockLoop');
         }
      }
   }


   if (Role == ROLE_Authority)
   {
      // delayed zap state, used when charging to properly set the animation... hopefully
      if (fDelayZap)
      {
         if (fZapDelay > 0)
            fZapDelay -= DeltaTime;

         else
         {
            fDelayZap = False;
            fZapDelay = Default.fZapDelay;
            SetZappedBehavior();
         }
      }

      if (fIsStunned)
      {
         if (fBruteStunTime + fStunDuration >= Level.TimeSeconds)
         {
            if (fStunAnimStop > 0)
            {
               fStunAnimStop -= 1;
               AnimEnd(0);
            }

            SetGroundSpeed(1);
            //warn("ZERO");
         }

         else
         {
            fIsStunned   = False;

            GroundSpeed  = GetOriginalGroundSpeed();
            SetGroundSpeed(GetOriginalGroundSpeed());

            if (fStunReset == 0)
            {
               fStunReset = 1;
               fClientSetfStunReset();
               //warn("StunReset Start");
            }
         }
      }

      if (!bChargingPlayer && GroundSpeed > GetOriginalGroundSpeed())
      {
         SetGroundSpeed(GetOriginalGroundSpeed());
      }

      if (fTempHealth == 0 && Health > 0)
         fTempHealth = Health;

      // falk rage mechanics, first part
      if (fCurRageTick < fRageTick)
         fCurRageTick += DeltaTime;

      else
      {
         fCurRageTick = 0;

         if (!bDecapitated && !bChargingPlayer && !fIsStunned && !bZapped && Health <= fTempHealth - CurRDThreshold)
         {
            //warn("Tick Start Charging");
            StartCharging();
         }

         fTempHealth = Health;
      }

      // Lock to target when attacking
      if (bShotAnim)
         if (LookTarget != none)
            Acceleration = AccelRate * Normal(LookTarget.Location - Location);

      if (Level.TimeSeconds > fAnimEndTime && !fIsStunned && !bZapped)
      {
         bShotAnim = false;

         if (fBruteAttackDelay > 0)
            fBruteAttackDelay -= DeltaTime;

         else
         {
            fBruteAttackDelay = Default.fBruteAttackDelay;
            FalkFire(); // FIRE here instead of wherever?
         }

         // Block according to rules
         if (!bShotAnim && Level.TimeSeconds > fAnimEndTime && !bServerBlock && Controller != none && Controller.Target != none)
         {
            ServerRaiseBlock();
         } 
      }
   }

   else
   {
      if (fIsStunned)
      {
         if (CurBruteStunCheck < BruteStunCheck)
            CurBruteStunCheck += DeltaTime;

         else
         {
            CurBruteStunCheck = 0;

            GetAnimParams(1, Sequence, Frame, Rate);

            if (Sequence != 'KnockDown')
            {
               fClientFlipOver();
            }

            
            //GetAnimParams(0, Sequence, Frame, Rate);

            //warn(Sequence@"-"@Level.TimeSeconds);
         }
      }

      else if (bZapped)
      {
         if (CurBruteStunCheck < BruteStunCheck)
            CurBruteStunCheck += DeltaTime;

         else
         {
            CurBruteStunCheck = 0;

            GetAnimParams(1, Sequence, Frame, Rate);

            if (Sequence == 'BlockLoop')
            {
               AnimStopLooping(1);
               AnimEnd(1);
               //AnimEnd(0);

               if (Physics == PHYS_Falling)
                  SetPhysics(PHYS_Walking);

               SetAnimAction(IdleRestAnim);

               bShotAnim       = true;
               SetAnimAction('KnockDown');
            }
         }
      }
   }
}

function bool IsHeadShot(vector Loc, vector Ray, float AdditionalScale)
{
   local float D;
   local float AddScale;
   local bool bIsBlocking;

   bBlockedHS = false;

   if (bServerBlock && !IsTweening(1))
   {
      bIsBlocking = true;
      AddScale = AdditionalScale + BlockAddScale;
   }

   else
      AddScale = AdditionalScale + 1.0;

   if (Super.IsHeadShot(Loc, Ray, AddScale))
   {
      if (bIsBlocking)
      {
         D = vector(Rotation) dot Ray;
         if (-D > 0.20)
         {
            bBlockedHS = true;
            return false;
         }

         else
            return true;
      }

      else
         return true;
   }

   else
      return false;
}

function TakeDamage(int Damage, Pawn InstigatedBy, Vector HitLocation, Vector Momentum, class<DamageType> DamType, optional int HitIndex)
{
    local bool bIsHeadShot;
    local Class<KFWeaponDamageType> KFWDT;

    KFWDT = Class<KFWeaponDamageType>(DamType);

    if (KFWDT != none && KFWDT.Default.bCheckForHeadShots)
    {
        bIsHeadShot = IsHeadShot(HitLocation, normal(Momentum), 1.0);

        if (!bIsHeadShot && bBlockedHS)
        {
            if (class<KFProjectileWeaponDamageType>(DamType) != none)
                PlaySound(class'MetalHitEmitter'.default.ImpactSounds[rand(3)],, 128);

            else if (class<DamTypeChainsaw>(DamType) != none)
                PlaySound(Sound'KF_ChainsawSnd.Chainsaw_Impact_Metal',, 128);

            else if (class<DamTypeMelee>(DamType) != none)
                PlaySound(Sound'KF_KnifeSnd.Knife_HitMetal',, 128);

            if (class<DamTypeBurned>(DamType) == none && class<DamTypeFlameThrower>(DamType) == none)
                Damage *= BlockDmgMul; // Greatly reduce damage as we only hit the metal plating

            else
                Damage *= BlockFireDmgMul; // Fire damage isn't reduced as much
        }
    }

    fCurRageTick = 0; // reset the timer so we keep building up rage every damage in two seconds
    Super.TakeDamage(Damage, instigatedBy, hitLocation, Momentum, DamType, HitIndex);

    // If criteria is met make him rage
    if (!bDecapitated && !bChargingPlayer && !bZapped && Health <= fTempHealth - CurRDThreshold)
    {
        //warn("TakeDamage Start Charging");

        fTempHealth = Health;
        StartCharging();

        if (InstigatedBy != None && Controller != none && !SameSpeciesAs(InstigatedBy))
        {
            if (Controller.Target != InstigatedBy)
                MonsterController(Controller).ChangeEnemy(InstigatedBy, Controller.CanSee(InstigatedBy));
        }
    }

    if (bDecapitated)
        Died(InstigatedBy.Controller, DamType, HitLocation);
}

// DEBUG
function Died(Controller Killer, class<DamageType> damageType, vector HitLocation)
{
   //warn("BRUTE "@fBruteID@"DIED"@Level.TimeSeconds);
   Super.Died(Killer, damageType, HitLocation);
}

function TakeFireDamage(int Damage, Pawn Instigator)
{
   Super.TakeFireDamage(Damage, Instigator);

   // Adjust movement speed if not charging
   if (!bChargingPlayer)
   {
      if (bBurnified)
         SetGroundSpeed(GetOriginalGroundSpeed() * fBurningSpeed);

      else
         SetGroundSpeed(GetOriginalGroundSpeed());
   }
}

function ClawDamageTarget()
{
   local KFHumanPawn HumanTarget;
   local Actor OldTarget;
   local name Sequence;
   local float Frame, Rate;
   local bool bHitSomeone;

   GetAnimParams(1, Sequence, Frame, Rate);

   fLastCanAttackTime = Level.TimeSeconds;

   if (fFrozen)
      return;

   if (fIsStunned)
   {
      bShotAnim       = false;
      bWaitForAnim    = false;
      AnimEnd(1);
      return;
   }

   if (!fFrozen && !bZapped && !fIsStunned && Controller != none && Controller.Target != none)
   {
      if (Sequence == 'BruteRageAttack' || Sequence == 'BruteRageAttackBloodbath')
      {
         OldTarget = Controller.Target;

         foreach VisibleCollidingActors(class'KFHumanPawn', HumanTarget, MeleeRange + class'KFHumanPawn'.default.CollisionRadius)
         {
            bHitSomeone = ClawDamageSingleTarget(MeleeDamage, HumanTarget);
         }

         Controller.Target = OldTarget;

         if (bHitSomeone)
            BlockHitsLanded++;
      }

      else if (Sequence != 'BruteAttack1' && Sequence != 'BruteAttack2' && Sequence != 'DoorBash') // Block attack
      {
         bHitSomeone = ClawDamageSingleTarget(FalkGetDamage(MeleeDamage), Controller.Target);
         if (bHitSomeone)
            BlockHitsLanded++;
      }

      else
         bHitSomeone = ClawDamageSingleTarget(FalkGetDamage(MeleeDamage), Controller.Target);

      if (bHitSomeone)
      {
         fLastClawDamageTime = Level.TimeSeconds;
         PlaySound(MeleeAttackHitSound, SLOT_Interact, 1.25);
      }
   }
}

function bool ClawDamageSingleTarget(float UsedMeleeDamage, Actor ThisTarget)
{
   local Pawn HumanTarget;
   local KFPlayerController HumanTargetController;
   local bool bHitSomeone;
   local float EnemyAngle;
   local Rotator RandRot;
   local Vector PushForceVar;

   if (fIsStunned || fFrozen)
      return False;

   EnemyAngle = Normal(ThisTarget.Location - Location) dot Vector(Rotation);

   if (EnemyAngle > 0)
   {
      Controller.Target = ThisTarget;
      if (MeleeDamageTarget(UsedMeleeDamage, vect(0, 0, 0)))
      {
         HumanTarget = KFHumanPawn(ThisTarget);
         if (HumanTarget != None)
         {
            RandRot.Yaw = Spread * (FRand() - 0.5);
            RandRot     = Rotator(Normal(HumanTarget.Location - Location) * vect(1, 1, 0) >> RandRot);

            EnemyAngle = (EnemyAngle * 0.5) + 0.5; // Players at sides get knocked back half as much
            PushForceVar = (PushForce * Normal(Normal(HumanTarget.Location - Location) + Vector(RandRot)) * EnemyAngle) + PushAdd;

            if (!bChargingPlayer)
               PushForceVar *= 0.85;

            if (!(HumanTarget.Physics == PHYS_WALKING || HumanTarget.Physics == PHYS_NONE))
               PushForceVar *= vect(1, 1, 0); // (!) Don't throw upwards if we are not on the ground - adjust for more flexibility

            HumanTarget.AddVelocity(PushForceVar);

            HumanTargetController = KFPlayerController(HumanTarget.Controller);
            if (HumanTargetController != None)
               HumanTargetController.ShakeView(ShakeViewRotMag, ShakeViewRotRate, ShakeViewRotTime, 
                     ShakeViewOffsetMag, ShakeViewOffsetRate, ShakeViewOffsetTime);

            bHitSomeone = true;
         }
      }
   }

   return bHitSomeone;
}

function StartCharging()
{
   if (Health <= 0 || /*fFrozen ||*/ fIsStunned || fFrozen || DecapitatedBy != none)
      return;

   // How many times should we hit before we cool down? 
   MaxRageCounter = 1;

   RageCounter = MaxRageCounter;
   PlaySound(RageSound, SLOT_Talk, 255);
   GotoState('RageCharging');
}

state RageCharging
{
   Ignores StartCharging;

   // Set the zed to the zapped behavior
   simulated function SetZappedBehavior()
   {
      GoToState('');
      //Global.SetZappedBehavior();

      fDelayZap        = True;
   }

   function PlayDirectionalHit(Vector HitLoc)
   {
      if (!bShotAnim)
         super(KFMonster).PlayDirectionalHit(HitLoc);
   }

   function bool CanGetOutOfWay()
   {
      return false;
   }

   function bool CanSpeedAdjust()
   {
      return false;
   }

   function BeginState()
   {
      if (bZapped /*|| fFrozen*/)
      {
         GoToState('');
      }

      bFrustrated = false;
      bChargingPlayer = true;
      RageSpeedTween = 0.0;

      if (Level.NetMode != NM_DedicatedServer)
         ClientChargingAnims();

      NetUpdateTime = Level.TimeSeconds - 1;
   }

   function EndState()
   {
      bChargingPlayer = false;

      FalkBruteController(Controller).RageFrustrationTimer = 0;

      if (Health > 0 && !bZapped)
      {
         GroundSpeed = GetOriginalGroundSpeed();

         if (bBurnified)
            SetGroundSpeed(GroundSpeed * fBurningSpeed);
         
         if (bZapped)
            SetGroundSpeed(GroundSpeed * ZappedSpeedMod);
      }

      if( Level.NetMode!=NM_DedicatedServer )
         ClientChargingAnims();

      NetUpdateTime = Level.TimeSeconds - 1;
   }

   function Tick(float Delta)
   {
      local float fSpeedCap;

      if (bZapped /*|| fFrozen*/ || bDecapitated || DecapitatedBy != none)
      {
         GoToState('');
      }

      if (!bShotAnim)
      {
         RageSpeedTween = FClamp(RageSpeedTween + (Delta * 0.75), 0, 1.0);
         fSpeedCap      = FMin(fRageSpeedCap, OriginalGroundSpeed * fRageSpeed); // first compute proper rage speed cap
         GroundSpeed    = FMin(fSpeedCap, OriginalGroundSpeed + ((OriginalGroundSpeed * 0.75 / MaxRageCounter * (RageCounter + 1) * RageSpeedTween))); // cap its speed at my var, but using the vanilla acceleration (on the right)

         if (bBurnified)
            SetGroundSpeed(GroundSpeed * fBurningSpeed);

         if (bZapped)
            SetGroundSpeed(GetOriginalGroundSpeed() * ZappedSpeedMod);
      }

      Global.Tick(Delta);
   }

   function Bump(Actor Other)
   {
      local KFMonster KFM;

      KFM = KFMonster(Other);

      // Hurt enemies that we run into while raging
      if (!bShotAnim && KFM != None && FalkZombieBrute(Other) == None && Pawn(Other).Health > 0)
         Other.TakeDamage(RageBumpDamage, self, Other.Location, Velocity * Other.Mass, class'DamTypePoundCrushed');
      else Global.Bump(Other);
   }

   function bool MeleeDamageTarget(int HitDamage, vector PushDir)
   {
      local bool DamDone, bWasEnemy;

      bWasEnemy = (Controller.Target == Controller.Enemy);

      DamDone = Super.MeleeDamageTarget(HitDamage * RageDamageMul, vect(0, 0, 0));

      if (bWasEnemy && DamDone)
      {
         ChangeTarget();
         CalmDown();
      }

      return DamDone;
   }

   function CalmDown()
   {
      RageCounter = FClamp(RageCounter - 1, 0, MaxRageCounter);
      if (RageCounter == 0)
         GotoState('');
   }

   function ChangeTarget()
   {
      local Controller C;
      local Pawn BestPawn;
      local float Dist, BestDist;

      for (C = Level.ControllerList; C != none; C = C.NextController)
         if (C.Pawn != none && KFHumanPawn(C.Pawn) != none)
         {
            Dist = VSize(C.Pawn.Location - Location);
            if (C.Pawn == Controller.Target)
               Dist += GroundSpeed * 4;

            if (BestPawn == none)
            {
               BestPawn = C.Pawn;
               BestDist = Dist;
            }
            else if (Dist < BestDist)
            {
               BestPawn = C.Pawn;
               BestDist = Dist;
            }
         }

      if (BestPawn != none && BestPawn != Controller.Enemy)
         MonsterController(Controller).ChangeEnemy(BestPawn, Controller.CanSee(BestPawn));
   }
}

// Shouldn't fight with anything but humans
function bool SameSpeciesAs(Pawn P)
{
   return (KFHumanPawn(P) == none);
}

function PlayTakeHit(vector HitLocation, int Damage, class<DamageType> DamageType)
{
   if (fFrozen)
      return;

   if (Level.TimeSeconds - LastPainAnim < MinTimeBetweenPainAnims)
      return;

   // Uncomment this if we want some damage to make him drop his block
   /*if( !Controller.IsInState('WaitForAnim') && Damage >= 10 )
     PlayDirectionalHit(HitLocation);*/

   LastPainAnim = Level.TimeSeconds;

   if (Level.TimeSeconds - LastPainSound < MinTimeBetweenPainSounds)
      return;

   LastPainSound = Level.TimeSeconds;
   PlaySound(HitSound[0], SLOT_Pain,1.25,,400);
}

// Overridden to handle playing upper body only attacks when moving
simulated event SetAnimAction(name NewAction)
{
   if (NewAction=='' || fFrozen)
      return;

   if (NewAction == 'Claw')
   {
      NewAction = MeleeAnims[rand(2)];
      CurrentDamType = ZombieDamType[0];
   }

   else if (NewAction == 'BlockClaw')
   {
      NewAction = 'BruteBlockSlam';
      CurrentDamType = ZombieDamType[0];
   }

   else if (NewAction == 'AoeClaw')
   {
      NewAction = curRageAttack;
      CurrentDamType = ZombieDamType[0];
   }

   else if (NewAction == 'DoorBash')
      CurrentDamType = ZombieDamType[Rand(3)];

   ExpectingChannel = DoAnimAction(NewAction);

   if (AnimNeedsWait(NewAction))
      bWaitForAnim = true;
   else
      bWaitForAnim = false;

   if (Level.NetMode != NM_Client)
   {
      AnimAction = NewAction;
      bResetAnimAct = True;
      ResetAnimActTime = Level.TimeSeconds+0.3;
   }
}

simulated function int DoAnimAction(name AnimName)
{
   if (fFrozen)
      return 0;

   if (AnimName == 'KnockDown')
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;

      if (Role == ROLE_Authority)
         bServerBlock = false;

      PlayAnim(AnimName,, 0.1, 1);
      return 1;
   }

   else if (AnimName=='BruteAttack1' || AnimName=='BruteAttack2' || AnimName=='ZombieFireGun' || AnimName == 'DoorBash' /*|| AnimName == 'KnockDown'*/)
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;

      if (Role == ROLE_Authority)
         ServerLowerBlock();

      AnimBlendParams(1, 1.0, 0.0,, FireRootBone);
      PlayAnim(AnimName,, 0.1, 1);

      return 1;
   }
   else if (AnimName == 'BruteRageAttack')
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;

      if (Role == ROLE_Authority && Level.Game.GameDifficulty < 8.0)
         ServerLowerBlock();

      AnimBlendParams(1, 1.0, 0.0,, FireRootBone);
      PlayAnim(AnimName,, 0.1, 1);

      return 1;
   }
   else if (AnimName == 'BlockLoop')
   {
      if (!bZapped)
      {
         AnimBlendParams(1, 1.0, 0.0,, FireRootBone);
         LoopAnim(AnimName,, 0.25, 1);
      }

      return 1;
   }

   else if (AnimName == 'BruteBlockSlam' || AnimName == 'BruteRageAttackBloodbath')
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;
      AnimBlendParams(2, 1.0, 0.0,, FireRootBone);
      PlayAnim(AnimName,, 0.1, 2);

      return 2;
   }

   return Super.DoAnimAction(AnimName);
}

// The animation is full body and should set the bWaitForAnim flag
simulated function bool AnimNeedsWait(name TestAnim)
{
   if (TestAnim == 'KnockDown' || TestAnim == 'DoorBash')
      return true;

   return false;
}

simulated function AnimEnd(int Channel)
{
   local name Sequence;
   local float Frame, Rate;

   GetAnimParams(Channel, Sequence, Frame, Rate);

   // Don't allow notification for a looping animation
   if (Sequence == 'BlockLoop')
      return;

   // Disable channel 2 when we're done with it
   if (Channel == 2 && (Sequence == 'BruteBlockSlam' || Sequence == 'BruteRageAttackBloodbath'))
   {
      AnimBlendParams(2, 0);
      bShotAnim = false;
      return;
   }

   if (fStunReset == 0 && bShotAnim && Channel==ExpectingChannel)
   {
      //warn("Stun anim end");
      fIsStunned = False;
      fStunReset = 1;
      fClientSetfStunReset();
   }

   Super.AnimEnd(Channel);
}

simulated function fClientSetfStunReset()
{
   fIsStunned = False;
   fStunReset = 1;
   //warn("StunReset Start Client"); 
}

simulated function ClientChargingAnims()
{
   PostNetReceive();
}

// lower the arm on stun
function bool FlipOver()
{
   if (!fAlreadyStunned && !fFrozen && !bShotAnim)
   {
      fFocalPoint      = Location + 512 * vector(Rotation);

      if (Controller != none)
      {
         //warn("CONTROLLER STORE:"@Level.TimeSeconds);
         fStoredStunEnemy = Controller.Enemy;
         fStoredStunFocus = Controller.Focus;

         Controller.Enemy = none;
         Controller.Focus = none;
      }

      GotoState('');
      bChargingPlayer = False;
      fBruteStunTime  = Level.TimeSeconds;
      ServerLowerBlock();
      fAlreadyStunned = True;
      bShotAnim       = false;
      bWaitForAnim    = false;
      AnimStopLooping(1);
      AnimEnd(2);
      AnimEnd(1);
      AnimEnd(0);
      SetAnimAction(IdleRestAnim);
      fIsStunned      = True;
      fClientFlipOver();
      PostNetReceive();
      return Super(KFMonster).FlipOver();
   }

   return False;
}


// pls stop walking while stunned
simulated function fClientFlipOver()
{
   bShotAnim        = false;
   bWaitForAnim     = false;
   AnimEnd(2);
   AnimEnd(1);
   AnimEnd(0);

   SetAnimAction(IdleRestAnim);

   fIsStunned       = True;
	bShotAnim        = true;
	SetAnimAction('KnockDown');
	Acceleration     = vect(0, 0, 0);
	Velocity.X       = 0;
	Velocity.Y       = 0;

   if (Controller != None)
   {
	   Controller.GoToState('WaitForAnim');
	   KFMonsterController(Controller).bUseFreezeHack = True;
   }
}

// lower arm on zap
simulated function SetZappedBehavior()
{
   if (fFrozen)
      return;

   fBruteZapFixLeft = 0;
   fBruteZapFixTick = Default.fBruteZapFixTick;

   GotoState('');
   bChargingPlayer = false;

   ServerLowerBlock();

   AnimStopLooping(1);
   AnimEnd(1);
   AnimEnd(0);

	if (Physics == PHYS_Falling)
		SetPhysics(PHYS_Walking);

	bShotAnim       = true;
	SetAnimAction('KnockDown');

   Super.SetZappedBehavior();
}

// trying to fix slowrage on zap, second part
simulated function UnSetZappedBehavior()
{
   bZapped      = False;
   bClientBlock = False;
   Super.UnSetZappedBehavior();
   PostNetReceive();
   fBruteZapFixLeft = Default.fBruteZapFixLeft;
}

// hopefully prevent weird bugs while defending from an headshot
function PlayHit(float Damage, Pawn InstigatedBy, vector HitLocation, class<DamageType> damageType, vector Momentum, optional int HitIdx)
{
   local Actor A;

   if (fFrozen)
      return;

   if (bBlockedHS)
      A = Spawn(class'BlockHitEmitter', InstigatedBy,, HitLocation, rotator(Normal(HitLocation - Location)));

   else
      Super.PlayHit(Damage, InstigatedBy, HitLocation, damageType, Momentum, HitIdx);
}

// superclass function is broken on the brute, just return default
simulated function float GetOriginalGroundSpeed()
{
	return Default.GroundSpeed;
}

// return zed class
function FalkZedClass FalkGetZedClass()
{
   return Falk_Brute;
}

// stop charging on freeze, also stop trying to unfreeze
function fFreezeFix()
{
   fFreezeReset     = 0;
   fBruteZapFixLeft = 0;
   fBruteZapFixTick = Default.fBruteZapFixTick;

   BruteFreezeCheck = Default.BruteFreezeCheck;

   GotoState('');
   bChargingPlayer = false;

   Super.fFreezeFix();
}

// speed restart on unfreeze
function fUnfreezeFix()
{
   fFreezeReset = 3;
   Super.fUnfreezeFix();
}

// override the flinch fix since you don't flinch anyway and I don't want to introduce bugs is possible
function PlayDirectionalHit(Vector HitLoc)
{
   super(KFMonster).PlayDirectionalHit(HitLoc);
}

// kinda like don't do this
function RangedAttack(Actor A)
{
}

// RangedAttack is sometimes deactivated without any reason, FalkRangedAttack always happens
function FalkRangedAttack(Actor A)
{
   if (fFrozen || fIsStunned || Level.TimeSeconds <= fAnimEndTime)
   {
      //warn("BRUTE"@fBruteID@"RETURN 1"@Level.TimeSeconds);
      return;
   }

   if (Level.TimeSeconds > fAnimEndTime)
      bShotAnim = False;

   if (bShotAnim)
   {
      //warn("BRUTE"@fBruteID@"RETURN 2"@Level.TimeSeconds);
      return;
   }

   else if (CanAttack(A))
   {
      //warn("BRUTE "@fBruteID@" Ranged Attack Call Time:"@Level.TimeSeconds);

      if (bChargingPlayer)
         SetAnimAction('AoeClaw');
      else
      {
         if (Rand(BlockHitsLanded) < 1)
            SetAnimAction('BlockClaw');
         else
            SetAnimAction('Claw');
      }

      bShotAnim = true;
      return;
   }
}

// called instead of super.CanAttack to debug wtf is going on
function bool FalkCanAttack(Actor A)
{
   if (A == none)
   {
      //warn("BRUTE "@fBruteID@"CAN'T ATTACK 1"@Level.TimeSeconds);
      return false;
   }

   if (bSTUNNED)
   {
      //warn("BRUTE "@fBruteID@"CAN'T ATTACK 2"@Level.TimeSeconds);
      return false;
   }

   if (KFDoorMover(A)!=none)
   {
      //warn("BRUTE "@fBruteID@"DOOR"@Level.TimeSeconds);
      return true;
   }

   else if (KFHumanPawn(A) != none && KFHumanPawn(A).Health <= 0)
   {
      if (VSize(A.Location - Location) < MeleeRange + CollisionRadius)
      {
         //warn("BRUTE "@fBruteID@"CAN ATTACK 1"@Level.TimeSeconds);
         return true;
      }

      else
      {
         //warn("BRUTE "@fBruteID@"CAN'T ATTACK 3"@Level.TimeSeconds);
         return false;
      }
   }

   else if (VSize(A.Location - Location) < MeleeRange + CollisionRadius + A.CollisionRadius)
   {
      //warn("BRUTE "@fBruteID@"CAN ATTACK 2"@Level.TimeSeconds);
      return true;
   }

   //warn("BRUTE "@fBruteID@"CAN'T ATTACK 4"@Level.TimeSeconds);
   return false;
}

// first attempt to prevent fake attacks from decapitated or frozen zeds, implement shock time
function bool CanAttack(Actor A)
{
   if (fFrozen || bDecapitated && fCurShockTime < fShockTime)
   {
      //warn("BRUTE "@fBruteID@"CAN'T ATTACK 0"@Level.TimeSeconds);
      return False;
   }

   if (FalkCanAttack(A))
   {
      if (Role == ROLE_Authority)
      {
         //warn("BRUTE "@fBruteID@"ATTACK"@Level.TimeSeconds);
         fLastCanAttackTime = Level.TimeSeconds;
      }

      return True;
   }

   return False;
}

// don't do shit here
function Fire(optional float F)
{
}

// basically called on tick instead of who knows when
function FalkFire(optional float F)
{
   local Actor BestTarget;
   local float bestAim, bestDist;
   local vector FireDir, X,Y,Z;
   local FMonsterController FC;

   if (Controller == none)
      return;

   //warn("BRUTE "@fBruteID@" FALK FIRE"@Level.TimeSeconds);

   bestAim = 0.90;
   GetAxes(Controller.Rotation,X,Y,Z);
   FireDir = X;

   FC = FMonsterController(Controller);

   if (FC != none)
      BestTarget = FC.FalkPickTarget(bestAim, bestDist, FireDir, GetFireStart(X,Y,Z), 6000, fBruteID);

   else
      BestTarget = Controller.PickTarget(bestAim, bestDist, FireDir, GetFireStart(X,Y,Z), 6000);

   if (KFHumanPawn(BestTarget) != none || FalkMonster(BestTarget) != none)
      FalkRangedAttack(BestTarget);
}

// ignore FalkRangedAttack too
State ZombieDying
{
   ignores AnimEnd, Trigger, Bump, HitWall, HeadVolumeChange, PhysicsVolumeChange, Falling, BreathTimer, Died, RangedAttack, FalkRangedAttack;
}

defaultproperties
{
   BruteStunCheck=0.1
   ControllerClass=Class'FalkBruteController'
}
