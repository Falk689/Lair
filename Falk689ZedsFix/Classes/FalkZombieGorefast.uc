/* LIGHT ZED */

class FalkZombieGorefast extends FalkZombieGorefastBase
   abstract;

#exec OBJ LOAD FILE=LairGorefast_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

// speed scaling based on difficulty
simulated function PostBeginPlay()
{
   Super.PostBeginPlay();

   if (Role < ROLE_Authority)
      return;

   /*if (Level.Game != none)
   {
      if(Level.Game.GameDifficulty < 3.0) 
          fRageSpeed = fNormalRageSpeed;

      else if(Level.Game.GameDifficulty < 5.0)
          fRageSpeed = fHardRageSpeed;

      else if(Level.Game.GameDifficulty < 6.0)
          fRageSpeed = fSuicidalRageSpeed;

      else if(Level.Game.GameDifficulty < 8.0)
          fRageSpeed = fHOERageSpeed;

      else
          fRageSpeed = fBBRageSpeed;
   }*/
}

// don't interrupt the running attack
simulated function bool HitCanInterruptAction()
{
   if (RunAttackTimeout > 0)
      return false;

   return !bShotAnim;
}

simulated function PostNetReceive()
{
   if (!bZapped)
   {
      if (bRunning)
         MovementAnims[0]='ZombieRun';
      else MovementAnims[0]=default.MovementAnims[0];
   }
}

// This zed has been taken control of, not sure what this does but nah, don't boost anything
function SetMindControlled(bool bNewMindControlled)
{
   if( bNewMindControlled )
   {
      NumZCDHits++;

      // if we hit him a couple of times, make him rage!
      if( NumZCDHits > 1 )
      {
         if( !IsInState('RunningToMarker') )
         {
            GotoState('RunningToMarker');
         }
         else
         {
            NumZCDHits = 1;
            if( IsInState('RunningToMarker') )
            {
               GotoState('');
            }
         }
      }
      else
      {
         if( IsInState('RunningToMarker') )
         {
            GotoState('');
         }
      }
   }
   else
   {
      NumZCDHits=0;
   }

   bZedUnderControl = bNewMindControlled;
}

// Handle the zed being commanded to move to a new location
function GivenNewMarker()
{
   if( bRunning && NumZCDHits > 1 )
   {
      GotoState('RunningToMarker');
   }
   else
   {
      GotoState('');
   }
}

function RangedAttack(Actor A)
{
   Super.RangedAttack(A);

   if (!bRunning && !bShotAnim && !bDecapitated && VSize(A.Location-Location) <= 700)
      GoToState('RunningState');
}

state RunningState
{
   // Set the zed to the zapped behavior
   simulated function SetZappedBehavior()
   {
      Global.SetZappedBehavior();
      GoToState('');
   }

   // Set the zed to the on fire behavior, tweaking the speed in a controlled way
   simulated function SetBurningBehavior()
   {
      if (Role == Role_Authority)
      {
         Intelligence = BRAINS_Retarded;

         SetGroundSpeed(OriginalGroundSpeed * fRageSpeed * fBurningSpeed);
         AirSpeed   = OriginalGroundSpeed * fRageSpeed * fBurningSpeed;
         WaterSpeed = OriginalGroundSpeed * fRageSpeed * fBurningSpeed;

         // Make them less accurate while they are burning
         if (Controller != none)
            MonsterController(Controller).Accuracy = -5;  // More chance of missing. (he's burning now, after all) B======D
      }

      // Set the forward movement anim to a random burning anim
      MovementAnims[0] = BurningWalkFAnims[Rand(3)];
      WalkAnims[0]     = BurningWalkFAnims[Rand(3)];

      // Set the rest of the movement anims to the headless anim (not sure if these ever even get played) - Ramm
      MovementAnims[1] = BurningWalkAnims[0];
      WalkAnims[1]     = BurningWalkAnims[0];
      MovementAnims[2] = BurningWalkAnims[1];
      WalkAnims[2]     = BurningWalkAnims[1];
      MovementAnims[3] = BurningWalkAnims[2];
      WalkAnims[3]     = BurningWalkAnims[2];
   }

   // Don't override speed in this state
   function bool CanSpeedAdjust()
   {
      return false;
   }

   function BeginState()
   {
      if( bZapped )
      {
         GoToState('');
      }
      else
      {
         SetGroundSpeed(OriginalGroundSpeed * fRageSpeed);
         bRunning = true;
         if( Level.NetMode!=NM_DedicatedServer )
            PostNetReceive();

         NetUpdateTime = Level.TimeSeconds - 1;
      }
   }

   function EndState()
   {
      bRunning = False;

      if (bZapped)
      {
         GroundSpeed = OriginalGroundSpeed * ZappedSpeedMod;
         SetGroundSpeed(OriginalGroundSpeed * ZappedSpeedMod);
      }

      else
      {
         GroundSpeed = OriginalGroundSpeed;
         SetGroundSpeed(OriginalGroundSpeed);
      }

      if (Level.NetMode != NM_DedicatedServer)
         PostNetReceive();

      RunAttackTimeout = 0;
      NetUpdateTime    = Level.TimeSeconds - 1;
   }

   function RemoveHead()
   {
      GoToState('');
      Global.RemoveHead();
   }

   function RangedAttack(Actor A)
   {
      local float ChargeChance;

      // Decide what chance the gorefast has of attacking while running
      if (Level.Game.GameDifficulty < 3.0)
         ChargeChance = 0.2;

      else if (Level.Game.GameDifficulty < 5.0)
         ChargeChance = 0.3;

      else if (Level.Game.GameDifficulty < 6.0)
         ChargeChance = 0.4;

      else if (Level.Game.GameDifficulty < 8.0)
         ChargeChance = 0.5;

      else // Hardest difficulty
         ChargeChance = 1.0;

      if (bShotAnim || Physics == PHYS_Swimming)
         return;

      else if (CanAttack(A))
      {
         bShotAnim = true;

         // Randomly do a moving attack so the player can't kite the zed
         if(ChargeChance >= 1.0 || FRand() < ChargeChance)
         {
            SetAnimAction('ClawAndMove');
            RunAttackTimeout = GetAnimDuration('GoreAttack1', 1.0) + 0.03;
         }

         else
         {
            SetAnimAction('Claw');
            Controller.bPreparingMove = true;
            Acceleration = vect(0,0,0);
            // Once we attack stop running
            GoToState('');
         }

         return;
      }
   }

   simulated function Tick(float DeltaTime)
   {

      if (Controller != None)
      {
         if (Controller.Target == None || (Pawn(Controller.Target) != none && Pawn(Controller.Target).Health <= 0))
         {
            GroundSpeed = OriginalGroundSpeed;
            GoToState('');
         }
      }

      // stop running if headless or zapped
      if (bZapped || bDecapitated || DecapitatedBy != none)
      {
         GroundSpeed = OriginalGroundSpeed;
         GoToState('');
      }

      // Keep moving toward the target until the timer runs out (anim finishes)
      if( RunAttackTimeout > 0 )
      {
         RunAttackTimeout -= DeltaTime;

         if( RunAttackTimeout <= 0 && !bZedUnderControl )
         {
            RunAttackTimeout = 0;
            GoToState('');
         }
      }

      // Keep the gorefast moving toward its target when attacking
      if (Role == ROLE_Authority && bShotAnim && !bWaitForAnim)
      {
         if (LookTarget != None)
            Acceleration = AccelRate * Normal(LookTarget.Location - Location);

         else
         {
            //warn("NO TARGET 1"@Level.Timeseconds);
            GoToState('');
         }
      }

      global.Tick(DeltaTime);
   }


Begin:
   While(True)
   {
      Sleep(0.5);
   }
}

// State where the zed is charging to a marked location.
state RunningToMarker extends RunningState
{
   simulated function Tick(float DeltaTime)
   {
      // Keep moving toward the target until the timer runs out (anim finishes)
      if( RunAttackTimeout > 0 )
      {
         RunAttackTimeout -= DeltaTime;

         if( RunAttackTimeout <= 0 && !bZedUnderControl )
         {
            RunAttackTimeout = 0;
            GoToState('');
         }
      }

      // Keep the gorefast moving toward its target when attacking
      if( Role == ROLE_Authority && bShotAnim && !bWaitForAnim )
      {
         if( LookTarget!=None )
         {
            Acceleration = AccelRate * Normal(LookTarget.Location - Location);
         }

         else
         {
            //warn("NO TARGET 2"@Level.Timeseconds);
            GoToState('');
         }
      }

      global.Tick(DeltaTime);
   }


Begin:
   While(True)
   {
      Sleep(0.5);
   }

/*Begin:
   GoTo('CheckCharge');
CheckCharge:
   if( bZedUnderControl || (Controller!=None && Controller.Target!=None && VSize(Controller.Target.Location-Location)<700) )
   {
      Sleep(0.5+ FRand() * 0.5);
      GoTo('CheckCharge');
   }
   else
   {
      GoToState('');
   }*/
}

// Overridden to handle playing upper body only attacks when moving
simulated event SetAnimAction(name NewAction)
{
   local int meleeAnimIndex;
   local bool bWantsToAttackAndMove;

   if (NewAction=='' || fFrozen)
      return;

   bWantsToAttackAndMove = NewAction == 'ClawAndMove';

   if(NewAction == 'Claw')
   {
      meleeAnimIndex = Rand(3);
      NewAction = meleeAnims[meleeAnimIndex];
      CurrentDamtype = ZombieDamType[meleeAnimIndex];
   }

   if( bWantsToAttackAndMove )
   {
      ExpectingChannel = AttackAndMoveDoAnimAction(NewAction);
   }
   else
   {
      ExpectingChannel = DoAnimAction(NewAction);
   }

   if( !bWantsToAttackAndMove && AnimNeedsWait(NewAction) )
   {
      bWaitForAnim = true;
   }
   else
   {
      bWaitForAnim = false;
   }

   if( Level.NetMode!=NM_Client )
   {
      AnimAction = NewAction;
      bResetAnimAct = True;
      ResetAnimActTime = Level.TimeSeconds+0.3;
   }
}

simulated function bool AnimNeedsWait(name AnimName)
{
   if (AnimName == 'KnockDown')
      return true;

   return Super.AnimNeedsWait(AnimName);
}

// Handle playing the anim action on the upper body only if we're attacking and moving
simulated function int AttackAndMoveDoAnimAction(name AnimName)
{
   // just do the second animation while moving since the other one is prone to bugs on unfreeze
   if (AnimName == 'ClawAndMove')
   {
      AnimName = 'GoreAttack2';
		CurrentDamtype = ZombieDamType[0];
   }

   if (AnimName == 'GoreAttack2' || AnimName == 'GoreAttack1')
   {
      fAnimEndTime     = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0);
      AnimBlendParams(1, 1.0, 0.0,, FireRootBone);
      PlayAnim(AnimName,, 0.1, 1);

      return 1;
   }

   return super.DoAnimAction(AnimName);
}


// Shouldn't fight with our own and boss
function bool SameSpeciesAs(Pawn P)
{
   return (FalkZombieGorefast(P) != none || Super.SameSpeciesAs(P));
}

simulated function HideBone(name boneName)
{
   //  Gorefast does not have a left arm and does not need it to be hidden
   if (boneName != LeftFArmBone)
   {
      super.HideBone(boneName);
   }
}

static simulated function PreCacheMaterials(LevelInfo myLevel)
{
   myLevel.AddPrecacheMaterial(Combiner'LairTextures_T.ZedsUpscale.GorefastCombinerFinal');
   myLevel.AddPrecacheMaterial(Combiner'LairTextures_T.ZedsUpscale.GorefastEnvironmentCombiner');
   myLevel.AddPrecacheMaterial(Texture'LairTextures_T.ZedsUpscale.gorefast_diff');
}

// Run on unfreeze, hopefully without losing the target
function fUnfreezeFix()
{
   bRunning   = True;
   GoToState('RunningState');

   Super.fUnfreezeFix();
}

// return zed class
function FalkZedClass FalkGetZedClass()
{
   return Falk_Gorefast;
}

// stop walking fast and raging slow pls
simulated function Tick(float DeltaTime)
{
    Super.Tick(DeltaTime);

    if (!bRunning && GroundSpeed > OriginalGroundSpeed)
        GroundSpeed = OriginalGroundSpeed;

    // yet another attempt to fix wrong rage anims being selected and slowrage from happening
    if (fSpeedUpdateTime > 0)
        fSpeedUpdateTime -= DeltaTime;

    else
    {
        fSpeedUpdateTime = Default.fSpeedUpdateTime;
        PostNetReceive();

        if (!bRunning || bZapped || bDecapitated || DecapitatedBy != none)
            return;

        if (bBurnified)
            GroundSpeed = OriginalGroundSpeed * fRageSpeed * fBurningSpeed;

        else
            GroundSpeed = OriginalGroundSpeed * fRageSpeed;
    }

}

defaultproperties
{
   EventClasses(0)="Falk689ZedsFix.FZombieGorefast_STANDARD"
   bLeftArmGibbed=True
   ControllerClass=Class'FalkGorefastController' 
}
