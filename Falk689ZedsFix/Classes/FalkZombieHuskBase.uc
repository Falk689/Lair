/* MEDIUM ZED */

class FalkZombieHuskBase extends FalkMonster
   abstract;

enum EBBSkillState
{
   BBS_Inactive,
   BBS_Started,
   BBS_Active,
   BBS_Used
};

var     float             NextFireProjectileTime;            // track when we will fire again
var     float             LastFireProjectileTime;            // track when we fired last time
var     float             fNextFireProjectileTime;           // track when we will fire again if we don't trigger the skill
var()   float             ProjectileFireInterval;            // how often to fire the fire projectile
var     class<Projectile> HuskFireProjClass;                 // projectile class we fire from our cannon
var     byte              BulletID;                          // multi hit fix
var     byte              fMeleeHits;                        // how many time we used the melee attack consecutively
var     byte              fMaxMeleeHits;                     // maximum consecutive melee attacks
var     float             fLastMeleeTime;                    // last time we attacked with a melee hit
var     float             fMeleeHitReset;                    // after how much time we'll reset the melee hit counter if no action is taken
var     EBBSkillState     fBloodbathSkillState;              // bloodbath skill state
var     float             fBloodbathSkillActivationTime;     // when the bloodbath skill activated
var     float             fBloodbathSkillMeleeCooldown;      // time in seconds in which we prevent a melee attack while the skill is active
var     float             fCurBloodbathSkillMeleeCooldown;   // current time in seconds in which we prevent a melee attack while the skill is active
var     float             fBloodbathSkillHealthMulti;        // health multiplier reached to use the bloodbath skill


defaultproperties
{
   MeleeAnims(0)="Strike"
   MeleeAnims(1)="Strike"
   MeleeAnims(2)="Strike"
   bHarpoonToBodyStuns=False
   bHarpoonToHeadStuns=False
   ZombieFlag=1
   MeleeDamage=15
   damageForce=70000
   bFatAss=True
   KFRagdollName="Burns_Trip"
   Intelligence=BRAINS_Mammal
   bCanDistanceAttackDoors=false
   bUseExtendedCollision=True
   ColOffset=(Z=36.000000)
   ColRadius=30.000000
   ColHeight=33.000000
   SeveredArmAttachScale=0.900000
   SeveredLegAttachScale=0.900000
   SeveredHeadAttachScale=0.900000
   OnlineHeadshotOffset=(X=20.000000,Z=55.000000)
   AmmunitionClass=Class'KFMod.BZombieAmmo' // lol - lol indeed
   IdleHeavyAnim="Idle"
   IdleRifleAnim="Idle"
   MeleeRange=30.000000
   HeadHeight=1.000000
   HeadScale=1.500000
   AmbientSoundScaling=8.000000
   MenuName="Husk"
   MovementAnims(0)="WalkF"
   MovementAnims(1)="WalkB"
   MovementAnims(2)="WalkL"
   MovementAnims(3)="WalkR"
   WalkAnims(1)="WalkB"
   WalkAnims(2)="WalkL"
   WalkAnims(3)="WalkR"
   IdleCrouchAnim="Idle"
   IdleWeaponAnim="Idle"
   IdleRestAnim="Idle"
   DrawScale=1.400000
   PrePivot=(Z=22.000000)
   Skins(1)=Shader'KF_Specimens_Trip_T_Two.burns.burns_shdr'
   SoundVolume=200
   RotationRate=(Yaw=45000,Roll=0)
   Mass=400.000000               // medium zeds standard
   MotionDetectorThreat=0.500000 // a single medium zed triggers a pipe, two trigger an ATM
   HeadHealth=200.000000
   Health=600
   HealthMax=600
   fHSKThreshold=0.55             // instakill at 330 damage at 1man normal
   fDeathDrama=0.06               // medium zeds standard
   ScoringValue=20                // medium zeds standard
   PlayerCountHealthScale=0.1     // medium zeds standard
   PlayerNumHeadHealthScale=0.1   // medium zeds standard
   BleedOutDuration=5.0           // medium zeds standard
   ZapDuration=3.5                // medium zeds standard
   GroundSpeed=115.000000
   WaterSpeed=115.000000
   ProjectileFireInterval=7.0     // was 6.0 in patch 13
   HuskFireProjClass=Class'HuskFireProjectileFalk'
   fSFireMultiplier=0.75
   fWFireMultiplier=0.25
   fShotgunWeakness=True
   ZappedDamageMod=1.000000
   ZapResistanceScale=3.000000
   ZapThreshold=10.000000          // medium zeds standard
   fStunThresh=400                 // medium zeds standard
   bMeleeStunImmune=false
   bStunImmune=false
   fMaxMeleeHits=1
   fMeleeHitReset=3.0
   fFleshpoundRageImmune=True
   fBloodbathSkillHealthMulti=0.5
   fBloodbathSkillMeleeCooldown=2.6
   fCurBloodbathSkillMeleeCooldown=2.6
}
