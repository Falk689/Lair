/* HEAVY ZED */

class FalkZombieScrake extends FalkZombieScrakeBase
   abstract;
   
#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=LairScrake_A.ukx

simulated function PostNetBeginPlay()
{
   EnableChannelNotify ( 1,1);
   AnimBlendParams(1, 1.0, 0.0,, SpineBone1);
   super.PostNetBeginPlay();
}

simulated function PostBeginPlay()
{
   Super.PostBeginPlay();

   SpawnExhaustEmitter();

   if (Role < ROLE_Authority)
      return;

   // Difficulty Scaling
   if (Level.Game != none)
   {
      if (Level.Game.GameDifficulty < 3.0) // normal
         fCurrentRageMulti = Default.fNormalRageMulti;

      else if (Level.Game.GameDifficulty < 5.0) // hard
         fCurrentRageMulti = Default.fHardRageMulti;

      else if (Level.Game.GameDifficulty < 6.0) // suicidal
         fCurrentRageMulti = Default.fSuicidalRageMulti;

      else if (Level.Game.GameDifficulty < 8.0) // hoe
         fCurrentRageMulti = Default.fHOERageMulti;

      else                                      // bloodbath
      {
         fCurrentRageMulti = Default.fBBRageMulti;

         // set the frustration timer
         fFrustrationRageTime = Level.TimeSeconds + fMinFrustration + fRandFrustration * FRand();

         //warn("CurrentTime:"@Level.TimeSeconds@" - RageTime:"@fFrustrationRageTime);
      }
   }

}

// Make the scrakes's ambient scale higher, since there are just a few and their chainsaws need to be heard from a distance
simulated function CalcAmbientRelevancyScale()
{
   // Make the zed only relevant by their ambient sound out to a range of 30 meters
   CustomAmbientRelevancyScale = 1500 / (100 * SoundRadius);
}

simulated function PostNetReceive()
{
   if (bCharging)
      MovementAnims[0] = 'ChargeF';

   else if (!(bCrispified && bBurnified))
      MovementAnims[0] = default.MovementAnims[0];

   if (!bCrispified && !bBurnified)
   {
      if (bZapped || (Health <= 0 && !fFrozen))
      {
         Skins[0]=Shader'KF_Specimens_Trip_T.scrake_FB';
         Skins[1]=TexPanner'KF_Specimens_Trip_T.scrake_saw_panner';
      }

      else if (!fFrozen)
      {
         Skins[0]=Shader'LairTextures_T.ZedsUpscale.ScrakeShader';
         Skins[1]=TexPanner'LairTextures_T.ZedsUpscale.ChainsawBladeTexPanner';
      }
   }
}

// This zed has been taken control of, not sure what this does but nah, don't boost anything
function SetMindControlled(bool bNewMindControlled)
{
   if( bNewMindControlled )
   {
      NumZCDHits++;

      // if we hit him a couple of times, make him rage!
      if( NumZCDHits > 1 )
      {
         if( !IsInState('RunningToMarker') )
         {
            GotoState('RunningToMarker');
         }
         else
         {
            NumZCDHits = 1;
            if( IsInState('RunningToMarker') )
            {
               GotoState('');
            }
         }
      }
      else
      {
         if( IsInState('RunningToMarker') )
         {
            GotoState('');
         }
      }
   }
   else
      NumZCDHits=0;

   bZedUnderControl = bNewMindControlled;
}

// Handle the zed being commanded to move to a new location
function GivenNewMarker()
{
   if (bCharging && NumZCDHits > 1)
      GotoState('RunningToMarker');

   else
      GotoState('');
}

// If we're burning keep charging
simulated function SetBurningBehavior()
{
	// Set the forward movement anim to a random burning anim
	MovementAnims[0] = BurningWalkFAnims[Rand(3)];
	WalkAnims[0]     = BurningWalkFAnims[Rand(3)];

	// Set the rest of the movement anims to the headless anim (not sure if these ever even get played) - Ramm
	MovementAnims[1] = BurningWalkAnims[0];
	WalkAnims[1]     = BurningWalkAnims[0];
	MovementAnims[2] = BurningWalkAnims[1];
	WalkAnims[2]     = BurningWalkAnims[1];
	MovementAnims[3] = BurningWalkAnims[2];
	WalkAnims[3]     = BurningWalkAnims[2];
}

simulated function SpawnExhaustEmitter()
{
   if ( Level.NetMode != NM_DedicatedServer )
   {
      if ( ExhaustEffectClass != none )
      {
         ExhaustEffect = Spawn(ExhaustEffectClass, self);

         if ( ExhaustEffect != none )
         {
            AttachToBone(ExhaustEffect, 'Chainsaw_lod1');
            ExhaustEffect.SetRelativeLocation(vect(0, -20, 0));
         }
      }
   }
}


simulated function UpdateExhaustEmitter()
{
   local byte Throttle;

   if ( Level.NetMode != NM_DedicatedServer )
   {
      if ( ExhaustEffect != none )
      {
         if ( bShotAnim )
         {
            Throttle = 3;
         }
         else
         {
            Throttle = 0;
         }
      }
      else
      {
         if ( !bNoExhaustRespawn )
         {
            SpawnExhaustEmitter();
         }
      }
   }
}

simulated function Tick(float DeltaTime)
{
   super.Tick(DeltaTime);

   UpdateExhaustEmitter();

   // don't run while stunned
   if (fIsStunned || Level.TimeSeconds < fFlipOverRecoverTime)
   {
      SetGroundSpeed(1);
      //warn("ZERO");
   }

   // don't slowrage, never ever pls
   else if (!fFrozen && !bZapped)
   {
      if (!bDecapitated && GroundSpeed > 1)
      {
         if (fFrustrationRageTime == -689 || HealthMax * fCurrentRageMulti > Health)
         {
            if (GroundSpeed < GetOriginalGroundSpeed() * Default.fRageSpeed)
            {
               //warn("RAGE");
               SetGroundSpeed(GetOriginalGroundSpeed() * Default.fRageSpeed);
            }
         }

         else
         {
            //warn("NORMAL");
            SetGroundSpeed(GetOriginalGroundSpeed());
         }
      }


      else
      {
         //warn("HEADLESS");
         SetGroundSpeed(GetOriginalGroundSpeed() * Default.fHeadlessSpeed);
      }
   }
}

// make the scrake rage under given health levels based on difficulty
function RangedAttack(Actor A)
{
   local PlayerController PC;

   if (fFrozen || bShotAnim ||fIsStunned || Level.TimeSeconds < fFlipOverRecoverTime || Physics == PHYS_Swimming)
      return;

   else if (CanAttack(A))
   {
      bShotAnim = true;
      SetAnimAction(MeleeAnims[0]);
      CurrentDamType = ZombieDamType[0];
      GoToState('SawingLoop');
   }

   //warn("SawingLoop:"@HealthMax * fCurrentRageMulti > Health@"Threshold:"@fCurrentRageMulti@"FIsStunned:"@fIsStunned);

   // rage mechanics
   if (!bShotAnim && !fIsStunned && Level.TimeSeconds >= fFlipOverRecoverTime && !bDecapitated && DecapitatedBy == none)
   {
      // normal health based rage
      if (HealthMax * fCurrentRageMulti > Health)
         GoToState('RunningState');

      // bloodbath frustration rage
      if (Role == ROLE_Authority && fFrustrationRageTime > 0)
      {  
         if (!fRageSoundPlayed && Level.TimeSeconds >= fFrustrationRageTime - 2.1)
         {
            fRageSoundPlayed = True;
            //PlaySound(FScrakeTaunt, SLOT_Talk, FTauntVolume, false, 689);

            foreach DynamicActors(class'PlayerController', PC)
                PC.ClientPlaySound(FScrakeTaunt, true, FTauntVolume);

         }

         if (Level.TimeSeconds >= fFrustrationRageTime)
         {
            fFrustrationRageTime = -689;
            GoToState('RunningState');
         }
      }
   }
}

state RunningState
{
   // kinda last hope to make it stop charging while decapitated
   function Tick( float Delta )
   {
      Global.Tick(Delta);

      if (bZapped || bDecapitated || DecapitatedBy != none)
      {
         SetGroundSpeed(GetOriginalGroundSpeed());
         GoToState('');
      }
   }

   // Set the zed to the zapped behavior
   simulated function SetZappedBehavior()
   {
      Global.SetZappedBehavior();
      GoToState('');
   }

   // Don't override speed in this state
   function bool CanSpeedAdjust()
   {
      return (Level.TimeSeconds >= fFlipOverRecoverTime && GroundSpeed < GetOriginalGroundSpeed() * Default.fRageSpeed);
   }

   function BeginState()
   {
      if (bZapped || bDecapitated || DecapitatedBy != none)
         GoToState('');

      else
      {
         SetGroundSpeed(OriginalGroundSpeed * Default.fRageSpeed);
         bCharging = true;

         if (Level.NetMode!=NM_DedicatedServer)
            PostNetReceive();

         NetUpdateTime = Level.TimeSeconds - 1;
      }
   }

   function EndState()
   {
      if (!bZapped)
         SetGroundSpeed(GetOriginalGroundSpeed());

      bCharging = False;

      if (Level.NetMode != NM_DedicatedServer)
         PostNetReceive();
   }

   function RemoveHead()
   {
      SetGroundSpeed(GetOriginalGroundSpeed());
      bCharging = False;
      GoToState('');
      Global.RemoveHead();
   }

   function RangedAttack(Actor A)
   {
      if ( bShotAnim || Physics == PHYS_Swimming)
         return;
      else if ( CanAttack(A) )
      {
         bShotAnim = true;
         SetAnimAction(MeleeAnims[1]);
         CurrentDamType = ZombieDamType[0];
         GoToState('SawingLoop');
      }
   }
}

// State where the zed is charging to a marked location.
// Not sure if we need this since its just like RageCharging,
// but keeping it here for now in case we need to implement some
// custom behavior for this state
state RunningToMarker extends RunningState
{
}

// removed some more weird shit
State SawingLoop
{
   // Don't override speed in this state
   function bool CanSpeedAdjust()
   {
      return false;
   }

   function bool CanGetOutOfWay()
   {
      return false;
   }

   function BeginState()
   {
      //warn("SawingLoop:"@HealthMax * fCurrentRageMulti > Health@"Threshold:"@fCurrentRageMulti@"FIsStunned:"@fIsStunned);

      if (!bShotAnim && !fIsStunned && Level.TimeSeconds >= fFlipOverRecoverTime && !bDecapitated && DecapitatedBy == none && Health > 0 && HealthMax * fCurrentRageMulti > Health)
         GoToState('RunningState');
   }

   function RangedAttack(Actor A)
   {
      if (bShotAnim)
         return;

      else if (CanAttack(A))
      {
         Acceleration = vect(0,0,0);
         bShotAnim = true;
         MeleeDamage = Max(DifficultyDamageModifer() * default.MeleeDamage * 0.5, 1);
         SetAnimAction('SawImpaleLoop');
         CurrentDamType = ZombieDamType[0];

         if (AmbientSound != SawAttackLoopSound)
            AmbientSound=SawAttackLoopSound;
      }

      else GoToState('');
   }

   function AnimEnd( int Channel )
   {
      Super.AnimEnd(Channel);
      if (Controller != None && Controller.Enemy != None)
         RangedAttack(Controller.Enemy); // Keep on attacking if possible.
   }

   function Tick( float Delta )
   {
      // Keep the scrake moving toward its target when attacking
      if( Role == ROLE_Authority && bShotAnim && !bWaitForAnim )
      {
         if( LookTarget!=None )
         {
            Acceleration = AccelRate * Normal(LookTarget.Location - Location);
         }
      }

      fLastCanAttackTime = Level.TimeSeconds;

      global.Tick(Delta);
   }

   function EndState()
   {
      AmbientSound=default.AmbientSound;
      MeleeDamage = Max(DifficultyDamageModifer() * default.MeleeDamage, 1); // god that "modifer" hurts me deep

      SetGroundSpeed(GetOriginalGroundSpeed());
      bCharging = False;

      if (Level.NetMode != NM_DedicatedServer)
         PostNetReceive();
   }
}

function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
   /*if (damageType == class'DamTypeFreezeStop')
      Damage = 3;*/

   if (FalkZombieFleshpound(InstigatedBy) != none)
   {
      Damage = 0;
      return;
   }

   Super.TakeDamage(Damage, InstigatedBy, Hitlocation, Momentum, damageType, HitIndex); 
}

function PlayTakeHit(vector HitLocation, int Damage, class<DamageType> DamageType)
{
   local int StunChance;

   if (fFrozen || fIsStunned || Level.TimeSeconds < fFlipOverRecoverTime)
      return;

   StunChance = rand(5);

   if (Level.TimeSeconds - LastPainAnim < MinTimeBetweenPainAnims)
      return;

   if (Damage >= 200)
      PlayDirectionalHit(HitLocation);

   LastPainAnim = Level.TimeSeconds;

   if (Level.TimeSeconds - LastPainSound < MinTimeBetweenPainSounds)
      return;

   LastPainSound = Level.TimeSeconds;
   PlaySound(HitSound[0], SLOT_Pain,1.25,,400);
}


simulated function int DoAnimAction(name AnimName)
{
   if (fFrozen || ((fIsStunned || Level.TimeSeconds < fFlipOverRecoverTime) && AnimName != 'KnockDown'))
      return 0;
   
   if (AnimName=='SawZombieAttack1' || AnimName=='SawZombieAttack2')
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;
      AnimBlendParams(1, 1.0, 0.0,, FireRootBone);
      PlayAnim(AnimName,, 0.1, 1);
      Return 1;
   }

   Return Super.DoAnimAction(AnimName);
}


simulated event SetAnimAction(name NewAction)
{
   local int meleeAnimIndex;

   if (NewAction=='' || fFrozen || ((fIsStunned || Level.TimeSeconds < fFlipOverRecoverTime) && NewAction != 'KnockDown'))
      return;

   if(NewAction == 'Claw')
   {
      if (IsInState('RunningToMarker') || isInState('RunningState'))
         meleeAnimIndex = 1;

      else
         meleeAnimIndex = 0;

      NewAction      = meleeAnims[meleeAnimIndex];
      CurrentDamtype = ZombieDamType[0];
   }
   ExpectingChannel = DoAnimAction(NewAction);

   if( AnimNeedsWait(NewAction) )
   {
      bWaitForAnim = true;
   }

   if( Level.NetMode!=NM_Client )
   {
      AnimAction = NewAction;
      bResetAnimAct = True;
      ResetAnimActTime = Level.TimeSeconds+0.3;
   }
}


// Shouldn't fight with our own and boss
function bool SameSpeciesAs(Pawn P)
{
   return (FalkZombieFleshpoundBase(P) != none || FalkZombieBruteBase(P) != none || FalkZombieScrakeBase(P) != none || Super.SameSpeciesAs(P));
}


// The animation is full body and should set the bWaitForAnim flag
simulated function bool AnimNeedsWait(name TestAnim)
{
   if (TestAnim == 'SawImpaleLoop' || TestAnim == 'DoorBash' || TestAnim == 'KnockDown')
      return true;

   return false;
}

function PlayDyingSound()
{
   if( Level.NetMode!=NM_Client )
   {
      if ( bGibbed )
      {
         // Do nothing for now
         PlaySound(GibGroupClass.static.GibSound(), SLOT_Pain,2.0,true,525);
         return;
      }

      if( bDecapitated )
      {

         PlaySound(HeadlessDeathSound, SLOT_Pain,1.30,true,525);
      }
      else
      {
         PlaySound(DeathSound[0], SLOT_Pain,1.30,true,525);
      }

      PlaySound(ChainSawOffSound, SLOT_Misc, 2.0,,525.0);
   }
}

// award a nade on LAW kill
function Died(Controller Killer, class<DamageType> damageType, vector HitLocation)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   if (!bCrispified && !bBurnified && !fFrozen)
   {
      Skins[0]=Shader'KF_Specimens_Trip_T.scrake_FB';
      Skins[1]=TexPanner'KF_Specimens_Trip_T.scrake_saw_panner';
   }
 
   if (Killer != none && damageType != none && (class<DamTypeLAWExplosiveBulletFalk>(damageType) != none || class<DamTypeLAWFalk>(damageType) != none))
   {
      KFPRI = KFPlayerReplicationInfo(Killer.PlayerReplicationInfo);

      if (KFPRI != none)
      {
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none) 
            Vet.Static.LAWKillNadeAward(KFPRI, LastBulletHitID);
      }
   }

   AmbientSound = none;


   if ( ExhaustEffect != none )
   {
      ExhaustEffect.Destroy();
      ExhaustEffect = none;
      bNoExhaustRespawn = true;
   }

   Super.Died(Killer, damageType, HitLocation);
}

simulated function ProcessHitFX()
{
   local Coords boneCoords;
   local class<xEmitter> HitEffects[4];
   local int i,j;
   local float GibPerterbation;

   if( (Level.NetMode == NM_DedicatedServer) || bSkeletized || (Mesh == SkeletonMesh))
   {
      SimHitFxTicker = HitFxTicker;
      return;
   }

   for ( SimHitFxTicker = SimHitFxTicker; SimHitFxTicker != HitFxTicker; SimHitFxTicker = (SimHitFxTicker + 1) % ArrayCount(HitFX) )
   {
      j++;
      if ( j > 30 )
      {
         SimHitFxTicker = HitFxTicker;
         return;
      }

      if( (HitFX[SimHitFxTicker].damtype == None) || (Level.bDropDetail && (Level.TimeSeconds - LastRenderTime > 3) && !IsHumanControlled()) )
         continue;

      //log("Processing effects for damtype "$HitFX[SimHitFxTicker].damtype);

      if( HitFX[SimHitFxTicker].bone == 'obliterate' && !class'GameInfo'.static.UseLowGore())
      {
         SpawnGibs( HitFX[SimHitFxTicker].rotDir, 1);
         bGibbed = true;
         // Wait a tick on a listen server so the obliteration can replicate before the pawn is destroyed
         if( Level.NetMode == NM_ListenServer )
         {
            bDestroyNextTick = true;
            TimeSetDestroyNextTickTime = Level.TimeSeconds;
         }
         else
         {
            Destroy();
         }
         return;
      }

      boneCoords = GetBoneCoords( HitFX[SimHitFxTicker].bone );

      if ( !Level.bDropDetail && !class'GameInfo'.static.NoBlood() && !bSkeletized && !class'GameInfo'.static.UseLowGore() )
      {
         //AttachEmitterEffect( BleedingEmitterClass, HitFX[SimHitFxTicker].bone, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir );

         HitFX[SimHitFxTicker].damtype.static.GetHitEffects( HitEffects, Health );

         if( !PhysicsVolume.bWaterVolume ) // don't attach effects under water
         {
            for( i = 0; i < ArrayCount(HitEffects); i++ )
            {
               if( HitEffects[i] == None )
                  continue;

               AttachEffect( HitEffects[i], HitFX[SimHitFxTicker].bone, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir );
            }
         }
      }
      if ( class'GameInfo'.static.UseLowGore() )
         HitFX[SimHitFxTicker].bSever = false;

      if( HitFX[SimHitFxTicker].bSever )
      {
         GibPerterbation = HitFX[SimHitFxTicker].damtype.default.GibPerterbation;

         switch( HitFX[SimHitFxTicker].bone )
         {
            case 'obliterate':
               break;

            case LeftThighBone:
               if( !bLeftLegGibbed )
               {
                  if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
                     SpawnSeveredGiblet(BurntDetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  else
                     SpawnSeveredGiblet(DetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  KFSpawnGiblet( class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  bLeftLegGibbed=true;
               }
               break;

            case RightThighBone:
               if( !bRightLegGibbed )
               {
                  if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
                     SpawnSeveredGiblet(BurntDetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  else
                     SpawnSeveredGiblet(DetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  KFSpawnGiblet( class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  bRightLegGibbed=true;
               }
               break;

            case LeftFArmBone:
               if( !bLeftArmGibbed )
               {
                  if (bCrispified && BurntDetachedArmClass != Class'KFChar.SeveredArmClot')
                     SpawnSeveredGiblet(BurntDetachedArmClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  else
                     SpawnSeveredGiblet(DetachedArmClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  KFSpawnGiblet( class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );;
                  bLeftArmGibbed=true;
               }
               break;

            case RightFArmBone:
               if( !bRightArmGibbed )
               {
                  if (bCrispified && BurntDetachedSpecialArmClass != Class'KFChar.SeveredArmClot')
                     SpawnSeveredGiblet(BurntDetachedSpecialArmClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  else
                     SpawnSeveredGiblet(DetachedSpecialArmClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  KFSpawnGiblet( class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  KFSpawnGiblet( class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250 );
                  bRightArmGibbed=true;
               }
               break;

            case 'head':
               if( !bHeadGibbed )
               {
                  if ( HitFX[SimHitFxTicker].damtype == class'DamTypeDecapitation' )
                  {
                     DecapFX( boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false);
                  }
                  else if( HitFX[SimHitFxTicker].damtype == class'DamTypeProjectileDecap' )
                  {
                     DecapFX( boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false, true);
                  }
                  else if( HitFX[SimHitFxTicker].damtype == class'DamTypeMeleeDecapitation' )
                  {
                     DecapFX( boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, true);
                  }

                  bHeadGibbed=true;
               }
               break;
         }


         if( HitFX[SimHitFXTicker].bone != 'Spine' && HitFX[SimHitFXTicker].bone != FireRootBone &&
               HitFX[SimHitFXTicker].bone != 'head' && Health <=0 )
            HideBone(HitFX[SimHitFxTicker].bone);
      }
   }
}

// Maybe spawn some chunks when the player gets obliterated
simulated function SpawnGibs(Rotator HitRotation, float ChunkPerterbation)
{
   if ( ExhaustEffect != none )
   {
      ExhaustEffect.Destroy();
      ExhaustEffect = none;
      bNoExhaustRespawn = true;
   }

   super.SpawnGibs(HitRotation,ChunkPerterbation);
}

static simulated function PreCacheMaterials(LevelInfo myLevel)
{
   myLevel.AddPrecacheMaterial(Combiner'LairTextures_T.ZedsUpscale.ScrakeCombinerFinal');
   myLevel.AddPrecacheMaterial(Texture'LairTextures_T.ZedsUpscale.scrake_diff');
   myLevel.AddPrecacheMaterial(Texture'KF_Specimens_Trip_T.scrake_spec');
   myLevel.AddPrecacheMaterial(Material'LairTextures_T.ZedsUpscale.ChainsawBladeTexPanner');
   myLevel.AddPrecacheMaterial(Material'LairTextures_T.ZedsUpscale.ScrakeShader');
   myLevel.AddPrecacheMaterial(Texture'LairTextures_T.ZedsUpscale.Chainsaw_blade_diff');
   myLevel.AddPrecacheMaterial(Combiner'KF_Specimens_Trip_T.scrake_env_cmb');
   myLevel.AddPrecacheMaterial(Material'KF_Specimens_Trip_T.scrake_saw_panner');
}

// italian job on the shader to hopefully make us visible while zapped
function SetZapped(float ZapAmount, Pawn Instigator)
{
   if (!bCrispified && !bBurnified)
   {
      Skins[0]=Shader'KF_Specimens_Trip_T.scrake_FB';
      Skins[1]=TexPanner'KF_Specimens_Trip_T.scrake_saw_panner';
   }

   Super.SetZapped(ZapAmount, Instigator);

   PostNetReceive();
}

// unset the italian job on the main shader
simulated function UnSetZappedBehavior()
{
   if (!bCrispified && !bBurnified)
   {
      Skins[0]=Shader'LairTextures_T.ZedsUpscale.ScrakeShader';
      Skins[1]=TexPanner'LairTextures_T.ZedsUpscale.ChainsawBladeTexPanner';
   }

   Super.UnSetZappedBehavior();

   PostNetReceive();
}

// return zed class
function FalkZedClass FalkGetZedClass()
{
   return Falk_Scrake;
}


// setup wait time to charge on knockdown
function bool FlipOver()
{
   if (!fAlreadyStunned && !fFrozen && !bShotAnim)
   {
      fFlipOverRecoverTime = Level.TimeSeconds + GetAnimDuration('KnockDown') + fFlipOverAddTime;

      //warn("Setting FlipOver Time:"@fFlipOverRecoverTime@"- Current Time -"@Level.TimeSeconds);
   }

   return Super.FlipOver();
}



defaultproperties
{
   EventClasses(0)="Falk689ZedsFix.FZombieScrake_STANDARD"
   ControllerClass=Class'FalkScrakeController'
}
