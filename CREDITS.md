# Lair Total Game Balance - Credits

* [Andewyl](https://steamcommunity.com/id/FearOfABlankPlanet): project management, gameplay balance, maps edits
* [Falk689](https://steamcommunity.com/id/Falk689): code, models, textures, animations
* [Marco](https://steamcommunity.com/profiles/76561197975509070): original server perks mutator
* [scary ghost](https://steamcommunity.com/id/imascaryghost): original server achievements mutator
* [Hemi](https://steamcommunity.com/id/hemicrania) & [Braindead](https://steamcommunity.com/profiles/76561197989157704) & [Benjamin](https://steamcommunity.com/id/BenjaminGoose): creation of the brute
* [IJC Development](https://steamcommunity.com/id/ijcdevelopment): concept of freeze mechanics and what's left of their original code
* [Lorex](https://steamcommunity.com/id/Lorex634): helping out with the creation of the handbook
* [Electro 69](https://steamcommunity.com/profiles/76561198048284656): helping out fixing some of the weirdest bugs we've had to deal with over the years
* [Ferenos](https://steamcommunity.com/id/Ferenos16): laying the foundations for KFL-Bioticsafterfall, KFL-Cemetery, KFL-Chaostemple, KFL-Consequences, KFL-Mushroomvalley, KFL-Scorpionrealm, KFL-Subsurface, KFL-Trainstation
* [DannyArt](https://steamcommunity.com/id/DannyManPc): laying the foundations for KFL-Dynasty
 
Special thanks to all the other mappers whose original maps have been edited and are now part of the official maplist.  
A thanks to all those modders who created weapons that made it into the project after being heavily edited.  
Last but not least, a special thank you to all those people who helped us testing and/or generally put up with us and our project during these years:

Snake  
Koragos  
Nekro  
A Pancake  
Istanteh  
Legacy  
scone  
Time  
[ScrN]Poosh  
Averill  
Cento  
Holy Username Batman!  
JColin677  
Mart  
murubura  
Spoom  
theCoon  
M3rc!_33#  

If we forgot someone, you know who you are and how you helped us, thank you!
