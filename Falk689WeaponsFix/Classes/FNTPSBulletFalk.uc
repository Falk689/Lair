class FNTPSBulletFalk extends ShotgunBulletFalk;

defaultproperties
{
   FDamage=37.000000
   Speed=5500.000000
   MaxSpeed=5500.000000
   MaxPenetrations=2
   PenDamageReduction=0.500000
   HeadShotDamageMult=1.100000
   MyDamageType=Class'DamTypeFNTPSFalk'
}