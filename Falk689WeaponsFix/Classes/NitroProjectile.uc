class NitroProjectile extends ShotgunBullet;

var class<Emitter> Trail1Class, Trail2Class;
var Emitter Trail1, Trail2;
var Actor      FLastDamaged;   // last damaged zed, used to attempt to fix multiple shots on the same zed
var byte       BulletID;       // bullet ID, passed at takedamage to prevent multiple shots
var Controller fCInstigator;   // instigator controller, I don't use vanilla InstigatorController 'cause of reasons

var int        FPenetrations;
var float      FDamage;


replication
{
	reliable if(Role == ROLE_Authority)
		BulletID, FLastDamaged, fCInstigator;
}


simulated function PostBeginPlay()
{
   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

	SetTimer(0.2, true);

	Velocity = Speed * Vector(Rotation);

	if ( Level.NetMode != NM_DedicatedServer )
	{
		if ( !PhysicsVolume.bWaterVolume )
		{
			Trail1 = Spawn(Trail1Class,self);
			Trail2 = Spawn(Trail2Class,self);
		}
	}

	Velocity.z += TossZ;
}

simulated function PostNetBeginPlay()
{
	local PlayerController PC;

	Super.PostNetBeginPlay();

	if ( Level.NetMode == NM_DedicatedServer )
	{
		return;
	}

	if ( Level.bDropDetail || (Level.DetailMode == DM_Low) )
	{
		bDynamicLight = false;
		LightType = LT_None;
	}
	else
	{
		PC = Level.GetLocalPlayerController();
		if ( (Instigator != None) && (PC == Instigator.Controller) )
		{
			return;
		}

		if ( (PC == None) || (PC.ViewTarget == None) || (VSize(PC.ViewTarget.Location - Location) > 3000) )
		{
			bDynamicLight = false;
			LightType = LT_None;
		}
	}
}

simulated function Landed( vector HitNormal )
{
	Explode(Location,HitNormal);
    if ( Level.DetailMode >= DM_High )
        Spawn(class'NitroGroundEffect',self,,Location);    
}

simulated function Explode(vector HitLocation,vector HitNormal)
{
	if ( Role == ROLE_Authority )
	{
		HurtRadius(FDamage, DamageRadius, MyDamageType, MomentumTransfer, HitLocation );
	}

    PlaySound(ImpactSound, SLOT_Misc);

	if ( KFHumanPawn(Instigator) != none && !Level.bDropDetail ) {
		if ( EffectIsRelevant(Location,false) ) {
			Spawn(ExplosionDecal,self,,Location, rotator(-HitNormal));
            if ( Level.DetailMode > DM_Low ) {
                Spawn(class'NitroSplash',self,,HitLocation,rotator(HitNormal));
                if ( Level.DetailMode >= DM_SuperHigh )
                    Spawn(class'NitroImpact',self,,HitLocation,rotator(-HitNormal));
            }
		}
	}

	SetCollisionSize(0.0, 0.0);
	Destroy();
}

// don't damage projectiles
simulated function HurtRadius( float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation )
{
	local actor Victims;
	local float dist;
	local vector dir;

	if ( bHurtEntry )
		return;

	bHurtEntry = true;
	foreach VisibleCollidingActors( class 'Actor', Victims, DamageRadius, HitLocation )
	{
		// don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
		if ((Victims != none) && (Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) 
                && !Victims.IsA('FluidSurfaceInfo') && !Victims.IsA('Projectile') )
		{
			dir  = Victims.Location - HitLocation;
			dist = FMax(1,VSize(dir));
			dir  = dir/dist;
			Victims.SetDelayedDamageInstigatorController(fCInstigator);
			if ( Victims == LastTouched )
				LastTouched = None;
			Victims.TakeDamage
			(
				FDamage,
				Instigator,
				Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dir,
				(Momentum * dir),
				DamageType,
            BulletID
			);

			if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
				Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);
		}
	}

	if ((LastTouched != None) && (LastTouched != self) && (LastTouched.Role == ROLE_Authority) 
        && !LastTouched.IsA('FluidSurfaceInfo') && (Victims == none || !Victims.IsA('Projectile')))
	{
		Victims = LastTouched;
		LastTouched = None;
		dir = Victims.Location - HitLocation;
		dist = FMax(1,VSize(dir));
		dir = dir/dist;
		Victims.SetDelayedDamageInstigatorController(fCInstigator);
		Victims.TakeDamage
		(
			FDamage,
			Instigator,
			Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dir,
			(Momentum * dir),
			DamageType,
         BulletID
		);
		if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
			Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);
	}

	bHurtEntry = false;
}

simulated function Destroyed()
{
	if ( Trail != none ) {
		Trail.mRegen=False;
		Trail.SetPhysics(PHYS_None);
	}
    
	if ( Trail1 != none ) {
		Trail1.Kill();
		Trail1.SetPhysics(PHYS_None);
	}    

	if ( Trail2 != none ) {
		Trail2.Kill();
		Trail2.SetPhysics(PHYS_None);
	}
    
	Super.Destroyed();
}

// attempt to fix point blank
simulated function ProcessTouch(Actor Other, vector HitLocation)
{
   local vector X;
   local int bHp;
   local Pawn HitPawn;

   if (Other == none || Other == Instigator || Other.Base == Instigator || Other == FLastDamaged || !Other.bBlockHitPointTraces || KFHumanPawn(Other) != none || KFHumanPawn(Other.Base) != none)
      return;

   FLastDamaged = Other;

   X = Vector(Rotation);

   HitPawn = Pawn(Other);

   if (HitPawn == none)
      HitPawn = Pawn(Other.Base);

   if (HitPawn != none)
   {
      bHP = HitPawn.Health;
      Other.SetDelayedDamageInstigatorController(fCInstigator);
      HitPawn.TakeDamage(FDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);

      // don't scale damage if we haven't damaged the zed
      if (bHP > 0 && HitPawn.Health < bHP)
      {
         FDamage *= PenDamageReduction; // Keep going, but lose effectiveness each time.

         // if we've struck through more than the max number of foes, destroy.
         if (FPenetrations >= MaxPenetrations)
            Destroy();

         FPenetrations++;
      }
   }

   // this may be needed since shotguns basically fire bugs
   else
   {
      Other.SetDelayedDamageInstigatorController(fCInstigator);
      Other.TakeDamage(FDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);

      FDamage *= PenDamageReduction; // Keep going, but lose effectiveness each time.

      // if we've struck through more than the max number of foes, destroy.
      if (FPenetrations >= MaxPenetrations)
         Destroy();

      FPenetrations++;
   }
}


simulated singular function HitWall(vector HitNormal, actor Wall)
{
	if (Role == ROLE_Authority)
   {

      if (Wall == none || Wall == Instigator || Wall.Base == Instigator || Wall == FLastDamaged || !Wall.bBlockHitPointTraces)
         return;

      if (!Wall.bStatic && !Wall.bWorldGeometry)
      {
         if (Instigator == None || Instigator.Controller == None)
         {
            Wall.SetDelayedDamageInstigatorController( InstigatorController);
         }
      }

      MakeNoise(1.0);
   }

   Explode(Location + ExploWallOut * HitNormal, HitNormal);

   HurtWall = None;
}


defaultproperties
{
   Trail1Class=Class'IJC_Project_Santa.NitroTrail'
   Trail2Class=Class'IJC_Project_Santa.NitroTrailB'
   MaxPenetrations=2
   PenDamageReduction=0.600000
   HeadShotDamageMult=1.000000
   Speed=1200.000000
   MaxSpeed=1200.000000
   TossZ=200.000000
   Damage=5.000000
   FDamage=5.000000
   DamageRadius=1.000000
   MomentumTransfer=0.000000
   MyDamageType=Class'DamTypeFreezerGun'
   ImpactSound=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_AcidSplash'
   ExplosionDecal=Class'NitroDecal'
   DrawType=DT_None
   StaticMesh=None
   Physics=PHYS_Falling
   LifeSpan=5.000000
   DrawScale=5.000000
   Style=STY_None
   bBlockHitPointTraces=False
}
