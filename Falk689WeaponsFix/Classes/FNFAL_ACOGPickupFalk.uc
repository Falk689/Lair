class FNFAL_ACOGPickupFalk extends KFMod.FNFAL_ACOG_Pickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   InventoryType=Class'FNFAL_ACOGAssaultRifleFalk'
   ItemName="FN Fusil Automatique Leger"
   ItemShortName="FAL"
   PickupMessage="You got an FN Fusil Automatique Leger"
   cost=3500
   BuyClipSize=20
   AmmoCost=10
   Weight=6.000000
   PowerValue=63
   SpeedValue=53
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
