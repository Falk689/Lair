class PipeBombPickupFalk extends KFMod.PipeBombPickup;

defaultproperties
{
   InventoryType=Class'PipeBombExplosiveFalk'
   ItemName="Pipe Bombs"
   ItemShortName="PBs"
   cost=300
   BuyClipSize=1
   PowerValue=95
   SpeedValue=0
   RangeValue=15
   Weight=1.000000
   AmmoCost=300
}