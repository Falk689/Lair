class DamTypeM249Falk extends DamTypeSW76Falk;

defaultproperties
{
   WeaponClass=Class'M249Falk'
   DeathString="%k killed %o (M249)"
   KDamageImpulse=5500.000000
   KDeathVel=450.000000
   KDeathUpKick=45.000000
   HeadShotDamageMult=1.100000
}