class SW76AmmoPickupFalk extends SW76Wep.SW76AmmoPickup;

defaultproperties
{
   AmmoAmount=32
   InventoryType=Class'SW76AmmoFalk'
   PickupMessage="You found some 9x19mm Parabellum rounds"
}