class M14EBRPickupFalk extends KFMod.M14EBRPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}


defaultproperties
{
   InventoryType=Class'M14EBRBattleRifleFalk'
   ItemName="M14 Enhanced Battle Rifle"
   ItemShortName="M14 EBR"
   PickupMessage="You got an M14 Enhanced Battle Rifle"
   cost=3450
   BuyClipSize=20
   AmmoCost=10
   PowerValue=23
   SpeedValue=24
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
