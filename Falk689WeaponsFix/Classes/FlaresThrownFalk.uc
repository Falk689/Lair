class FlaresThrownFalk extends Flares.FlareThrown;

// tell the gametype we've spawned in server too
simulated function PostNetBeginPlay()
{
   local Falk689GameTypeBase Flk;

   Super.PostNetBeginPlay();
 
   if (Role == ROLE_Authority)
   {
      Flk = Falk689GameTypeBase(Level.Game);

      if (Flk != none)
      {
         //warn("SPAWNED:"@Level.TimeSeconds@"Instigator:"@Instigator);
         Flk.FlareSpawned(PlayerController(Instigator.Controller));
      }
   }
}

// tell the gametype we have been destroyed
simulated function Destroyed()
{
   local Falk689GameTypeBase     Flk;

   Super.Destroyed();

   if (Role == ROLE_Authority)
   {
      Flk = Falk689GameTypeBase(Level.Game);

      if (Flk != none)
      {
         //warn("DESTROYED:"@Level.TimeSeconds@"Instigator:"@Instigator);
         Flk.FlareDestroyed(PlayerController(Instigator.Controller));
      }
   }
}

// testing stuff to prevent the ambient sound from doing weird shit
simulated function Tick(float DeltaTime)
{
   if (bDud && Physics == PHYS_Falling)
      SetRotation(Rotator(Normal(Velocity)));

   if (LifeSpan > 0)
      AmbientVolumeScale = default.AmbientVolumeScale;

   else
      AmbientVolumeScale = 0;
}


// don't touch humans
simulated function ProcessTouch(Actor Other, vector HitLocation)
{
   if (KFHumanPawn(Other) != none || KFHumanPawn(Other.Base) != none)
      return;

   Super.ProcessTouch(Other, HitLocation);
}

defaultproperties
{
   FlameTrailEmitterClass=Class'FlaresTrailFalk'
   LifeSpan=60.000000
   FizzleTimer=300.000000
   Damage=0.000000
   DamageRadius=0.000000
   ImpactDamage=0
}
