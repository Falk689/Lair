class SyringeAltFireFalk extends KFMod.SyringeAltFire;

// block with AllowFire()
event ModeDoFire()
{
    local FHumanPawn FP;

    if (AllowFire())
    {
        FP = FHumanPawn(Instigator);

        if (FP != none)
        {
            FP.fSyringeMain      = False;
            FP.fCanSwitchSyringe = True;
            FP.fIsQHResetTime    = Level.TimeSeconds + FireRate;
        }

        Load = 0;
        Super.ModeDoFire(); // We don't consume the ammo just yet.
    }
}

// called from the syringe to just do the animation without much logic
event HackModeDoFire()
{
    Super.ModeDoFire();
}


// return false if we're feeling well
function bool AllowFire()
{
    local FHumanPawn FP;

    FP = FHumanPawn(Instigator);

    if (FP == none || FP.fHealth >= FP.fHealthMax)
    {
        return false;
    }

    return Weapon.AmmoAmount(ThisModeNum) >= AmmoPerFire;
}

// block with AllowFire()
function PlayFiring()
{
   if (AllowFire())
      Super.PlayFiring();
}

defaultproperties
{
   AmmoPerFire=5000
   FireRate=1.000000
   FireAnimRate=1.3
   FireLoopAnimRate=1.3
}
