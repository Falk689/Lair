class AK47LLIPickupFalk extends KFMod.AK47Pickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
    Weight=6.000000
    cost=1650
    AmmoCost=15
    BuyClipSize=30
    PowerValue=45
    SpeedValue=35
    RangeValue=100
    ItemName="AK47 Assault Rifle"
    ItemShortName="AK47"
    AmmoItemName="7.62x39mm rounds"
    CorrespondingPerkIndex=3
    EquipmentCategoryID=2
    InventoryType=Class'AK47LLIAssaultRifleFalk'
    PickupMessage="You got an AK47 Assault Rifle"
    PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
    StaticMesh=StaticMesh'AK47LLINew_A.AK47LLI_st'
    DrawScale=1.0
    CollisionRadius=25.000000
    CollisionHeight=5.000000
}