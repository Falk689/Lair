class CrossbowArrowFalk extends SW76BulletFalk;

var float fGreenT;
var float fCurT;
var bool  fGreen;

// Don't do artillerist stuff 
simulated function PostBeginPlay()
{
   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   Super(Projectile).PostBeginPlay();

   Velocity = Speed * Vector(Rotation); // starts off slower so combo can be done closer

   SetTimer(0.4, false);
}


// apply green layout
simulated function Tick(float Delta)
{
   Super.Tick(Delta);

   if (Level.NetMode != NM_DedicatedServer && !fGreen)
   {
      fCurT += Delta;

      if (fCurT >= fGreenT)
      {
         fGreen = True;
         UV2Texture = FadeColor'PatchTex.Common.PickupOverlay';
      }
   }
}

// Stick to a wall on hit
simulated function HitWall(vector HitNormal, actor Wall)
{
   speed = VSize(Velocity);

   if (Role == ROLE_Authority && Wall != none && KFMonster(Wall) == none)
   {
      if (!Wall.bStatic && !Wall.bWorldGeometry)
      {
         if (Instigator == None || Instigator.Controller == None)
            Wall.SetDelayedDamageInstigatorController(InstigatorController);

         Wall.TakeDamage(FDamage, instigator, Location, MomentumTransfer * Normal(Velocity), MyDamageType);
         HurtWall = Wall;
      }

      MakeNoise(1.0);
   }

   Playsound(ImpactSounds[Rand(6)]);

   if(Level.NetMode != NM_DedicatedServer)
      Spawn(class'ROBulletHitEffect',,, Location, rotator(-HitNormal));

   if(Instigator!=None && Level.NetMode!=NM_Client)
      MakeNoise(0.3);

   Stick(Wall, Location + HitNormal);
}

// Simplified wall stick since arrows don't stick to zeds anyway
simulated function Stick(actor Wall, vector HitLocation)
{
   SetPhysics(PHYS_None);
   SetBase(Wall);

   if (Base == None)
      Destroy();

   else
      GoToState('OnWall');
}

// on wall state
simulated state OnWall
{
   Ignores HitWall;

   function ProcessTouch(Actor Other, vector HitLocation)
   {
      local Inventory inv;

      if (Pawn(Other) != None && Pawn(Other).Inventory != None)
      {
         for (inv=Pawn(Other).Inventory; inv!=None; inv=inv.Inventory)
         {
            if(Crossbow(Inv) != None && Weapon(inv).AmmoAmount(0) < Weapon(inv).MaxAmmo(0))
            {
               KFweapon(Inv).AddAmmo(1,0);
               PlaySound(Sound'KF_InventorySnd.Ammo_GenericPickup', SLOT_Pain, 2 * TransientSoundVolume,, 400);

               if(PlayerController(Pawn(Other).Controller) != none)
                  PlayerController(Pawn(Other).Controller).ReceiveLocalizedMessage(class'ProjectilePickupMessageFalk', 0);

               Destroy();
            }
         }
      }
   }

   simulated function BeginState()
   {
      bCollideWorld = False;

      if (Level.NetMode != NM_DedicatedServer)
         AmbientSound = None;

      SetCollisionSize(30, 30);
      KSetBlockKarma(false);
   }
}

defaultproperties
{
   StaticMesh=StaticMesh'LairStaticMeshes_SM.ProjectilePickups.Arrow'
   MyDamageType=Class'DamTypeCrossbowFalk'
   Speed=15000.000000
   MaxSpeed=15000.000000
   FDamage=215
   Damage=215
   LifeSpan=95
   fGreenT=0.2
   fShouldPenetrate=True
   MaxPenetrations=2
   PenDamageReduction=0.500000
}
