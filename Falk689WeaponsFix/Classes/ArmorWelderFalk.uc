class ArmorWelderFalk extends ArmorWelderBaseFalk
   dependson(KFVoicePack);

#exec OBJ LOAD FILE=LairAnimations_A.ukx

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

var () float AmmoRegenRate;

var float AmmoRegenCount;

// Scripted Nametag vars
var ScriptedTexture  ScriptedScreen;
var Shader ShadedScreen;
var Material   ScriptedScreenBack;

//Font/Color/stuff
var Font NameFont;
var font SmallNameFont;                           // Used when the name is to big too fit
var color NameColor;                                // Colors
var Color BackColor;

var float ScreenWeldPercent;
var bool bNoTarget;  // Not close enough to door to get reading
var int FireModeArray;

// Speech
var	bool	bJustStarted;
var	float	LastWeldingMessageTime;
var	float	WeldingMessageDelay;


replication
{
   reliable if(Role == ROLE_Authority)
      FalkClientFixIdle;
}

simulated function Tick(float dt)
{
   local KFDoorMover LastDoorHitActor;
   local WeldFire fWF;
   local ArmorWelderFireFalk fAWF;

   Super.Tick(dt);

   if (FireMode[0].bIsFiring)
   {
      FireModeArray = 0;
      fAWF = ArmorWelderFireFalk(FireMode[FireModeArray]);
   }

   else if (FireMode[1].bIsFiring)
   {
      FireModeArray = 1;
      fWF = WeldFire(FireMode[FireModeArray]);
   }

   else
      bJustStarted = true;

   if ((fWF != none && fWF.LastHitActor != none)  ||
         (fAWF != none && (fAWF.CachedHealee != none ||
                           (fAWF.LastHitActor != none ))))
   {
      bNoTarget = false;

      if (fWF != none)
      {
         LastDoorHitActor = KFDoorMover(fWF.LastHitActor);

         if (LastDoorHitActor != none)
            ScreenWeldPercent = (LastDoorHitActor.WeldStrength / LastDoorHitActor.MaxWeld) * 100;
      }

      else if (fAWF != none)
      {
         LastDoorHitActor = KFDoorMover(fAWF.LastHitActor);

         if (LastDoorHitActor != none)
            ScreenWeldPercent = (LastDoorHitActor.WeldStrength / LastDoorHitActor.MaxWeld) * 100;

         else if (fAWF.CachedHealee != none)
            ScreenWeldPercent = fAWF.CachedHealee.ShieldStrength;
      }

      if (ScriptedScreen == None)
         InitMaterials();

      ScriptedScreen.Revision++;

      if (ScriptedScreen.Revision>10)
         ScriptedScreen.Revision = 1;

      if (FireMode[1].bIsFiring && Level.Game != none && Level.Game.NumPlayers > 1 && bJustStarted && Level.TimeSeconds - LastWeldingMessageTime > WeldingMessageDelay)
      {
         bJustStarted = false;
         LastWeldingMessageTime = Level.TimeSeconds;

         if (Instigator != none && Instigator.Controller != none && PlayerController(Instigator.Controller) != none)
            PlayerController(Instigator.Controller).Speech('AUTO', 1, "");
      }
   }

   else 
   {
      ScreenWeldPercent = 0;

      if (ScriptedScreen==None)
         InitMaterials();

      ScriptedScreen.Revision++;

      if (ScriptedScreen.Revision > 10)
         ScriptedScreen.Revision = 1;

      bNoTarget = true;

      /*if (ClientState != WS_Hidden && Level.NetMode != NM_DedicatedServer && Instigator != none && Instigator.IsLocallyControlled())
        PlayIdle();*/
   }

   if (AmmoAmount(0) < FireMode[0].AmmoClass.Default.MaxAmmo)
   {
      AmmoRegenCount += (dT * AmmoRegenRate);
      ConsumeAmmo(0, -1*(int(AmmoRegenCount)));
      AmmoRegenCount -= int(AmmoRegenCount);
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
   local int Mode;

   Instigator = Pawn(Owner);

   bPendingSwitch = bPossiblySwitch;

   if( Instigator == None )
   {
      GotoState('PendingClientWeaponSet');
      return;
   }

   for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
   {
      if( FireModeClass[Mode] != None )
      {
         // laurent -- added check for vehicles (ammo not replicated but unlimited)
         if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
         {
            GotoState('PendingClientWeaponSet');
            return;
         }
      }

      FireMode[Mode].Instigator = Instigator;
      FireMode[Mode].Level = Level;
   }

   ClientState = WS_Hidden;
   GotoState('Hidden');

   if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
      return;

   if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
   {
      if (Instigator.PendingWeapon != None)
         Instigator.ChangedWeapon();
      else
         BringUp();
      return;
   }

   if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
      return;

   if ( Instigator.Weapon == None)
   {
      Instigator.PendingWeapon = self;
      Instigator.ChangedWeapon();
   }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

function byte BestMode()
{
   return 1;
}

simulated function Destroyed()
{
   Super.Destroyed();
   if( ScriptedScreen!=None )
   {
      ScriptedScreen.SetSize(256,256);
      ScriptedScreen.FallBackMaterial = None;
      ScriptedScreen.Client = None;
      Level.ObjectPool.FreeObject(ScriptedScreen);
      ScriptedScreen = None;
   }
   if( ShadedScreen!=None )
   {
      ShadedScreen.Diffuse = None;
      ShadedScreen.Opacity = None;
      ShadedScreen.SelfIllumination = None;
      ShadedScreen.SelfIlluminationMask = None;
      Level.ObjectPool.FreeObject(ShadedScreen);
      ShadedScreen = None;
      skins[3] = None;
   }
}

// Destroy this stuff when the level changes
simulated function PreTravelCleanUp()
{
   if( ScriptedScreen!=None )
   {
      ScriptedScreen.SetSize(256,256);
      ScriptedScreen.FallBackMaterial = None;
      ScriptedScreen.Client = None;
      Level.ObjectPool.FreeObject(ScriptedScreen);
      ScriptedScreen = None;
   }

   if( ShadedScreen!=None )
   {
      ShadedScreen.Diffuse = None;
      ShadedScreen.Opacity = None;
      ShadedScreen.SelfIllumination = None;
      ShadedScreen.SelfIlluminationMask = None;
      Level.ObjectPool.FreeObject(ShadedScreen);
      ShadedScreen = None;
      skins[3] = None;
   }
}

simulated function InitMaterials()
{
   if( ScriptedScreen==None )
   {
      ScriptedScreen = ScriptedTexture(Level.ObjectPool.AllocateObject(class'ScriptedTexture'));
      ScriptedScreen.SetSize(256,256);
      ScriptedScreen.FallBackMaterial = ScriptedScreenBack;
      ScriptedScreen.Client = Self;
   }

   if( ShadedScreen==None )
   {
      ShadedScreen = Shader(Level.ObjectPool.AllocateObject(class'Shader'));
      ShadedScreen.Diffuse = ScriptedScreen;
      ShadedScreen.SelfIllumination = ScriptedScreen;
      skins[3] = ShadedScreen;
   }
}


simulated function float ChargeBar()
{
   return FMin(1, (AmmoAmount(0))/(FireMode[0].AmmoClass.Default.MaxAmmo));
}


simulated event RenderTexture(ScriptedTexture Tex)
{
   local int SizeX,  SizeY;

   Tex.DrawTile(0,0,Tex.USize,Tex.VSize,0,0,256,256,Texture'KillingFloorWeapons.Welder.WelderScreen',BackColor);   // Draws the tile background

   if(!bNoTarget && ScreenWeldPercent > 0 )
   {
      // Err for now go with a name in black letters
      NameColor.R=(255 - (ScreenWeldPercent * 2));
      NameColor.G=(0 + (ScreenWeldPercent * 2.55));
      NameColor.B=(20 + ScreenWeldPercent);
      NameColor.A=255;
      Tex.TextSize(ScreenWeldPercent@"%",NameFont,SizeX,SizeY); // get the size of the players name
      Tex.DrawText( (Tex.USize - SizeX) * 0.5, 85,ScreenWeldPercent@"%", NameFont, NameColor);
      Tex.TextSize("Integrity:",NameFont,SizeX,SizeY);
      Tex.DrawText( (Tex.USize - SizeX) * 0.5, 50,"Integrity:", NameFont, NameColor);
   }
   else
   {
      NameColor.R=255;
      NameColor.G=255;
      NameColor.B=255;
      NameColor.A=255;
      Tex.TextSize("-",NameFont,SizeX,SizeY); // get the size of the players name
      Tex.DrawText( (Tex.USize - SizeX) * 0.5, 85,"-", NameFont, NameColor);
      Tex.TextSize("Integrity:",NameFont,SizeX,SizeY);
      Tex.DrawText( (Tex.USize - SizeX) * 0.5, 50,"Integrity:", NameFont, NameColor);
   }
}



simulated function PostBeginPlay()
{
   Super.PostBeginPlay();
   bNoTarget =  true;
   /*if( Level.NetMode==NM_DedicatedServer )
     Return;*/
}

// attempt to fix the animation in a less retard fashion
function ServerStopFire(byte Mode)
{
   Super.ServerStopFire(Mode);

   FalkClientFixIdle();
}

// second attempt to fix the animation
simulated function FalkClientFixIdle()
{
   PlayIdle();
}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }

   // we failed to drop this weapon on death, award us some cash instead
   else if (Instigator.Health <= 0 && Instigator.PlayerReplicationInfo != none)
   {
      Instigator.PlayerReplicationInfo.Score += SellValue;
      
      if (FHumanPawn(Instigator) != none)
         FHumanPawn(Instigator).FalkSetDosh();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}
// just don't
simulated function DoAutoSwitch(){}

defaultproperties
{
   AmmoRegenRate=40.000000
   ScriptedScreenBack=FinalBlend'KillingFloorWeapons.Welder.WelderWindowFinal'
   NameFont=Font'ROFonts.ROBtsrmVr24'
   SmallNameFont=Font'ROFonts.ROBtsrmVr12'
   BackColor=(B=128,G=128,R=128,A=255)
   WeldingMessageDelay=10.000000
   weaponRange=90.000000
   HudImage=Texture'KillingFloorHUD.WeaponSelect.welder_unselected'
   SelectedHudImage=Texture'KillingFloorHUD.WeaponSelect.Welder'
   Weight=0.000000
   bAmmoHUDAsBar=True
   bConsumesPhysicalAmmo=False
   StandardDisplayFOV=75.000000
   SleeveNum=2
   AIRating=-2.000000
   bMeleeWeapon=False
   bShowChargingBar=True
   EffectOffset=(X=100.000000,Y=25.000000,Z=-10.000000)
   DisplayFOV=75.000000
   Priority=15
   InventoryGroup=5
   GroupOffset=1
   BobDamping=6.000000
   AttachmentClass=Class'KFMod.WelderAttachment'
   IconCoords=(X1=169,Y1=39,X2=241,Y2=77)
   Skins(0)=Combiner'KF_Weapons_Trip_T.equipment.welder_cmb'
   Skins(1)=Shader'KillingFloorWeapons.Welder.FlameShader'
   AmbientGlow=2
   PutDownAnim="DioPutDown"
   SelectAnim="DioSelect"
   PutDownAnimRate=1.5
   SelectAnimRate=1.5
   BringUpTime=0.28000
   PutDownTime=0.28000
   FireModeClass(0)=Class'ArmorWelderFireFalk'
   FireModeClass(1)=Class'ArmorWelderAltFireFalk'
   ItemName="Welder"
   bKFNeverThrow=False
   PickupClass=Class'ArmorWelderPickupFalk'
   Description="This welder can be used to weld doors. However, supports seem to have attended several classes for a sewing and cutting specialization by Aldridge, making them able to repair armors."
   TraderInfoTexture=Texture'LairTextures_T.equipment.Welder'
   Mesh=SkeletalMesh'LairAnimations_A.WelderMesh'
   //MeshRef="LairAnimations_A.WelderMesh"
   fMaxTry=3
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
