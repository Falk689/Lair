class M32AmmoPickupFalk extends KFMod.M32AmmoPickup;

defaultproperties
{
    AmmoAmount=3
    InventoryType=Class'M32AmmoFalk'
    PickupMessage="You found some 40x46mm grenades"
}