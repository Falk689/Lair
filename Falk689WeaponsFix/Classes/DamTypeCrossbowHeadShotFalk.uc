class DamTypeCrossbowHeadShotFalk extends KFMod.DamTypeCrossbowHeadShot;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FSharpshooterDamage', Amount);
}

defaultproperties
{
   HeadShotDamageMult=2.2500000      // heavy sharpshooter weaponry standard
   WeaponClass=Class'CrossbowFalk'
}