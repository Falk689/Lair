class Slayer_AUGPickupFalk extends Slayer_AUG.Slayer_AUGPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=2400
   InventoryType=Class'Slayer_AUGFalk'
   ItemName="Steyr Mannlicher AUG A3"
   ItemShortName="AUG A3"
   PickupMessage="You got a Steyr Mannlicher AUG A3"
   Weight=6.000000
   BuyClipSize=28
   AmmoCost=14
   PowerValue=44
   SpeedValue=45
   RangeValue=100
}