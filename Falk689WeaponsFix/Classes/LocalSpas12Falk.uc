class LocalSpas12Falk extends LocalSpas12BaseFalk;

var float fReloadSpeedFix;  // reload time speed fix
var float fReloadSpeedFixT; // reload time speed fix

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction


// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
   local int Mode;

   Instigator = Pawn(Owner);

   bPendingSwitch = bPossiblySwitch;

   if( Instigator == None )
   {
      GotoState('PendingClientWeaponSet');
      return;
   }

   for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
   {
      if( FireModeClass[Mode] != None )
      {
         // laurent -- added check for vehicles (ammo not replicated but unlimited)
         if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
         {
            GotoState('PendingClientWeaponSet');
            return;
         }
      }

      FireMode[Mode].Instigator = Instigator;
      FireMode[Mode].Level = Level;
   }

   ClientState = WS_Hidden;
   GotoState('Hidden');

   if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
      return;

   if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
   {
      if (Instigator.PendingWeapon != None)
         Instigator.ChangedWeapon();
      else
         BringUp();
      return;
   }

   if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
      return;

   if ( Instigator.Weapon == None)
   {
      Instigator.PendingWeapon = self;
      Instigator.ChangedWeapon();
   }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// attempt to see the whole fockin reload animation in server
exec function ReloadMeNow()
{
   if (MagAmmoRemaining <= 0)
      fShouldBreechReload = True;
   
   Super.ReloadMeNow();
}

// attempt to see the whole fockin reload animation
simulated function ClientReload()
{
   if (MagAmmoRemaining <= 0)
      fShouldBreechReload = True;

   ReloadRate = Default.ReloadRate;

   Super.ClientReload();
}

// second attempt to see the whole fockin reload animation
simulated function AddReloadedAmmo()
{
   if (AmmoAmount(0) > 0)
      ++MagAmmoRemaining;

   if (MagAmmoRemaining <= 5)
      ReloadRate -= fReloadSpeedFix;

   else if (MagAmmoRemaining <= 7)
      ReloadRate += fReloadSpeedFixT;

   // last bullet to load, don't interrupt or drop
   if (fShouldBreechReload && AmmoAmount(0) > MagCapacity - 1 && MagAmmoRemaining == MagCapacity - 1)
   {
      ReloadRate         = fLastBulletRate;
      bCanThrow          = False;
      fLoadingLastBullet = True;

   }

   // restore vanilla stuff
   else if (MagAmmoRemaining == MagCapacity)
   {
      bCanThrow           = True;
      fShouldBreechReload = False;
      fLoadingLastBullet  = False;
   }
}

// third attempt to see the whole fockin reload
simulated function bool InterruptReload()
{
   if (fLoadingLastBullet)
      return False;

   if (Super.InterruptReload())
   {
      fShouldBreechReload = False;
      bCanThrow           = True;

      return True;
   }

   return False;
}

// attempt to fix more and more weird shit happening with the reload
simulated function ClientInterruptReload()
{
   if (fLoadingLastBullet)
      return;

   fShouldBreechReload = False;
   bCanThrow           = True;
   Super.ClientInterruptReload();
}

// more attempt to fix that reload on server
simulated function ServerInterruptReload()
{
   if (fLoadingLastBullet)
      return;

   fShouldBreechReload = False;
   bCanThrow           = True;
   Super.ServerInterruptReload();
}

// permit throwing while reloading, blocking it during the last bullet
simulated function bool CanThrow()
{
   if (bIsReloading && !fLoadingLastBullet)
   {
      InterruptReload();
      return True;
   }

   return Super.CanThrow();
}

// just don't
simulated function DoAutoSwitch(){}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }

   // we failed to drop this weapon on death, award us some cash instead
   else if (Instigator.Health <= 0 && Instigator.PlayerReplicationInfo != none)
   {
      Instigator.PlayerReplicationInfo.Score += SellValue;
      
      if (FHumanPawn(Instigator) != none)
         FHumanPawn(Instigator).FalkSetDosh();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

defaultproperties
{
   ModeSwitchAnim="LightOn"
   FireModeClass(0)=Class'Spas12FireFalk'
   Weight=7.000000
   ItemName="Franchi SPAS12 Shotgun"
   PickupClass=Class'LocalSpas12PickupFalk'
   Description="The SPAS12 is a combat shotgun manufactured by italian firearms company Franchi from 1979 to 2000. While supposed to be a bimodal weapon, the semi-auto feature was taken off in favour of a tactical extra durable flashlight. High damage output, but scarce ammo efficiency. Wider spread than a Benelli M3."
   ReloadRate=0.72
   fLastBulletRate=1.7
   fReloadSpeedFix=0.045
   fReloadSpeedFixT=0.02
   fMaxTry=3
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
