class SPGrenadeFireFalk extends M79FireFalk;

var byte BID;
//var   bool  bVeryLastShotAnim;

// set weapon to pending reload state
event ModeDoFire()
{
   if (!AllowFire())
      return;

   //bVeryLastShotAnim = Weapon.AmmoAmount(0) <= AmmoPerFire;

   super.ModeDoFire();

   if (Weapon != None && Weapon.Role == ROLE_Authority)
      SPGrenadeLauncherFalk(Weapon).SetPendingReload();
}

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID += 4;

   if (BID > 200)
      BID = BID - 200;

   class<SPGrenadeProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super(M79Fire).SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local SPGrenadeProjectileFalk FG;
   local KFPlayerReplicationInfo KFPRI;

   FG = SPGrenadeProjectileFalk(P);

   if (FG != none && Instigator != none)
      FG.fCInstigator = Instigator.Controller;

   KFPRI       = KFPlayerReplicationInfo(Weapon.Instigator.PlayerReplicationInfo);

   if (KFPRI.ClientVeteranSkill != none)
   {
      P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Weapon.Instigator), P.Damage, Class'KFMod.DamTypeSPGrenade');
   }

   Super(M79Fire).PostSpawnProjectile(P);
}

// kind of allow fire only in proper situations?
simulated function bool AllowFire()
{
   if (KFWeapon(Weapon).bIsReloading)
      return false;

   if(KFPawn(Instigator).SecondaryItem!=none)
      return false;

   if (KFPawn(Instigator).bThrowingNade)
      return false;

   if (Level.TimeSeconds - LastClickTime>FireRate)
      LastClickTime = Level.TimeSeconds;

   if (KFWeapon(Weapon).MagAmmoRemaining < 1)
      return false;

   return super(WeaponFire).AllowFire();
}

defaultproperties
{
     EffectiveRange=2500.000000
     maxVerticalRecoilAngle=200
     maxHorizontalRecoilAngle=50
     FireAimedAnim="Iron_Fire"
     FireSoundRef="KF_SP_OrcaSnd.KFO_Orca_Fire_M"
     StereoFireSoundRef="KF_SP_OrcaSnd.KFO_Orca_Fire_S"
     NoAmmoSoundRef="KF_M79Snd.M79_DryFire"
     ProjPerFire=1
     ProjSpawnOffset=(X=50.000000,Y=10.000000)
     bWaitForRelease=True
     TransientSoundVolume=1.800000
     FireForce="AssaultRifleFire"
     FireRate=0.34
     AmmoClass=Class'SPGrenadeAmmoFalk'
     ShakeRotMag=(X=3.000000,Y=4.000000,Z=2.000000)
     ShakeRotRate=(X=10000.000000,Y=10000.000000,Z=10000.000000)
     ShakeOffsetMag=(X=3.000000,Y=3.000000,Z=3.000000)
     ProjectileClass=Class'SPGrenadeProjectileFalk'
     BotRefireRate=1.800000
     FlashEmitterClass=Class'ROEffects.MuzzleFlash1stSPGrenade'
     aimerror=42.000000
     Spread=0.015000
}

