class FlameNadeFalk extends KFMod.FlameNade;

#exec OBJ LOAD FILE=KF_GrenadeSnd.uax
#exec OBJ LOAD FILE=LairTextures_T.utx

var bool       fTimerSet;            // safety to prevent setting the timer twice
var byte       BulletID;             // bullet ID, passed at takedamage to prevent multiple shots
var Controller fCInstigator;         // instigator controller, I don't use vanilla InstigatorController 'cause of reasons

replication
{
    reliable if (Role==ROLE_Authority)
        fTimerSet, fCInstigator;
}

// Siren override
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
}

// Post death kill fix 
simulated function PostBeginPlay()
{
   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   Super.PostBeginPlay();
}

// Initial timer setting
function PostNetBeginPlay()
{
	SetTimer(ExplodeTimer, false);
   fTimerSet = true;

   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;
}

// prevent setting ExplodeTimer twice
simulated function HitWall(vector HitNormal, actor Wall)
{
   local Vector VNorm;
   local PlayerController PC;

   // hopefully shatter glasses more reliably
   if (KFPawn(Wall) == none && KFPawn(Wall.Base) == none && KFDoorMover(Wall) == none && KFDoorMover(Wall.Base) == none)
      Wall.TakeDamage(100, Instigator, Location, HitNormal, class'DamTypeFragImpactFalk');

   if ((Pawn(Wall) != None) || (GameObjective(Wall) != None))
   {
      Explode(Location, HitNormal);
      return;
   }

   if (!fTimerSet)
   {
      SetTimer(ExplodeTimer * 0.8, false);
      fTimerSet = true;
   }

   // Reflect off Wall w/damping
   VNorm = (Velocity dot HitNormal) * HitNormal;
   Velocity = -VNorm * DampenFactor + (Velocity - VNorm) * DampenFactorParallel;

   RandSpin(50000);
   DesiredRotation.Roll = 0;
   RotationRate.Roll = 0;
   Speed = VSize(Velocity);

   if (Speed < 20)
   {
      bBounce = False;
      PrePivot.Z = -1.5;
      SetPhysics(PHYS_None);
      DesiredRotation = Rotation;
      DesiredRotation.Roll = 0;
      DesiredRotation.Pitch = 0;
      SetRotation(DesiredRotation);

      if (Trail != None)
         Trail.mRegen = false; // stop the emitter from regenerating
   }

   else
   {
      if ((Level.NetMode != NM_DedicatedServer) && (Speed > 50))
         PlaySound(ImpactSound, SLOT_Misc);

      else
      {
         bFixedRotationDir = false;
         bRotateToDesired = true;
         DesiredRotation.Pitch = 0;
         RotationRate.Pitch = 50000;
      }

      if (!Level.bDropDetail && (Level.DetailMode != DM_Low) && (Level.TimeSeconds - LastSparkTime > 0.5) &&
         EffectIsRelevant(Location,false))
      {
         PC = Level.GetLocalPlayerController();

         if ((PC.ViewTarget != None) && VSize(PC.ViewTarget.Location - Location) < 6000)
            Spawn(HitEffectClass,,, Location, Rotator(HitNormal));

         LastSparkTime = Level.TimeSeconds;
      }
   }
}

// use BulletID to damage zeds
simulated function HurtRadius( float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation )
{
   local actor Victims;
   local float damageScale, dist;
   local vector dirs;
   local int NumKilled;
   local KFMonster KFMonsterVictim;
   local Pawn P;
   local KFPawn KFP;
   local array<Pawn> CheckedPawns;
   local int i;
   local bool bAlreadyChecked;


   if ( bHurtEntry )
      return;

   bHurtEntry = true;

   foreach CollidingActors (class 'Actor', Victims, DamageRadius, HitLocation)
   {
      // don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
      if( (Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo')
            && ExtendedZCollision(Victims)==None )
      {
         dirs = Victims.Location - HitLocation;
         dist = FMax(1,VSize(dirs));
         dirs = dirs/dist;
         damageScale = 1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius);
         Victims.SetDelayedDamageInstigatorController(fCInstigator);

         if ( Victims == LastTouched )
            LastTouched = None;

         P = Pawn(Victims);

         if( P != none )
         {
            for (i = 0; i < CheckedPawns.Length; i++)
            {
               if (CheckedPawns[i] == P)
               {
                  bAlreadyChecked = true;
                  break;
               }
            }

            if( bAlreadyChecked )
            {
               bAlreadyChecked = false;
               P = none;
               continue;
            }

            KFMonsterVictim = KFMonster(Victims);

            if( KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
            {
               KFMonsterVictim = none;
            }

            KFP = KFPawn(Victims);

            if( KFMonsterVictim != none )
            {
               damageScale *= KFMonsterVictim.GetExposureTo(HitLocation/*Location + 15 * -Normal(PhysicsVolume.Gravity)*/);
            }
            else if( KFP != none )
            {
               damageScale *= KFP.GetExposureTo(HitLocation/*Location + 15 * -Normal(PhysicsVolume.Gravity)*/);
            }

            CheckedPawns[CheckedPawns.Length] = P;

            if ( damageScale <= 0)
            {
               P = none;
               continue;
            }
            else
            {
               //Victims = P;
               P = none;
            }
         }

         if(Victims == Instigator)
            damageScale *= 0.3;

         Victims.TakeDamage
            (
             damageScale * DamageAmount,
             Instigator,
             Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs,
             (damageScale * Momentum * dirs),
             DamageType,
             BulletID
            );
         if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
            Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);

         if( Role == ROLE_Authority && KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
         {
            NumKilled++;
         }
      }
   }
   if ( (LastTouched != None) && (LastTouched != self) && (LastTouched.Role == ROLE_Authority) && !LastTouched.IsA('FluidSurfaceInfo') )
   {
      Victims = LastTouched;
      LastTouched = None;
      dirs = Victims.Location - HitLocation;
      dist = FMax(1,VSize(dirs));
      dirs = dirs/dist;
      damageScale = FMax(Victims.CollisionRadius/(Victims.CollisionRadius + Victims.CollisionHeight),1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius));

      Victims.SetDelayedDamageInstigatorController(fCInstigator);

      if(Victims == Instigator)
         damageScale *= 0.3;

      Victims.TakeDamage
         (
          damageScale * DamageAmount,
          Instigator,
          Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs,
          (damageScale * Momentum * dirs),
          DamageType,
          BulletID
         );
      if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
         Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);
   }

   bHurtEntry = false;
}

// Touch only if we haven't exploded yet and don't hit broken zeds heads
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{
   local FalkMonsterBase P;
   local float fHeadMulti;
   local vector X;

   X = Vector(Rotation);

   // hopefully shatter glasses more reliably
   if (KFPawn(Other) == none && KFPawn(Other.Base) == none && KFDoorMover(Other) == none && KFDoorMover(Other.Base) == none)
      Other.TakeDamage(100, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), class'DamTypeFragImpactFalk');

   if (!bHasExploded)
   {
      P = FalkMonsterBase(Other);

      if (P != none)
      {
         fHeadMulti = P.FalkGetNadeHeadScale();

         if (fHeadMulti <= 0 || !P.IsHeadShot(HitLocation, X, fHeadMulti))
            Super.ProcessTouch(Other, HitLocation);
      }
   }
}

defaultproperties
{
   Speed=850.0
	Damage=50.000000
	MyDamageType=Class'DamTypeFlameNadeFalk'
	StaticMesh=StaticMesh'LairStaticMeshes_SM.Grenades.FirebugGrenade'
	ExplodeTimer=1.500000       // was 1.4 in patch 13
   ExplosionDecal=Class'BurnMarkLargeFalk'
}