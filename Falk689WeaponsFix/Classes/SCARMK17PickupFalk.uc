class SCARMK17PickupFalk extends KFMod.SCARMK17Pickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
	Weight=5.000000
	cost=3100
	AmmoCost=10
	BuyClipSize=20
	PowerValue=65
    SpeedValue=50
    RangeValue=100
	ItemName="FN SCAR-H Combat Rifle"
	ItemShortName="SCAR-H"
	AmmoItemName="7.62x51mm rounds"
	InventoryType=Class'SCARMK17AssaultRifleFalk'
	PickupMessage="You got an FN SCAR-H Combat Rifle"
	PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}