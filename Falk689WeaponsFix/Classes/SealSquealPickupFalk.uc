class SealSquealPickupFalk extends KFMod.SealSquealPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   InventoryType=Class'SealSquealHarpoonBomberFalk'
   ItemName="Seal Squeal Harpoon Bomber"
   ItemShortName="Harpoon Bomber"
   PickupMessage="You got a Seal Squeal Harpoon Bomber"
   BuyClipSize=3
   AmmoCost=15
   PowerValue=64
   SpeedValue=65
   RangeValue=100
   Weight=6.000000
   cost=2000
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}