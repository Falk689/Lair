class SeekerSixPickupFalk extends KFMod.SeekerSixPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   InventoryType=Class'SeekerSixRocketLauncherFalk'
   ItemName="Seeker Six Minirocket Launcher"
   ItemShortName="Seeker Six"
   PickupMessage="You got a Seeker Six Minirocket Launcher"
   cost=2300
   Weight=6.000000
   BuyClipSize=6
   AmmoCost=18
   PowerValue=27
   SpeedValue=80
   RangeValue=100
   DrawScale=1.100000
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
