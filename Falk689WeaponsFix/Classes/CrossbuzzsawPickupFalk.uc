class CrossbuzzsawPickupFalk extends KFMod.CrossbuzzsawPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=9.000000
   InventoryType=Class'CrossbuzzsawFalk'
   ItemName="Cheetah X-T012 Buzzsaw Bow"
   ItemShortName="Cheetah"
   PickupMessage="You got a Cheetah X-T012 Buzzsaw Bow"
   cost=2450
   AmmoCost=10
   BuyClipSize=1
   PowerValue=100
   SpeedValue=10
   RangeValue=95
   DrawScale=1.15000
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
