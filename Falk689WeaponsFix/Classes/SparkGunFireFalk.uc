class SparkGunFireFalk extends MedicBrowningAltFireFalk;

// kind of don't do shit in zed time pls
function float GetFireSpeed()
{
	return 1.0;
}

// Added zed time support skills
function DoFireEffect()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes>   Vet;

   KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

   if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
   {
      Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

      if (Vet != none)
      {
         ProjPerFire = Vet.Static.GetShotgunProjPerFire(KFPRI, Default.ProjPerFire, Weapon);
         Spread      = 1.0 * Vet.Static.GetShotgunSpreadMultiplier(KFPRI, Weapon);

         if (Spread <= 1.0)
            Spread = Default.Spread;
      }
   }

   Super.DoFireEffect();
}


// using my ammo var
simulated function bool AllowFire()
{
	if(KFWeapon(Weapon).bIsReloading)
		return false;
	if(KFPawn(Instigator).SecondaryItem!=none)
		return false;
	if(KFPawn(Instigator).bThrowingNade)
		return false;

	return SparkGunFalk(Weapon).fHealingAmmoFalk >= AmmoPerFire;
}

defaultproperties
{
   AmmoPerFire=1750
   FlashEmitterClass=Class'ROEffects.MuzzleFlash1stSPSniper'
   ProjectileClass=Class'SparkGunProjectileFalk'
   FireRate=0.55
}
