class MK23AmmoFalk extends KFMod.MK23Ammo;

defaultproperties
{
   MaxAmmo=120
   InitialAmount=48
   AmmoPickupAmount=24
   PickupClass=Class'MK23AmmoPickupFalk'
   ItemName=".45 ACP rounds"
}
