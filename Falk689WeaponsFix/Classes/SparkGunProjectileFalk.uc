class SparkGunProjectileFalk extends MP7MHealingProjectileFalk;

var     xEmitter    Trail;

// added shotgun trail
simulated function PostBeginPlay()
{
	Super(HealingProjectile).PostBeginPlay();

    if (Level.NetMode != NM_DedicatedServer)
    {
        if (!PhysicsVolume.bWaterVolume)
        {
            Trail = Spawn(class'KFTracer',self);
            Trail.Lifespan = Lifespan;
        }
    }
}


// Do a healing effect/sound instead of standard "explode"
simulated function HitHealTarget(vector HitLocation, vector HitNormal)
{
   fHitHealTarget = true;
   bHidden        = true;
   SetPhysics(PHYS_None);

   /*if (Role == ROLE_Authority)
      PlaySound(ExplosionSound);*/

   HealLocation  = HitLocation;
   HealRotation  = rotator(HitNormal);
   NetUpdateTime = Level.TimeSeconds - 1;
}


// edited to change veterancy skill
simulated function HealRadius(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation, vector HitNormal)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;
   local actor                  Target;
   local byte                   healed;     // currently healed players
   local float                  healRadius; // heal radius perk modifier

   //warn("kaboom");

   healRadius = DamageRadius;
   healed     = 0;

   if (Instigator != none)
   {
      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

      if (KFPRI != none)
      {
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none)
            healRadius *= Vet.Static.GetSparkRadiusMod(KFPRI); 
      }
   }

   SetPhysics(PHYS_None);

   foreach VisibleCollidingActors(class'Actor', Target, healRadius, HitLocation)
   {
      if (KFHumanPawn(Target) != none && Target != Instigator)
      {
         bHurtEntry = false;
         //warn("healing: "@Target);

         if (HealOther(Target, HitLocation))
         {
            fAlreadyHit(Target, True); // add this pawn
            healed++;
         }

         if (healed >= fMaxHeal)
            break;
      }
   }

   if (healed > 0)
   {
      if (healed > 1)
         fHealedName = fMultiHealStr;

      if (SparkGunFalk(Instigator.Weapon) != none)
         SparkGunFalk(Instigator.Weapon).ClientSuccessfulHeal(fHealedName);
   }
}

// Return True if a pawn is healed
simulated function bool HealOther(Actor Other, Vector HitLocation)
{
   local KFPlayerReplicationInfo KFPRI;
   local int MedicReward;
   local FHumanPawn Healed; 
   local KFPlayerController PC;
   local Falk689GameTypeBase Flk;

   if (Other == none || Other == Instigator || Other.Base == Instigator || Role != ROLE_Authority)
      return False;

   if (fAlreadyHit(Other) >= 0) // we already healed this dude
   {
      //log("Already Healed: "@Other);
      return True;
   }

   Healed = FHumanPawn(Other);

   if (Instigator != none)
      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

   if (Healed != none)
   {
      if (Healed.GetPlayerName() == "") // only stuff with a name should be healed
      {
         //log("Failed");
         fFailedHeal = True;
         return False;
      }

      fFailedHeal = False;
      // base MedicReward
      MedicReward = HealBoostAmount;

      // healing and reward math
      if (Healed.fShield > 0 && Healed.fShield < Healed.fShieldMax)// && Healed.bCanBeHealed)
      {
         // healing visual effect, only the first time if we heal multiple targets
         if (!fHitHealTarget)
            HitHealTarget(HitLocation, -vector(Rotation));

         if ((Healed.fShield + MedicReward) > Healed.fShieldMax)
         {
            MedicReward = Healed.fShieldMax - Healed.fShield;

            if (MedicReward < 0)
               MedicReward = 0;
         }

         Healed.AddShieldStrength(MedicReward);

         if (KFPRI != None)
         {
            PC = KFPlayerController(Instigator.Controller);

            if (PC != none && SRStatsBase(PC.SteamStatsAndAchievements) != none) // perk progress
               SRStatsBase(PC.SteamStatsAndAchievements).Rep.ProgressCustomValue(Class'FArmorWeld', MedicReward);

            // weld reward block stuff
            if (PC != none)
            {
               Flk = Falk689GameTypeBase(Level.Game);

               if (Flk != none)
                  MedicReward = Flk.GetSupportReward(PC, MedicReward);
            }

            if (MedicReward > 0)
            {
               // difficulty scaling
               if (Level.Game.GameDifficulty >= 8.0) // Bloodbath
                  MedicReward = int(float(MedicReward) * 0.2);

               else if (Level.Game.GameDifficulty >= 7.0) // Hell on Earth
                  MedicReward = int(float(MedicReward) * 0.4);

               else if (Level.Game.GameDifficulty >= 5.0) // Suicidal
                  MedicReward = int(float(MedicReward) * 0.6);

               else if (Level.Game.GameDifficulty >= 4.0) // Hard
                  MedicReward = int(float(MedicReward) * 0.8);


               KFPRI.ReceiveRewardForHealing(MedicReward, Healed);
            }

            fHealedName = Healed.GetPlayerName();
         }

         return True;
      }

      return False;
   }

   return False;
}

// added more heals attempt to fix weird shit happening sometimes
simulated function Tick(float DeltaTime)
{
   Super(HealingProjectile).Tick(DeltaTime);

   if (Role == ROLE_Authority)
   {
      if (fFailedHeal && fTry < fMaxTry)
      {
         fTry++;
         //log("Retry: "@fTry);
         bHasExploded = False;
         Explode(fHitLocation, fHitNormal);
      }

      else if (!fDestroyed && (fDestroy || fTry >= fMaxTry))
      {
         fDestroy = False;
         SetTimer(0.2, false);
      }

      if (bHasExploded && (!fFailedHeal || fTry >= fMaxTry))
         fShouldSpawn = True;
   }

   if ((Level.NetMode == NM_Client || (Level.NetMode == NM_ListenServer && Role == ROLE_Authority)) && fShouldSpawn && !fEffectSpawned)
   {
      fEffectSpawned = True;

      if (fHitHealTarget)
         Spawn(Class'SparkGunHitEffectFalk',,, fHitLocation, rotator(fHitNormal));

      else
		   Spawn(class'ROBulletHitEffect',,, fHitLocation, rotator(-fHitNormal));

      //log("spawn");
   }
}

// remove the trail
simulated function Destroyed()
{
	if (Trail !=None)
      Trail.mRegen = False;
   
   Super.Destroyed();
}

defaultproperties
{
   fMaxHeal=11
   HealBoostAmount=5
   fMultiHealStr="multiple targets"
}
