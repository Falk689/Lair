class MAC10FireFalk extends KFMod.MAC10Fire;

var byte  BID;                // bullet ID to prevent multiple hits on a single zed

// multi-hit, pawn penetration and invisible head fix for normal hitscan weapons
function DoTrace(Vector Start, Rotator Dir)
{
   local Vector           X,Y,Z, End, HitLocation, HitNormal, ArcEnd;
   local Actor            Other, DamageActor, OldDamageActor;
   local array<int>       HitPoints;
   local int              bHP;
   local byte             Retries;

   BID++;

   if (BID > 200)
      BID = 1;

   MaxRange();
   DamageType = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.static.GetMAC10DamageType(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo));

   Weapon.GetViewAxes(X, Y, Z);

   if (Weapon.WeaponCentered())
      ArcEnd = (Instigator.Location + Weapon.EffectOffset.X * X + 1.5 * Weapon.EffectOffset.Z * Z);

   else
   {
      ArcEnd = (Instigator.Location + Instigator.CalcDrawOffset(Weapon) + Weapon.EffectOffset.X * X +
            Weapon.Hand * Weapon.EffectOffset.Y * Y + Weapon.EffectOffset.Z * Z);
   }

   X = Vector(Dir);
   End = Start + TraceRange * X;

   while (Retries < 100)
   {
      Retries    += 1;
      DamageActor = none;

      Other = Instigator.HitPointTrace(HitLocation, HitNormal, End, HitPoints, Start,, 1);

      if (Other == None)
      {
         start = HitLocation;
         continue;
      }

      if (Other == Instigator || KFHumanPawn(Other) != None || Other.Base == Instigator || KFHumanPawn(Other.Base) != None)
      {
         start = HitLocation + X;
         continue;
      }

      if (ExtendedZCollision(Other) != None && Other.Owner != None)
         Other = Pawn(Other.Owner);

      if (!Other.bWorldGeometry && Other != Level)
      {
         if (KFMonster(Other) != None)
            DamageActor = Other;

         if (KFMonster(DamageActor) != None)
         {
            if (DamageActor == OldDamageActor)
            {
               start = HitLocation + X;
               continue;
            }

            OldDamageActor = DamageActor;
            bHP            = KFMonster(DamageActor).Health;

            if (bHP <= 0)
            {
               start = HitLocation + X;
               continue;
            }

            DamageActor.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);

            if (KFMonster(DamageActor).Health == bHP)
            {
               start = HitLocation + X;
               continue;
            }

            return;
         }

         else
            Other.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);
      }

      else if (BlockingVolume(Other) == None)
      {
         if(KFWeaponAttachment(Weapon.ThirdPersonActor) != None)
            KFWeaponAttachment(Weapon.ThirdPersonActor).UpdateHit(Other, HitLocation, HitNormal);

         break;
      }
   }
}

defaultproperties
{
   DamageMin=21
   DamageMax=21
   AmmoClass=Class'MAC10AmmoFalk'
   FireRate=0.052000
   RecoilRate=0.05000
   aimerror=42.000000
}
