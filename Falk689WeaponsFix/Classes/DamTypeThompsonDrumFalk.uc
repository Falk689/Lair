class DamTypeThompsonDrumFalk extends KFMod.DamTypeThompsonDrum;

defaultproperties
{
    WeaponClass=Class'ThompsonDrumSMGFalk'
    DeathString="%k killed %o (M1928A1)"
	HeadShotDamageMult=1.100000
}