class DamTypeMAC10MPIncFalk extends KFProjectileWeaponDamageType;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount)
{
    KFStatsAndAchievements.AddFlameThrowerDamage(Amount);
}

defaultproperties
{
   bDealBurningDamage=True
   HeadShotDamageMult=1.100000
   WeaponClass=Class'MAC10MPFalk'
   DeathString="%k killed %o (MAC-10)"
   bRagdollBullet=True
   KDamageImpulse=1850.000000
   KDeathVel=150.000000
   KDeathUpKick=5.000000
}