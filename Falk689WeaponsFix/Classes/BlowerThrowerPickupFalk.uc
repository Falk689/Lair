class BlowerThrowerPickupFalk extends KFMod.BlowerThrowerPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   InventoryType=Class'BlowerThrowerFalk'
   ItemName="Bloat Bile Launcher"
   ItemShortName="Bile Launcher"
   PickupMessage="You got a Bloat Bile Launcher"
   PowerValue=24
   SpeedValue=62
   RangeValue=50
   BuyClipSize=50
   AmmoCost=25
   cost=1250
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
