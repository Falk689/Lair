class ShotgunAmmoPickupFalk extends KFMod.ShotgunAmmoPickup;

defaultproperties
{
    AmmoAmount=8
    InventoryType=Class'ShotgunAmmoFalk'
    PickupMessage="You found some 12-gauge shells"
}