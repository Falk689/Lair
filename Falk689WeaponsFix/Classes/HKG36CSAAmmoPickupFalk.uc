class HKG36CSAAmmoPickupFalk extends HKG36CSA.HKG36CSAAmmoPickup;

defaultproperties
{
   AmmoAmount=30
   PickupMessage="You found some 5.56x45mm rounds"
   InventoryType=Class'HKG36CSAAmmoFalk'
}