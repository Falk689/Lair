class Glock18SAAmmoFalk extends KFAmmunition;

defaultproperties
{
     AmmoPickupAmount=34
     MaxAmmo=204
     InitialAmount=85
     PickupClass=Class'Glock18SAAmmoPickupFalk'
     IconMaterial=Texture'KillingFloorHUD.Generic.HUD'
     IconCoords=(X1=338,Y1=40,X2=393,Y2=79)
     ItemName="Glock18 bullets"
}
