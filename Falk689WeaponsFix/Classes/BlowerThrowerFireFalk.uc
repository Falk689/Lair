class BlowerThrowerFireFalk extends KFMod.BlowerThrowerFire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<BlowerThrowerProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local BlowerThrowerProjectileFalk FB;

   FB = BlowerThrowerProjectileFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

// wait for the alt fire to end before allowing fire
simulated function bool AllowFire()
{
    if (!BlowerThrowerFalk(Weapon).fCanMainFire())
        return False;

    if (KFWeapon(Weapon).bIsReloading)
        return false;

    if (KFPawn(Instigator).SecondaryItem != none)
        return false;

    if (KFPawn(Instigator).bThrowingNade)
        return false;

    return super.AllowFire();
}


defaultproperties
{
   AmmoClass=Class'BlowerThrowerAmmoFalk'
   ProjectileClass=Class'BlowerThrowerProjectileFalk'
   FireRate=0.07
}
