class CLGLFireFalk extends KFShotgunFire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID += 4;

   if (BID > 200)
      BID = BID - 200;

   class<M79GrenadeProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local M79GrenadeProjectileFalk FG;
   local KFPlayerReplicationInfo KFPRI;

   FG = M79GrenadeProjectileFalk(P);

   if (FG != none && Instigator != none)
      FG.fCInstigator = Instigator.Controller;

   KFPRI       = KFPlayerReplicationInfo(Weapon.Instigator.PlayerReplicationInfo);

   if (KFPRI.ClientVeteranSkill != none)
   {
      P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Weapon.Instigator), P.Damage, Class'KFMod.DamTypeM79Grenade');
   }

   Super.PostSpawnProjectile(P);
}

// Overrides KFShotgunFire to allow for lobbing
// shots when the sight is up and the user is aiming
function DoFireEffect()
{
    local Vector StartProj, HitLocation, HitNormal, StartTrace, X,Y,Z;
    local Rotator R, Aim;
    local Actor Other;
    local int p, SpawnCount;

	Instigator.MakeNoise(1.0);
	Weapon.GetViewAxes(X,Y,Z);

	StartTrace = Instigator.Location + Instigator.EyePosition();
	StartProj = StartTrace + X*ProjSpawnOffset.X;
	if ( !Weapon.WeaponCentered() && !KFWeap.bAimingRifle )
		StartProj = StartProj + Weapon.Hand * Y*ProjSpawnOffset.Y + Z*ProjSpawnOffset.Z;
	Other = Weapon.Trace(HitLocation, HitNormal, StartProj, StartTrace, false);
	
	if (Other != None)
	{
		StartProj = HitLocation;
	}

	Aim = AdjustAim(StartProj, AimError);

	if (KFWeap.bAimingRifle)
	{
		if (CLGLFalk(Weapon).SightUp == 1)
		{
			Aim.Pitch += 3000;
		}
		else
		{
			Aim.Pitch += 380;
		}
	}
	
	SpawnCount = Max(1, ProjPerFire * int(Load));

    switch (SpreadStyle)
    {
    case SS_Random:
        X = Vector(Aim);
        for (p = 0; p < SpawnCount; p++)
        {
            R.Yaw = Spread * (FRand()-0.5);
            R.Pitch = Spread * (FRand()-0.5);
            R.Roll = Spread * (FRand()-0.5);
			SpawnProjectile(StartProj, Rotator(X >> R));
        }
        break;
    }

	if (Instigator != none )
	{
		if( Instigator.Physics != PHYS_Falling  )
		{
			Instigator.AddVelocity(KickMomentum >> Instigator.GetViewRotation());
		}
		else if( Instigator.Physics == PHYS_Falling
			&& Instigator.PhysicsVolume.Gravity.Z > class'PhysicsVolume'.default.Gravity.Z)
		{
			Instigator.AddVelocity((KickMomentum * LowGravKickMomentumScale) >> Instigator.GetViewRotation());
		}
	}
}

simulated event ModeDoFire()
{
	if (CLGLFalk(Weapon).SightUp == 0)
	{
		if (CLGLFalk(Weapon).MagAmmoRemaining <= 1)
		{
			FireAnim = 'FireLast';
		}
		Else
		{
			FireAnim = default.FireAnim;
		}
	}
	else
	{
		if (CLGLFalk(Weapon).MagAmmoRemaining <= 1)
		{
			FireAnim = 'UPFireLast';
		}
		Else
		{
			FireAnim = 'UPFire';
		}
	}
	Super.ModeDoFire();
}

function float MaxRange()
{
    return 2500;
}

defaultproperties
{
     EffectiveRange=2500.000000
     maxVerticalRecoilAngle=200
     maxHorizontalRecoilAngle=50
     FireAimedAnim="Iron_Fire"
     StereoFireSound=SoundGroup'KF_M32Snd.M32_FireST'
     FireSoundRef="KF_M32Snd.M32_Fire"
     StereoFireSoundRef="KF_M32Snd.M32_FireST"
     NoAmmoSoundRef="KF_M79Snd.M79_DryFire"
     ProjPerFire=1
     ProjSpawnOffset=(X=50.000000,Y=10.000000)
     bWaitForRelease=True
     TransientSoundVolume=1.800000
     FireSound=SoundGroup'KF_M32Snd.M32_Fire'
     NoAmmoSound=Sound'KF_M79Snd.M79_DryFire'
     FireForce="AssaultRifleFire"
     FireRate=1.100000
     AmmoClass=Class'CLGLAmmoFalk'
     ShakeRotMag=(X=3.000000,Y=4.000000,Z=2.000000)
     ShakeRotRate=(X=10000.000000,Y=10000.000000,Z=10000.000000)
     ShakeOffsetMag=(X=3.000000,Y=3.000000,Z=3.000000)
     ProjectileClass=Class'CLGLGrenadeFalk'
     BotRefireRate=1.800000
     FlashEmitterClass=Class'ROEffects.MuzzleFlash1stNadeL'
     aimerror=42.000000
     Spread=0.015000
}
