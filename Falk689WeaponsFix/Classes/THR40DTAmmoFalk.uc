class THR40DTAmmoFalk extends THR40DT.THR40DTAmmo;

defaultproperties
{
    AmmoPickupAmount=20 // one quarter of magazine, heavy weapons standard
    MaxAmmo=320
    InitialAmount=160
    PickupClass=Class'THR40DTAmmoPickupFalk'
    ItemName="5.56mm NATO rounds"
}