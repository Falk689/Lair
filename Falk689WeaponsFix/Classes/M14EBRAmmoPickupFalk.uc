class M14EBRAmmoPickupFalk extends KFMod.M14EBRAmmoPickup;

defaultproperties
{
    AmmoAmount=20
    InventoryType=Class'M14EBRAmmoFalk'
    PickupMessage="You found some 7.62x51mm NATO rounds"
}