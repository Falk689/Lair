class M99FireFalk extends KFMod.KFFire;

var float PenDamageReduction; // multiplier to apply on damage when we hit a zed
var byte  MaxPenetrations;    // max amount of penetrations on zeds
var byte  BID;                // bullet ID to prevent multiple hits on a single zed

// kind of allow fire only in proper situations?
simulated function bool AllowFire()
{
   if (KFWeapon(Weapon).bIsReloading)
      return false;

   if (KFPawn(Instigator).SecondaryItem != none)
      return false;

   if (KFPawn(Instigator).bThrowingNade)
      return false;

   if (KFWeapon(Weapon).MagAmmoRemaining < 1)
   {
      if (Level.TimeSeconds - LastClickTime > FireRate)
         LastClickTime = Level.TimeSeconds;

      return false;
   }

   return super(WeaponFire).AllowFire();
}

// multi-hit, pawn penetration and invisible head fix hitscan weapons with penetration
function DoTrace(Vector Start, Rotator Dir)
{
   local Vector           X,Y,Z, End, HitLocation, HitNormal, ArcEnd;
   local Actor            Other, DamageActor, OldDamageActor;
   local array<int>       HitPoints;
   local int              bHP;
   local byte             Retries, HitCount;
   local float            HitDamage;

   BID++;

   if (BID > 200)
      BID = 1;

   MaxRange();

   Weapon.GetViewAxes(X, Y, Z);

   if (Weapon.WeaponCentered())
      ArcEnd = (Instigator.Location + Weapon.EffectOffset.X * X + 1.5 * Weapon.EffectOffset.Z * Z);

   else
   {
      ArcEnd = (Instigator.Location + Instigator.CalcDrawOffset(Weapon) + Weapon.EffectOffset.X * X +
            Weapon.Hand * Weapon.EffectOffset.Y * Y + Weapon.EffectOffset.Z * Z);
   }

   X = Vector(Dir);
   End = Start + TraceRange * X;

   while (Retries < 100)
   {
      Retries    += 1;
      DamageActor = none;

      Other = Instigator.HitPointTrace(HitLocation, HitNormal, End, HitPoints, Start,, 1);

      if (Other == None)
      {
         start = HitLocation;
         continue;
      }

      if (Other == Instigator || KFHumanPawn(Other) != None || Other.Base == Instigator || KFHumanPawn(Other.Base) != None)
      {
         start = HitLocation + X;
         continue;
      }

      if (ExtendedZCollision(Other) != None && Other.Owner != None)
         Other = Pawn(Other.Owner);

      if (!Other.bWorldGeometry && Other != Level)
      {
         if (KFMonster(Other) != None)
            DamageActor = Other;

         if (KFMonster(DamageActor) != None)
         {
            if (DamageActor == OldDamageActor)
            {
               start = HitLocation + X;
               continue;
            }

            OldDamageActor = DamageActor;
            bHP            = KFMonster(DamageActor).Health;

            if (bHP <= 0)
            {
               start = HitLocation + X;
               continue;
            }

            if (HitCount == 0)
               HitDamage = DamageMax;

            DamageActor.TakeDamage(int(HitDamage), Instigator, HitLocation, Momentum*X, DamageType, BID);

            if (KFMonster(DamageActor).Health == bHP) // do not decrease the damage if we've hit an invisible head
            {
               start = HitLocation + X;
               continue;
            }

            if (HitCount >= MaxPenetrations)
               return;

            Retries    = 0;
            HitDamage *= PenDamageReduction;
            HitCount++;
         }

         else
            Other.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);
      }

      else if (BlockingVolume(Other) == None)
      {
         if(KFWeaponAttachment(Weapon.ThirdPersonActor) != None)
            KFWeaponAttachment(Weapon.ThirdPersonActor).UpdateHit(Other, HitLocation, HitNormal);

         break;
      }
   }
}

// set weapon to pending reload state
event ModeDoFire()
{
   super.ModeDoFire();

   // server
   if (Weapon.Role == ROLE_Authority && AllowFire() && Weapon != None)
      M99SniperRifleFalk(Weapon).SetPendingReload();
}

// keep this gun in a way that a headshot by a lv20 sharp oneshots even a 12man hoe fp, currenly deals 2021 damage to one

defaultproperties
{
   RecoilRate=0.100000
   maxVerticalRecoilAngle=4000
   maxHorizontalRecoilAngle=90
   FireAimedAnim="Fire_Iron"
   FireAnim="Fire"
   bRandomPitchFireSound=False
   FireSoundRef="KF_M99Snd.M99_Fire_M"
   StereoFireSoundRef="KF_M99Snd.M99_Fire_S"
   NoAmmoSoundRef="KF_SCARSnd.SCAR_DryFire"
   bWaitForRelease=True
   TransientSoundVolume=1.800000
   FireForce="AssaultRifleFire"
   FireRate=0.34
   AmmoClass=Class'M99AmmoFalk'
   ShakeRotMag=(X=5.000000,Y=7.000000,Z=3.000000)
   ShakeRotRate=(X=10000.000000,Y=10000.000000,Z=10000.000000)
   ShakeOffsetMag=(X=5.000000,Y=5.000000,Z=5.000000)
   BotRefireRate=3.570000
   FlashEmitterClass=Class'ROEffects.MuzzleFlash1stPTRD'
   aimerror=1.000000
   Spread=0.004000
   DamageType=Class'DamTypeM99SniperRifleFalk'
   DamageMin=620               
   DamageMax=620
   MaxPenetrations=2
   PenDamageReduction=0.9
   AmmoPerFire=1
}
