class BaseballBatPickupFalk extends BaseballBat.BaseballBatPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=4.000000
   cost=1350
   InventoryType=Class'BaseballBatFalk'
   ItemName="Nailed Baseball Bat"
   ItemShortName="Nailbat"
   PickupMessage="You got a Nailed Baseball Bat"
   PowerValue=45
   SpeedValue=70
   RangeValue=42
   DrawScale=1.250000
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
