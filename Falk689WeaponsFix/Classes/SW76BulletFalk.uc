class SW76BulletFalk extends ShotgunBulletFalk;

var     String         ImpactSoundRefs[6];

var byte Bounces;
var bool bFinishedPenetrating;
var bool fShouldPenetrate;
var bool fAlreadyHit;
var bool fZedTimeSkill;

var KFMonster MonsterHeadAttached;
var ProjectileBodyPart Giblet;

// ice stuff
var class<Emitter> Trail1Class, Trail2Class;
var Emitter Trail1, Trail2;

replication
{
   reliable if (bNetInitial && Role == ROLE_Authority)
      Bounces;

   reliable if (Role == ROLE_Authority)
      MonsterHeadAttached, bFinishedPenetrating, fShouldPenetrate, fAlreadyHit, fZedTimeSkill;
}

static function PreloadAssets()
{
   //default.AmbientSound = sound(DynamicLoadObject(default.AmbientSoundRef, class'Sound', true));

   default.ImpactSounds[0] = sound(DynamicLoadObject(default.ImpactSoundRefs[0], class'Sound', true));
   default.ImpactSounds[1] = sound(DynamicLoadObject(default.ImpactSoundRefs[1], class'Sound', true));
   default.ImpactSounds[2] = sound(DynamicLoadObject(default.ImpactSoundRefs[2], class'Sound', true));
   default.ImpactSounds[3] = sound(DynamicLoadObject(default.ImpactSoundRefs[3], class'Sound', true));
   default.ImpactSounds[4] = sound(DynamicLoadObject(default.ImpactSoundRefs[4], class'Sound', true));
   default.ImpactSounds[5] = sound(DynamicLoadObject(default.ImpactSoundRefs[5], class'Sound', true));

   //UpdateDefaultStaticMesh(StaticMesh(DynamicLoadObject(default.StaticMeshRef, class'StaticMesh', true)));
}

static function bool UnloadAssets()
{
   default.ImpactSounds[0] = none;
   default.ImpactSounds[1] = none;
   default.ImpactSounds[2] = none;
   default.ImpactSounds[3] = none;
   default.ImpactSounds[4] = none;
   default.ImpactSounds[5] = none;

   //UpdateDefaultStaticMesh(none);

   return true;
}

// zed time skill addition, this should set stuff up to add a freezing trail behind bullets during zedtime
simulated function PostBeginPlay()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes>  Vet;

   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   super(Projectile).PostBeginPlay();

   if (Instigator != none)
   {
      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

      if (KFPRI != none)
      {
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none && Vet.Static.ArtilleristZedTimeBonus(KFPRI))
         {
            // this used to add penetration and bounces to bullets but we changed that skill
            /*fShouldPenetrate   = True;
            Bounces            = 7;
            LifeSpan           = 100.000000;
            MaxPenetrations    = 5;
            PenDamageReduction = 0.500000;*/
            fZedTimeSkill = True;

         }
      }
   }

   Velocity = Speed * Vector(Rotation);

   SetTimer(0.4, false);

   if ( Level.NetMode != NM_DedicatedServer )
   {
      if ( !PhysicsVolume.bWaterVolume )
      {
         Trail          = Spawn(class'NailGunTracer',self);
         Trail.Lifespan = Lifespan;
      }
   }
}

simulated function PostNetReceive()
{
   local Coords boneCoords;

   super.PostNetReceive();

   if( Giblet == none && MonsterHeadAttached != none )
   {
      boneCoords = MonsterHeadAttached.GetBoneCoords( 'head' );

      Giblet = Spawn( Class'ProjectileBodyPart',,, boneCoords.Origin, Rotator(boneCoords.XAxis) );
      Giblet.SetStaticMesh(MonsterHeadAttached.DetachedHeadClass.default.StaticMesh);
      Giblet.SetLocation(Location);
      Giblet.SetPhysics( PHYS_None );
      Giblet.SetBase(self);
      Giblet.Lifespan = Lifespan;
   }
}


// this should actually add the freezing trails and do... something about rotations?
simulated function Tick(float DeltaTime)
{
   if (Level.NetMode != NM_DedicatedServer && Physics != PHYS_None)
      SetRotation(Rotator(Normal(Velocity)));

   // add a freezing trail behind bullets 
   if (fZedTimeSkill && Level.NetMode != NM_DedicatedServer)
   {
      if (!PhysicsVolume.bWaterVolume)
      {
         if (Trail1 == None)
         {
            Trail1          = Spawn(Trail1Class,self);
            Trail1.Lifespan = Lifespan;
         }

         if (Trail2 == None)
         {
            Trail2          = Spawn(Trail2Class,self);
            Trail2.Lifespan = Lifespan;
         }
      }
   }
}


simulated function ProcessTouch(Actor Other, vector HitLocation)
{
   local vector X;
   local Vector TempHitLocation, HitNormal;
   local array<int>	HitPoints;
   local KFPawn KFHitPawn;
   local Pawn HitPawn;
   local int bHp;

   if (Other == none || Other == Instigator || Other.Base == Instigator || Other == FLastDamaged || !Other.bBlockHitPointTraces || KFHumanPawn(Other) != none || KFHumanPawn(Other.Base) != none)
      return;

   if (!fShouldPenetrate && fAlreadyHit)
   {
      Destroy();
      return;
   }

   fAlreadyHit  = True;
   FLastDamaged = Other;

   X = Vector(Rotation);

   if (ROBulletWhipAttachment(Other) != none)
   {
      if (!Other.Base.bDeleteMe)
      {
         Other = Instigator.HitPointTrace(TempHitLocation, HitNormal, HitLocation + (200 * X), HitPoints, HitLocation,, 1);

         if( Other == none || HitPoints.Length == 0 )
            return;

         KFHitPawn = KFPawn(Other);

         if (Role == ROLE_Authority)
         {
            if (KFHitPawn != none)
            {
               if (!KFHitPawn.bDeleteMe)
               {
                  KFHitPawn.SetDelayedDamageInstigatorController(fCInstigator);
                  KFHitPawn.ProcessLocationalDamage(FDamage, Instigator, TempHitLocation, MomentumTransfer * Normal(Velocity), MyDamageType,HitPoints);

                  FDamage *= PenDamageReduction; // Keep going, but lose effectiveness each time.

                  if (!fShouldPenetrate || FPenetrations >= MaxPenetrations)
                  {
                     Bounces = 0;
                     Destroy();
                  }

                  FPenetrations++;
               }
            }
         }
      }
   }

   else
   {
      HitPawn = Pawn(Other);

      if (HitPawn == none)
         HitPawn = Pawn(Other.Base);

      if (HitPawn != none)
      {

         bHP = HitPawn.Health;
         Other.SetDelayedDamageInstigatorController(fCInstigator);
         HitPawn.TakeDamage(FDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);

         // don't scale damage if we haven't damaged the zed
         if (bHP > 0 && HitPawn.Health < bHP)
         {
            PenDamageReduction = default.PenDamageReduction;
            FDamage *= PenDamageReduction; // Keep going, but lose effectiveness each time.

            // if we've struck through more than the max number of foes, destroy.
            if (!fShouldPenetrate || FPenetrations >= MaxPenetrations)
            {
               Bounces = 0;
               Destroy();
            }

            FPenetrations++;
         }
      }

      // this may be needed since shotguns basically fire bugs
      else
      {
         Other.SetDelayedDamageInstigatorController(fCInstigator);
         Other.TakeDamage(FDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);

         PenDamageReduction = default.PenDamageReduction;
         FDamage *= PenDamageReduction; // Keep going, but lose effectiveness each time.

         // if we've struck through more than the max number of foes, destroy.
         if (!fShouldPenetrate || FPenetrations >= MaxPenetrations)
         {
            Bounces = 0;
            Destroy();
         }

         FPenetrations++;
      }
   }
}


simulated singular function HitWall(vector HitNormal, actor Wall)
{
   HurtWall = None;

   if (Wall == none || Wall == Instigator || Wall.Base == Instigator || Wall == FLastDamaged || !Wall.bBlockHitPointTraces)
      return;

   if ((Wall.bStatic && Wall.bWorldGeometry) || 
       KFDoorMover(Wall)  != none            ||
       KFTraderDoor(Wall) != none)
   {

      if (Bounces <= 0)
      {
         Spawn(class'ROBulletHitEffect',,, Location, rotator(-HitNormal));
         SetPhysics(PHYS_None);
         Destroy();
         return;
      }

      if (!Level.bDropDetail && (FRand() < 0.4))
         Playsound(ImpactSounds[Rand(6)]);

      Velocity = Velocity - 2.0*HitNormal*(Velocity dot HitNormal);
      Bounces--;

      // We bounced, change BulletID so we can hit a zed again
      if (BulletID < 689)
         BulletID = BulletID * 8 + 689;

      else
         BulletID++;

      FLastDamaged = none;

      if (!Level.bDropDetail && (Level.NetMode != NM_DedicatedServer))
         Spawn(class'ROEffects.ROBulletHitMetalEffect',,,Location, rotator(HitNormal));
      return;

      if (Trail != None)
      {
         Trail.mRegen=False;
         Trail.SetPhysics(PHYS_None);
      }

      bBounce = false;
   }
}

simulated function PhysicsVolumeChange( PhysicsVolume Volume )
{
   if (Volume.bWaterVolume)
   {
      if ( Trail != None )
         Trail.mRegen=False;
      //Velocity *= 0.65;
   }
}

simulated function Landed( Vector HitNormal )
{
   SetPhysics(PHYS_None);
   //LifeSpan = 5.0;
}

simulated function Destroyed()
{
	if (Trail1 != none)
   {
		Trail1.Kill();
		Trail1.SetPhysics(PHYS_None);
	}    

	if (Trail2 != none)
   {
		Trail2.Kill();
		Trail2.SetPhysics(PHYS_None);
	}

   super.Destroyed();

   if (Giblet != none)
   {
      Giblet.Destroy();
      Giblet = none;
   }

   if (MonsterHeadAttached != none)
      MonsterHeadAttached = none;
}


defaultproperties
{
   bNetTemporary=False
   bNetNotify=True
   bBounce=True
   fShouldPenetrate=False
   DamageAtten=5.000000
   MaxPenetrations=1
   PenDamageReduction=0.500000
   HeadShotDamageMult=1.100000
   Speed=5500.000000
   MaxSpeed=5500.000000
   bSwitchToZeroCollision=True
   FDamage=28.000000 // Needed 'cause default.Damage is ignored by ProcessTouch
   Damage=28.000000
   DamageRadius=0.000000
   MomentumTransfer=50000.000000
   DrawType=DT_StaticMesh
   CullDistance=3000.000000
   LifeSpan=3.000000
   DrawScale=0.5
   Style=STY_Alpha
   ImpactEffect=class'ROBulletHitEffect'
   MyDamageType=Class'DamTypeSW76Falk'
   ImpactSoundRefs(0)="ProjectileSounds.Bullets.Impact_Metal"
   ImpactSoundRefs(1)="ProjectileSounds.Bullets.Impact_Metal"
   ImpactSoundRefs(2)="ProjectileSounds.Bullets.Impact_Metal"
   ImpactSoundRefs(3)="ProjectileSounds.Bullets.Impact_Metal"
   ImpactSoundRefs(4)="ProjectileSounds.Bullets.Impact_Metal"
   ImpactSoundRefs(5)="ProjectileSounds.Bullets.Impact_Metal"
   Trail1Class=Class'IJC_Project_Santa.NitroTrail'
   Trail2Class=Class'IJC_Project_Santa.NitroTrailB'
}
