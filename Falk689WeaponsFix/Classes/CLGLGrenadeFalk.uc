class CLGLGrenadeFalk extends M79GrenadeProjectileFalk;

defaultproperties
{
   ImpactDamageType=Class'DamTypeCLGLGrenadeFalk'
   fCClass=class'CLGLClusterFalk'
   Damage=330         // was 275 before april buff
   ImpactDamage=90    // was 75 before april buff
}