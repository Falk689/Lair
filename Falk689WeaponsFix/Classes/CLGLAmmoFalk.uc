class CLGLAmmoFalk extends KFAmmunition;

defaultproperties
{
   PickupClass=Class'CLGLAmmoPickupFalk'
   IconMaterial=Texture'KillingFloorHUD.Generic.HUD'
   IconCoords=(X1=4,Y1=350,X2=110,Y2=395)
   AmmoPickupAmount=4
   MaxAmmo=24
   InitialAmount=12
   ItemName="40x46mm grenades"
}
