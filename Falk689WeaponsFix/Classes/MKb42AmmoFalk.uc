class MKb42AmmoFalk extends KFMod.MKb42Ammo;

defaultproperties
{
    MaxAmmo=300
    InitialAmount=120
    AmmoPickupAmount=30
    PickupClass=Class'MKb42AmmoPickupFalk'
    ItemName="7.92x33mm Kurz rounds"
}
