class DwarfAxePickupFalk extends KFMod.DwarfAxePickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   cost=2000
   Weight=7.000000
   InventoryType=Class'DwarfAxeFalk'
   ItemName="Dwarven Axe"
   ItemShortName="Dwarven Axe"
   PickupMessage="You got a Dwarven Axe"
   PowerValue=70
   SpeedValue=30
   RangeValue=30
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
