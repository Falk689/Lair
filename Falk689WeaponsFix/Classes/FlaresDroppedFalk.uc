class FlaresDroppedFalk extends Flares.FlareDropped;

// tell the gametype we've spawned in server too
simulated function PostNetBeginPlay()
{
   local Falk689GameTypeBase Flk;

   Super.PostNetBeginPlay();
 
   if (Role == ROLE_Authority)
   {
      Flk = Falk689GameTypeBase(Level.Game);

      if (Flk != none)
      {
         //warn("SPAWNED:"@Level.TimeSeconds@"Instigator:"@Instigator);
         Flk.FlareSpawned(PlayerController(Instigator.Controller));
      }
   }
}

// tell the gametype we have been destroyed
simulated function Destroyed()
{
   local Falk689GameTypeBase     Flk;

   Super.Destroyed();

   if (Role == ROLE_Authority)
   {
      Flk = Falk689GameTypeBase(Level.Game);

      if (Flk != none)
      {
         //warn("DESTROYED:"@Level.TimeSeconds@"Instigator:"@Instigator);
         Flk.FlareDestroyed(PlayerController(Instigator.Controller));
      }
   }
}

defaultproperties
{
   FlameTrailEmitterClass=Class'FlaresTrailFalk'
   LifeSpan=60.000000
} 
