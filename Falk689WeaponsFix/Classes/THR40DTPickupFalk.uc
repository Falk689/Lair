class THR40DTPickupFalk extends THR40DT.THR40DTPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
    Weight=9.000000
    cost=1150
    AmmoCost=80
    BuyClipSize=80
    PowerValue=50
    SpeedValue=55
    RangeValue=100
    ItemName="THR40 Machine Gun"
    ItemShortName="THR40"
    AmmoItemName="5.56mm NATO rounds"
    CorrespondingPerkIndex=7
    EquipmentCategoryID=4
    InventoryType=Class'THR40DTFalk'
    PickupMessage="You got a THR40 Machine Gun"
    PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
