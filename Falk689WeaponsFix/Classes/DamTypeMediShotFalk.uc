class DamTypeMediShotFalk extends DamTypeShotgunFalk;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FMedicDamage', Amount);
}

defaultproperties
{
   WeaponClass=Class'MediShotFalk'
   DeathString="%k killed %o (SSS-M)"
   FemaleSuicide="%o shot herself in the foot."
   MaleSuicide="%o shot himself in the foot."
   bRagdollBullet=True
   bBulletHit=True
   FlashFog=(X=600.000000)
   KDamageImpulse=10000.000000
   KDeathVel=300.000000
   KDeathUpKick=100.000000
   VehicleDamageScaling=0.700000
   bIsPowerWeapon=True
}
