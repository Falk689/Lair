class DamTypeTHR40DTFalk extends DamTypeSW76Falk;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FArtilleristDamage', Amount);
}

defaultproperties
{
    WeaponClass=Class'THR40DTFalk'
    DeathString="%k killed %o (THR40)"
    KDamageImpulse=5500.000000
    KDeathVel=450.000000
    KDeathUpKick=45.000000
    HeadShotDamageMult=1.100000
}