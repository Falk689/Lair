class AAR525SPickupFalk extends AAR525SPickupBaseFalk;

#exec OBJ LOAD FILE=LairStaticMeshes_SM.usx
#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   StaticMesh=StaticMesh'LairStaticMeshes_SM.CustomReskins.AlienFlameSpitterPickup'
   cost=3500
   Weight=6.000000
   AmmoCost=30
   BuyClipSize=60
   InventoryType=Class'AAR525SFalk'
   ItemName="Alien Flame Spitter 525"
   ItemShortName="AFS525"
   PickupMessage="You got an Alien Flame Spitter 525"
   PowerValue=13
   SpeedValue=80
   RangeValue=60
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
