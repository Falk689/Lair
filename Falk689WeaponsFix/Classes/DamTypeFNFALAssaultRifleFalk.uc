class DamTypeFNFALAssaultRifleFalk extends KFMod.DamTypeFNFALAssaultRifle;

defaultproperties
{
    WeaponClass=Class'FNFAL_ACOGAssaultRifleFalk'
    DeathString="%k killed %o (FAL)"
	HeadShotDamageMult=1.100000
}