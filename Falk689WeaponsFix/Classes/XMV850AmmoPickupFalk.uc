class XMV850AmmoPickupFalk extends XMV850.XMV850AmmoPickup;

defaultproperties
{
   InventoryType=Class'XMV850AmmoFalk'
   PickupMessage="You found 7.62x51mm rounds"
   AmmoAmount=50
}