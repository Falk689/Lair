class ThompsonDrumAmmoFalk extends KFMod.ThompsonDrumAmmo;

defaultproperties
{
    AmmoPickupAmount=50
    MaxAmmo=300
    InitialAmount=150
    PickupClass=Class'ThompsonDrumAmmoPickupFalk'
    ItemName=".45 ACP rounds"
}