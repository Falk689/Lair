class DamTypeShotgunFalk extends DamTypeShotgun;

defaultproperties
{
   HeadShotDamageMult=1.100000
   bIsPowerWeapon=True
   WeaponClass=Class'Falk689WeaponsFix.ShotgunFalk'
   DeathString="%k killed %o (Benelli M3)"
   bRagdollBullet=True
   bBulletHit=True
   FlashFog=(X=600.000000)
   KDamageImpulse=10000.000000
   KDeathVel=300.000000
   KDeathUpKick=100.000000
}
