class SCARMK17AmmoPickupFalk extends KFMod.SCARMK17AmmoPickup;

defaultproperties
{
    AmmoAmount=20
    InventoryType=Class'SCARMK17AmmoFalk'
    PickupMessage="You found some 7.62x51mm NATO rounds"
}