class ScytheFireBFalk extends KFMod.ScytheFireB;

var byte  BID; // bullet ID to prevent multiple hits on a single zed

simulated function Timer()
{
   local Actor            HitActor;
   local Actor            OldDamageActor;
   local vector           StartTrace, EndTrace, HitLocation, HitNormal;
   local rotator          PointRot;
   local int              MyDamage;
   local bool             bBackStabbed;
   local Pawn             Victims;
   local vector           dir, lookdir;
   local float            DiffAngle, VictimDist;
   local array<Actor>     IgnoreActors;
   local int              bHP;
   local byte             LoopCount;
   local bool             fHit;

   BID++;

   if (BID > 200)
      BID = 1;

   MyDamage = MeleeDamage;

   If (!KFWeapon(Weapon).bNoHit)
   {
      MyDamage   = MeleeDamage;
      StartTrace = Instigator.Location + Instigator.EyePosition();

      if(Instigator.Controller != None && PlayerController(Instigator.Controller) == None && Instigator.Controller.Enemy != None)
         PointRot = rotator(Instigator.Controller.Enemy.Location - StartTrace); // Give aimbot for bots.

      else
         PointRot = Instigator.GetViewRotation();

      EndTrace = StartTrace + vector(PointRot) * weaponRange;

      While (LoopCount <= 10)
      {
         LoopCount++;

         HitActor = Instigator.Trace( HitLocation, HitNormal, EndTrace, StartTrace, true);

         if(HitActor == None)
            break;

         else if (HitActor == Instigator || HitActor.Base == Instigator)
         {
            //log("Instigator");
            IgnoreActors[IgnoreActors.Length] = HitActor;
            HitActor.SetCollision(false);
            StartTrace = HitLocation;
            continue;
         }

         else
         {
            ImpactShakeView();

            if(HitActor.IsA('ExtendedZCollision') && HitActor.Base != none && HitActor.Base.IsA('KFMonster') )
               HitActor = HitActor.Base;

            if (HitActor.IsA('KFMonster')  && KFMeleeGun(Weapon).BloodyMaterial != none)
            {
               Weapon.Skins[KFMeleeGun(Weapon).BloodSkinSwitchArray] = KFMeleeGun(Weapon).BloodyMaterial;
               Weapon.texture = Weapon.default.Texture;
            }

            if(Level.NetMode == NM_Client)
               Return;

            if(HitActor.IsA('Pawn') && !HitActor.IsA('Vehicle')
                  && (Normal(HitActor.Location-Instigator.Location) dot vector(HitActor.Rotation)) > 0)
            {
               bBackStabbed = true;
               MyDamage *= 1.25;
            }

            if((KFMonster(HitActor) != none))
            {
               //	log(VSize(Instigator.Velocity));

               if (HitActor != OldDamageActor) // don't hit this zed twice
               {
                  fHit                             = True;
                  OldDamageActor                   = HitActor;
                  bHP                              = KFMonster(HitActor).Health;
                  KFMonster(HitActor).bBackstabbed = bBackStabbed;

                  HitActor.TakeDamage(MyDamage, Instigator, HitLocation, vector(PointRot), hitDamageClass, BID);

                  if(MeleeHitSounds.Length > 0)
                     Weapon.PlaySound(MeleeHitSounds[Rand(MeleeHitSounds.length)],SLOT_None,MeleeHitVolume,,,,false);

                  if(VSize(Instigator.Velocity) > 300 && KFMonster(HitActor).Mass <= Instigator.Mass)
                     KFMonster(HitActor).FlipOver();

                  if (KFMonster(HitActor).Health != bHP) // this zed was hit, break the loop
                     break;

                  else // maybe we tried an headshot on a decapitated zed, ignore it
                  {
                     IgnoreActors[IgnoreActors.Length] = HitActor;
                     HitActor.SetCollision(false);
                     //log("ignore");
                  }
               }

               else
                  break;

            }

            else
            {
               HitActor.TakeDamage(MyDamage, Instigator, HitLocation, vector(PointRot), hitDamageClass, BID);
               Spawn(HitEffectClass,,, HitLocation, rotator(HitLocation - StartTrace));
               break;
               //if( KFWeaponAttachment(Weapon.ThirdPersonActor)!=None )
               //  KFWeaponAttachment(Weapon.ThirdPersonActor).UpdateHit(HitActor,HitLocation,HitNormal);

               //Weapon.IncrementFlashCount(ThisModeNum);
            }
         }
      }

      if(WideDamageMinHitAngle > 0)
      {
         foreach Weapon.VisibleCollidingActors(class'Pawn', Victims, (weaponRange * 2), StartTrace) //, RadiusHitLocation
         {
            if((HitActor != none && Victims == HitActor) || Victims.Health <= 0)
               continue;

            if(Victims != Instigator)
            {
               VictimDist = VSizeSquared(Instigator.Location - Victims.Location);

               //log("VictimDist = "$VictimDist$" Weaponrange = "$(weaponRange*Weaponrange));

               //Instigator.ClearStayingDebugLines();
               //Instigator.DrawStayingDebugLine( Instigator.Location, EndTrace,0, 255, 0);

               if(VictimDist > (((weaponRange * 1.1) * (weaponRange * 1.1)) + (Victims.CollisionRadius * Victims.CollisionRadius)))
                  continue;

               lookdir = Normal(Vector(Instigator.GetViewRotation()));
               dir = Normal(Victims.Location - Instigator.Location);

               DiffAngle = lookdir dot dir;

               if(DiffAngle > WideDamageMinHitAngle)
               {
                  //Instigator.DrawStayingDebugLine( Victims.Location + vect(0,0,10), Instigator.Location,255, 0, 0);
                  //log("Shot would hit "$Victims$" DiffAngle = "$DiffAngle$" WideDamageMinHitAngle = "$WideDamageMinHitAngle$" for damage of: "$(MyDamage*DiffAngle));
                  if (fHit)
                     Victims.TakeDamage(MyDamage * DiffAngle, Instigator, (Victims.Location + Victims.CollisionHeight * vect(0,0,0.7)), vector(PointRot), hitDamageClass, BID);

                  else
                     Victims.TakeDamage(MyDamage, Instigator, (Victims.Location + Victims.CollisionHeight * vect(0,0,0.7)), vector(PointRot), hitDamageClass, BID);

                  fHit = True;

                  if(MeleeHitSounds.Length > 0)
                     Victims.PlaySound(MeleeHitSounds[Rand(MeleeHitSounds.length)],SLOT_None,MeleeHitVolume,,,,false);
               }
               //else
               //{
               //Instigator.DrawStayingDebugLine( Victims.Location, Instigator.Location,255, 255, 0);
               //log("Shot would miss "$Victims$" DiffAngle = "$DiffAngle$" WideDamageMinHitAngle = "$WideDamageMinHitAngle);
               //}
            }
         }
      }

      // Turn the collision back on for any actors we turned it off
      if (IgnoreActors.Length > 0)
      {
         for (i=0; i<IgnoreActors.Length; i++)
         {
            if(IgnoreActors[i] != None)
               IgnoreActors[i].SetCollision(true);
         }
      }
   }
}

// don't fire while throwing a nade
simulated function bool AllowFire()
{
    if (KFPawn(Instigator).bThrowingNade)
        return false;

    if (KFPawn(Instigator).SecondaryItem != none)
        return false;

    return true;
}

defaultproperties
{
    MeleeDamage=345
    weaponRange=106.000000
    DamagedelayMin=1.040000
    DamagedelayMax=1.040000
    hitDamageClass=Class'DamTypeScytheFalk'
    FireRate=1.650000
    FireAnimRate=0.910000
    bWaitForRelease=True
}
