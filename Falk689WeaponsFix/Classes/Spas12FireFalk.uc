class Spas12FireFalk extends Spas12.SpasFire;

var byte BID;

// attempt to see the whole fockin reload anim, step dunno
simulated function bool AllowFire()
{
   if ((Spas12Falk(Weapon)      != None && Spas12Falk(Weapon).fLoadingLastBullet) ||
       (LocalSpas12Falk(Weapon) != None && LocalSpas12Falk(Weapon).fLoadingLastBullet))
   {
      return False;
   }

	return Super.AllowFire();
}

// Added zed time support skills
function DoFireEffect()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes>   Vet;

   KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

   if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
   {
      Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

      if (Vet != none)
      {
         ProjPerFire = Vet.Static.GetShotgunProjPerFire(KFPRI, Default.ProjPerFire, Weapon);
         Spread      = Default.Spread * Vet.Static.GetShotgunSpreadMultiplier(KFPRI, Weapon);
      }
   }

   Super.DoFireEffect();
}


// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 210)
      BID = 1;

   class<ShotgunBulletFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local ShotgunBulletFalk FB;

   FB = ShotgunBulletFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
   ProjectileClass=Class'Spas12BulletFalk'
   AmmoClass=Class'SpasAmmoFalk'
   Spread=1250.00000
   FireRate=0.965
}
