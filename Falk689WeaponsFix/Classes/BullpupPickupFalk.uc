class BullpupPickupFalk extends KFMod.BullpupPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=4.000000
   InventoryType=Class'BullpupFalk'
   ItemName="SA80 L22A1 Carbine"
   ItemShortName="L22A1"
   PickupMessage="You got an SA80 L22A1 Carbine"
   PowerValue=26
   SpeedValue=45
   RangeValue=100
   cost=500
   BuyClipSize=30
   AmmoCost=15
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}