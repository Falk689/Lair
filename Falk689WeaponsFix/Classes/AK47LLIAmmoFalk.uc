class AK47LLIAmmoFalk extends KFMod.AK47Ammo;

defaultproperties
{
    AmmoPickupAmount=30
    MaxAmmo=300
    InitialAmount=120
    ItemName="7.62x39mm rounds"
	PickupClass=Class'AK47LLIAmmoPickupFalk'
}
