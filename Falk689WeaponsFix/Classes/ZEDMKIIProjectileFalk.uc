class ZEDMKIIProjectileFalk extends ZEDGunProjectileFalk;

defaultproperties
{
   ExplosionEmitter=Class'KFMod.ZEDMKIIPrimaryProjectileImpact'
   FlameTrailEmitterClass=Class'KFMod.ZEDMKIIPrimaryProjectileTrail'
   ExplosionSoundVolume=1.650000
   ArmDistSquared=0.000000
   StaticMeshRef="ZED_FX_SM.Energy.ZED_FX_Energy_Card"
   ExplosionSoundRef="KF_FY_ZEDV2SND.WEP_ZEDV2_Projectile_Explode"
   AmbientSoundRef="KF_FY_ZEDV2SND.WEP_ZEDV2_Projectile_Loop"
   AmbientVolumeScale=2.500000
   Speed=1700.000000
   MaxSpeed=1700.000000
   Damage=50.000000
   DamageRadius=0.000000
   MyDamageType=Class'DamTypeZEDGunFalk'
   ExplosionDecal=Class'BurnMarkMediumFalk'
   LightType=LT_Steady
   LightHue=128
   LightSaturation=64
   LightBrightness=255.000000
   LightRadius=4.000000
   LightCone=16
   bDynamicLight=True
   DrawScale=0.500000
   AmbientGlow=254
   bUnlit=True
}

