class Spas12BulletFalk extends ShotgunBulletFalk;

defaultproperties
{
   FDamage=43.000000
   MaxPenetrations=2
   PenDamageReduction=0.500000
   HeadShotDamageMult=1.100000
   MyDamageType=Class'DamTypeSpas12Falk'
}
