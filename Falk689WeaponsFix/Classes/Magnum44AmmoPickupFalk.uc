class Magnum44AmmoPickupFalk extends KFMod.Magnum44AmmoPickup;

defaultproperties
{
   AmmoAmount=12
   InventoryType=Class'Magnum44AmmoFalk'
   PickupMessage="You got .44 rounds"
}
