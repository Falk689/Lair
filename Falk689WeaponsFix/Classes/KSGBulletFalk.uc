class KSGBulletFalk extends ShotgunBulletFalk;

defaultproperties
{
   FDamage=20.000000
   MaxPenetrations=2
   PenDamageReduction=0.500000
   HeadShotDamageMult=1.100000
   MyDamageType=Class'DamTypeKSGFalk'
}
