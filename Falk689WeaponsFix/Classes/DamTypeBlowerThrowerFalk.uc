class DamTypeBlowerThrowerFalk extends KFMod.DamTypeBlowerThrower;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FMedicDamage', Amount);
}

defaultproperties
{
   bCheckForHeadshots=False
   WeaponClass=Class'BlowerThrowerFalk'
}