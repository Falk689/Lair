class AA12PickupFalk extends KFMod.AA12Pickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
	InventoryType=Class'AA12Falk'
	ItemName="AA12 Automatic Shotgun"
	ItemShortName="AA12"
	PickupMessage="You got an AA12 Automatic Shotgun"
	cost=4000
	Weight=10.000000
	BuyClipSize=20
	AmmoCost=40
	PowerValue=30
    SpeedValue=80
    RangeValue=100
	PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}