class DamTypeW1300_Compact_EditionFalk extends DamTypeShotgunFalk;

defaultproperties
{
   KDeathVel=300.000000
   KDeathUpKick=100.000000
   HeadShotDamageMult=1.100000
   WeaponClass=Class'W1300_Compact_EditionFalk'
}
