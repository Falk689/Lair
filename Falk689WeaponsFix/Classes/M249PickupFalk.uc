class M249PickupFalk extends M249Mut.M249Pickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=12.000000
   cost=2000
   AmmoCost=100
   BuyClipSize=100
   PowerValue=54
   SpeedValue=50
   RangeValue=100
   ItemName="M249 Squad Automatic Weapon"
   ItemShortName="M249"
   CorrespondingPerkIndex=7
   InventoryType=Class'M249Falk'
   PickupMessage="You got an M249 Squad Automatic Weapon"
   DrawScale=1.25
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
