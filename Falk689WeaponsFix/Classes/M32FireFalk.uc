class M32FireFalk extends M32Fire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID += 4;

   if (BID > 200)
      BID = BID - 200;

   class<M79GrenadeProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local M79GrenadeProjectileFalk FG;
   local KFPlayerReplicationInfo KFPRI;

   FG = M79GrenadeProjectileFalk(P);

   if (FG != none && Instigator != none)
      FG.fCInstigator = Instigator.Controller;

   KFPRI       = KFPlayerReplicationInfo(Weapon.Instigator.PlayerReplicationInfo);

   if (KFPRI.ClientVeteranSkill != none)
   {
      P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Weapon.Instigator), P.Damage, Class'KFMod.DamTypeM79Grenade');
   }

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
   ProjectileClass=Class'M32GrenadeProjectileFalk'
   AmmoClass=Class'M32AmmoFalk'
   FireRate=0.33000
}
