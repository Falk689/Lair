class ScythePickupFalk extends KFMod.ScythePickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   InventoryType=Class'ScytheFalk'
   ItemName="Reaper Scythe"
   ItemShortName="Scythe"
   PickupMessage="You got a Reaper Scythe"
   cost=1600
   Weight=6.000000
   PowerValue=69
   SpeedValue=35
   RangeValue=42
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
