class ZEDGunPickupFalk extends KFMod.ZEDGunPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   InventoryType=Class'ZEDGunFalk'
   cost=500
   ItemName="Zed Eradication Device"
   ItemShortName="ZED"
   PickupMessage="You got a Zed Eradication Device"
   CorrespondingPerkIndex=8
   BuyClipSize=100
   AmmoCost=50
   PowerValue=80
   SpeedValue=40
   RangeValue=100
   Weight=8.000000
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
