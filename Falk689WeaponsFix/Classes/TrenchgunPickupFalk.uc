class TrenchgunPickupFalk extends KFMod.TrenchgunPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
    Weight=7.000000
    cost=1250
    BuyClipSize=6
    AmmoCost=12
    PowerValue=56
    SpeedValue=10
    RangeValue=100
    ItemName="Gentleman's Disclosure"
    ItemShortName="Disclosure"
    AmmoItemName="Dragon's breath shells"
    InventoryType=Class'TrenchgunFalk'
    PickupMessage="You got a Gentleman's Disclosure"
    StaticMesh=StaticMesh'LairStaticMeshes_SM.CustomReskins.SteampunkTrenchgunPickup'
    PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
