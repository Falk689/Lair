class SPShotgunPickupFalk extends KFMod.SPShotgunPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   cost=3000
   Weight=8.000000
   BuyClipSize=10
   AmmoCost=20
   PowerValue=46
   SpeedValue=60
   RangeValue=100
   InventoryType=Class'SPShotgunFalk'
   ItemName="Multichamber Zed Thrower"
   ItemShortName="MZT"
   PickupMessage="You got a Multichamber Zed Thrower"
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}