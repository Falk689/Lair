class StingerPickupFalk extends StingerPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
   if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
   {
      if (KFGameType(Level.Game) != none)
         KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
   }

   super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
     Weight=16.000000
     cost=3500
     AmmoCost=100
     BuyClipSize=100
     PowerValue=52
     SpeedValue=60
     RangeValue=100
     Description="This gatling gun has slightly lower damage output than the XMV850, but it's lighter, and cheaper. It does not need to be reloaded, as all the ammunition is available at any moment. However, it features a much slower fire rate compared to the XMV850. Ammo capacity is shown as a percentage value in the HUD."
     ItemName="Stinger Gatling Gun"
     ItemShortName="Stinger"
     AmmoItemName="7.62x51mm rounds"
     AmmoMesh=StaticMesh'KillingFloorStatics.L85Ammo'
     CorrespondingPerkIndex=7
     EquipmentCategoryID=3
     InventoryType=Class'StingerFalk'
     PickupMessage="You got a Stinger Gatling Gun"
     PickupForce="AssaultRiflePickup"
     StaticMesh=StaticMesh'Stinger_SM.UT3StingerPickup'
     CollisionRadius=25.000000
     CollisionHeight=5.000000
     PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
