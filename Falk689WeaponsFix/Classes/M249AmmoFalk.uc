class M249AmmoFalk extends M249Mut.M249Ammo;

defaultproperties
{
   AmmoPickupAmount=25 // one quarter of magazine, heavy weapons standard
   MaxAmmo=400
   InitialAmount=200
   PickupClass=Class'M249AmmoPickupFalk'
}