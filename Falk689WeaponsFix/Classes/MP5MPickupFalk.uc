class MP5MPickupFalk extends KFMod.MP5MPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

var int fStoredAmmo; // remaining ammo stored into the pickup when dropped

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// set fStoredAmmo to the weapon class on pickup
auto state pickup
{
   function BeginState()
   {
      UntriggerEvent(Event, self, None);
      if ( bDropped )
      {
         AddToNavigation();
         SetTimer(20, false);
      }
   }

   // When touched by an actor.  Let's mod this to account for Weights. (Player can't pickup items)
   // IF he's exceeding his max carry weight.
   function Touch(Actor Other)
   {
      local Inventory Copy;
      local Falk689GameTypeBase Flk;

      if ( KFHumanPawn(Other) != none && !CheckCanCarry(KFHumanPawn(Other)) )
      {
         return;
      }

      // If touched by a player pawn, let him pick this up.
      if ( ValidTouch(Other) )
      {
         Copy = SpawnCopy(Pawn(Other));

         AnnouncePickup(Pawn(Other));
         SetRespawn();
 
         if (Copy != None)
         {
            if (MP5MMedicGunFalk(Copy) != none)
            {
               Flk = Falk689GameTypeBase(Level.Game);

               if (Flk != none && Flk.InZedTime())
                  MP5MMedicGunFalk(Copy).StartForceReload();

               else if (MP5MMedicGunFalk(Copy).fStoredAmmo == -1)
               {
                  MP5MMedicGunFalk(Copy).fStoredAmmo        = fStoredAmmo;
                  MP5MMedicGunFalk(Copy).fPickupAmmoRestore = True;
                  MP5MMedicGunFalk(Copy).fPawnAmmoRestore   = True;
                  //Warn("Recovering from pickup: "@fStoredAmmo);
               }
            }

            Copy.PickupFunction(Pawn(Other));
         }

         if ( MySpawner != none && KFGameType(Level.Game) != none )
         {
            KFGameType(Level.Game).WeaponPickedUp(MySpawner);
         }

         if ( KFWeapon(Copy) != none )
         {
            KFWeapon(Copy).SellValue = SellValue;
            KFWeapon(Copy).bPreviouslyDropped = bDropped;

            if ( !bPreviouslyDropped && KFWeapon(Copy).bIsTier3Weapon &&
                  Pawn(Other).Controller != none && Pawn(Other).Controller != DroppedBy )
            {
               KFWeapon(Copy).Tier3WeaponGiver = DroppedBy;
            }
         }
      }
   }
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   InventoryType=Class'MP5MMedicGunFalk'
   ItemName="Medical Heckler & Koch MP5 SMG"
   ItemShortName="MP5-M"
   PickupMessage="You got a Medical Heckler & Koch MP5 SMG"
   PowerValue=14
   SpeedValue=60
   RangeValue=100
   BuyClipSize=32
   AmmoCost=16
   cost=1600
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
