class ATMineAmmoFalk extends ATMineAmmoBaseFalk;

var KFWeapon Weapon; // weapon instance

// call weapon FNewAmmoAdded to restore anims and value on success
function bool FShouldAddAmmo(int AmmoToAdd)
{
    local bool                  result;

    result = Super.FShouldAddAmmo(AmmoToAdd);

    if (!result)
        return result;

    if (ATMineExplosiveFalk(Weapon) != none)
        ATMineExplosiveFalk(Weapon).FNewAmmoAdded();

    return result;
}

defaultproperties
{
    MaxAmmo=2
    InitialAmount=1
    AmmoPickupAmount=1
}
