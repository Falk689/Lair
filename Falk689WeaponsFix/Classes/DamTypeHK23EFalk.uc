class DamTypeHK23EFalk extends DamTypeSW76Falk;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FArtilleristDamage', Amount);
}

defaultproperties
{
   HeadShotDamageMult=1.100000
   WeaponClass=Class'HK23EFalk'
   DeathString="%k killed %o (HK23E)"
}
