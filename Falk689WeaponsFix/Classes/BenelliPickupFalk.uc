class BenelliPickupFalk extends KFMod.BenelliPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   cost=3400
   BuyClipSize=6
   AmmoCost=12
   InventoryType=Class'BenelliShotgunFalk'
   ItemName="Benelli M4 Super 90 Combat Shotgun"
   ItemShortName="Benelli M4"
   PickupMessage="You got a Benelli M4 Super 90 Combat Shotgun"
   PowerValue=49
   SpeedValue=80
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
