class CLGLPickupFalk extends KFWeaponPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
    Weight=6.000000
    cost=1750
    AmmoCost=20
    BuyClipSize=4
    PowerValue=47
    SpeedValue=35
    RangeValue=100
    AmmoMesh=StaticMesh'KillingFloorStatics.XbowAmmo'
    CorrespondingPerkIndex=6
    MaxDesireability=0.790000
    InventoryType=Class'CLGLFalk'
    PickupForce="AssaultRiflePickup"
    StaticMesh=StaticMesh'CLGL_R.CLGLMeshPickup'
    DrawScale=1.400000
    CollisionRadius=25.000000
    CollisionHeight=10.000000
    ItemName="China Lake Grenade Launcher"
	ItemShortName="CL"
    AmmoItemName="40x46mm grenades"
    EquipmentCategoryID=3
    PickupMessage="You got a China Lake Grenade Launcher"
	PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
