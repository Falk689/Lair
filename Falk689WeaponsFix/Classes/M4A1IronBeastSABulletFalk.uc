class M4A1IronBeastSABulletFalk extends TrenchgunBulletFalk;

defaultproperties
{
   DamageAtten=5.000000
   MaxPenetrations=1
   PenDamageReduction=1.000000
   HeadShotDamageMult=1.100000
   Speed=3500.000000
   MaxSpeed=3500.000000
   bSwitchToZeroCollision=True
   Damage=30.000000             // was somehow 39 in patch 13 even though it was supposed to already be 30
   FDamage=30.000000            // was somehow 39 in patch 13 even though it was supposed to already be 30
   DamageRadius=0.000000
   MomentumTransfer=50000.000000
   MyDamageType=Class'DamTypeM4A1IronBeastSAFalk'
   ExplosionDecal=Class'BurnMarkTinyFalk'
   ExplosionEmitter=class'KFMod.DragonsBreathImpactEffect'
   DrawType=DT_StaticMesh
   StaticMeshRef="EffectsSM.Ger_Tracer"
   CullDistance=3000.000000
   LifeSpan=3.000000
   DrawScale=0.5
   Style=STY_Alpha
   ImpactEffect=class'ROBulletHitEffect'
}
