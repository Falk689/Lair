class AxePickupFalk extends KFMod.AxePickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
    Weight=5.000000
    cost=650
    PowerValue=51
    SpeedValue=35
    RangeValue=30
    ItemName="Fire Axe"
    ItemShortName="Axe"
    InventoryType=Class'AxeFalk'
    PickupMessage="You got a Fire Axe"
    PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
