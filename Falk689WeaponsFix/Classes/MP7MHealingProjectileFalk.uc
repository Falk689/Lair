class MP7MHealingProjectileFalk extends KFMod.HealingProjectile;

var byte                   fMaxHeal;        // how many targets this bullet heals offperk
var byte                   fMaxTry;         // how many times we retry to heal failed ones
var byte                   fTry;            // current retry attempt
var transient array<actor> fHealedActors;   // already healed actors so we don't heal people twice or more
var bool                   fHitHealTarget;  // have we healed someone?
var bool                   fEffectSpawned;  // have we spawned an hit effect?
var bool                   fShouldSpawn;    // should we spawn an effect?
var bool                   fDestroy;        // should we destroy this bullet?
var bool                   fFailedHeal;     // we failed an heal attempt
var bool                   fDestroyed;      // already destroyed? stop trying to
var bool                   fBlockReward;    // should this dart block the reward to its owner?
var bool                   fRewardChecked;  // have we already checked if we should reward the owner?
var vector                 fHitLocation;    // stored hitlocation
var vector                 fHitNormal;      // stored hitnormal
var string                 fMultiHealStr;   // multiple heal string, used when you healed more than one dood
var string                 fHealedName;     // multiple heal string, used when you healed more than one dood
var xEmitter               Trail;           // healing projectile trail

replication
{
	reliable if(Role == ROLE_Authority)
		fHitHealTarget, fHitLocation, fHitNormal, fShouldSpawn, fHealedName, fBlockReward, fRewardChecked;

   reliable if(Level.NetMode == NM_Client || (Level.NetMode == NM_ListenServer && Role == ROLE_Authority))
      fEffectSpawned;
}

// spawn the trail
simulated function PostBeginPlay()
{
   Super.PostBeginPlay();

   if (Level.NetMode != NM_DedicatedServer)
   {
      if (!PhysicsVolume.bWaterVolume)
      {
         Trail = Spawn(class'MedicTracerFalk', self);
         Trail.Lifespan = Lifespan;
      }
   }
}

// Check if we already hit a pawn and optionally add it
function int fAlreadyHit(actor HitActor, optional bool fAdd)
{
   local int i;

   for (i=0; i < fHealedActors.length; i++) 
   {
      if (fHealedActors[i] == HitActor)
      {
         //warn("Found: "@i);
         return i;
      }
   }

   if (fAdd)
   {
      //warn("Adding new Actor: "@i);
      fHealedActors.insert(i, 1);
      fHealedActors[i] = HitActor;
      return i;
   }

   //warn("Not Found");
   return -1;
}

// Do a healing effect/sound instead of standard "explode"
simulated function HitHealTarget(vector HitLocation, vector HitNormal)
{
   fHitHealTarget = true;
   bHidden        = true;
   SetPhysics(PHYS_None);

   if (Role == ROLE_Authority)
      PlaySound(ExplosionSound);

   HealLocation  = HitLocation;
   HealRotation  = rotator(HitNormal);
   NetUpdateTime = Level.TimeSeconds - 1;
}

// try to heal an actor, probably a wall, and explode
simulated singular function HitWall(vector HitNormal, actor Wall)
{
   if (Wall == none || Wall == Instigator || Wall.Base == Instigator || KFHumanPawn(Wall) != none || KFHumanPawn(Wall.Base) != none)
      return;

   if (Role == ROLE_Authority && HealOther(Wall, Location)); 
      fAlreadyHit(Wall, True);

   fHitLocation = Location + ExploWallOut * HitNormal;
   fHitNormal   = HitNormal;

   if (Trail != None)
   {
      Trail.mRegen = False;
      Trail.SetPhysics(PHYS_None);
   }

   Explode(fHitLocation, HitNormal);
}

// try to heal a target and explode
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{
   if (Other == none || Other == Instigator || Other.Base == Instigator)
      return;

   if (FHumanPawn(Other) != none || FalkMonsterBase(Other) != none)
   {
      if (Role == ROLE_Authority && FHumanPawn(Other) != none && HealOther(Other, HitLocation))
         fAlreadyHit(Other, True);

      fHitLocation = HitLocation;
      fHitNormal   = -vector(Rotation);

      Explode(HitLocation, fHitNormal);
   }
} 

// explode healing nearby targets
simulated function Explode(vector HitLocation, vector HitNormal)
{
   if (bHasExploded)
      return;

   bHasExploded = True;
   
   if (Role == ROLE_Authority)
   {
      HealRadius(Damage, DamageRadius, MyDamageType, MomentumTransfer, HitLocation, HitNormal); 

      if (!fFailedHeal)
      {
         //warn("done");
         //fSpawnEffect = True;
         fDestroy     = True;
      }

   }
}

// medic HurtRadius
simulated function HealRadius(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation, vector HitNormal)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;
   local actor                  Target;
   local byte                   healed;     // currently healed players
   local float                  healRadius; // heal radius perk modifier

   //warn("kaboom");

   healRadius = DamageRadius;
   healed     = 0;

   if (Instigator != none)
   {
      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

      if (KFPRI != none)
      {
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none)
            healRadius *= Vet.Static.GetHealRadiusMod(KFPRI); 
      }
   }

   SetPhysics(PHYS_None);

   foreach VisibleCollidingActors(class'Actor', Target, healRadius, HitLocation)
   {
      if (FHumanPawn(Target) != none && Target != Instigator)
      {
         bHurtEntry = false;
         //warn("healing: "@Target);

         if (HealOther(Target, HitLocation))
         {
            fAlreadyHit(Target, True); // add this pawn
            healed++;
         }

         if (healed >= fMaxHeal)
            break;
      }
   }

   if (healed > 0)
   {
      if (healed > 1)
         fHealedName = fMultiHealStr;

      if (KFMedicGun(Instigator.Weapon) != none)
         KFMedicGun(Instigator.Weapon).ClientSuccessfulHeal(fHealedName);
   }
}

// Return True if a pawn is healed
simulated function bool HealOther(Actor Other, Vector HitLocation)
{
   local KFPlayerReplicationInfo KFPRI;
   local int MedicReward;
   local float fMedicReward;
   local FHumanPawn Healed;
   local Falk689GameTypeBase Flk;
   local float HealSum; // for modifying based on perks

   if (Other == none || Other == Instigator || Other.Base == Instigator || Role != ROLE_Authority)
      return False;

   if (fAlreadyHit(Other) >= 0) // we already healed this dude
   {
      //warn("Already Healed: "@Other);
      return True;
   }


   Healed = FHumanPawn(Other);

   if (Instigator != none)
   {
      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

      if (!fRewardChecked && PlayerController(Instigator.Controller) != none)
      {
         fRewardChecked = true;
 
         // medic reward block stuff
         Flk = Falk689GameTypeBase(Level.Game);

         if (Flk != none)
            fBlockReward = !Flk.ShouldRewardMedic(PlayerController(Instigator.Controller));
      }
   }

   if (Healed != none)
   {
      if (Healed.GetPlayerName() == "") // only stuff with a name should be healed
      {
         //warn("Failed");
         fFailedHeal = True;
         return False;
      }

      fFailedHeal = False;
      // base MedicReward
      MedicReward = HealBoostAmount;

      // healing and reward math
      if (Healed.Health < Healed.HealthMax && Healed.bCanBeHealed)
      {
         // healing visual effect, only the first time if we heal multiple targets
         if (!fHitHealTarget)
            HitHealTarget(HitLocation, -vector(Rotation));

         // proper MedicReward
         if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
            MedicReward *= KFPRI.ClientVeteranSkill.Static.GetHealPotency(KFPRI);

         HealSum = MedicReward;

         if ((Healed.Health + Healed.fHealthToGive + MedicReward) > Healed.HealthMax)
         {
            MedicReward = Healed.HealthMax - (Healed.Health + Healed.fHealthToGive);

            if (MedicReward < 0)
               MedicReward = 0;
         }

         Healed.GiveHealth(HealSum, Healed.HealthMax);

         if (KFPRI != None)
         {
            if (MedicReward > 0 && KFSteamStatsAndAchievements(KFPRI.SteamStatsAndAchievements) != none)
               AddDamagedHealStats( MedicReward );

            if (!fBlockReward)
            {
               fMedicReward = float(MedicReward) * 0.3; // 20hp healed = £ 6

               // difficulty scaling
               if (Level.Game.GameDifficulty >= 8.0) // Bloodbath
                  fMedicReward *= 0.34;

               else if (Level.Game.GameDifficulty >= 7.0) // Hell on Earth
                  fMedicReward *= 0.5;

               else if (Level.Game.GameDifficulty >= 5.0 ) // Suicidal
                  fMedicReward *= 0.67;

               else if (Level.Game.GameDifficulty >= 4.0 ) // Hard
                  fMedicReward *= 0.84;

               MedicReward = int(fMedicReward);

               KFPRI.ReceiveRewardForHealing(MedicReward, Healed);
            }

            if (KFHumanPawn(Instigator) != none)
               KFHumanPawn(Instigator).AlphaAmount = 255;

            fHealedName = Healed.GetPlayerName();

            /*if(KFMedicGun(Instigator.Weapon) != none)
               KFMedicGun(Instigator.Weapon).ClientSuccessfulHeal(Healed.GetPlayerName());*/
         }

         return True;
      }

      return False;
   }

   return False;
}

// added more heals attempt to fix weird shit happening sometimes
simulated function Tick(float DeltaTime)
{
   Super.Tick(DeltaTime);

   if (Role == ROLE_Authority)
   {
      if (fFailedHeal && fTry < fMaxTry)
      {
         fTry++;
         //warn("Retry: "@fTry);
         bHasExploded = False;
         Explode(fHitLocation, fHitNormal);
      }

      else if (!fDestroyed && (fDestroy || fTry >= fMaxTry))
      {
         fDestroy = False;
         SetTimer(0.2, false);
      }

      if (bHasExploded && (!fFailedHeal || fTry >= fMaxTry))
         fShouldSpawn = True;
   }

   if ((Level.NetMode == NM_Client || (Level.NetMode == NM_ListenServer && Role == ROLE_Authority)) && fShouldSpawn && !fEffectSpawned)
   {
      fEffectSpawned = True;
      fShouldSpawn   = False;

      if (fHitHealTarget)
         Spawn(Class'KFMod.HealingFX',,, fHitLocation, rotator(fHitNormal));

      else
		   Spawn(class'ROBulletHitEffect',,, fHitLocation, rotator(-fHitNormal));

      //warn("Spawn");
   }
}

function Timer()
{
   if (fDestroyed)
      return;

   fDestroyed = True;
   //warn("Destroy");
   //BlowUp(fHitLocation);
   Destroy();
}


// destroy the trail
simulated function Destroyed()
{
	if (Trail !=None)
      Trail.mRegen = False;
   
   Super.Destroyed();

}

defaultproperties
{
   fMaxHeal=11
   fMaxTry=3
   DamageRadius=60.000000
   Speed=12500.00000
   MaxSpeed=12500.00000
   HealBoostAmount=20
   fMultiHealStr="multiple targets"
}
