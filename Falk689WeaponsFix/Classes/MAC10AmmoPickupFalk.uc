class MAC10AmmoPickupFalk extends KFMod.KFAmmoPickup;

defaultproperties
{
    AmmoAmount=30
    InventoryType=Class'MAC10AmmoFalk'
    PickupMessage="You found some .45 ACP rounds"
}