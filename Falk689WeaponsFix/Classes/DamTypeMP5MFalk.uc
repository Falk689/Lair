class DamTypeMP5MFalk extends KFMod.DamTypeMP5M;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FMedicDamage', Amount);
}

defaultproperties
{
   WeaponClass=Class'MP5MMedicGunFalk'
   HeadShotDamageMult=1.10000
}