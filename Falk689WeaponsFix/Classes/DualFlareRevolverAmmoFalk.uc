class DualFlareRevolverAmmoFalk extends KFMod.FlareRevolverAmmo;

// debug stuff to see if something goes wrong here
function bool AddAmmo(int AmmoToAdd)
{
   if (AmmoAmount < MaxAmmo)
      AmmoAmount = Min(MaxAmmo, AmmoAmount+AmmoToAdd);

   NetUpdateTime = Level.TimeSeconds - 1;
   return true;
}

defaultproperties
{
   AmmoPickupAmount=12
   MaxAmmo=90
   InitialAmount=84
   PickupClass=Class'DualFlareRevolverAmmoPickupFalk'
}
