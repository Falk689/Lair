class FlareRevolverProjectileFalk extends KFMod.FlareRevolverProjectile;

var Actor      FLastDamaged;
var int        BulletID;
var float      FDamage;
var Controller fCInstigator;   // instigator controller, I don't use vanilla InstigatorController 'cause of reasons

replication
{
   reliable if(Role == ROLE_Authority)
      BulletID, FLastDamaged, fCInstigator;
}

// Post death kill fix 
simulated function PostBeginPlay()
{
   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   Super.PostBeginPlay();
}

// override to fix point blank fire
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{
   local vector X;
   local Vector TempHitLocation, HitNormal;
   local array<int>	HitPoints;
   local KFPawn HitPawn;

   // Don't let it hit this player, or blow up on another player
   if (Other == none || Other == Instigator || Other.Base == Instigator || Other == FLastDamaged || !Other.bBlockHitPointTraces || KFBulletWhipAttachment(Other) != none || KFHumanPawn(Other) != none || KFHumanPawn(Other.Base) != none)
      return;

   FLastDamaged = Other;

   // Use the instigator's location if it exists. This fixes issues with
   // the original location of the projectile being really far away from
   // the real Origloc due to it taking a couple of milliseconds to
   // replicate the location to the client and the first replicated location has
   // already moved quite a bit.
   if(Instigator != none)
      OrigLoc = Instigator.Location;

   X = Vector(Rotation);

   if(Role == ROLE_Authority)
   {
      if( ROBulletWhipAttachment(Other) != none )
      {
         if(!Other.Base.bDeleteMe)
         {
            Other = Instigator.HitPointTrace(TempHitLocation, HitNormal, HitLocation + (200 * X), HitPoints, HitLocation,, 1);

            if( Other == none || HitPoints.Length == 0 )
               return;

            HitPawn = KFPawn(Other);

            if (Role == ROLE_Authority)
            {
               if ( HitPawn != none )
               {
                  // Hit detection debugging
                  /*log("Bullet hit "$HitPawn.PlayerReplicationInfo.PlayerName);
                    HitPawn.HitStart = HitLocation;
                    HitPawn.HitEnd = HitLocation + (65535 * X);*/

                  if (!HitPawn.bDeleteMe)
                  {
                     HitPawn.SetDelayedDamageInstigatorController(fCInstigator);
                     HitPawn.ProcessLocationalDamage(ImpactDamage, Instigator, TempHitLocation, MomentumTransfer * Normal(Velocity), ImpactDamageType,HitPoints);
                  }

                  // Hit detection debugging
                  //if( Level.NetMode == NM_Standalone)
                  //	HitPawn.DrawBoneLocation();
               }
            }
         }
      }

      else
      {
         if (Pawn(Other) != none && Pawn(Other).IsHeadShot(HitLocation, X, 1.0))
         {
            Other.SetDelayedDamageInstigatorController(fCInstigator);
            Pawn(Other).TakeDamage(ImpactDamage * HeadShotDamageMult, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), ImpactDamageType, BulletID);
         }

         else
         {
            Other.SetDelayedDamageInstigatorController(fCInstigator);
            Other.TakeDamage(ImpactDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), ImpactDamageType, BulletID);
         }
      }
   }

   if(!bDud)
      Explode(HitLocation,Normal(HitLocation-Other.Location));
}

function TakeDamage( int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
   return;
}

defaultproperties
{
   ImpactDamageType=Class'DamTypeFlareProjectileImpactFalk'
   Speed=1500.000000
   MaxSpeed=1500.000000
   Damage=25.000000
   DamageRadius=100.000000
   HeadShotDamageMult=1.100000
   ImpactDamage=75
   ExplosionDecal=Class'BurnMarkMediumFalk'
}
