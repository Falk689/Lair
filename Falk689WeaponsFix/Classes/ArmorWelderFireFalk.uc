class ArmorWelderFireFalk extends WeldFire;

var FHumanPawn CachedHealee;

// Falk689 stuff
var float fArmorStep; // default armor step amount
var float fArmorWeld; // current armor weld amount

simulated function bool AllowFire()
{
   local KFDoorMover WeldTarget;
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   WeldTarget = GetDoor();

   if (!CanFindHealee())
   {
      CachedHealee = none;

      if (WeldTarget == none)
      {
         if ( KFPlayerController(Instigator.Controller) != none )
         {
            KFPlayerController(Instigator.Controller).CheckForHint(54);

            if ( FailTime + 0.5 < Level.TimeSeconds )
               FailTime = Level.TimeSeconds;
         }

         return false;
      }
   }

   if (WeldTarget != none && WeldTarget.bDisallowWeld)
      return false;

   if (Instigator != none)
   {
      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

      if (KFPRI != none)
      {
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none)
            AmmoPerFire = Default.AmmoPerFire - Vet.Static.GetArmorWelderAmmoReduction(KFPRI);
      }
   }

   return Weapon.AmmoAmount(ThisModeNum) >= AmmoPerFire;
}

// Can we find someone to heal
function bool CanFindHealee()
{
   local FHumanPawn Healtarget;
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   if (Instigator != none)
   {
      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

      if (KFPRI != none)
      {
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none)
         {
            if (!Vet.Static.CanWeldArmor())
               return false;

            Healtarget = GetHealee();
            CachedHealee = Healtarget;

            if (Healtarget == none)
               return false;

            if (Healtarget.fShield >= 100 || Healtarget.fShield <= 0)
               return false;

            return true;
         }
      }
   }

   return false;
}


function FHumanPawn GetHealee()
{
   local FHumanPawn KFHP, BestKFHP;
   local vector Dir;
   local float TempDot, BestDot;
   local vector Dummy,End,Start;

   Dir = vector(Instigator.GetViewRotation());

   foreach Instigator.VisibleCollidingActors(class'FHumanPawn', KFHP, 80.0)
   {
      if (KFHP.fShield < KFHP.fShieldMax)
      {
         TempDot = Dir dot (KFHP.Location - Instigator.Location);
         if ( TempDot > 0.7 && TempDot > BestDot )
         {
            BestKFHP = KFHP;
            BestDot = TempDot;
         }
      }
   }

   Start = Instigator.Location+Instigator.EyePosition();
   End = Start+vector(Instigator.GetViewRotation())*weaponRange;
   Instigator.bBlockHitPointTraces = false;
   Instigator.Trace(Dummy,Dummy,End,Start,True);
   return BestKFHP;
}


simulated Function Timer()
{
   local Actor HitActor;
   local vector StartTrace, EndTrace, HitLocation, HitNormal,AdjustedLocation;
   local rotator PointRot;
   local int MyDamage;
   local ArmorWelderFalk aWF;

   If (!KFWeapon(Weapon).bNoHit)
   {
      MyDamage = MeleeDamage + Rand(MaxAdditionalDamage);

      if (KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none)
         MyDamage = float(MyDamage) * KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.Static.GetWeldSpeedModifier(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo));

      PointRot = Instigator.GetViewRotation();
      StartTrace = Instigator.Location + Instigator.EyePosition();

      if(AIController(Instigator.Controller) != None && Instigator.Controller.Target != None)
      {
         EndTrace = StartTrace + vector(PointRot)*weaponRange;
         Weapon.bBlockHitPointTraces = false;
         HitActor = Trace(HitLocation, HitNormal, EndTrace, StartTrace, true);
         Weapon.bBlockHitPointTraces = Weapon.default.bBlockHitPointTraces;

         if(HitActor == None)
         {
            EndTrace = Instigator.Controller.Target.Location;
            Weapon.bBlockHitPointTraces = false;
            HitActor = Trace(HitLocation, HitNormal, EndTrace, StartTrace, true);
            Weapon.bBlockHitPointTraces = Weapon.default.bBlockHitPointTraces;
         }

         if(HitActor == None)
            HitLocation = Instigator.Controller.Target.Location;

         HitActor = Instigator.Controller.Target;
      }

      else
      {
         EndTrace = StartTrace + vector(PointRot) * weaponRange;
         Weapon.bBlockHitPointTraces = false;
         HitActor = Trace(HitLocation, HitNormal, EndTrace, StartTrace, true);
         Weapon.bBlockHitPointTraces = Weapon.default.bBlockHitPointTraces;
      }

      LastHitActor = HitActor;
      aWF          = ArmorWelderFalk(Weapon);

      if(LastHitActor != none && Level.NetMode != NM_Client && FHumanPawn(HitActor) == none)
      {
         AdjustedLocation = Hitlocation;
         AdjustedLocation.Z = (Hitlocation.Z - 0.15 * Instigator.collisionheight);

         HitActor.TakeDamage(MyDamage, Instigator, HitLocation , vector(PointRot),hitDamageClass);
         Spawn(class'KFWelderHitEffect',,, AdjustedLocation, rotator(HitLocation - StartTrace));

         if (aWF != none && Level.Game != none && Level.Game.NumPlayers > 1 && aWF.bJustStarted && Level.TimeSeconds - aWF.LastWeldingMessageTime > aWF.WeldingMessageDelay)
         {
            aWF.bJustStarted = false;
            aWF.LastWeldingMessageTime = Level.TimeSeconds;

            if (Instigator != none && Instigator.Controller != none && PlayerController(Instigator.Controller) != none)
               PlayerController(Instigator.Controller).Speech('AUTO', 0, "");
         }
      }

      else if(CachedHealee != none && Instigator != none && Level.NetMode != NM_Client)
      {
         AdjustedLocation = Hitlocation;
         AdjustedLocation.Z = (Hitlocation.Z - 0.15 * Instigator.collisionheight);
         Spawn(class'KFWelderHitEffect',,, AdjustedLocation, rotator(HitLocation - StartTrace)); 			
         WeldArmor(CachedHealee);
      }

      else if (HitActor == none)
         CachedHealee = none;
   }
}


function bool WeldArmor(FHumanPawn CachedHealeeM)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;
   local KFPlayerController PC;
   local float cWeldPower;  // used to mod based on perk
   local int   cWeldEffect; // applied weld effect
   local Falk689GameTypeBase Flk;

   cWeldPower = fArmorStep;

   if (Instigator != none)
   {

      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);
      PC    = KFPlayerController(Instigator.Controller);

      if (KFPRI != none)
      {
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none)
            cWeldPower += Vet.Static.GetArmorWeldBoost(KFPRI);
      }
   }

   fArmorWeld += cWeldPower;

   if (fArmorWeld >= 1.0) // this should slow down armor welding for lower perks levels
   {
      fArmorWeld -= 1.0;
      cWeldEffect = 1;

      if (fArmorWeld > 1.0)  // there's a lot of welding power left, let's just add some more armor
      {
         cWeldEffect += int(fArmorWeld);
         fArmorWeld = 0;
      }

      if (CachedHealeeM.fShield + cWeldEffect < CachedHealeeM.fShieldMax)
      {
         CachedHealeeM.fShield += cWeldEffect;

         if (PC != none && SRStatsBase(PC.SteamStatsAndAchievements) != none) // perk progress
            SRStatsBase(PC.SteamStatsAndAchievements).Rep.ProgressCustomValue(Class'FArmorWeld', cWeldEffect);

         if (KFPRI != none)
         {
            if (PC != none)
            {
               Flk = Falk689GameTypeBase(Level.Game);

               if (Flk != none)
                  cWeldEffect = Flk.GetSupportReward(PC, cWeldEffect);
            }

            if (cWeldEffect > 0)
            {
               //warn("cWeldEffect:"@cWeldEffect);
               KFPRI.ReceiveRewardForHealing(cWeldEffect, CachedHealeeM);
            }
         }
      }

      else
      {
         if (KFPRI != none)
         {
            cWeldEffect           = CachedHealeeM.fShieldMax - CachedHealeeM.fShield;
            CachedHealeeM.fShield = CachedHealeeM.fShieldMax;

            if (PC != none && SRStatsBase(PC.SteamStatsAndAchievements) != none) // perk progress
               SRStatsBase(PC.SteamStatsAndAchievements).Rep.ProgressCustomValue(Class'FArmorWeld', cWeldEffect);

            if (PC != none)
            {

               Flk = Falk689GameTypeBase(Level.Game);

               if (Flk != none)
                  cWeldEffect = Flk.GetSupportReward(PC, cWeldEffect);
            }

            if (cWeldEffect > 0)
            {
               //warn("cWeldMaxShield:"@CachedHealeeM.fShieldMax - CachedHealeeM.fShield);
               KFPRI.ReceiveRewardForHealing(cWeldEffect, CachedHealeeM);
            }
         }
      }

      return true;
   }

   return false;
}


defaultproperties
{
   fArmorStep=0.15
   fArmorWeld=0.0
   maxAdditionalDamage=0
   hitDamageClass=Class'KFMod.DamTypeWelder'
   FireRate=0.200000
   AmmoClass=Class'ArmorWelderAmmoFalk'
   AmmoPerFire=20
   NoWeldTargetMessage=""
   CantWeldTargetMessage=""
}
