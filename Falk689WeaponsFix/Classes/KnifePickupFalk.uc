class KnifePickupFalk extends KFMod.KnifePickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   cost=10
   InventoryType=Class'KnifeFalk'
   ItemName="Military Combat Knife"
   ItemShortName="Knife"
   PickupMessage="You got a Military Combat Knife"
   SpeedValue=80
   RangeValue=16
   PowerValue=11
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
