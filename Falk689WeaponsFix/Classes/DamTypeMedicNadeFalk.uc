class DamTypeMedicNadeFalk extends DamTypeVomit;

var float fDotMultipler;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FMedicDamage', Amount);
}

defaultproperties
{
    fDotMultipler=0.1
    bCheckForHeadShots=False
    DeathString="%k poisoned %o (Medical smoke grenade)"
    FemaleSuicide="%o Auschwitz'd herself"
    MaleSuicide="%o Auschwitz'd himself"
    bLocationalHit=False
}
