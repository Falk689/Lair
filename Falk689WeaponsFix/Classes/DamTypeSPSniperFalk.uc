class DamTypeSPSniperFalk extends KFMod.DamTypeSPSniper;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FSharpshooterDamage', Amount);
}

defaultproperties
{
   HeadShotDamageMult=1.50000  // lever-action rifles standard
   WeaponClass=Class'SPSniperRifleFalk'
}
