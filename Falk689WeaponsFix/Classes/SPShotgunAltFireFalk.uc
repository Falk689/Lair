class SPShotgunAltFireFalk extends KFMod.SPShotgunAltFire;

defaultproperties
{
    FireRate=1.0
    FireAnimRate=0.84
    PushRange=215
    WideDamageMinHitAngle=0.6
    AmmoPerFire=0
}
