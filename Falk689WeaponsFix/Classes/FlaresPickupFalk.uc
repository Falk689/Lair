class FlaresPickupFalk extends Flares.FlarePickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   cost=50
   AmmoCost=50
   BuyClipSize=5
   InventoryType=Class'FlaresFalk'
   ItemName="Emergency Flares"
   ItemShortName="Flares"
   PickupMessage="You got some Emergency Flares"
   CorrespondingPerkIndex=8
   PowerValue=0
   SpeedValue=0
   RangeValue=0
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}