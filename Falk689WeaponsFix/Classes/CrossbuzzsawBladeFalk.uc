class CrossbuzzsawBladeFalk extends CrossbowArrowFalk;

#exec OBJ LOAD FILE=KF_InventorySnd.uax

// Sounds
var     sound       BladeHitWall;   // Sound of the blade hitting a wall
var     sound       BladeHitArmor;  // Sound of the blade hitting an armored enemy
var     sound       BladeHitFlesh;  // Sound of the blade hitting flesh

// Physics
var() 	float 	  StraightFlightTime;          // How long the projectile and flies straight
var 		float 	  TotalFlightTime;             // How long the rocket has been in flight
var 		bool 		  bOutOfPropellant;            // Projectile is out of propellant

// Falk689 stuff
var      bool              fLanded;              // we landed
var      bool              fStick;               // don't stick twice
var      class<Projectile> fCClass;              // Cluster class, what we spawn on kaboom
var      vector            fCSpawnCorZ;          // Z correction we apply to check if we hit the ceiling
var      vector            fCSpawnCorY;          // Y correction we apply to check if we hit the ceiling
var      vector            fCSpawnCorX;          // X correction we apply to check if we hit the ceiling


replication
{
   reliable if (Role == ROLE_Authority)
      fLanded;
}

simulated function PostBeginPlay()
{
   local vector Dir;

   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   Dir = vector(Rotation);
   Velocity = Speed * Dir;

   if( Level.NetMode != NM_DedicatedServer )
   {
      if ( !PhysicsVolume.bWaterVolume )
      {
         Trail = Spawn(class'CrossbuzzsawTracer',self);
         Trail.Lifespan = Lifespan;
      }
   }

   super(Projectile).PostBeginPlay();
}

simulated function HitWall( vector HitNormal, actor Wall)
{
   if ((Mover(Wall) != None) && Mover(Wall).bDamageTriggered)
   {
      if (Level.NetMode != NM_Client)
         Wall.TakeDamage( Damage, instigator, Location, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID); 

      //Destroy();
      return;
   }

   PlaySound(BladeHitWall,SLOT_None,0.6,,300,0.9+frand()*0.2);

   if (EffectIsRelevant(Location,false))
      Spawn(class'CrossbuzzsawImpact',,,, rotator(hitnormal));

   if (Physics == PHYS_Projectile && Bounces > 0)
   {
      Velocity = Velocity - 2.0 * HitNormal * (Velocity dot HitNormal);
      Bounces--; 
      return;
   }

   else
   {
      bBounce = false;
      Stick(Wall, Location);
   }

   SetRotation(rotator(Velocity));

   if(Instigator!=None && Level.NetMode!=NM_Client)
      MakeNoise(0.3);
}

// Bypass crossbow stuff
/*simulated function ProcessTouch(Actor Other, vector HitLocation)
{
   Super(SW76BulletFalk).ProcessTouch(Other, HitLocation);
}*/


simulated function Tick( float Delta)
{
   SetRotation(Rotator(Normal(Velocity)));

   if (Level.NetMode != NM_DedicatedServer && fLanded && !fGreen)
   {
      fCurT += Delta;

      if (fCurT >= fGreenT)
      {
         fGreen       = True;
         AmbientSound = None;
         UV2Texture   = FadeColor'PatchTex.Common.PickupOverlay';
      }
   }
}

// Simplified wall stick since arrows don't stick to zeds anyway
simulated function Stick(actor Wall, vector HitLocation)
{
   local Projectile P;
   local int i;

   if (fStick == True)
      return;

   AmbientSound = None;
   fStick       = True;

   if (Trail !=None)
      Trail.mRegen = False;

   Super.Stick(Wall, HitLocation);

   fLanded = True;

   if (Level.NetMode != NM_DedicatedServer)
   {
      fGreen  = True;
      UV2Texture = FadeColor'PatchTex.Common.PickupOverlay';
   }

   else
   {
      P = Spawn(fCClass,,, Location + fCSpawnCorZ);

      if (P == None)
         i++;

      P = Spawn(fCClass,,, Location + fCSpawnCorZ + fCSpawnCorY);

      if (P == None)
         i++;

      P = Spawn(fCClass,,, Location + fCSpawnCorZ - fCSpawnCorY);

      if (P == None)
         i++;

      P = Spawn(fCClass,,, Location + fCSpawnCorZ + fCSpawnCorX);

      if (P == None)
         i++;

      P = Spawn(fCClass,,, Location + fCSpawnCorZ - fCSpawnCorX);

      if (P == None)
         i++;

      //log(i);

      if (i == 5)
      {
         //log("Ceiling");
         SetPhysics(PHYS_Falling);
      }
   }
}

// on wall state
simulated state OnWall
{
   Ignores HitWall;

   function ProcessTouch(Actor Other, vector HitLocation)
   {
      local Inventory inv;

      if (Pawn(Other) != None && Pawn(Other).Inventory != None)
      {
         for (inv=Pawn(Other).Inventory; inv!=None; inv=inv.Inventory)
         {
            if(Crossbuzzsaw(Inv) != None && Weapon(inv).AmmoAmount(0) < Weapon(inv).MaxAmmo(0))
            {
               KFweapon(Inv).AddAmmo(1,0);
               PlaySound(Sound'KF_InventorySnd.Ammo_GenericPickup', SLOT_Pain, 2 * TransientSoundVolume,, 400);

               if(PlayerController(Pawn(Other).Controller) != none)
                  PlayerController(Pawn(Other).Controller).ReceiveLocalizedMessage(class'ProjectilePickupMessageFalk', 1);

               Destroy();
            }
         }
      }
   }

   simulated function BeginState()
   {
      bCollideWorld = False;

      if (Level.NetMode != NM_DedicatedServer)
         AmbientSound = None;

      SetCollisionSize(30, 30);
      KSetBlockKarma(false);
   }
}

simulated function Explode(vector HitLocation, vector HitNormal);

function PlayHitNoise(bool bArmored)
{
   if(bArmored)
      PlaySound(BladeHitArmor,,2.0);   // implies hit a target with shield/armor

   else
      PlaySound(BladeHitFlesh,,2.0);
}


simulated function Landed(vector HitNormal)
{
   HitWall(HitNormal, None);
}

simulated function Destroyed()
{
   if (Trail !=None)
      Trail.mRegen = False;

   Super.Destroyed();
}

defaultproperties
{
   HeadShotDamageMult=1.100000
   Speed=1500.000000
   MaxSpeed=1500.000000
   Damage=500.000000
   FDamage=500.000000
   MyDamageType=Class'DamTypeCrossbuzzsawFalk'
   bBounce=true
   fShouldPenetrate=True
   Bounces=2
   MaxPenetrations=4
   PenDamageReduction=0.500000
   AmbientSound=Sound'KF_IJC_HalloweenSnd.KF_SawbladeBow_Projectile_Loop'
   StaticMesh=StaticMesh'EffectsSM.Weapons.cheetah_blade'
   BladeHitWall=Sound'KF_IJC_HalloweenSnd.KF_SawbladeBow_Projectile_Hit'
   BladeHitArmor=Sound'KF_IJC_HalloweenSnd.KF_SawbladeBow_Projectile_Hit'
   BladeHitFlesh=Sound'KF_AxeSnd.Axe_HitFlesh'
   StraightFlightTime=0.650000
   MomentumTransfer=50000.000000
   ExplosionDecal=Class'KFMod.ShotgunDecal'
   DrawType=DT_StaticMesh
   CullDistance=7500.000000
   bUpdateSimulatedPosition=True
   DrawScale=3.000000
   Style=STY_Alpha
   bUnlit=False
   SoundVolume=175
   SoundRadius=250.000000
   TransientSoundVolume=0.500000
   fCClass=class'CrossbuzzsawCeilingDetector'
   fCSpawnCorZ=(Z=15.000000)
   fCSpawnCorY=(Y=20.000000)
   fCSpawnCorX=(X=20.000000)
}
