class M79FireFalk extends M79Fire;

var byte BID;
//var   bool  bVeryLastShotAnim;

// set weapon to pending reload state
event ModeDoFire()
{
   if (!AllowFire())
      return;

   //bVeryLastShotAnim = Weapon.AmmoAmount(0) <= AmmoPerFire;

   super.ModeDoFire();

   if (M79GrenadeLauncherFalk(Weapon) != None && Weapon.Role == ROLE_Authority)
      M79GrenadeLauncherFalk(Weapon).SetPendingReload();
}

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID += 4;

   if (BID > 200)
      BID = BID - 200;

   class<M79GrenadeProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local M79GrenadeProjectileFalk FG;
   local KFPlayerReplicationInfo KFPRI;

   FG = M79GrenadeProjectileFalk(P);

   if (FG != none && Instigator != none)
      FG.fCInstigator = Instigator.Controller;

   KFPRI       = KFPlayerReplicationInfo(Weapon.Instigator.PlayerReplicationInfo);

   if (KFPRI.ClientVeteranSkill != none)
   {
      P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Weapon.Instigator), P.Damage, Class'KFMod.DamTypeM79Grenade');
   }

   Super.PostSpawnProjectile(P);
}

// kind of allow fire only in proper situations?
simulated function bool AllowFire()
{
   if (KFWeapon(Weapon).bIsReloading)
      return false;

   if (KFPawn(Instigator).SecondaryItem!=none)
      return false;

   if (KFPawn(Instigator).bThrowingNade)
      return false;

   if (Level.TimeSeconds - LastClickTime>FireRate)
      LastClickTime = Level.TimeSeconds;

   if (KFWeapon(Weapon).MagAmmoRemaining < 1)
      return false;

   return super(WeaponFire).AllowFire();
}

defaultproperties
{
   ProjectileClass=Class'M79GrenadeProjectileFalk'
   AmmoClass=Class'M79AmmoFalk'
   FireRate=0.34
}
