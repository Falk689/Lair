class HuntingRifleAmmoFalk extends BDHuntingRifleFinal.Hunting_RifleAmmo;

defaultproperties
{
   AmmoPickupAmount=5
   MaxAmmo=35
   InitialAmount=15
   ItemName="6.5mm Creedmoor rounds"
}