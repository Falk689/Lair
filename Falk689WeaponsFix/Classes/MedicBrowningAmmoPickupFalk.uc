class MedicBrowningAmmoPickupFalk extends KFAmmoPickup;

defaultproperties
{
   AmmoAmount=16
   InventoryType=Class'MedicBrowningAmmoFalk'
   PickupMessage="You found some .40 SW rounds"
}