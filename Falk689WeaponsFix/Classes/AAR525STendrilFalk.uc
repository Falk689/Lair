class AAR525STendrilFalk extends FlameTendrilFalk;

defaultproperties
{
   Damage=26.000000
   FDamage=26.000000
   MyDamageType=Class'DamTypeAAR525SFalk'
   Speed=2400.000000           // this is what actually influences the real range of the gun
   MaxSpeed=2400.000000        // this is what actually influences the real range of the gun
}