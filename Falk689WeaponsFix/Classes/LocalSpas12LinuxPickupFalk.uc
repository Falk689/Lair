class LocalSpas12LinuxPickupFalk extends Spas12.SpasPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   cost=1400
   Weight=6.000000
   InventoryType=Class'LocalSpas12LinuxFalk'
   ItemName="Franchi SPAS12 Shotgun"
   ItemShortName="SPAS12"
   PickupMessage="You got a Franchi SPAS12 Shotgun"
   BuyClipSize=8
   AmmoCost=16
   PowerValue=60
   SpeedValue=30
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
