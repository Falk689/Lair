class CrossbowFireFalk extends KFMod.CrossbowFire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<CrossbowArrowFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local CrossbowArrowFalk FB;

   FB = CrossbowArrowFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

// don't fire while throwing a nade
simulated function bool AllowFire()
{
    if (KFWeapon(Weapon).bIsReloading)
        return false;

    if (KFPawn(Instigator).SecondaryItem != none)
        return false;

    if (KFPawn(Instigator).bThrowingNade)
        return false;

    return Super.AllowFire();
}

defaultproperties
{
    FireRate=2.10000
    AmmoClass=Class'CrossbowAmmoFalk'
    ProjectileClass=Class'CrossbowArrowFalk'
}
