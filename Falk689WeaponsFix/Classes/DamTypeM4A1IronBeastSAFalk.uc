class DamTypeM4A1IronBeastSAFalk extends KFProjectileWeaponDamageType;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount)
{
	KFStatsAndAchievements.AddFlameThrowerDamage(Amount);
}

defaultproperties
{
   bIsPowerWeapon=False
   bDealBurningDamage=True
   WeaponClass=Class'M4A1IronBeastSAAssaultRifleFalk'
   DeathString="%k killed %o (M4A1 IB)"
   bRagdollBullet=True
   bBulletHit=True
   FlashFog=(X=600.000000)
   KDamageImpulse=10000.000000
   KDeathVel=300.000000
   KDeathUpKick=100.000000
   VehicleDamageScaling=0.700000
}
