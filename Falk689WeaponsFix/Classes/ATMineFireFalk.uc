class ATMineFireFalk extends ATMine.ATMineFire;

var byte BID;

// implement our switch on timer since it should kind of work
function Timer()
{
   local FHumanPawn FHP;
   local ATMineExplosiveFalk cinnamonRoll;

   Weapon.ConsumeAmmo(ThisModeNum, Load);
   DoFireEffect();
   Weapon.PlaySound(Sound'KF_AxeSnd.Axe_Fire',SLOT_Interact,TransientSoundVolume,,TransientSoundRadius,,false);

   if (Weapon.AmmoAmount(0) <= 0 && Instigator != none)
   {
      FHP          = FHumanPawn(Instigator);
      cinnamonRoll = ATMineExplosiveFalk(Weapon);

      if (FHP != none && cinnamonRoll != none)
      {
         FHP.CurrentWeight -= cinnamonRoll.fWeight;
         cinnamonRoll.FLastAmmoUsed();
         FHP.FSwitchToBestWeapon();
         return;
      }

      Weapon.Destroy();
   }
}

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID += 4;

   if (BID > 200)
      BID = BID - 200;

   class<PipeBombProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator, damage modifiers and other perk settings
function PostSpawnProjectile(Projectile P)
{
   local PipeBombProjectileFalk FG;
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   FG = PipeBombProjectileFalk(P);

   if (FG != none && Instigator != none)
      FG.fCInstigator = Instigator.Controller;

   KFPRI       = KFPlayerReplicationInfo(Weapon.Instigator.PlayerReplicationInfo);

   if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
   {
      if (FG != none)
      { 
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none)
         {
            FG.fStoredKaboom = Vet.Static.DetonateBulletsControlBonus(KFPRI, True);
            FG.fDamageMulti  = float(Vet.Static.AddDamage(KFPRI, None, KFPawn(Weapon.Instigator), 1000, P.MyDamageType)) / 1000.0;
            P.Damage        *= FG.fDamageMulti;
         }
 
         //warn("NEW:"@P.Damage@"- MULTI:"@FG.fDamageMulti@"- FStoredKaboom:"@FG.fStoredKaboom@"- DamType:"@P.MyDamageType);
      }

      else
      {
         P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Weapon.Instigator), P.Damage, P.MyDamageType);
         //warn("FALLBACK:"@P.DAmage);
      }

      //warn("Damage:"@P.Damage);
   }

   Super.PostSpawnProjectile(P);
}

// removed some weird shit
simulated function bool AllowFire()
{
   if (KFPawn(Instigator).SecondaryItem != none)
      return false;

   if (KFPawn(Instigator).bThrowingNade)
      return false;

   if (Weapon != none && ATMineExplosiveFalk(Weapon) != none && ATMineExplosiveFalk(Weapon).fAmmoPickupTime > 0)
      return false;

   return super(WeaponFire).AllowFire();
}


// last fire anim when out of ammo
function PlayFiring()
{
   local float RandPitch;

   if (Weapon.Mesh != None)
      Weapon.PlayAnim(FireAnim, FireAnimRate, TweenTime);

   if (Weapon.Instigator != none && Weapon.Instigator.IsLocallyControlled() &&
       Weapon.Instigator.IsFirstPerson() && StereoFireSound != none)
   {
      if (bRandomPitchFireSound)
      {
         RandPitch = FRand() * RandomPitchAdjustAmt;

         if (FRand() < 0.5)
            RandPitch *= -1.0;
      }

      Weapon.PlayOwnedSound(StereoFireSound,SLOT_Interact,TransientSoundVolume * 0.85,,TransientSoundRadius,(1.0 + RandPitch),false);
   }

   else
   {
      if (bRandomPitchFireSound)
      {
         RandPitch = FRand() * RandomPitchAdjustAmt;

         if (FRand() < 0.5)
            RandPitch *= -1.0;
      }

      Weapon.PlayOwnedSound(FireSound,SLOT_Interact,TransientSoundVolume,,TransientSoundRadius,(1.0 + RandPitch),false);
   }

   ClientPlayForceFeedback(FireForce);  // jdf

   FireCount++;
}

defaultproperties
{
   AmmoClass=Class'ATMineAmmoFalk'
   ProjectileClass=Class'ATMineProjectileFalk'
   ProjectileSpawnDelay=1.100000
   FireRate=1.500000
   FireAnimRate=1.220000
   ProjSpawnOffset=(X=10.000000,Y=0.000000,Z=-40.000000)
}
