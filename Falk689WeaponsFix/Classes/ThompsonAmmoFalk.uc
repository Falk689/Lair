class ThompsonAmmoFalk extends KFMod.ThompsonAmmo;

defaultproperties
{
    AmmoPickupAmount=30
    MaxAmmo=300
    InitialAmount=150
    PickupClass=Class'ThompsonAmmoPickupFalk'
    ItemName=".45 ACP rounds"
}