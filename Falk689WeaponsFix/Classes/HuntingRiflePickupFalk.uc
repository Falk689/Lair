class HuntingRiflePickupFalk extends BDHuntingRifleFinal.Hunting_RiflePickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=8.000000
   cost=2300
   InventoryType=Class'HuntingRifleFalk'
   ItemName="Nosler M48 Hunting Rifle"
   ItemShortName="M48"
   BuyClipSize=5
   AmmoCost=10
   PickupMessage="You got a Nosler M48 Hunting Rifle"
   PowerValue=63
   SpeedValue=1
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
