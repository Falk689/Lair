class CrossbuzzsawCeilingDetector extends ATMineSoundFix;

// Don't load anything, just stay put
static function PreloadAssets()
{
   return;
}

// Don't take any damage, we don't need it
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
   return;
}

// Just start, explode and destroy 
simulated function PostBeginPlay()
{ 
   SetTimer(0.1, false);
}

// Delayed explosion
function Timer()
{
   Destroy(); 
}

// Overridden prevent collisions
simulated function HitWall(vector HitNormal, actor Wall)
{
   return;
}

// don't Bounce on hitwall
simulated function BounceHitWall(vector HitNormal, actor Wall)
{
   return;
}

// don't kaboom on wall 
simulated function KaboomHitWall(vector HitNormal, actor Wall)
{
   return;
}

// Don't touch me
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{ 
   return;
}

// Play a sound and destroy
simulated function Explode(vector HitLocation, vector HitNormal)
{
   if (bHasExploded)
      return;

   bHasExploded = True;

   Destroy();
}

defaultproperties
{
   ExplosionSound=none
}

