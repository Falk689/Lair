class DualMK23PickupFalk extends KFMod.DualMK23Pickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   cost=500
   BuyClipSize=24
   PowerValue=15
   SpeedValue=49
   RangeValue=100
   AmmoCost=24
   ItemName="Dual Heckler & Koch MK23s"
   ItemShortName="MK23s"
   AmmoItemName=".45 ACP rounds"
   InventoryType=Class'DualMK23PistolFalk'
   PickupMessage="You found another Heckler & Koch MK23"
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
