class MTS255PickupFalk extends MTS255.MTS255Pickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=6.000000
   InventoryType=Class'MTS255Falk'
   cost=950
   BuyClipSize=5
   AmmoCost=10
   ItemName="MTs255 Semi-Automatic Shotgun"
   ItemShortName="MTs255"
   PickupMessage="You got an MTs255 Semi-Automatic Shotgun"
   PowerValue=42
   SpeedValue=70
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}