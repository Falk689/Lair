class MKb42PickupFalk extends KFMod.MKb42Pickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
	Weight=6.000000
	cost=2200
	AmmoCost=15
	BuyClipSize=30
	PowerValue=46
    SpeedValue=45
    RangeValue=100
	ItemName="Sturmgewehr 44"
	ItemShortName="StG44"
	AmmoItemName="7.92x33mm Kurz rounds"
	InventoryType=Class'MKb42AssaultRifleFalk'
	PickupMessage="You got a Sturmgewehr 44"
	PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}