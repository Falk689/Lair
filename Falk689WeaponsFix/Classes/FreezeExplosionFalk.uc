class FreezeExplosionFalk extends KFNadeExplosion;

simulated function PostBeginPlay()
{
   bDynamicLight = false;
	Super(Emitter).Postbeginplay();
}

simulated function NadeLight()
{
}

defaultproperties
{
     Begin Object Class=SpriteEmitter Name=SpriteEmitter
         FadeOut=True
         RespawnDeadParticles=False
         SpinParticles=True
         UseSizeScale=True
         UseRegularSizeScale=False
         UniformSize=True
         BlendBetweenSubdivisions=True
         Acceleration=(Z=100.000000)
         ColorScale(0)=(Color=(B=255,G=255,R=255,A=255))
         ColorScale(1)=(RelativeTime=1.000000,Color=(B=255,G=255,R=255,A=255))
         MaxParticles=50
         StartLocationShape=PTLS_Sphere
         SpinsPerSecondRange=(X=(Max=1.000000))
         StartSpinRange=(X=(Max=1.000000))
         SizeScale(0)=(RelativeSize=-5.000000)
         StartSizeRange=(X=(Min=6.000000,Max=60.000000),Y=(Min=6.000000,Max=60.000000),Z=(Min=6.000000,Max=60.000000))
         Texture=Texture'LairTextures_T.Grenades.FreezeGrenadeExplosion'
         TextureUSubdivisions=4
         TextureVSubdivisions=4
         LifetimeRange=(Min=0.801000,Max=1.000000)
         StartVelocityRange=(X=(Min=-500.000000,Max=500.000000),Y=(Min=-500.000000,Max=500.000000),Z=(Max=100.000000))
     End Object
     Emitters(0)=SpriteEmitter'FreezeExplosionFalk.SpriteEmitter'

     Emitters(1)=None

     Emitters(2)=None

     Emitters(3)=None

     Emitters(4)=None

     LightType=None
     LightSaturation=0
     LightBrightness=0.000000
     LightRadius=0.000000
}
