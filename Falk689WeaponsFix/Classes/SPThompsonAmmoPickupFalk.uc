class SPThompsonAmmoPickupFalk extends KFMod.SPThompsonAmmoPickup;

defaultproperties
{
    AmmoAmount=50
    InventoryType=Class'SPThompsonAmmoFalk'
    PickupMessage="You found some .45 ACP rounds"
}