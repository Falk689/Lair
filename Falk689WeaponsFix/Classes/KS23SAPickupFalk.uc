class KS23SAPickupFalk extends KS23SAPickupBaseFalk;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=6.000000
   cost=2750
   BuyClipSize=4
   AmmoCost=8
   PowerValue=96
   SpeedValue=25
   RangeValue=100
   ItemName="KS23 Special Carbine"
   ItemShortName="KS23"
   AmmoItemName="KS23 shells"
   InventoryType=Class'KS23SAShotgunFalk'
   PickupMessage="You got a KS23 Special Carbine"
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
