class NadeFalk extends Nade;

#exec OBJ LOAD FILE=LairTextures_T.utx

var class<projectile> fCClass;              // cluster class, what we spawn on kaboom
var byte              BulletID;             // used to prevent multi hit on a single zed
var byte              fNClusters;           // How many clusters we spawn on kaboom
var byte              fSClusters;           // How many clusters we have spawned
var byte              fTry;                 // How many times we tried to spawn a cluster
var byte              fMaxTry;              // How many times we should try to spawn a cluster
var vector            fCSpawnLoc;           // Where to spawn clusters
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction
var vector            fSuccessPos;          // Success position so we don't have to do all the while again
var vector            fTempPos;             // Temp position so we can set success pos only on actual success
var float             fCLastCheck;          // When was the last zed-time check
var float             fCheckSecs;           // Time between checks
var float             fLifeTime;            // Time this projectile was spawned
var bool              fSuccessSpawn;        // have we found a place to spawn stuff?
var bool              fShouldKaboom;        // Should we spawn clusters?
var bool              fShouldCheck;         // should check cluster math on tick
var Controller        fCInstigator;         // instigator controller, I don't use vanilla InstigatorController 'cause of reasons
var bool              fTimerSet;            // safety to prevent setting the timer twice
var bool              fSirenImmune;         // Is this projectile immune to sirens?
var float             fClotHeadMulti;       // clot head multiplier

replication
{
   reliable if(Role == ROLE_Authority)
		fSirenImmune, BulletID, fCInstigator, fSuccessSpawn, fSuccessPos, fTimerSet;
}

// Initial clusters check if we're on zedtime on spawn
simulated function PostBeginPlay()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   if (Instigator != none)
   {
      if (Role == ROLE_Authority && fCInstigator == none && Instigator.Controller != None)
         fCInstigator = Instigator.Controller;

      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

      if (KFPRI != none)
      {
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none) 
         {
            fShouldKaboom = Vet.Static.DetonateBulletsControlBonus(KFPRI);
            fSirenImmune  = Vet.Static.KaboomSirenImmunity(KFPRI);
         }
      }
   }

   if (Role == ROLE_Authority)
   {
      Velocity = Speed * Vector(Rotation);

      RandSpin(25000);
      bCanHitOwner = false;

      if (Instigator.HeadVolume.bWaterVolume)
      {
         bHitWater = true;
         Velocity = 0.6*Velocity;
      }
   }
}

// Initial timer setting
function PostNetBeginPlay()
{
	SetTimer(ExplodeTimer, false);
   fTimerSet = true;
}


// Spawn clusters on explode if we're on zedtime, with second check
simulated function Explode(vector HitLocation, vector HitNormal)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;
   local byte i;
   local Projectile P;
   local vector fPLocation;
   local vector SpawnCorrection;

   SpawnCorrection = vect(0,0,0);

   if (bHasExploded)
      return;

   if (Role == ROLE_Authority && fShouldCheck)
   {
      // Check again if we should spawn explosive shrapnels

      if (Instigator != none)
      {
         if (!fShouldKaboom)
         {
            KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

            if (KFPRI != none)
            {
               Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

               if (Vet != none && Vet.Static.DetonateBulletsControlBonus(KFPRI))
                  fShouldKaboom = True;
            }
         }

         if (fShouldKaboom)
         {
            // attempt to fix ground cluster spawn
            fPLocation = Instigator.Location + fCZRetryLoc;

            //log(Location.Z);
            //log(fPLocation.Z);

            if (Location.Z <= fPLocation.Z) // if the nade if under the player, correct spawn position
               SpawnCorrection = fCSpawnLoc;
         }
      }

      // Kaboom
      if (fShouldKaboom)
      {
         for(i=0; i<fNClusters; i++)
         {
            class<M79ClusterFalk>(fCClass).Default.BulletID = BulletID + fSClusters;

            P = Spawn(fCClass,,, Location + SpawnCorrection, RotRand(True));

            if (P != none) 
            {
               fSClusters++;
               P.Instigator   = Instigator;

               if (M79ClusterFalk(P) != none)
                  M79ClusterFalk(P).fCInstigator = fCInstigator;

               if (M79ClusterFalk(P) != none) // pass siren immunity state to the cluster
                  M79ClusterFalk(P).default.fSirenImmune = fSirenImmune;
            }
         }

         //log("Initial spawn");
         //log(fSClusters);
      }
   }

   FalkExplode(HitLocation, HitNormal);
}


// Overridden to prevent clusters from blowing up our nade
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
   local FalkMonsterBase Siren;

   if (damageType == class'SirenScreamDamage')
   {
      Siren = FalkMonsterBase(InstigatedBy);

      if (!fSirenImmune && Siren != none && !Siren.fIsStunned && !Siren.fFrozen && Siren.HeadHealth > 0 && !Siren.bDecapitated)
         Disintegrate(HitLocation, vect(0,0,1));
   }
 
   else if ((!bHasExploded) && 
            (InstigatedBy == Instigator && damageType != class'DamTypeExplosiveBulletFalk') || 
            (KFHumanPawn(InstigatedBy) == none && (damageType == class'DamTypeFrag' || damageType == class'DamTypeBurned')))
      Explode(HitLocation, vect(0,0,0));
}

// Timed check to see if we should spawn clusters
simulated function Tick(float DeltaTime)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;
   local Projectile P;
   local byte fAttempt;

   Super.Tick(DeltaTime);

   if (Role == ROLE_Authority && fShouldCheck)
   {
      fLifeTime += DeltaTime;

      if (!bHasExploded && !fShouldKaboom && fLifeTime >= fCLastCheck + fCheckSecs && Instigator != none)
      {
         KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

         fCLastCheck = fLifeTime;

         if (KFPRI != none)
         {
            Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

            if (Vet != none && Vet.Static.DetonateBulletsControlBonus(KFPRI))
               fShouldKaboom = True;
         }
      }

      if (fShouldKaboom && bHasExploded)
      {
         if (fSClusters < fNClusters)
         {
            // Try to spawn remaining clusters
            if (fTry < fMaxTry)
            {
               class<M79ClusterFalk>(fCClass).Default.BulletID = BulletID + fSClusters;

               if (fSuccessSpawn)
               {
                  P = Spawn(fCClass,,, fSuccessPos, RotRand(True));

                  if (P == None)
                     fSuccessSpawn = False;
               }

               if (!fSuccessSpawn)
               {
                  While (P == None && fAttempt < 27)
                  { 
                     fAttempt++; // we don't even test 0 since we're here for a reason

                     if (fAttempt >= 27)
                     {
                        //warn("FAIL"@fTry);
                        fACZRetryLoc += fCZRetryLoc;
                        fACXRetryLoc += fCXRetryLoc;
                        fACYRetryLoc += fCYRetryLoc;
                        fTry++;
                     }


                     else if (fAttempt >= 18)
                     {
                        fTempPos = FClusterQuad(Location - fACZRetryLoc, fAttempt - 18);
                        //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
                     }

                     else if (fAttempt >= 9)
                     {
                        fTempPos = FClusterQuad(Location + fACZRetryLoc, fAttempt - 9);
                        //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
                     }

                     else
                     {
                        fTempPos = FClusterQuad(Location, fAttempt);
                        //warn("First:"@fAttempt@"Location:"@fTempPos);
                     }

                     P = Spawn(fCClass,,, fTempPos, RotRand(True));
                  }
               }

               if (P != none)
               {
                  //warn("Spawned");
                  fSuccessPos   = fTempPos; 
                  fSuccessSpawn = True;
                  P.Instigator  = Instigator; 

                  fSClusters++;

                  fTry          = 0;

                  if (Vet == none)
                  {

                     KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

                     if (KFPRI != none)
                        Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);
                  }

                  if (Vet != none)
                     P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Instigator), P.Damage, Class'KFMod.DamTypeM79Grenade');

                  if (M79ClusterFalk(P) != none)
                     M79ClusterFalk(P).fCInstigator = fCInstigator;
               }
            }

            // Give up
            else
            {
               //warn("Giving up");
               fACZRetryLoc = fCZRetryLoc;
               fACXRetryLoc = fCXRetryLoc;
               fACYRetryLoc = fCYRetryLoc;
               fSClusters   = fNClusters;
               fTry = 0;
               Destroy();
            }
         }

         else
            Destroy();
      }
   }
}


// attempt to spawn clusters in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}


// Explode but check if we spawned all clusters before destroy
simulated function FalkExplode(vector HitLocation, vector HitNormal)
{
   local PlayerController  LocalPlayer;
   local Projectile P;
   local byte i;

   bHasExploded = True;
   BlowUp(HitLocation);

   PlaySound(ExplodeSounds[rand(ExplodeSounds.length)],,2.0);

   // Shrapnel
   for( i=Rand(6); i<10; i++ )
   {
      P = Spawn(ShrapnelClass,,,,RotRand(True));
      if( P!=None )
         P.RemoteRole = ROLE_None;
   }

   Spawn(class'KaboomFalk',,,HitLocation + HitNormal*20,rotator(HitNormal));
   Spawn(ExplosionDecal,self,,HitLocation, rotator(-HitNormal));

   if (Role != ROLE_Authority || !fShouldKaboom || fSClusters >= fNClusters)
   {
      fShouldCheck = False;
      Destroy();
   }

   // Shake nearby players screens
   LocalPlayer = Level.GetLocalPlayerController();
   if ( (LocalPlayer != None) && (VSize(Location - LocalPlayer.ViewTarget.Location) < (DamageRadius * 1.5)) )
      LocalPlayer.ShakeView(RotMag, RotRate, RotTime, OffsetMag, OffsetRate, OffsetTime);

}

// Touch only if we haven't exploded yet and don't hit broken zeds heads
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{
   local FalkMonsterBase P;
   local float fHeadMulti;
   local vector X;

   X = Vector(Rotation);

   // hopefully shatter glasses more reliably
   if (KFPawn(Other) == none && KFPawn(Other.Base) == none && KFDoorMover(Other) == none && KFDoorMover(Other.Base) == none)
      Other.TakeDamage(100, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), class'DamTypeFragImpactFalk');

   if (!bHasExploded)
   {
      P = FalkMonsterBase(Other);

      if (P != none)
      {
         fHeadMulti = P.FalkGetNadeHeadScale();

         if (fHeadMulti <= 0 || !P.IsHeadShot(HitLocation, X, fHeadMulti))
            Super.ProcessTouch(Other, HitLocation);
      }
   }
}


// prevent setting ExplodeTimer twice
simulated function HitWall(vector HitNormal, actor Wall)
{
   local Vector VNorm;
   local PlayerController PC;

   // hopefully shatter glasses more reliably
   //if (KFPawn(Wall) == none && KFPawn(Wall.Base) == none && KFDoorMover(Wall) == none && KFDoorMover(Wall.Base) == none)
   //   Wall.TakeDamage(100, Instigator, Location, HitNormal, class'DamTypeFragImpactFalk');

   if ((Pawn(Wall) != None) || (GameObjective(Wall) != None))
   {
      Explode(Location, HitNormal);
      return;
   }

   if (!fTimerSet)
   {
      SetTimer(ExplodeTimer * 0.8, false);
      fTimerSet = true;
   }

   // Reflect off Wall w/damping
   VNorm = (Velocity dot HitNormal) * HitNormal;
   Velocity = -VNorm * DampenFactor + (Velocity - VNorm) * DampenFactorParallel;

   RandSpin(50000);
   DesiredRotation.Roll = 0;
   RotationRate.Roll = 0;
   Speed = VSize(Velocity);

   if (Speed < 20)
   {
      bBounce = False;
      PrePivot.Z = -1.5;
      SetPhysics(PHYS_None);
      DesiredRotation = Rotation;
      DesiredRotation.Roll = 0;
      DesiredRotation.Pitch = 0;
      SetRotation(DesiredRotation);

      if (Trail != None)
         Trail.mRegen = false; // stop the emitter from regenerating
   }

   else
   {
      if ((Level.NetMode != NM_DedicatedServer) && (Speed > 50))
         PlaySound(ImpactSound, SLOT_Misc);

      else
      {
         bFixedRotationDir = false;
         bRotateToDesired = true;
         DesiredRotation.Pitch = 0;
         RotationRate.Pitch = 50000;
      }

      if (!Level.bDropDetail && (Level.DetailMode != DM_Low) && (Level.TimeSeconds - LastSparkTime > 0.5) &&
         EffectIsRelevant(Location,false))
      {
         PC = Level.GetLocalPlayerController();

         if ((PC.ViewTarget != None) && VSize(PC.ViewTarget.Location - Location) < 6000)
            Spawn(HitEffectClass,,, Location, Rotator(HitNormal));

         LastSparkTime = Level.TimeSeconds;
      }
   }
}

// use BulletID to damage zeds
simulated function HurtRadius( float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation )
{
   local actor Victims;
   local float damageScale, dist;
   local vector dirs;
   local int NumKilled;
   local KFMonster KFMonsterVictim;
   local Pawn P;
   local KFPawn KFP;
   local array<Pawn> CheckedPawns;
   local int i;
   local bool bAlreadyChecked;


   if ( bHurtEntry )
      return;

   bHurtEntry = true;

   foreach CollidingActors (class 'Actor', Victims, DamageRadius, HitLocation)
   {
      // don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
      if((Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo')
            && ExtendedZCollision(Victims) == None)
      {
         dirs = Victims.Location - HitLocation;
         dist = FMax(1,VSize(dirs));
         dirs = dirs/dist;
         damageScale = 1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius);

         Victims.SetDelayedDamageInstigatorController(fCInstigator);

         if (Victims == LastTouched)
            LastTouched = None;

         P = Pawn(Victims);

         if( P != none )
         {
            for (i = 0; i < CheckedPawns.Length; i++)
            {
               if (CheckedPawns[i] == P)
               {
                  bAlreadyChecked = true;
                  break;
               }
            }

            if( bAlreadyChecked )
            {
               bAlreadyChecked = false;
               P = none;
               continue;
            }

            KFMonsterVictim = KFMonster(Victims);

            if( KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
            {
               KFMonsterVictim = none;
            }

            KFP = KFPawn(Victims);

            if( KFMonsterVictim != none )
            {
               damageScale *= KFMonsterVictim.GetExposureTo(HitLocation/*Location + 15 * -Normal(PhysicsVolume.Gravity)*/);
            }
            else if( KFP != none )
            {
               damageScale *= KFP.GetExposureTo(HitLocation/*Location + 15 * -Normal(PhysicsVolume.Gravity)*/);
            }

            CheckedPawns[CheckedPawns.Length] = P;

            if ( damageScale <= 0)
            {
               P = none;
               continue;
            }
            else
            {
               //Victims = P;
               P = none;
            }
         }

         if(Victims == Instigator)
            damageScale *= 0.3;

         Victims.TakeDamage
            (
             damageScale * DamageAmount,
             Instigator,
             Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs,
             (damageScale * Momentum * dirs),
             DamageType,
             BulletID
            );
         if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
            Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, fCInstigator, DamageType, Momentum, HitLocation);

         if( Role == ROLE_Authority && KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
         {
            NumKilled++;
         }
      }
   }
   if ((LastTouched != None) && (LastTouched != self) && (LastTouched.Role == ROLE_Authority) && !LastTouched.IsA('FluidSurfaceInfo'))
   {
      Victims = LastTouched;
      LastTouched = None;
      dirs = Victims.Location - HitLocation;
      dist = FMax(1,VSize(dirs));
      dirs = dirs/dist;
      damageScale = FMax(Victims.CollisionRadius/(Victims.CollisionRadius + Victims.CollisionHeight),1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius));

      Victims.SetDelayedDamageInstigatorController(fCInstigator);

      if(Victims == Instigator)
         damageScale *= 0.3;

      Victims.TakeDamage
         (
          damageScale * DamageAmount,
          Instigator,
          Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs,
          (damageScale * Momentum * dirs),
          DamageType,
          BulletID
         );
      if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
         Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, fCInstigator, DamageType, Momentum, HitLocation);
   }

   bHurtEntry = false;
}

defaultproperties
{
   Speed=850.0
   fCClass=class'NadeClusterFalk'
   fNClusters=3
   fShouldKaboom=False
   fCheckSecs=0.500000
   fMaxTry=3
	ExplodeTimer=2.000000
   fCSpawnLoc=(Z=10.000000)
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
   Damage=350
   StaticMesh=StaticMesh'LairStaticMeshes_SM.Grenades.DemomanGrenade'
   fSirenImmune=False
   fShouldCheck=True
   fClotHeadMulti=3.0
   ExplosionDecal=class'ScorchMarkFalk'
}
