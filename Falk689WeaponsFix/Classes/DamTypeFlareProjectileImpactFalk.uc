class DamTypeFlareProjectileImpactFalk extends KFProjectileWeaponDamageType;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount)
{
	KFStatsAndAchievements.AddFlameThrowerDamage(Amount);
}

defaultproperties
{
   HeadShotDamageMult=1.100000
}
