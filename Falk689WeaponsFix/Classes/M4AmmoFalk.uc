class M4AmmoFalk extends KFMod.M4Ammo;

defaultproperties
{
   MaxAmmo=390
   InitialAmount=160
   AmmoPickupAmount=30
   PickupClass=Class'M4AmmoPickupFalk'
   ItemName="5.56mm rounds"
}
