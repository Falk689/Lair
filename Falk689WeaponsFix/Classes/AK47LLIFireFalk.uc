class AK47LLIFireFalk extends KFMod.AK47Fire;

var byte  BID;                // bullet ID to prevent multiple hits on a single zed

// multi-hit, pawn penetration and invisible head fix for normal hitscan weapons
function DoTrace(Vector Start, Rotator Dir)
{
   local Vector           X,Y,Z, End, HitLocation, HitNormal, ArcEnd;
   local Actor            Other, DamageActor, OldDamageActor;
   local array<int>       HitPoints;
   local int              bHP;
   local byte             Retries;

   BID++;

   if (BID > 200)
      BID = 1;

   MaxRange();

   Weapon.GetViewAxes(X, Y, Z);

   if (Weapon.WeaponCentered())
      ArcEnd = (Instigator.Location + Weapon.EffectOffset.X * X + 1.5 * Weapon.EffectOffset.Z * Z);

   else
   {
      ArcEnd = (Instigator.Location + Instigator.CalcDrawOffset(Weapon) + Weapon.EffectOffset.X * X +
            Weapon.Hand * Weapon.EffectOffset.Y * Y + Weapon.EffectOffset.Z * Z);
   }

   X = Vector(Dir);
   End = Start + TraceRange * X;

   while (Retries < 100)
   {
      Retries    += 1;
      DamageActor = none;

      Other = Instigator.HitPointTrace(HitLocation, HitNormal, End, HitPoints, Start,, 1);

      if (Other == None)
      {
         start = HitLocation;
         continue;
      }

      if (Other == Instigator || KFHumanPawn(Other) != None || Other.Base == Instigator || KFHumanPawn(Other.Base) != None)
      {
         start = HitLocation + X;
         continue;
      }

      if (ExtendedZCollision(Other) != None && Other.Owner != None)
         Other = Pawn(Other.Owner);

      if (!Other.bWorldGeometry && Other != Level)
      {
         if (KFMonster(Other) != None)
            DamageActor = Other;

         if (KFMonster(DamageActor) != None)
         {
            if (DamageActor == OldDamageActor)
            {
               start = HitLocation + X;
               continue;
            }

            OldDamageActor = DamageActor;
            bHP            = KFMonster(DamageActor).Health;

            if (bHP <= 0)
            {
               start = HitLocation + X;
               continue;
            }

            DamageActor.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);

            if (KFMonster(DamageActor).Health == bHP)
            {
               start = HitLocation + X;
               continue;
            }

            return;
         }

         else
            Other.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);
      }

      else if (BlockingVolume(Other) == None)
      {
         if(KFWeaponAttachment(Weapon.ThirdPersonActor) != None)
            KFWeaponAttachment(Weapon.ThirdPersonActor).UpdateHit(Other, HitLocation, HitNormal);

         break;
      }
   }
}

defaultproperties
{
    RecoilRate=0.080000
    maxVerticalRecoilAngle=500
    maxHorizontalRecoilAngle=250
    bRecoilRightOnly=True
    ShellEjectClass=Class'AK47LLINewMut.KFShellEjectAK47LLI'
    ShellEjectBoneName="Shell_eject"
    bAccuracyBonusForSemiAuto=True
    bRandomPitchFireSound=False
    NoAmmoSound=Sound'AK47LLINew_A.AK47LLINew_Snd.AK47LLI_empty'
    DamageType=Class'DamTypeAK47LLIAssaultRifleFalk'
    DamageMax=45
    DamageMin=45
    Momentum=18500.000000
    bPawnRapidFireAnim=True
    TransientSoundVolume=1.800000
    TweenTime=0.025000
    FireRate=0.109000
    AmmoClass=Class'AK47LLIAmmoFalk'
    ShakeRotMag=(X=50.000000,Y=50.000000,Z=350.000000)
    ShakeRotRate=(X=5000.000000,Y=5000.000000,Z=5000.000000)
    ShakeRotTime=0.750000
    ShakeOffsetMag=(X=6.000000,Y=3.000000,Z=7.500000)
    ShakeOffsetRate=(X=1000.000000,Y=1000.000000,Z=1000.000000)
    ShakeOffsetTime=1.250000
    aimerror=42.000000
    Spread=0.025000
}
