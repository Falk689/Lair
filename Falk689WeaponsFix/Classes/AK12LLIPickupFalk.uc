class AK12LLIPickupFalk extends AK12LLIMut.AK12LLIPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=6.000000
   cost=2000
   InventoryType=Class'AK12LLIAssaultRifleFalk'
   BuyClipSize=35
   AmmoCost=18
   ItemName="Silenced AK12 Assault Rifle"
   ItemShortName="Silenced AK12"
   PickupMessage="You got a Silenced AK12 Assault Rifle"
   PowerValue=37
   SpeedValue=45
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}