class KrissMMedicGunFalk extends MP7MMedicGunFalk;

#exec OBJ LOAD FILE="LairAnimations_A.ukx"

// vanilla stuff
simulated event OnZoomOutFinished()
{
   local name anim;
   local float frame, rate;

   GetAnimParams(0, anim, frame, rate);

   if (ClientState == WS_ReadyToFire)
   {
      // Play the regular idle anim when we're finished zooming out
      if (anim == IdleAimAnim)
      {
         PlayIdle();
      }
      // Switch looping fire anims if we switched to/from zoomed
      else if( FireMode[0].IsInState('FireLoop') && anim == 'Fire_Iron_Loop')
      {
         LoopAnim('Fire_Loop', FireMode[0].FireLoopAnimRate, FireMode[0].TweenTime);
      }
   }
}

//Called by the native code when the interpolation of the first person weapon to the zoomed position finishes
simulated event OnZoomInFinished()
{
   local name anim;
   local float frame, rate;

   GetAnimParams(0, anim, frame, rate);

   if (ClientState == WS_ReadyToFire)
   {
      // Play the iron idle anim when we're finished zooming in
      if (anim == IdleAnim)
      {
         PlayIdle();
      }
      // Switch looping fire anims if we switched to/from zoomed
      else if( FireMode[0].IsInState('FireLoop') && anim == 'Fire_Loop' )
      {
         LoopAnim('Fire_Iron_Loop', FireMode[0].FireLoopAnimRate, FireMode[0].TweenTime);
      }
   }
}


// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
      {
         DetachFromPawn(Instigator);

         if (FHumanPawn(Instigator) != none && fHealingAmmoFalk > -1)
         {
            //warn("Storing ammo to the pawn: "@fHealingAmmoFalk);
            FHumanPawn(Instigator).fStoredSyringeAmmo = fHealingAmmoFalk;
            FHumanPawn(Instigator).fStoredSyringeTime = Level.TimeSeconds;
         }
      }

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      // storing ammo into the pickup
      if (KrissMPickupFalk(Pickup) != none)
      {
         //warn("Storing ammo to the pickup: "@fHealingAmmoFalk);
         KrissMPickupFalk(Pickup).fStoredAmmo = fHealingAmmoFalk;
      }

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }

   // we failed to drop this weapon on death, award us some cash instead
   else if (Instigator.Health <= 0 && Instigator.PlayerReplicationInfo != none)
   {
      Instigator.PlayerReplicationInfo.Score += SellValue;
      
      if (FHumanPawn(Instigator) != none)
         FHumanPawn(Instigator).FalkSetDosh();
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}

defaultproperties
{
   HealBoostAmount=20
   AmmoRegenRate=0.200000 // smg fast recharge rate standard
   MagCapacity=25
   ReloadRate=3.4
   ReloadShortRate=2.62
   WeaponReloadAnim="Reload_Kriss"
   SleeveNum=1
   Weight=4.00000
   bIsTier3Weapon=True
   FireModeClass(0)=Class'KrissMFireFalk'
   FireModeClass(1)=Class'KrissMAltFireFalk'
   SelectSoundRef="KF_KrissSND.KF_WEP_KRISS_Handling_Select"
   HudImage=Texture'KillingFloor2HUD.WeaponSelect.KRISS_unselected'
   HudImageRef="KillingFloor2HUD.WeaponSelect.KRISS_unselected"
   MeshRef="LairAnimations_A.KrissMesh"
   SkinRefs(0)="Kf_Weapons9_Trip_T.Weapons.Medic_Kriss_cmb"
   Mesh=SkeletalMesh'LairAnimations_A.KrissMesh'
   Skins(0)=Combiner'Kf_Weapons9_Trip_T.Weapons.Medic_Kriss_cmb'
   SelectedHudImage=Texture'KillingFloor2HUD.WeaponSelect.KRISS'
   SelectedHudImageRef="KillingFloor2HUD.WeaponSelect.KRISS"
   Description="The Vector is a submachine gun chambered for .45 ACP rounds. Larger magazine capacity than an MP7-M, but same fire rate. Faster reload than an MP5-M, and higher damage. This particular version is capable of firing medical darts."
   Priority=215
   GroupOffset=17
   PickupClass=Class'KrissMPickupFalk'
   ItemName="Medical KRISS Vector Super V SMG"
   TraderInfoTexture=texture'KillingFloor2HUD.Trader_Weapon_Icons.Trader_KRISS'
   AttachmentClass=Class'KFMod.KrissMAttachment'
}
