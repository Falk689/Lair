class KatanaPickupFalk extends KFMod.KatanaPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

DefaultProperties
{
    Weight=3.000000
    cost=900
    PowerValue=39
    SpeedValue=70
    RangeValue=38
    ItemName="Katana"
    ItemShortName="Katana"
    InventoryType=Class'KatanaFalk'
    PickupMessage="You got a Katana"
    PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
