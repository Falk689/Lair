class MediShotAmmoPickupFalk extends KFAmmoPickup;

defaultproperties
{
   AmmoAmount=4
   InventoryType=Class'MediShotAmmoFalk'
   PickupMessage="You got 12-gauge shells"
   StaticMesh=None
}
