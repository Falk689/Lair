class MediShotAmmoFalk extends KFAmmunition;

#EXEC OBJ LOAD FILE=InterfaceContent.utx

defaultproperties
{
   AmmoPickupAmount=8
   MaxAmmo=48
   InitialAmount=24
   PickupClass=Class'MediShotAmmoPickupFalk'
   IconMaterial=Texture'KillingFloorHUD.Generic.HUD'
   IconCoords=(X1=413,Y1=82,X2=457,Y2=125)
}
