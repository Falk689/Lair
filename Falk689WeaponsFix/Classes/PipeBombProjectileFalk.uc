class PipeBombProjectileFalk extends PipeBombProjectile;

var class<projectile> fCClass;              // cluster class, what we spawn on kaboom
var byte              BulletID;             // used to prevent multi hit on a single zed
var byte              fNClusters;           // How many clusters we spawn on kaboom
var byte              fSClusters;           // How many clusters we have spawned
var byte              fTry;                 // How many times we tried to spawn a cluster
var byte              fMaxTry;              // How many times we should try to spawn a cluster
var vector            fCSpawnLoc;           // Where to spawn clusters
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction
var vector            fSuccessPos;          // Success position so we don't have to do all the while again
var vector            fTempPos;             // Temp position so we can set success pos only on actual success
var float             fSleepTimeNormal;     // sleep time at normal difficulty
var float             fSleepTimeHard;       // sleep time at hard difficulty
var float             fSleepTimeSuicidal;   // sleep time at suicidal difficulty
var float             fSleepTimeHOE;        // sleep time at hell on earth difficulty
var float             fSleepTimeBB;         // sleep time at bloodbath difficulty
var float             fUsedSleepTime;       // used time between checks to see if zeds are around
var float             fDamageMulti;         // stored damage multiplier, passed to the clusters
var float             fCLastCheck;          // When was the last zed-time check
var float             fCheckSecs;           // Time between checks
var float             fLifeTime;            // Time this projectile was spawned
var float             fMinDamageMulti;      // Minimum damage multiplier of this mine despite distance
var bool              fStoredKaboom;        // Stored kaboom state, used to spawn clusters if our instigator died
var bool              fSuccessSpawn;        // have we found a place to spawn stuff?
var bool              fShouldKaboom;        // Should we spawn clusters?
var bool              fShouldCheck;         // should check cluster math on tick
var Controller        fCInstigator;         // instigator controller, I don't use vanilla InstigatorController 'cause of reasons
var bool              fSirenImmune;         // Is this projectile immune to sirens?
var class<Projectile> sFClass;              // Sound fix class, a projectile that just does kaboom

replication
{
   reliable if(Role == ROLE_Authority)
      fSirenImmune, BulletID, fCInstigator, fSuccessSpawn, fSuccessPos;
}

// Initial clusters check if we're on zedtime on spawn
simulated function PostBeginPlay()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   Super.PostBeginPlay();

   if (Level != none && Level.Game != none)
   {
      // Setting sleep timer based on difficulty
      if (Level.Game.GameDifficulty < 3.0)
         fUsedSleepTime = fSleepTimeNormal;

      else if (Level.Game.GameDifficulty < 5.0)
         fUsedSleepTime = fSleepTimeHard;

      else if (Level.Game.GameDifficulty < 6.0)
         fUsedSleepTime = fSleepTimeSuicidal;

      else if (Level.Game.GameDifficulty < 8.0)
         fUsedSleepTime = fSleepTimeHOE;

      else
         fUsedSleepTime = fSleepTimeBB;
   }

   // skills related stuff
   if (Instigator != none)
   {
      if (Role == ROLE_Authority && fCInstigator == none && Instigator.Controller != None)
         fCInstigator = Instigator.Controller;

      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

      if (KFPRI != none)
      {
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none) 
         {
            fShouldKaboom = Vet.Static.DetonateBulletsControlBonus(KFPRI);
            fSirenImmune  = Vet.Static.KaboomSirenImmunity(KFPRI);
         }
      }
   }
}

// Spawn clusters on explode if we're on zedtime, with second check
simulated function Explode(vector HitLocation, vector HitNormal)
{
   //local KFPlayerReplicationInfo KFPRI;
   //local class<FVeterancyTypes> Vet;
   local byte i;
   local Projectile P;
   local Falk689GameTypeBase Flk;

   if (bHasExploded)
      return;

   if (Role == ROLE_Authority)
   {
      // sound fix
      P = Spawn(sFClass,,, Location + fCSpawnLoc, rotator(vect(0,0,1)));

      if (P == none) 
         P = Spawn(sFClass,,, Location + fCSpawnLoc + fCZRetryLoc, rotator(vect(0,0,1)));

      // clusters
      if (fShouldCheck)
      {
         // Check again if we should spawn explosive shrapnels
         if (!fShouldKaboom)
         {

            if (fStoredKaboom)
            {
               Flk = Falk689GameTypeBase(Level.Game);

               if (Flk != None)
                  fShouldKaboom = Flk.InZedTime();
            }

            /*if (Instigator != none)
              {
              KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

              if (KFPRI != none)
              {
              Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

              if (Vet != none && Vet.Static.DetonateBulletsControlBonus(KFPRI))
              fShouldKaboom = True;
              }
              }*/
         }

         // Kaboom
         if (fShouldKaboom)
         {
            for(i=0; i<fNClusters; i++)
            {
               class<M79ClusterFalk>(fCClass).Default.BulletID = BulletID + fSClusters;

               P = Spawn(fCClass,,, Location + fCSpawnLoc, RotRand(True));

               if (P != none) 
               {
                  fSClusters++;
                  P.Instigator = Instigator;

                  /*if (Vet == none)
                    {
                    KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

                    if (KFPRI != none)
                    Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);
                    }*/

                  //if (Vet != none)
                  P.Damage = P.Damage *= fDamageMulti; //KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Instigator), P.Damage, P.MyDamageType);
                  //warn("Cluster Damage:"@P.Damage@"- Default:"@P.Default.Damage);

                  if (M79ClusterFalk(P) != none) // pass siren immunity state to the cluster
                  {
                     M79ClusterFalk(P).fCInstigator = fCInstigator;
                     M79ClusterFalk(P).default.fSirenImmune = fSirenImmune;
                  }
               }
            }

            //log("Initial spawn");
            //log(fSClusters);
         }
      }
   }

   FalkExplode(HitLocation, HitNormal);
}

// Overridden to prevent most weapons from blowing up our bomb
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
   local FalkMonsterBase Siren;

   if (damageType == class'SirenScreamDamage')
   {
      Siren = FalkMonsterBase(InstigatedBy);

      if (!fSirenImmune && Siren != none && !Siren.fIsStunned && !Siren.fFrozen && Siren.HeadHealth > 0 && !Siren.bDecapitated)
         Disintegrate(HitLocation, vect(0,0,1));
   }

   else if (!bHasExploded && 
         (InstigatedBy == Instigator &&
          damageType != class'DamTypeExplosiveBulletFalk'          &&
          (class<DamTypeFrag>(damageType)                != none   ||
           //class<DamTypePipeBomb>(damageType)            != none   ||
           class<DamTypeM79Grenade>(damageType)          != none   ||
           class<DamTypeM32Grenade>(damageType)          != none   ||
           class<DamTypeM203GrenadeFalk>(damageType)     != none   ||
           class<DamTypeRocketImpact>(damageType)        != none   ||
           class<DamTypeSPGrenade>(damageType)           != none   ||
           class<DamTypeSealSquealExplosion>(damageType) != none   ||
           class<DamTypeSeekerSixRocketFalk>(damageType) != none   ||
           class<DamTypeLAWFalk>(damageType)             != none   ||
           //class<DamTypeATMine>(damageType)              != none   ||
           class<DamTypeTrigunFalk>(damageType)          != none)) ||
         (KFHumanPawn(InstigatedBy) == none && (damageType == class'DamTypeFrag' || damageType == class'DamTypeBurned')))
      Explode(HitLocation, vect(0,0,0));
}


// Timed check to see if we should spawn clusters
simulated function Tick(float DeltaTime)
{
   /*local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;*/
   local Projectile P;
   local byte fAttempt;

   Super.Tick(DeltaTime);

   if (Role == ROLE_Authority && fShouldCheck)
   {
      fLifeTime += DeltaTime;

      /*if (!bHasExploded && !fShouldKaboom && fLifeTime >= fCLastCheck + fCheckSecs && Instigator != none)
      {
         KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

         fCLastCheck = fLifeTime;

         if (KFPRI != none)
         {
            Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

            if (Vet != none && Vet.Static.DetonateBulletsControlBonus(KFPRI))
               fShouldKaboom = True;
         }
      }*/

      if (fShouldKaboom && bHasExploded)
      {
         if (fSClusters < fNClusters)
         {
            // Try to spawn remaining clusters
            if (fTry < fMaxTry)
            {
               class<M79ClusterFalk>(fCClass).Default.BulletID = BulletID + fSClusters;

               if (fSuccessSpawn)
               {
                  P = Spawn(fCClass,,, fSuccessPos, RotRand(True));

                  if (P == None)
                     fSuccessSpawn = False;
               }

               if (!fSuccessSpawn)
               {
                  While (P == None && fAttempt < 27)
                  { 
                     fAttempt++; // we don't even test 0 since we're here for a reason

                     if (fAttempt >= 27)
                     {
                        //warn("FAIL"@fTry);
                        fACZRetryLoc += fCZRetryLoc;
                        fACXRetryLoc += fCXRetryLoc;
                        fACYRetryLoc += fCYRetryLoc;
                        fTry++;
                     }


                     else if (fAttempt >= 18)
                     {
                        fTempPos = FClusterQuad(Location - fACZRetryLoc, fAttempt - 18);
                        //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
                     }

                     else if (fAttempt >= 9)
                     {
                        fTempPos = FClusterQuad(Location + fACZRetryLoc, fAttempt - 9);
                        //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
                     }

                     else
                     {
                        fTempPos = FClusterQuad(Location, fAttempt);
                        //warn("First:"@fAttempt@"Location:"@fTempPos);
                     }

                     P = Spawn(fCClass,,, fTempPos, RotRand(True));
                  }
               }

               if (P != none)
               {
                  //warn("Spawned");
                  fSuccessPos   = fTempPos; 
                  fSuccessSpawn = True;
                  P.Instigator  = Instigator; 

                  fSClusters++;

                  fTry          = 0;

                  /*KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

                  if (KFPRI != none)
                  {
                     Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

                     if (Vet != none)
                        P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Instigator), P.Damage, P.MyDamageType);
                  }*/

                  P.Damage = P.Damage *= fDamageMulti; //KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Instigator), P.Damage, P.MyDamageType);
                  //warn("Cluster Damage:"@P.Damage@"- Default:"@P.Default.Damage);

                  if (M79ClusterFalk(P) != none) // pass siren immunity state to the cluster
                  {
                     M79ClusterFalk(P).fCInstigator = fCInstigator;
                     M79ClusterFalk(P).default.fSirenImmune = fSirenImmune;
                  }
               }
            }

            // Give up
            else
            {
               //warn("Giving up");
               fACZRetryLoc = fCZRetryLoc;
               fACXRetryLoc = fCXRetryLoc;
               fACYRetryLoc = fCYRetryLoc;
               fSClusters   = fNClusters;
               fTry = 0;
               Destroy();
            }
         }

         else
            Destroy();
      }
   }
}

// attempt to spawn clusters in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// Explode but check if we spawned all clusters before destroy, also don't play sound here
simulated function FalkExplode(vector HitLocation, vector HitNormal)
{
   local PlayerController  LocalPlayer;
   local Projectile P;
   local byte i;

   bHasExploded = True;
   BlowUp(HitLocation);

   bTriggered = true;

   if(Role == ROLE_Authority)
   {
      SetTimer(0.1, false);
      NetUpdateTime = Level.TimeSeconds - 1;
   }

   /*if (ExplodeSounds.Length > 0)
      PlaySound(ExplodeSounds[0],,2.0);*/

   // Shrapnel
   for(i=Rand(6); i<10; i++)
      P = Spawn(ShrapnelClass,,,,RotRand(True));

   Spawn(Class'KaboomFalk',,, HitLocation, rotator(vect(0,0,1)));
   //Spawn(ExplosionDecal,self,,HitLocation, rotator(HitNormal));

   // Shake nearby players screens
   LocalPlayer = Level.GetLocalPlayerController();

   if ( (LocalPlayer != None) && (VSize(Location - LocalPlayer.ViewTarget.Location) < (DamageRadius * 1.5)) )
      LocalPlayer.ShakeView(RotMag, RotRate, RotTime, OffsetMag, OffsetRate, OffsetTime);

   if (Role != ROLE_Authority || !fShouldKaboom || fSClusters >= fNClusters)
   {
      fShouldCheck = False;
      Destroy();
   }
}

// Touch only if we haven't exploded yet
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{
   if (!bHasExploded)
      Super.ProcessTouch(Other, HitLocation);
}

// added clusters fix and changed the detection logic
function Timer()
{
   local Pawn CheckPawn;
   local float ThreatLevel;

   if (!bHidden && !bTriggered)
   {
      if (ArmingCountDown >= 0)
      {
         ArmingCountDown -= 0.1;

         if (ArmingCountDown <= 0)
            SetTimer(1.0,True);
      }

      else
      {
         if (bEnemyDetected)
         {
            bAlwaysRelevant = true;
            Countdown--;

            if (CountDown > 0)
               PlaySound(BeepSound,SLOT_Misc,2.0,,150.0);

            else
            {
               Explode(Location, vector(Rotation));
               return;
            }
         }

         else
         {
            bAlwaysRelevant = false;
            CountDown       = Default.CountDown;
            PlaySound(BeepSound,,0.5,,50.0);
         }

         foreach VisibleCollidingActors(class'Pawn', CheckPawn, DetectionRadius, Location)
         {
            // just check if it's a zed and its role
            if (CheckPawn.Role == ROLE_Authority && KFMonster(CheckPawn) != none && KFMonster(CheckPawn).Health > 0)
            {
               ThreatLevel += KFMonster(CheckPawn).MotionDetectorThreat;

               // kinda pointless to keep checking if we're already going to go kaboom
               if (ThreatLevel >= ThreatThreshhold)
               {
                  bEnemyDetected = true;
                  SetTimer(0.1,True);
                  return;
               }
            }
         }

         // faster check and beep if something is in range
         if (ThreatLevel > 0)
         {
            bEnemyDetected = false;
            SetTimer(0.3,True);
         }
         
         // standard beep while waiting
         else
         {
            bEnemyDetected = false;
            SetTimer(fUsedSleepTime,True);
         }
      }
   }

   else
   {
      fShouldCheck = False;
      Destroy();
   }
}

// use BulletID to damage zeds
simulated function HurtRadius(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation)
{
   local actor Victims;
   local float damageScale, dist;
   local vector dirs;
   local int NumKilled;
   local KFMonster KFMonsterVictim;
   local Pawn P;
   local KFPawn KFP;
   local array<Pawn> CheckedPawns;
   local int i;
   local bool bAlreadyChecked;
   local float fActualDamage;


   if ( bHurtEntry )
      return;

   bHurtEntry = true;

   foreach CollidingActors (class 'Actor', Victims, DamageRadius, HitLocation)
   {
      // don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
      if((Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo')
            && ExtendedZCollision(Victims)==None)
      {
         dirs = Victims.Location - HitLocation;
         dist = FMax(1,VSize(dirs));
         dirs = dirs/dist;
         damageScale = 1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius);
         Victims.SetDelayedDamageInstigatorController(fCInstigator);
         if ( Victims == LastTouched )
            LastTouched = None;

         P = Pawn(Victims);

         if (P != none)
         {
            for (i = 0; i < CheckedPawns.Length; i++)
            {
               if (CheckedPawns[i] == P)
               {
                  bAlreadyChecked = true;
                  break;
               }
            }

            if( bAlreadyChecked )
            {
               bAlreadyChecked = false;
               P = none;
               continue;
            }

            KFMonsterVictim = KFMonster(Victims);

            if( KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
            {
               KFMonsterVictim = none;
            }

            KFP = KFPawn(Victims);

            if (KFMonsterVictim != none)
               damageScale    *= KFMonsterVictim.GetExposureTo(HitLocation);

            else if (KFP != none)
               damageScale *= KFP.GetExposureTo(HitLocation);

            CheckedPawns[CheckedPawns.Length] = P;

            if (damageScale <= 0)
            {
               P = none;
               continue;
            }

            else
            {
               //Victims = P;
               P = none;
            }
         }

         if (Victims == Instigator)
         {
            damageScale *= 0.3;
            fActualDamage = damageScale * DamageAmount;
         }

         else
            fActualDamage = FMax(damageScale * DamageAmount, fMinDamageMulti * DamageAmount);
            Victims.TakeDamage(fActualDamage, Instigator, Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs, damageScale * Momentum * dirs, DamageType, BulletID);

         if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
            Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);

         if( Role == ROLE_Authority && KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
         {
            NumKilled++;
         }
      }
   }
   if ((LastTouched != None) && (LastTouched != self) && (LastTouched.Role == ROLE_Authority) && !LastTouched.IsA('FluidSurfaceInfo'))
   {
      Victims = LastTouched;
      LastTouched = None;
      dirs = Victims.Location - HitLocation;
      dist = FMax(1,VSize(dirs));
      dirs = dirs/dist;
      damageScale = FMax(Victims.CollisionRadius/(Victims.CollisionRadius + Victims.CollisionHeight),1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius));
      Victims.SetDelayedDamageInstigatorController(fCInstigator);

      if (Victims == Instigator)
      {
         damageScale *= 0.3;
         fActualDamage = damageScale * DamageAmount;
      }

      else
         fActualDamage = FMax(damageScale * DamageAmount, fMinDamageMulti * DamageAmount);

      Victims.TakeDamage(fActualDamage, Instigator, Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs, damageScale * Momentum * dirs, DamageType, BulletID);

      if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
         Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);
   }

   bHurtEntry = false;
}


defaultproperties
{
   MyDamageType=Class'DamTypePipeBomb'
   ThreatThreshhold=0.5000
   Damage=1000.000000
   DamageRadius=350.000000
   fCCLass=Class'PipeBombClusterFalk'
   fNClusters=3
   fShouldKaboom=False
   fCheckSecs=0.500000
   fMaxTry=3
   CountDown=4
   DetectionRadius=150.0
   fCSpawnLoc=(Z=10.000000)
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
   fSirenImmune=False
   fShouldCheck=True
   fDamageMulti=1.0
   sFClass=class'ATMineSoundFix'
   ExplosionDecal=class'ScorchMarkFalk'
   bCollideActors=false
   bUseCylinderCollision=false
   fSleepTimeNormal=0.9
   fSleepTimeHard=0.8
   fSleepTimeSuicidal=0.7
   fSleepTimeHOE=0.6
   fSleepTimeBB=0.5
   fUsedSleepTime=0.9
   fMinDamageMulti=0.125
}
