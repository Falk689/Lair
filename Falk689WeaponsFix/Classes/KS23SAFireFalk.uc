class KS23SAFireFalk extends KS23SAMut.KS23SAFire;

var byte BID;

// attempt to see the whole fockin reload anim, step dunno
simulated function bool AllowFire()
{
   if ((KS23SAShotgunFalk(Weapon)      != None && KS23SAShotgunFalk(Weapon).fLoadingLastBullet) ||
       (LocalKS23SAShotgunFalk(Weapon) != None && LocalKS23SAShotgunFalk(Weapon).fLoadingLastBullet))
   {
      return False;
   }

	return Super.AllowFire();
}

// Added zed time support skills
function DoFireEffect()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes>   Vet;

   KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

   if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
   {
      Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

      if (Vet != none)
      {
         ProjPerFire = Vet.Static.GetShotgunProjPerFire(KFPRI, Default.ProjPerFire, Weapon);
         Spread      = Default.Spread * Vet.Static.GetShotgunSpreadMultiplier(KFPRI, Weapon);
      }
   }

   Super.DoFireEffect();
}


// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<ShotgunBulletFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local ShotgunBulletFalk FB;

   FB = ShotgunBulletFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
   FireRate=1.500000
   Spread=1125.000000
   ProjPerFire=8
   AmmoClass=Class'KS23SAAmmoFalk'
   ProjectileClass=Class'KS23SABulletFalk'
}
