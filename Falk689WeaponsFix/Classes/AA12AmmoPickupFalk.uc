class AA12AmmoPickupFalk extends KFMod.AA12AmmoPickup;

defaultproperties
{
    AmmoAmount=20
    InventoryType=Class'ShotgunAmmoFalk'
    PickupMessage="You found a 12-gauge drum magazine"
}