class ThompsonPickupFalk extends KFMod.ThompsonPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   InventoryType=Class'ThompsonSMGFalk'
   ItemName="Thompson M1A1 SMG"
   ItemShortName="M1A1"
   PickupMessage="You got a Thompson M1A1 SMG"
   cost=1200
   Weight=5.000000
   BuyClipSize=30
   AmmoCost=15
   PowerValue=40
   SpeedValue=65
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}