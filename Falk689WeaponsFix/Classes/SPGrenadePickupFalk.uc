class SPGrenadePickupFalk extends KFMod.SPGrenadePickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   InventoryType=Class'SPGrenadeLauncherFalk'
   Cost=1550
   BuyClipSize=1
   AmmoCost=5
   ItemName="Orca Bomb Propeller"
   ItemShortName="Orca Bomber"
   PickupMessage="You got an Orca Bomb Propeller"
   PowerValue=64
   SpeedValue=8
   RangeValue=85
   Weight=4.000000
   DrawScale=1.15
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
