class CLGLAmmoPickupFalk extends KFAmmoPickup;

defaultproperties
{
    InventoryType=Class'CLGLAmmoFalk'
    StaticMesh=StaticMesh'KillingFloorStatics.FragPickup'
    CollisionRadius=25.000000
    AmmoAmount=4
    PickupMessage="You found some 40x46mm grenades"
}
