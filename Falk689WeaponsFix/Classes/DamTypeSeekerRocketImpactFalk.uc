class DamTypeSeekerRocketImpactFalk extends KFMod.DamTypeSeekerRocketImpact;

// nope nope nope
static function ScoredHeadshot(KFSteamStatsAndAchievements KFStatsAndAchievements, class<KFMonster> MonsterClass, bool bLaserSightedM14EBRKill)
{
}

defaultproperties
{
   HeadShotDamageMult=1.100000
   WeaponClass=Class'SeekerSixRocketLauncherFalk'
   DeathString="%k killed %o (Seeker Six rocket impact)"
   bCheckForHeadShots=True
}
