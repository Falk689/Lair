class SeekerSixFireFalk extends KFMod.SeekerSixFire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID += 4;

   if (BID > 200)
      BID = BID - 200;

   class<SeekerSixRocketProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local SeekerSixRocketProjectileFalk FG;
   local KFPlayerReplicationInfo KFPRI;

   FG = SeekerSixRocketProjectileFalk(P);

   if (FG != none && Instigator != none)
      FG.fCInstigator = Instigator.Controller;

   KFPRI       = KFPlayerReplicationInfo(Weapon.Instigator.PlayerReplicationInfo);

   if (KFPRI.ClientVeteranSkill != none)
   {
      P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Weapon.Instigator), P.Damage, Class'DamTypeSeekerSixRocketFalk');
   }

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
   AmmoClass=Class'SeekerSixAmmoFalk'
   ProjectileClass=Class'SeekerSixRocketProjectileFalk'
   FireRate=0.33000
}
