class AR15SAPickupFalk extends AR15SAMut.AR15SAPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
    Weight=4.000000
    cost=1800
    AmmoCost=15
    BuyClipSize=30
    PowerValue=63
    SpeedValue=55
    RangeValue=100
    ItemName="Armalite AR15 Service Rifle"
    ItemShortName="AR15"
    AmmoItemName="5.56x45mm NATO rounds"
    InventoryType=Class'AR15SAFalk'
    PickupMessage="You got an Armalite AR15 Service Rifle"
	PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
