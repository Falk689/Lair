class SPShotgunAmmoPickupFalk extends KFMod.SPShotgunAmmoPickup;

defaultproperties
{
    AmmoAmount=10
    InventoryType=Class'SPShotgunAmmoFalk'
    PickupMessage="You found some 12-gauge shells"
}