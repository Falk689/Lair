class RPK47PickupFalk extends RPK47.RPK47Pickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=10.000000
   cost=1500
   BuyClipSize=75
   AmmoCost=75
   InventoryType=Class'RPK47Falk'
   ItemName="RPK47 Machine Gun"
   ItemShortName="RPK47"
   PickupMessage="You got an RPK47 Machine Gun"
   CorrespondingPerkIndex=7
   EquipmentCategoryID=4
   PowerValue=61
   SpeedValue=50
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
