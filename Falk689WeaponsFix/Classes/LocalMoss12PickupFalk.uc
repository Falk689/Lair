class LocalMoss12PickupFalk extends Moss12Mut.Moss12Pickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   InventoryType=Class'LocalMoss12Falk'
   ItemName="Moss12 Shotgun"
   ItemShortName="Moss12"
   PickupMessage="You got a Moss12 Shotgun"
   cost=700
   BuyClipSize=6
   AmmoCost=12
   PowerValue=46
   SpeedValue=30
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
