class TrigunPickupFalk extends Trigun_Pistol.Trigun_PistolPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

auto state pickup
{
	function Touch(Actor Other)
	{
		local Inventory Copy;
      local FHumanPawn FHP;

		if ( KFHumanPawn(Other) != none && !CheckCanCarry(KFHumanPawn(Other)) )
		{
			return;
		}

		// If touched by a player pawn, let him pick this up.
		if ( ValidTouch(Other) )
		{
			Copy = SpawnCopy(Pawn(Other));
			AnnouncePickup(Pawn(Other));
			SetRespawn();

			if ( Copy != None )
			{
				Copy.PickupFunction(Pawn(Other));
			}

			if ( MySpawner != none && KFGameType(Level.Game) != none )
			{
				KFGameType(Level.Game).WeaponPickedUp(MySpawner);
			}

			if ( KFWeapon(Copy) != none )
			{
            // sell price scaling based on difficulty
            if (!bDropped)
            {
               //warn("VRG");

               FHP = FHumanPawn(Other);

               if (FHP != none)
               {
                  if (Level.Game.GameDifficulty >= 8.0)       // BB
                  {
                     SellValue = cost * FHP.fBBRPCostMulti;
                     //warn("BB");
                  }

                  else if (Level.Game.GameDifficulty >= 7.0)  // HOE
                  {
                     SellValue = cost * FHP.fHOERPCostMulti;
                     //warn("HOE");
                  }

                  else if (Level.Game.GameDifficulty >= 5.0)  // suicidal
                  {
                     SellValue = cost * FHP.fSuicidalRPCostMulti;
                     //warn("SUI");
                  }

                  else if (Level.Game.GameDifficulty >= 4.0)  // hard
                  {
                     SellValue = cost * FHP.fHardRPCostMulti;
                     //warn("HRD");
                  }
               }
            }

            //warn("VAL:"@SellValue);

				KFWeapon(Copy).SellValue = SellValue;
				KFWeapon(Copy).bPreviouslyDropped = bDropped;

				if ( !bPreviouslyDropped && KFWeapon(Copy).bIsTier3Weapon &&
					 Pawn(Other).Controller != none && Pawn(Other).Controller != DroppedBy )
				{
					KFWeapon(Copy).Tier3WeaponGiver = DroppedBy;
				}
			}
		}
	}
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=2.000000
   cost=650
   BuyClipSize=6
   AmmoCost=12
   InventoryType=Class'TrigunFalk'
   ItemName="Trigun Revolver"
   ItemShortName="Trigun"
   PickupMessage="You got a Trigun Revolver"
   PowerValue=25
   SpeedValue=75
   RangeValue=85
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
