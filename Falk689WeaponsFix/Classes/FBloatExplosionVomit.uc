class FBloatExplosionVomit extends KFBloatVomit;

#exec OBJ LOAD FILE=LairStaticMeshes_SM.usx
#exec OBJ LOAD FILE=LairTextures_T.utx

var Emitter  ExtraTrail;

var float FDamage;  // touch damage
var byte BulletID; // bullet ID, aka don't hit twice
var bool FReady;   // don't hit before being ready

replication
{
   reliable if(Role == ROLE_Authority)
      BulletID, FReady;
}

// add some vomit trail
simulated function PostBeginPlay()
{
   SetOwner(None);

   if (Role == ROLE_Authority)
   {
      Velocity = Vector(Rotation) * Speed;
      Velocity.Z += TossZ;
   }

   if (Role == ROLE_Authority)
      Rand3 = Rand(3);

   if ((Level.NetMode != NM_DedicatedServer) && ((Level.DetailMode == DM_Low) || Level.bDropDetail))
   {
      bDynamicLight = false;
      LightType = LT_None;
   }

   if (Level.NetMode != NM_DedicatedServer)
   {
      if (!PhysicsVolume.bWaterVolume)
      {
         ExtraTrail = Spawn(class'BlowerThrowerSecondaryEmitter',self);
         Trail      = Spawn(class'BlowerThrowerPrimaryEmitter',self);
      }
   }

   // Difficulty Scaling
   if (Level.Game != none)
   {
      FDamage    = Max((DifficultyDamageModifer() * Default.FDamage), 1);       // i know i've already said this in a scrake file somewhere, but this "modifer" keeps hurting me and keeps me up at night
      BaseDamage = Max((DifficultyDamageModifer() * Default.BaseDamage), 1);
		Damage     = Max((DifficultyDamageModifer() * Default.Damage), 1);
   }

   FReady = True;
}

// remove the trail
simulated function Destroyed()
{
	if (Trail != none)
	{
		Trail.mRegen = False;
		Trail.SetPhysics(PHYS_None);
	}

	if (ExtraTrail != none)
	{
		ExtraTrail.Kill();
		ExtraTrail.SetPhysics(PHYS_None);
	}

	Super.Destroyed();
}

// Scales the damage this Zed deals by the difficulty level
function float DifficultyDamageModifer()
{
   if (Level.Game.GameDifficulty >= 8.0) // bloodbath
      return 1.0;

   return 1.0;
}

// use BulletID and just do a single amount of damage, also don't damage doors
simulated function HurtRadius(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation)
{
   local actor Victims;
   local vector dir;

   if (bHurtEntry)
      return;

   bHurtEntry = true;

   if (!FReady)
   {
      FDamage    = Max((DifficultyDamageModifer() * Default.FDamage), 1);

      FReady = true;
   }

   foreach VisibleCollidingActors( class 'Actor', Victims, DamageRadius, HitLocation )
   {
      // don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
      if( (Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo') )
      {
         dir = Victims.Location - HitLocation;

         if ( Instigator == None || Instigator.Controller == None )
            Victims.SetDelayedDamageInstigatorController( InstigatorController );

         if ( Victims == LastTouched )
            LastTouched = None;

         if (KFDoorMover(Victims) == none)
            Victims.TakeDamage(FDamage, Instigator, Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dir, (Momentum * dir), MyDamageType, BulletID);

         if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
            Vehicle(Victims).DriverRadiusDamage(FDamage, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);

      }
   }
   if ( (LastTouched != None) && (LastTouched != self) && (LastTouched.Role == ROLE_Authority) && !LastTouched.IsA('FluidSurfaceInfo') )
   {
      Victims = LastTouched;
      LastTouched = None;
      dir = Victims.Location - HitLocation;

      if (Instigator == None || Instigator.Controller == None)
         Victims.SetDelayedDamageInstigatorController(InstigatorController);

      if (KFDoorMover(Victims) == none)
         Victims.TakeDamage(FDamage, Instigator, Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dir, (Momentum * dir), MyDamageType, BulletID);

      if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
         Vehicle(Victims).DriverRadiusDamage(FDamage, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);
   }

   bHurtEntry = false;
}

// use bulletID
simulated function ClientSideTouch(Actor Other, Vector HitLocation)
{
   if (!FReady)
      return;

   if (KFDoorMover(Other) == none)
	   Other.TakeDamage(FDamage, instigator, Location, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);
}

// no damage on doors, hopefully.
simulated singular function HitWall(vector HitNormal, actor Wall)
{
	local PlayerController PC;

   if (!FReady)
      return;

	if ( Role == ROLE_Authority )
	{
		if ( !Wall.bStatic && !Wall.bWorldGeometry )
		{
			if (Instigator == None || Instigator.Controller == None)
				Wall.SetDelayedDamageInstigatorController( InstigatorController);

         if (KFDoorMover(Wall) == none)
			   Wall.TakeDamage(FDamage, instigator, Location, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);

			if (DamageRadius > 0 && Vehicle(Wall) != None && Vehicle(Wall).Health > 0)
				Vehicle(Wall).DriverRadiusDamage(FDamage, DamageRadius, InstigatorController, MyDamageType, MomentumTransfer, Location);

			HurtWall = Wall;
		}
		MakeNoise(1.0);
	}
	Explode(Location + ExploWallOut * HitNormal, HitNormal);
	if ( (ExplosionDecal != None) && (Level.NetMode != NM_DedicatedServer)  )
	{
		if ( ExplosionDecal.Default.CullDistance != 0 )
		{
			PC = Level.GetLocalPlayerController();
			if ( !PC.BeyondViewDistance(Location, ExplosionDecal.Default.CullDistance) )
				Spawn(ExplosionDecal,self,,Location, rotator(-HitNormal));
			else if ( (Instigator != None) && (PC == Instigator.Controller) && !PC.BeyondViewDistance(Location, 2*ExplosionDecal.Default.CullDistance) )
				Spawn(ExplosionDecal,self,,Location, rotator(-HitNormal));
		}
		else
			Spawn(ExplosionDecal,self,,Location, rotator(-HitNormal));
	}
	HurtWall = None;
}

auto state Flying
{
    simulated function Landed( Vector HitNormal )
    {
        local Rotator NewRot;
        local int CoreGoopLevel;

        if ( Level.NetMode != NM_DedicatedServer )
        {
            PlaySound(ImpactSound, SLOT_Misc);
            // explosion effects
        }

        SurfaceNormal = HitNormal;

        // spawn globlings
        CoreGoopLevel = Rand3 + MaxGoopLevel - 3;
        if (GoopLevel > CoreGoopLevel)
        {
            if (Role == ROLE_Authority)
                SplashGlobs(GoopLevel - CoreGoopLevel);
            SetGoopLevel(CoreGoopLevel);
        }
        spawn(class'KFMod.VomitDecal',,,, rotator(-HitNormal));

        bCollideWorld = false;
        SetCollisionSize(GoopVolume*10.0, GoopVolume*10.0);
        bProjTarget = true;

        NewRot = Rotator(HitNormal);
        NewRot.Roll += 32768;
        SetRotation(NewRot);
        SetPhysics(PHYS_None);
        bCheckedsurface = false;
        Fear = Spawn(class'AvoidMarker');
        GotoState('OnGround');
    }

	simulated function HitWall( Vector HitNormal, Actor Wall )
	{
      if (!FReady)
         return;

		Landed(HitNormal);
		if ( !Wall.bStatic && !Wall.bWorldGeometry )
		{
			bOnMover = true;
			SetBase(Wall);
			if (Base == None)
				BlowUp(Location);
		}
	}

	simulated function ProcessTouch(Actor Other, Vector HitLocation)
	{
      if (!FReady)
         return;

		if (Other != Instigator && (Other.IsA('Pawn') || Other.IsA('DestroyableObjective') || Other.bProjTarget))
			HurtRadius(FDamage,DamageRadius, MyDamageType, MomentumTransfer, HitLocation);

		else if ( Other != Instigator && Other.bBlockActors )
			HitWall( Normal(HitLocation-Location), Other );
	}
}

defaultproperties
{
   TouchDetonationDelay=0.000000
   BaseDamage=2
   Speed=300.000000
   Damage=2.000000
   FDamage=2.000000
   MomentumTransfer=0.000000
   MyDamageType=Class'DamTypeBloatVomitFalk'
   ImpactSound=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_AcidSplash'
   DrawType=DT_StaticMesh
   StaticMesh=StaticMesh'LairStaticMeshes_SM.VomitRework.BloatVomit'
   DrawScale=0.6
   bDynamicLight=False
   LifeSpan=8.000000
   Skins(0)=Texture'LairTextures_T.VomitRework.BloatVomit'
   bUseCollisionStaticMesh=True
   bBlockHitPointTraces=False
   CollisionRadius=5.000000
   CollisionHeight=5.000000
}
