class ThompsonDrumAmmoPickupFalk extends KFMod.ThompsonDrumAmmoPickup;

defaultproperties
{
    AmmoAmount=50
    InventoryType=Class'ThompsonDrumAmmoFalk'
    PickupMessage="You found some .45 ACP rounds"
}