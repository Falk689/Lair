class KrissMAmmoFalk extends KFMod.KrissMAmmo;

defaultproperties
{
    InitialAmount=175
	MaxAmmo=400
	AmmoPickupAmount=25
	ItemName=".45 ACP rounds"
	PickupClass=Class'KrissMAmmoPickupFalk'
}