class BoomStickAmmoPickupFalk extends KFMod.DBShotgunAmmoPickup;

defaultproperties
{
    AmmoAmount=4
    InventoryType=Class'BoomStickAmmoFalk'
    PickupMessage="You found some 12-gauge shells"
}