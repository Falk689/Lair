class DamTypeZEDGunFalk extends KFMod.DamTypeZEDGun;

defaultproperties
{
    WeaponClass=Class'ZEDGunFalk'
    DeathString="%k killed %o (ZED)"
    bIsPowerWeapon=false
	HeadShotDamageMult=1.100000
    bCheckForHeadShots=False
}