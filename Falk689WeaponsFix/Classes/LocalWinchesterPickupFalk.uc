class LocalWinchesterPickupFalk extends KFMod.WinchesterPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
    cost=1000
    ItemName="Winchester M1894"
    ItemShortName="M1894"
    AmmoItemName=".44 rounds"
    BuyClipSize=8
    AmmoCost=16
    InventoryType=Class'LocalWinchesterFalk'
    PickupMessage="You got a Winchester M1894"
    PowerValue=37
    SpeedValue=11
    RangeValue=100
    Weight=6.00000
    PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
