class NailBombFalk extends NadeFalk;

var int BulletID; // used to prevent multi-hit
var int fBNumber; // multiplier to prevent the fix on different nade

#exec OBJ LOAD FILE=LairTextures_T.utx

// Spawn nails on explode
simulated function Explode(vector HitLocation, vector HitNormal)
{
   local byte i;
   local Projectile P;
   local vector fPLocation;
   local vector SpawnCorrection;

   if (bHasExploded)
      return;

   SpawnCorrection = vect(0,0,0);

   // Kaboom
   if (Role == ROLE_Authority)
   {
      // attempt to fix ground cluster spawn
      fPLocation = Instigator.Location + fCZRetryLoc;

      //log(Location.Z);
      //log(fPLocation.Z);

      if (Location.Z <= fPLocation.Z) // if the nade if under the player, correct spawn position
         SpawnCorrection = fCSpawnLoc;

      for(i=0; i<fNClusters; i++)
      {
         class<NailBombProjectileFalk>(fCClass).default.BulletID = GetBulletID(class<NailBombProjectileFalk>(fCClass));

         P = Spawn(fCClass,,, Location + SpawnCorrection, RotRand(True));

         if (P != None)
         {
            P.Instigator = Instigator; 
            
            if (ShotgunBulletFalk(P) != none)
               ShotgunBulletFalk(P).fCInstigator = fCInstigator;

            fSClusters++;
         }
      }

      //log("Initial spawn");
      //log(fSClusters);
   }

   else
		Spawn(Class'NailBombExplosionFalk',,, HitLocation, rotator(vect(0,0,1)));

   FalkExplode(HitLocation, HitNormal);
}

// Timed check to see if we should spawn clusters
simulated function Tick(float DeltaTime)
{
   local Projectile P;
   local byte fAttempt;

   Super(Nade).Tick(DeltaTime);

   if (Role == ROLE_Authority)
   {
      fLifeTime += DeltaTime;

      if (bHasExploded)
      {
         if (fSClusters < fNClusters)
         {
            // Try to spawn remaining clusters
            if (fTry < fMaxTry)
            {
               class<NailBombProjectileFalk>(fCClass).default.BulletID = GetBulletID(class<NailBombProjectileFalk>(fCClass));

               while (fSuccessSpawn && fSClusters < fNClusters)
               {
                  P = Spawn(fCClass,,, fSuccessPos, RotRand(True));

                  if (P != none)
                  {
                     P.Instigator  = Instigator; 
                     fSClusters++;

                     class<NailBombProjectileFalk>(fCClass).default.BulletID = GetBulletID(class<NailBombProjectileFalk>(fCClass));
                  }

                  else
                     fSuccessSpawn = False;
               }

               if (!fSuccessSpawn)
               {
                  While (P == None && fAttempt < 27)
                  { 
                     fAttempt++; // we don't even test 0 since we're here for a reason

                     if (fAttempt >= 27)
                     {
                        //warn("FAIL"@fTry);
                        fACZRetryLoc += fCZRetryLoc;
                        fACXRetryLoc += fCXRetryLoc;
                        fACYRetryLoc += fCYRetryLoc;
                        fTry++;
                     }


                     else if (fAttempt >= 18)
                     {
                        fTempPos = FClusterQuad(Location - fACZRetryLoc, fAttempt - 18);
                        //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
                     }

                     else if (fAttempt >= 9)
                     {
                        fTempPos = FClusterQuad(Location + fACZRetryLoc, fAttempt - 9);
                        //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
                     }

                     else
                     {
                        fTempPos = FClusterQuad(Location, fAttempt);
                        //warn("First:"@fAttempt@"Location:"@fTempPos);
                     }

                     P = Spawn(fCClass,,, fTempPos, RotRand(True));
                  }
               }

               if (P != none)
               {
                  //warn("Spawned");
                  fSuccessPos   = fTempPos; 
                  fSuccessSpawn = True;
                  P.Instigator  = Instigator; 

                  fSClusters++;

                  fTry          = 0;


                  while (fSuccessSpawn && fSClusters < fNClusters)
                  {
                     P = Spawn(fCClass,,, fSuccessPos, RotRand(True));

                     if (P != none)
                     {
                        P.Instigator  = Instigator; 
                        fSClusters++;

                        class<NailBombProjectileFalk>(fCClass).default.BulletID = GetBulletID(class<NailBombProjectileFalk>(fCClass));
                     }

                     else
                        fSuccessSpawn = False;
                  }
               }
            }

            // Give up
            else
            {
               //warn("Giving up");
               fACZRetryLoc = fCZRetryLoc;
               fACXRetryLoc = fCXRetryLoc;
               fACYRetryLoc = fCYRetryLoc;
               fSClusters   = fNClusters;
               fTry = 0;
               Destroy();
            }
         }

         else
            Destroy();
      }
   }
}


// Setting up the projectile BulletID
/*simulated function Tick(float DeltaTime)
{
   if (Role == ROLE_Authority && bHasExploded && fSClusters < fNClusters && fTry < fMaxTry)
      class<NailBombProjectileFalk>(fCClass).default.BulletID = GetBulletID(class<NailBombProjectileFalk>(fCClass));

   Super.Tick(DeltaTime);
}*/


simulated function HurtRadius( float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation )
{
   return;
}


// Explode but check if we spawned all the nails before destroy
simulated function FalkExplode(vector HitLocation, vector HitNormal)
{
   //local PlayerController  LocalPlayer;

   bHasExploded = True;

   PlaySound(ExplodeSounds[rand(ExplodeSounds.length)],,2.0);

   if (Role != ROLE_Authority || fSClusters >= fNClusters)
      Destroy();
}



// Overridden to prevent clusters from blowing up our nade
function TakeDamage( int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
   local FalkMonsterBase Siren;

   if (damageType == class'SirenScreamDamage')
   {
      Siren = FalkMonsterBase(InstigatedBy);

      if (Siren != none && !Siren.fIsStunned && !Siren.fFrozen && Siren.HeadHealth > 0 && !Siren.bDecapitated)
         Disintegrate(HitLocation, vect(0,0,1));
   }
 
   else if ((!bHasExploded) && 
           (KFHumanPawn(InstigatedBy) == none &&
           (damageType == class'DamTypeFrag' || damageType == class'DamTypeBurned')))
      Explode(HitLocation, vect(0,0,0));
}

// prevent setting ExplodeTimer twice
simulated function HitWall(vector HitNormal, actor Wall)
{
   local Vector VNorm;
   local PlayerController PC;

   if ((Pawn(Wall) != None) || (GameObjective(Wall) != None))
   {
      Explode(Location, HitNormal);
      return;
   }

   if (!fTimerSet)
   {
      SetTimer(ExplodeTimer * 0.8, false);
      fTimerSet = true;
   }

   // Reflect off Wall w/damping
   VNorm = (Velocity dot HitNormal) * HitNormal;
   Velocity = -VNorm * DampenFactor + (Velocity - VNorm) * DampenFactorParallel;

   RandSpin(50000);
   DesiredRotation.Roll = 0;
   RotationRate.Roll = 0;
   Speed = VSize(Velocity);

   if (Speed < 20)
   {
      bBounce = False;
      PrePivot.Z = -1.5;
      SetPhysics(PHYS_None);
      DesiredRotation = Rotation;
      DesiredRotation.Roll = 0;
      DesiredRotation.Pitch = 0;
      SetRotation(DesiredRotation);

      if (Trail != None)
         Trail.mRegen = false; // stop the emitter from regenerating
   }

   else
   {
      if ((Level.NetMode != NM_DedicatedServer) && (Speed > 50))
         PlaySound(ImpactSound, SLOT_Misc);

      else
      {
         bFixedRotationDir = false;
         bRotateToDesired = true;
         DesiredRotation.Pitch = 0;
         RotationRate.Pitch = 50000;
      }

      if (!Level.bDropDetail && (Level.DetailMode != DM_Low) && (Level.TimeSeconds - LastSparkTime > 0.5) &&
         EffectIsRelevant(Location,false))
      {
         PC = Level.GetLocalPlayerController();

         if ((PC.ViewTarget != None) && VSize(PC.ViewTarget.Location - Location) < 6000)
            Spawn(HitEffectClass,,, Location, Rotator(HitNormal));

         LastSparkTime = Level.TimeSeconds;
      }
   }
}

// returns the right ID for this bullet, sorry for this...
function int GetBulletID(class<NailBombProjectileFalk> Prj)
{
   return fBNumber + ((BulletID - 1) * (fNClusters * Prj.default.Bounces)) + ((fSClusters - 1) * Prj.default.Bounces);
}

defaultproperties
{
   Speed=850.0
   fCClass=class'NailBombProjectileFalk'
   fNClusters=40
   ExplodeTimer=1.250000
   StaticMesh=StaticMesh'LairStaticMeshes_SM.Grenades.SupportSpecialistGrenade'
   fBNumber=690
   fShouldKaboom=True
}
