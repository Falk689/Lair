class AK12SAAmmoFalk extends AK12SAgent.AK12SAAmmo;

defaultproperties
{
   MaxAmmo=300
   InitialAmount=140
   AmmoPickupAmount=35
   ItemName="5.45x39mm rounds"
   PickupClass=Class'AK12SAAmmoPickupFalk'
}