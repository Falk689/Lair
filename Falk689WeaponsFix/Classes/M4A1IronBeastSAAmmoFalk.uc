class M4A1IronBeastSAAmmoFalk extends M4A1IronBeastSAMut.M4A1IronBeastSAAmmo;

defaultproperties
{
   AmmoPickupAmount=30
   MaxAmmo=240
   InitialAmount=90
   PickupClass=Class'M4A1IronBeastSAAmmoPickupFalk'
   ItemName="5.56mm rounds"
}