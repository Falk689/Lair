class Walter2000SAPickupFalk extends Walter2000SA.Walter2000SAPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   cost=1250
   Weight=6.000000
   BuyClipSize=10
   AmmoCost=10
   InventoryType=Class'Walter2000SAFalk'
   ItemName="Walther WA2000"
   ItemShortName="WA2000"
   PickupMessage="You got a Walther WA2000"
   PowerValue=28
   SpeedValue=33
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
