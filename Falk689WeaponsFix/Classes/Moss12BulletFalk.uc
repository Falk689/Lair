class Moss12BulletFalk extends ShotgunBulletFalk;

defaultproperties
{
   FDamage=33.000000
   MaxPenetrations=2
   PenDamageReduction=0.500000
   HeadShotDamageMult=1.100000
   MomentumTransfer=30000.000000
   MyDamageType=Class'DamTypeMoss12Falk'
}
