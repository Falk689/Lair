class M4A1IronBeastSAAmmoPickupFalk extends M4A1IronBeastSAMut.M4A1IronBeastSAAmmoPickup;

defaultproperties
{
   AmmoAmount=30
   InventoryType=Class'M4A1IronBeastSAAmmoFalk'
   PickupMessage="You got 5.56mm rounds"
}