class InstantAcidNadeFalk extends NadeFalk;

// Don't load anything, just stay put
static function PreloadAssets()
{
   return;
}

// Difficulty scaling for the number of clusters
function PostBeginPlay()
{
   if (Level.Game.GameDifficulty < 3.0)  // Normal
      fNClusters = 6;

   else if (Level.Game.GameDifficulty < 5.0) // Hard
      fNClusters = 8;

   else if (Level.Game.GameDifficulty < 6.0) // Suicidal
      fNClusters = 10;

   else if (Level.Game.GameDifficulty < 8.0) // HOE
      fNClusters = 12;

   else // BB
      fNClusters = 14;
}

// Go Kaboom
function PostNetBeginPlay()
{
   SetTimer(ExplodeTimer, false);
   fTimerSet = true;
}

// Use proper cluster class for the checks
simulated function Explode(vector HitLocation, vector HitNormal)
{
   bHasExploded    = True;
}

// don't
simulated function FalkExplode(vector HitLocation, vector HitNormal)
{
}

// don't
function TakeDamage( int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
}

// Timed check to see if we should spawn clusters
simulated function Tick(float DeltaTime)
{
   local Projectile P;
   local byte fAttempt;

   Super(Nade).Tick(DeltaTime);

   if (Role == ROLE_Authority && fShouldCheck)
   {
      fLifeTime += DeltaTime;

      if (bHasExploded)
      {
         if (fSClusters < fNClusters)
         {
            // Try to spawn remaining clusters
            if (fTry < fMaxTry)
            {
               class'FBloatExplosionVomit'.Default.BulletID = BulletID + fSClusters + 1;

               //warn("Setting Vomit Bullet ID:"@BulletID + fSClusters + 1@"- Time:"@Level.TimeSeconds);

               if (fSuccessSpawn)
               {
                  P = Spawn(class'FBloatExplosionVomit',,, fSuccessPos, RotRand(True));

                  if (P == None)
                     fSuccessSpawn = False;
               }

               if (!fSuccessSpawn)
               {
                  While (P == None && fAttempt < 27)
                  { 
                     fAttempt++; // we don't even test 0 since we're here for a reason

                     if (fAttempt >= 27)
                     {
                        //warn("FAIL"@fTry);
                        fACZRetryLoc += fCZRetryLoc;
                        fACXRetryLoc += fCXRetryLoc;
                        fACYRetryLoc += fCYRetryLoc;
                        fTry++;
                     }


                     else if (fAttempt >= 18)
                     {
                        fTempPos = FClusterQuad(Location - fACZRetryLoc, fAttempt - 18);
                        //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
                     }

                     else if (fAttempt >= 9)
                     {
                        fTempPos = FClusterQuad(Location + fACZRetryLoc, fAttempt - 9);
                        //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
                     }

                     else
                     {
                        fTempPos = FClusterQuad(Location, fAttempt);
                        //warn("First:"@fAttempt@"Location:"@fTempPos);
                     }

                     P = Spawn(class'FBloatExplosionVomit',,, fTempPos, RotRand(True));
                  }
               }

               if (P != none)
               {
                  //warn("Spawned");
                  fSuccessPos   = fTempPos; 
                  fSuccessSpawn = True;
                  P.Instigator  = Instigator; 

                  fSClusters++;

                  fTry          = 0;
               }
            }

            // Give up
            else
            {
               //warn("Giving up");
               fACZRetryLoc = fCZRetryLoc;
               fACXRetryLoc = fCXRetryLoc;
               fACYRetryLoc = fCYRetryLoc;
               fSClusters   = fNClusters;
               fTry = 0;
               Destroy();
            }
         }

         else
            Destroy();
      }
   }
}

// No Hurt radius
simulated function HurtRadius(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation)
{
}

defaultproperties
{
   ExplodeTimer=0.1
   DrawScale=0.000000
   DrawType=DT_None
   Damage=0.000000
   DamageRadius=0.000000
   fShouldCheck=True
   fMaxTry=10
   fShouldKaboom=True
   fCheckSecs=0.500000
   ExplodeSounds=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_AcidSplash'
   ExplosionDecal=Class'KFMod.VomitDecal'
   MomentumTransfer=0.000000
}
