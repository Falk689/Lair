class DamTypeStingerFalk extends DamTypeSW76Falk;

defaultproperties
{
   WeaponClass=Class'StingerFalk'
   HeadShotDamageMult=1.100000
   KDamageImpulse=6500.000000
   KDeathVel=250.000000
   KDeathUpKick=80.000000
}
