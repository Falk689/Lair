class ZEDGunFireFalk extends KFMod.ZEDGunFire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<ZEDGunProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local ZEDGunProjectileFalk FB;

   FB = ZEDGunProjectileFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
    ProjPerFire=1
    AmmoPerFire=1
    FireRate=0.200000
    FireAnimRate=1.000000
    AmmoClass=Class'ZEDGunAmmoFalk'
    ProjectileClass=Class'ZEDGunProjectileFalk'
}

