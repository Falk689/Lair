class ZEDMKIIPickupFalk extends KFMod.ZEDMKIIPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   cost=650
   InventoryType=Class'ZEDMKIIWeaponFalk'
   ItemName="Zed Eradication Device MK2"
   ItemShortName="ZED MK2"
   PickupMessage="You got a Zed Eradication Device MK2"
   CorrespondingPerkIndex=8
   PowerValue=50
   SpeedValue=57
   RangeValue=100
   Weight=6.000000
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
