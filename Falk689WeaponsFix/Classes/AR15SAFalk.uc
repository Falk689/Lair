class AR15SAFalk extends AR15SAMut.AR15SA;

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

var float             FBringUpTime;         // last time bringup was called
var float             FBringUpSafety;       // safety to prevent reload on server before the weapon is ready

// don't do anything here
simulated function AltFire(float F)
{
}

// no toggle
simulated function DoToggle()
{
}

// removed weird vanilla shit
simulated function FillToInitialAmmo()
{
   if (bNoAmmoInstances)
   {
      if (AmmoClass[0] != None)
         AmmoCharge[0] = AmmoClass[0].Default.InitialAmount;

      if ((AmmoClass[1] != None) && (AmmoClass[0] != AmmoClass[1]))
         AmmoCharge[1] = AmmoClass[1].Default.InitialAmount;
      return;
   }

   if (Ammo[0] != None)
      Ammo[0].AmmoAmount = Ammo[0].Default.InitialAmount;

   if (Ammo[1] != None)
      Ammo[1].AmmoAmount = Ammo[1].Default.InitialAmount;
}

// removed more vanilla shit
function GiveAmmo(int m, WeaponPickup WP, bool bJustSpawned)
{
   local bool bJustSpawnedAmmo;
   local int addAmount, InitialAmount;
   local KFPawn KFP;
   local KFPlayerReplicationInfo KFPRI;

   KFP = KFPawn(Instigator);

   if(KFP != none)
      KFPRI = KFPlayerReplicationInfo(KFP.PlayerReplicationInfo);

   UpdateMagCapacity(Instigator.PlayerReplicationInfo);

   if (FireMode[m] != None && FireMode[m].AmmoClass != None)
   {
      Ammo[m] = Ammunition(Instigator.FindInventoryType(FireMode[m].AmmoClass));
      bJustSpawnedAmmo = false;

      if (bNoAmmoInstances)
      {
         if ((FireMode[m].AmmoClass == None) || ((m != 0) && (FireMode[m].AmmoClass == FireMode[0].AmmoClass)))
            return;

         InitialAmount = FireMode[m].AmmoClass.Default.InitialAmount;

         if(WP!=none && WP.bThrown==true)
            InitialAmount = WP.AmmoAmount[m];
         else
            MagAmmoRemaining = MagCapacity;

         if (Ammo[m] != None)
         {
            addamount = InitialAmount + Ammo[m].AmmoAmount;
            Ammo[m].Destroy();
         }
         else
            addAmount = InitialAmount;

         AddAmmo(addAmount,m);
      }

      else
      {
         if ((Ammo[m] == None) && (FireMode[m].AmmoClass != None))
         {
            Ammo[m] = Spawn(FireMode[m].AmmoClass, Instigator);
            Instigator.AddInventory(Ammo[m]);
            bJustSpawnedAmmo = true;
         }

         else if ((m == 0) || (FireMode[m].AmmoClass != FireMode[0].AmmoClass))
            bJustSpawnedAmmo = (bJustSpawned || ((WP != None) && !WP.bWeaponStay));

         if (WP != none && WP.bThrown == true)
            addAmount = WP.AmmoAmount[m];

         else if (bJustSpawnedAmmo)
         {
            if (default.MagCapacity == 0)
               addAmount = 0;  // prevent division by zero.
            else
               addAmount = Ammo[m].InitialAmount;
         }

         if (WP != none && m > 0 && (FireMode[m].AmmoClass == FireMode[0].AmmoClass))
            return;

         if(KFPRI != none && KFPRI.ClientVeteranSkill != none)
            Ammo[m].MaxAmmo = float(Ammo[m].MaxAmmo) * KFPRI.ClientVeteranSkill.Static.AddExtraAmmoFor(KFPRI, Ammo[m].Class);

         Ammo[m].AddAmmo(addAmount);
         Ammo[m].GotoState('');
      }
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }
}

// just don't
simulated function DoAutoSwitch(){}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
    local int                  m;
    local Pickup               Pickup;
    local byte                 fAttempt;
    local vector               fTempPos;
    local vector               Direction;

    if (!bCanThrow)
        return;

    for (m = 0; m < NUM_FIRE_MODES; m++)
    {
        // if _RO_
        if( FireMode[m] == none )
            continue;
        // End _RO_

        if (FireMode[m].bIsFiring)
            StopFire(m);
    }

        if (Instigator != None)
            Direction = vector(Instigator.GetViewRotation());

        else if (Pawn(Owner) != none)
            Direction = vector(Pawn(Owner).GetViewRotation());

    Pickup = Spawn(PickupClass,,, StartLocation);

    // Try to spawn remaining clusters
    while (Pickup == None && fTry < fMaxTry)
    {
        fTry++;
        fAttempt = 0;

        while (Pickup == None && fAttempt < 27)
        {
            fAttempt++; // we don't even test 0 since we're here for a reason

            if (fAttempt >= 27)
            {
                //warn("FAIL"@fTry);
                fACZRetryLoc += fCZRetryLoc;
                fACXRetryLoc += fCXRetryLoc;
                fACYRetryLoc += fCYRetryLoc;
            }

            else if (fAttempt >= 18)
            {
                fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
                //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
            }

            else if (fAttempt >= 9)
            {
                fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
                //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
            }

            else
            {
                fTempPos = FClusterQuad(StartLocation, fAttempt);
                //warn("First:"@fAttempt@"Location:"@fTempPos);
            }

            Pickup = Spawn(PickupClass,,, fTempPos);
        }
    }

    fTry         = 0;
    fACZRetryLoc = fCZRetryLoc;
    fACXRetryLoc = fCXRetryLoc;
    fACYRetryLoc = fCYRetryLoc;


    if (Pickup != None)
    {
        ClientWeaponThrown();

        if (Instigator != None)
            DetachFromPawn(Instigator);

        Pickup.InitDroppedPickupFor(self);
        Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

        //if (Instigator.Health > 0)
            WeaponPickup(Pickup).bThrown = true;

        Destroy();
    }

    // we failed to drop this weapon on death, award us some cash instead
    else if (Instigator.Health <= 0 && Instigator.PlayerReplicationInfo != none)
    {
        Instigator.PlayerReplicationInfo.Score += SellValue;

        if (FHumanPawn(Instigator) != none)
            FHumanPawn(Instigator).FalkSetDosh();
    }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
    Switch (fAttempt)
    {
        case 0:
            return fSLocation;

        case 1:
            return fSLocation + fACXRetryLoc;

        case 2:
            return fSLocation - fACXRetryLoc;

        case 3:
            return fSLocation + fACYRetryLoc;

        case 4:
            return fSLocation - fACYRetryLoc;

        case 5:
            return fSLocation + fACXRetryLoc + fACYRetryLoc;

        case 6:
            return fSLocation - fACXRetryLoc + fACYRetryLoc;

        case 7:
            return fSLocation + fACXRetryLoc - fACYRetryLoc;

        case 8:
            return fSLocation - fACXRetryLoc - fACYRetryLoc;
    }
}


// setting bringup time
simulated function BringUp(optional Weapon PrevWeapon)
{
   if (Level.NetMode == NM_DedicatedServer)
      FBringUpTime = Level.TimeSeconds;

   Super(KFWeapon).BringUp(PrevWeapon);
}


// block reload on bringup
simulated function bool AllowReload()
{
    local FHumanPawn FHP;

    FHP = FHumanPawn(Instigator);

    if (FHP != none && FHP.fReloadState != F_Reload_Allowed)
        return false;

    if (Level.TimeSeconds <= FBringUpTime + BringUpTime + FBringUpSafety)
        return false;

    if (AR15SAFireFalk(FireMode[0]) != none && AR15SAFireFalk(FireMode[0]).bBursting && AR15SAFireFalk(FireMode[0]).RoundsToFire > 0)
        return false;

    if (FireMode[0].NextFireTime + 0.1 > Level.TimeSeconds || FireMode[1].NextFireTime + 0.1 > Level.TimeSeconds)
        return false;

    return Super.AllowReload();
}

// don't throw while bursting
simulated function bool CanThrow()
{
   if (AR15SAFireFalk(FireMode[0]) != none && AR15SAFireFalk(FireMode[0]).bBursting && AR15SAFireFalk(FireMode[0]).RoundsToFire > 0)
   {
      //warn("RETURN 1");
      return False;
   }

   if (FireMode[0].NextFireTime + 0.1 > Level.TimeSeconds)
   {
      //warn("RETURN 2");
      return False;
   }

   return Super.CanThrow();
}

defaultproperties
{
   MagCapacity=30
   ReloadRate=3.95000
   ReloadAnimRate=1.000000
   Weight=4.000000
   BringUpTime=0.60000
   PutDownTime=0.350000
   FireModeClass(0)=Class'AR15SAFireFalk'
   Description="The AR15 is a lightweight, air-cooled rifle with a rotating lock bolt. Can only be fired in semi-automatic mode. Features the same damage output of an FN FAL and a soft recoil."
   PickupClass=Class'AR15SAPickupFalk'
   ItemName="Armalite AR15 Service Rifle"
   InventoryGroup=3
   Priority=135
   fMaxTry=3
   FBringUpSafety=0.1
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
