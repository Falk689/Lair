class W1300_Compact_EditionBulletFalk extends ShotgunBulletFalk;

defaultproperties
{
   FDamage=40.000000
   MaxPenetrations=2
   PenDamageReduction=0.500000
   HeadShotDamageMult=1.100000
   MyDamageType=Class'DamTypeW1300_Compact_EditionFalk'
}