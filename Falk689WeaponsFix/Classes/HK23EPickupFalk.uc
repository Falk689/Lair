class HK23EPickupFalk extends KFWeaponPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=10.000000
   cost=900
   AmmoCost=90
   BuyClipSize=90
   PowerValue=48
   SpeedValue=50
   RangeValue=100
   ItemName="HK23E Light Machine Gun"
   ItemShortName="HK23E"
   AmmoItemName="5.56mm NATO rounds"
   AmmoMesh=StaticMesh'KillingFloorStatics.L85Ammo'
   CorrespondingPerkIndex=7
   EquipmentCategoryID=4
   InventoryType=Class'HK23EFalk'
   PickupMessage="You got a HK23E Light Machine Gun"
   PickupForce="AssaultRiflePickup"
   StaticMesh=StaticMesh'HK23ESA_A.HK23_Static.HK23PickupSA'
   CollisionRadius=25.000000
   CollisionHeight=5.000000
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
