class FlameThrowerPickupFalk extends KFMod.FlameThrowerPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   InventoryType=Class'FlameThrowerFalk'
   cost=1750
   PickupMessage="You got a Flamethrower"
   ItemShortName="Flamer"
   ItemName="Flamethrower"
   BuyClipSize=100
   AmmoCost=50
   PowerValue=10
   SpeedValue=80
   RangeValue=60
   Weight=8.000000
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
