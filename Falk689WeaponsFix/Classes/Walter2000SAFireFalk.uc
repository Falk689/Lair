class Walter2000SAFireFalk extends Walter2000SA.Walter2000SAFire;

var byte  BID;                // bullet ID to prevent multiple hits on a single zed

// multi-hit, pawn penetration and invisible head fix for normal hitscan weapons
function DoTrace(Vector Start, Rotator Dir)
{
   local Vector           X,Y,Z, End, HitLocation, HitNormal, ArcEnd;
   local Actor            Other, DamageActor, OldDamageActor;
   local array<int>       HitPoints;
   local int              bHP;
   local byte             Retries;

   BID++;

   if (BID > 200)
      BID = 1;

   MaxRange();

   Weapon.GetViewAxes(X, Y, Z);

   if (Weapon.WeaponCentered())
      ArcEnd = (Instigator.Location + Weapon.EffectOffset.X * X + 1.5 * Weapon.EffectOffset.Z * Z);

   else
   {
      ArcEnd = (Instigator.Location + Instigator.CalcDrawOffset(Weapon) + Weapon.EffectOffset.X * X +
            Weapon.Hand * Weapon.EffectOffset.Y * Y + Weapon.EffectOffset.Z * Z);
   }

   X = Vector(Dir);
   End = Start + TraceRange * X;

   while (Retries < 100)
   {
      Retries    += 1;
      DamageActor = none;

      Other = Instigator.HitPointTrace(HitLocation, HitNormal, End, HitPoints, Start,, 1);

      if (Other == None)
      {
         start = HitLocation;
         continue;
      }

      if (Other == Instigator || KFHumanPawn(Other) != None || Other.Base == Instigator || KFHumanPawn(Other.Base) != None)
      {
         start = HitLocation + X;
         continue;
      }

      if (ExtendedZCollision(Other) != None && Other.Owner != None)
         Other = Pawn(Other.Owner);

      if (!Other.bWorldGeometry && Other != Level)
      {
         if (KFMonster(Other) != None)
            DamageActor = Other;

         if (KFMonster(DamageActor) != None)
         {
            if (DamageActor == OldDamageActor)
            {
               start = HitLocation + X;
               continue;
            }

            OldDamageActor = DamageActor;
            bHP            = KFMonster(DamageActor).Health;

            if (bHP <= 0)
            {
               start = HitLocation + X;
               continue;
            }

            DamageActor.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);

            if (KFMonster(DamageActor).Health == bHP)
            {
               start = HitLocation + X;
               continue;
            }

            return;
         }

         else
            Other.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);
      }

      else if (BlockingVolume(Other) == None)
      {
         if(KFWeaponAttachment(Weapon.ThirdPersonActor) != None)
            KFWeaponAttachment(Weapon.ThirdPersonActor).UpdateHit(Other, HitLocation, HitNormal);

         break;
      }
   }
}

defaultproperties
{
   AmmoClass=Class'Walter2000SAAmmoFalk'
   DamageType=Class'DamTypeWalter2000SAFalk'
   DamageMin=110
   DamageMax=110
   aimerror=25.000000 // semi-auto snipers standard
   FireRate=0.350000 // semi-auto snipers standard
   RecoilRate=0.075000
   maxVerticalRecoilAngle=1800 // 20% more than SVT40
   maxHorizontalRecoilAngle=600 // 20% more than SVT40
   Spread=0.006000
}
