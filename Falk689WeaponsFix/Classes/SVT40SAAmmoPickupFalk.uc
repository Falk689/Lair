class SVT40SAAmmoPickupFalk extends SVT40SAMut.SVT40SAAmmoPickup;

defaultproperties
{
    AmmoAmount=10
    InventoryType=Class'SVT40SAAmmoFalk'
    PickupMessage="You found 7.62x54mmR rounds"
}