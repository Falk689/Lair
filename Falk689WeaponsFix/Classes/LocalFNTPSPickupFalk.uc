class LocalFNTPSPickupFalk extends FN_TPS.FN_TPSPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   cost=1600
   BuyClipSize=8
   AmmoCost=16
   Weight=7.000000           // was 8 in patch 13
   InventoryType=Class'LocalFNTPSFalk'
   ItemName="FN Tactical Police Shotgun"
   ItemShortName="TPS"
   PickupMessage="You got an FN Tactical Police Shotgun"
   PowerValue=52
   SpeedValue=30
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
