class M4203AmmoFalk extends KFMod.M4203Ammo;

defaultproperties
{
    MaxAmmo=300
    InitialAmount=120
    AmmoPickupAmount=30
    PickupClass=Class'M4203AmmoPickupFalk'
    ItemName="5.56mm NATO rounds"
}