class MP5MAmmoFalk extends MP5MAmmo;

defaultproperties
{
    InitialAmount=192
	MaxAmmo=400
	AmmoPickupAmount=32
	ItemName="9x19mm Parabellum rounds"
	PickupClass=Class'MP5MAmmoPickupFalk'
}