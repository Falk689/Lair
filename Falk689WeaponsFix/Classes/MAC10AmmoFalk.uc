class MAC10AmmoFalk extends KFMod.MAC10Ammo;

defaultproperties
{
    MaxAmmo=180
	InitialAmount=60
	AmmoPickupAmount=30
	PickupClass=Class'MAC10AmmoPickupFalk'
	ItemName=".45 ACP rounds"
}