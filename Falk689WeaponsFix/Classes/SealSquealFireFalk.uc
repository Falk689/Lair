class SealSquealFireFalk extends KFMod.SealSquealFire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID += 4;

   if (BID > 200)
      BID = BID - 200;

   class<SealSquealProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local SealSquealProjectileFalk FG;
   local KFPlayerReplicationInfo KFPRI;

   FG = SealSquealProjectileFalk(P);

   if (FG != none && Instigator != none)
      FG.fCInstigator = Instigator.Controller;

   KFPRI       = KFPlayerReplicationInfo(Weapon.Instigator.PlayerReplicationInfo);

   if (KFPRI.ClientVeteranSkill != none)
   {
      P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Weapon.Instigator), P.Damage, P.MyDamageType);
   }

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
   ProjectileClass=Class'SealSquealProjectileFalk'
   AmmoClass=Class'SealSquealAmmoFalk'
   FireRate=0.55000
}
