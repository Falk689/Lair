class M4AmmoPickupFalk extends KFMod.M4AmmoPickup;

defaultproperties
{
   AmmoAmount=30
   InventoryType=Class'M4AmmoFalk'
   PickupMessage="You got 5.56mm rounds"
}
