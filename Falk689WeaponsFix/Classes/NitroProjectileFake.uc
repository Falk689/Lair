class NitroProjectileFake extends M79ClusterFalk;

var class<Emitter> Trail1Class, Trail2Class;
var Emitter Trail1, Trail2;

// add trails
simulated function PostBeginPlay()
{
   Super.PostBeginPlay();

   if (Level.NetMode != NM_DedicatedServer)
   {
      if (!PhysicsVolume.bWaterVolume)
      {
         Trail1 = Spawn(Trail1Class,self);
         Trail2 = Spawn(Trail2Class,self);
      }
   }
}

// add effects
simulated function PostNetBeginPlay()
{
   local PlayerController PC;

   Super.PostNetBeginPlay();

   if ( Level.NetMode == NM_DedicatedServer )
   {
      return;
   }

   if ( Level.bDropDetail || (Level.DetailMode == DM_Low) )
   {
      bDynamicLight = false;
      LightType = LT_None;
   }
   else
   {
      PC = Level.GetLocalPlayerController();
      if ( (Instigator != None) && (PC == Instigator.Controller) )
      {
         return;
      }

      if ( (PC == None) || (PC.ViewTarget == None) || (VSize(PC.ViewTarget.Location - Location) > 3000) )
      {
         bDynamicLight = false;
         LightType = LT_None;
      }
   }
}

// explode effects
simulated function Explode(vector HitLocation,vector HitNormal)
{
   if (bHasExploded)
      return;

   bHasExploded = True;

   PlaySound(ImpactSound, SLOT_Misc);

   if ( KFHumanPawn(Instigator) != none && !Level.bDropDetail ) {
      if ( EffectIsRelevant(Location,false) ) {
         Spawn(ExplosionDecal,self,,Location, rotator(-HitNormal));
         if ( Level.DetailMode > DM_Low ) {
            Spawn(class'NitroSplash',self,,HitLocation,rotator(HitNormal));
            if ( Level.DetailMode >= DM_SuperHigh )
               Spawn(class'NitroImpact',self,,HitLocation,rotator(-HitNormal));
         }
      }
   }

   if (Level.DetailMode >= DM_High)
      Spawn(class'NitroGroundEffect',self,,Location);    

   SetCollisionSize(0.0, 0.0);
   Destroy();
}

// don't damage anything
simulated function HurtRadius( float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation )
{
}

simulated function Destroyed()
{
   if (Trail1 != none)
   {
      Trail1.Kill();
      Trail1.SetPhysics(PHYS_None);
   }    

   if (Trail2 != none)
   {
      Trail2.Kill();
      Trail2.SetPhysics(PHYS_None);
   }

   Super.Destroyed();
}

simulated function ProcessTouch(Actor Other, vector HitLocation)
{
   if (!fShouldCollide || bHasExploded || Other == none)
      return;

   // Don't collide with bullet whip attachments
   if(KFBulletWhipAttachment(Other) != none)
      return;

   Explode(self.Location,self.Location);
}


simulated singular function HitWall(vector HitNormal, actor Wall)
{
   if (Role == ROLE_Authority)
   {

      if (Wall == none || !Wall.bBlockHitPointTraces)
         return;

      MakeNoise(1.0);
   }

   Explode(Location + ExploWallOut * HitNormal, HitNormal);
}


defaultproperties
{
   Trail1Class=Class'IJC_Project_Santa.NitroTrail'
   Trail2Class=Class'IJC_Project_Santa.NitroTrailB'
   Speed=200.000000
   MaxSpeed=200.000000
   Damage=0.000000
   DamageRadius=1.000000
   MomentumTransfer=0.000000
   ImpactSound=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_AcidSplash'
   ExplosionDecal=Class'NitroDecal'
   DrawType=DT_None
   StaticMesh=None
   Physics=PHYS_Falling
   LifeSpan=2.000000
   DrawScale=5.000000
   Style=STY_None
   bBlockHitPointTraces=False
   fSirenImmune=True
}
