class M32AmmoFalk extends KFMod.M32Ammo;

defaultproperties
{
    MaxAmmo=36
    InitialAmount=12
    AmmoPickupAmount=3
    PickupClass=Class'M32AmmoPickupFalk'
    ItemName="40x46mm grenades"
}