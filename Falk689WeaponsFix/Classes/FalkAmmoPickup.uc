class FalkAmmoPickup extends KFAmmoPickup;

var int   FSleepCalls;
var float FSleepDelay;
var float FHideDelay;

auto state Sleeping
{
	ignores Touch;

	function bool ReadyToPickup(float MaxWait)
	{
		return (bPredictRespawns && LatentFloat < MaxWait);
	}

   simulated function Tick(float DeltaTime)
   {
      Global.Tick(DeltaTime);

      if (FSleepCalls > 0)
      {

         if (FHideDelay > 0)
            FHideDelay -= DeltaTime;

         else
         {
            FHideDelay = Default.FHideDelay;
            FSleepCalls--;

		      NetUpdateTime = Level.TimeSeconds - 1;
            bHidden = true;
         }
      }
   }

   function StartSleeping() {}

   function BeginState()
   {
      local int i;

      FSleepCalls = 6;

		NetUpdateTime = Level.TimeSeconds - 1;
		bHidden = true;
		bSleeping = true;
		SetCollision(false, false);

		for ( i = 0; i < 4; i++ )
		{
			TeamOwner[i] = None;
		}
	}

	function EndState()
	{
		NetUpdateTime = Level.TimeSeconds - 1;
		bHidden = false;
		bSleeping = false;
		SetCollision(default.bCollideActors, default.bBlockActors);
	}

Begin:
	bSleeping = true;

   while (true)
   {
	   Sleep(FSleepDelay);
   }
}

state Pickup
{
   // When touched by an actor.
   function Touch(Actor Other)
   {
      local Inventory CurInv;
      local bool bPickedUp;
      local int AmmoPickupAmount;
      local CrossbuzzsawFalk CBFalk;
      local bool bResuppliedBoomstick;
      local KFPlayerReplicationInfo KFPRI;
      local class<FVeterancyTypes> Vet;
      local FHumanPawn FHP;
      local float addWeight;

      addWeight = 0;

      if (Pawn(Other) != none && Pawn(Other).bCanPickupInventory && Pawn(Other).Controller != none &&
           FastTrace(Other.Location, Location))
      {
         KFPRI = KFPlayerReplicationInfo(Pawn(Other).PlayerReplicationInfo);
         Vet   = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);
         FHP   = FHumanPawn(Other);

         if (FHP != none)
         {
            if (FHP.Weapon != none)
            {
               if (PipeBombExplosiveFalk(FHP.Weapon) != none)
                  PipeBombExplosiveFalk(FHP.Weapon).fAmmoPickupTime = PipeBombExplosiveFalk(FHP.Weapon).Default.fAmmoPickupTime;

               else if (ATMineExplosiveFalk(FHP.Weapon) != none)
                  ATMineExplosiveFalk(FHP.Weapon).fAmmoPickupTime = ATMineExplosiveFalk(FHP.Weapon).Default.fAmmoPickupTime;
            }
         }

         // add an extra kg to pipebombs checks if we have atmines to either skip them or make sure we can hold both
         for (CurInv = Other.Inventory; CurInv != none; CurInv = CurInv.Inventory)
         {
            if (ATMineAmmoFalk(CurInv) != none)
            {
                addWeight = 1.0;
                break;
            }
         }

         for (CurInv = Other.Inventory; CurInv != none; CurInv = CurInv.Inventory)
         {
            if (CrossbuzzsawFalk(CurInv) != none)
               CBFalk = CrossbuzzsawFalk(CurInv);

            if (SpitfireFalk(CurInv) != none)
            {
               //FHP = FHumanPawn(Other);

               if (FHP != none)
                  FHP.fSpitfireReload = FHP.Default.fSpitfireReload;
            }
            
            if (StingerFalk(CurInv) != none)
            {
               //FHP = FHumanPawn(Other);

               if (FHP != none)
                  FHP.fStingerReload = FHP.Default.fStingerReload;
            }

            if (MercyFalk(CurInv) != none)
            {
               //FHP = FHumanPawn(Other);

               if (FHP != none)
                  FHP.fMercyReload = FHP.Default.fMercyReload;
            }

            if (KFAmmunition(CurInv) != none && KFAmmunition(CurInv).bAcceptsAmmoPickups)
            {
               // prevent pipebombs from taking ammo if we can't carry them
               if (PipeBombAmmoFalk(CurInv) != none && !PipeBombAmmoFalk(CurInv).FShouldAddAmmo(KFAmmunition(CurInv).AmmoPickupAmount, addWeight))
                  continue;

               // prevent atmines from taking ammo if we can't carry them
               if (ATMineAmmoFalk(CurInv) != none && !ATMineAmmoFalk(CurInv).FShouldAddAmmo(KFAmmunition(CurInv).AmmoPickupAmount))
                  continue;

               if (KFAmmunition(CurInv).AmmoPickupAmount > 1)
               {
                  if (KFAmmunition(CurInv).AmmoAmount < KFAmmunition(CurInv).MaxAmmo)
                  {
                     if (KFPRI != none && Vet != none)
                        AmmoPickupAmount = float(KFAmmunition(CurInv).AmmoPickupAmount) * Vet.static.GetAmmoPickupMod(KFPRI, KFAmmunition(CurInv));

                     else
                        AmmoPickupAmount = KFAmmunition(CurInv).AmmoPickupAmount;

                     KFAmmunition(CurInv).AmmoAmount = Min(KFAmmunition(CurInv).MaxAmmo, KFAmmunition(CurInv).AmmoAmount + AmmoPickupAmount);

                     if (DBShotgunAmmo(CurInv) != none)
                        bResuppliedBoomstick = true;

                     bPickedUp = true;
                  }
               }

               // always give a fucking nade no matter what
               else if (KFAmmunition(CurInv).AmmoAmount < KFAmmunition(CurInv).Default.MaxAmmo * Vet.static.AddNadeExtraAmmo(KFPRI, KFAmmunition(CurInv)))
               {
                  KFAmmunition(CurInv).AmmoAmount++;
                  bPickedUp = true;
               }
            }
         }

         if (bPickedUp)
         {
            if (CBFalk != none)
               CBFalk.AmmoPickedUp();

            AnnouncePickup(Pawn(Other));
            GotoState('Sleeping', 'Begin');

            //FSleepCalls = 6;

            if (KFGameType(Level.Game) != none)
               KFGameType(Level.Game).AmmoPickedUp(self);
         }
      }
   }
}

defaultproperties
{
   PickupMessage="Found some ammo"
   FSleepDelay=0.1
   FHideDelay=0.1
}
