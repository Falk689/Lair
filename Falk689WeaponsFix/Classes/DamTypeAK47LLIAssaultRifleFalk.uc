class DamTypeAK47LLIAssaultRifleFalk extends KFMod.DamTypeAK47AssaultRifle;

defaultproperties
{
    WeaponClass=Class'AK47LLIAssaultRifleFalk'
    DeathString="%k killed %o (АK47)"
    KDamageImpulse=6500.000000
    KDeathVel=250.000000
    KDeathUpKick=80.000000
	HeadShotDamageMult=1.100000
}
