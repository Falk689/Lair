class CrossbuzzsawAmmoFalk extends KFMod.CrossbuzzsawAmmo;

defaultproperties
{
    MaxAmmo=24
    InitialAmount=10
    AmmoPickupAmount=4
    PickupClass=Class'CrossbuzzsawPickupFalk'
    ItemName="Sawblade"
}