class DamTypeM7A3MFalk extends KFMod.DamTypeM7A3M;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FMedicDamage', Amount);
}

defaultproperties
{
   WeaponClass=Class'M7A3MMedicGunFalk'
}