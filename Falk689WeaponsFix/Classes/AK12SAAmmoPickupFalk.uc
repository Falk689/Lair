class AK12SAAmmoPickupFalk extends AK12SAgent.AK12SAAmmoPickup;

defaultproperties
{
   InventoryType=Class'AK12SAAmmoFalk'
   PickupMessage="You found some 5.45x39mm rounds"
   AmmoAmount=35
}