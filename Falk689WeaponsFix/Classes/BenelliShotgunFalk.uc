class BenelliShotgunFalk extends BenelliShotgunBaseFalk;

// server stuff
var float fRRCheckTick;                 // on the server we kind of need to check and adjust ReloadRate times to times, this is the interval
var float fRRCurCheckTick;              // used to check what time is it and tick the ReloadRate check

var float fReloadSpeedFix; // reload time speed fix
var float fDReloadRate;    // desired reload rate

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

var float             FBringUpTime;         // last time bringup was called
var float             FBringUpSafety;       // safety to prevent reload on server before the weapon is ready

replication
{
   reliable if(Role == ROLE_Authority)
      fDReloadRate;
}

// attempt to see the whole reload animation in server
simulated function WeaponTick(float dt)
{
   local float LastSeenSeconds;

   if (fShouldBreechReload)
   {
      if (fRRCurCheckTick < fRRCheckTick)
         fRRCurCheckTick += dt;

      else
      {
         fRRCurCheckTick = 0;

         if (fLoadingLastBullet)
            ReloadRate = fLastBulletRate;

         else
            ReloadRate = fDReloadRate;

      }
   }

   //else if (fDReloadRate != Default.ReloadRate)
      //fDReloadRate = Default.ReloadRate;

   // vanilla stuff
	if ( bHasAimingMode )
	{
        if( bForceLeaveIronsights )
        {
        	if( bAimingRifle )
        	{
                ZoomOut(true);

            	if( Role < ROLE_Authority)
        			ServerZoomOut(false);
            }

            bForceLeaveIronsights = false;
        }

        if( ForceZoomOutTime > 0 )
        {
            if( bAimingRifle )
            {
        	    if( Level.TimeSeconds - ForceZoomOutTime > 0 )
        	    {
                    ForceZoomOutTime = 0;

                	ZoomOut(true);

                	if( Role < ROLE_Authority)
            			ServerZoomOut(false);
        		}
    		}
    		else
    		{
                ForceZoomOutTime = 0;
    		}
    	}
	}

	if ( (Level.NetMode == NM_Client) || Instigator == None || KFFriendlyAI(Instigator.Controller) == none && Instigator.PlayerReplicationInfo == None)
	   return;

	UpdateMagCapacity(Instigator.PlayerReplicationInfo);

	if(!bIsReloading)
	{
		if(!Instigator.IsHumanControlled())
		{
			LastSeenSeconds = Level.TimeSeconds - Instigator.Controller.LastSeenTime;
			if(MagAmmoRemaining == 0 || ((LastSeenSeconds >= 5 || LastSeenSeconds > MagAmmoRemaining) && MagAmmoRemaining < MagCapacity))
				ReloadMeNow();
		}
	}

	else
	{
		if ((Level.TimeSeconds - ReloadTimer) >= ReloadRate)
		{
			if(AmmoAmount(0) <= MagCapacity && !bHoldToReload)
			{
				MagAmmoRemaining = AmmoAmount(0);
				ActuallyFinishReloading();
			}

			else
			{
				AddReloadedAmmo();

				if (bHoldToReload)
               NumLoadedThisReload++;

				if (MagAmmoRemaining < MagCapacity && MagAmmoRemaining < AmmoAmount(0) && bHoldToReload)
					ReloadTimer = Level.TimeSeconds;

				if (MagAmmoRemaining >= MagCapacity || MagAmmoRemaining >= AmmoAmount(0) || !bHoldToReload || bDoSingleReload)
					ActuallyFinishReloading();

				else if (Level.NetMode!=NM_Client)
					Instigator.SetAnimAction(WeaponReloadAnim);
			}
		}

		else if(bIsReloading && !bReloadEffectDone && Level.TimeSeconds - ReloadTimer >= ReloadRate / 2)
		{
			bReloadEffectDone = true;
			ClientReloadEffects();
		}
	}
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
   local int Mode;

   Instigator = Pawn(Owner);

   bPendingSwitch = bPossiblySwitch;

   if( Instigator == None )
   {
      GotoState('PendingClientWeaponSet');
      return;
   }

   for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
   {
      if( FireModeClass[Mode] != None )
      {
         // laurent -- added check for vehicles (ammo not replicated but unlimited)
         if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
         {
            GotoState('PendingClientWeaponSet');
            return;
         }
      }

      FireMode[Mode].Instigator = Instigator;
      FireMode[Mode].Level = Level;
   }

   ClientState = WS_Hidden;
   GotoState('Hidden');

   if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
      return;

   if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
   {
      if (Instigator.PendingWeapon != None)
         Instigator.ChangedWeapon();
      else
         BringUp();
      return;
   }

   if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
      return;

   if ( Instigator.Weapon == None)
   {
      Instigator.PendingWeapon = self;
      Instigator.ChangedWeapon();
   }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// attempt to see the whole fockin reload animation in server
exec function ReloadMeNow()
{
   if (MagAmmoRemaining <= 0)
      fShouldBreechReload = True;

   fDReloadRate = Default.ReloadRate;

   Super.ReloadMeNow();
}

// attempt to see the whole fockin reload animation
simulated function ClientReload()
{
   if (MagAmmoRemaining <= 0)
      fShouldBreechReload = True;

   Super.ClientReload();
}

// second attempt to see the whole fockin reload animation
simulated function AddReloadedAmmo()
{
   if (AmmoAmount(0) > 0)
      ++MagAmmoRemaining;

   fDReloadRate -= fReloadSpeedFix;
   ReloadRate    = fDReloadRate;

   // last bullet to load, don't interrupt or drop
   if (fShouldBreechReload && AmmoAmount(0) > MagCapacity - 1 && MagAmmoRemaining == MagCapacity - 1)
   {
      ReloadRate         = fLastBulletRate;
      bCanThrow          = False;
      fLoadingLastBullet = True;
   }

   // restore vanilla stuff
   else if (MagAmmoRemaining == MagCapacity)
   {
      fDReloadRate        = Default.ReloadRate;
      bCanThrow           = True;
      fShouldBreechReload = False;
      fLoadingLastBullet  = False;
   }
}

// third attempt to see the whole fockin reload
simulated function bool InterruptReload()
{
   if (fLoadingLastBullet)
      return False;

   if (Super.InterruptReload())
   {
      fDReloadRate        = Default.ReloadRate;
      fShouldBreechReload = False;
      bCanThrow           = True;

      return True;
   }

   return False;
}

// attempt to fix more and more weird shit happening with the reload
simulated function ClientInterruptReload()
{
   if (fLoadingLastBullet)
      return;

   fDReloadRate        = Default.ReloadRate;
   fShouldBreechReload = False;
   bCanThrow           = True;
   Super.ClientInterruptReload();
}

// more attempt to fix that reload on server
simulated function ServerInterruptReload()
{
   if (fLoadingLastBullet)
      return;

   fDReloadRate        = Default.ReloadRate;
   fShouldBreechReload = False;
   bCanThrow           = True;
   Super.ServerInterruptReload();
}

// permit throwing while reloading, blocking it during the last bullet
simulated function bool CanThrow()
{
   if (bIsReloading && !fLoadingLastBullet)
   {
      InterruptReload();
      return True;
   }

   return Super.CanThrow();
}

// just don't
simulated function DoAutoSwitch(){}


// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }

   // we failed to drop this weapon on death, award us some cash instead
   else if (Instigator.Health <= 0 && Instigator.PlayerReplicationInfo != none)
   {
      Instigator.PlayerReplicationInfo.Score += SellValue;
      
      if (FHumanPawn(Instigator) != none)
         FHumanPawn(Instigator).FalkSetDosh();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// setting bringup time
simulated function BringUp(optional Weapon PrevWeapon)
{
   if (Level.NetMode == NM_DedicatedServer)
      FBringUpTime = Level.TimeSeconds;

   Super(KFWeapon).BringUp(PrevWeapon);
}


// block reload on bringup
simulated function bool AllowReload()
{
    local FHumanPawn FHP;

    FHP = FHumanPawn(Instigator);

    if (FHP != none && FHP.fReloadState != F_Reload_Allowed)
        return false;

    if (Level.TimeSeconds <= FBringUpTime + BringUpTime + FBringUpSafety)
        return false;

    return Super.AllowReload();
}

defaultproperties
{
   ModeSwitchAnim="LightOn"
   PickupClass=Class'BenelliPickupFalk'
   FireModeClass(0)=Class'BenelliFireFalk'
   ReloadAnimRate=1.2                       // slowed 20% down
   ReloadRate=0.7
   fDReloadRate=0.7
   fLastBulletRate=1.6
   fReloadSpeedFix=0.02
   fRRCheckTick=0.1
   MagCapacity=6
   bTorchEnabled=true
   Description="The M4 Super 90 is an italian semi-automatic gas-operated shotgun manufactured by Benelli. Faster fire rate than any other semi-automatic shotgun. Slightly higher damage output of its M3 pump-action counterpart."
   ItemName="Benelli M4 Super 90 Combat Shotgun"
   InventoryGroup=3
   Priority=225
   fMaxTry=3
   FBringUpSafety=0.1
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
