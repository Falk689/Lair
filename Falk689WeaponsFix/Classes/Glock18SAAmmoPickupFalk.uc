class Glock18SAAmmoPickupFalk extends KFAmmoPickup;

defaultproperties
{
     KFPickupImage=Texture'KillingFloorHUD.ClassMenu.Deagle'
     AmmoAmount=34
     InventoryType=Class'Glock18SAAmmoFalk'
     PickupMessage="Found 9mm rounds"
     StaticMesh=None
}