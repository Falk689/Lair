class AngelicPickupFalk extends Angelic.AngelicPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=11.000000
   cost=3300
   PowerValue=79
   SpeedValue=40
   RangeValue=60
   ItemName="Angelic Greatsword"
   ItemShortName="Angelic"
   InventoryType=Class'AngelicFalk'
   PickupMessage="You got the Angelic Greatsword"
   StaticMesh=StaticMesh'Angelic_SM.Angelic'
   DrawScale=1.20000
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
