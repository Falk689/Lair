class BusterSworddPickupFalk extends BusterSwordd.BusterSworddPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=10.000000
   cost=2800
   PowerValue=67
   SpeedValue=50
   RangeValue=45
   InventoryType=Class'BusterSworddFalk'
   ItemName="Buster Sword"
   ItemShortName="Buster"
   PickupMessage="You got a Buster Sword"
   DrawScale=2.050000
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'

}
