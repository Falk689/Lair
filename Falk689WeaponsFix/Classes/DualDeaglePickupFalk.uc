class DualDeaglePickupFalk extends KFMod.DualDeaglePickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
    Weight=4.000000
    cost=700
    BuyClipSize=14
    PowerValue=22
    SpeedValue=47
    RangeValue=100
    AmmoCost=14
    ItemName="Dual IMI Desert Eagles MK XIX"
    ItemShortName="Deagles"
    AmmoItemName=".50 AE rounds"
    InventoryType=Class'DualDeagleFalk'
    PickupMessage="You found another IMI Desert Eagle MK XIX"
    PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
