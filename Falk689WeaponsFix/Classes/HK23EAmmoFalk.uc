class HK23EAmmoFalk extends KFAmmunition;

defaultproperties
{
   AmmoPickupAmount=23 // one quarter of magazine, heavy weapons standard
   MaxAmmo=360
   InitialAmount=180
   IconMaterial=Texture'KillingFloorHUD.Generic.HUD'
   IconCoords=(X1=336,Y1=82,X2=382,Y2=125)
   ItemName="5.56mm NATO rounds"
}