class AA12ExplosiveFireFalk extends AA12Explosive.AA12ExplosiveFire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID += 1;

   if (BID > 200)
      BID = BID - 200;

   class<AA12ExplosiveBulletFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// Added zed time demoman skills
function DoFireEffect()
{   
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);
 
	if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
	{
      Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

      if (Vet != none)
         ProjPerFire = Vet.Static.GetExplosiveProjPerFire(KFPRI, Default.ProjPerFire);
	}

   Super.DoFireEffect();
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local AA12ExplosiveBulletFalk FG;
   local KFPlayerReplicationInfo KFPRI;

   FG = AA12ExplosiveBulletFalk(P);

   if (FG != none && Instigator != none)
      FG.fCInstigator = Instigator.Controller;

   KFPRI       = KFPlayerReplicationInfo(Weapon.Instigator.PlayerReplicationInfo);

   if (KFPRI.ClientVeteranSkill != none)
   {
      P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Weapon.Instigator), P.Damage, Class'KFMod.DamTypeM79Grenade');
   }

   Super.PostSpawnProjectile(P);
}


defaultproperties
{
   AmmoClass=Class'AA12ExplosiveAmmoFalk'
   ProjectileClass=Class'AA12ExplosiveBulletFalk'
   Spread=1200.000000     // 25% less than trigun's
   FireRate=0.550000
}
