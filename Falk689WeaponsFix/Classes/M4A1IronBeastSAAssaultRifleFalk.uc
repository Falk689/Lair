class M4A1IronBeastSAAssaultRifleFalk extends M4A1IronBeastSAMut.M4A1IronBeastSAAssaultRifle;

#exec OBJ LOAD FILE=LairAnimations_A.ukx

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

var float             FBringUpTime;         // last time bringup was called
var float             FBringUpSafety;       // safety to prevent reload on server before the weapon is ready

var float             ReloadAnimShortRate;  // reload anim rate for short reload
var float             fReloadEndTime;       // when reaload finished
var float             fReloadSwitchCD;      // time to wait after a reload to switch the weapon


// don't load hud icons
static function PreloadAssets(Inventory Inv, optional bool bSkipRefCount)
{
    local int i;

    if ( !bSkipRefCount )
    {
        default.ReferenceCount++;
    }

    UpdateDefaultMesh(SkeletalMesh(DynamicLoadObject(default.MeshRef, class'SkeletalMesh')));

    for ( i = 0; i < default.SkinRefs.Length; i++ )
        default.Skins[i] = Material(DynamicLoadObject(default.SkinRefs[i], class'Material'));

    if ( KFWeapon(Inv) != none )
    {
        Inv.LinkMesh(default.Mesh);
        KFWeapon(Inv).SelectSound = default.SelectSound;

        for (i = 0; i < default.SkinRefs.Length; i++)
            Inv.Skins[i] = default.Skins[i];
    }
}

// attempt to fix the quick putdown
simulated function bool PutDown()
{
    local int Mode;

    InterruptReload();

    if (bIsReloading)
        return false;

    if (bAimingRifle)
        ZoomOut(False);

    // From Weapon.uc
    if (ClientState == WS_BringUp || ClientState == WS_ReadyToFire)
    {
        if ( (Instigator.PendingWeapon != None) && !Instigator.PendingWeapon.bForceSwitch )
        {
            for (Mode = 0; Mode < NUM_FIRE_MODES; Mode++)
            {
                // if _RO_
                if( FireMode[Mode] == none )
                    continue;
                // End _RO_

                if ( FireMode[Mode].bFireOnRelease && FireMode[Mode].bIsFiring )
                    return false;

                if ( FireMode[Mode].NextFireTime > Level.TimeSeconds + FireMode[Mode].FireRate*(1.f - MinReloadPct))
                    DownDelay = FMax(DownDelay, FireMode[Mode].NextFireTime - Level.TimeSeconds - FireMode[Mode].FireRate*(1.f - MinReloadPct));
            }
        }

        if (Instigator.IsLocallyControlled())
        {
            for (Mode = 0; Mode < NUM_FIRE_MODES; Mode++)
            {
                // if _RO_
                if( FireMode[Mode] == none )
                    continue;
                // End _RO_

                if ( FireMode[Mode].bIsFiring )
                    ClientStopFire(Mode);
            }

            if ((DownDelay <= 0 || KFPawn(Instigator).bIsQuickHealing > 0) && HasAnim(PutDownAnim))
            {
                if( ClientGrenadeState == GN_TempDown || KFPawn(Instigator).bIsQuickHealing > 0)
                    PlayAnim(PutDownAnim, PutDownAnimRate * (PutDownTime/QuickPutDownTime), 0.0);

                else
                    PlayAnim(PutDownAnim, PutDownAnimRate, 0.0);
            }
        }

        ClientState = WS_PutDown;

        if (Level.GRI.bFastWeaponSwitching)
            DownDelay = 0;

        if (DownDelay > 0)
        {
            SetTimer(DownDelay, false);
        }

        else
        {
            if( ClientGrenadeState == GN_TempDown )
                SetTimer(QuickPutDownTime, false);

            else
                SetTimer(PutDownTime, false);
        }
    }

    for (Mode = 0; Mode < NUM_FIRE_MODES; Mode++)
    {
        // if _RO_
        if(FireMode[Mode] == none)
            continue;
        // End _RO_

        FireMode[Mode].bServerDelayStartFire = false;
        FireMode[Mode].bServerDelayStopFire = false;
    }

    Instigator.AmbientSound = None;
    OldWeapon = None;

    return true; // return false if preventing weapon switch
}



// don't randomly unload anything
static function bool UnloadAssets()
{
    default.ReferenceCount--;
    return default.ReferenceCount == 0;
}

// removed weird vanilla shit
simulated function FillToInitialAmmo()
{
   if (bNoAmmoInstances)
   {
      if (AmmoClass[0] != None)
         AmmoCharge[0] = AmmoClass[0].Default.InitialAmount;

      if ((AmmoClass[1] != None) && (AmmoClass[0] != AmmoClass[1]))
         AmmoCharge[1] = AmmoClass[1].Default.InitialAmount;
      return;
   }

   if (Ammo[0] != None)
      Ammo[0].AmmoAmount = Ammo[0].Default.InitialAmount;

   if (Ammo[1] != None)
      Ammo[1].AmmoAmount = Ammo[1].Default.InitialAmount;
}

// removed more vanilla shit
function GiveAmmo(int m, WeaponPickup WP, bool bJustSpawned)
{
   local bool bJustSpawnedAmmo;
   local int addAmount, InitialAmount;
   local KFPawn KFP;
   local KFPlayerReplicationInfo KFPRI;

   KFP = KFPawn(Instigator);

   if(KFP != none)
      KFPRI = KFPlayerReplicationInfo(KFP.PlayerReplicationInfo);

   UpdateMagCapacity(Instigator.PlayerReplicationInfo);

   if (FireMode[m] != None && FireMode[m].AmmoClass != None)
   {
      Ammo[m] = Ammunition(Instigator.FindInventoryType(FireMode[m].AmmoClass));
      bJustSpawnedAmmo = false;

      if (bNoAmmoInstances)
      {
         if ((FireMode[m].AmmoClass == None) || ((m != 0) && (FireMode[m].AmmoClass == FireMode[0].AmmoClass)))
            return;

         InitialAmount = FireMode[m].AmmoClass.Default.InitialAmount;

         if(WP!=none && WP.bThrown==true)
            InitialAmount = WP.AmmoAmount[m];
         else
            MagAmmoRemaining = MagCapacity;

         if (Ammo[m] != None)
         {
            addamount = InitialAmount + Ammo[m].AmmoAmount;
            Ammo[m].Destroy();
         }
         else
            addAmount = InitialAmount;

         AddAmmo(addAmount,m);
      }

      else
      {
         if ((Ammo[m] == None) && (FireMode[m].AmmoClass != None))
         {
            Ammo[m] = Spawn(FireMode[m].AmmoClass, Instigator);
            Instigator.AddInventory(Ammo[m]);
            bJustSpawnedAmmo = true;
         }

         else if ((m == 0) || (FireMode[m].AmmoClass != FireMode[0].AmmoClass))
            bJustSpawnedAmmo = (bJustSpawned || ((WP != None) && !WP.bWeaponStay));

         if (WP != none && WP.bThrown == true)
            addAmount = WP.AmmoAmount[m];

         else if (bJustSpawnedAmmo)
         {
            if (default.MagCapacity == 0)
               addAmount = 0;  // prevent division by zero.
            else
               addAmount = Ammo[m].InitialAmount;
         }

         if (WP != none && m > 0 && (FireMode[m].AmmoClass == FireMode[0].AmmoClass))
            return;

         if(KFPRI != none && KFPRI.ClientVeteranSkill != none)
            Ammo[m].MaxAmmo = float(Ammo[m].MaxAmmo) * KFPRI.ClientVeteranSkill.Static.AddExtraAmmoFor(KFPRI, Ammo[m].Class);

         Ammo[m].AddAmmo(addAmount);
         Ammo[m].GotoState('');
      }
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// using two different anim reload rates
simulated function ClientReload()
{
	local float ReloadMulti;

	if (bHasAimingMode && bAimingRifle)
	{
		FireMode[1].bIsFiring = False;

		ZoomOut(false);
		if( Role < ROLE_Authority)
			ServerZoomOut(false);
	}

	if (KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none)
		ReloadMulti = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.Static.GetReloadSpeedModifier(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo), self);

	else
		ReloadMulti = 1.0;

	bIsReloading = true;

	if (MagAmmoRemaining <= 0)
		PlayAnim(ReloadAnim, ReloadAnimRate * ReloadMulti, 0.1);

	else if (MagAmmoRemaining >= 1)
		PlayAnim(ReloadShortAnim, ReloadAnimShortRate * ReloadMulti, 0.1);
}

// just don't
simulated function DoAutoSwitch(){}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }

   // we failed to drop this weapon on death, award us some cash instead
   else if (Instigator.Health <= 0 && Instigator.PlayerReplicationInfo != none)
   {
      Instigator.PlayerReplicationInfo.Score += SellValue;
      
      if (FHumanPawn(Instigator) != none)
         FHumanPawn(Instigator).FalkSetDosh();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// setting bringup time
simulated function BringUp(optional Weapon PrevWeapon)
{
   if (Level.NetMode == NM_DedicatedServer)
      FBringUpTime = Level.TimeSeconds;

   Super(KFWeapon).BringUp(PrevWeapon);
}


// block reload on bringup
simulated function bool AllowReload()
{
    local FHumanPawn FHP;

    FHP = FHumanPawn(Instigator);

    if (FHP != none && FHP.fReloadState != F_Reload_Allowed)
        return false;

    if (Level.TimeSeconds <= FBringUpTime + BringUpTime + FBringUpSafety)
        return false;

    return Super.AllowReload();
}

// block throwing right after reloading
simulated function bool CanThrow()
{
   if (bIsReloading || Level.TimeSeconds < fReloadEndTime + fReloadSwitchCD)
   {
      return false;
   }

   return Super.CanThrow();
}


// set reload end var
simulated function ActuallyFinishReloading()
{
   //warn("RELOAD END"@Level.TimeSeconds);
   fReloadEndTime = Level.TimeSeconds;
   Super.ActuallyFinishReloading();
}

// set reload end var here too
simulated function ClientFinishReloading()
{
   //warn("RELOAD END 2"@Level.TimeSeconds);
   fReloadEndTime = Level.TimeSeconds;
   Super.ClientFinishReloading();
}

defaultproperties
{
    FlashBoneName="tip"
    MagCapacity=30
    ReloadRate=4.050000
    BringUpTime=0.52000
    PutDownAnim="PutDown"
    Mesh=SkeletalMesh'LairAnimations_A.IronBeastMesh'
    MeshRef="LairAnimations_A.IronBeastMesh"
    ReloadAnim="Reload_EmptySA"
    ReloadShortAnim="ReloadSA"
    WeaponReloadAnim="Reload_M4"
    ReloadShortRate=2.620000
    ReloadAnimRate=1.11000
    ReloadAnimShortRate=1.31000
    Weight=7.000000
    FireModeClass(0)=Class'M4A1IronBeastSAFireFalk'
    Description="A heavier M4A1 variant designed to fire powerful dragon's breath rounds. Great for taking down medium zeds with a barrage of bullets, or by slowly watching them roast. Capable of firing semi-automatically or fully automatically just like the original M4A1."
    PickupClass=Class'M4A1IronBeastSAPickupFalk'
    ItemName="Dragon's Breath M4A1 Iron Beast"
    PlayerViewOffset=(X=-5.000000,Y=18.750000,Z=-7.00000)
    InventoryGroup=3
    Priority=210
    fMaxTry=3
    FBringUpSafety=0.1
    fCZRetryLoc=(Z=25.000000)
    fCXRetryLoc=(X=25.000000)
    fCYRetryLoc=(Y=25.000000)
    fACZRetryLoc=(Z=25.000000)
    fACXRetryLoc=(X=25.000000)
    fACYRetryLoc=(Y=25.000000)
    fReloadSwitchCD=0.2
}
