class SparkGunFalk extends SparkGunBaseFalk;

// Vanilla Spur stuff for the scope
#exec OBJ LOAD FILE=..\textures\ScopeShaders.utx
#exec OBJ LOAD FILE=LairTextures_T.utx

var	bool		bInitializedScope;
var()	Material ScopeMaterial_Textured;
var	string ScopeMaterial_TexturedRef;
var()	Texture ScopeMaterial_Modeled;
var	ScriptedTexture   ScopeScriptedTexture;
var	Combiner	ScopeScriptedCombiner;
var	Shader	ScopeScriptedShader;
var	Material	ScopeOriginalMaterial;
var	string ScopeOriginalMaterialRef;
var()	float	scopePortalFOV;
var()	float	scopePortalFOVHigh;
var()	int	lenseMaterialID;
var()	int	scopePitch;
var()	int	scopePitchHigh;
var()	int	scopeYaw;
var()	int	scopeYawHigh;
var()	vector	XoffsetHighDetail;
var()	vector	XoffsetScoped;

var bool                   fHitHealTarget;         // have we healed someone?
var bool                   fSpawnEffect;           // should we spawn an hit effect?
var vector                 fHitLocation;           // stored hitlocation
var vector                 fHitNormal;             // stored hitnormal
var int                    fHealingAmmoFalk;       // lazy fix but this should work
var int                    fStoredAmmo;            // stored ammo from the pickup
var bool                   fPawnAmmoRestore;       // we restored ammo from the pawn
var bool                   fPickupAmmoRestore;     // we restored ammo from the pickup
var float                  fSyringeFixTime;        // how much time to fix syringe ammo from the pawn
var string                 fMultiHealStr;

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

replication
{
	reliable if (Role == ROLE_Authority)
		fHitHealTarget, fHitLocation, fHitNormal, fHealingAmmoFalk, fStoredAmmo;
}

// just don't
simulated function DoAutoSwitch(){}

//Scope code as copied from LRifleEX
static function PreloadAssets(Inventory Inv, optional bool bSkipRefCount)
{
	super.PreloadAssets(Inv, bSkipRefCount);

	default.ScopeMaterial_Textured = FinalBlend(DynamicLoadObject(default.ScopeMaterial_TexturedRef, class'FinalBlend', true));
	default.ScopeOriginalMaterial = texture(DynamicLoadObject(default.ScopeOriginalMaterialRef, class'texture', true));

	if ( Spur(Inv) != none )
	{
		Spur(Inv).ScopeMaterial_Textured = default.ScopeMaterial_Textured;
		Spur(Inv).ScopeOriginalMaterial = default.ScopeOriginalMaterial;
	}
}

static function bool UnloadAssets()
{
	if ( super.UnloadAssets() )
	{
		default.ScopeMaterial_Textured = none;
		default.ScopeOriginalMaterial = none;
	}

	return true;
}

exec function pfov(int thisFOV)
{
	if( !class'ROEngine.ROLevelInfo'.static.RODebugMode() )
		return;

	scopePortalFOV = thisFOV;
}

exec function pPitch(int num)
{
	if( !class'ROEngine.ROLevelInfo'.static.RODebugMode() )
		return;

	scopePitch = num;
	scopePitchHigh = num;
}

exec function pYaw(int num)
{
	if( !class'ROEngine.ROLevelInfo'.static.RODebugMode() )
		return;

	scopeYaw = num;
	scopeYawHigh = num;
}

simulated exec function TexSize(int i, int j)
{
	if( !class'ROEngine.ROLevelInfo'.static.RODebugMode() )
		return;

	ScopeScriptedTexture.SetSize(i, j);
}

simulated function bool ShouldDrawPortal()
{
	if (bAimingRifle)
		return true;
	else
		return false;
}

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
	KFScopeDetail = class'KFMod.KFWeapon'.default.KFScopeDetail;
	UpdateScopeMode();
}

simulated function UpdateScopeMode()
{
	if (Level.NetMode != NM_DedicatedServer && Instigator != none && Instigator.IsLocallyControlled() &&
	Instigator.IsHumanControlled() )
	{
	if( KFScopeDetail == KF_ModelScope )
		{
			scopePortalFOV = default.scopePortalFOV;
			ZoomedDisplayFOV = CalcAspectRatioAdjustedFOV(default.ZoomedDisplayFOV);
			if (bAimingRifle)
				{
				PlayerViewOffset = XoffsetScoped;
				}

			if( ScopeScriptedTexture == none )
				{
				ScopeScriptedTexture = ScriptedTexture(Level.ObjectPool.AllocateObject(class'ScriptedTexture'));
				}

			ScopeScriptedTexture.FallBackMaterial = ScopeOriginalMaterial;
			ScopeScriptedTexture.SetSize(512,512);
			ScopeScriptedTexture.Client = Self;

			if( ScopeScriptedCombiner == none )
			{
				ScopeScriptedCombiner = Combiner(Level.ObjectPool.AllocateObject(class'Combiner'));
				ScopeScriptedCombiner.Material1 = ScopeMaterial_Modeled;
				ScopeScriptedCombiner.FallbackMaterial = Shader'ScopeShaders.Zoomblur.LensShader';
				ScopeScriptedCombiner.CombineOperation = CO_Multiply;
				ScopeScriptedCombiner.AlphaOperation = AO_Use_Mask;
				ScopeScriptedCombiner.Material2 = ScopeScriptedTexture;
			}

			if( ScopeScriptedShader == none )
			{
				ScopeScriptedShader = Shader(Level.ObjectPool.AllocateObject(class'Shader'));
				ScopeScriptedShader.Diffuse = ScopeScriptedCombiner;
				ScopeScriptedShader.SelfIllumination = ScopeScriptedCombiner;
				ScopeScriptedShader.FallbackMaterial = Shader'ScopeShaders.Zoomblur.LensShader';
			}

			bInitializedScope = true;
		}
		else if( KFScopeDetail == KF_ModelScopeHigh )
		{
			scopePortalFOV = scopePortalFOVHigh;
			ZoomedDisplayFOV = CalcAspectRatioAdjustedFOV(default.ZoomedDisplayFOVHigh);

			if (bAimingRifle)
			{
				PlayerViewOffset = XoffsetHighDetail;
			}

			if( ScopeScriptedTexture == none )
			{
				ScopeScriptedTexture = ScriptedTexture(Level.ObjectPool.AllocateObject(class'ScriptedTexture'));
			}
			
			ScopeScriptedTexture.FallBackMaterial = ScopeOriginalMaterial;
			ScopeScriptedTexture.SetSize(1024,1024);
			ScopeScriptedTexture.Client = Self;

			if( ScopeScriptedCombiner == none )
			{
				ScopeScriptedCombiner = Combiner(Level.ObjectPool.AllocateObject(class'Combiner'));
				ScopeScriptedCombiner.Material1 = ScopeMaterial_Modeled;
				ScopeScriptedCombiner.FallbackMaterial = Shader'ScopeShaders.Zoomblur.LensShader';
				ScopeScriptedCombiner.CombineOperation = CO_Multiply;
				ScopeScriptedCombiner.AlphaOperation = AO_Use_Mask;
				ScopeScriptedCombiner.Material2 = ScopeScriptedTexture;
			}

			if( ScopeScriptedShader == none )
			{
				ScopeScriptedShader = Shader(Level.ObjectPool.AllocateObject(class'Shader'));
				ScopeScriptedShader.Diffuse = ScopeScriptedCombiner;
				ScopeScriptedShader.SelfIllumination = ScopeScriptedCombiner;
				ScopeScriptedShader.FallbackMaterial = Shader'ScopeShaders.Zoomblur.LensShader';
			}

			bInitializedScope = true;
		}
		else if (KFScopeDetail == KF_TextureScope)
		{
			ZoomedDisplayFOV = CalcAspectRatioAdjustedFOV(default.ZoomedDisplayFOV);
			PlayerViewOffset.X = default.PlayerViewOffset.X;

			bInitializedScope = true;
		}
	}
}

simulated event RenderTexture(ScriptedTexture Tex)
{
	local rotator RollMod;
	RollMod = Instigator.GetViewRotation();
	
	if(Owner != none && Instigator != none && Tex != none && Tex.Client != none)
		Tex.DrawPortal(0,0,Tex.USize,Tex.VSize,Owner,(Instigator.Location + Instigator.EyePosition()), RollMod,  scopePortalFOV );
}

simulated function SetZoomBlendColor(Canvas c)
{
	local Byte	val;
	local Color   clr;
	local Color   fog;

	clr.R = 255;
	clr.G = 255;
	clr.B = 255;
	clr.A = 255;

	if( Instigator.Region.Zone.bDistanceFog )
	{
		fog = Instigator.Region.Zone.DistanceFogColor;
		val = 0;
		val = Max( val, fog.R);
		val = Max( val, fog.G);
		val = Max( val, fog.B);
		if( val > 128 )
		{
			val -= 128;
			clr.R -= val;
			clr.G -= val;
			clr.B -= val;
		}
	}
	c.DrawColor = clr;
}

simulated function ZoomIn(bool bAnimateTransition)
{
	super(BaseKFWeapon).ZoomIn(bAnimateTransition);

	// Copied in from dualies version
    if( bAnimateTransition )
    {
        if( bZoomOutInterrupted )
        {
            PlayAnim('SightUp',1.0,0.1);
        }
        else
        {
            PlayAnim('SightUp',1.0,0.1);
        }
    }
	
	bAimingRifle = True;
	if( KFHumanPawn(Instigator)!=None )
	KFHumanPawn(Instigator).SetAiming(True);

	if( Level.NetMode != NM_DedicatedServer && KFPlayerController(Instigator.Controller) != none )
	{
		if( AimInSound != none )
		{
			PlayOwnedSound(AimInSound, SLOT_Interact,,,,, false);
		}
	}
}

simulated function ZoomOut(bool bAnimateTransition)
{
    local float AnimLength, AnimSpeed;

	super.ZoomOut(bAnimateTransition);
	bAimingRifle = False;

	//inserted from Dualies version
	if( bAnimateTransition )
    {
        AnimLength = GetAnimDuration('SightDown', 1.0);

        if( ZoomTime > 0 && AnimLength > 0 )
        {
            AnimSpeed = AnimLength/ZoomTime;
        }
        else
        {
            AnimSpeed = 1.0;
        }
        PlayAnim('SightDown',AnimSpeed,0.1);
    }
	// End dualies insert

	if( KFHumanPawn(Instigator)!=None )
	KFHumanPawn(Instigator).SetAiming(False);

	if( Level.NetMode != NM_DedicatedServer && KFPlayerController(Instigator.Controller) != none )
	{
		if( AimOutSound != none )
		{
			PlayOwnedSound(AimOutSound, SLOT_Interact,,,,, false);
		}
		KFPlayerController(Instigator.Controller).TransitionFOV(KFPlayerController(Instigator.Controller).DefaultFOV,0.0);
	}
}

simulated event OnZoomInFinished()
{
	local name anim;
	local float frame, rate;

	GetAnimParams(0, anim, frame, rate);

	if (ClientState == WS_ReadyToFire)
	{
		if (anim == IdleAnim)
		{
			PlayIdle();
		}
	}

	if( Level.NetMode != NM_DedicatedServer && KFPlayerController(Instigator.Controller) != none &&
	KFScopeDetail == KF_TextureScope )
	{
		KFPlayerController(Instigator.Controller).TransitionFOV(PlayerIronSightFOV,0.0);
	}
}

simulated event RenderOverlays(Canvas Canvas)
{
	local int m;
	local PlayerController PC;
	
	if (Instigator == None)
		return;
	PC = PlayerController(Instigator.Controller);
	
	if(PC == None)
		return;

	if(!bInitializedScope && PC != none )
	{
		UpdateScopeMode();
	}

	Canvas.DrawActor(None, false, true); 

	for (m = 0; m < NUM_FIRE_MODES; m++)
	{
		if (FireMode[m] != None)
		{
			FireMode[m].DrawMuzzleFlash(Canvas);
		}
	}

	SetLocation( Instigator.Location + Instigator.CalcDrawOffset(self) );
	SetRotation( Instigator.GetViewRotation() + ZoomRotInterp);
	PreDrawFPWeapon();

 	if(bAimingRifle && PC != none && (KFScopeDetail == KF_ModelScope || KFScopeDetail == KF_ModelScopeHigh))
 	{
		if (ShouldDrawPortal())
		{
			if ( ScopeScriptedTexture != none )
			{
				Skins[LenseMaterialID] = ScopeScriptedShader;
				ScopeScriptedTexture.Client = Self;   
				ScopeScriptedTexture.Revision = (ScopeScriptedTexture.Revision +1);
			}
		}

		bDrawingFirstPerson = true;
		
		Canvas.DrawBoundActor(self, false, false,DisplayFOV,PC.Rotation,rot(0,0,0),Instigator.CalcZoomedDrawOffset(self));
		bDrawingFirstPerson = false;
	}
	
	else if( KFScopeDetail == KF_TextureScope && PC.DesiredFOV == PlayerIronSightFOV && bAimingRifle)
	{
		Skins[LenseMaterialID] = ScopeOriginalMaterial;
		SetZoomBlendColor(Canvas);
		Canvas.Style = ERenderStyle.STY_Normal;
		Canvas.SetPos(0, 0);
		Canvas.DrawTile(ScopeMaterial_Textured, (Canvas.SizeX - Canvas.SizeY) / 2, Canvas.SizeY, 0.0, 0.0, 8, 8);
		Canvas.SetPos(Canvas.SizeX, 0);
		Canvas.DrawTile(ScopeMaterial_Textured, -(Canvas.SizeX - Canvas.SizeY) / 2, Canvas.SizeY, 0.0, 0.0, 8, 8);
		Canvas.Style = 255;
		Canvas.SetPos((Canvas.SizeX - Canvas.SizeY) / 2,0);
		Canvas.DrawTile(ScopeMaterial_Textured, Canvas.SizeY, Canvas.SizeY, 0.0, 0.0, 1024, 1024);
		Canvas.SetPos(Canvas.SizeX * 0.16, Canvas.SizeY * 0.47);
	}
 	else
 	{
		Skins[LenseMaterialID] = ScopeOriginalMaterial;
		bDrawingFirstPerson = true;
		Canvas.DrawActor(self, false, false, DisplayFOV);
		bDrawingFirstPerson = false;
 	}
}



simulated function float CalcAspectRatioAdjustedFOV(float AdjustFOV)
{
	local KFPlayerController KFPC;
	local float ResX, ResY;
	local float AspectRatio;
	KFPC = KFPlayerController(Level.GetLocalPlayerController());

	if( KFPC == none )
	{
		return AdjustFOV;
	}

	ResX = float(GUIController(KFPC.Player.GUIController).ResX);
	ResY = float(GUIController(KFPC.Player.GUIController).ResY);
	AspectRatio = ResX / ResY;

	if ( KFPC.bUseTrueWideScreenFOV && AspectRatio >= 1.60 )
	{
		return CalcFOVForAspectRatio(AdjustFOV);
	}
	else
	{
		return AdjustFOV;
	}
}

simulated function AdjustIngameScope()
{
	local PlayerController PC;
	PC = PlayerController(Instigator.Controller);

	if( !bHasScope )
		return;

	switch (KFScopeDetail)
	{
	case KF_ModelScope:
		if( bAimingRifle )
			DisplayFOV = CalcAspectRatioAdjustedFOV(default.ZoomedDisplayFOV);
		if ( PC.DesiredFOV == PlayerIronSightFOV && bAimingRifle )
		{
			if( Level.NetMode != NM_DedicatedServer && KFPlayerController(Instigator.Controller) != none )
			{
				KFPlayerController(Instigator.Controller).TransitionFOV(KFPlayerController(Instigator.Controller).DefaultFOV,0.0);
			}
		}
		break;

	case KF_TextureScope:
		if( bAimingRifle )
			DisplayFOV = CalcAspectRatioAdjustedFOV(default.ZoomedDisplayFOV);
		if ( bAimingRifle && PC.DesiredFOV != PlayerIronSightFOV )
		{
			if( Level.NetMode != NM_DedicatedServer && KFPlayerController(Instigator.Controller) != none )
			{
				KFPlayerController(Instigator.Controller).TransitionFOV(PlayerIronSightFOV,0.0);
			}
		}
		break;

	case KF_ModelScopeHigh:
		if( bAimingRifle )
		{
			if( ZoomedDisplayFOVHigh > 0 )
			{
				DisplayFOV = CalcAspectRatioAdjustedFOV(default.ZoomedDisplayFOVHigh);
			}
			else
			{
				DisplayFOV = CalcAspectRatioAdjustedFOV(default.ZoomedDisplayFOV);
			}
		}
		if ( bAimingRifle && PC.DesiredFOV == PlayerIronSightFOV )
		{
		if( Level.NetMode != NM_DedicatedServer && KFPlayerController(Instigator.Controller) != none )
			{
			KFPlayerController(Instigator.Controller).TransitionFOV(KFPlayerController(Instigator.Controller).DefaultFOV,0.0);
			}
		}
		break;
	}

	UpdateScopeMode();
}

simulated event Destroyed()
{
	if (ScopeScriptedTexture != None)
	{
		ScopeScriptedTexture.Client = None;
		Level.ObjectPool.FreeObject(ScopeScriptedTexture);
		ScopeScriptedTexture=None;
	}

	if (ScopeScriptedCombiner != None)
	{
		ScopeScriptedCombiner.Material2 = none;
		Level.ObjectPool.FreeObject(ScopeScriptedCombiner);
		ScopeScriptedCombiner = none;
	}

	if (ScopeScriptedShader != None)
	{
		ScopeScriptedShader.Diffuse = none;
		ScopeScriptedShader.SelfIllumination = none;
		Level.ObjectPool.FreeObject(ScopeScriptedShader);
		ScopeScriptedShader = none;
	}

	Super.Destroyed();
}

simulated function PreTravelCleanUp()
{
	if (ScopeScriptedTexture != None)
	{
		ScopeScriptedTexture.Client = None;
		Level.ObjectPool.FreeObject(ScopeScriptedTexture);
		ScopeScriptedTexture=None;
	}

	if (ScopeScriptedCombiner != None)
	{
		ScopeScriptedCombiner.Material2 = none;
		Level.ObjectPool.FreeObject(ScopeScriptedCombiner);
		ScopeScriptedCombiner = none;
	}

	if (ScopeScriptedShader != None)
	{
		ScopeScriptedShader.Diffuse = none;
		ScopeScriptedShader.SelfIllumination = none;
		Level.ObjectPool.FreeObject(ScopeScriptedShader);
		ScopeScriptedShader = none;
	}
}
//End Scope Code

//Everything below until DefaultProperties came from KFMod.HuskGun
function float GetAIRating()
{
	local AIController B;

	B = AIController(Instigator.Controller);
	if ( (B == None) || (B.Enemy == None) )
		return AIRating;

	return (AIRating + 0.0003 * FClamp(1500 - VSize(B.Enemy.Location - Instigator.Location),0,1000));
}

function byte BestMode()
{
	return 0;
}

function bool RecommendRangedAttack()
{
	return true;
}

function bool RecommendLongRangedAttack()
{
	return true;
}

function float SuggestAttackStyle()
{
	return -1.0;
}

simulated event OnZoomOutFinished()
{
	local name anim;
	local float frame, rate;

	GetAnimParams(0, anim, frame, rate);

	if (ClientState == WS_ReadyToFire)
	{
		// Play the regular idle anim when we're finished zooming out
		if (anim == IdleAimAnim)
		{
            PlayIdle();
		}
	}
}

// testing stuff, just return heal ammo charge
simulated function int AmmoAmount(int mode)
{
	return fHealingAmmoFalk;
}

// more testing stuff, again just with 
simulated function bool AmmoMaxed(int mode)
{
	return fHealingAmmoFalk >= Default.fHealingAmmoFalk;
}

// don't pls
function ServerRequestAutoReload()
{
}

// nope
exec function ReloadMeNow()
{
}

// meh
simulated function ClientReload()
{
}

// consume healing ammo
simulated function bool ConsumeAmmo(int Mode, float load, optional bool bAmountNeededIsMax)
{
   if (Mode == 0)
   {
      if (load > fHealingAmmoFalk)        
         return False;

      fHealingAmmoFalk -= Load;

      return True;
   }

   return False;
}

// using default.healammocharge
simulated function float AmmoStatus(optional int Mode) // returns float value for ammo amount
{
   if (Mode == 0)
	   return float(fHealingAmmoFalk) / float(Default.fHealingAmmoFalk);

	else
	   return super.AmmoStatus(Mode);
}

// removed weird shit
function bool AddAmmo(int AmmoToAdd, int Mode)
{
   return False;
}

// just healing ammo again
simulated function GetAmmoCount(out float MaxAmmoPrimary, out float CurAmmoPrimary)
{
   MaxAmmoPrimary = Default.fHealingAmmoFalk;
   CurAmmoPrimary = fHealingAmmoFalk;
}

// edited to use default.healammocharge
simulated function MaxOutAmmo()
{
	fHealingAmmoFalk = Default.fHealingAmmoFalk;
}

// testing shit
simulated function bool HasAmmo()
{
    if (fHealingAmmoFalk >= FireModeClass[0].Default.AmmoPerFire)
        return true;

    return false;
}

// fire only if we got ammo
simulated function bool StartFire(int Mode)
{
   if (fHealingAmmoFalk > FireModeClass[0].Default.AmmoPerFire)
	   return super(KFMedicGun).StartFire(Mode);

   return False;
}

// not needed?
simulated function SuperMaxOutAmmo()
{
   MaxOutAmmo();
}

// removed weird vanilla shit
simulated function FillToInitialAmmo()
{
   if (bNoAmmoInstances)
   {
      if (AmmoClass[0] != None)
         AmmoCharge[0] = AmmoClass[0].Default.InitialAmount;

      if ((AmmoClass[1] != None) && (AmmoClass[0] != AmmoClass[1]))
         AmmoCharge[1] = AmmoClass[1].Default.InitialAmount;
      return;
   }

   if (Ammo[0] != None)
      Ammo[0].AmmoAmount = Ammo[0].Default.InitialAmount;

   if (Ammo[1] != None)
      Ammo[1].AmmoAmount = Ammo[1].Default.InitialAmount;
}

// Fixed stuff and added a second force reload
simulated function Tick(float dt)
{
   local class<FVeterancyTypes> FVet;
   local KFPlayerReplicationInfo KFPRI;

   // falk, lazy falk.
   if (bIsReloading)
      bIsReloading = False;

	if (Level.NetMode != NM_Client)
	{
      // attempting to restore stored ammo from the pawn
      if (!fPawnAmmoRestore)
      {
         if (fStoredAmmo == -1 && FHumanPawn(Instigator) != none)
         {
            if (FHumanPawn(Instigator).fStoredSyringeAmmo > -1 && FHumanPawn(Instigator).fStoredSyringeTime + fSyringeFixTime >= Level.TimeSeconds)
            {
               fStoredAmmo = FHumanPawn(Instigator).fStoredSyringeAmmo;
               //warn("Tick recovering from pawn: "@fStoredAmmo);
               fPawnAmmoRestore = True;
            }

            else
            {
               //warn("F2");
               fPawnAmmoRestore = True;
               
               if (!fPickupAmmoRestore)
               {
                  fPickupAmmoRestore = True;
                  fHealingAmmoFalk   = 0;
               }
            }
         }

         else
         {
            //warn("F1");
            fPawnAmmoRestore = True;
         }
      }

      // setting ammo back on pickup, final step
      if (fStoredAmmo > -1)
      {
         fHealingAmmoFalk = fStoredAmmo;
         fStoredAmmo      = -1;
      }

      else if (fHealingAmmoFalk < Default.fHealingAmmoFalk && RegenTimer < Level.TimeSeconds)
      {
         RegenTimer = Level.TimeSeconds + AmmoRegenRate;
         KFPRI      = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

		   if (KFPRI != none)
         {
            FVet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);
            
            if (FVet != none)
               fHealingAmmoFalk += 100 * FVet.Static.GetSparkGunChargeRate(KFPRI);

		      else
			      fHealingAmmoFalk += 100;
         }

		   else
			   fHealingAmmoFalk += 100;

		   if (fHealingAmmoFalk > Default.fHealingAmmoFalk)
			   fHealingAmmoFalk = Default.fHealingAmmoFalk;
      }
	}
}

// Return a float value representing the current healing charge amount
simulated function float ChargeBar()
{
	return FClamp(float(fHealingAmmoFalk) / float(Default.fHealingAmmoFalk),0,1);
}

// The server lets the client know they successfully healed someone
simulated function ClientSuccessfulHeal(String HealedName)
{
   //warn(Level.Game.ParseKillMessage("You",HealedName,SuccessfulHealMessage));
   if (HealedName != fMultiHealStr)
      PlayerController(Instigator.controller).ClientMessage("You repaired "$HealedName$"'s kevlar", 'CriticalEvent');

   else
      PlayerController(Instigator.controller).ClientMessage("You repaired the kevlar of "$HealedName, 'CriticalEvent');
}

// should throw if low on ammo
simulated function bool CanThrow()
{
   if (FireMode[0].AmmoPerFire >= fHealingAmmoFalk)
   {
      if (FireMode[0].bFireOnRelease && FireMode[0].bIsFiring)
         return false;

      if (FireMode[0].NextFireTime > Level.TimeSeconds)
         return false;
   }

   else
      bCanThrow = True;

   return (bCanThrow && /*!bIsReloading &&*/ (ClientState == WS_ReadyToFire || (Level.NetMode == NM_DedicatedServer) || (Level.NetMode == NM_ListenServer)));
}


// restore ammo from the pawn if they're fresh enough
function PickupFunction(Pawn Other)
{
   Super.PickupFunction(Other);

   if (fStoredAmmo == -1 && FHumanPawn(Other) != none &&
         FHumanPawn(Other).fStoredSyringeAmmo > -1      &&
         FHumanPawn(Other).fStoredSyringeTime + fSyringeFixTime >= Level.TimeSeconds)
   {
      fStoredAmmo = FHumanPawn(Other).fStoredSyringeAmmo;
      //warn("Restoring ammo from the pawn: "@fStoredAmmo);
      FHumanPawn(Other).fStoredSyringeAmmo = -1;
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
      {
         DetachFromPawn(Instigator);

         if (FHumanPawn(Instigator) != none && fHealingAmmoFalk > -1)
         {
            //warn("Storing ammo to the pawn: "@fHealingAmmoFalk);
            FHumanPawn(Instigator).fStoredSyringeAmmo = fHealingAmmoFalk;
            FHumanPawn(Instigator).fStoredSyringeTime = Level.TimeSeconds;
         }
      }

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      // storing ammo into the pickup
      if (SparkGunPickupFalk(Pickup) != none)
      {
         //warn("Storing ammo to the pickup: "@fHealingAmmoFalk);
         SparkGunPickupFalk(Pickup).fStoredAmmo = fHealingAmmoFalk;
      }

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }

   // we failed to drop this weapon on death, award us some cash instead
   else if (Instigator.Health <= 0 && Instigator.PlayerReplicationInfo != none)
   {
      Instigator.PlayerReplicationInfo.Score += SellValue;
      
      if (FHumanPawn(Instigator) != none)
         FHumanPawn(Instigator).FalkSetDosh();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

defaultproperties
{
   ReloadAnim=""
   WeaponReloadAnim=""
   fHealingAmmoFalk=5000
   AmmoRegenRate=0.300000 // same as medic browning
   FireModeClass[0]=Class'SparkGunFireFalk'
   FireModeClass[1]=Class'KFMod.NoFire'
   PickupClass=Class'SparkGunPickupFalk'
   Description="Repair your teammates' armors at a distance with this handy tool. Recharges over time. Supports are the most efficient users of the Spark."
   ItemName="Spark Aid Gun"
   fStoredAmmo=-1
   fSyringeFixTime=0.5

   ScopeMaterial_Textured=FinalBlend'PolarStar_R.Scope6Reticle_FB'
   ScopeMaterial_TexturedRef="PolarStar_R.Scope6Reticle_FB"
   ScopeMaterial_Modeled=Texture'LairTextures_T.CustomReskins.SparkGunSight'
   ScopeOriginalMaterial=Texture'PolarStar_R.PolarStar_sight_blank'
   ScopeOriginalMaterialRef="PolarStar_R.PolarStar_sight_blank"
   scopePortalFOV=12
   scopePortalFOVHigh=12 //22 //What the scope sees on Modeled(High)
   lenseMaterialID=2
   ZoomedDisplayFOVHigh=22 //35 //What the player's FOV is on Modeled(High)
   XoffsetScoped=(X=0.0,Y=0.0,Z=0.0)
   scopePitch=0
   scopeYaw=0
   XoffsetHighDetail=(X=0.0,Y=0.0,Z=0.0)
   scopePitchHigh=0
   scopeYawHigh=0
   KFScopeDetail=KF_ModelScope
   bHasScope=true

   IdleAimAnim=Idle_Iron

   SleeveNum=0
   Skins(1)=Texture'LairTextures_T.CustomReskins.SparkGun'
   SkinRefs(1)="LairTextures_T.CustomReskins.SparkGun"
   Skins(2)=Texture'PolarStar_R.PolarStar_sight_blank'
   SkinRefs(2)="PolarStar_R.PolarStar_sight_blank"

   Weight=1
   PutDownAnim="PutDown"
   SelectSound=None
   SelectSoundRef=""
   SelectForce="SwitchToAssaultRifle"
   AIRating=0.650000
   CurrentRating=0.650000
   Priority=10
   InventoryGroup=5
   GroupOffset=1
   PlayerViewOffset=(X=20,Y=40,Z=-10)
   BobDamping=6.000000
   AttachmentClass=Class'SparkGunAttachmentFalk'
   IconCoords=(X1=253,Y1=146,X2=333,Y2=181)
   LightType=LT_None
   LightBrightness=0.000000
   LightRadius=0.000000
   Mesh=SkeletalMesh'PolarStar_R.PSMesh1st'
   MeshRef="PolarStar_R.PSMesh1st"
   DrawScale=1.000000
   AmbientGlow=0

   ZoomTime=0.25
   FastZoomOutTime=0.2
   ZoomInRotation=(Pitch=-910,Yaw=0,Roll=2910)
   bHasAimingMode=true

   DisplayFOV=70
   StandardDisplayFOV=70.0
   PlayerIronSightFOV=75
   ZoomedDisplayFOV=65

   HudImage=texture'LairTextures_T.CustomReskins.SparkGunUnselect'
   HudImageRef="LairTextures_T.CustomReskins.SparkGunUnselect"
   SelectedHudImage=texture'LairTextures_T.CustomReskins.SparkGunSelect'
   SelectedHudImageRef="LairTextures_T.CustomReskins.SparkGunSelect"
   TraderInfoTexture=texture'LairTextures_T.CustomReskins.SparkGunTrader'

   fMultiHealStr="multiple targets"

   fMaxTry=3
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
