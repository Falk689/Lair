class MedicBrowningPickupFalk extends KFWeaponPickup;

#exec OBJ LOAD FILE=LairStaticMeshes_SM.usx
#exec OBJ LOAD FILE=LairSounds_S.uax

var int fStoredAmmo; // remaining ammo stored into the pickup when dropped

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// set fStoredAmmo to the weapon class on pickup
auto state pickup
{
   function BeginState()
   {
      UntriggerEvent(Event, self, None);
      if ( bDropped )
      {
         AddToNavigation();
         SetTimer(20, false);
      }
   }

   // When touched by an actor.  Let's mod this to account for Weights. (Player can't pickup items)
   // IF he's exceeding his max carry weight.
   function Touch(Actor Other)
   {
      local FHumanPawn FHP;
      local Inventory Copy;
      local Falk689GameTypeBase Flk;

      if ( KFHumanPawn(Other) != none && !CheckCanCarry(KFHumanPawn(Other)) )
      {
         return;
      }

      // If touched by a player pawn, let him pick this up.
      if ( ValidTouch(Other) )
      {
         Copy = SpawnCopy(Pawn(Other));

         AnnouncePickup(Pawn(Other));
         SetRespawn();
 
         if (Copy != None)
         {
            if (MedicBrowningFalk(Copy) != none)
            {
               Flk = Falk689GameTypeBase(Level.Game);

               if (Flk != none && Flk.InZedTime())
                  MedicBrowningFalk(Copy).StartForceReload();

               else if (MedicBrowningFalk(Copy).fStoredAmmo == -1)
               {
                  MedicBrowningFalk(Copy).fStoredAmmo        = fStoredAmmo;
                  MedicBrowningFalk(Copy).fPickupAmmoRestore = True;
                  MedicBrowningFalk(Copy).fPawnAmmoRestore   = True;
                  //Warn("Recovering from pickup: "@fStoredAmmo);
               }
            }

            Copy.PickupFunction(Pawn(Other));
         }

         if ( MySpawner != none && KFGameType(Level.Game) != none )
         {
            KFGameType(Level.Game).WeaponPickedUp(MySpawner);
         }

         if (KFWeapon(Copy) != none)
         {
            // sell price scaling based on difficulty
            if (!bDropped)
            {
               //warn("VRG");

               FHP = FHumanPawn(Other);

               if (FHP != none)
               {
                  if (Level.Game.GameDifficulty >= 8.0)       // BB
                  {
                     SellValue = cost * FHP.fBBRPCostMulti;
                     //warn("BB");
                  }

                  else if (Level.Game.GameDifficulty >= 7.0)  // HOE
                  {
                     SellValue = cost * FHP.fHOERPCostMulti;
                     //warn("HOE");
                  }

                  else if (Level.Game.GameDifficulty >= 5.0)  // suicidal
                  {
                     SellValue = cost * FHP.fSuicidalRPCostMulti;
                     //warn("SUI");
                  }

                  else if (Level.Game.GameDifficulty >= 4.0)  // hard
                  {
                     SellValue = cost * FHP.fHardRPCostMulti;
                     //warn("HRD");
                  }
               }
            }

            //warn("VAL:"@SellValue);

            KFWeapon(Copy).SellValue = SellValue;
            KFWeapon(Copy).bPreviouslyDropped = bDropped;

            if (!bPreviouslyDropped && KFWeapon(Copy).bIsTier3Weapon && Pawn(Other).Controller != none && Pawn(Other).Controller != DroppedBy)
            {
               KFWeapon(Copy).Tier3WeaponGiver = DroppedBy;
            }
         }
      }
   }
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=2.000000
   cost=650
   AmmoCost=16
   BuyClipSize=8
   PowerValue=53
   SpeedValue=20
   RangeValue=100
   ItemName="Medical Browning High-Power"
   ItemShortName="HP-M"
   AmmoItemName=".300 JHP rounds"
   InventoryType=Class'MedicBrowningFalk'
   PickupMessage="You got a Medical Browning High-Power"
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
   PickupForce="AssaultRiflePickup"
   CollisionHeight=5.000000
   StaticMesh=StaticMesh'LairStaticMeshes_SM.CustomReskins.MedicBrowningPickup'
   DrawScale=0.6
}
