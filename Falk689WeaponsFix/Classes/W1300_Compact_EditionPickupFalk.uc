class W1300_Compact_EditionPickupFalk extends W1300_Compact_Edition.W1300_Compact_EditionPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=5.000000
   InventoryType=Class'W1300_Compact_EditionFalk'
   cost=1800
   BuyClipSize=4
   AmmoCost=8
   ItemName="W1300 Compact Edition"
   ItemShortName="W1300"
   PickupMessage="You got a W1300 Compact Edition"
   PowerValue=72
   SpeedValue=30
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}