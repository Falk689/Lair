class M60AmmoFalk extends M60Wep.M60Ammo;

defaultproperties
{
   AmmoPickupAmount=38 // one quarter of magazine, heavy weapons standard
   MaxAmmo=300
   InitialAmount=150
   PickupClass=Class'M60AmmoPickupFalk'
}