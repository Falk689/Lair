class LocalMediShotFalk extends LocalMediShotBaseFalk;

#exec OBJ LOAD FILE=LairTextures_T.utx

var float fReloadSpeedFix; // reload time speed fix

// just don't
simulated function DoAutoSwitch(){}

// Fixed stuff and added a second force reload
simulated function Tick(float dt)
{
	if (Level.NetMode != NM_Client)
	{
      // attempting to restore stored ammo from the pawn
      if (!fPawnAmmoRestore)
      {
         if (fStoredAmmo == -1 && FHumanPawn(Instigator) != none)
         {
            if (FHumanPawn(Instigator).fStoredSyringeAmmo > -1 && FHumanPawn(Instigator).fStoredSyringeTime + fSyringeFixTime >= Level.TimeSeconds)
            {
               fStoredAmmo = FHumanPawn(Instigator).fStoredSyringeAmmo;
               //warn("Tick recovering from pawn: "@fStoredAmmo);
               fPawnAmmoRestore = True;
            }

            else
            {
               //warn("F2");
               fPawnAmmoRestore = True;
               
               if (!fPickupAmmoRestore)
               {
                  fPickupAmmoRestore = True;
                  fHealingAmmoFalk   = 0;
               }
            }
         }

         else
         {
            //warn("F1");
            fPawnAmmoRestore = True;
         }
      }

      // setting ammo back on pickup, final step
      if (fStoredAmmo > -1)
      {
         fHealingAmmoFalk = fStoredAmmo;
         fStoredAmmo      = -1;
      }

      if (fShouldReload)
         fShouldReload = ForceHealingReload();

      else if (fHealingAmmoFalk < Default.fHealingAmmoFalk && RegenTimer < Level.TimeSeconds)
      {
         RegenTimer = Level.TimeSeconds + AmmoRegenRate;

		   if (KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none)
			   fHealingAmmoFalk += 100 * KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.Static.GetSyringeChargeRate(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo));

		   else
			   fHealingAmmoFalk += 100;

		   if (fHealingAmmoFalk > Default.fHealingAmmoFalk)
			   fHealingAmmoFalk = Default.fHealingAmmoFalk;
      }
	}
}

// override to activate infinite medic ammo on zed time
simulated function bool ConsumeAmmo(int Mode, float load, optional bool bAmountNeededIsMax)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   if (Mode == 1)
   {
      // if we're medic and in zed time we get infinite medic darts
      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

		if (KFPRI                    != none &&
          KFPRI.ClientVeteranSkill != none)
      {

         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none && Vet.Static.InfiniteMedicDarts(KFPRI)) 
            return True;
      }

      if(Load > fHealingAmmoFalk)
         return False;

      fHealingAmmoFalk -= Load;
      return True;
   }

   return super(MP7MMedicGun).ConsumeAmmo(Mode, load, bAmountNeededIsMax);
}


// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;


   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
      {
         DetachFromPawn(Instigator);

         if (FHumanPawn(Instigator) != none && fHealingAmmoFalk > -1)
         {
            //warn("Storing ammo to the pawn: "@fHealingAmmoFalk);
            FHumanPawn(Instigator).fStoredSyringeAmmo = fHealingAmmoFalk;
            FHumanPawn(Instigator).fStoredSyringeTime = Level.TimeSeconds;
         }
      }

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      // storing ammo into the pickup
      if (MediShotPickupFalk(Pickup) != none)
      {
         //warn("Storing ammo to the pickup: "@fHealingAmmoFalk);
         MediShotPickupFalk(Pickup).fStoredAmmo = fHealingAmmoFalk;
      }

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }

   // we failed to drop this weapon on death, award us some cash instead
   else if (Instigator.Health <= 0 && Instigator.PlayerReplicationInfo != none)
   {
      Instigator.PlayerReplicationInfo.Score += SellValue;
      
      if (FHumanPawn(Instigator) != none)
         FHumanPawn(Instigator).FalkSetDosh();
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
   local int Mode;

   Instigator = Pawn(Owner);

   bPendingSwitch = bPossiblySwitch;

   if( Instigator == None )
   {
      GotoState('PendingClientWeaponSet');
      return;
   }

   for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
   {
      if( FireModeClass[Mode] != None )
      {
         // laurent -- added check for vehicles (ammo not replicated but unlimited)
         if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
         {
            GotoState('PendingClientWeaponSet');
            return;
         }
      }

      FireMode[Mode].Instigator = Instigator;
      FireMode[Mode].Level = Level;
   }

   ClientState = WS_Hidden;
   GotoState('Hidden');

   if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
      return;

   if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
   {
      if (Instigator.PendingWeapon != None)
         Instigator.ChangedWeapon();
      else
         BringUp();
      return;
   }

   if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
      return;

   if ( Instigator.Weapon == None)
   {
      Instigator.PendingWeapon = self;
      Instigator.ChangedWeapon();
   }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// attempt to see the whole fockin reload animation in server
exec function ReloadMeNow()
{
   if (MagAmmoRemaining <= 0)
      fShouldBreechReload = True;
   
   Super.ReloadMeNow();
}

// attempt to see the whole fockin reload animation
simulated function ClientReload()
{
   if (MagAmmoRemaining <= 0)
      fShouldBreechReload = True;

   ReloadRate = Default.ReloadRate;

   Super.ClientReload();
}

// second attempt to see the whole fockin reload animation
simulated function AddReloadedAmmo()
{
   if (AmmoAmount(0) > 0)
      ++MagAmmoRemaining;

   ReloadRate -= fReloadSpeedFix;

   // last bullet to load, don't interrupt or drop
   if (fShouldBreechReload && AmmoAmount(0) > MagCapacity - 1 && MagAmmoRemaining == MagCapacity - 1)
   {
      ReloadRate         = fLastBulletRate;
      bCanThrow          = False;
      fLoadingLastBullet = True;

   }

   // restore vanilla stuff
   else if (MagAmmoRemaining == MagCapacity)
   {
      bCanThrow           = True;
      fShouldBreechReload = False;
      fLoadingLastBullet  = False;
   }
}

// third attempt to see the whole fockin reload
simulated function bool InterruptReload()
{
   if (fLoadingLastBullet)
      return False;

   if (Super.InterruptReload())
   {
      fShouldBreechReload = False;
      bCanThrow           = True;

      return True;
   }

   return False;
}

// attempt to fix more and more weird shit happening with the reload
simulated function ClientInterruptReload()
{
   if (fLoadingLastBullet)
      return;

   fShouldBreechReload = False;
   bCanThrow           = True;
   Super.ClientInterruptReload();
}

// more attempt to fix that reload on server
simulated function ServerInterruptReload()
{
   if (fLoadingLastBullet)
      return;

   fShouldBreechReload = False;
   bCanThrow           = True;
   Super.ServerInterruptReload();
}

// restore ammo from the pawn if they're fresh enough
function PickupFunction(Pawn Other)
{
   Super(MP7MMedicGun).PickupFunction(Other);

   if (fStoredAmmo == -1 && FHumanPawn(Other) != none &&
       FHumanPawn(Other).fStoredSyringeAmmo > -1      &&
       FHumanPawn(Other).fStoredSyringeTime + fSyringeFixTime >= Level.TimeSeconds)
   {
      fStoredAmmo = FHumanPawn(Other).fStoredSyringeAmmo;
      //warn("Restoring ammo from the pawn: "@fStoredAmmo);
      FHumanPawn(Other).fStoredSyringeAmmo = -1;
   }
}

// permit throwing while reloading, blocking it during the last bullet
simulated function bool CanThrow()
{
   if (bIsReloading && !fLoadingLastBullet)
   {
      InterruptReload();
      return True;
   }

   return Super.CanThrow();
}

defaultproperties
{
   HealBoostAmount=20
   AmmoRegenRate=0.200000 // exception for SSS-M only, changed from 0.3 to 0.2 in patch 9
   MagCapacity=4
   bHoldToReload=True
   WeaponReloadAnim="Reload_Shotgun"
   HudImage=Texture'LairTextures_T.CustomReskins.MedishotUnselected'
   SelectedHudImage=Texture'LairTextures_T.CustomReskins.MedishotSelected'
   HudImageRef=""
   SelectedHudImageRef=""
   Weight=4.000000
   SleeveNum=0
   TraderInfoTexture=Texture'WhiskyMediShot_T.Trader_Medishot'
   FireModeClass(0)=Class'MediShotFireFalk'
   FireModeClass(1)=Class'MediShotAltFireFalk'
   SelectSound=Sound'KF_MP7Snd.MP7_Select'
   Priority=80
   GroupOffset=4
   PickupClass=Class'LocalMediShotPickupFalk'
   AttachmentClass=Class'MediShotAttachmentFalk'
   Description="The Serbu Super-Shorty is a compact and stockless pump-action AOW chambered in 12-gauge. This particular version is modified to fire healing darts. Features a strong damage output and a slightly more efficient penetration than a Benelli M3."
   ItemName="Medical Serbu Super-Shorty Shotgun"
   Mesh=SkeletalMesh'WhiskyMediShot_A.medishot_weapon'
   MeshRef="WhiskyMediShot_A.medishot_weapon"
   Skins(1)=Texture'WhiskyMediShot_T.v1texture'
   SkinRefs(0)="WhiskyMediShot_T.v1texture"
   SkinRefs(1)="WhiskyMediShot_T.v1texture"
   ReloadRate=0.67
   fLastBulletRate=1.24
   fReloadSpeedFix=0.02
}
