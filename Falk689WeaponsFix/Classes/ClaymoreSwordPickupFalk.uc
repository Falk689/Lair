class ClaymoreSwordPickupFalk extends KFMod.ClaymoreSwordPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

DefaultProperties
{
    Weight=6.000000
    cost=1850
    PowerValue=63
    SpeedValue=40
    RangeValue=45
    ItemName="Claymore Sword"
    ItemShortName="Claymore"
    InventoryType=Class'ClaymoreSwordFalk'
    PickupMessage="You got a Claymore Sword"
    PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
