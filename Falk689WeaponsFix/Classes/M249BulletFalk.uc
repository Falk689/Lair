class M249BulletFalk extends SW76BulletFalk;

defaultproperties
{
   FDamage=54.000000 // Needed 'cause default.Damage is ignored by ProcessTouch
   Damage=54.000000
   MyDamageType=Class'DamTypeM249Falk'
   fShouldPenetrate=True
   MaxPenetrations=2
   PenDamageReduction=0.500000
}