class AAR525PickupFalk extends AAR525PickupBaseFalk;

#exec OBJ LOAD FILE=LairStaticMeshes_SM.usx
#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   StaticMesh=StaticMesh'LairStaticMeshes_SM.CustomReskins.AlienRifleCommandoPickup'
   cost=4000
   InventoryType=Class'AAR525Falk'
   Weight=7.000000
   ItemName="Alien Assault Rifle 525"
   PickupMessage="You got an Alien Assault Rifle 525"
   PowerValue=69
   SpeedValue=35
   RangeValue=100
   BuyClipSize=25
   AmmoCost=13
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}