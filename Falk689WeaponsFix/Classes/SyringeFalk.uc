class SyringeFalk extends KFMod.Syringe;

#exec OBJ LOAD FILE=LairSounds_S.uax

Const MaxAmmoCount=5000;
const FMaxAmmoCount=5000;

var bool  fShouldReload;          // used to instant reload syringe?
var int   fStoredAmmo;            // stored ammo from the pickup
var bool  fPawnAmmoRestore;       // we restored ammo from the pawn
var bool  fPickupAmmoRestore;     // we restored ammo from the pickup
var float fSyringeFixTime;        // how much time to fix syringe ammo from the pawn

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

replication
{
    reliable if (Role == ROLE_Authority)
        fStoredAmmo, FClientHackAltFire;
}

simulated function PostBeginPlay()
{
   // Weapon will handle FireMode instantiation
   Super(KFMeleeGun).PostBeginPlay();

   HealBoostAmount = GetHealBoostAmount();
}

// Start syringe ammo istant reload
simulated function StartForceReload()
{
   fShouldReload = True;
}

// praise snek on heal
function PraiseSnek()
{
   if (Instigator != none && Instigator.Controller != none && PlayerController(Instigator.Controller) != none)
      PlayerController(Instigator.Controller).ClientPlaySound(sound'LairSounds_S.Memes.Graziesneic',False,,SLOT_Talk);
}

// Istantly reload syringe ammo, not sure this makes sense
simulated function bool ForceHealingReload()
{
   AmmoCharge[0] = MaxAmmoCount;

   return (AmmoCharge[0] < MaxAmmoCount);
}

// Added instantreload trigger
simulated function Tick(float dt)
{
   if (Level.NetMode != NM_Client)
   {
      if (!fPawnAmmoRestore)
      {
         if (fStoredAmmo == -1 && FHumanPawn(Instigator) != none)
         {
            if (FHumanPawn(Instigator).fStoredSyringeAmmo > -1 && FHumanPawn(Instigator).fStoredSyringeTime + fSyringeFixTime >= Level.TimeSeconds)
            {
               fStoredAmmo = FHumanPawn(Instigator).fStoredSyringeAmmo;
               //warn("Tick recovering from pawn: "@fStoredAmmo);
               fPawnAmmoRestore = True;
            }

            else
            {
               //warn("F2");
               fPawnAmmoRestore = True;

               if (!fPickupAmmoRestore)
               {
                  fPickupAmmoRestore = True;
                  AmmoCharge[0]      = 0;
               }
            }
         }

         else
         {
            //warn("F1");
            fPawnAmmoRestore = True;
         }
      }

      if (fStoredAmmo > -1)
      {
         AmmoCharge[0] = fStoredAmmo;
         fStoredAmmo   = -1;
      }

      if (fShouldReload)
      {
         AmmoCharge[0] = MaxAmmoCount;
         fShouldReload = False;
         //fShouldReload = ForceHealingReload();
      }

      if (AmmoCharge[0] < MaxAmmoCount && RegenTimer < Level.TimeSeconds)
      {
         RegenTimer = Level.TimeSeconds + AmmoRegenRate;

         if (KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none)
            AmmoCharge[0] += 100 * KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.Static.GetSyringeChargeRate(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo));

         else
            AmmoCharge[0] += 100;

         if (AmmoCharge[0] > MaxAmmoCount)
            AmmoCharge[0] = MaxAmmoCount;
      }
   }
}

// Add some heal boost if there's only one player in game
function int GetHealBoostAmount()
{
   return default.HealBoostAmount;
}

// testing stuff, attempt to fix syringe healing us even on MaxHealth if < max
simulated function Timer()
{
   local FHumanPawn FP;

   Super(KFMeleeGun).Timer();

   FP = FHumanPawn(Instigator);

   if (FP != none)
   {
      if (ClientState == WS_ReadyToFire)
      {
         if (FP.bIsQuickHealing > 0)
         {
            if (FP.bIsQuickHealing == 1)
            {
               if (!HackClientStartFire())
               {
                  if (FP.fHealth >= FP.fHealthMax || ChargeBar() < 0.75)
                     FP.bIsQuickHealing = 2; // Was healed by someone else or some other error occurred.

                  SetTimer(0.1, False);
                  return;
               }

               FP.bIsQuickHealing = 2;
               SetTimer(FireMode[1].FireRate + 0.5, False);
            }

            else
            {
               FP.fWeapSwapTick = 0;
               Instigator.SwitchToLastWeapon();
               FP.bIsQuickHealing = 0;
            }
         }

         else if (FP.fWeapSwitch != 69)
         {
            FP.fCanSwitchSyringe  = True;
            FP.fWeapSwapTick      = 0;
            FP.SwitchToLastWeapon();
         }
      }
   }

   else if (ClientState == WS_Hidden && KFPawn(Instigator) != None)
      KFPawn(Instigator).bIsQuickHealing = 0;
}

// restore ammo from the pawn if they're fresh enough
function PickupFunction(Pawn Other)
{
   Super.PickupFunction(Other);

   if (fStoredAmmo == -1 && FHumanPawn(Other) != none &&
       FHumanPawn(Other).fStoredSyringeAmmo > -1      &&
       FHumanPawn(Other).fStoredSyringeTime + fSyringeFixTime >= Level.TimeSeconds)
   {
      fStoredAmmo = FHumanPawn(Other).fStoredSyringeAmmo;
      //warn("Restoring ammo from the pawn: "@fStoredAmmo);
      FHumanPawn(Other).fStoredSyringeAmmo = -1;
   }
}

// using my vars to prevent shit happening
simulated function float ChargeBar()
{
   local float result;

   //warn("AmmoCharge:"@AmmoCharge[0]@"- FMaxAmmoCount:"@FMaxAmmoCount@"- fStoredAmmo:"@fStoredAmmo);

	result = FClamp(float(AmmoCharge[0]) / float(FMaxAmmoCount), 0, 1);

   return result;
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
            // laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
        if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// fixed a bug that can softlock the syringe while trying to fire right after a self heal
simulated function bool ReadyToFire(int Mode)
{
    local int alt;

    if (Mode == 0)
        alt = 1;

    else
        alt = 0;

    if (FireMode[alt].NextFireTime > Level.TimeSeconds + FireMode[alt].PreFireTime + 0.05)
    {
        return false;
    }

    return Super.ReadyToFire(Mode);
}


// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
      {
         DetachFromPawn(Instigator);

         if (FHumanPawn(Instigator) != none && AmmoCharge[0] > -1)
         {
            //warn("Storing ammo to the pawn: "@AmmoCharge[0] > -1);
            FHumanPawn(Instigator).fStoredSyringeAmmo = AmmoCharge[0];
            FHumanPawn(Instigator).fStoredSyringeTime = Level.TimeSeconds;
         }
      }

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      // storing ammo into the pickup
      if (SyringePickupFalk(Pickup) != none)
      {
         //warn("Storing ammo to the pickup: "@AmmoCharge[0]);
         SyringePickupFalk(Pickup).fStoredAmmo = AmmoCharge[0];
      }

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }

   // we failed to drop this weapon on death, award us some cash instead
   else if (Instigator.Health <= 0 && Instigator.PlayerReplicationInfo != none)
   {
      Instigator.PlayerReplicationInfo.Score += SellValue;
      
      if (FHumanPawn(Instigator) != none)
         FHumanPawn(Instigator).FalkSetDosh();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// Zed time medic skill
simulated function bool ConsumeAmmo(int Mode, float load, optional bool bAmountNeededIsMax)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

    if (Load > AmmoCharge[0])
        Return False;

   // if we're medic and in zed time we get infinite medic darts
   KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

    if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
    {
        Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

        if (Vet != none && Vet.Static.InfiniteMedicDarts(KFPRI))
            return True;
    }

    AmmoCharge[0] -= Load;

    Return True;
}

// attempt to fix the syringe not healing us randomly
simulated function bool HackClientStartFire()
{
    if (StartFire(1))
    {
        if (Level.NetMode != NM_DedicatedServer)
            ServerStartFire(69);

        return true;
    }

    return false;
}

simulated function FClientHackAltFire()
{
    FireMode[1].ModeDoFire();
}

// attempt to fix the syringe not healing us randomly part 2
event ServerStartFire(byte Mode)
{
    if (Mode != 69 && Instigator != None && Instigator.Weapon != self)
    {
        if (Instigator.Weapon == None)
            Instigator.ServerChangedWeapon(None,self);

        else
            Instigator.Weapon.SynchronizeWeapon(self);

        return;
    }

    if (Mode == 69)
        Mode = 1;

    if (FireMode[Mode].NextFireTime <= Level.TimeSeconds + FireMode[Mode].PreFireTime && StartFire(Mode))
    {
        FireMode[Mode].ServerStartFireTime   = Level.TimeSeconds;
        FireMode[Mode].bServerDelayStartFire = false;

        if (Mode == 1)
            FClientHackAltFire();
    }

    else if (FireMode[Mode].AllowFire())
    {
        FireMode[Mode].bServerDelayStartFire = true;

        if (Mode == 1)
            FClientHackAltFire();
    }

    else
    {
        ClientForceAmmoUpdate(Mode, AmmoAmount(Mode));
    }
}

// just don't
simulated function DoAutoSwitch(){}

defaultproperties
{
   fStoredAmmo=-1
   Weight=0.000000
   fSyringeFixTime=0.5
   bKFNeverThrow=False
   PickupClass=Class'SyringePickupFalk'
   ItemName="Medical Syringe"
   FireModeClass(0)=Class'SyringeFireFalk'
   FireModeClass(1)=Class'SyringeAltFireFalk'
   Description="Shoot stuff in your veins to feel better. That's how it's always worked."
   TraderInfoTexture=Texture'LairTextures_T.equipment.Syringe'
   Priority=20
   fMaxTry=3
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
