class LAWFireFalk extends KFMod.LawFire;

var   byte  BID;
var   float FireLastRate;
var() Name  FireLastAnim;
var   bool  bVeryLastShotAnim;

// set pending reload and do stuff
event ModeDoFire()
{
   if (!AllowFire())
      return;

   bVeryLastShotAnim = Weapon.AmmoAmount(0) <= AmmoPerFire;

   // attempt to set proper fire rate to the last shot
   if (bVeryLastShotAnim)
      FireRate = FireLastRate;

   else
      FireRate = Default.FireRate;

   super.ModeDoFire();
}

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID += 7;

   if (BID > 200)
      BID = BID - 200;

   class<LAWProjFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local LAWProjFalk FG;
   local KFPlayerReplicationInfo KFPRI;

   FG = LAWProjFalk(P);

   if (FG != none && Instigator != none)
      FG.fCInstigator = Instigator.Controller;

   KFPRI       = KFPlayerReplicationInfo(Weapon.Instigator.PlayerReplicationInfo);

   if (KFPRI.ClientVeteranSkill != none)
   {
      if (FG != none)
      {
         FG.FDamage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Weapon.Instigator), FG.FDamage, P.MyDamageType);
         P.Damage   = FG.FDamage;
         //warn("FG DAMAGE:"@FG.FDamage);
      }

      else
      {
         P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Weapon.Instigator), P.Damage, P.MyDamageType);
         //warn("P DAMAGE:"@P.Damage);
      }
   }

   else
   {
      warn("No KFPRI.ClientVeteranSkill found while setting the damage for our projectile.");

      if (FG != none)
      {
         FG.fFailedToSetDamage = True;
         //warn("Setting up fallback");
      }
   }

   Super.PostSpawnProjectile(P);
}

// don't interrupt reload
simulated function bool AllowFire()
{
   if (Instigator != none && Instigator.IsHumanControlled())
   {
      if (LAWFalk(Weapon) != None && !LAWFalk(Weapon).fAiming)
      {
         //warn(1);
         return false;
      }

      if (!KFWeapon(Weapon).bAimingRifle || KFWeapon(Weapon).bZoomingIn)
      {
         //warn(2);
         return false;
      }
   }

   if (KFWeapon(Weapon).bIsReloading)
   {
      //warn(3);
      return false;
   }

   if (KFPawn(Instigator).SecondaryItem!=none)
   {
      //warn(4);
      return false;
   }

   if (KFPawn(Instigator).bThrowingNade)
   {
      //warn(5);
      return false;
   }

   if (Level.TimeSeconds - LastClickTime>FireRate)
      LastClickTime = Level.TimeSeconds;

   if (KFWeaponShotgun(Weapon).MagAmmoRemaining < 1)
   {
      //warn(6);
      return false;
   }

   return super(WeaponFire).AllowFire();
}

// Overridden to support special anim functionality of the double barreled shotgun
function PlayFiring()
{
   local float RandPitch;

   if (Weapon.Mesh != None)
   {
      if (FireCount > 0)
      {
         if (KFWeap.bAimingRifle)
         {
            if (Weapon.HasAnim(FireLoopAimedAnim))
            {
               Weapon.PlayAnim(FireLoopAimedAnim, FireLoopAnimRate, 0.0);
            }
            else if( Weapon.HasAnim(FireAimedAnim) )
            {
               Weapon.PlayAnim(FireAimedAnim, FireAnimRate, TweenTime);
            }
            else
            {
               Weapon.PlayAnim(FireAnim, FireAnimRate, TweenTime);
            }
         }
         else
         {
            if (Weapon.HasAnim(FireLoopAnim))
            {
               Weapon.PlayAnim(FireLoopAnim, FireLoopAnimRate, 0.0);
            }
            else
            {
               Weapon.PlayAnim(FireAnim, FireAnimRate, TweenTime);
            }
         }
      }

      else
      {
         if (KFWeap.bAimingRifle)
         {
            if (bVeryLastShotAnim && Weapon.HasAnim(FireLastAnim))
               Weapon.PlayAnim(FireLastAnim, FireAnimRate, TweenTime);

            else if(Weapon.HasAnim(FireAimedAnim))
               Weapon.PlayAnim(FireAimedAnim, FireAnimRate, TweenTime);

            else
               Weapon.PlayAnim(FireAnim, FireAnimRate, TweenTime);
         }
         else
         {
            if (bVeryLastShotAnim && Weapon.HasAnim(FireLastAnim))
               Weapon.PlayAnim(FireLastAnim, FireAnimRate, TweenTime);

            else
               Weapon.PlayAnim(FireAnim, FireAnimRate, TweenTime);
         }
      }
   }

   if (Weapon.Instigator != none && Weapon.Instigator.IsLocallyControlled() &&
         Weapon.Instigator.IsFirstPerson() && StereoFireSound != none)
   {
      if (bRandomPitchFireSound)
      {
         RandPitch = FRand() * RandomPitchAdjustAmt;

         if( FRand() < 0.5 )
         {
            RandPitch *= -1.0;
         }
      }

      Weapon.PlayOwnedSound(StereoFireSound,SLOT_Interact,TransientSoundVolume * 0.85,,TransientSoundRadius,(1.0 + RandPitch),false);
   }

   else
   {
      if (bRandomPitchFireSound)
      {
         RandPitch = FRand() * RandomPitchAdjustAmt;

         if( FRand() < 0.5 )
         {
            RandPitch *= -1.0;
         }
      }

      Weapon.PlayOwnedSound(FireSound,SLOT_Interact,TransientSoundVolume,,TransientSoundRadius,(1.0 + RandPitch),false);
   }

   ClientPlayForceFeedback(FireForce);  // jdf

   FireCount++;
}

defaultproperties
{
   FireLastAnim="Fire"
   FireAnim="FireShort"
   AmmoClass=Class'LAWAmmoFalk'
   ProjectileClass=Class'LAWProjFalk'
   FireRate=0.35
   FireLastRate=0.88
}
