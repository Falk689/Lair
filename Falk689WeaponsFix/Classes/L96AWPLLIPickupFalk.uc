class L96AWPLLIPickupFalk extends L96AWPLLIPickupBaseFalk;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   cost=3700
   BuyClipSize=5
   AmmoCost=15
   InventoryType=Class'L96AWPLLIFalk'
   ItemName="L96 Arctic Warfare Rifle"
   ItemShortName="L96 AWP"
   PickupMessage="You got an L96 Arctic Warfare Rifle"
   PowerValue=87
   SpeedValue=1
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
