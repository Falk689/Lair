class BlowerThrowerAltFireFalk extends KFMod.BlowerThrowerAltFire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<BlowerThrowerAltProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local BlowerThrowerProjectileFalk FB;

   FB = BlowerThrowerProjectileFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

// Autoreload and override to WeaponFire
simulated function bool AllowFire()
{
    if (KFWeapon(Weapon).bIsReloading || KFPawn(Instigator).SecondaryItem != none || KFPawn(Instigator).bThrowingNade)
        return false;

    if (KFWeapon(Weapon).MagAmmoRemaining < AmmoPerFire)
    {
        if (Level.TimeSeconds - LastClickTime > FireRate)
            LastClickTime = Level.TimeSeconds;

        if (AIController(Instigator.Controller) != None)
            KFWeapon(Weapon).ReloadMeNow();

        return false;
    }

    if (KFWeapon(Weapon).bIsReloading)
        return false;

    if (KFPawn(Instigator).SecondaryItem != none)
        return false;

    if (KFPawn(Instigator).bThrowingNade)
        return false;

    return super(WeaponFire).AllowFire();
}


function DoFireEffect()
{
   local Vector StartProj, StartTrace, X,Y,Z;
   local Rotator Aim;
   local Vector HitLocation, HitNormal;
   local Actor Other;
   local int p;
   local int SpawnCount;
   local float theta;

   Instigator.MakeNoise(1.0);
   Weapon.GetViewAxes(X,Y,Z);

   StartTrace = Instigator.Location + Instigator.EyePosition();// + X*Instigator.CollisionRadius;
   StartProj = StartTrace + X*ProjSpawnOffset.X;
   if ( !Weapon.WeaponCentered() && !KFWeap.bAimingRifle )
      StartProj = StartProj + Weapon.Hand * Y*ProjSpawnOffset.Y + Z*ProjSpawnOffset.Z;

   // check if projectile would spawn through a wall and adjust start location accordingly
   Other = Weapon.Trace(HitLocation, HitNormal, StartProj, StartTrace, false);

   if (Other != None)
      StartProj = HitLocation;

   Aim = AdjustAim(StartProj, AimError);

   SpawnCount = Max(1, ProjPerFire);

   for (p = 0; p < SpawnCount; p++)
   {
      theta = Spread*PI/32768*(p - float(SpawnCount-1)/2.0);
      X.X = Cos(theta);
      X.Y = Sin(theta);
      X.Z = 0.0;
      SpawnProjectile(StartProj, Rotator(X >> Aim));
   }

   if (Instigator != none)
   {
      if(Instigator.Physics != PHYS_Falling)
         Instigator.AddVelocity(KickMomentum >> Instigator.GetViewRotation());

      // Really boost the momentum for low grav
      else if(Instigator.Physics == PHYS_Falling
            && Instigator.PhysicsVolume.Gravity.Z > class'PhysicsVolume'.default.Gravity.Z)
         Instigator.AddVelocity((KickMomentum * LowGravKickMomentumScale) >> Instigator.GetViewRotation());
   }
}


defaultproperties
{
   AmmoClass=Class'BlowerThrowerAmmoFalk'
   ProjectileClass=Class'BlowerThrowerAltProjectileFalk'
   ProjPerFire=5
   AmmoPerFire=5
   FireRate=0.55       // was 0.965, changed to 0.55 with patch 5
   FireAnimRate=1.66   // was 0.95, changed to 1.66 with patch 5
   aimerror=1.000000
   Spread=1500.0
   SpreadStyle=SS_Line
   maxVerticalRecoilAngle=1500
   maxHorizontalRecoilAngle=900
   FireAnim=FireALL
   FireAimedAnim=FireALL_Iron
   ShakeOffsetMag=(X=6.0,Y=2.0,Z=10.0)
   ShakeOffsetRate=(X=1000.0,Y=1000.0,Z=1000.0)
   ShakeOffsetTime=3.0
   ShakeRotMag=(X=50.0,Y=50.0,Z=400.0)
   ShakeRotRate=(X=12500.0,Y=12500.0,Z=12500.0)
   ShakeRotTime=5.0
   bWaitForRelease=true
   bRandomPitchFireSound=false
   FireSoundRef="KF_FY_BlowerThrowerSND.WEP_Blower_Secondary_Fire_M"
   StereoFireSoundRef="KF_FY_BlowerThrowerSND.WEP_Blower_Secondary_Fire_S"
   NoAmmoSound=Sound'KF_PumpSGSnd.SG_DryFire'
   bAttachSmokeEmitter=True
   TransientSoundVolume=2.0
   TransientSoundRadius=500.000000
}
