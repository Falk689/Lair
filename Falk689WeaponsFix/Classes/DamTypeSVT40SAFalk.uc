class DamTypeSVT40SAFalk extends SVDLLImut.DamTypeSVDLLI;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FSharpshooterDamage', Amount);
}

defaultproperties
{
    HeadShotDamageMult=2.00000 // semi-auto snipers standard
	WeaponClass=Class'SVT40SAFalk'
}