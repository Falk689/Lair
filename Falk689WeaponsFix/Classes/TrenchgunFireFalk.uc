class TrenchgunFireFalk extends KFMod.TrenchgunFire;

var byte BID;

// attempt to see the whole fockin reload anim, step dunno
simulated function bool AllowFire()
{
   if ((TrenchgunFalk(Weapon)      != None && TrenchgunFalk(Weapon).fLoadingLastBullet) ||
       (LocalTrenchgunFalk(Weapon) != None && LocalTrenchgunFalk(Weapon).fLoadingLastBullet))
   {
      return False;
   }

	return Super.AllowFire();
}

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<TrenchgunBulletFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local TrenchgunBulletFalk FB;

   FB = TrenchgunBulletFalk(P);

   Super.PostSpawnProjectile(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;
}

defaultproperties
{
   AmmoClass=Class'TrenchgunAmmoFalk'
   ProjectileClass=Class'TrenchgunBulletFalk'
   FireRate=0.965000
   ProjPerFire=14
}
