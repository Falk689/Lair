class HK23EAmmoPickupFalk extends KFAmmoPickup;

defaultproperties
{
   AmmoAmount=23
   InventoryType=Class'HK23EAmmoFalk'
   PickupMessage="You found some 5.56x45mm NATO rounds"
   StaticMesh=StaticMesh'KillingFloorStatics.L85Ammo'
}