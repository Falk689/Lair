class DualiesPickupFalk extends NineMMPlusDualPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}


defaultproperties
{
   cost=150
   InventoryType=Class'DualiesFalk'
   Weight=2.000000
   ItemName="Dual Beretta 92FS M9A1s"
   ItemShortName="Dual M9A1s"
   PickupMessage="You found another Beretta 92FS M9A1"
   BuyClipSize=30
   AmmoCost=15
   PowerValue=9
   SpeedValue=60
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
