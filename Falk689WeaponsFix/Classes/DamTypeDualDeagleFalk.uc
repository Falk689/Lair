class DamTypeDualDeagleFalk extends KFMod.DamTypeDualDeagle;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FSharpshooterDamage', Amount);
}

defaultproperties
{
   WeaponClass=Class'DualDeagleFalk'
   DeathString="%k killed %o (Dual Deagles)"
   HeadShotDamageMult=1.100000
}