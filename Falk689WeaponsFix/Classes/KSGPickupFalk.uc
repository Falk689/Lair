class KSGPickupFalk extends KFMod.KSGPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
	InventoryType=Class'KSGShotgunFalk'
	ItemName="Kel-Tec KSG"
	ItemShortName="KSG"
	PickupMessage="You got a Kel-Tec KSG"
	cost=2000
	BuyClipSize=12
	AmmoCost=24
	PowerValue=48
    SpeedValue=35
    RangeValue=100
	PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}