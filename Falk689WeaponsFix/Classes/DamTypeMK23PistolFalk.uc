class DamTypeMK23PistolFalk extends KFMod.DamTypeMK23Pistol;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FSharpshooterDamage', Amount);
}

defaultproperties
{
   WeaponClass=Class'MK23PistolFalk'
   DeathString="%k killed %o with an MK23"
   HeadShotDamageMult=1.100000
}
