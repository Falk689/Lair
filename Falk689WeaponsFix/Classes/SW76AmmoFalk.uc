class SW76AmmoFalk extends SW76Wep.SW76Ammo;

defaultproperties
{
   AmmoPickupAmount=32
   MaxAmmo=256
   InitialAmount=96
   PickupClass=Class'SW76AmmoPickupFalk'
   ItemName="9x19mm Parabellum rounds"
}