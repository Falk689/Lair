class PDWPickupFalk extends PDW.PDWPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   cost=1600
   Weight=4.000000
   InventoryType=Class'PDWAssaultRifleFalk'
   BuyClipSize=30
   AmmoCost=15
   ItemName="KAC Personal Defense Weapon"
   ItemShortName="PDW"
   PickupMessage="You got a KAC Personal Defense Weapon"
   PowerValue=42
   SpeedValue=32
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}