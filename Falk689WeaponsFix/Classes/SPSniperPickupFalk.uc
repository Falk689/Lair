class SPSniperPickupFalk extends KFMod.SPSniperPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   InventoryType=Class'SPSniperRifleFalk'
   ItemName="Steampunk Lee Enfield SMLE"
   ItemShortName="SP SMLE"
   PickupMessage="You got a Steampunk Lee Enfield SMLE"
   cost=2000
   BuyClipSize=10
   AmmoCost=20
   PowerValue=46
   SpeedValue=9
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
