class M60FireFalk extends AA12FireFalk;

defaultproperties
{
   AmmoClass=Class'M60AmmoFalk'
   ProjectileClass=Class'M60BulletFalk'
   ProjPerFire=1
   AmmoPerFire=1
   aimerror=42.000000
   FireRate=0.100000
   RecoilRate=0.1
   maxVerticalRecoilAngle=400
   maxHorizontalRecoilAngle=50
   TweenTime=0.025000
   FireAimedAnim="Fire"
   ShellEjectClass=None
   ShellEjectBoneName="Shell_eject"
   StereoFireSound=Sound'RPK_GAL.RPK47_snd.RPK47_fire'
   bRandomPitchFireSound=False
   bPawnRapidFireAnim=True
   TransientSoundVolume=1.800000
   FireLoopAnim="Fire"
   FireSound=Sound'RPK_GAL.RPK47_snd.RPK47_fire'
   NoAmmoSound=Sound'KF_AK47Snd.AK47_DryFire'
   ShakeRotMag=(X=50.000000,Y=50.000000,Z=350.000000)
   ShakeRotRate=(X=5000.000000,Y=5000.000000,Z=5000.000000)
   ShakeRotTime=0.750000
   ShakeOffsetMag=(X=6.000000,Y=3.000000,Z=7.500000)
   ShakeOffsetRate=(X=1000.000000,Y=1000.000000,Z=1000.000000)
   ShakeOffsetTime=1.250000
   BotRefireRate=0.990000
   FlashEmitterClass=Class'ROEffects.MuzzleFlash1stSTG'
   Spread=0.015000
   SpreadStyle=SS_Random
}