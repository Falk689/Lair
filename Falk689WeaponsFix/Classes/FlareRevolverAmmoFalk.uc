class FlareRevolverAmmoFalk extends KFMod.FlareRevolverAmmo;

defaultproperties
{
   AmmoPickupAmount=12
   MaxAmmo=90
   InitialAmount=42
   PickupClass=Class'FlareRevolverAmmoPickupFalk'
}