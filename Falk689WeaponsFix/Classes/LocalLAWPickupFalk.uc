class LocalLAWPickupFalk extends KFMod.LAWPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   InventoryType=Class'LocalLAWFalk'
   ItemName="Light Antiarmor Weapon 80"
   ItemShortName="LAW80"
   PickupMessage="You got a Light Antiarmor Weapon 80"
   cost=3000
   BuyClipSize=1
   AmmoCost=20
   Weight=13.00000
   PowerValue=95
   SpeedValue=11
   RangeValue=100
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
