class DamTypeAK12LLIFalk extends AK12LLIMut.DamTypeAK12LLI;

defaultproperties
{
   HeadShotDamageMult=1.100000
   DeathString="%k killed %o (Silenced AK12)"
   WeaponClass=Class'AK12LLIAssaultRifleFalk'
}