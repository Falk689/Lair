class ATMineExplosiveFalk extends ATMineExplosiveBaseFalk;

#exec OBJ LOAD FILE=LairAnimations_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

var float fWeight;                // static weight of this weapon
var float fAmmoPickupTime;        // reset to default on ammo pickup to fix a bug with the interface

var() Name EmptyIdleAnim;
var() Name EmptySelectAnim;
var() Name EmptyPutDownAnim;

var float EmptyBringUpTime;
var float EmptySelectAnimRate;

replication
{
    reliable if(Role == ROLE_Authority)
        fAmmoPickupTime, FLastAmmoUsedClient, FNewAmmoAddedClient;

    reliable if (Role != ROLE_Authority)
        FSetVanillaAnims;
}

// don't load hud icons
static function PreloadAssets(Inventory Inv, optional bool bSkipRefCount)
{
    local int i;

    if ( !bSkipRefCount )
    {
        default.ReferenceCount++;
    }

    UpdateDefaultMesh(SkeletalMesh(DynamicLoadObject(default.MeshRef, class'SkeletalMesh')));

    for ( i = 0; i < default.SkinRefs.Length; i++ )
        default.Skins[i] = Material(DynamicLoadObject(default.SkinRefs[i], class'Material'));

    if ( KFWeapon(Inv) != none )
    {
        Inv.LinkMesh(default.Mesh);
        KFWeapon(Inv).SelectSound = default.SelectSound;

        for (i = 0; i < default.SkinRefs.Length; i++)
            Inv.Skins[i] = default.Skins[i];
    }
}


// don't randomly unload anything
static function bool UnloadAssets()
{
    default.ReferenceCount--;
    return default.ReferenceCount == 0;
}


// fixed weird shit happening with the last ammo
function ServerStopFire(byte Mode)
{
    super(KFWeapon).ServerStopFire(Mode);
    MagAmmoRemaining = 0;
}


// server method called when the last ammo is used
function FLastAmmoUsed()
{
    //warn("LAST AMMO USED ON SERVER");
    fBackupSellValue   = SellValue;
    SellValue          = 0;
    Weight             = 0;

    SelectAnim         = EmptySelectAnim;
    SelectAnimRate     = EmptySelectAnimRate;
    IdleAnim           = EmptyIdleAnim;
    PutDownAnim        = EmptyPutDownAnim;
    BringUpTime        = EmptyBringUpTime;
    fEmptyAnim         = true;

    if (Role == ROLE_Authority && Level.NetMode != NM_ListenServer)
        FLastAmmoUsedClient();
}

// client method called when the last ammo is used
simulated function FLastAmmoUsedClient()
{
    //warn("LAST AMMO USED ON CLIENT");

    if (SellValue > 0)
    {
        fBackupSellValue   = SellValue;
        SellValue          = 0;
    }

    Weight             = 0;

    SelectAnim         = EmptySelectAnim;
    SelectAnimRate     = EmptySelectAnimRate;
    IdleAnim           = EmptyIdleAnim;
    PutDownAnim        = EmptyPutDownAnim;
    BringUpTime        = EmptyBringUpTime;
    fEmptyAnim         = true;
}

// server method called when at least one ammo is added after the last was used
function FNewAmmoAdded()
{
    if (Level.NetMode == NM_ListenServer)
    {
        FNewAmmoAddedClient();
        return;
    }

    //warn("NEW AMMO ON SERVER");
    if (fBackupSellValue > 0)
        SellValue    = fBackupSellValue;

    if (fWeight > 0)
        Weight       = fWeight;

    fBackupSellValue = 0;
    SelectAnim       = default.SelectAnim;
    SelectAnimRate   = default.SelectAnimRate;
    IdleAnim         = default.IdleAnim;
    PutDownAnim      = default.PutDownAnim;
    BringUpTime      = default.BringUpTime;
    fEmptyAnim       = false;

    if (Role == ROLE_Authority)
        FNewAmmoAddedClient();
}

// client method called when at least one ammo is added after the last was used
simulated function FNewAmmoAddedClient()
{
    //warn("NEW AMMO ON CLIENT");
    if (fBackupSellValue > 0)
        SellValue    = fBackupSellValue;

    if (fWeight > 0)
        Weight       = fWeight;

    fBackupSellValue = 0;
    SelectAnim       = default.SelectAnim;
    SelectAnimRate   = default.SelectAnimRate;
    IdleAnim         = default.IdleAnim;
    PutDownAnim      = default.PutDownAnim;
    BringUpTime      = default.BringUpTime;

    if (fEmptyAnim && ClientState == WS_ReadyToFire)
        PlayAnim(default.SelectAnim, default.SelectAnimRate, 0.1);

    else if (fEmptyAnim)
        fDoBringUpAnim = true;

    fEmptyAnim       = false;
}


// set vanilla anims from the client to the server
function FSetVanillaAnims()
{
    //warn("SET VANILLA ANIMS FROM CLIENT");
    SelectAnim       = default.SelectAnim;
    SelectAnimRate   = default.SelectAnimRate;
    IdleAnim         = default.IdleAnim;
    PutDownAnim      = default.PutDownAnim;
    BringUpTime      = default.BringUpTime;
    fEmptyAnim       = false;
}


// set ammo access to our instance
function GiveAmmo(int m, WeaponPickup WP, bool bJustSpawned)
{
    Super.GiveAmmo(m, WP, bJustSpawned);

    if (ATMineAmmoFalk(Ammo[m]) != none)
        ATMineAmmoFalk(Ammo[m]).Weapon = self;
}

// called regardless of this being the current weapon
simulated function Tick(float dt)
{
    if (fAmmoPickupTime > 0)
        fAmmoPickupTime -= dt;

    Super.Tick(dt);
}

// queue the vanilla bring up animation if needed
simulated function Timer()
{
    local name anim;
    local float frame, rate;

    GetAnimParams(0, anim, frame, rate);

    if (ClientState != WS_PutDown && fDoBringUpAnim && anim == EmptySelectAnim)
    {
        //warn("QUEUED THINGY");
        fDoBringUpAnim   = false;
        SelectAnim       = default.SelectAnim;
        SelectAnimRate   = default.SelectAnimRate;
        IdleAnim         = default.IdleAnim;
        PutDownAnim      = default.PutDownAnim;
        BringUpTime      = default.BringUpTime;
        fEmptyAnim       = false;
        FSetVanillaAnims();
        PlayAnim(default.SelectAnim, default.SelectAnimRate, 0.1);
        SetTimer(default.BringUpTime, false);
        return;
    }

    Super.Timer();
}

// switch anims on tick --- I fucking hate everything about this aaaaa
simulated function WeaponTick(float dt)
{
    if (fEmptyAnim)
    {
        SelectAnim       = EmptySelectAnim;
        SelectAnimRate   = EmptySelectAnimRate;
        IdleAnim         = EmptyIdleAnim;
        PutDownAnim      = EmptyPutDownAnim;
    }

    else
    {
        SelectAnim       = default.SelectAnim;
        SelectAnimRate   = default.SelectAnimRate;
        IdleAnim         = default.IdleAnim;
        PutDownAnim      = default.PutDownAnim;
    }

    Super.WeaponTick(dt);
}


// kind of try to do only reasonable stuff here
simulated function bool ReadyToFire(int Mode)
{  
   if (FireMode[Mode].bIsFiring || !FireMode[Mode].AllowFire() || (FireMode[Mode].NextFireTime + 0.2 > Level.TimeSeconds))
      return False;

   return True;
}


// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// first attempt to fix shit happening with the fire animation
simulated function AnimEnd(int channel)
{
    local name anim;
    local float frame, rate;

    GetAnimParams(0, anim, frame, rate);

    if (ClientState == WS_ReadyToFire)
    {
        if (anim == FireMode[0].FireAnim && ammoAmount(0) > 0 )
        {
            PlayAnim(SelectAnim, SelectAnimRate, 0.1);
        }
    }
}

// just nope
simulated function ServerInterruptReload()
{
}

// lol nope
simulated function ClientInterruptReload()
{
}

// don't play idle here since it kind of bugs everything
simulated function ClientFinishReloading()
{
    bIsReloading = false;

    if(Instigator.PendingWeapon != none && Instigator.PendingWeapon != self)
        Instigator.Controller.ClientSwitchToBestWeapon();
}

defaultproperties
{
    MagCapacity=1
    Weight=1.000000
    fWeight=1.000000
    FireModeClass[0]=Class'ATMineFireFalk'
    PickupClass=Class'ATMinePickupFalk'
    ItemName="Antitank Mines"
    Description="Land mines designed to damage or destroy vehicles including tanks and armored fighting vehicles. Thought for disposing of the heaviest zeds, or great crowds. Remarkable explosion range."
    PutDownAnimRate=3.0
    PutDownTime=0.15
    InventoryGroup=4
    Priority=35
    fAmmoPickupTime=0.5
    SelectedHudImageRef=""
    SelectedHudImage=Texture'LairTextures_T.CustomReskins.ATMineSelected'
    HudImageRef=""
    HudImage=Texture'LairTextures_T.CustomReskins.ATMineUnselected'
    TraderInfoTexture=Texture'LairTextures_T.CustomReskins.ATMineTrader'
    AttachmentClass=Class'ATMineAttachmentFalk'
    SkinRefs(1)="LairTextures_T.CustomReskins.ATMine"
    Skins(1)=Texture'LairTextures_T.CustomReskins.ATMine'
    Mesh=SkeletalMesh'LairAnimations_A.ATMMesh'
    MeshRef="LairAnimations_A.ATMMesh"
    IdleAnim=Idle
    SelectAnim=Select
    PutDownAnim=PutDown
    EmptyIdleAnim=IdleEmpty
    EmptySelectAnim=SelectEmpty
    EmptyPutDownAnim=IdleEmpty
    BringUpTime=0.43
    EmptyBringUpTime=0.7
    EmptySelectAnimRate=1.0
    SelectSoundRef=""
}
