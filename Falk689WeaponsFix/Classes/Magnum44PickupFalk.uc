class Magnum44PickupFalk extends KFMod.Magnum44Pickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
    if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
    {
        if (KFGameType(Level.Game) != none)
            KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
    }

    super(WeaponPickup).Destroyed();
}

function inventory SpawnCopy( pawn Other )
{  
   local Inventory I;
   local class<Inventory> D;
   
   for ( I = Other.Inventory; I != none; I = I.Inventory )
   { 
      if ( Magnum44Pistol(I) != none )
      {
         if( Inventory != none )
            Inventory.Destroy();

         D = Default.InventoryType;
         Default.InventoryType = Class'Dual44MagnumFalk';

         AmmoAmount[0] += Magnum44Pistol(I).AmmoAmount(0);
         MagAmmoRemaining += Magnum44Pistol(I).MagAmmoRemaining;

         I.Destroyed();
         I.Destroy();

         I = Super.SpawnCopy(Other);
         Default.InventoryType = D;
         return I;
      }
   }
   
   InventoryType = Default.InventoryType;
   Return Super.SpawnCopy(Other);
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
    Weight=2.000000
    cost=450
    BuyClipSize=6
    PowerValue=28
    SpeedValue=34
    RangeValue=100
    AmmoCost=12
    ItemName="Smith & Wesson 29"
    ItemShortName="SW29"
    AmmoItemName=".44 rounds"
    InventoryType=Class'Magnum44PistolFalk'
    PickupMessage="You got a Smith & Wesson 29"
    PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
