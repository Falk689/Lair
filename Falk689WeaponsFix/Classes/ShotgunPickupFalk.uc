class ShotgunPickupFalk extends KFMod.ShotgunPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
	ItemName="Benelli M3 Super 90 Shotgun"
	ItemShortName="Benelli M3"
	InventoryType=Class'ShotgunFalk'
	PickupMessage="You got a Benelli M3 Super 90 Shotgun"
	cost=500
	BuyClipSize=8
	AmmoCost=16
	PowerValue=46
    SpeedValue=30
    RangeValue=100
	PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
