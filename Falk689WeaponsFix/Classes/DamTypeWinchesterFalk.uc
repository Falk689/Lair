class DamTypeWinchesterFalk extends KFMod.DamTypeWinchester;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FSharpshooterDamage', Amount);
}

defaultproperties
{
    HeadShotDamageMult=1.500000     // lever-action rifles standard
    WeaponClass=Class'WinchesterFalk'
    DeathString="%k killed %o (M1894)"
}