class PipeBombAmmoFalk extends PipeBombAmmoBaseFalk;

var KFWeapon Weapon; // weapon instance

// call weapon FNewAmmoAdded to restore anims and value on success
function bool FShouldAddAmmo(int AmmoToAdd, optional float AddWeight)
{
    local bool                  result;

    result = Super.FShouldAddAmmo(AmmoToAdd, AddWeight);

    if (!result)
        return result;

    if (PipeBombExplosiveFalk(Weapon) != none)
        PipeBombExplosiveFalk(Weapon).FNewAmmoAdded();

    return result;
}

defaultproperties
{
    MaxAmmo=2
    InitialAmount=1
    AmmoPickupAmount=1
    IconMaterial=Texture'KillingFloorHUD.Generic.HUD'
    IconCoords=(X1=451,Y1=445,X2=510,Y2=500)
    PickupClass=Class'PipeBombAmmoPickupFalk'
}
