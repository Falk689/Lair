class SPThompsonPickupFalk extends KFMod.SPThompsonPickup;

#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   InventoryType=Class'SPThompsonSMGFalk'
   ItemName="Dr T's Lead Delivery System"
   ItemShortName="LDS"
   cost=1500
   BuyClipSize=50
   AmmoCost=25
   PickupMessage="You got a Dr T's Lead Delivery System"
   PowerValue=40
   SpeedValue=65
   RangeValue=100
   Weight=5.000000
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}