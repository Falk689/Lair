class AA12ExplosivePickupFalk extends AA12ExplosivePickupBaseFalk;

#exec OBJ LOAD FILE=LairStaticMeshes_SM.usx
#exec OBJ LOAD FILE=LairSounds_S.uax

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   cost=4000
   InventoryType=Class'AA12ExplosiveFalk'
   Weight=10.000000
   AmmoCost=20
   BuyClipSize=10
   CorrespondingPerkIndex=6
   EquipmentCategoryID=6
   ItemName="AA12 Automatic Grenade Launcher"
   ItemShortName="Explosive AA12"
   PickupMessage="You got an AA12 Automatic Grenade Launcher"
   StaticMesh=StaticMesh'LairStaticMeshes_SM.CustomReskins.AA12ExplosivePickup'
   PowerValue=50
   SpeedValue=65
   RangeValue=85
   DrawScale=6.2
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
}
