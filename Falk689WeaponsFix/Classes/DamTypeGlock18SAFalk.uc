class DamTypeGlock18SAFalk extends DamTypeBullpupFalk;

defaultproperties
{
   bSniperWeapon=False
   WeaponClass=Class'Glock18SAFalk'
   DeathString="%k killed %o with Glock18."
   FemaleSuicide="%o shot herself in the foot."
   MaleSuicide="%o shot himself in the foot."
   bRagdollBullet=True
   bBulletHit=True
   FlashFog=(X=600.000000)
   KDamageImpulse=1850.000000
   KDeathVel=150.000000
   KDeathUpKick=5.000000
   VehicleDamageScaling=0.800000
   HeadShotDamageMult=1.100000
}
