class BenelliAmmoPickupFalk extends KFMod.BenelliAmmoPickup;

defaultproperties
{
    AmmoAmount=6
    InventoryType=Class'BenelliAmmoFalk'
    PickupMessage="You found some 12-gauge shells"
}