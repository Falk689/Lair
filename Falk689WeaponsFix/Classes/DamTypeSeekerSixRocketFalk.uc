class DamTypeSeekerSixRocketFalk extends KFMod.DamTypeSeekerSixRocket;

defaultproperties
{
   HeadShotDamageMult=1.100000
   WeaponClass=Class'SeekerSixRocketLauncherFalk'
   DeathString="%o filled %k's body with shrapnel"
   bIsExplosive=True
}
