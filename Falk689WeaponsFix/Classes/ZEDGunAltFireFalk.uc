class ZEDGunAltFireFalk extends KFMod.ZEDGunAltFire;

var float	ZapAmount;

// give this gun a proper zap damage
simulated function ModeTick(float dt)
{
   local Vector StartTrace, EndTrace, X, Y, Z;
   local Vector HitLocation, HitNormal, EndEffect;
   local Actor Other;
   local Rotator Aim;
   local ZEDGun ZEDGun;
   local ZEDBeamEffect LB;
   local KFMonster KFM;
   local float ChargeScale;

   if (!bIsFiring)
      return;

   ZEDGun = ZEDGun(Weapon);

   // Handle the beam firing
   if (AllowFire() && ((UpTime > 0.0) || (Instigator.Role < ROLE_Authority)))
   {
      UpTime -= dt;

      if (ChargeUpTime == 0)
      {
         // Play the chargeup sound
         PlayAmbientSound(AmbientChargeUpSound);
         InitChargeEffect();
         SetTimer(0.15, true);
         PlayPreFire();
      }

      ChargeUpTime += dt;

      // the to-hit trace starts at the spawn offset to it looks like it is coming from the barrel
      ZEDGun.GetViewAxes(X, Y, Z);
      StartTrace = GetFireStart( X, Y, Z);

      StartTrace = StartTrace + X*ProjSpawnOffset.X;
      StartTrace = StartTrace + Weapon.Hand * Y*ProjSpawnOffset.Y + Z*ProjSpawnOffset.Z;

      // Find the beam on the client
      if (Instigator.Role < ROLE_Authority)
      {
         if (Beam == None)
         {
            ForEach Weapon.DynamicActors(class'ZEDBeamEffect', LB)
            {
               if (!LB.bDeleteMe && (LB.Instigator != None) && (LB.Instigator == Instigator))
               {
                  Beam = LB;
                  break;
               }
            }
         }
      }

      // Consume the ammo on the server
      if (Instigator.Role == ROLE_Authority)
      {
         if (bDoHit)
            Weapon.ConsumeAmmo(0, AmmoPerFire);
      }

      // client
      if (bDoHit && Instigator.IsLocallyControlled())
         HandleRecoil(1.0);

      // Set the end point of the beam
      Aim      = AdjustAim(StartTrace, AimError);
      X        = Vector(Aim);
      EndTrace = StartTrace + TraceRange * X;
      Other    = Weapon.Trace(HitLocation, HitNormal, EndTrace, StartTrace, true);

      if (Other != None && Other != Instigator)
         EndEffect = HitLocation;

      else
         EndEffect = EndTrace;

      if (Beam != None)
         Beam.EndEffect = EndEffect;

      // Everything else here needs to happen on the server only
      if (Instigator.Role < ROLE_Authority)
         return;

      if (Other != None && Other != Instigator)
      {
         if (KFMonster(Other) != none && KFMonster(Other).Health > 0)
            KFMonster(Other).SetZapped(dt, Instigator);

         else if (ExtendedZCollision(Other)!=None && Other.Base != none && KFMonster(Other.Base) != none && KFMonster(Other.Base).Health > 0)
            KFMonster(Other.Base).SetZapped(dt, Instigator);

         // Make noise every time we "fire"
         if (bDoHit)
            Instigator.MakeNoise(1.0);
      }

      /*if (ChargeUpTime < MaxZedSphereChargeTime)
         ChargeScale = ChargeUpTime / MaxZedSphereChargeTime;

      else*/
         ChargeScale = 1.0;

      if (Beam != None)
         Beam.SphereCharge = ChargeScale;

      if (bDoHit /*&& Other != None && Other != Instigator*/)
      {
         foreach Weapon.VisibleCollidingActors(class 'KFMonster', KFM, 250/* * ChargeScale*/, EndEffect)
         {
            if (KFM != none /*&& KFM != Other*/)
            {
               if (KFM.Health > 0)
                  KFM.SetZapped(ZapAmount, Instigator);
            }
         }
      }

      // beam effect is created and destroyed when firing starts and stops
      if ((Beam == None) && bIsFiring)
         Beam = Weapon.Spawn(BeamEffectClass, Instigator);

      if (Beam != None)
      {
         Beam.bHitSomething = (Other != None);
         Beam.EndEffect = EndEffect;
      }
   }

   else
      StopFiring();

   bStartFire = false;
   bDoHit = false;
}

defaultproperties
{
   BeamEffectClass=Class'ZEDBeamEffectFalk'
   DamageType=none
   FireRate=0.120000
   RecoilRate=0.070000
   AmmoClass=Class'ZEDGunAmmoFalk'
   AmmoPerFire=1
   MaxChargeTime=1.000000
   MaxZedSphereChargeTime=3.000000
   ZapAmount=1.67
}
