class LocalMediShotPickupFalk extends KFWeaponPickup;

#exec OBJ LOAD FILE=LairStaticMeshes_SM.usx
#exec OBJ LOAD FILE=LairSounds_S.uax

var int fStoredAmmo; // remaining ammo stored into the pickup when dropped

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// set fStoredAmmo to the weapon class on pickup
auto state pickup
{
   function BeginState()
   {
      UntriggerEvent(Event, self, None);
      if ( bDropped )
      {
         AddToNavigation();
         SetTimer(20, false);
      }
   }

   // When touched by an actor.  Let's mod this to account for Weights. (Player can't pickup items)
   // IF he's exceeding his max carry weight.
   function Touch(Actor Other)
   {
      local Inventory Copy;
      local Falk689GameTypeBase Flk;

      if ( KFHumanPawn(Other) != none && !CheckCanCarry(KFHumanPawn(Other)) )
      {
         return;
      }

      // If touched by a player pawn, let him pick this up.
      if ( ValidTouch(Other) )
      {
         Copy = SpawnCopy(Pawn(Other));

         AnnouncePickup(Pawn(Other));
         SetRespawn();
 
         if (Copy != None)
         {
            if (MediShotFalk(Copy) != none)
            {
               Flk = Falk689GameTypeBase(Level.Game);

               if (Flk != none && Flk.InZedTime())
                  MediShotFalk(Copy).StartForceReload();

               else if (MediShotFalk(Copy).fStoredAmmo == -1)
               {
                  MediShotFalk(Copy).fStoredAmmo        = fStoredAmmo;
                  MediShotFalk(Copy).fPickupAmmoRestore = True;
                  MediShotFalk(Copy).fPawnAmmoRestore   = True;
                  //Warn("Recovering from pickup: "@fStoredAmmo);
               }
            }

            Copy.PickupFunction(Pawn(Other));
         }

         if ( MySpawner != none && KFGameType(Level.Game) != none )
         {
            KFGameType(Level.Game).WeaponPickedUp(MySpawner);
         }

         if ( KFWeapon(Copy) != none )
         {
            KFWeapon(Copy).SellValue = SellValue;
            KFWeapon(Copy).bPreviouslyDropped = bDropped;

            if ( !bPreviouslyDropped && KFWeapon(Copy).bIsTier3Weapon &&
                  Pawn(Other).Controller != none && Pawn(Other).Controller != DroppedBy )
            {
               KFWeapon(Copy).Tier3WeaponGiver = DroppedBy;
            }
         }
      }
   }
}

// add a pickup sound cooldown
function AnnouncePickup(Pawn Receiver)
{
    local FHumanPawn FP;

    Receiver.HandlePickup(self);

    FP = FHumanPawn(Receiver);

    if (FP == none || FP.FShouldPlayPickupSound())
        PlaySound(PickupSound, SLOT_Interact, 2.0);
}

defaultproperties
{
   Weight=4.000000
   cost=2400
   AmmoCost=8
   BuyClipSize=4
   PowerValue=67
   SpeedValue=15
   RangeValue=70
   ItemName="Medical Serbu Super-Shorty Shotgun"
   ItemShortName="SSS-M"
   AmmoItemName="12-gauge shells"
   EquipmentCategoryID=2
   InventoryType=Class'LocalMediShotFalk'
   PickupMessage="You got a Medical Serbu Super-Shorty Shotgun"
   PickupSound=Sound'LairSounds_S.pickups.StandardPickupSound'
   PickupForce="AssaultRiflePickup"
   StaticMesh=StaticMesh'LairStaticMeshes_SM.CustomReskins.MedishotPickup'
   CollisionRadius=25.000000
   CollisionHeight=5.000000
}
