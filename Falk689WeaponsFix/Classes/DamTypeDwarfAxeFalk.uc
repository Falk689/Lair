class DamTypeDwarfAxeFalk extends KFMod.DamTypeDwarfAxe;

DefaultProperties
{
    WeaponClass=Class'DwarfAxeFalk'
    KDamageImpulse=25000.000000
    KDeathUpKick=350
    KDeathVel=300.000000
    HeadShotDamageMult=1.100000
}