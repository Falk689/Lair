class DamTypeDeagleFalk extends KFMod.DamTypeDeagle;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FSharpshooterDamage', Amount);
}

defaultproperties
{
   WeaponClass=Class'DeagleFalk'
   DeathString="%k killed %o (Deagle)"
   HeadShotDamageMult=1.100000
}
