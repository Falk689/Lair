class KS23SABulletFalk extends ShotgunBulletFalk;

defaultproperties
{
   FDamage=60.000000
   MaxPenetrations=2
   PenDamageReduction=0.5000
   HeadShotDamageMult=1.100000
   MyDamageType=Class'DamTypeKS23SAFalk'
}