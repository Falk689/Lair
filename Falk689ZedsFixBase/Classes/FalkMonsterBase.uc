class FalkMonsterBase extends KFMod.KFMonster
   abstract;

enum FalkZedClass
{
   FALK_Clot,
   FALK_Gorefast,
   FALK_Stalker,
   FALK_Crawler,
   FALK_Bloat,
   FALK_Siren,
   FALK_Husk,
   FALK_Brute,
   FALK_Scrake,
   FALK_Fleshpound,
   FALK_Boss,
   FALK_Metal_Clot,
};

var bool        FIsBoss;                  // this zed is a boss, activate boss features
var bool        FBlockAllDamage;          // this zed is (hopefully temporarily) immortal, don't take damage
var bool        FZMultiFix;               // if true, activate multi hit bug fix

var bool        fHarpoonHit;              // was this zed hit by an harpoon?
var int         fStunThresh;              // fixed amount of damage needed to stun this zed
var int         fUsedThresh;              // used amount for the fixed damage needed to stun this zed, equal to fStunTresh at normal edited by code
var float       fUsedHardMult;            // multiplier for fUsedThresh at hard difficulty
var float       fUsedSuiMult;             // multiplier for fUsedThresh at suicidal difficulty
var float       fUsedHoeMult;             // multiplier for fUsedThresh at hell on earth difficulty
var float       fUsedBBMult;              // multiplier for fUsedThresh at bloodbath difficulty
var float       fStunMult;                // alternative stun implementation, based on max health
var bool        fFrozen;                  // used to check if we're frozen and tweak how we behave
var bool        fIsStunned;               // used by stuff that can rage to be correctly stunned
var bool        fFlinching;               // is this zed flinching?

replication
{
   reliable if(Role == ROLE_Authority)
      fHarpoonHit, fFrozen, fIsStunned, fFlinching, fClientKillTaunt;
}

// return head scale for this zed while hit by a nade
function float FalkGetNadeHeadScale()
{
   if (FalkGetZedClass() == FALK_Gorefast)
      return 1.0;

   if (FalkGetZedClass() == FALK_Stalker)
      return 1.0;

   if (FalkGetZedClass() == FALK_Bloat)
      return 1.1;

   if (FalkGetZedClass() == FALK_Husk)
      return 1.0;

   if (FalkGetZedClass() == FALK_Siren)
      return 1.0;

   if (FalkGetZedClass() == FALK_Brute)
      return 1.2;

   if (FalkGetZedClass() == FALK_Boss)
      return 1.0;

   return 0;
}

// return zed class
function FalkZedClass FalkGetZedClass()
{
   return Falk_Boss;
}

// start a delayed kill taunt, used for the pat
function FKillTauntStart(int FAlivePlayers)
{
}

// taunt replicated on client, hopefully it should work like knockdown on other zeds
simulated function fClientKillTaunt()
{
}

defaultproperties
{
}
