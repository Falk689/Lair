Class MutKillMessageFalk extends MutKillMessage;

defaultproperties
{
     FriendlyName="Lair - Kills and damage messages"
     Description="Shows damage received by specimens and eventual kills."
}