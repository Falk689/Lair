class FreezerAmmoPickup extends KFAmmoPickup;

defaultproperties
{
     AmmoAmount=40
     InventoryType=Class'FreezerAmmo'
     PickupMessage="You got liquid nitrogen"
     StaticMesh=StaticMesh'KillingFloorStatics.FT_AmmoMesh'
}
